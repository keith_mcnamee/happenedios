import Foundation

class Happen {

    static let instance: Happen = Happen()

    let happenedHelper: HappenedHelper = HappenedHelper();

    let happenedModel: HappenedModel = HappenedModel();

    func applyPerformanceCommand( calculatorVO:CalculatorVO ) -> ApplyPerformanceCommand {
        return ApplyPerformanceCommand( calculatorVO:calculatorVO )
    }
}
