import Foundation

class HappenedHelper {

    private var app:App { return App.instance }
    private var balancing:Balancing { return Balancing.instance }
    private var happen: Happen { return Happen.instance }
    private var localization:Localization { return Localization.instance }

    func getNextFocalAncestor( seasonRef:Int, listingRef:Int ) -> CompetitionVO? {
        let listingBalancingVO:ListingBalancingVO? = balancing.balancingModel.getListingVO( listingRef, seasonRef: seasonRef )
        if listingBalancingVO == nil {
            return nil
        }
        var competitionVO:CompetitionVO? = getBestCompetitionVO(listingRef: listingRef, seasonRef: seasonRef)

        while competitionVO != nil {

            if( competitionVO!.balancingVO.competitionType == BalancingConstant.FOCAL_COMPETITION_TYPE )
            {
                return competitionVO
            }

            competitionVO = getBestCompetitionVO(competitionRef: competitionVO!.balancingVO.parentCompetitionRef, seasonRef: seasonRef)
        }
        return nil
    }

    func getBestCompetitionVO(listingRef:Int? = nil, competitionRef:Int? = nil, seasonRef:Int? = nil) -> CompetitionVO? {
        var thisCompetitionRef:Int? = competitionRef

        if thisCompetitionRef == nil && listingRef != nil {
            if let listingBalancingVO:ListingBalancingVO = balancing.balancingModel.getListingVO(listingRef!, seasonRef: seasonRef) {
                thisCompetitionRef = listingBalancingVO.parentCompetitionRef
            }
        }
        if thisCompetitionRef == nil {
            return nil
        }
        let competitionVO:CompetitionVO? = happen.happenedModel.competitionVOs[thisCompetitionRef!]
        if competitionVO == nil {
            return nil
        }
        if seasonRef == nil {
            return competitionVO
        }
        let seasonBalancingVO:SeasonBalancingVO? = balancing.balancingModel.seasonVOs[seasonRef!]
        if seasonBalancingVO == nil {
            return nil
        }

        let orderedSeasonGroupRefs:[Int] = balancing.balancingHelper.getOrderedSeasonGroupRefs(seasonBalancingVO!.seasonGroupRefs!)
        for seasonGroupRef:Int in orderedSeasonGroupRefs {
            if let competitionSeasonGroupVO:CompetitionVO = competitionVO!.seasonGroupVOs![seasonGroupRef] {
                return competitionSeasonGroupVO
            }
        }
        return competitionVO
    }

    func listingSeasonPriority( _ listingSeasonRefs:[Int:Any] ) -> [Int] {
        var scores:[Int:Int] = [:]
        var listingSeasonVOs:[Int:ListingSeasonVO] = [:]

        for listingSeasonRef:Int in listingSeasonRefs.keys {
            if let listingSeasonVO:ListingSeasonVO = happen.happenedModel.listingSeasonVOs[ listingSeasonRef ] {
                scores[ listingSeasonRef ] = 0
                listingSeasonVOs[ listingSeasonRef ] = listingSeasonVO
            }
        }

        for listingSeason1VO:ListingSeasonVO in listingSeasonVOs.values {
            var started = false
            for listingSeason2VO:ListingSeasonVO in listingSeasonVOs.values {
                if !started {
                    if listingSeason1VO.ref == listingSeason2VO.ref {
                        started = true
                    }
                    continue
                }
                var compareScores:Int = compareListings( listingRef1:listingSeason1VO.listingRef, seasonRef1:listingSeason1VO.seasonRef, listingRef2:listingSeason2VO.listingRef, seasonRef2:listingSeason2VO.seasonRef )
                if( compareScores != 0 )
                {
                    if compareScores > 0 {
                        scores[ listingSeason1VO.ref ]! += 1
                    } else {
                        scores[ listingSeason2VO.ref ]! += 1
                    }
                }
                else
                {
                    compareScores = compareListings( listingRef1:listingSeason2VO.listingRef, seasonRef1:listingSeason2VO.seasonRef, listingRef2:listingSeason1VO.listingRef, seasonRef2:listingSeason1VO.seasonRef)
                    if compareScores > 0 {
                        scores[ listingSeason1VO.ref ]! += 1
                    } else if compareScores < 0 {
                        scores[ listingSeason2VO.ref ]! += 1
                    }
                }
            }
        }

        var prioritisedRefs:[Int] = app.arrayHelper.sortKeysByValue( scores )
        prioritisedRefs.reverse()

        return prioritisedRefs
    }

    func listingPriority( _ listingRefs:[Int:Any], seasonRef:Int ) -> [Int] {
        var scores:[Int:Int] = [:]

        for listingRef:Int in listingRefs.keys {
            scores[listingRef] = 0
        }

        for listingRef1:Int in listingRefs.keys {
            var started = false
            for listingRef2:Int in listingRefs.keys {
                if !started {
                    if listingRef1 == listingRef2 {
                        started = true
                    }
                    continue
                }
                var compareScores:Int = compareListings( listingRef1:listingRef1, seasonRef1:seasonRef, listingRef2:listingRef2, seasonRef2:seasonRef )
                if( compareScores != 0 )
                {
                    if compareScores > 0 {
                        scores[ listingRef1]! += 1
                    } else {
                        scores[ listingRef2 ]! += 1
                    }
                }
                else
                {
                    compareScores = compareListings( listingRef1:listingRef2, seasonRef1:seasonRef, listingRef2:listingRef1, seasonRef2:seasonRef)
                    if compareScores > 0 {
                        scores[ listingRef1 ]! += 1
                    } else if compareScores < 0 {
                        scores[ listingRef2 ]! += 1
                    }
                }
            }
        }

        var prioritisedRefs:[Int] = app.arrayHelper.sortKeysByValue( scores )
        prioritisedRefs.reverse()

        return prioritisedRefs
    }

    func orderCompetitionVOs( _ competitionVOs:[CompetitionVO] ) -> [CompetitionVO] {
        var vos:[CompetitionVO] = competitionVOs
        vos.sort {
            let score:Int = compareCompetitionBalancingVOs( $0.balancingVO, $1.balancingVO )
            if score > 0 {
                return true
            }
            return false
        }
        return vos
    }

    func orderListingSeasonVOs( _ listingSeasonVOs:[ListingSeasonVO], byShowPriority:Bool = false ) -> [ListingSeasonVO] {
        var vos:[ListingSeasonVO] = listingSeasonVOs
        vos.sort {
            let score:Int = compareListings( listingRef1:$0.listingRef, seasonRef1:$0.seasonRef, listingRef2:$1.listingRef, seasonRef2:$1.seasonRef, byShowPriority:byShowPriority )
            if score > 0 {
                return true
            }
            return false
        }
        return vos

    }

    func getPlaceEventVOs(_ placeEventRefs:[Int:AnyObject], skipRemoved:Bool = false, securedOnly:Bool = false) -> [Int:PlaceEventVO] {
        var vos:[Int:PlaceEventVO] = [:]
        for placeEventRef:Int in placeEventRefs.keys {
            let placeEventVO:PlaceEventVO? = happen.happenedModel.placeEventVOs[placeEventRef]
            if placeEventVO == nil {
                continue
            }
            if !placeEventVO!.permanentPlace {
                if skipRemoved && Bool(placeEventVO!.removedDate != nil) && Bool(placeEventVO!.removedDate! >= Date()) {
                    continue
                }
                if securedOnly && Bool(placeEventVO!.securedDate != nil) && Bool(placeEventVO!.securedDate! >= Date()) {
                    continue
                }
            }
            vos[placeEventRef] = placeEventVO
        }
        return vos
    }

    func sortFixtureVOs( dicFixtureVOs:[Int:FixtureVO]? = nil, arrFixtureVOs:[FixtureVO]? = nil, descendingDate:Bool = false ) -> [FixtureVO] {

        if dicFixtureVOs == nil && arrFixtureVOs == nil {
            return []
        }
        var returnArrFixtureVOs:[FixtureVO] = dicFixtureVOs != nil ? [FixtureVO](dicFixtureVOs!.values) : arrFixtureVOs!

        var listingSeasonRefs:[Int:NSNull] = [:]
        var teamListingVOs:[Int:TeamListingVO] = [:]
        var listingSeasonRefPriorities:[Int:Int] = [:]
        var teamNames:[Int:String] = [:]
        var fixtureListingPriorities:[Int:Int] = [:]
        var fixtureTeamNames:[Int:String] = [:]

        for fixtureVO:FixtureVO in returnArrFixtureVOs {
            let listingSeasonVO:ListingSeasonVO = happen.happenedModel.getListingSeasonVO( fixtureVO.seasonRef, fixtureVO.listingRef )!
            let homeTeamListingVO:TeamListingVO = happen.happenedModel.getTeamListingVO( fixtureVO.seasonRef, fixtureVO.listingRef, fixtureVO.homeRef  )!
            let awayTeamListingVO:TeamListingVO = happen.happenedModel.getTeamListingVO( fixtureVO.seasonRef, fixtureVO.listingRef, fixtureVO.awayRef  )!
            listingSeasonRefs[ listingSeasonVO.ref ] = NSNull()
            teamListingVOs[ homeTeamListingVO.ref ] = homeTeamListingVO
            teamListingVOs[ awayTeamListingVO.ref ] = awayTeamListingVO
        }

        let prioritisedListingSeasonRefs:[Int] = listingSeasonPriority( listingSeasonRefs )
        var priority:Int = 0
        for listingSeasonRef in prioritisedListingSeasonRefs {
            priority += 1
            listingSeasonRefPriorities[ listingSeasonRef ] = priority
        }

        for teamListingVO:TeamListingVO in teamListingVOs.values {
            let teamName:String = localization.localizationHelper.getTeamName(teamListingVO.teamRef, seasonRef: teamListingVO.seasonRef)+"_"+String(teamListingVO.teamRef)+"_"+String(teamListingVO.seasonRef)
            teamNames[ teamListingVO.ref ] = teamName
        }

        for fixtureVO:FixtureVO in returnArrFixtureVOs {
            let listingSeasonVO:ListingSeasonVO = happen.happenedModel.getListingSeasonVO(fixtureVO.seasonRef, fixtureVO.listingRef)!
            let homeTeamListingVO:TeamListingVO = happen.happenedModel.getTeamListingVO(fixtureVO.seasonRef, fixtureVO.listingRef, fixtureVO.homeRef)!
            let awayTeamListingVO:TeamListingVO = happen.happenedModel.getTeamListingVO(fixtureVO.seasonRef, fixtureVO.listingRef, fixtureVO.awayRef)!
            let listingPriority:Int = listingSeasonRefPriorities[ listingSeasonVO.ref ]!
            let homeTeamName:String = teamNames[ homeTeamListingVO.ref ]!
            let awayTeamName:String = teamNames[ awayTeamListingVO.ref ]!
            fixtureListingPriorities[fixtureVO.ref] = listingPriority
            fixtureTeamNames[fixtureVO.ref] = homeTeamName+"__"+awayTeamName
        }

        returnArrFixtureVOs.sort {
            if descendingDate  {
                if $0.sortingDate < $1.sortingDate {
                    return false
                }
                if $0.sortingDate > $1.sortingDate {
                    return true
                }
            } else {
                if $0.sortingDate > $1.sortingDate {
                    return false
                }
                if $0.sortingDate < $1.sortingDate {
                    return true
                }
            }
            let aListingPriority:Int = fixtureListingPriorities[$0.ref]!
            let bListingPriority:Int = fixtureListingPriorities[$1.ref]!
            if aListingPriority > bListingPriority {
                return false
            }
            if aListingPriority < bListingPriority {
                return true
            }
            let aTeamName:String = fixtureTeamNames[$0.ref]!
            let bTeamName:String = fixtureTeamNames[$1.ref]!
            if aTeamName > bTeamName {
                return false
            }
            if aTeamName < bTeamName {
                return true
            }
            return false
        }

        return returnArrFixtureVOs
    }

    func haveCompleteListingSeasons(_ seasonRefs:[Int:Any], _ listingRefs:[Int:Any]) -> Bool {
        for seasonRef:Int in seasonRefs.keys {
            let nonExistSeasonVO:IntDicVOsVO? = happen.happenedModel.nonExistentListingSeasons[seasonRef]
            if nonExistSeasonVO != nil {
                continue
            }
            let seasonVO:SeasonVO? = happen.happenedModel.seasonVOs[seasonRef]
            if seasonVO == nil || !seasonVO!.haveData {
                return false
            }
            for listingRef:Int in listingRefs.keys {
                if nonExistSeasonVO?.vos[listingRef] != nil {
                    continue
                }
                let listingSeasonRef:Int? = seasonVO!.listingSeasonRefs[listingRef]
                let listingSeasonVO:ListingSeasonVO? = listingSeasonRef != nil ? happen.happenedModel.getListingSeasonVO(seasonRef, listingRef) : nil

                if listingSeasonVO == nil || !listingSeasonVO!.haveData {
                    return false
                }
            }

        }
        return true
    }

    func storedFixtureList( seasonRefs:[Int:Any], listingRefs:[Int:Any], includeHappened:Bool, includeUpcoming:Bool, descendingDate:Bool, focusOnTeams:[Int:NSNull]?=nil ) -> [FixtureVO] {
        var fixtureVOs:[FixtureVO] = []
        for seasonRef:Int in seasonRefs.keys {
            let seasonVO:SeasonVO? = happen.happenedModel.seasonVOs[seasonRef]
            if seasonVO == nil {
                continue
            }
            for listingRef:Int in listingRefs.keys {
                let listingSeasonRef:Int? = seasonVO!.listingSeasonRefs[listingRef]
                if listingSeasonRef == nil {
                    continue
                }
                let listingSeasonVO:ListingSeasonVO? = happen.happenedModel.getListingSeasonVO(seasonRef, listingRef)
                if listingSeasonVO == nil {
                    continue
                }
                let inFixtureVOs:[Int:FixtureVO] = getFixtureVOsInRefs(inRefs: listingSeasonVO!.fixtureRefs, activeOnly: true)
                for fixtureVO:FixtureVO in inFixtureVOs.values {
                    if fixtureVO.resultVO.ignoreFixture {
                        continue
                    }
                    if focusOnTeams != nil {
                        if focusOnTeams![fixtureVO.homeRef] == nil && focusOnTeams![fixtureVO.awayRef] == nil {
                            continue
                        }
                    }

                    if includeHappened && fixtureVO.resultVO.played {
                        fixtureVOs.append( fixtureVO )
                    }
                    if includeUpcoming && !fixtureVO.resultVO.played {
                        fixtureVOs.append( fixtureVO )
                    }
                }
            }
        }
        return sortFixtureVOs(arrFixtureVOs: fixtureVOs, descendingDate: descendingDate)
    }

    func getFixtureVOsInRefs( inRefs:[Int:Any], activeOnly:Bool = false, parkedOnly:Bool = false ) -> [Int:FixtureVO] {
        let allFixtureVOs:[Int:FixtureVO] = !activeOnly && !parkedOnly ? happen.happenedModel.fixtureVOs : activeOnly ? happen.happenedModel.activeFixtureVOs : happen.happenedModel.parkedFixtureVOs
        var fixtureVOs:[Int:FixtureVO] = [:]
        for fixtureRef:Int in inRefs.keys {
            if let fixtureVO:FixtureVO = allFixtureVOs[fixtureRef] {
                fixtureVOs[fixtureVO.ref] = fixtureVO
            }
        }
        return fixtureVOs
    }

    func getFocalCompetitionRef( listingRef:Int, seasonRef:Int?=nil, avoidMatchListingRefs:[Int:Any]?=nil ) -> Int? {
        var avoidCompetitionRefs:[Int:NSNull] = [:]
        if avoidMatchListingRefs != nil {
            for checkListingRef:Int in avoidMatchListingRefs!.keys {
                if checkListingRef == listingRef {
                    continue
                }
                let checkListingBalancingVO:ListingBalancingVO? = balancing.balancingModel.getListingVO(checkListingRef, seasonRef: seasonRef)
                if checkListingBalancingVO == nil{
                    continue
                }
                var checkCompetitionRef:Int? = checkListingBalancingVO!.parentCompetitionRef!
                var checkCompetitionRefs:[Int:NSNull] = [checkCompetitionRef!: NSNull()]

                var checkFocalFound:Bool = false
                while checkCompetitionRef != nil {
                    let checkCompetitionSeasonGroupVO:CompetitionVO? = happen.happenedHelper.getBestCompetitionVO( competitionRef: checkCompetitionRef, seasonRef:seasonRef )
                    if checkCompetitionSeasonGroupVO == nil{
                        continue
                    }
                    let checkCompetitionBalancingVO:CompetitionBalancingVO = checkCompetitionSeasonGroupVO!.balancingVO
                    checkCompetitionRef = checkCompetitionBalancingVO.parentCompetitionRef
                    if checkCompetitionBalancingVO.competitionType == BalancingConstant.FOCAL_COMPETITION_TYPE {
                        checkFocalFound = true
                        break
                    }
                    checkCompetitionRefs[ checkCompetitionRef! ] = NSNull()
                }
                if checkFocalFound {
                    avoidCompetitionRefs = app.arrayHelper.mergeIntNulDics(avoidCompetitionRefs, checkCompetitionRefs)
                }
            }
        }

        let listingBalancingVO:ListingBalancingVO? = balancing.balancingModel.getListingVO( listingRef, seasonRef: seasonRef )
        if listingBalancingVO == nil {
            return nil
        }
        var competitionRef:Int? = listingBalancingVO!.parentCompetitionRef!

        while competitionRef != nil{
            let competitionSeasonGroupVO:CompetitionVO? = happen.happenedHelper.getBestCompetitionVO( competitionRef: competitionRef, seasonRef:seasonRef)
            if competitionSeasonGroupVO == nil {
                continue
            }
            let competitionBalancingVO:CompetitionBalancingVO = competitionSeasonGroupVO!.balancingVO
            if avoidCompetitionRefs[ competitionRef! ] != nil {
                return nil
            }
            if competitionBalancingVO.competitionType == BalancingConstant.FOCAL_COMPETITION_TYPE {
                return competitionRef
            }
            competitionRef = competitionBalancingVO.parentCompetitionRef
        }
        return nil
    }




    func hasNonExistentListingSeason( _ seasonRef:Int, _ listingRef:Int) -> Bool {
        return happen.happenedModel.nonExistentListingSeasons[ seasonRef ]?.vos[listingRef] != nil
    }

    func cloneCalculatorVO ( _ calculatorVO:CalculatorVO, resultVOs:[Int:ResultVO] = [:] ) -> CalculatorVO {

        var clonedCalculatorVO:CalculatorVO = CalculatorVO( calculatorVO.listingSeasonRef )

        func cloneTeamListingVOs () {

            for teamListingVO in calculatorVO.teamListingVOs.values {

                let clonedTeamListingVO:TeamListingVO = TeamListingVO(teamListingVO.ref, teamListingVO.seasonRef, teamListingVO.listingRef, teamListingVO.teamRef)

                clonedTeamListingVO.globalRank = teamListingVO.globalRank

                clonedTeamListingVO.adjustTeamRefs = teamListingVO.adjustTeamRefs
                clonedTeamListingVO.eventTeamVOs = teamListingVO.eventTeamVOs
                clonedTeamListingVO.bestListingScenarioRef = teamListingVO.bestListingScenarioRef
                clonedTeamListingVO.worstListingScenarioRef = teamListingVO.worstListingScenarioRef

                clonedTeamListingVO.splitPotIndex = teamListingVO.splitPotIndex

                clonedCalculatorVO.teamListingVOs[ clonedTeamListingVO.teamRef ] = clonedTeamListingVO
            }
        }

        func cloneFixtureVOs (){
            for fixtureVO in calculatorVO.fixtureVOs.values {

                let clonedFixtureVO:FixtureVO = FixtureVO(fixtureVO.ref, fixtureVO.seasonRef, fixtureVO.listingRef , fixtureVO.homeRef, fixtureVO.awayRef)

                clonedFixtureVO.playDate = fixtureVO.playDate

                clonedFixtureVO.resultVO = resultVOs[ fixtureVO.ref ] != nil ? resultVOs[ fixtureVO.ref ]! : fixtureVO.resultVO.birthClone(fixtureVO.resultVO.fixtureRef)

                clonedCalculatorVO.fixtureVOs[ clonedFixtureVO.ref ] = clonedFixtureVO
                if !clonedFixtureVO.resultVO.ignoreFixture {
                    clonedCalculatorVO.activeFixtureVOs[ clonedFixtureVO.ref ] = clonedFixtureVO
                } else {
                    clonedCalculatorVO.parkedFixtureVOs[ clonedFixtureVO.ref ] = clonedFixtureVO
                }
            }
        }

        func cloneSplSplitVO(){
            clonedCalculatorVO.splSplitVO.includesBasicSplitInfo = calculatorVO.splSplitVO.includesBasicSplitInfo
            clonedCalculatorVO.splSplitVO.invalidData = calculatorVO.splSplitVO.includesBasicSplitInfo
            clonedCalculatorVO.splSplitVO.noOfTopSplitPlaces = calculatorVO.splSplitVO.noOfTopSplitPlaces
            clonedCalculatorVO.splSplitVO.splitOccurred = calculatorVO.splSplitVO.includesBasicSplitInfo
            //only want to copy basic split info
        }

        cloneTeamListingVOs()
        cloneFixtureVOs()
        cloneSplSplitVO()

        return clonedCalculatorVO
    }

    func parentCompetitionVO(_ seasonRef:Int, _ listingRef:Int ) -> CompetitionVO? {
        if let listingBalancingVO:ListingBalancingVO = balancing.balancingModel.getListingVO(listingRef, seasonRef: seasonRef) {
            if let happenedVO:CompetitionVO = happen.happenedHelper.getBestCompetitionVO(competitionRef: listingBalancingVO.parentCompetitionRef, seasonRef:seasonRef) {
                return happenedVO
            }
        }
        return nil
    }

    func eventScenarioOccurred( primaryVO:EventScenarioVO?, secondaryVO:EventScenarioVO? ) -> Bool {
        return (primaryVO != nil && primaryVO!.occurred) && !( secondaryVO != nil && secondaryVO!.occurred )
    }

    func eventScenarioCanNotOccur( primaryVO:EventScenarioVO?, secondaryVO:EventScenarioVO? ) -> Bool {
        return !(primaryVO != nil && primaryVO!.occurred) && ( secondaryVO != nil && secondaryVO!.occurred )
    }


    func haveUnscheduledFixtures( _ seasonRef:Int, _ listingRef:Int ) -> Bool {
        let listingSeasonVO:ListingSeasonVO? = happen.happenedModel.getListingSeasonVO( seasonRef, listingRef)
        if listingSeasonVO == nil {
            return false
        }
        for fixtureRef:Int in listingSeasonVO!.fixtureRefs.keys {
            let fixtureVO:FixtureVO? = happen.happenedModel.activeFixtureVOs[fixtureRef]
            if fixtureVO == nil {
                continue
            }
            if fixtureVO!.playDate == nil && !fixtureVO!.resultVO.ignoreFixture {
                return true
            }
        }
        return false
    }

    private func compareListings(listingRef1:Int, seasonRef1:Int, listingRef2:Int, seasonRef2:Int, byShowPriority:Bool = false ) -> Int {
        
        if listingRef1 == listingRef2 {
            let season1VO:SeasonBalancingVO? = balancing.balancingModel.seasonVOs[seasonRef1]
            let season2VO:SeasonBalancingVO? = balancing.balancingModel.seasonVOs[seasonRef2]
            if( season1VO == nil || season2VO == nil )
            {
                return 0
            }
            return compareSeasonBalancingVOs( season1VO!, season2VO! )
        }
        
        var competition1VO:CompetitionVO? = getBestCompetitionVO(listingRef: listingRef1, seasonRef: seasonRef1)
        var competition2VO:CompetitionVO? = getBestCompetitionVO(listingRef: listingRef2, seasonRef: seasonRef2)
        
        var childCompetition1VO:CompetitionVO? = nil
        var childCompetition2VO:CompetitionVO? = nil
        while competition1VO != nil {
            while competition2VO != nil {
                if competition1VO!.ref == competition2VO!.ref {
                    if ( childCompetition1VO != nil  && childCompetition2VO == nil ) || ( childCompetition1VO == nil && childCompetition2VO != nil ) {
                        return 0
                    }
                    if childCompetition1VO != nil{
                        return compareCompetitionBalancingVOs( childCompetition1VO!.balancingVO, childCompetition2VO!.balancingVO, byShowPriority: byShowPriority )
                    }
                    else
                    {
                        let listingBalancing1VO:ListingBalancingVO? = balancing.balancingModel.getListingVO(listingRef1, seasonRef: seasonRef1)
                        let listingBalancing2VO:ListingBalancingVO? = balancing.balancingModel.getListingVO(listingRef2, seasonRef: seasonRef2)
                        if listingBalancing1VO == nil || listingBalancing2VO == nil {
                            return 0
                        }
                        return compareListingBalancingVOs( listingBalancing1VO!, listingBalancing2VO! )
                    }
                }
                childCompetition2VO = competition2VO!
        
                competition2VO = competition2VO!.balancingVO.parentCompetitionRef != nil ? happen.happenedModel.getCompetitionVO( competition2VO!.balancingVO.parentCompetitionRef!, seasonGroupRef: competition2VO!.seasonGroupRef) : nil
            }
            childCompetition1VO = competition1VO
            childCompetition2VO = competition2VO
        
            competition1VO = competition1VO?.balancingVO.parentCompetitionRef != nil ? happen.happenedModel.getCompetitionVO( competition1VO!.balancingVO.parentCompetitionRef!, seasonGroupRef: competition1VO!.seasonGroupRef) : nil
            competition2VO = getBestCompetitionVO(listingRef: listingRef2, seasonRef: seasonRef2)!
        }
        return 0
    }

    private func compareSeasonBalancingVOs( _ season1VO:SeasonBalancingVO, _ season2VO:SeasonBalancingVO ) -> Int {
    
        if season1VO.startDate! > season2VO.startDate! {
            return 1
        } else if season1VO.startDate! < season2VO.startDate! {
            return -1
        } else {
            if season1VO.endDate! > season2VO.endDate! {
                return 1
            } else if season1VO.endDate! < season2VO.endDate! {
                return -1
            }
        }
        return 0
    }

    private func compareCompetitionBalancingVOs( _ competition1VO:CompetitionBalancingVO, _ competition2VO:CompetitionBalancingVO, byShowPriority:Bool = false ) -> Int {

        let priority1:Int = !byShowPriority ? competition1VO.listPriority! : competition1VO.showPriority!
        let priority2:Int = !byShowPriority ? competition2VO.listPriority! : competition2VO.showPriority!
        if priority1 < priority2 {
            return 1
        } else if priority1 > priority2 {
            return -1
        } else {

            let name1:String = localization.localizationHelper.getCompetitionName( competition1VO.ref, seasonRef:competition1VO.seasonRef )
            let name2:String = localization.localizationHelper.getCompetitionName( competition2VO.ref, seasonRef:competition2VO.seasonRef )
            let lcName1:String = name1.lowercased()
            let lcName2:String = name2.lowercased()

            if lcName1 < lcName2 {
                return 1
            } else if lcName2 < lcName1 {
                return -1
            } else {
                if name1 < name2 {
                    return 1
                } else if name2 < name1 {
                    return -1
                }
            }
        }
        return 0
    }

    private func compareListingBalancingVOs( _ listing1VO:ListingBalancingVO, _ listing2VO:ListingBalancingVO ) -> Int {

        if listing1VO.listPriority! < listing2VO.listPriority! {
            return 1
        } else if listing1VO.listPriority! > listing2VO.listPriority! {
            return -1
        } else {
            let name1:String = localization.localizationHelper.getListingName( listing1VO.ref, seasonRef:listing1VO.seasonRef )
            let name2:String = localization.localizationHelper.getListingName( listing2VO.ref, seasonRef:listing2VO.seasonRef )
            let lcName1:String = name1.lowercased()
            let lcName2:String = name2.lowercased()

            if lcName1 < lcName2 {
                return 1
            } else if lcName2 < lcName1 {
                return -1
            } else {
                if name1 < name2 {
                    return 1
                } else if name2 < name1 {
                    return -1
                }
            }
        }
        return 0
    }
}
