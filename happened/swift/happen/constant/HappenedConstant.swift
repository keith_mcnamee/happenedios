import Foundation

enum IsHappenedState {
    case happenedOnly, upcomingOnly, both, scenario
}

enum ToHappenedState {
    case toHappenOnly, toNotHappenOnly, both
}