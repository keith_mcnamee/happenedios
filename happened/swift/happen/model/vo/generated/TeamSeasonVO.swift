import Foundation

class TeamSeasonVO {

    let seasonRef:Int
    let teamRef:Int

    var teamListingRefs:[Int:Int] = [:]
    var haveData:Bool = false

    init(_ seasonRef:Int, _ teamRef:Int) {
        self.seasonRef = seasonRef
        self.teamRef = teamRef
    }

}
