import Foundation

class EventListingVO {

    var seasonRef:Int
    var listingRef:Int
    var eventRef:Int

    var placeEventRefs:[Int:NSNull] = [:]

    init(_ seasonRef:Int, _ listingRef:Int, _ eventRef:Int) {
        self.seasonRef = seasonRef
        self.listingRef = listingRef
        self.eventRef = eventRef
    }
}
