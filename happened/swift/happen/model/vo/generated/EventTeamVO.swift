import Foundation

class EventTeamVO {

    var seasonRef:Int
    var listingRef:Int
    var teamRef:Int
    var eventRef:Int

    var scenarioToHappenRef:Int?
    var scenarioNotHappenRef:Int?
    var exemptionRef:Int?

    init(_ seasonRef:Int, _ listingRef:Int, _ teamRef:Int, _ eventRef:Int) {
        self.seasonRef = seasonRef
        self.listingRef = listingRef
        self.teamRef = teamRef
        self.eventRef = eventRef
    }

}
