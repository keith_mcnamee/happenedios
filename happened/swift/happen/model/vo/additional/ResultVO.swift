import Foundation

class ResultVO {

    let fixtureRef:Int

    var homeGoals:Int = 0
    var awayGoals:Int = 0
    var played:Bool = false
    var ignoreFixture:Bool = false

    init( _ fixtureRef:Int ) {
        self.fixtureRef = fixtureRef
    }

    func birthClone(_ fixtureRef:Int ) -> ResultVO {
        let vo:ResultVO = birthResult(fixtureRef)
        clone(vo)
        return vo
    }

    func clone(_ vo:ResultVO) {
        vo.homeGoals = homeGoals;
        vo.awayGoals = awayGoals;
        vo.played = played;
        vo.ignoreFixture = ignoreFixture;
    }

    func birthResult(_ fixtureRef:Int ) -> ResultVO {
        return ResultVO(fixtureRef)
    }
}
