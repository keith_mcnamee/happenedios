import Foundation

class CalculatorVO {

    let listingSeasonRef:Int
    let splSplitVO:SplSplitVO = SplSplitVO()

    var haveData:Bool = false
    var calculated:Bool = false

    var teamListingVOs:[Int:TeamListingVO] = [:]
    var fixtureVOs:[Int:FixtureVO] = [:]
    var activeFixtureVOs:[Int:FixtureVO] = [:]
    var parkedFixtureVOs:[Int:FixtureVO] = [:]
    var orderedTeamListingVOs:[TeamListingVO] = []
    var playedFixtureRefs:[Int:NSNull] = [:]
    var remainingFixtureRefs:[Int:NSNull] = [:]
    var playUpcomingVisibleFixtures:Bool = false
    var playUpcomingInvisibleFixtures:Bool = false
    var includeUpcomingNonFixtures:Bool = false
    var maximumPlaceEventRefs:[Int:IntDicVOsVO] = [:]
    var minimumPlaceEventRefs:[Int:IntDicVOsVO] = [:]
    var maximumAllToPlaceEventRefs:[Int:IntDicVOsVO] = [:]
    var minimumAllToPlaceEventRefs:[Int:IntDicVOsVO] = [:]
    var date:Date?

    init( _ listingSeasonRef:Int ) {
        self.listingSeasonRef = listingSeasonRef
    }
}
