import Foundation

class SplSplitVO {

    var includesBasicSplitInfo:Bool = false
    var invalidData:Bool = false
    var noOfTopSplitPlaces:Int = 0
    var splitOccurred:Bool = false
    var firstSplitFixtureDate:Date?

    var topSplitTeamRefs:[Int:NSNull] = [:]
    var bottomSplitTeamRefs:[Int:NSNull] = [:]
    var ignoreFixtureRefs:[Int:NSNull] = [:]
    var splitRoundFixtureRefs:[Int:NSNull] = [:]
    var preSplitFixtureRefs:[Int:NSNull] = [:]

    var isSplSplit: Bool {
        return noOfTopSplitPlaces > 0
    }

    func reset() {
        includesBasicSplitInfo = false
        invalidData = false
        noOfTopSplitPlaces = 0
        splitOccurred = false
        firstSplitFixtureDate = nil

        topSplitTeamRefs.removeAll()
        bottomSplitTeamRefs.removeAll()
        ignoreFixtureRefs.removeAll()
        splitRoundFixtureRefs.removeAll()
        preSplitFixtureRefs.removeAll()
    }

}
