import Foundation

class PerformanceVO {

    var played:Int = 0
    var remaining:Int = 0
    var points:Int = 0
    var goalsFor:Int = 0
    var goalsAgainst:Int = 0
    var gamesWon:Int = 0
    var gamesLost:Int = 0
    var gamesDrawn:Int = 0
    var awayWins:Int = 0
    var rankHigh:Int = 0
    var rankLow:Int = 0
    var miniLeagueRank:Int = 0
    var awayGoalsScored:Int = 0

    var goalDifference: Int {
        return goalsFor - goalsAgainst
    }

    func reset() {
        played = 0
        remaining = 0
        points = 0
        goalsFor = 0
        goalsAgainst = 0
        gamesWon = 0
        gamesLost = 0
        gamesDrawn = 0
        awayWins = 0
        rankHigh = 0
        rankLow = 0
        miniLeagueRank = 0
        awayGoalsScored = 0
    }

}
