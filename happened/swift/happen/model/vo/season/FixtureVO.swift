import Foundation

class FixtureVO:SeasonTypeVO {

    private var app:App { return App.instance }

    let homeRef:Int
    let awayRef:Int
    var resultVO:ResultVO

    var playDate:Date?

    var haveData:Bool = false

    init(_ ref:Int, _ seasonRef:Int, _ listingRef:Int, _ homeRef:Int, _ awayRef:Int) {

        self.homeRef = homeRef
        self.awayRef = awayRef

        resultVO = ResultVO(ref)

        super.init(ref, seasonRef, listingRef)
    }

    var sortingDate:Date {
        return playDate != nil ? playDate! : app.appModel.dayLast
    }

    var invisible:Bool {
        return resultVO.ignoreFixture || Bool(playDate == nil)
    }

}
