import Foundation

class PlaceEventVO:SeasonTypeVO {

    let eventRef:Int

    var securedDate:Date?
    var removedDate:Date?
    var placesForEvent:Int = 0
    var permanentPlace:Bool = false

    init(_ ref:Int, _ seasonRef:Int, _ listingRef:Int, _ eventRef:Int) {

        self.eventRef = eventRef

        super.init(ref, seasonRef, listingRef)
    }

}
