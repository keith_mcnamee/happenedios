import Foundation

class AdjustTeamVO:SeasonTypeVO {

    let teamRef:Int

    var appliedDate:Date?
    var pointsOffset:Int = 0
    var goalForOffset:Int = 0
    var goalAgainstOffset:Int = 0
    var awayGoalOffset:Int = 0
    var winOffset:Int = 0
    var drawOffset:Int = 0
    var lossOffset:Int = 0
    var awayWinOffset:Int = 0
    var adjustAgainstTeamRef:Int?

    init(_ ref:Int, _ seasonRef:Int, _ listingRef:Int, _ teamRef:Int) {

        self.teamRef = teamRef

        super.init(ref, seasonRef, listingRef)
    }

    var goalOffset:Int {
        return goalForOffset - goalAgainstOffset
    }

}
