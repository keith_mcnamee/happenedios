import Foundation

class ExemptionTeamVO:SeasonTypeVO {

    let teamRef:Int
    let eventRef:Int

    var appliedDate:Date?
    var eventReceivedByListing:Bool = false

    init(_ ref:Int, _ seasonRef:Int, _ listingRef:Int, _ teamRef:Int, _ eventRef:Int) {

        self.teamRef = teamRef
        self.eventRef = eventRef

        super.init(ref, seasonRef, listingRef)
    }

}
