import Foundation

class ListingSeasonVO:SeasonTypeVO {

    let calculatorVO:CalculatorVO
    var teamListingRefs:[Int:Int] = [:]
    var fixtureRefs:[Int:NSNull] = [:]
    var eventListingVOs:[Int:EventListingVO] = [:]
    var invalidTeamRefs:[Int:NSNull] = [:]
    var validEventRefs:[Int:NSNull] = [:]
    var splitCalculatorVO:CalculatorVO?

    var haveData:Bool = false
    var havePartialData:Bool = false

    override init(_ ref:Int, _ seasonRef:Int, _ listingRef:Int) {

        calculatorVO = CalculatorVO(ref)

        super.init(ref, seasonRef, listingRef)
    }

}
