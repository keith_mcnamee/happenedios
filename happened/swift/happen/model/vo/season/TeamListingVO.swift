import Foundation

class TeamListingVO:SeasonTypeVO {

    let teamRef:Int

    var haveData:Bool = false
    var havePartialData:Bool = false
    var haveScenario:Bool = false
    var globalRank:Int = 0

    var adjustTeamRefs:[Int:NSNull] = [:]
    var eventTeamVOs:[Int:EventTeamVO] = [:]
    var bestListingScenarioRef:Int?
    var worstListingScenarioRef:Int?

    var splitPotIndex:Int = 0
    var performanceVO:PerformanceVO = PerformanceVO()
    var miniLeaguePerformanceVO:PerformanceVO = PerformanceVO()

    init(_ ref:Int, _ seasonRef:Int, _ listingRef:Int, _ teamRef:Int) {

        self.teamRef = teamRef

        super.init(ref, seasonRef, listingRef)
    }

}
