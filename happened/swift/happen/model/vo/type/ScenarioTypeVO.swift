import Foundation

class ScenarioTypeVO: SeasonTypeVO {

    let teamRef:Int

    var invalid:Bool = false
    var finalCalculatorVO:CalculatorVO
    var occurredCalculatorVO:CalculatorVO?
    var splCalculatorVO:CalculatorVO?

    init(_ ref:Int, _ seasonRef:Int, _ listingRef:Int, _ teamRef:Int, finalCalculatorVO:CalculatorVO) {

        self.teamRef = teamRef

        self.finalCalculatorVO = finalCalculatorVO

        super.init(ref, seasonRef, listingRef)
    }
}
