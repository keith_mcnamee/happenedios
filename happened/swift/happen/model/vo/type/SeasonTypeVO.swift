import Foundation

class SeasonTypeVO: HappenedVO {

    let seasonRef:Int

    let listingRef:Int

    init(_ ref:Int, _ seasonRef:Int, _ listingRef:Int) {

        self.seasonRef = seasonRef
        self.listingRef = listingRef

        super.init(ref)
    }
}
