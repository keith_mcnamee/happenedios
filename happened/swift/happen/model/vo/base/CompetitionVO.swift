import Foundation

class CompetitionVO: HappenedVO {

    let balancingVO:CompetitionBalancingVO
    let seasonGroupRef:Int?
    var seasonGroupVOs:[Int: CompetitionVO]?

    var childCompetitionRefs:[Int:NSNull] = [:]
    var childListingSeasonRefs:[Int:NSNull] = [:]


    init(_ ref:Int, balancingVO:CompetitionBalancingVO, seasonGroupRef:Int?=nil) {
        self.balancingVO = balancingVO
        self.seasonGroupRef = seasonGroupRef

        super.init(ref)
    }

    var listingParent:Bool {
        return childListingSeasonRefs.count > 0
    }
}
