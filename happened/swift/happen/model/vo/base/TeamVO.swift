import Foundation

class TeamVO: HappenedVO {

    var havePartialData:Bool = false
    var haveData:Bool = false
    var teamSeasonVOs:[Int:TeamSeasonVO] = [:]
}
