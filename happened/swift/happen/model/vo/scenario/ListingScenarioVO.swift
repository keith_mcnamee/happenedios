import Foundation

class ListingScenarioVO: ScenarioTypeVO {

    let best:Bool

    var position:Int?

    init(_ ref:Int, _ seasonRef:Int, _ listingRef:Int, _ teamRef:Int, _ best:Bool, finalCalculatorVO:CalculatorVO) {

        self.best = best

        super.init(ref, seasonRef, listingRef, teamRef, finalCalculatorVO:finalCalculatorVO)
    }
}
