import Foundation

class EventScenarioVO: ScenarioTypeVO {

    let eventRef:Int
    let toHappen:Bool

    var upcoming:Bool = false
    var fromResult:Bool = false
    var occurredDate:Date?
    var occurredFixtureRef:Int?

    var haveData:Bool = false

    init(_ ref:Int, _ seasonRef:Int, _ listingRef:Int, _ teamRef:Int, _ eventRef:Int, _ toHappen:Bool, finalCalculatorVO:CalculatorVO) {

        self.eventRef = eventRef
        self.toHappen = toHappen

        super.init(ref, seasonRef, listingRef, teamRef, finalCalculatorVO:finalCalculatorVO)
    }

    var occurred:Bool {
        return !upcoming && !invalid
    }

}
