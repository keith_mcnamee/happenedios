import Foundation

class HappenedModel {

    var clientVersion:Int?

    var seasonVOs:[Int:SeasonVO] = [:]
    var seasonGroupVOs:[Int:SeasonGroupVO] = [:]
    var competitionVOs:[Int:CompetitionVO] = [:]
    var listingVOs:[Int:ListingVO] = [:]
    var teamVOs:[Int:TeamVO] = [:]
    var listingSeasonVOs:[Int:ListingSeasonVO] = [:]
    var teamListingVOs:[Int:TeamListingVO] = [:]
    var fixtureVOs:[Int:FixtureVO] = [:]
    var activeFixtureVOs:[Int:FixtureVO] = [:]
    var parkedFixtureVOs:[Int:FixtureVO] = [:]
    var adjustTeamVOs:[Int:AdjustTeamVO] = [:]
    var placeEventVOs:[Int:PlaceEventVO] = [:]
    var exemptionTeamVOs:[Int:ExemptionTeamVO] = [:]
    var eventScenarioVOs:[Int:EventScenarioVO] = [:]
    var listingScenarioVOs:[Int:ListingScenarioVO] = [:]
    var nonExistentSeasonRefs:[Int:NSNull] = [:]
    var nonExistentListingRefs:[Int:NSNull] = [:]
    var nonExistentTeamRefs:[Int:NSNull] = [:]
    var nonExistentListingSeasons:[Int:IntDicVOsVO] = [:]
    var nonExistentTeamSeasons:[Int:IntDicVOsVO] = [:]

    var initialized:Bool = false

    func reset() {
        seasonVOs.removeAll()
        seasonGroupVOs.removeAll()
        listingVOs.removeAll()
        teamVOs.removeAll()
        listingSeasonVOs.removeAll()
        teamListingVOs.removeAll()
        fixtureVOs.removeAll()
        activeFixtureVOs.removeAll()
        parkedFixtureVOs.removeAll()
        adjustTeamVOs.removeAll()
        placeEventVOs.removeAll()
        exemptionTeamVOs.removeAll()
        eventScenarioVOs.removeAll()
        listingScenarioVOs.removeAll()
        nonExistentSeasonRefs.removeAll()
        nonExistentListingRefs.removeAll()
        nonExistentTeamRefs.removeAll()
        nonExistentListingSeasons.removeAll()
        nonExistentTeamSeasons.removeAll()
    }

    func getCompetitionVO(_ competitionRef:Int, seasonGroupRef:Int? = nil) -> CompetitionVO? {
        if let competitionVO:CompetitionVO = competitionVOs[competitionRef] {
            if seasonGroupRef == nil {
                return competitionVO
            }
            return competitionVO.seasonGroupVOs![seasonGroupRef!]
        }
        return nil
    }

    func getListingSeasonVO(_ seasonRef:Int, _ listingRef:Int) -> ListingSeasonVO? {
        if let seasonVO:SeasonVO = seasonVOs[seasonRef] {
            if let listingSeasonRef:Int = seasonVO.listingSeasonRefs[listingRef] {
                return listingSeasonVOs[listingSeasonRef]
            }
        }
        return nil
    }

    func getTeamListingVO(_ seasonRef:Int, _ listingRef:Int, _ teamRef:Int) -> TeamListingVO? {
        if let listingSeasonVO:ListingSeasonVO = getListingSeasonVO(seasonRef, listingRef) {
            if let teamListingRef:Int = listingSeasonVO.teamListingRefs[teamRef] {
                return teamListingVOs[teamListingRef]
            }
        }
        return nil
    }

    func getListingScenarioVO(_ seasonRef:Int, _ listingRef:Int, _ teamRef:Int, _ best:Bool) -> ListingScenarioVO? {
        if let teamListingVO:TeamListingVO = getTeamListingVO(seasonRef, listingRef, teamRef) {
            var listingScenarioRef:Int?
            if best {
                listingScenarioRef = teamListingVO.bestListingScenarioRef
            } else {
                listingScenarioRef = teamListingVO.worstListingScenarioRef
            }
            if listingScenarioRef != nil {
                return listingScenarioVOs[listingScenarioRef!]
            }
        }
        return nil
    }

    func getEventScenarioVO(_ seasonRef:Int, _ listingRef:Int, _ teamRef:Int, _ eventRef:Int, _ toHappen:Bool) -> EventScenarioVO? {
        if let eventTeamVO:EventTeamVO = getEventTeamVO(seasonRef, listingRef, teamRef, eventRef) {
            var eventScenarioRef:Int?
            if toHappen {
                eventScenarioRef = eventTeamVO.scenarioToHappenRef
            } else {
                eventScenarioRef = eventTeamVO.scenarioNotHappenRef
            }
            if eventScenarioRef != nil {
                return eventScenarioVOs[eventScenarioRef!]
            }
        }
        return nil
    }

    func getEventListingVO(_ seasonRef:Int, _ listingRef:Int, _ eventRef:Int) -> EventListingVO? {
        if let listingSeasonVO:ListingSeasonVO = getListingSeasonVO(seasonRef, listingRef) {
            return listingSeasonVO.eventListingVOs[eventRef]
        }
        return nil
    }

    func getEventTeamVO(_ seasonRef:Int, _ listingRef:Int, _ teamRef:Int, _ eventRef:Int) -> EventTeamVO? {
        if let teamListingVO:TeamListingVO = getTeamListingVO(seasonRef, listingRef, teamRef) {
            return teamListingVO.eventTeamVOs[eventRef]
        }
        return nil
    }

    func getTeamSeasonVO(_ teamRef:Int, _ seasonRef:Int) -> TeamSeasonVO? {
        if let teamVO:TeamVO = teamVOs[teamRef] {
            return teamVO.teamSeasonVOs[seasonRef]
        }
        return nil
    }
}
