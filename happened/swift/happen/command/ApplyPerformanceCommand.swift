import Foundation

class ApplyPerformanceCommand {

    private var app:App { return App.instance }
    private var balancing:Balancing { return Balancing.instance }
    private var happen: Happen { return Happen.instance }
    private var localization:Localization { return Localization.instance }

    private var calculatorVO:CalculatorVO
    private var listingSeasonVO:ListingSeasonVO
    private var leagueStructureVO:LeagueStructureBalancingVO
    private var competitionBalancingVO:CompetitionBalancingVO

    private var usePreSplitFixtures:Bool = false
    private var gamesAgainst:[Int: IntDicVOsVO]?
    private var maxGames:Int?
    private var error:Bool = false
    private var bestTopPot:[Int:NSNull] = [:]
    private var bestBottomPot:[Int:NSNull] = [:]

    init( calculatorVO:CalculatorVO ) {
        self.calculatorVO = calculatorVO
        let balancing:Balancing = Balancing.instance
        let happen: Happen = Happen.instance
        if let listingSeasonVO:ListingSeasonVO = happen.happenedModel.listingSeasonVOs[calculatorVO.listingSeasonRef] {
            if let leagueStructureVO:LeagueStructureBalancingVO = balancing.balancingHelper.getLeagueStructureVO(listingSeasonVO.seasonRef, listingSeasonVO.listingRef) {
                if let parentCompetitionVO:CompetitionBalancingVO = happen.happenedHelper.getBestCompetitionVO(listingRef: listingSeasonVO.listingRef, seasonRef: listingSeasonVO.seasonRef)?.balancingVO {
                    self.listingSeasonVO = listingSeasonVO
                    self.leagueStructureVO = leagueStructureVO
                    self.competitionBalancingVO = parentCompetitionVO
                    return
                }
            }
        }
        self.listingSeasonVO = ListingSeasonVO( 0, 0, 0 )
        self.leagueStructureVO = LeagueStructureBalancingVO( 0 )
        self.competitionBalancingVO = CompetitionBalancingVO( 0 )
    }

    func command( limitAtSplit:Bool = false ) {

        if self.listingSeasonVO.ref == 0 {
            return // Invalid listing season
        }

        setNonElapsedUnplayed()
        let flagCurrentSplitRequired:Bool = calculateSplit()
        if calculatorVO.splSplitVO.isSplSplit && limitAtSplit {
            calculatorVO.date = calculatorVO.splSplitVO.firstSplitFixtureDate != nil ? Date(timeIntervalSince1970: calculatorVO.splSplitVO.firstSplitFixtureDate!.timeIntervalSince1970 - 1 ) : nil
            usePreSplitFixtures = true
        }

        applyCurrent()
        applyHeadToHead()
        sortCurrentTeams()

        if flagCurrentSplitRequired {
            let allFixtureVOs:[Int:FixtureVO] = calculatorVO.fixtureVOs
            applyCurrentSplit(allFixtureVOs)

            applyCurrent()
            applyHeadToHead()
            sortCurrentTeams()
        }
        flagPlaceEvents()
        calculatorVO.calculated = true
    }

    private func resetSplit() {
        usePreSplitFixtures = false
        gamesAgainst = nil
        maxGames = nil
    }

    private func setNonElapsedUnplayed() {
        for fixtureVO:FixtureVO in calculatorVO.fixtureVOs.values {
            if( !elapsedFixture( fixtureVO ) ) {
                fixtureVO.resultVO.played = false
                fixtureVO.resultVO.homeGoals = 0
                fixtureVO.resultVO.awayGoals = 0
            }
        }
    }

    private func sortCurrentTeams( excludeHeadToHead:Bool = false ) {
        let teamListingVOs:[Int:TeamListingVO] = calculatorVO.teamListingVOs
        let thisSortKeys:[String] = sortKeys( excludeHeadToHead:excludeHeadToHead )
        calculatorVO.orderedTeamListingVOs = sortTeams(teamListingVOs, thisSortKeys)
    }

    private func sortTeams( _ teamListingVOs:[Int:TeamListingVO], _ thisSortKeys:[String], miniLeague:Bool = false ) -> [TeamListingVO] {
        var usedSortKeys:[String] = app.arrayHelper.cloneStrArr(thisSortKeys)

        usedSortKeys.append(BalancingConstant.NAME)

        var sortedTeamListingVOs:[TeamListingVO] = [TeamListingVO](teamListingVOs.values)
        sortedTeamListingVOs.sort {
            let aPerformanceVO = !miniLeague ? $0.performanceVO : $0.miniLeaguePerformanceVO
            let bPerformanceVO = !miniLeague ? $1.performanceVO : $1.miniLeaguePerformanceVO
            for key:String in thisSortKeys {
                var val:Int = 0
                switch( key ){
                case BalancingConstant.SPLIT_POT_INDEX:
                    val = $1.splitPotIndex - $0.splitPotIndex
                    break

                case BalancingConstant.POINTS:
                    val = aPerformanceVO.points - bPerformanceVO.points
                    break

                case BalancingConstant.GOAL_DIFFERENCE:
                    val = aPerformanceVO.goalDifference - bPerformanceVO.goalDifference
                    break

                case BalancingConstant.GOALS_SCORED:
                    val = aPerformanceVO.goalsFor - bPerformanceVO.goalsFor
                    break

                case BalancingConstant.AWAY_GOALS:
                    val = aPerformanceVO.awayGoalsScored - bPerformanceVO.awayGoalsScored
                    break

                case BalancingConstant.MATCHES_WON:
                    val = aPerformanceVO.gamesWon - bPerformanceVO.gamesWon
                    break

                case BalancingConstant.AWAY_WINS:
                    val = aPerformanceVO.awayWins - bPerformanceVO.awayWins
                    break

                case BalancingConstant.HEAD_TO_HEAD_PT_GD,
                     BalancingConstant.HEAD_TO_HEAD_PT_GD_GS,
                     BalancingConstant.HEAD_TO_HEAD_PT_GD_AG,
                     BalancingConstant.HEAD_TO_HEAD_PT_GD_GS_AG:
                    val = bPerformanceVO.miniLeagueRank - aPerformanceVO.miniLeagueRank
                    break

                case BalancingConstant.GLOBAL_RANK:
                    val = $1.globalRank - $0.globalRank
                    break

                case BalancingConstant.NAME:
                    let aTeamName = localization.localizationHelper.getSortingTeamName(seasonRef: $0.seasonRef, teamRef: $0.teamRef, useLocalizedFirst: true)
                    let bTeamName = localization.localizationHelper.getSortingTeamName(seasonRef: $1.seasonRef, teamRef: $1.teamRef, useLocalizedFirst: true)
                    val = bTeamName.lowercased() > aTeamName.lowercased() ?  1 : ( bTeamName.lowercased() < aTeamName.lowercased() ? -1 : bTeamName > aTeamName ?  1 : ( bTeamName < aTeamName ? -1 : 0 ) )
                    break
                default:
                    break
                }
                if val > 0 {
                    return true
                }
                if val < 0 {
                    return false
                }
            }

            return false
        }

        var i:Int = -1
        for teamListingVO:TeamListingVO in sortedTeamListingVOs {
            i += 1
            let nextRank:Int = i + 1
            let performanceVO:PerformanceVO = !miniLeague ? teamListingVO.performanceVO : teamListingVO.miniLeaguePerformanceVO
            performanceVO.rankHigh = nextRank
            performanceVO.rankLow = nextRank
            if i > 0 {
                var compareTeamListingVO:TeamListingVO = sortedTeamListingVOs[ i - 1 ]
                let comparePerformanceVO:PerformanceVO = !miniLeague ? compareTeamListingVO.performanceVO : compareTeamListingVO.miniLeaguePerformanceVO
                let match:Bool = isMatch(thisSortKeys: thisSortKeys, teamListingVO: teamListingVO, compareTeamListingVO: compareTeamListingVO, miniLeague: miniLeague)
                if match {
                    performanceVO.rankHigh = comparePerformanceVO.rankHigh
                    for j:Int in (0...i - 1).reversed() {
                        compareTeamListingVO = sortedTeamListingVOs[ j ]
                        if comparePerformanceVO.rankHigh == performanceVO.rankHigh
                        {
                            comparePerformanceVO.rankLow = performanceVO.rankLow
                        } else {
                            break
                        }
                    }
                }
            }
        }

        return sortedTeamListingVOs
    }

    private func isMatch( thisSortKeys:[String], teamListingVO:TeamListingVO, compareTeamListingVO:TeamListingVO, miniLeague:Bool = false ) -> Bool {
        let performanceVO:PerformanceVO = !miniLeague ? teamListingVO.performanceVO : teamListingVO.miniLeaguePerformanceVO
        let comparePerformanceVO:PerformanceVO = !miniLeague ? compareTeamListingVO.performanceVO : compareTeamListingVO.miniLeaguePerformanceVO

        for sortKey:String in thisSortKeys {
            var match = true
            switch sortKey {
                case BalancingConstant.SPLIT_POT_INDEX:
                    match = teamListingVO.splitPotIndex == compareTeamListingVO.splitPotIndex
                    break

                case BalancingConstant.POINTS:
                    match = performanceVO.points == comparePerformanceVO.points
                    break

                case BalancingConstant.GOAL_DIFFERENCE:
                    match = performanceVO.goalDifference == comparePerformanceVO.goalDifference
                    break

                case BalancingConstant.GOALS_SCORED:
                    match = performanceVO.goalsFor == comparePerformanceVO.goalsFor
                    break

                case BalancingConstant.AWAY_GOALS:
                    match = performanceVO.awayGoalsScored == comparePerformanceVO.awayGoalsScored
                    break

                case BalancingConstant.MATCHES_WON:
                    match = performanceVO.gamesWon == comparePerformanceVO.gamesWon
                    break

                case BalancingConstant.AWAY_WINS:
                    match = performanceVO.awayWins == comparePerformanceVO.awayWins
                    break

                case BalancingConstant.HEAD_TO_HEAD_PT_GD,
                     BalancingConstant.HEAD_TO_HEAD_PT_GD_GS,
                     BalancingConstant.HEAD_TO_HEAD_PT_GD_AG,
                     BalancingConstant.HEAD_TO_HEAD_PT_GD_GS_AG:
                    match = performanceVO.miniLeagueRank == comparePerformanceVO.miniLeagueRank
                    break

                case BalancingConstant.GLOBAL_RANK:
                    match = teamListingVO.globalRank == compareTeamListingVO.globalRank
                    break

                case BalancingConstant.NAME:
                    match = false
                    break
                default:
                    break
            }
            if !match {
                return false
            }
        }
        return true
    }

    private func sortKeys( excludeHeadToHead:Bool = false ) -> [String] {
        var thisSortKeys:[String] = [BalancingConstant.SPLIT_POT_INDEX, BalancingConstant.POINTS]
        let thisListingDecidedOrder = filterListingDecidedOrder()
        for listingDecidedType in thisListingDecidedOrder {
            if excludeHeadToHead && isHeadToHeadType(listingDecidedType) {
                break
            }
            thisSortKeys.append(listingDecidedType)
        }
        return thisSortKeys
    }

    private func isHeadToHeadType( _ listingDecidedType:String ) -> Bool {
        return sortKeysForHeadToHeadType( listingDecidedType ) != nil
    }

    private func hasHeadToHead() -> Bool {
        return sortKeysForHeadToHead() != nil
    }

    private func sortKeysForHeadToHead() -> [String]? {
        let thisListingDecidedOrder = filterListingDecidedOrder()
        for listingDecidedType in thisListingDecidedOrder {
            if let keys:[String] = sortKeysForHeadToHeadType(listingDecidedType) {
                return keys
            }
        }
        return nil
    }

    private func sortKeysForHeadToHeadType( _ headToHeadType:String ) -> [String]? {

        switch headToHeadType {
            case BalancingConstant.HEAD_TO_HEAD_PT_GD:
                return [
                        BalancingConstant.SPLIT_POT_INDEX,
                        BalancingConstant.POINTS,
                        BalancingConstant.GOAL_DIFFERENCE
                ]

            case BalancingConstant.HEAD_TO_HEAD_PT_GD_GS:
                return [
                        BalancingConstant.SPLIT_POT_INDEX,
                        BalancingConstant.POINTS,
                        BalancingConstant.GOAL_DIFFERENCE,
                        BalancingConstant.GOALS_SCORED
                ]

            case BalancingConstant.HEAD_TO_HEAD_PT_GD_AG:
                return [
                        BalancingConstant.SPLIT_POT_INDEX,
                        BalancingConstant.POINTS,
                        BalancingConstant.GOAL_DIFFERENCE,
                        BalancingConstant.AWAY_GOALS
                ]

            case BalancingConstant.HEAD_TO_HEAD_PT_GD_GS_AG:
                return [
                        BalancingConstant.SPLIT_POT_INDEX,
                        BalancingConstant.POINTS,
                        BalancingConstant.GOAL_DIFFERENCE,
                        BalancingConstant.GOALS_SCORED,
                        BalancingConstant.AWAY_GOALS
                ]

            default:
                break
        }
        return nil
    }

    private func filterListingDecidedOrder() -> [String] {

        var filteredListingDecidedOrder:[String] = []
        for listingDecidedType in leagueStructureVO.listingDecidedOrder! {
            if listingDecidedType == BalancingConstant.GLOBAL_RANK {
                if calculatorVO.remainingFixtureRefs.count > 0 && competitionBalancingVO.applyGlobalRankAtEnd! {
                    continue
                }
            }
            filteredListingDecidedOrder.append(listingDecidedType)
        }
        return filteredListingDecidedOrder
    }

    private func applyHeadToHead() {
        if !hasHeadToHead() {
            return
        }
        for teamListingVO:TeamListingVO in calculatorVO.teamListingVOs.values {
            teamListingVO.miniLeaguePerformanceVO.reset()
        }

        sortCurrentTeams(excludeHeadToHead: true)
        var miniLeagues:[Int: IntDicVOsVO] = [:]
        for teamListingVO:TeamListingVO in calculatorVO.teamListingVOs.values {
            if miniLeagues[ teamListingVO.performanceVO.rankHigh ] == nil {
                miniLeagues[ teamListingVO.performanceVO.rankHigh ] = IntDicVOsVO()
            }
            miniLeagues[ teamListingVO.performanceVO.rankHigh ]!.vos[ teamListingVO.teamRef ] = teamListingVO
        }
        if miniLeagues.count < 2 {
            return
        }

        for miniLeagueTeamListingVOs: IntDicVOsVO in miniLeagues.values {
            if miniLeagueTeamListingVOs.vos.count == 1 {
                continue
            }
            applyMiniLeague(miniLeagueTeamListingVOs.vos as! [Int:TeamListingVO])
        }
    }

    private func applyMiniLeague( _ teamListingVOs:[Int:TeamListingVO] ) {

        for teamRef:Int in teamListingVOs.keys {
            applyAdjustment(teamRef, miniLeagueTeamListingVOs: teamListingVOs)
        }

        for fixtureVO:FixtureVO in calculatorVO.fixtureVOs.values {
            if teamListingVOs[ fixtureVO.homeRef ] != nil && teamListingVOs[ fixtureVO.awayRef ] != nil {
                addToPerformanceResult(fixtureVO.ref, miniLeague: true)
            }
        }
        let thisSortKeys:[String] = sortKeysForHeadToHead()!
        _ = sortTeams(teamListingVOs, thisSortKeys, miniLeague: true)
        for teamListingVO:TeamListingVO in calculatorVO.teamListingVOs.values {
            teamListingVO.performanceVO.miniLeagueRank = teamListingVO.miniLeaguePerformanceVO.rankHigh
        }
    }

    private func addToPerformanceResult( _ fixtureRef:Int, miniLeague:Bool = false ) {
        let fixtureVO:FixtureVO? = calculatorVO.activeFixtureVOs[ fixtureRef ]
        let factor:Int = 1 // remnant from server. if factor were -1 I would be subtracting. client side however im only adding
        if fixtureVO == nil {
            return
        }

        let homeTeamVO:TeamListingVO? = calculatorVO.teamListingVOs[ fixtureVO!.homeRef ]
        let awayTeamVO:TeamListingVO? = calculatorVO.teamListingVOs[ fixtureVO!.awayRef ]

        if homeTeamVO == nil || awayTeamVO == nil {
            return
        }

        let homePerformanceVO:PerformanceVO = !miniLeague ? homeTeamVO!.performanceVO : homeTeamVO!.miniLeaguePerformanceVO
        let awayPerformanceVO:PerformanceVO = !miniLeague ? awayTeamVO!.performanceVO : awayTeamVO!.miniLeaguePerformanceVO

        homePerformanceVO.played = homePerformanceVO.played + 1 * factor
        awayPerformanceVO.played = awayPerformanceVO.played + 1 * factor

        if fixtureVO!.resultVO.homeGoals > fixtureVO!.resultVO.awayGoals {
            homePerformanceVO.points = homePerformanceVO.points + leagueStructureVO.pointsPerWin! * factor
            awayPerformanceVO.points = awayPerformanceVO.points + leagueStructureVO.pointsPerLoss! * factor

            homePerformanceVO.gamesWon = homePerformanceVO.gamesWon + 1 * factor
            awayPerformanceVO.gamesLost = awayPerformanceVO.gamesLost + 1 * factor
        } else if fixtureVO!.resultVO.homeGoals < fixtureVO!.resultVO.awayGoals {
            homePerformanceVO.points = homePerformanceVO.points + leagueStructureVO.pointsPerLoss! * factor
            awayPerformanceVO.points = awayPerformanceVO.points + leagueStructureVO.pointsPerWin! * factor

            homePerformanceVO.gamesLost = homePerformanceVO.gamesLost + 1 * factor
            awayPerformanceVO.gamesWon = awayPerformanceVO.gamesWon + 1 * factor

            awayPerformanceVO.awayWins = awayPerformanceVO.awayWins + 1 * factor
        } else {
            homePerformanceVO.points = homePerformanceVO.points + leagueStructureVO.pointsPerDraw! * factor
            awayPerformanceVO.points = awayPerformanceVO.points + leagueStructureVO.pointsPerDraw! * factor

            homePerformanceVO.gamesDrawn = homePerformanceVO.gamesDrawn + 1 * factor
            awayPerformanceVO.gamesDrawn = awayPerformanceVO.gamesDrawn + 1 * factor
        }

        homePerformanceVO.goalsFor = homePerformanceVO.goalsFor + fixtureVO!.resultVO.homeGoals * factor
        awayPerformanceVO.goalsFor = awayPerformanceVO.goalsFor + fixtureVO!.resultVO.awayGoals * factor

        homePerformanceVO.goalsAgainst = homePerformanceVO.goalsAgainst + fixtureVO!.resultVO.awayGoals * factor
        awayPerformanceVO.goalsAgainst = awayPerformanceVO.goalsAgainst + fixtureVO!.resultVO.homeGoals * factor

        awayPerformanceVO.awayGoalsScored = awayPerformanceVO.awayGoalsScored + fixtureVO!.resultVO.awayGoals * factor
    }

    private func calculateSplit() -> Bool {

        determineBasicSpiltInfo()

        removeSplitFlags()

        if !calculatorVO.splSplitVO.isSplSplit {
            return false
        }
        if !calculatorVO.splSplitVO.splitOccurred {
            return true
        }

        var allFixtureVOs:[Int:FixtureVO] = calculatorVO.fixtureVOs

        var preSplitFixtureVOs:[Int:FixtureVO] = [:]

        for ref in calculatorVO.splSplitVO.preSplitFixtureRefs.keys {
            preSplitFixtureVOs[ ref ] = allFixtureVOs[ref]!
        }

        calculatorVO.activeFixtureVOs = preSplitFixtureVOs
        calculatorVO.parkedFixtureVOs = [:]

        sortCurrent()
        applyCurrentSplit( allFixtureVOs )

        for fixtureVO:FixtureVO in allFixtureVOs.values {
            if !fixtureVO.resultVO.ignoreFixture {
                calculatorVO.activeFixtureVOs[ fixtureVO.ref ] = fixtureVO
            } else {
                calculatorVO.parkedFixtureVOs[ fixtureVO.ref ] = fixtureVO
                fixtureVO.resultVO.played = false
                fixtureVO.resultVO.homeGoals = 0
                fixtureVO.resultVO.awayGoals = 0
            }
        }
        return false
    }

    private func sortCurrent() {

        applyCurrent()

        applyHeadToHead()

        sortCurrentTeams()
    }

    private func applyCurrent() {
        calculatorVO.playedFixtureRefs = [:]
        calculatorVO.remainingFixtureRefs = [:]

        for teamListingVO:TeamListingVO in calculatorVO.teamListingVOs.values {
            teamListingVO.performanceVO.reset()
            applyAdjustments(teamListingVO.teamRef)
        }

        for fixtureVO:FixtureVO in calculatorVO.activeFixtureVOs.values {
            addToPerformance(fixtureVO.ref)
        }

        for teamListingVO:TeamListingVO in calculatorVO.teamListingVOs.values {
            teamListingVO.miniLeaguePerformanceVO.reset()
            if !calculatorVO.splSplitVO.splitOccurred {
                teamListingVO.splitPotIndex = 0
            }
        }
    }

    private func addToPerformance( _ fixtureRef:Int ) {
        let fixtureVO:FixtureVO? = calculatorVO.activeFixtureVOs[ fixtureRef ]

        if fixtureVO == nil {
            return
        }

        let homeTeamVO:TeamListingVO? = calculatorVO.teamListingVOs[ fixtureVO!.homeRef ]
        let awayTeamVO:TeamListingVO? = calculatorVO.teamListingVOs[ fixtureVO!.awayRef ]

        if homeTeamVO == nil || awayTeamVO == nil {
            return
        }

        var playedFixtureRefs:[Int:NSNull]  = calculatorVO.playedFixtureRefs
        var remainingFixtureRefs:[Int:NSNull]  = calculatorVO.remainingFixtureRefs

        let played:Bool = fixtureVO!.resultVO.played && elapsedFixture( fixtureVO! )
        if !played {

            playedFixtureRefs.removeValue(forKey: fixtureVO!.ref)
            remainingFixtureRefs[ fixtureVO!.ref ] = NSNull()

            homeTeamVO!.performanceVO.remaining = homeTeamVO!.performanceVO.remaining + 1
            awayTeamVO!.performanceVO.remaining = awayTeamVO!.performanceVO.remaining + 1
        }
        else
        {
            playedFixtureRefs[ fixtureVO!.ref ] = NSNull()
            remainingFixtureRefs.removeValue(forKey: fixtureVO!.ref)

            addToPerformanceResult( fixtureRef )
        }

        calculatorVO.playedFixtureRefs = playedFixtureRefs
        calculatorVO.remainingFixtureRefs = remainingFixtureRefs
    }

    private func applyAdjustments( _ teamRef:Int, miniLeagueTeamListingVOs:[Int:TeamListingVO]?=nil ) {
        let teamListingVO:TeamListingVO? = calculatorVO.teamListingVOs[ teamRef ]
        if teamListingVO == nil {
            return
        }

        for adjustTeamRef:Int in teamListingVO!.adjustTeamRefs.keys {
            applyAdjustment( adjustTeamRef, miniLeagueTeamListingVOs:miniLeagueTeamListingVOs )
        }
    }

    private func applyAdjustment( _ adjustTeamRef:Int, miniLeagueTeamListingVOs:[Int:TeamListingVO]?=nil ) {
        let adjustTeamVO:AdjustTeamVO? = happen.happenedModel.adjustTeamVOs[ adjustTeamRef ]
        if adjustTeamVO == nil {
            return
        }

        if miniLeagueTeamListingVOs != nil {
            if adjustTeamVO!.adjustAgainstTeamRef == nil || miniLeagueTeamListingVOs![adjustTeamVO!.adjustAgainstTeamRef!] == nil {
                return
            }
        }

        if !elapsed( adjustTeamVO!.appliedDate ) {
            return
        }

        let factor:Int = 1

        let teamListingVO:TeamListingVO? = calculatorVO.teamListingVOs[ adjustTeamVO!.teamRef ]
        if teamListingVO == nil {
            return
        }
        teamListingVO!.performanceVO.points = teamListingVO!.performanceVO.points + adjustTeamVO!.pointsOffset * factor
        teamListingVO!.performanceVO.goalsFor = teamListingVO!.performanceVO.goalsFor + adjustTeamVO!.goalForOffset * factor
        teamListingVO!.performanceVO.goalsAgainst = teamListingVO!.performanceVO.goalsAgainst + adjustTeamVO!.goalAgainstOffset * factor
        teamListingVO!.performanceVO.awayGoalsScored = teamListingVO!.performanceVO.awayGoalsScored + adjustTeamVO!.awayGoalOffset * factor
        teamListingVO!.performanceVO.gamesWon = teamListingVO!.performanceVO.gamesWon + adjustTeamVO!.winOffset * factor
        teamListingVO!.performanceVO.gamesDrawn = teamListingVO!.performanceVO.gamesDrawn + adjustTeamVO!.drawOffset * factor
        teamListingVO!.performanceVO.gamesLost = teamListingVO!.performanceVO.gamesLost + adjustTeamVO!.lossOffset * factor
        teamListingVO!.performanceVO.awayWins = teamListingVO!.performanceVO.awayWins + adjustTeamVO!.awayWinOffset * factor

    }

    private func elapsedFixture( _ fixtureVO:FixtureVO) -> Bool {
        if usePreSplitFixtures {
            return calculatorVO.splSplitVO.preSplitFixtureRefs[ fixtureVO.ref ] != nil
        }
        return elapsed(fixtureVO.sortingDate)
    }

    private func elapsed( _ date:Date?) -> Bool {
        if calculatorVO.date == nil {
            return true//there is no future. everything we can possible know about has elapsed
        }
        if date == nil {
            return false
        }
        return date! <= calculatorVO.date!
    }

    private func determineBasicSpiltInfo() {
        let splitVO:SplSplitVO = calculatorVO.splSplitVO
        if splitVO.includesBasicSplitInfo {
            return
        }
        resetSplit()
        splitVO.reset()
        splitVO.includesBasicSplitInfo = true
        determineHasSplit()
        if !splitVO.isSplSplit {
            return
        }

        determineRoundTypes()

        if splitVO.invalidData {
            return
        }

        splitVO.splitOccurred = isSplitOccurred()
    }

    private func determineHasSplit() {
        let splitVO:SplSplitVO = calculatorVO.splSplitVO
        //js checked for nil split here. I don't believe this is possible
        splitVO.reset()

        splitVO.noOfTopSplitPlaces = getNoOfTopSplitPlaces()
    }

    private func getNoOfTopSplitPlaces() -> Int {

        var noOfTopSplitPlaces = 0
        for eventListingVO:EventListingVO in listingSeasonVO.eventListingVOs.values {
            let eventBalancingVO:EventBalancingVO = balancing.balancingModel.getEventVO(eventListingVO.eventRef, seasonRef: eventListingVO.seasonRef)!
            if eventBalancingVO.splSplit! {
                let placeEventVOs:[Int:PlaceEventVO] = happen.happenedHelper.getPlaceEventVOs(eventListingVO.placeEventRefs, skipRemoved: true)
                for placeEventVO:PlaceEventVO in placeEventVOs.values {
                    noOfTopSplitPlaces += placeEventVO.placesForEvent
                }
            }
        }


        return noOfTopSplitPlaces
    }

    private func determineRoundTypes() {
        var preSplitRoundFixtureRefs:[Int:NSNull] = [:]
        var splitRoundFixtureRefs:[Int:NSNull] = [:]

        let gamesAgainst:[Int: IntDicVOsVO] = getGamesAgainst()
        let maxGames:Int = getMaxGames()

        for intDicVOsVO:IntDicVOsVO in gamesAgainst.values {
            let opponents:[Int:ArrVOsVO] = intDicVOsVO.vos as! [Int:ArrVOsVO]

            for arrVOsVO:ArrVOsVO in opponents.values {
                let orderedFixtureVOs:[FixtureVO] = arrVOsVO.vos as! [FixtureVO]
                var roundNo = -1
                for fixtureVO:FixtureVO in orderedFixtureVOs {
                    if !elapsedFixture( fixtureVO ) {
                        fixtureVO.resultVO.played = false
                        fixtureVO.resultVO.homeGoals = 0
                        fixtureVO.resultVO.awayGoals = 0
                    }

                    roundNo += 1
                    if roundNo == maxGames {
                        calculatorVO.splSplitVO.invalidData = true
                        return
                    }

                    if roundNo < maxGames - 1 {
                        preSplitRoundFixtureRefs[ fixtureVO.ref ] = NSNull()
                    } else if roundNo == maxGames - 1 {
                        splitRoundFixtureRefs[ fixtureVO.ref ] = NSNull()
                    }
                }
            }
        }

        calculatorVO.splSplitVO.preSplitFixtureRefs = preSplitRoundFixtureRefs
        calculatorVO.splSplitVO.splitRoundFixtureRefs = splitRoundFixtureRefs
    }

    private func getGamesAgainst() -> [Int: IntDicVOsVO] {
        if gamesAgainst == nil {
            let orderedFixtureVOs:[FixtureVO] = getOrderedFixtureVOs()
            gamesAgainst = orderedFixturesAgainst( orderedFixtureVOs )
        }
        return gamesAgainst!
    }

    private func getOrderedFixtureVOs( activeOnly:Bool = false ) -> [FixtureVO] {
        let fixtureVOs:[Int:FixtureVO] = !activeOnly ? calculatorVO.fixtureVOs : calculatorVO.activeFixtureVOs
        return happen.happenedHelper.sortFixtureVOs( dicFixtureVOs:fixtureVOs )
    }

    private func orderedFixturesAgainst( _ orderedFixtureVOs:[FixtureVO] ) -> [Int: IntDicVOsVO] {
        var gamesAgainst:[Int: IntDicVOsVO] = createAgainstArray()

        for fixtureVO:FixtureVO in orderedFixtureVOs {
            (gamesAgainst[ fixtureVO.homeRef ]!.vos[ fixtureVO.awayRef ] as! ArrVOsVO).vos.append(fixtureVO)
            (gamesAgainst[ fixtureVO.awayRef ]!.vos[ fixtureVO.homeRef ] as! ArrVOsVO).vos.append( fixtureVO )
        }

        return gamesAgainst
    }

    private func createAgainstArray() -> [Int: IntDicVOsVO] {
        var against:[Int: IntDicVOsVO] = [:]

        for teamRef:Int in calculatorVO.teamListingVOs.keys {
            if against[ teamRef ] == nil {
                against[ teamRef ] = IntDicVOsVO()
            }
            for oppRef:Int in calculatorVO.teamListingVOs.keys {
                if oppRef == teamRef {
                    continue
                }
                against[ teamRef ]!.vos[ oppRef ] = ArrVOsVO()
            }
        }
        return against
    }

    private func getMaxGames() -> Int {
        if maxGames == nil {
            maxGames = getMinimumGamesAgainst( leagueStructureVO.gamesAgainstType! )
        }
        return maxGames!
    }

    private func getMinimumGamesAgainst( _ gamesAgainstType:String ) -> Int {

        switch gamesAgainstType {
            case BalancingConstant.VAL_1_NEUTRAL_LEAGUE_AGAINST_STRUCTURE_VALUE,
                 BalancingConstant.VAL_1_NEUTRAL_KO_AGAINST_STRUCTURE_VALUE,
                 BalancingConstant.VAL_1_HOME_OR_AWAY_AND_1_REPLAY_KO_AGAINST_STRUCTURE_VALUE:
                return 1

            case BalancingConstant.VAL_1_HOME_1_AWAY_LEAGUE_AGAINST_STRUCTURE_VALUE,
                 BalancingConstant.VAL_1_HOME_1_AWAY_KO_AGAINST_STRUCTURE_VALUE:
                return 2

            case BalancingConstant.VAL_2_HOME_2_AWAY_LEAGUE_AGAINST_STRUCTURE_VALUE:
                return 4

            default:
                return 0
        }
        
    }

    private func isSplitOccurred() -> Bool {

        if !calculatorVO.splSplitVO.isSplSplit {
            return false
        }

        for fixtureRef in calculatorVO.splSplitVO.splitRoundFixtureRefs.keys {
            let fixtureVO:FixtureVO = calculatorVO.fixtureVOs[fixtureRef]!
            if fixtureVO.resultVO.played {
                return true
            }
        }

        for fixtureRef in calculatorVO.splSplitVO.preSplitFixtureRefs.keys {
            let fixtureVO:FixtureVO = calculatorVO.fixtureVOs[fixtureRef]!
            if !fixtureVO.resultVO.played {
                return false
            }
        }

        return true
    }

    private func removeSplitFlags() {

        for fixtureVO:FixtureVO in calculatorVO.fixtureVOs.values {
            fixtureVO.resultVO.ignoreFixture = false
        }
        calculatorVO.activeFixtureVOs = calculatorVO.fixtureVOs
        calculatorVO.parkedFixtureVOs = [:]

        removeTeamSplitFlags()
    }

    private func removeTeamSplitFlags() {
        for teamListingVO:TeamListingVO in calculatorVO.teamListingVOs.values {
            teamListingVO.splitPotIndex = 0
        }
    }

    private func applyCurrentSplit( _ allFixtureVOs:[Int:FixtureVO] ) {
        calculatorVO.activeFixtureVOs = allFixtureVOs
        calculatorVO.parkedFixtureVOs = [:]
        flagCurrentSplit()
        calculatorVO.activeFixtureVOs = [:]
        calculatorVO.parkedFixtureVOs = [:]

        for fixtureVO:FixtureVO in allFixtureVOs.values {
            if !fixtureVO.resultVO.ignoreFixture {
                calculatorVO.activeFixtureVOs[ fixtureVO.ref ] = fixtureVO
            } else {
                calculatorVO.parkedFixtureVOs[ fixtureVO.ref ] = fixtureVO
                fixtureVO.resultVO.played = false
                fixtureVO.resultVO.homeGoals = 0
                fixtureVO.resultVO.awayGoals = 0
            }
        }
    }

    private func flagCurrentSplit() {
        var topSplitTeamRefs:[Int:NSNull] = [:]

        for teamListingVO:TeamListingVO in calculatorVO.orderedTeamListingVOs {
            if topSplitTeamRefs.count == calculatorVO.splSplitVO.noOfTopSplitPlaces {
                break
            }
            topSplitTeamRefs[ teamListingVO.teamRef ] = NSNull()
        }

        if calculatorVO.splSplitVO.splitOccurred && calculatorVO.splSplitVO.invalidData {
            return
        }
        flagSplit( topSplitTeamRefs )
    }

    private func flagSplit( _ topSplitTeamRefs:[Int:NSNull] ) {

        var bottomSplitTeamRefs:[Int:NSNull] = [:]

        for teamRef:Int in calculatorVO.teamListingVOs.keys {
            if topSplitTeamRefs[ teamRef ] == nil {
                bottomSplitTeamRefs[ teamRef ] = NSNull()
            }
        }

        let orderedFixtureVOs:[FixtureVO] = getOrderedFixtureVOs()
        let gamesAgainst:[Int: IntDicVOsVO] = orderedFixturesAgainst( orderedFixtureVOs )
        let maxGames:Int = getMinimumGamesAgainst( leagueStructureVO.gamesAgainstType! )

        var ignoreFixtureRefs:[Int:NSNull] = [:]
        var splitRoundFixtureRefs:[Int:NSNull] = [:]
        var preSplitRoundFixtureRefs:[Int:NSNull] = [:]

        var firstSplitFixtureDate:Date? = nil


        for intDicVOsVO:IntDicVOsVO in gamesAgainst.values {
            let opponents: [Int: ArrVOsVO] = intDicVOsVO.vos as! [Int: ArrVOsVO]

            for arrVOsVO: ArrVOsVO in opponents.values {
                let thisOrderedFixtureVOs: [FixtureVO] = arrVOsVO.vos as! [FixtureVO]
                var roundNo:Int = -1
                for fixtureVO:FixtureVO in thisOrderedFixtureVOs {
                    roundNo += 1
                    if roundNo == maxGames - 1 {
                        if
                          ( topSplitTeamRefs[ fixtureVO.homeRef ] != nil && bottomSplitTeamRefs[ fixtureVO.awayRef ] != nil ) ||
                          ( bottomSplitTeamRefs[ fixtureVO.homeRef ] != nil && topSplitTeamRefs[ fixtureVO.awayRef ] != nil ) {
                            ignoreFixtureRefs[ fixtureVO.ref ] = NSNull()
                        } else {
                            splitRoundFixtureRefs[ fixtureVO.ref ] = NSNull()
                        }
                        if fixtureVO.playDate != nil && (firstSplitFixtureDate == nil || fixtureVO.playDate! < firstSplitFixtureDate! ) {
                            firstSplitFixtureDate = fixtureVO.playDate
                        }
                    } else if roundNo < maxGames - 1 {
                        preSplitRoundFixtureRefs[ fixtureVO.ref ] = NSNull()
                    }
                }
            }
        }

        calculatorVO.splSplitVO.firstSplitFixtureDate = firstSplitFixtureDate
        calculatorVO.splSplitVO.topSplitTeamRefs = topSplitTeamRefs
        calculatorVO.splSplitVO.bottomSplitTeamRefs = bottomSplitTeamRefs
        calculatorVO.splSplitVO.ignoreFixtureRefs = ignoreFixtureRefs
        calculatorVO.splSplitVO.preSplitFixtureRefs = preSplitRoundFixtureRefs
        calculatorVO.splSplitVO.splitRoundFixtureRefs = splitRoundFixtureRefs

        flagFixtureVOs()
        flagTeamListingVOs()
    }

    private func flagFixtureVOs() {
        var ignoreFixtureRefs:[Int:NSNull] = calculatorVO.splSplitVO.ignoreFixtureRefs

        for fixtureVO:FixtureVO in calculatorVO.fixtureVOs.values {
            fixtureVO.resultVO.ignoreFixture = ignoreFixtureRefs[ fixtureVO.ref ] != nil
        }
    }

    private func flagTeamListingVOs() {

        for teamListingVO in calculatorVO.teamListingVOs.values {
            if calculatorVO.splSplitVO.splitOccurred {
                if calculatorVO.splSplitVO.bottomSplitTeamRefs[ teamListingVO.teamRef ] != nil {
                    teamListingVO.splitPotIndex = 1
                } else {
                    teamListingVO.splitPotIndex = 0
                }
            } else {
                teamListingVO.splitPotIndex = 0
            }
        }
    }

    private func flagPlaceEvents() {

        var sortedPlaceEventVOs:[Int:IntDicVOsVO] = [:]
        var sortedExemptionVOs:[Int:IntDicVOsVO] = [:]

        var completedMaximumEventVOs:IntDicVOsVO = IntDicVOsVO()
        var completedMinimumEventVOs:IntDicVOsVO = IntDicVOsVO()

        class CompletedEventVO {

            var allToHereRefs: [Int:NSNull]
            var thisOnlyRefs: [Int:NSNull]

            init( allToHereRefs: [Int:NSNull], thisOnlyRefs: [Int:NSNull] ) {
                self.allToHereRefs = allToHereRefs
                self.thisOnlyRefs = thisOnlyRefs
            }
        }

        func addPlacesFor( _ eventRef:Int, isMaximum:Bool ) -> [Int:NSNull] {
            let completedEvents:IntDicVOsVO = isMaximum ? completedMaximumEventVOs : completedMinimumEventVOs
            if completedEvents.vos[ eventRef ] != nil {
                return ( completedEvents.vos[ eventRef ]! as! CompletedEventVO).allToHereRefs
            }
            let eventBalancingVO:EventBalancingVO? = balancing.balancingModel.getEventVO(eventRef, seasonRef: listingSeasonVO.seasonRef)

            if( eventBalancingVO == nil ) {
                return [:]
            }
            let previousUsedRefs:[Int:NSNull] = eventBalancingVO!.followsEventRef != nil ? addPlacesFor( eventBalancingVO!.followsEventRef!, isMaximum: isMaximum ) : [:]
            let allToHereRefs:[Int:NSNull] = app.arrayHelper.cloneIntNulDic(previousUsedRefs)
            completedEvents.vos[ eventRef ] = CompletedEventVO(allToHereRefs: allToHereRefs, thisOnlyRefs: [:])

            if sortedPlaceEventVOs[ eventRef ] == nil {
                return (completedEvents.vos[ eventRef ]! as! CompletedEventVO).allToHereRefs
            }
            let thisPlaceEventVOs:[Int:PlaceEventVO] = sortedPlaceEventVOs[ eventRef ]!.vos as! [Int:PlaceEventVO]

            var thisPlaces = 0
            for thisPlaceEventVO:PlaceEventVO in thisPlaceEventVOs.values {

                if !thisPlaceEventVO.permanentPlace && thisPlaceEventVO.removedDate != nil && elapsed( thisPlaceEventVO.removedDate! ) {
                    continue
                }

                if isMaximum || thisPlaceEventVO.permanentPlace || ( thisPlaceEventVO.securedDate != nil && elapsed( thisPlaceEventVO.securedDate! ) ) {
                    thisPlaces += thisPlaceEventVO.placesForEvent
                }
            }

            var orderedTeamListingVOs:[TeamListingVO] = []
            for teamListingVO:TeamListingVO in calculatorVO.orderedTeamListingVOs {
                orderedTeamListingVOs.append(teamListingVO)
            }

            if !eventBalancingVO!.positive! {
                orderedTeamListingVOs.reverse()
            }
            var usedPlaces:Int = 0
            for teamListingVO:TeamListingVO in orderedTeamListingVOs {
                if usedPlaces == thisPlaces {
                    break
                }
                var match:Bool = false
                for checkTeamRef in allToHereRefs.keys {
                    if checkTeamRef == teamListingVO.teamRef {
                        match = true
                        break
                    }
                }
                if !match {
                    if sortedExemptionVOs[ eventRef ] != nil && sortedExemptionVOs[ eventRef ]!.vos[ teamListingVO.teamRef ] != nil {
                        let exemptionVO:ExemptionTeamVO = sortedExemptionVOs[ eventRef ]!.vos[ teamListingVO.teamRef ]! as! ExemptionTeamVO
                        if !exemptionVO.eventReceivedByListing {
                            usedPlaces += 1
                        }
                        continue
                    }
                    (completedEvents.vos[ eventRef ]! as! CompletedEventVO).allToHereRefs[ teamListingVO.teamRef ] = NSNull()
                    (completedEvents.vos[ eventRef ]! as! CompletedEventVO).thisOnlyRefs[ teamListingVO.teamRef ] = NSNull()
                    usedPlaces += 1
                }
            }
            return (completedEvents.vos[ eventRef ]! as! CompletedEventVO).allToHereRefs
        }

        for eventListingVO:EventListingVO in listingSeasonVO.eventListingVOs.values {
            for placeEventRef in eventListingVO.placeEventRefs.keys {
                let placeEventVO:PlaceEventVO? = happen.happenedModel.placeEventVOs[ placeEventRef ]

                if( placeEventVO == nil || ( !placeEventVO!.permanentPlace && ( placeEventVO!.removedDate != nil && elapsed( placeEventVO!.removedDate! ) ) ) )
                {
                    continue
                }

                if sortedPlaceEventVOs[ placeEventVO!.eventRef ] == nil {
                    sortedPlaceEventVOs[ placeEventVO!.eventRef ] = IntDicVOsVO()
                }
                sortedPlaceEventVOs[ placeEventVO!.eventRef ]!.vos[ placeEventRef ] = placeEventVO
            }
        }

        for teamListingVO:TeamListingVO in calculatorVO.teamListingVOs.values {
            for eventTeamVO:EventTeamVO in teamListingVO.eventTeamVOs.values {
                if eventTeamVO.exemptionRef != nil {
                    let exemptionTeamVO:ExemptionTeamVO? = happen.happenedModel.exemptionTeamVOs[ eventTeamVO.exemptionRef! ]
                    if exemptionTeamVO == nil || !elapsed( exemptionTeamVO!.appliedDate ) {
                        continue
                    }

                    if sortedExemptionVOs[ eventTeamVO.eventRef ] == nil{
                        sortedExemptionVOs[ eventTeamVO.eventRef ] = IntDicVOsVO()
                    }
                    sortedExemptionVOs[ eventTeamVO.eventRef ]!.vos[ exemptionTeamVO!.teamRef ] = exemptionTeamVO
                }
            }
        }

        for eventRef in sortedPlaceEventVOs.keys {
            _ = addPlacesFor( eventRef, isMaximum: true )
            _ = addPlacesFor( eventRef, isMaximum: false )
        }

        calculatorVO.maximumPlaceEventRefs = [:]
        calculatorVO.minimumPlaceEventRefs = [:]
        calculatorVO.maximumAllToPlaceEventRefs = [:]
        calculatorVO.minimumAllToPlaceEventRefs = [:]

        for (thisEventRef, completedMaximumEventVO) in completedMaximumEventVOs.vos {
            if calculatorVO.maximumPlaceEventRefs[ thisEventRef ] == nil {
                calculatorVO.maximumPlaceEventRefs[ thisEventRef ] = IntDicVOsVO()
            }
            if calculatorVO.maximumAllToPlaceEventRefs[ thisEventRef ] == nil {
                calculatorVO.maximumAllToPlaceEventRefs[ thisEventRef ] =  IntDicVOsVO()
            }
            calculatorVO.maximumPlaceEventRefs[ thisEventRef ]!.vos = (completedMaximumEventVO as! CompletedEventVO).thisOnlyRefs
            calculatorVO.maximumAllToPlaceEventRefs[ thisEventRef ]!.vos = (completedMaximumEventVO as! CompletedEventVO).allToHereRefs
        }

        for (thisEventRef, completedMinimumEventVO) in completedMinimumEventVOs.vos {

            if calculatorVO.minimumPlaceEventRefs[ thisEventRef ] == nil {
                calculatorVO.minimumPlaceEventRefs[ thisEventRef ] = IntDicVOsVO()
            }
            if calculatorVO.minimumAllToPlaceEventRefs[ thisEventRef ] == nil {
                calculatorVO.minimumAllToPlaceEventRefs[ thisEventRef ] =  IntDicVOsVO()
            }
            calculatorVO.minimumPlaceEventRefs[ thisEventRef ]!.vos = (completedMinimumEventVO as! CompletedEventVO).thisOnlyRefs
            calculatorVO.minimumAllToPlaceEventRefs[ thisEventRef ]!.vos = (completedMinimumEventVO as! CompletedEventVO).allToHereRefs
        }
    }
}
