import Foundation

class LocalizationHelper {

    private var app: App { return App.instance }
    private var balancing: Balancing { return Balancing.instance }
    private var happen: Happen { return Happen.instance }
    private var localization: Localization { return Localization.instance }
    private var viewed: Viewed { return Viewed.instance }

    func localize(_ textID: String, returnNilIfNotFound: Bool = false, localizationType: String? = nil) -> String? {
        let localizedValue: String? = localization.localizationModel.getLocalizedTextId(textID, localizationType: localizationType)
        if localizedValue == nil {
            if !returnNilIfNotFound {
                return textID + localization.localizationConfig.notFoundExtension
            }
        }
        return localizedValue
    }

    func localizeExtended(_ baseTextID: String, ext: String? = nil, params: [String: String]? = nil, returnNilIfNotFound: Bool = false, localizationType: String? = nil) -> String? {
        let thisExt: String = ext != nil ? ext! : localization.localizationConfig.defaultExtension
        let textID: String = buildTextID(baseTextID: baseTextID, ext: thisExt)
        var localizedValue: String? = localize(textID, returnNilIfNotFound: true, localizationType: localizationType)
        if localizedValue == nil {
            let alternativeExtension: String? = localization.localizationConfig.alternateExtensions[thisExt]
            if alternativeExtension != nil {
                localizedValue = localizeExtended(baseTextID, ext: alternativeExtension, params: nil, returnNilIfNotFound: true, localizationType: localizationType)
                if localizedValue != nil {
                    localization.localizationModel.addToLocalizationDictionary(textID, localizedValue!, localizationType: localizationType)
                }
            }
        }
        if localizedValue == nil {
            if returnNilIfNotFound {
                return nil
            }
            localizedValue = localize(textID, localizationType: localizationType)
        }
        if localizedValue != nil && params != nil {
            localizedValue = localizeParams(textID, params!, localizationType: localizationType)
        }
        return localizedValue
    }

    func localizeVOs(_ textID: String, _ replaceWith: [String: String], observerIDs: [String: String]? = nil, requestTypes: [String: String]? = nil, actions: [String: Any]? = nil, localizationType: String? = nil) -> [TextVO]? {
        let localizableVO: LocalizationParamVO? = localizeParamVO(textID, localizationType: localizationType)
        if (localizableVO == nil) {
            return nil
        }
        var returnVOs: [TextVO] = []

        for entryVO: LocalizationParamEntryVO in localizableVO!.entryVOs {
            var returnVO: TextVO? = nil
            if !entryVO.forReplace {
                returnVO = TextVO(entryVO.stringValue)
            } else if replaceWith[entryVO.stringValue] != nil {
                var attributes: [AppViewAttribute: Any]? = [:]
                if observerIDs != nil {
                    if observerIDs![entryVO.stringValue] != nil {
                        attributes![.observerID] = observerIDs![entryVO.stringValue]!
                    }
                }
                if requestTypes != nil {
                    if requestTypes![entryVO.stringValue] != nil {
                        attributes![.requestType] = requestTypes![entryVO.stringValue]!
                    }
                }
                if actions != nil {
                    if actions![entryVO.stringValue] != nil {
                        attributes![.action] = actions![entryVO.stringValue]!
                    }
                }
                if attributes!.count == 0 {
                    attributes = nil
                }
                returnVO = TextVO(replaceWith[entryVO.stringValue]!, replaceID: entryVO.stringValue, attributes: attributes)
            } else {
                returnVO = TextVO(localization.localizationConfig.openReplaceParam + entryVO.stringValue + localization.localizationConfig.closeReplaceParam, replaceID: entryVO.stringValue)
            }
            returnVOs.append(returnVO!)
        }

        return returnVOs
    }

    func buildTextID(baseTextID: String, ext: String) -> String {
        return baseTextID + "_" + ext
    }

    func getSeasonGroupName(_ seasonGroupRef: Int, ext: String? = nil, defaultValue: String = "Undefined") -> String {
        let balancingVO: SeasonGroupBalancingVO? = balancing.balancingModel.seasonGroupVOs[seasonGroupRef]
        if balancingVO == nil {
            return defaultValue
        }
        return localizeExtended(balancingVO!.baseTextID!, ext: ext)!
    }

    func getSeasonName(_ seasonRef: Int, ext: String? = nil, defaultValue: String = "Undefined") -> String {
        let balancingVO: SeasonBalancingVO? = balancing.balancingModel.seasonVOs[seasonRef]
        if balancingVO == nil {
            return defaultValue
        }
        return localizeExtended(balancingVO!.baseTextID!, ext: ext)!
    }

    func getAppropriateListingCompetitionName( _ listingRef:Int, seasonRef:Int, avoidMatchListingRefs:[Int:Any]?=nil ) -> String {
        let focalCompetitionRef:Int? = happen.happenedHelper.getFocalCompetitionRef(listingRef:listingRef, seasonRef: seasonRef, avoidMatchListingRefs: avoidMatchListingRefs)
        var competitionName:String? = nil
        if focalCompetitionRef != nil {
            competitionName = getCompetitionName( focalCompetitionRef!, seasonRef: seasonRef)
        } else {
            var parentName:String? = getPrimaryParentName( listingRef, seasonRef:seasonRef )

            if parentName != nil {
                parentName = " ( " +  parentName! + " ) "
            } else {
                parentName = ""
            }

            competitionName = getCompetitionOrListingName(listingRef, seasonRef:seasonRef, allowDefault: true)! + parentName!

        }
        return competitionName!
    }

    func getPrimaryParentName( _ listingRef:Int, seasonRef:Int ) -> String? {
        let listingBalancingVO:ListingBalancingVO? = balancing.balancingModel.getListingVO(listingRef, seasonRef: seasonRef)
        if listingBalancingVO == nil {
            return nil
        }

        var childCompetitionRef:Int? = nil
        var parentCompetitionRef:Int? = listingBalancingVO!.parentCompetitionRef
        var parentName:String? = nil
        var childName:String? = nil
        var parentCompetitionVO:CompetitionVO? = nil
        while parentCompetitionRef != nil {
            parentCompetitionVO = happen.happenedHelper.getBestCompetitionVO(competitionRef: parentCompetitionRef, seasonRef: seasonRef)!
            if parentCompetitionVO!.listingParent && parentCompetitionVO!.childListingSeasonRefs.count > 1 {
                childName = getListingName( listingRef, seasonRef:seasonRef )
                break
            }
            if childCompetitionRef != nil && !parentCompetitionVO!.listingParent && parentCompetitionVO!.childCompetitionRefs.count > 1 {
                childName = getCompetitionName( childCompetitionRef!, seasonRef: seasonRef)
                break
            }

            if parentCompetitionVO!.balancingVO.competitionType == BalancingConstant.FOCAL_COMPETITION_TYPE {
                childName = getCompetitionName( parentCompetitionRef!, seasonRef: seasonRef)
                childCompetitionRef = parentCompetitionRef
                parentCompetitionRef = parentCompetitionVO!.balancingVO.parentCompetitionRef
                break
            }

            childCompetitionRef = parentCompetitionRef
            parentCompetitionRef = parentCompetitionVO!.balancingVO.parentCompetitionRef
        }

        if parentCompetitionRef != nil {
            parentName =  getCompetitionName( parentCompetitionRef!, seasonRef: seasonRef)
        }

        childCompetitionRef = 0
        while parentCompetitionRef != nil && childName != nil {
            parentCompetitionVO = happen.happenedHelper.getBestCompetitionVO( competitionRef: parentCompetitionRef, seasonRef:seasonRef )!
            if childCompetitionRef != nil && parentCompetitionVO!.childCompetitionRefs.count > 1 {
                parentName = getCompetitionName( childCompetitionRef!, seasonRef:seasonRef )
                break
            }

            if parentCompetitionVO!.balancingVO.competitionType == BalancingConstant.FOCAL_COMPETITION_TYPE {
                parentName = getCompetitionName( parentCompetitionRef!, seasonRef:seasonRef )
                break
            }

            childCompetitionRef = parentCompetitionRef
            parentCompetitionRef = parentCompetitionVO!.balancingVO.parentCompetitionRef
        }
        return parentName
    }

    func getCompetitionName( _ competitionRef:Int, seasonRef:Int?=nil, ext:String?=nil, showRefs:Bool=false, acceptRecognizer:Bool=false, ignoreIf:String = "", defaultValue:String = "Undefined" ) -> String {

        let competitionBalancingVO:CompetitionBalancingVO? = balancing.balancingModel.getCompetitionVO(competitionRef, seasonRef: seasonRef)
        if competitionBalancingVO == nil {
            return defaultValue
        }
        var competitionName:String? = localizeExtended(competitionBalancingVO!.baseTextID!, ext: ext, returnNilIfNotFound: true)
        if competitionName == nil {
            if acceptRecognizer {
                competitionName = competitionBalancingVO!.recognizer
            }
        }
        if competitionName == nil {
            competitionName = defaultValue
        }
        if competitionName! == ignoreIf {
            competitionName = ""
        }
        if showRefs {
            competitionName! += " (" + String(competitionRef) + ")"
        }
        return competitionName!
    }

    func getListingName( _ listingRef:Int, seasonRef:Int?=nil, ext:String?=nil, showRefs:Bool=false, acceptRecognizer:Bool=false, defaultValue:String = "Undefined" ) -> String {

        let listingBalancingVO:ListingBalancingVO? = balancing.balancingModel.getListingVO(listingRef, seasonRef: seasonRef)
        if listingBalancingVO == nil {
            return defaultValue
        }
        var listingName:String? = localizeExtended(listingBalancingVO!.baseTextID!, ext: ext, returnNilIfNotFound: true)
        if listingName == nil {
            if acceptRecognizer {
                listingName = listingBalancingVO!.recognizer
            }
        }
        if listingName == nil {
            listingName = defaultValue
        }
        if showRefs {
            listingName! += " ("
            if seasonRef != nil {
                if let listingSeasonVO:ListingSeasonVO = happen.happenedModel.getListingSeasonVO(seasonRef!, listingRef) {
                    listingName! += String(listingSeasonVO.ref) + ", "
                }
            }
            listingName! += String(listingRef) + ")"
        }
        return listingName!
    }

    func getTeamName( _ teamRef:Int, seasonRef:Int?, nonBreak:Bool=false, ext:String?=nil, showRefsForListingRef:Int?=nil, acceptRecognizer:Bool=false, defaultValue:String = "Undefined" ) -> String {

        let clubBalancingVO:ClubBalancingVO? = balancing.balancingHelper.getClubVO(teamRef, seasonRef: seasonRef)
        if clubBalancingVO == nil {
            return defaultValue
        }
        var teamName:String? = localizeExtended( clubBalancingVO!.baseTextID!, ext:ext )

        if acceptRecognizer {
            teamName = teamName != nil ? teamName : clubBalancingVO!.recognizer
        }

        teamName = teamName != nil ? teamName : defaultValue

        if showRefsForListingRef != nil  {
            if let teamListingVO:TeamListingVO = happen.happenedModel.getTeamListingVO( seasonRef!, showRefsForListingRef!, teamRef ) {
                teamName = teamName! + " (" + String(teamListingVO.ref)
                teamName = teamName! + ", " + String(teamRef) + ")"
            }
        }
        if nonBreak {
            teamName = teamName!.splitJoin(" ", viewed.viewConfig.nonBreakCode)
        }

        return teamName!
    }

    func getEventName( _ eventRef:Int, seasonRef:Int?=nil, ext:String?=nil ) -> String {
        let thisExt:String = ext != nil ? ext! : LocalizationConstant.READ_EXTENSION
        let eventBalancingVO:EventBalancingVO = balancing.balancingModel.getEventVO(eventRef, seasonRef: seasonRef)!
        return localizeExtended( eventBalancingVO.baseTextID!, ext:thisExt )!
    }

    func getCompetitionEventTitle( _ listingRef:Int, _ eventRef:Int, seasonRef:Int? = nil, ext:String?=nil, competitionExtension:String?=nil ) -> String {
        let thisExt:String = ext != nil ? ext! : LocalizationConstant.READ_EXTENSION
        let eventSeasonBalancingVO:EventBalancingVO = balancing.balancingModel.getEventVO(eventRef, seasonRef: seasonRef)!

        let textID:String = buildTextID(baseTextID: eventSeasonBalancingVO.baseTextID!, ext: thisExt)
        let competitionExt:String = competitionExtension != nil ? competitionExtension! : LocalizationConstant.NAME_EXTENSION
        var competitionName:String? = getCompetitionOrListingName(listingRef, seasonRef: seasonRef, ext: competitionExt)
        competitionName = competitionName != nil ? competitionName : ""
        let replaceWith:[String:String] = [LocalizationConstant.COMPETITION_PARAM:competitionName!]

        var returnValue = ""
        let vos:[TextVO]? = localizeVOs(textID, replaceWith)
        if vos != nil {
            for textVO in vos! {
                returnValue += textVO.text
            }
        }
        return returnValue
    }

    func getCompetitionOrListingName(  _ listingRef:Int, seasonRef:Int? = nil, ext:String?=nil, allowDefault:Bool = false, defaultValue:String = "Undefined" ) -> String? {
        let listingBalancingVO:ListingBalancingVO? = balancing.balancingModel.getListingVO(listingRef, seasonRef: seasonRef)
        if listingBalancingVO == nil {
            return allowDefault ? defaultValue : nil
        }
        var competitionVO:CompetitionVO? = happen.happenedHelper.getBestCompetitionVO(competitionRef: listingBalancingVO!.parentCompetitionRef, seasonRef: seasonRef)

        let seasonGroupRef:Int? = competitionVO?.seasonGroupRef
        var prevCompetitionRef:Int?
        while competitionVO != nil {
            if competitionVO!.listingParent && competitionVO!.childListingSeasonRefs.count > 1 {
                return getListingName(listingRef, seasonRef: seasonRef, ext: ext)
            }
            if !competitionVO!.listingParent && competitionVO!.childCompetitionRefs.count > 1 && prevCompetitionRef != nil {
                return getCompetitionName(prevCompetitionRef!, seasonRef: seasonRef, ext: ext)
            }
            if competitionVO!.balancingVO.competitionType == BalancingConstant.FOCAL_COMPETITION_TYPE {
                return getCompetitionName(competitionVO!.ref, seasonRef: seasonRef, ext: ext)
            }
            if competitionVO!.balancingVO.parentCompetitionRef == nil {
                break
            }
            prevCompetitionRef = competitionVO!.ref
            competitionVO = happen.happenedModel.getCompetitionVO(competitionVO!.balancingVO.parentCompetitionRef!, seasonGroupRef: seasonGroupRef)
        }
        return allowDefault ? defaultValue : nil
    }

    func getEventScenarioExtension( _ eventScenarioRef:Int, forceAgainstExtension:String? = nil ) -> String? {
        let eventScenarioVO:EventScenarioVO? = happen.happenedModel.eventScenarioVOs[eventScenarioRef]
        if eventScenarioVO == nil {
            return nil
        }

        let upcomingExtension = eventScenarioVO!.upcoming ? LocalizationConstant.UPCOMING_EXTENSION : LocalizationConstant.HAPPENED_EXTENSION
        let toHappenExtension = eventScenarioVO!.toHappen ? LocalizationConstant.TO_HAPPEN_EXTENSION : LocalizationConstant.NOT_HAPPEN_EXTENSION

        var againstExtension:String? = nil
        let occurredFixtureRef:Int? = eventScenarioVO!.occurredFixtureRef
        let fixtureVO:FixtureVO? = occurredFixtureRef != nil ? happen.happenedModel.fixtureVOs[ occurredFixtureRef! ] : nil
        if forceAgainstExtension != nil {
            againstExtension = forceAgainstExtension
        } else if fixtureVO != nil {
            againstExtension = LocalizationConstant.AGAINST_EXTENSION
        } else if eventScenarioVO!.fromResult {
            againstExtension = LocalizationConstant.DID_NOT_PLAY_EXTENSION
        } else {
            againstExtension = LocalizationConstant.NO_GAMES_EXTENSION
        }
        return upcomingExtension + "_" + toHappenExtension + "_" + againstExtension!
    }

    func getSortingTeamName(seasonRef: Int, teamRef: Int, useLocalizedFirst: Bool = false, ext: String? = nil) -> String {
        let clubBalancingVO:ClubBalancingVO? = balancing.balancingModel.getClubVO(teamRef, seasonRef:seasonRef)
        let teamBalancingVO:TeamBalancingVO? = balancing.balancingModel.getTeamVO(teamRef, seasonRef:seasonRef)

        if( useLocalizedFirst && clubBalancingVO != nil )
        {
            return localizeExtended( clubBalancingVO!.baseTextID!, ext:ext )!
        }

        if( teamBalancingVO != nil && teamBalancingVO!.recognizer != nil )
        {
            return teamBalancingVO!.recognizer!.lowercased()
        }

        if( clubBalancingVO == nil )
        {
            return ""
        }
        if( clubBalancingVO!.recognizer != nil )
        {
            return clubBalancingVO!.recognizer!.lowercased()
        }
        return localizeExtended( clubBalancingVO!.baseTextID!, ext:ext )!


    }

    func getPrimaryCompetitionNames( seasonRef:Int, listingRef:Int ) -> [String] {

        var childName:String? = nil
        var parentName:String? = nil
        var childCompetitionRef:Int? = nil

        var competitionVO:CompetitionVO? = happen.happenedHelper.parentCompetitionVO(seasonRef, listingRef)
        while competitionVO != nil {

            if competitionVO!.listingParent && competitionVO!.childListingSeasonRefs.count > 1 {
                childName = getListingName(listingRef, seasonRef: seasonRef)
                break
            }
            if !competitionVO!.listingParent && competitionVO!.childCompetitionRefs.count > 1 && childCompetitionRef != nil {
                childName = getCompetitionName(childCompetitionRef!, seasonRef: seasonRef)
                break
            }

            var doBreak:Bool = false
            if competitionVO!.balancingVO.competitionType == BalancingConstant.FOCAL_COMPETITION_TYPE {
                childName = getCompetitionName(competitionVO!.ref, seasonRef: seasonRef)
                doBreak = true
            }

            childCompetitionRef = competitionVO!.ref
            if competitionVO!.balancingVO.parentCompetitionRef != nil {
                competitionVO = happen.happenedModel.getCompetitionVO(competitionVO!.balancingVO.parentCompetitionRef!, seasonGroupRef: competitionVO!.seasonGroupRef)
            } else {
                competitionVO = nil
            }
            if doBreak {
                break
            }
        }

        if competitionVO != nil {
            parentName = getCompetitionName(competitionVO!.ref, seasonRef: seasonRef)
        }

        childCompetitionRef = nil
        while competitionVO != nil && childName != nil {
            if childCompetitionRef != nil && competitionVO!.childCompetitionRefs.count > 1 {
                parentName = getCompetitionName(childCompetitionRef!, seasonRef: seasonRef)
                break
            }

            if competitionVO!.balancingVO.competitionType == BalancingConstant.FOCAL_COMPETITION_TYPE {
                parentName = getCompetitionName(competitionVO!.ref, seasonRef: seasonRef)
                break
            }

            childCompetitionRef = competitionVO!.ref
            if competitionVO!.balancingVO.parentCompetitionRef != nil {
                competitionVO = happen.happenedModel.getCompetitionVO(competitionVO!.balancingVO.parentCompetitionRef!, seasonGroupRef: competitionVO!.seasonGroupRef)
            } else {
                competitionVO = nil
            }
        }

        childName = childName != nil ? childName : "undefined".localized
        parentName = parentName != nil ? parentName : "undefined".localized
        return [parentName!, childName!]
    }

    func getEventScenarioDescription( eventScenarioRef:Int, forceAgainstExtension:String? = nil ) -> [TextVO]?
    {
        let eventScenarioVO:EventScenarioVO? = happen.happenedModel.eventScenarioVOs[ eventScenarioRef ]
        if eventScenarioVO == nil {
            return nil
        }

        let teamName:String = localization.localizationHelper.getTeamName( eventScenarioVO!.teamRef, seasonRef: eventScenarioVO!.seasonRef, nonBreak: true )

        var competitionName:String? = localization.localizationHelper.getCompetitionOrListingName(eventScenarioVO!.listingRef, seasonRef: eventScenarioVO!.seasonRef, ext: LocalizationConstant.WANT_THE_EXTENSION)
        competitionName = competitionName != nil ? competitionName : "undefined".localized
        var eventScenarioReplaceWith:[String:String] = [
                LocalizationConstant.TEAM_PARAM: teamName,
                LocalizationConstant.COMPETITION_PARAM: competitionName!
        ]

        let occurredFixtureRef:Int? = eventScenarioVO!.occurredFixtureRef

        let fixtureVO:FixtureVO? = occurredFixtureRef != nil  ? happen.happenedModel.fixtureVOs[ occurredFixtureRef! ] : nil
        var oppTeamRef:Int? = nil
        if fixtureVO != nil {
            oppTeamRef = fixtureVO!.homeRef == eventScenarioVO!.teamRef ? fixtureVO!.awayRef : fixtureVO!.homeRef
            let homeAwayID:String = fixtureVO!.homeRef == eventScenarioVO!.teamRef ? LocalizationConstant.HOME_ID : LocalizationConstant.AWAY_ID
            let teamGoals:Int = fixtureVO!.homeRef == eventScenarioVO!.teamRef ? fixtureVO!.resultVO.homeGoals : fixtureVO!.resultVO.awayGoals
            let oppGoals:Int = fixtureVO!.homeRef == eventScenarioVO!.teamRef ? fixtureVO!.resultVO.awayGoals : fixtureVO!.resultVO.homeGoals
            var winDefeatDrawID:String? = nil
            if teamGoals > oppGoals {
                winDefeatDrawID = LocalizationConstant.WIN_ID
            } else if teamGoals < oppGoals {
                winDefeatDrawID = LocalizationConstant.DEFEAT_ID
            } else {
                winDefeatDrawID = LocalizationConstant.DRAW_ID
            }
            let winGoals:Int = fixtureVO!.resultVO.homeGoals >= fixtureVO!.resultVO.awayGoals ? fixtureVO!.resultVO.homeGoals : fixtureVO!.resultVO.awayGoals
            let loseGoals:Int = fixtureVO!.resultVO.homeGoals < fixtureVO!.resultVO.awayGoals ? fixtureVO!.resultVO.homeGoals : fixtureVO!.resultVO.awayGoals

            let oppName:String = localization.localizationHelper.getTeamName(oppTeamRef!, seasonRef: eventScenarioVO!.seasonRef, nonBreak: true)
            let homeAwayString:String = localization.localizationHelper.localize( homeAwayID )!
            let winDefeatDrawString:String = localization.localizationHelper.localize( winDefeatDrawID! )!
            eventScenarioReplaceWith[ LocalizationConstant.OPP_PARAM ] = oppName
            eventScenarioReplaceWith[ LocalizationConstant.HOME_AWAY_PARAM ] = homeAwayString
            eventScenarioReplaceWith[ LocalizationConstant.WIN_DEFEAT_DRAW_PARAM ] = winDefeatDrawString
            eventScenarioReplaceWith[ LocalizationConstant.WIN_SCORE_PARAM ] = String(winGoals)
            eventScenarioReplaceWith[ LocalizationConstant.LOSE_SCORE_PARAM ] = String(loseGoals)
        }
        let localizationExtension:String = localization.localizationHelper.getEventScenarioExtension(eventScenarioRef, forceAgainstExtension: forceAgainstExtension)!
        let eventBalancingVO:EventBalancingVO = balancing.balancingModel.getEventVO(eventScenarioVO!.eventRef, seasonRef: eventScenarioVO!.seasonRef)!
        let textID:String = localization.localizationHelper.buildTextID(baseTextID: eventBalancingVO.baseTextID!, ext: localizationExtension)
        let requestTypes:[String:String]  = [
                LocalizationConstant.TEAM_PARAM: AppConstant.SHOW_TEAM_REQUEST,
                LocalizationConstant.OPP_PARAM: AppConstant.SHOW_TEAM_REQUEST
        ]

        var actions:[String:Int] = [:]
        let teamRef:Int? = happen.happenedModel.getTeamListingVO(eventScenarioVO!.seasonRef, eventScenarioVO!.listingRef, eventScenarioVO!.teamRef)?.ref
        let oppRef:Int? = oppTeamRef != nil ? happen.happenedModel.getTeamListingVO(eventScenarioVO!.seasonRef, eventScenarioVO!.listingRef, oppTeamRef!)?.ref : nil
        if teamRef != nil {
            actions[LocalizationConstant.TEAM_PARAM] = teamRef
        }
        if oppRef != nil {
            actions[LocalizationConstant.OPP_PARAM] = oppRef
        }
        let textVOs:[TextVO] = localization.localizationHelper.localizeVOs(textID, eventScenarioReplaceWith, requestTypes:requestTypes, actions:actions)!

        return textVOs
    }

    func localizeParams( _ textID:String, _ params:[String:String], returnNilIfNotFound:Bool=false, localizationType:String?=nil ) -> String? {
        let vo:LocalizationParamVO? = localizeParamVO( textID, localizationType:localizationType )
        if vo == nil {
            if !returnNilIfNotFound {
                return localize(textID, returnNilIfNotFound: false, localizationType: localizationType)
            }
            return nil
        }
        var returnString:String = ""
        for entryVO:LocalizationParamEntryVO in vo!.entryVOs {
            if !entryVO.forReplace {
                returnString += entryVO.stringValue
            } else if params[entryVO.stringValue] != nil {
                returnString += params[entryVO.stringValue]!
            } else {
                returnString += localization.localizationConfig.openReplaceParam + entryVO.stringValue + localization.localizationConfig.closeReplaceParam
            }
        }
        return returnString
    }

    func localizeParamVO( _ textID:String, localizationType:String?=nil ) -> LocalizationParamVO? {
        let typeVO:LocalizationVO? = localization.localizationModel.typeVO(localizationType)
        if typeVO == nil {
            return nil
        }
        var paramVO:LocalizationParamVO? = localization.localizationModel.getLocalizationParamVO(textID, localizationType: localizationType)
        if paramVO != nil {
            return paramVO
        }
        let localizedString:String? = localize(textID, returnNilIfNotFound: true, localizationType: localizationType)
        if localizedString == nil {
            return nil
        }
        paramVO = LocalizationParamVO()
        typeVO!.paramVOs[textID] = paramVO
        let openParam:String = localization.localizationConfig.openReplaceParam
        let closeParam:String = localization.localizationConfig.closeReplaceParam

        var nextPos:Int = 0

        while nextPos < localizedString!.count {
            var nextOpenPos:Int? = localizedString!.indexOf(value: openParam, subFromStart: nextPos)
            var nextClosePos:Int?
            if nextOpenPos != nil {
                nextClosePos = localizedString!.indexOf(value: closeParam, subFromStart: nextPos + 1)
                if nextClosePos == nil {
                    nextOpenPos = nil
                }
            }
            if nextOpenPos == nil || nextOpenPos! != nextPos {
                let distance:Int? = nextOpenPos == nil ? nil : nextOpenPos! - nextPos
                let standardString:String = localizedString!.substring(subFromStart: nextPos, distFrom: distance)
                let entryVO:LocalizationParamEntryVO = LocalizationParamEntryVO(standardString, false)
                paramVO!.entryVOs.append(entryVO)
            } else {
                let distance:Int = nextClosePos! - nextOpenPos! - openParam.count
                let replaceString:String = localizedString!.substring(subFromStart: nextPos + openParam.count, distFrom: distance)
                let entryVO:LocalizationParamEntryVO = LocalizationParamEntryVO(replaceString, true)
                paramVO!.entryVOs.append(entryVO)
            }
            if nextOpenPos == nil {
                break
            }
            if nextOpenPos! == nextPos {
                nextPos = nextClosePos! + closeParam.count
                continue
            }
            nextPos = nextOpenPos!
        }
        return paramVO
    }


    func applySpecial( _ dictionary:[String:String] ) -> [String:String] {
        var returnDictionary:[String:String] = [:]
        for (key, value) in dictionary {
            var useValue:String = value
            useValue = useValue.splitJoin("u0027", viewed.viewConfig.apostrophe)
            useValue = useValue.splitJoin("u0026", viewed.viewConfig.ampersand)
            useValue = useValue.splitJoin("u2011", viewed.viewConfig.hyphen)
            useValue = useValue.splitJoin("&amp;", viewed.viewConfig.ampersand)
            returnDictionary[key] = useValue
        }
        return returnDictionary
    }
}
