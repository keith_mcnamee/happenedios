import Foundation

class ParseNativeLocalizationCommand {

    private var localization:Localization { return Localization.instance }

    func command() {
        let localizationType:String = localization.localizationModel.currentType!

        let localizationVO:LocalizationVO = LocalizationVO(LocalizationConstant.NATIVE_TYPE, nil)

        if let bundlePath:String = Bundle.main.path(forResource: "Localizable", ofType: "strings", inDirectory:nil, forLocalization: localizationType) {
            let translations:[String:String] = NSDictionary(contentsOfFile:bundlePath) as! [String:String]
            localizationVO.translations = localization.localizationHelper.applySpecial(translations)
            localizationVO.initialized = true

            localization.localizationModel.typeVOs[localizationVO.type] = localizationVO
        }


    }
}
