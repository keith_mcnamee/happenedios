import Foundation

class ParseLocalizationCommand {

    private var app:App { return App.instance }
    private var localization:Localization { return Localization.instance }

    func command(_ type:String? = nil) {

        let localizationType:String = type != nil ? type! : localization.localizationModel.currentType!
        if let localizationVO:LocalizationVO = localization.localizationModel.typeVOs[localizationType] {
            let localizationURL:URL = app.fileHelper.fileURL(localizationVO.fileName!, localization.localizationConfig.fileExt)!
            let translations:[String:String] = NSDictionary(contentsOfFile:localizationURL.path) as! [String:String]
            localizationVO.translations = localization.localizationHelper.applySpecial(translations)
            localizationVO.paramVOs = [:]
            localizationVO.initialized = true
        }
    }
}
