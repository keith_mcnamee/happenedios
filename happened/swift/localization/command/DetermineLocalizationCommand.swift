import Foundation

class DetermineLocalizationCommand {

    private var localization:Localization { return Localization.instance }

    func command() {
        let localizations = Bundle.main.localizations;
        let preferredLanguages = Locale.preferredLanguages
        var determinedValue:String?
        for languageType:String in preferredLanguages {
            let haveConfig:Bool = localization.localizationConfig.configVOs[languageType] != nil
            if haveConfig
            {
                for localization:String in localizations {
                    if localization == languageType {
                        determinedValue = languageType
                        break;
                    }
                }
                if determinedValue != nil {
                    break
                }
            }
        }
        if determinedValue == nil {
            determinedValue = localization.localizationConfig.defaultType
        }

        localization.localizationModel.currentType = determinedValue
    }
}
