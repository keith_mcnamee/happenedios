import Foundation

class InitializeLocalizationCommand {

    private var localization:Localization { return Localization.instance }
    private var preference:Preference { return Preference.instance }

    func command() {

        if localization.localizationModel.currentType == nil {
            localization.determineLocalizationCommand().command()
        }
        applyDynamic()
        applyNative()
    }

    func applyDynamic() {

        let localizationType:String = localization.localizationModel.currentType!
        let fileName:String = localization.localizationConfig.configVO(localizationType).fileName
        let localizationVO:LocalizationVO = LocalizationVO(localizationType, fileName)

        localization.localizationModel.typeVOs[localizationVO.type] = localizationVO

        localizationVO.version = preference.preferenceModel.getLocalizationVersion(localizationVO.type)!

        localization.parseLocalizationCommand().command()
    }

    func applyNative() {
        localization.parseNativeLocalizationCommand().command()
    }
}
