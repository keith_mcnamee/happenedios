import Foundation

class LocalizationConfig {

    private var localization:Localization { return Localization.instance }

    let fileExt:String = "strings"

    var defaultType:String = "en"

    var defaultExtension:String = "Name"
    var notFoundExtension:String = "**"
    var openReplaceParam:String = "{"
    var closeReplaceParam:String = "}"
    var alternateExtensions:[String:String] = [:] // I currently have none

    var configVOs:[String: LocalizationConfigVO] = [:]

    init() {
        let enType = LocalizationConfigVO("en", "LocalizableEn")
        addConfigVO(enType)
    }


    func addConfigVO (_ configVO: LocalizationConfigVO) {
        configVOs[ configVO.type ] = configVO
    }

    func configVO(_ type:String) ->LocalizationConfigVO {
        if let vo:LocalizationConfigVO = configVOs[type] {
            return vo
        }
        return configVOs[defaultType]!
    }
}
