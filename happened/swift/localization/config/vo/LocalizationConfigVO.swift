import Foundation

class LocalizationConfigVO {

    let type:String
    let fileName:String

    init(_ type:String, _ fileName:String) {
        self.type = type
        self.fileName = fileName
    }
}
