import Foundation

class LocalizationParamEntryVO {

    var stringValue:String
    var forReplace:Bool

    init(_ stringValue: String, _ forReplace: Bool) {
        self.stringValue = stringValue
        self.forReplace = forReplace
    }

}
