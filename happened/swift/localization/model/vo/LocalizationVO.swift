import Foundation

class LocalizationVO {

    let type:String
    let fileName:String?

    var version:Int?
    var translations:[String : String] = [:]
    var paramVOs:[String : LocalizationParamVO] = [:]
    var initialized:Bool = false

    init(_ type: String, _ fileName: String?) {
        self.type = type
        self.fileName = fileName
    }

    func reset() {
        translations = [:]
        paramVOs = [:]
        initialized = false
    }

}
