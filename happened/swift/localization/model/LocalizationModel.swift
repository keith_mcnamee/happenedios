import Foundation

class LocalizationModel {

    private var localization:Localization { return Localization.instance }

    var currentType:String?
    var typeVOs:[String : LocalizationVO] = [:]

    func typeVO(_ type:String?=nil) ->LocalizationVO? {
        if type == nil {
            if currentType == nil {
                return nil
            }
            return typeVOs[currentType!]
        }
        return typeVOs[type!]
    }

    func getLocalizedTextId( _ textID:String, localizationType:String?=nil ) -> String? {
        let vo:LocalizationVO? = typeVO(localizationType)
        if vo == nil {
            return nil;
        }
        return vo!.translations[textID]
    }

    func getLocalizationParamVO( _ textID:String, localizationType:String?=nil ) -> LocalizationParamVO? {
        let vo:LocalizationVO? = typeVO(localizationType)
        if vo == nil {
            return nil;
        }
        return vo!.paramVOs[textID]
    }

    func addToLocalizationDictionary( _ textID:String, _ localizationValue:String, localizationType:String?=nil ) {
        let vo:LocalizationVO? = typeVO(localizationType)
        if vo == nil {
            return;
        }
        vo!.translations[textID] = localizationValue
    }

}
