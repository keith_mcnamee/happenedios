import Foundation

class Localization {

    static let instance:Localization = Localization()

    let localizationConfig:LocalizationConfig = LocalizationConfig();
    let localizationHelper:LocalizationHelper = LocalizationHelper();
    let localizationModel:LocalizationModel = LocalizationModel();


    func determineLocalizationCommand() -> DetermineLocalizationCommand {
        return DetermineLocalizationCommand();
    }

    func initializeLocalizationCommand() -> InitializeLocalizationCommand {
        return InitializeLocalizationCommand();
    }

    func parseLocalizationCommand() -> ParseLocalizationCommand {
        return ParseLocalizationCommand();
    }

    func parseNativeLocalizationCommand() -> ParseNativeLocalizationCommand {
        return ParseNativeLocalizationCommand();
    }
}
