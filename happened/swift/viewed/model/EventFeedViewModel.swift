import Foundation

class EventFeedViewModel {

    var containerFeedListVO: ContainerFeedListVO = ContainerFeedListVO()

    func reset() {
        containerFeedListVO.removeAll()
    }
}
