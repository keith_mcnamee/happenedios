import Foundation

class ViewModel {

    var seasonGroupVOs:[Int:SeasonGroupViewVO] = [Int:SeasonGroupViewVO]()
    var seasonVOs:[Int:SeasonViewVO] = [Int:SeasonViewVO]()
    var listingSeasonVOs:[Int:RefTextViewVO] = [Int:RefTextViewVO]()

    var tabBarDisplayed:Bool = false
    var visibleTab:TabIdentifier? = nil

    var waitingPageTabID:TabIdentifier? = nil
    var waitingPageUserInfo:[AnyHashable : Any]? = nil

    func reset() {
        seasonGroupVOs.removeAll()
        seasonVOs.removeAll()
        listingSeasonVOs.removeAll()
    }

}
