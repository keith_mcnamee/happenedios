import Foundation

class RefTextViewVO {

    let ref:Int
    let text:String

    init( ref:Int, text:String){
        self.ref = ref
        self.text = text
    }
}
