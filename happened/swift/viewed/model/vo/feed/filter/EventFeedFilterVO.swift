import Foundation

class EventFeedFilterVO: FeedFilterVO {

    var toHappenState:ToHappenedState = .toHappenOnly

    var allEventSelected:Bool = false
    var eventRefs:[Int:NSNull] = [:]

}
