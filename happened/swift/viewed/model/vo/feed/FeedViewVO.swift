import Foundation

class FeedViewVO {

    let feedType:FeedType
    let feedSource:FeedSource
    let seasonGroupRef:Int
    let isHappenedState:IsHappenedState
    var filterVO: FeedFilterVO?

    var entryVOs:[FeedEntryVO] = []
    var endOfList:Bool = false
    var amountAvailable:Int?

    init(feedType:FeedType, feedSource:FeedSource, seasonGroupRef:Int, isHappenedState: IsHappenedState, filterVO: FeedFilterVO?=nil){

        self.feedType = feedType
        self.feedSource = feedSource
        self.seasonGroupRef = seasonGroupRef
        self.isHappenedState = isHappenedState
        self.filterVO = filterVO
    }

    func reset() {
        entryVOs = []
        endOfList = false
        amountAvailable = nil
        filterVO = nil
    }

    var isFiltered:Bool {
        return filterVO?.filterString != nil
    }

    var isTeamFeed:Bool {
        return feedSource == .specificTeam || feedSource == .allTeam
    }

}
