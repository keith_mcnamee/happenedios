import Foundation

class FeedListVO {

    private var viewed: Viewed { return Viewed.instance }

    let feedType:FeedType
    let feedSource:FeedSource
    let seasonGroupRef:Int

    var happenedRegularFeedVO:FeedViewVO?
    var happenedFilteredFeedVO:FeedViewVO?
    var upcomingRegularFeedVO:FeedViewVO?
    var upcomingFilteredFeedVO:FeedViewVO?

    init(feedType:FeedType, feedSource:FeedSource, seasonGroupRef:Int ){
        self.feedType = feedType
        self.feedSource = feedSource
        self.seasonGroupRef = seasonGroupRef
    }

    func reset() {
        happenedRegularFeedVO = nil
        happenedFilteredFeedVO = nil
        upcomingRegularFeedVO = nil
        upcomingFilteredFeedVO = nil
    }

    func resetFeed(isUpcoming:Bool, isFiltered:Bool) {

        if !isUpcoming {
            if !isFiltered {
                happenedRegularFeedVO = nil
            }
            happenedFilteredFeedVO = nil
        }
        if !isFiltered {
            upcomingRegularFeedVO = nil
        }
        upcomingFilteredFeedVO = nil
    }

    func addFeedVO(_ vo:FeedViewVO) {
        if vo.isHappenedState != .upcomingOnly {
            if !vo.isFiltered {
                happenedRegularFeedVO = vo
                return
            }
            happenedFilteredFeedVO = vo
            return
        }
        if !vo.isFiltered {
            upcomingRegularFeedVO = vo
            return
        }
        upcomingFilteredFeedVO = vo
    }

    func getFeedVO(isUpcoming:Bool, isFiltered:Bool) -> FeedViewVO? {
        if !isUpcoming {
            if !isFiltered {
                return happenedRegularFeedVO
            }
            return happenedFilteredFeedVO
        }
        if !isFiltered {
            return upcomingRegularFeedVO
        }
        return upcomingFilteredFeedVO
    }
}
