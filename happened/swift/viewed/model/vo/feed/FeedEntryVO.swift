import Foundation

class FeedEntryVO {

    let index:Int
    let ref:Int?
    let requested:Bool
    let invalid:Bool

    init( _ index:Int, ref:Int?=nil, requested:Bool=false, invalid:Bool=false ){
        self.index = index
        self.ref = ref
        self.requested = requested
        self.invalid = invalid
    }
}
