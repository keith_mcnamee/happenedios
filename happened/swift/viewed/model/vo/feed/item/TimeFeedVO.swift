import Foundation

class TimeFeedVO: ItemFeedVO {

    let date:Date?
    let anyTime:Bool

    var childVOs:[CompetitionFeedVO] = []

    init( _ textVOs:[TextVO], date:Date? = nil, anyTime:Bool = false ){
        self.date = date
        self.anyTime = anyTime

        super.init( textVOs )
    }

    var unscheduled:Bool {
        return date == nil
    }

}
