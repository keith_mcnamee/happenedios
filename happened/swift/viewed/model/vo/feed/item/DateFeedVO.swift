import Foundation

class DateFeedVO: ItemFeedVO {

    let date12AM:Date?
    let year:Int?

    var childVOs:[TimeFeedVO] = []

    init( _ textVOs:[TextVO], date12AM:Date? = nil, year:Int? = nil ) {
        self.date12AM = date12AM
        self.year = year
        super.init( textVOs )
    }

    var unscheduled:Bool {
        return date12AM == nil
    }
}
