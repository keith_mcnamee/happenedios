import Foundation

class CompetitionFeedVO: ItemFeedVO {

    let seasonRef:Int
    let listingRef:Int

    var childVOs:[EventFeedVO] = []

    init( _ textVOs:[TextVO], seasonRef:Int, listingRef:Int ){
        self.seasonRef = seasonRef
        self.listingRef = listingRef

        super.init( textVOs )
    }

}
