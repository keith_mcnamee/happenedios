import Foundation

class EventFeedVO: ItemFeedVO {

    let eventScenarioRef:Int

    init( _ textVOs:[TextVO], eventScenarioRef:Int ){
        self.eventScenarioRef = eventScenarioRef

        super.init( textVOs )
    }

}
