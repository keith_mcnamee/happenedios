import Foundation

class ContainerFeedListVO {

    var listingVOs:[Int:FeedListVO] = [Int:FeedListVO]()
    var teamSpecificVOs:[Int:FeedListVO] = [Int:FeedListVO]()
    var teamAllVOs:[Int:FeedListVO] = [Int:FeedListVO]()

    func getFeedListVO( feedSource:FeedSource, seasonGroupRef:Int ) -> FeedListVO? {
        if feedSource == .listing {
            return listingVOs[seasonGroupRef]
        }
        if feedSource == .specificTeam {
            return teamSpecificVOs[seasonGroupRef]
        }
        if feedSource == .allTeam {
            return teamAllVOs[seasonGroupRef]
        }
        return nil
    }

    func addFeedListVO( _ feedListVO:FeedListVO ) {
        if feedListVO.feedSource == .listing {
            listingVOs[feedListVO.seasonGroupRef] = feedListVO
        }
        if feedListVO.feedSource == .specificTeam {
            teamSpecificVOs[feedListVO.seasonGroupRef] = feedListVO
        }
        if feedListVO.feedSource == .allTeam {
            teamAllVOs[feedListVO.seasonGroupRef] = feedListVO
        }
    }

    func removeAll( teamOnly:Bool = false ) {
        if !teamOnly {
            listingVOs.removeAll()
        }
        teamSpecificVOs.removeAll()
        teamAllVOs.removeAll()
    }
}
