import Foundation

class FixturePageVO: FeedPageVO {

    var columnVOs:[FixtureColumnVO] = []
    var rowVOs:[FixtureRowVO] = []
}
