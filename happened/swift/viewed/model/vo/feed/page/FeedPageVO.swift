import Foundation

class FeedPageVO {

    var prepared:Bool = false
    var dataRetrieved:Bool = false
    var hasNoEntries:Bool = false
    var firstEntryConstantIndex:Int = 0
    var pageIndex:Int

    init( pageIndex:Int ){
        self.pageIndex = pageIndex
    }

}
