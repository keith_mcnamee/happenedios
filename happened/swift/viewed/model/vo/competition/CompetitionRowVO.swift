import Foundation

class CompetitionRowVO {

    var competitionRef:Int
    var textVOs:[TextVO]
    var childCompetitionVOs:[CompetitionRowVO] = []
    var childListingVOs:[CompetitionListingRowVO] = []

    init( competitionRef:Int, textVOs:[TextVO] ){
        self.competitionRef = competitionRef
        self.textVOs = textVOs
    }

    var isListingParent:Bool {
        return childListingVOs.count > 0
    }
}
