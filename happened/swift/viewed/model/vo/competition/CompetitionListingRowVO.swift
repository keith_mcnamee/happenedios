import Foundation

class CompetitionListingRowVO {

    let listingRef:Int
    let listingSeasonRef:Int
    let textVOs:[TextVO]

    init( listingRef:Int, listingSeasonRef:Int, textVOs:[TextVO]){

        self.listingRef = listingRef
        self.listingSeasonRef = listingSeasonRef
        self.textVOs = textVOs
    }
}
