import Foundation

class SeasonGroupViewVO: RefTextViewVO {

    let nominalDate:Date

    init( ref:Int, text:String, nominalDate:Date ){

        self.nominalDate = nominalDate

        super.init( ref:ref, text:text )
    }
}
