import Foundation

class SeasonViewVO: RefTextViewVO {

    let startDate:Date
    let endDate:Date

    init( ref:Int, text:String, startDate:Date, endDate:Date ){

        self.startDate = startDate
        self.endDate = endDate

        super.init( ref:ref, text:text )
    }
}
