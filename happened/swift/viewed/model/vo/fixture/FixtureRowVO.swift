import Foundation

class FixtureRowVO {
    
    var prepared:Bool = false
    var columnTextVOs:[FixtureColumnType:[TextVO]] = [:]
    var attributes:[AppViewAttribute:Any] = [:]

}
