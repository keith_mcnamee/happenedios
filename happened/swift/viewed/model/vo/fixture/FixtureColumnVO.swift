import Foundation

class FixtureColumnVO {

    let columnType: FixtureColumnType

    var attributes:[AppViewAttribute:Any] = [:]

    init(columnType: FixtureColumnType ){
        self.columnType = columnType
    }
}
