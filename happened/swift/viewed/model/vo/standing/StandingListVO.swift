import Foundation

class StandingListVO {

    let columnVOs:[StandingColumnVO]
    let rowVOs:[StandingRowVO]

    let placeEventHeaderVOs:[TextVO]
    let placeEventRowVOs:[[TextVO]]

    let exemptionHeaderVOs:[TextVO]
    let exemptionRowVOs:[[TextVO]]

    let adjustmentAgainstTitleVOs:[TextVO]
    let adjustmentAgainstHeaderVOs:[TextVO]
    let adjustmentAgainstRowVOs:[[TextVO]]

    let adjustmentForTitleVOs:[TextVO]
    let adjustmentForHeaderVOs:[TextVO]
    let adjustmentForRowVOs:[[TextVO]]

    init( columnVOs:[StandingColumnVO], rowVOs:[StandingRowVO], placeEventHeaderVOs:[TextVO], placeEventRowVOs:[[TextVO]], exemptionHeaderVOs:[TextVO], exemptionRowVOs:[[TextVO]], adjustmentAgainstTitleVOs:[TextVO], adjustmentAgainstHeaderVOs:[TextVO], adjustmentAgainstRowVOs:[[TextVO]], adjustmentForTitleVOs:[TextVO], adjustmentForHeaderVOs:[TextVO], adjustmentForRowVOs:[[TextVO]] ){
        self.columnVOs = columnVOs
        self.rowVOs = rowVOs

        self.placeEventHeaderVOs = placeEventHeaderVOs
        self.placeEventRowVOs = placeEventRowVOs

        self.exemptionHeaderVOs = exemptionHeaderVOs
        self.exemptionRowVOs = exemptionRowVOs

        self.adjustmentAgainstTitleVOs = adjustmentAgainstTitleVOs
        self.adjustmentAgainstHeaderVOs = adjustmentAgainstHeaderVOs
        self.adjustmentAgainstRowVOs = adjustmentAgainstRowVOs

        self.adjustmentForTitleVOs = adjustmentForTitleVOs
        self.adjustmentForHeaderVOs = adjustmentForHeaderVOs
        self.adjustmentForRowVOs = adjustmentForRowVOs
    }
}
