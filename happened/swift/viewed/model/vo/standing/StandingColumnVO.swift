import Foundation

class StandingColumnVO {

    let columnType: StandingColumnType
    let header:[TextVO]
    let description:[TextVO]

    var attributes:[AppViewAttribute:Any] = [:]

    init(columnType: StandingColumnType, header:[TextVO], description:[TextVO] ){
        self.columnType = columnType
        self.header = header
        self.description = description
    }

}
