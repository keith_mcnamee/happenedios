import Foundation

class ScenarioStateViewVO {

    let calculatorVO:CalculatorVO
    let teamRef:Int
    let fromDate:Date?
    let toDate:Date?
    let title:String
    let descendingDate:Bool
    let onlyIncludeRefs:[Int:NSNull]?
    let doNotIncludeRefs:[Int:NSNull]?

    var standingVO:StandingListVO? = nil

    var allFixturePageVOs:[FixturePageVO] = []
    var teamFixturePageVOs:[FixturePageVO] = []

    init( calculatorVO:CalculatorVO, teamRef:Int, fromDate:Date?, toDate:Date?, title:String, descendingDate:Bool, onlyIncludeRefs:[Int:NSNull]?, doNotIncludeRefs:[Int:NSNull]? ){

        self.calculatorVO = calculatorVO
        self.teamRef = teamRef
        self.fromDate = fromDate
        self.toDate = toDate
        self.title = title
        self.descendingDate = descendingDate
        self.onlyIncludeRefs = onlyIncludeRefs
        self.doNotIncludeRefs = doNotIncludeRefs

    }

}
