import Foundation

class ScenarioViewVO {

    var stateVOs:[ScenarioStateViewVO] = []
    var selectedStateIndex:Int = 0

    func addState(_ stateVO:ScenarioStateViewVO, isSelectedState:Bool = false ){
        stateVOs.append(stateVO)
        if isSelectedState {
            selectedStateIndex = stateVOs.count - 1
        }
    }
}
