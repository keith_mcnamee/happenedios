import Foundation

class TextVO {

    var text:String
    var replaceID:String?
    var attributes:[AppViewAttribute:Any] = [:]

    init(_ text:String, replaceID:String?=nil, attributes:[AppViewAttribute:Any]?=nil){

        self.text = text
        self.replaceID = replaceID
        self.attributes = attributes != nil ? attributes! : self.attributes
    }

    func birthClone(withAttributes:[AppViewAttribute:Any]?=nil) -> TextVO {
        var thisAttributes:[AppViewAttribute:Any] = [:]
        for (key, value) in attributes {
            thisAttributes[key] = value
        }
        if withAttributes != nil {
            for (key, value) in withAttributes! {
                thisAttributes[key] = value
            }
        }
        return TextVO(text, replaceID: replaceID, attributes: thisAttributes)
    }

    var retrieveID:String {
        if replaceID != nil {
            return replaceID!
        }
        if attributes[.action] != nil {
            if attributes[.action] is String {
                return attributes[.action]! as! String
            }
            if attributes[.action] is Int {
                return String(attributes[.action]! as! Int)
            }
        }
        return text
    }

}
