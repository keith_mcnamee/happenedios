import Foundation

class FixtureFeedViewModel {

    var containerFeedListVO: ContainerFeedListVO = ContainerFeedListVO()

    func reset() {
        containerFeedListVO.removeAll()
    }
}
