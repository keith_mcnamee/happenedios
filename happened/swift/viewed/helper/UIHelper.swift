import UIKit

class UIHelper {

    func sidesAttributes(_ value:Any? ) -> [AppViewAttribute:Any] {
        if value == nil {
            return [:]
        }
        let allSides:[AppViewAttribute:Any] = [.top:NSNull(), .bottom:NSNull(), .left:NSNull(), .right:NSNull()]
        let attributes:[AppViewAttribute:Any]? = value as? [AppViewAttribute:Any]
        if attributes == nil {
            return allSides
        }
        let sidesAttributes:[AppViewAttribute:Any]? = attributes![.sides] as? [AppViewAttribute:Any]
        if sidesAttributes == nil {
            return allSides
        }
        var thisSides:[AppViewAttribute:Any] = [:]
        if sidesAttributes![.top] != nil {
            thisSides[.top] = sidesAttributes![.top]
        }
        if sidesAttributes![.bottom] != nil {
            thisSides[.bottom] = sidesAttributes![.bottom]
        }
        if sidesAttributes![.left] != nil {
            thisSides[.left] = sidesAttributes![.left]
        }
        if sidesAttributes![.right] != nil {
            thisSides[.right] = sidesAttributes![.right]
        }
        return thisSides
    }

    func lineStyleAttribute(_ value:Any? ) -> AppViewAttribute {
        if value == nil {
            return .none
        }
        let attributes:[AppViewAttribute:Any]? = value as? [AppViewAttribute:Any]
        if attributes == nil {
            return .regular
        }
        let styleAttributes:[AppViewAttribute:Any]? = attributes![.style] as? [AppViewAttribute:Any]
        if styleAttributes == nil {
            return .regular
        }
        if styleAttributes![.dashed] != nil {
            return .dashed
        }
        return .regular
    }

    func textAlignAttribute(_ value:Any?, defaultAttribute:NSTextAlignment?=nil ) -> NSTextAlignment? {

        if value == nil {
            return defaultAttribute
        }
        let attributes:[AppViewAttribute:Any]? = value as? [AppViewAttribute:Any]
        if attributes == nil {
            return defaultAttribute
        }
        let alignmentAttributes:[NSTextAlignment:Any]? = attributes![.textAlignment] as? [NSTextAlignment:Any]
        if alignmentAttributes == nil {
            return defaultAttribute
        }
        if alignmentAttributes![.left] != nil {
            return .left
        }
        if alignmentAttributes![.right] != nil {
            return .right
        }
        if alignmentAttributes![.center] != nil {
            return .center
        }
        return defaultAttribute
    }

    func pointSizeAttribute(_ value:Any?, defaultAttribute:CGFloat?=nil ) -> CGFloat? {

        if value == nil {
            return defaultAttribute
        }
        let attributes:[AppViewAttribute:Any]? = value as? [AppViewAttribute:Any]
        if attributes == nil {
            return defaultAttribute
        }
        let pointSizeAttribute:[AppViewAttribute:Any]? = attributes![.pointSize] as? [AppViewAttribute:Any]
        if pointSizeAttribute == nil {
            return defaultAttribute
        }
        let pointSize:CGFloat? = CGFloat.any(pointSizeAttribute)
        if pointSize != nil {
            return pointSize!
        }
        return defaultAttribute
    }

    func backgroundColorAttribute(_ value:Any?, defaultAttribute:UIColor?=nil ) -> UIColor? {

        if value == nil {
            return defaultAttribute
        }
        let attributes:[AppViewAttribute:Any]? = value as? [AppViewAttribute:Any]
        if attributes == nil {
            return defaultAttribute
        }
        let backgroundColorAttribute:UIColor? = attributes![.backgroundColor] as? UIColor
        if backgroundColorAttribute != nil {
            return backgroundColorAttribute!
        }
        return defaultAttribute
    }

    var maxScreenSize:CGFloat {
        let screenSize:CGSize = UIScreen.main.bounds.size
        return max( screenSize.width, screenSize.height)
    }

    func addHorizontalSpacer(toView:UIView, leadingToItem:UIView, leadingToAttribute:NSLayoutConstraint.Attribute, primarySpacer:UIView?=nil, maxWidth:CGFloat?=nil, fillWithLowPriority:Bool=false ) -> AppView {

        let spacer:AppView = AppView()
        spacer.backgroundColor = .clear
        spacer.isUserInteractionEnabled = false
        toView.addSubview(spacer)

        let topConstraint:NSLayoutConstraint = NSLayoutConstraint(item: spacer, attribute: .top, relatedBy: .equal, toItem: toView, attribute: .top, multiplier: 1, constant: 0)
        let leadingConstraint:NSLayoutConstraint =  NSLayoutConstraint(item: spacer, attribute: .leading, relatedBy: .equal, toItem: leadingToItem, attribute: leadingToAttribute, multiplier: 1, constant: 0)
        let bottomConstraint:NSLayoutConstraint = NSLayoutConstraint(item: spacer, attribute: .bottom, relatedBy: .equal, toItem: toView, attribute: .bottom, multiplier: 1, constant: 0)
        var widthConstraint:NSLayoutConstraint? = nil
        if primarySpacer == nil {
            let useMaxWidth:CGFloat = maxWidth != nil ? maxWidth! : maxScreenSize
            widthConstraint = NSLayoutConstraint(item: spacer, attribute: .width, relatedBy: .lessThanOrEqual, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: useMaxWidth)

            let lowPriorityWidth:CGFloat = fillWithLowPriority ? useMaxWidth : 0
            let altWidthConstraint:NSLayoutConstraint = NSLayoutConstraint(item: spacer, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: lowPriorityWidth)
            altWidthConstraint.priority = UILayoutPriority.defaultLow
            toView.addConstraint(altWidthConstraint)

        } else {
            widthConstraint = NSLayoutConstraint(item: spacer, attribute: .width, relatedBy: .equal, toItem: primarySpacer!, attribute: .width, multiplier: 1, constant: 0)
        }

        spacer.addAppConstraint(topConstraint, toView)
        spacer.addAppConstraint(leadingConstraint, toView)
        spacer.addAppConstraint(bottomConstraint, toView)
        spacer.addAppConstraint(widthConstraint!, toView)

        return spacer
    }

    func addVerticalSpacer(toView:UIView, topToItem:UIView, topToAttribute:NSLayoutConstraint.Attribute, primarySpacer:UIView?=nil, maxHeight:CGFloat?=nil, fillWithLowPriority:Bool=false ) -> AppView {

        let spacer:AppView = AppView()
        spacer.backgroundColor = .clear
        spacer.isUserInteractionEnabled = false
        toView.addSubview(spacer)

        let topConstraint:NSLayoutConstraint = NSLayoutConstraint(item: spacer, attribute: .top, relatedBy: .equal, toItem: topToItem, attribute: topToAttribute, multiplier: 1, constant: 0)
        let leadingConstraint:NSLayoutConstraint =  NSLayoutConstraint(item: spacer, attribute: .leading, relatedBy: .equal, toItem: toView, attribute: .leading, multiplier: 1, constant: 0)
        let trailingConstraint:NSLayoutConstraint = NSLayoutConstraint(item: spacer, attribute: .trailing, relatedBy: .equal, toItem: toView, attribute: .trailing, multiplier: 1, constant: 0)
        var heightConstraint:NSLayoutConstraint? = nil
        if primarySpacer == nil {
            let useMaxHeight:CGFloat = maxHeight != nil ? maxHeight! : maxScreenSize
            heightConstraint = NSLayoutConstraint(item: spacer, attribute: .height, relatedBy: .lessThanOrEqual, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: useMaxHeight)

            let lowPriorityHeight:CGFloat = fillWithLowPriority ? useMaxHeight : 0
            let altHeightConstraint:NSLayoutConstraint = NSLayoutConstraint(item: spacer, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: lowPriorityHeight)
            altHeightConstraint.priority = UILayoutPriority.defaultLow
            toView.addConstraint(altHeightConstraint)

        } else {
            heightConstraint = NSLayoutConstraint(item: spacer, attribute: .height, relatedBy: .equal, toItem: primarySpacer!, attribute: .height, multiplier: 1, constant: 0)
        }

        spacer.addAppConstraint(topConstraint, toView)
        spacer.addAppConstraint(leadingConstraint, toView)
        spacer.addAppConstraint(trailingConstraint, toView)
        spacer.addAppConstraint(heightConstraint!, toView)

        return spacer
    }

    func getAppConstraint( _ type: AppConstraint, _ forView:ExtAppView ) -> NSLayoutConstraint? {
        switch type {
        case .height:
            return forView._heightConstraint
        case .width:
            return forView._widthConstraint
        case .top:
            return forView._topConstraint
        case .bottom:
            return forView._bottomConstraint
        case .leading:
            return forView._leadingConstraint
        case .trailing:
            return forView._trailingConstraint
        case .centerX:
            return forView._centerXConstraint
        case .centerY:
            return forView._centerYConstraint
        }
    }

    func addAppConstraint(_ constraint:NSLayoutConstraint, _ forView:ExtAppView, _ toView:UIView?=nil, _ type: AppConstraint? = nil ){
        var useType:AppConstraint? = type

        let uiView:UIView = forView as! UIView

        var useAttribute:NSLayoutConstraint.Attribute? = nil
        if  constraint.firstItem as? UIView == uiView {
            useAttribute = constraint.firstAttribute
        } else if constraint.secondItem != nil && constraint.secondItem! as? UIView == uiView {
            useAttribute = constraint.secondAttribute
        }


        if useAttribute == nil {
            return
        }

        if useType == nil {
            switch useAttribute! {
            case .height:
                useType = .height
            case .width:
                useType = .width
            case .top:
                useType = .top
            case .bottom:
                useType = .bottom
            case .leading:
                useType = .leading
            case .trailing:
                useType = .trailing
            case .centerX:
                useType = .centerX
            case .centerY:
                useType = .centerY
            default:
                break
            }
        }

        if useType == nil {
            return
        }

        removeAppConstraint(useType!, forView)
        let useToView:UIView = toView != nil ? toView! : uiView
        useToView.addConstraint(constraint)
        switch useType! {
        case .height:
            forView._heightConstraint = constraint
        case .width:
            forView._widthConstraint = constraint
        case .top:
            forView._topConstraint = constraint
        case .bottom:
            forView._bottomConstraint = constraint
        case .leading:
            forView._leadingConstraint = constraint
        case .trailing:
            forView._trailingConstraint = constraint
        case .centerX:
            forView._centerXConstraint = constraint
        case .centerY:
            forView._centerYConstraint = constraint
        }
    }

    func removeAppConstraint( _ type: AppConstraint, _ forView:ExtAppView ) {
        let uiView:UIView = forView as! UIView
        let constraint:NSLayoutConstraint? = getAppConstraint(type, forView)
        if constraint == nil {
            return
        }
        var superview:UIView? = uiView
        while superview != nil {
            for checkConstraint:NSLayoutConstraint in superview!.constraints {
                if checkConstraint == constraint {
                    superview!.removeConstraint(checkConstraint)
                    return
                }
            }
            superview = superview!.superview
        }
        switch type {
        case .height:
            forView._heightConstraint = nil
        case .width:
            forView._widthConstraint = nil
        case .top:
            forView._topConstraint = nil
        case .bottom:
            forView._bottomConstraint = nil
        case .leading:
            forView._leadingConstraint = nil
        case .trailing:
            forView._trailingConstraint = nil
        case .centerX:
            forView._centerXConstraint = nil
        case .centerY:
            forView._centerYConstraint = nil
        }
    }
}
