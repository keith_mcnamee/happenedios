import UIKit

class ViewHelper {

    private var app:App { return App.instance }
    private var balancing:Balancing { return Balancing.instance }
    private var happen: Happen { return Happen.instance }
    private var localization:Localization { return Localization.instance }
    private var viewed: Viewed { return Viewed.instance }

    func getSeasonGroupViewVOs( _ seasonGroupRefs:[Int:Any]?=nil ) -> [SeasonGroupViewVO] {
        let thisSeasonGroupRefs:[Int:Any] = seasonGroupRefs != nil ? seasonGroupRefs! : happen.happenedModel.seasonGroupVOs
        var viewVOs:[SeasonGroupViewVO] = []
        for seasonGroupRef:Int in thisSeasonGroupRefs.keys {
            if let viewVO:SeasonGroupViewVO = getSeasonGroupViewVO( seasonGroupRef ) {
                viewVOs.append(viewVO)
            }
        }
        viewVOs.sort {
            if $0.nominalDate > $1.nominalDate {
                return true
            }
            if $0.nominalDate < $1.nominalDate {
                return false
            }
            if $0.text > $1.text {
                return true
            }
            if $0.text < $1.text {
                return false
            }
            return false
        }
        return viewVOs
    }

    func getSeasonViewVOs( _ seasonRefs:[Int:Any]?=nil ) -> [SeasonViewVO] {
        let thisSeasonRefs:[Int:Any] = seasonRefs != nil ? seasonRefs! : happen.happenedModel.seasonVOs
        var viewVOs:[SeasonViewVO] = []
        for seasonRef:Int in thisSeasonRefs.keys {
            if let viewVO:SeasonViewVO = getSeasonViewVO( seasonRef ) {
                viewVOs.append(viewVO)
            }
        }
        viewVOs.sort {
            if $0.startDate > $1.startDate {
                return true
            }
            if $0.startDate < $1.startDate {
                return false
            }
            if $0.endDate > $1.endDate {
                return true
            }
            if $0.endDate < $1.endDate {
                return false
            }
            if $0.text > $1.text {
                return true
            }
            if $0.text < $1.text {
                return false
            }
            return false
        }
        return viewVOs
    }

    func getListingSeasonViewVOs(_ listingSeasonRefs:[Int:Any] ) -> [RefTextViewVO] {
        var listingSeasonVOs:[ListingSeasonVO] = []
        var listingRefs:[Int:NSNull] = [:]
        for listingSeasonRef:Int in listingSeasonRefs.keys {
            let listingSeasonVO:ListingSeasonVO = happen.happenedModel.listingSeasonVOs[listingSeasonRef]!
            listingSeasonVOs.append(listingSeasonVO)
            listingRefs[listingSeasonVO.listingRef] = NSNull()
        }
        listingSeasonVOs = happen.happenedHelper.orderListingSeasonVOs( listingSeasonVOs, byShowPriority:true )
        var viewVOs:[RefTextViewVO] = []
        for listingSeasonVO:ListingSeasonVO in listingSeasonVOs {
            if let viewVO: RefTextViewVO = getListingSeasonViewVO(listingSeasonVO.ref, avoidMatchListingRefs: listingRefs) {
                viewVOs.append(viewVO)
            }
        }
        return viewVOs
    }

    func getSeasonGroupViewVO( _ seasonGroupRef:Int ) -> SeasonGroupViewVO? {
        if let viewVO:SeasonGroupViewVO = viewed.viewModel.seasonGroupVOs[seasonGroupRef] {
            return viewVO
        }
        let balancingVO:SeasonGroupBalancingVO? = balancing.balancingModel.seasonGroupVOs[seasonGroupRef]
        if balancingVO == nil {
            return nil
        }
        let text:String = localization.localizationHelper.getSeasonGroupName( seasonGroupRef )
        return SeasonGroupViewVO(ref: seasonGroupRef, text: text, nominalDate: balancingVO!.nominalDate!)
    }

    func getSeasonViewVO( _ seasonRef:Int ) -> SeasonViewVO?  {
        if let viewVO:SeasonViewVO = viewed.viewModel.seasonVOs[seasonRef] {
            return viewVO
        }
        let balancingVO:SeasonBalancingVO? = balancing.balancingModel.seasonVOs[seasonRef]
        if balancingVO == nil {
            return nil
        }
        let text:String = localization.localizationHelper.getSeasonName( seasonRef )
        return SeasonViewVO(ref: seasonRef, text: text, startDate:balancingVO!.startDate!, endDate:balancingVO!.endDate! )
    }

    func getListingSeasonViewVO( _ listingSeasonRef:Int, avoidMatchListingRefs:[Int:Any] ) -> RefTextViewVO? {
        if let viewVO:RefTextViewVO = viewed.viewModel.listingSeasonVOs[listingSeasonRef] {
            return viewVO
        }
        let listingSeasonVO:ListingSeasonVO? = happen.happenedModel.listingSeasonVOs[listingSeasonRef]
        if listingSeasonVO == nil {
            return nil
        }
        let text:String = localization.localizationHelper.getAppropriateListingCompetitionName(listingSeasonVO!.listingRef, seasonRef: listingSeasonVO!.seasonRef, avoidMatchListingRefs: avoidMatchListingRefs)
        return RefTextViewVO(ref: listingSeasonRef, text: text )
    }

    func getPreferredListingRef(seasonRef:Int, listingRef:Int, teamRef:Int ) -> Int? {
        let teamListingVO:TeamListingVO? = happen.happenedModel.getTeamListingVO(seasonRef, listingRef, teamRef)
        if teamListingVO != nil {
            return listingRef
        }
        let teamVO:TeamVO? = happen.happenedModel.teamVOs[teamRef]
        if teamVO == nil {
            return nil
        }
        let teamSeasonVO:TeamSeasonVO? = teamVO!.teamSeasonVOs[seasonRef]
        if teamSeasonVO == nil {
            return nil
        }
        let competitionVO:CompetitionVO? = happen.happenedHelper.getNextFocalAncestor( seasonRef: seasonRef, listingRef:listingRef )
        if competitionVO == nil {
            return nil
        }
        var matchListingRefs:[Int:NSNull] = [:]
        var unMatchListingRefs:[Int:NSNull] = [:]
        for checkListingRef:Int in teamSeasonVO!.teamListingRefs.keys {
            let checkCompetitionVO:CompetitionVO? = happen.happenedHelper.getNextFocalAncestor( seasonRef: seasonRef, listingRef:checkListingRef )
            if checkCompetitionVO != nil {
                if checkCompetitionVO!.balancingVO.parentCompetitionRef == competitionVO!.balancingVO.parentCompetitionRef {
                    matchListingRefs[checkListingRef] = NSNull()
                } else {
                    unMatchListingRefs[checkListingRef] = NSNull()
                }
            }
        }
        if matchListingRefs.count == 0 {
            matchListingRefs = unMatchListingRefs
        }

        var prioritisedRefs:[Int] = happen.happenedHelper.listingPriority( matchListingRefs, seasonRef:seasonRef )
        if prioritisedRefs.count == 0 {
            return nil
        }
        return prioritisedRefs[0]
    }

    func availableListingSeasonRefs( seasonGroupRef:Int?=nil, listingRefs:[Int:Any]? = nil ) -> [Int] {

        var listingSeasonRefs:[Int:NSNull] = [:]
        let useListingRefs:[Int:Any] = listingRefs != nil ? listingRefs! : happen.happenedModel.listingVOs
        for listingRef:Int in useListingRefs.keys {

            let seasonRefs:[Int: NSNull] = seasonGroupRef == nil ? balancing.balancingModel.getDefaultVO()!.seasonRefs! : balancing.balancingHelper.getAllSeasonRefs(seasonGroupRef: seasonGroupRef)

            for checkSeasonRef:Int in seasonRefs.keys {
                if let listingSeasonVO = happen.happenedModel.getListingSeasonVO(checkSeasonRef, listingRef) {
                    listingSeasonRefs[ listingSeasonVO.ref ] = NSNull()
                }
            }
        }

        return happen.happenedHelper.listingSeasonPriority( listingSeasonRefs )

    }

    func createFilterCompetitionListValues(seasonGroupRef:Int?=nil, listingRefs:[Int:Any]? = nil ) -> ([Int:IntDicVOsVO], [TextVO] ) {

        let prioritisedListingSeasonRefs:[Int] = availableListingSeasonRefs( seasonGroupRef:seasonGroupRef, listingRefs:listingRefs )
        var competitionRefsOrder:[Int] = []
        var listingRefsByCompetitionRef:[Int:IntDicVOsVO] = [:]

        for listingSeasonRef:Int in prioritisedListingSeasonRefs {
            let listingSeasonVO:ListingSeasonVO = happen.happenedModel.listingSeasonVOs[listingSeasonRef]!
            let focalCompetitionRef:Int? = happen.happenedHelper.getFocalCompetitionRef( listingRef:listingSeasonVO.listingRef, seasonRef:listingSeasonVO.seasonRef )
            if focalCompetitionRef == nil {
                continue
            }
            if listingRefsByCompetitionRef[ focalCompetitionRef! ] == nil {
                listingRefsByCompetitionRef[ focalCompetitionRef! ] = IntDicVOsVO()
                competitionRefsOrder.append(focalCompetitionRef!)
            }
            listingRefsByCompetitionRef[ focalCompetitionRef! ]!.vos[ listingSeasonVO.listingRef ] = NSNull()
        }

        var competitionTextVOs:[TextVO] = []

        for competitionRef:Int in competitionRefsOrder {
            let competitionVO:CompetitionVO = happen.happenedModel.getCompetitionVO(competitionRef, seasonGroupRef: seasonGroupRef)!
            var competitionParentRef:Int? = competitionVO.balancingVO.parentCompetitionRef
            if competitionParentRef != nil{
                var matched = false
                for compareCompetitionRef:Int in competitionRefsOrder {
                    if compareCompetitionRef == competitionRef {
                        continue
                    }
                    var thisCompareCompetitionRef:Int? = compareCompetitionRef
                    var level:Int = -1
                    while thisCompareCompetitionRef != nil {
                        level += 1
                        if thisCompareCompetitionRef == competitionParentRef && level >= 2 {
                            matched = true
                            break
                        }
                        let compareCompetitionVO = happen.happenedModel.getCompetitionVO(thisCompareCompetitionRef!, seasonGroupRef: seasonGroupRef)!
                        thisCompareCompetitionRef = compareCompetitionVO.balancingVO.parentCompetitionRef
                    }
                    if matched {
                        break
                    }
                }
                if matched {
                    competitionParentRef = nil
                }
            }
            var competitionName:String = localization.localizationHelper.getCompetitionName( competitionRef, seasonRef: balancing.balancingHelper.getBestSeasonRef( seasonGroupRef: seasonGroupRef ) )
            if competitionParentRef != nil {
                competitionName += " ( " + localization.localizationHelper.getCompetitionName( competitionParentRef!, seasonRef: balancing.balancingHelper.getBestSeasonRef( seasonGroupRef: seasonGroupRef ) ) + " ) "
            }
            competitionTextVOs.append(TextVO(competitionName, attributes: [.action: competitionRef]))
        }
        return (listingRefsByCompetitionRef, competitionTextVOs)
    }

    func createPrimaryCompetitionTextVOs( seasonRef:Int, listingRef:Int ) -> [TextVO] {
        if let listingSeasonVO:ListingSeasonVO = happen.happenedModel.getListingSeasonVO(seasonRef, listingRef) {
            let primaryNames:[String] = localization.localizationHelper.getPrimaryCompetitionNames(seasonRef: seasonRef, listingRef: listingRef)
            let replaceWith:[String:String] = [
                    "competition1": primaryNames[0],
                    "competition2": primaryNames[1]
            ]
            let actions:[String:Int]  = [
                    "competition2": listingSeasonVO.ref
            ]
            let requestTypes:[String:String]  = [
                    "competition2": AppConstant.SHOW_LISTING_REQUEST
            ]
            let textVOs:[TextVO] = localization.localizationHelper.localizeVOs(
                    "competition1_dash_competition2",
                    replaceWith,
                    requestTypes:requestTypes,
                    actions: actions,
                    localizationType: LocalizationConstant.NATIVE_TYPE)!
            return textVOs
        }
        return []
    }

    func sortEventBalancingVOs( _ eventBalancingVOs:[EventBalancingVO], weightEventRefs:[Int:Any]?=nil ) -> [EventBalancingVO] {
        let sorted:[EventBalancingVO] = eventBalancingVOs.sorted {
            if weightEventRefs != nil {
                if( weightEventRefs![ $0.ref ] != nil && weightEventRefs![ $1.ref ] == nil )
                {
                    return true
                }
                if( weightEventRefs![ $1.ref ] != nil && weightEventRefs![ $0.ref ] == nil )
                {
                    return false
                }
            }

            if( $0.positive!.intValue < $1.positive!.intValue )
            {
                return false
            }
            if( $0.positive!.intValue > $1.positive!.intValue )
            {
                return true
            }
            let factor = $0.positive!.intValue
            let aPriority = $0.priority! * factor
            let bPriority = $1.priority! * factor
            if( aPriority > bPriority )
            {
                return false
            }
            if( aPriority < bPriority )
            {
                return true
            }
            let aName = localization.localizationHelper.getEventName($0.ref, seasonRef: $0.seasonRef)
            let bName = localization.localizationHelper.getEventName($1.ref, seasonRef: $1.seasonRef)
            return aName < bName
        }
        return sorted
    }

    func constructFilterString( _ filterVO:FeedFilterVO ) -> String {
        var listingString:String = ""

        if !filterVO.allCompetitionSelected {
            for listingRef:Int in filterVO.listingRefs.keys {
                if listingString == "" {
                    listingString = "___" + ViewConstant.LEAGUE + "__"
                } else {
                    listingString += "_"
                }
                listingString += String(listingRef)
            }
        }
        return "v" + listingString
    }

    func constructEventFilterString( _ filterVO:EventFeedFilterVO ) -> String {
        let listingString:String = constructFilterString(filterVO)
        var eventString:String  = ""
        var toHappenString:String  = ""

        if !filterVO.allEventSelected {
            for eventRef:Int in filterVO.eventRefs.keys {
                if eventString == "" {
                    eventString = "___" + ViewConstant.EVENT + "__"
                } else {
                    eventString += "_"
                }
                eventString += String(eventRef)
            }
        }
        if filterVO.toHappenState != .both {
            toHappenString = "___" + ViewConstant.TO_HAPPEN + "__" + ( filterVO.toHappenState == .toHappenOnly ? "1" : "2" )
        }
        return "v"+listingString + eventString + toHappenString
    }

    func constructTextVOsString( _ textVOs:[TextVO] ) -> String {
        var string:String = ""
        for textVO:TextVO in textVOs {
            string += textVO.text
        }
        return string
    }

    func cloneTextVOs( _ textVOs:[TextVO], withAttributes:[AppViewAttribute:Any]?=nil, cloneIfAttributes:[AppViewAttribute]?=nil) -> [TextVO] {
        var returnVOs:[TextVO] = []
        for textVO:TextVO in textVOs {
            var doClone:Bool = true
            if cloneIfAttributes != nil {
                for attribute:AppViewAttribute in cloneIfAttributes! {
                    if textVO.attributes[attribute] == nil {
                        doClone = false
                        break
                    }
                }
            }
            if !doClone {
                returnVOs.append(textVO)
            } else {
                returnVOs.append(textVO.birthClone(withAttributes: withAttributes))
            }
        }
        return returnVOs
    }

    func nextUnplayedFixtureDate( _ inFixtureVOs:[Int:FixtureVO] ) -> Date? {
        var date:Date? = nil;
        for fixtureVO:FixtureVO in inFixtureVOs.values {
            if fixtureVO.resultVO.played {
                continue;
            }
            if( date == nil || fixtureVO.sortingDate < date! )
            {
                date = fixtureVO.sortingDate;
            }
        }
        return date;
    }

    func getSortedEventBalancingVOs( seasonRef:Int, listingRef:Int, includeDuplicates:Bool=false, weightEventRefs:[Int:Any]?=nil ) -> [EventBalancingVO] {
        var eventBalancingVOs:[EventBalancingVO] = []
        let listingSeasonVO:ListingSeasonVO = happen.happenedModel.getListingSeasonVO( seasonRef, listingRef )!

        let maximumPlaceEventRefs = listingSeasonVO.calculatorVO.maximumPlaceEventRefs

        for eventRef:Int in listingSeasonVO.eventListingVOs.keys {
            let eventBalancingVO:EventBalancingVO = balancing.balancingModel.getEventVO(eventRef, seasonRef: seasonRef)!

            var matched:Bool = false
            if !includeDuplicates {
                for checkEventRef:Int in maximumPlaceEventRefs.keys {
                    if( eventRef == checkEventRef )
                    {
                        continue
                    }

                    let checkEventBalancingVO = balancing.balancingModel.getEventVO( checkEventRef, seasonRef: seasonRef )!
                    if checkEventBalancingVO.positive! == eventBalancingVO.positive! &&
                               checkEventBalancingVO.priority! < eventBalancingVO.priority! &&
                            app.arrayHelper.hasMatchingKeys( listingSeasonVO.calculatorVO.maximumAllToPlaceEventRefs[ checkEventRef ]!.vos, listingSeasonVO.calculatorVO.maximumAllToPlaceEventRefs[ eventRef ]!.vos )
                    {
                        var exemptionOK:Bool = true
                        for checkTeamRef:Int in listingSeasonVO.teamListingRefs.keys {
                            let eventTeamAVO:EventTeamVO? = happen.happenedModel.getEventTeamVO( seasonRef, listingRef, checkTeamRef, eventBalancingVO.ref )
                            let eventTeamBVO:EventTeamVO? = happen.happenedModel.getEventTeamVO( seasonRef, listingRef, checkTeamRef, checkEventBalancingVO.ref )
                            let exemptionTeamAVO:ExemptionTeamVO? = eventTeamAVO != nil && eventTeamAVO!.exemptionRef != nil ? happen.happenedModel.exemptionTeamVOs[eventTeamAVO!.exemptionRef!] : nil
                            let exemptionTeamBVO:ExemptionTeamVO?  = eventTeamBVO != nil && eventTeamBVO!.exemptionRef != nil ? happen.happenedModel.exemptionTeamVOs[eventTeamBVO!.exemptionRef!] : nil
                            if( exemptionTeamAVO == nil && exemptionTeamBVO == nil ) {
                                //ok
                            } else if( (exemptionTeamAVO != nil && exemptionTeamBVO != nil) && (exemptionTeamAVO!.appliedDate == exemptionTeamBVO!.appliedDate && exemptionTeamAVO!.eventReceivedByListing == exemptionTeamBVO!.eventReceivedByListing ) ){
                                //ok
                            } else {
                                exemptionOK = false
                            }
                        }
                        if( exemptionOK ){
                            matched = true
                            break
                        }
                    }
                }
            }
            if( !matched || includeDuplicates )
            {
                eventBalancingVOs.append( balancing.balancingModel.getEventVO(eventRef, seasonRef: seasonRef)! )
            }
        }

        return sortEventBalancingVOs( eventBalancingVOs, weightEventRefs:weightEventRefs )

    }
}
