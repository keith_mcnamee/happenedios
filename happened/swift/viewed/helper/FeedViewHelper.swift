import Foundation

class FeedViewHelper {

    private var balancing:Balancing { return Balancing.instance }
    private var viewed: Viewed { return Viewed.instance }

    func getFeedVO( feedType:FeedType, feedSource:FeedSource, seasonGroupRef:Int?=nil, isHappenedState: IsHappenedState = .happenedOnly, filterVO: FeedFilterVO?=nil, createIfNotMatched:Bool=false) -> FeedViewVO? {

        let feedListVO:FeedListVO? = getFeedListVO(feedType: feedType, feedSource: feedSource, seasonGroupRef: seasonGroupRef, createIfNotMatched:createIfNotMatched)
        if feedListVO == nil {
            return nil
        }

        let isFiltered:Bool = filterVO?.filterString != nil
        var vo:FeedViewVO? = feedListVO!.getFeedVO(isUpcoming: isHappenedState == .upcomingOnly, isFiltered: isFiltered)
        if vo == nil || vo!.filterVO?.filterString != filterVO?.filterString {
            if !createIfNotMatched {
                return nil
            }
            vo = FeedViewVO(feedType:feedListVO!.feedType, feedSource:feedListVO!.feedSource, seasonGroupRef: feedListVO!.seasonGroupRef, isHappenedState:isHappenedState, filterVO: filterVO)
            feedListVO!.addFeedVO(vo!)
        }
        return vo!
    }

    func getFeedListVO( feedType:FeedType, feedSource:FeedSource, seasonGroupRef:Int?=nil, createIfNotMatched:Bool=false ) -> FeedListVO? {

        let useSeasonGroupRef:Int = seasonGroupRef != nil ? seasonGroupRef! : balancing.balancingModel.getDefaultVO()!.seasonGroupRef!
        let containerFeedListVO:ContainerFeedListVO = getContainerFeedListVO( feedType: feedType )

        var feedListVO:FeedListVO? = containerFeedListVO.getFeedListVO(feedSource: feedSource, seasonGroupRef: useSeasonGroupRef)
        if feedListVO == nil {
            if !createIfNotMatched {
                return nil
            }
            feedListVO = FeedListVO( feedType: feedType, feedSource: feedSource, seasonGroupRef:useSeasonGroupRef )
            containerFeedListVO.addFeedListVO( feedListVO! )
        }
        return feedListVO
    }

    func haveFeedEntries( _ feedViewVO:FeedViewVO, entriesSpan:Int, entriesFrom:Int=0) -> Bool {
        for i in entriesFrom..<entriesFrom + entriesSpan {
            if i >= feedViewVO.entryVOs.count {
                if feedViewVO.endOfList {
                    return true
                }
                return false
            }
            let entryVO:FeedEntryVO = feedViewVO.entryVOs[i]
            if !entryVO.requested {
                return false
            }
        }
        return true
    }

    func getContainerFeedListVO( feedType:FeedType ) -> ContainerFeedListVO {
        switch feedType {
            case .event:
                return viewed.eventFeedViewModel.containerFeedListVO

            case .fixture:
                return viewed.fixtureFeedViewModel.containerFeedListVO
        }
    }

    func haveFeedData( feedType:FeedType, feedSource:FeedSource, seasonGroupRef:Int?=nil, filterVO: FeedFilterVO?=nil) -> Bool {

        let feedListVO:FeedListVO? = getFeedListVO(feedType: feedType, feedSource:feedSource, seasonGroupRef: seasonGroupRef)
        if feedListVO == nil {
            return false
        }

        let isFiltered:Bool = filterVO?.filterString != nil
        let primaryVO:FeedViewVO? = feedListVO!.getFeedVO(isUpcoming: !viewed.viewConfig.homeDefaultIsHappened, isFiltered: isFiltered)
        if primaryVO == nil || primaryVO!.filterVO?.filterString != filterVO?.filterString {
            return false
        }
        return true
    }

    func primaryIsHappened( feedType:FeedType, feedSource:FeedSource, seasonGroupRef:Int?=nil, filterVO: FeedFilterVO?=nil) -> Bool {

        let feedListVO:FeedListVO? = getFeedListVO(feedType: feedType, feedSource:feedSource, seasonGroupRef: seasonGroupRef)
        if feedListVO == nil {
            return viewed.viewConfig.homeDefaultIsHappened
        }
        let isFiltered:Bool = filterVO?.filterString != nil
        let primaryVO:FeedViewVO? = feedListVO!.getFeedVO(isUpcoming: !viewed.viewConfig.homeDefaultIsHappened, isFiltered: isFiltered)
        let secondaryVO:FeedViewVO? = feedListVO!.getFeedVO(isUpcoming: viewed.viewConfig.homeDefaultIsHappened, isFiltered: isFiltered)
        if primaryVO == nil || primaryVO!.filterVO?.filterString != filterVO?.filterString {
            if secondaryVO != nil && secondaryVO!.filterVO?.filterString == filterVO?.filterString {
                return false
            }
            return true
        }
        if primaryVO!.entryVOs.count == 0 && primaryVO!.endOfList {
            if secondaryVO!.entryVOs.count > 0 {
                return false
            }
        }
        return true
    }

    func resetFeeds( teamOnly:Bool=false ) {
        getContainerFeedListVO( feedType:.event ).removeAll(teamOnly: teamOnly)
        getContainerFeedListVO( feedType:.fixture).removeAll(teamOnly: teamOnly)
    }

    func isDataReset( teamOnly:Bool=false ) -> Bool {
        let eventContainer:ContainerFeedListVO = getContainerFeedListVO( feedType:.event )
        if !teamOnly {
            if eventContainer.listingVOs.count > 0 {
                return false
            }
        }
        if eventContainer.teamSpecificVOs.count > 0 {
            return false
        }
        if eventContainer.teamAllVOs.count > 0 {
            return false
        }
        let fixtureContainer:ContainerFeedListVO = getContainerFeedListVO( feedType:.fixture)
        if !teamOnly {
            if fixtureContainer.listingVOs.count > 0 {
                return false
            }
        }
        if fixtureContainer.teamSpecificVOs.count > 0 {
            return false
        }
        if fixtureContainer.teamAllVOs.count > 0 {
            return false
        }
        return true
    }
}
