import UIKit
import UserNotifications

class RequirePushAuthorizationViewCommand {

    private var app: App { return App.instance }
    private var preference: Preference { return Preference.instance }

    func command( viewController:ViewController, on:Bool, subscriptionsDidChange:Bool, allowNotSupportedError:Bool = true ){

        let pushStatus:PushAuthorizationStatus = preference.preferenceModel.pushAuthorizationStatus

        if pushStatus == .unknown || pushStatus == .notSupported {
            if allowNotSupportedError {
                let alert = UIAlertController(title: "error".localized, message: "incorrectly_configured", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "ok", style: .default, handler: nil))

                viewController.present(alert, animated: true)
            }
        } else if pushStatus == .notRequested {

            if on {
                let alert = UIAlertController(title: "push_notifications_required_title".localized, message: "push_notifications_required_body".localized, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "cancel".localized, style: .default, handler: nil))
                alert.addAction(UIAlertAction(title: "ok".localized, style: .default, handler: registerForPushNotifications))

                viewController.present(alert, animated: true)
            } else {
                //do nothing
            }
        } else if pushStatus == .denied {

            if on {
                let alert = UIAlertController(title: "push_notifications_required_title".localized, message: "turn_on_push_notifications".localized, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "ok", style: .default, handler: nil))

                viewController.present(alert, animated: true)
            } else {
                //do nothing
            }
        } else {
            app.updatePushStatusAppCommand().command(forceServerUpdate: subscriptionsDidChange)
        }
    }

    func registerForPushNotifications( _:Any?) {
        UNUserNotificationCenter.current().requestAuthorization(options:[.badge, .alert, .sound]) { (granted, error) in }
        UIApplication.shared.registerForRemoteNotifications()
    }
}
