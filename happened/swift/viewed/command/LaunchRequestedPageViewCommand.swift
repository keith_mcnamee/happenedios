import Foundation

class LaunchRequestedPageViewCommand {

    private var app:App { return App.instance }
    private var viewed: Viewed { return Viewed.instance }

    func launchWithProperties(pageID:String, action:Int? = nil, tabIdentifier:TabIdentifier? = nil ){

        var pageUserInfo:[AnyHashable : Any] = [
                AppViewAttribute.requestType: pageID
        ]
        if action != nil {
            pageUserInfo[AppViewAttribute.action] = action!
        }

        viewed.viewModel.waitingPageTabID = tabIdentifier
        viewed.viewModel.waitingPageUserInfo = pageUserInfo
        checkForWaitingTab()
    }

    func checkForWaitingTab() {
        if viewed.viewModel.waitingPageTabID != nil {
            if viewed.viewModel.tabBarDisplayed {
                if viewed.viewModel.visibleTab == viewed.viewModel.waitingPageTabID {
                    viewed.viewModel.waitingPageTabID = nil
                    checkForWaitingPage()
                } else {

                    let tabUserInfo:[AnyHashable : Any] = [
                            AppViewAttribute.requestType: AppConstant.SHOW_TAB_REQUEST,
                            AppViewAttribute.action: viewed.viewModel.waitingPageTabID!
                    ]

                    DispatchQueue.main.async {
                        NotificationCenter.default.post(name: Notification.Name(TabBarController.OBSERVER_ID), object: nil, userInfo: tabUserInfo)
                    }
                }
            }
        } else {
            checkForWaitingPage()
        }
    }

    func checkForWaitingPage() {
        if viewed.viewModel.waitingPageTabID != nil && viewed.viewModel.visibleTab != viewed.viewModel.waitingPageTabID {
            return
        }
        if viewed.viewModel.waitingPageUserInfo == nil {
            return
        }

        let waitingPageUserInfo:[AnyHashable : Any] = viewed.viewModel.waitingPageUserInfo!
        viewed.viewModel.waitingPageUserInfo = nil

        NotificationCenter.default.post(name: Notification.Name(AppConstant.SHOW_PAGE_REQUEST), object: nil, userInfo: waitingPageUserInfo)
    }
}
