import Foundation

class ResetViewCommand {

    private var viewed: Viewed { return Viewed.instance }

    func command(){
        viewed.eventFeedViewModel.reset()
        viewed.fixtureFeedViewModel.reset()
        viewed.viewModel.reset()
        viewed.feedViewHelper.resetFeeds()
    }
}
