import UIKit

class PickerView:UIPickerView, ExtAppView {

    private var viewed: Viewed { return Viewed.instance }

    weak var _heightConstraint:NSLayoutConstraint? = nil
    weak var _widthConstraint:NSLayoutConstraint? = nil
    weak var _topConstraint:NSLayoutConstraint? = nil
    weak var _bottomConstraint:NSLayoutConstraint? = nil
    weak var _leadingConstraint:NSLayoutConstraint? = nil
    weak var _trailingConstraint:NSLayoutConstraint? = nil
    weak var _centerXConstraint:NSLayoutConstraint? = nil
    weak var _centerYConstraint:NSLayoutConstraint? = nil

    var font:UIFont? = nil
    var rowPadding: CGSize = CGSize.zero

    func getAppConstraint( _ type: AppConstraint ) -> NSLayoutConstraint? {
        return viewed.uiHelper.getAppConstraint(type, self)
    }

    func addAppConstraint( _ constraint:NSLayoutConstraint, _ toView:UIView?=nil, _ type: AppConstraint? = nil ){
        viewed.uiHelper.addAppConstraint(constraint, self, toView, type)
    }

    func removeAppConstraint( _ type: AppConstraint ) {
        viewed.uiHelper.removeAppConstraint(type, self)
    }

    required init() {
        super.init(frame: .zero)
        initialize()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initialize()
    }

    func initialize() {
        translatesAutoresizingMaskIntoConstraints = false
        applyDefaultVisuals()
    }

    func applyDefaultVisuals() {
        font = UIFont.font( family: viewed.viewConfig.fontFamily2, pointSizeStyle: .title4)
        rowPadding = CGSize(width: 0, height: 3)
    }

    func destroy() {
    }

    deinit {
        destroy()
    }
}
