import UIKit

class ResizingTextView: UITextView, ExtAppView {

    private var viewed: Viewed { return Viewed.instance }

    weak var _heightConstraint:NSLayoutConstraint? = nil
    weak var _widthConstraint:NSLayoutConstraint? = nil
    weak var _topConstraint:NSLayoutConstraint? = nil
    weak var _bottomConstraint:NSLayoutConstraint? = nil
    weak var _leadingConstraint:NSLayoutConstraint? = nil
    weak var _trailingConstraint:NSLayoutConstraint? = nil
    weak var _centerXConstraint:NSLayoutConstraint? = nil
    weak var _centerYConstraint:NSLayoutConstraint? = nil

    func getAppConstraint( _ type: AppConstraint ) -> NSLayoutConstraint? {
        return viewed.uiHelper.getAppConstraint(type, self)
    }

    func addAppConstraint( _ constraint:NSLayoutConstraint, _ toView:UIView?=nil, _ type: AppConstraint? = nil ){
        viewed.uiHelper.addAppConstraint(constraint, self, toView, type)
    }

    func removeAppConstraint( _ type: AppConstraint ) {
        viewed.uiHelper.removeAppConstraint(type, self)
    }

    var prevHeight:CGFloat?
    var preparedText:String?

    required override init(frame: CGRect, textContainer: NSTextContainer?) {
        super.init(frame: frame, textContainer:textContainer)
        initialize()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initialize()
    }

    func initialize() {

    }

    func initiateResizing(_ usePrepared:String? = nil) {
        layoutIfNeeded()
        sizeToFit()
        textContainerInset = UIEdgeInsets.init(top: 0, left: 0, bottom: 0, right: 0)
        if usePrepared != nil {
            prepareText( usePrepared! )
        }
    }

    func applyCurrentState() -> Bool {
        if let usePrepared:String = preparedText {
            preparedText = nil
            return applyResizingText(usePrepared)
        }
        sizeToFit()
        if height != prevHeight {
            prevHeight = height
            return true
        }
        return false
    }

    func applyResizingText(_ newValue:String) -> Bool {
        layoutIfNeeded()
        text = newValue
        return applyCurrentState()
    }

    func prepareText(_ newValue:String) {
        preparedText = newValue
    }

    func destroy() {
    }

    deinit {
        destroy()
    }
}
