import UIKit

class ListCellView: AppView {

    var primaryView:UIView? = nil

    override func initialize() {
        super.initialize()
        primaryView = self
    }

    func addChildView(_ childView:ExtAppView, xPos:AppViewAttribute = .left, yPos:AppViewAttribute = .top, isPrimary:Bool = true ) {
        let uiView:UIView = childView as! UIView

        addSubview( uiView )

        if isPrimary {
            primaryView = uiView
        }
        if let attributedTextView:AttributedTextView = childView as? AttributedTextView {
            for constraint:NSLayoutConstraint in attributedTextView.constraints {
                attributedTextView.removeConstraint(constraint)
            }
        }

        let topConstraint:NSLayoutConstraint = NSLayoutConstraint(item: childView, attribute: .top, relatedBy: yPos == .top ? .equal : .greaterThanOrEqual, toItem: self, attribute: .top, multiplier: 1, constant: 0)
        let leadingConstraint:NSLayoutConstraint =  NSLayoutConstraint(item: childView, attribute: .leading, relatedBy: xPos == .left ? .equal : .greaterThanOrEqual, toItem: self, attribute: .leading, multiplier: 1, constant: 0)
        let trailingConstraint:NSLayoutConstraint = NSLayoutConstraint(item: childView, attribute: .trailing, relatedBy: xPos == .right ? .equal : .lessThanOrEqual, toItem: self, attribute: .trailing, multiplier: 1, constant: 0)
        let bottomConstraint:NSLayoutConstraint = NSLayoutConstraint(item: childView, attribute: .bottom, relatedBy: yPos == .bottom ? .equal : .lessThanOrEqual, toItem: self, attribute: .bottom, multiplier: 1, constant: 0)

        childView.addAppConstraint(topConstraint, self, nil)
        childView.addAppConstraint(bottomConstraint, self, nil)
        childView.addAppConstraint(leadingConstraint, self, nil)
        childView.addAppConstraint(trailingConstraint, self, nil)

        if xPos == .centerX {
            let centerXConstraint:NSLayoutConstraint =  NSLayoutConstraint(item: childView, attribute: .centerX, relatedBy: .equal, toItem: self, attribute: .centerX, multiplier: 1, constant: 0)
            childView.addAppConstraint(centerXConstraint, self, nil)
        }

        if yPos == .centerY {
            let centerYConstraint:NSLayoutConstraint =  NSLayoutConstraint(item: childView, attribute: .centerY, relatedBy: .equal, toItem: self, attribute: .centerY, multiplier: 1, constant: 0)
            childView.addAppConstraint(centerYConstraint, self, nil)
        }
        self.layoutIfNeeded()
    }

    override func destroy() {
        if primaryView != nil && primaryView != self {
            primaryView!.removeFromSuperview()
            primaryView = self
        }
    }

    var minimumWidth:CGFloat {
        if let attributedTextView:AttributedTextView = primaryView as? AttributedTextView {
            return attributedTextView.getMinimumWidth()
        }
        if primaryView != nil && primaryView != self {
            return primaryView!.width
        }
        return 0
    }

    var maximumWidth:CGFloat {
        if let attributedTextView:AttributedTextView = primaryView as? AttributedTextView {
            return attributedTextView.getMaximumWidth()
        }
        if primaryView != nil && primaryView != self {
            return primaryView!.width
        }
        return 0
    }
}
