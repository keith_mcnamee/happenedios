import UIKit

class ScrollView:UIScrollView, ExtAppView {

    private var viewed: Viewed { return Viewed.instance }

    weak var _heightConstraint:NSLayoutConstraint? = nil
    weak var _widthConstraint:NSLayoutConstraint? = nil
    weak var _topConstraint:NSLayoutConstraint? = nil
    weak var _bottomConstraint:NSLayoutConstraint? = nil
    weak var _leadingConstraint:NSLayoutConstraint? = nil
    weak var _trailingConstraint:NSLayoutConstraint? = nil
    weak var _centerXConstraint:NSLayoutConstraint? = nil
    weak var _centerYConstraint:NSLayoutConstraint? = nil

    func getAppConstraint( _ type: AppConstraint ) -> NSLayoutConstraint? {
        return viewed.uiHelper.getAppConstraint(type, self)
    }

    func addAppConstraint( _ constraint:NSLayoutConstraint, _ toView:UIView?=nil, _ type: AppConstraint? = nil ){
        viewed.uiHelper.addAppConstraint(constraint, self, toView, type)
    }

    func removeAppConstraint( _ type: AppConstraint ) {
        viewed.uiHelper.removeAppConstraint(type, self)
    }

    required init( frame:CGRect = .zero, a:Bool=false ) {
        super.init(frame: frame)
        initialize()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initialize()
    }

    func initialize() {
        translatesAutoresizingMaskIntoConstraints = false
        applyDefaultVisuals()
    }

    func applyDefaultVisuals() {

    }

    var isAtTop: Bool {
        return contentOffset.y <= verticalOffsetForTop
    }

    var isAtBottom: Bool {
        return contentOffset.y >= verticalOffsetForBottom
    }

    var isAtLeading: Bool {
        return contentOffset.x <= horizontalOffsetForLeading
    }

    var isAtTrailing: Bool {
        return contentOffset.x >= horizontalOffsetForTrailing
    }

    var isScrolledToBottom: Bool {
        return isAtBottom && contentSize.height > height
    }

    var verticalOffsetForTop: CGFloat {
        let topInset = contentInset.top
        return -topInset
    }

    var verticalOffsetForBottom: CGFloat {
        let scrollViewHeight = height
        let scrollContentSizeHeight = contentSize.height
        let bottomInset = contentInset.bottom
        let scrollViewBottomOffset = scrollContentSizeHeight + bottomInset - scrollViewHeight
        return scrollViewBottomOffset
    }

    var horizontalOffsetForLeading: CGFloat {
        return 0
    }

    var horizontalOffsetForTrailing: CGFloat {
        let scrollViewWidth = width
        let scrollContentSizeWidth = contentSize.width
        let scrollViewTrailingOffset = scrollContentSizeWidth - scrollViewWidth
        return scrollViewTrailingOffset
    }
}
