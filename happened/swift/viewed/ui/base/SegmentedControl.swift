import UIKit

class SegmentedControl: UISegmentedControl, ExtAppView {

    private var viewed: Viewed { return Viewed.instance }

    weak var _heightConstraint:NSLayoutConstraint? = nil
    weak var _widthConstraint:NSLayoutConstraint? = nil
    weak var _topConstraint:NSLayoutConstraint? = nil
    weak var _bottomConstraint:NSLayoutConstraint? = nil
    weak var _leadingConstraint:NSLayoutConstraint? = nil
    weak var _trailingConstraint:NSLayoutConstraint? = nil
    weak var _centerXConstraint:NSLayoutConstraint? = nil
    weak var _centerYConstraint:NSLayoutConstraint? = nil

    var font:UIFont? = nil

    var segmentPadding: CGSize = CGSize.zero

    enum DummyIndexes:Int {
        case first = 0,
             second = 1
    }

    func getAppConstraint( _ type: AppConstraint ) -> NSLayoutConstraint? {
        return viewed.uiHelper.getAppConstraint(type, self)
    }

    func addAppConstraint( _ constraint:NSLayoutConstraint, _ toView:UIView?=nil, _ type: AppConstraint? = nil ){
        viewed.uiHelper.addAppConstraint(constraint, self, toView, type)
    }

    func removeAppConstraint( _ type: AppConstraint ) {
        viewed.uiHelper.removeAppConstraint(type, self)
    }

    required init() {
        super.init(frame: .zero)
        initialize()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initialize()
    }

    func initialize() {
        translatesAutoresizingMaskIntoConstraints = false
        applyDefaultVisuals()
    }

    func applyDefaultVisuals() {

        font = UIFont.font( family: viewed.viewConfig.fontFamily2, pointSizeStyle: .body3)

        segmentPadding = CGSize(width: CGFloat( 6 ).deviceValue, height: CGFloat( 4 ).deviceValue)

        let attributes:[AnyHashable : Any] = [
                convertFromNSAttributedStringKey(NSAttributedString.Key.font): font!
        ]
        setTitleTextAttributes(attributes as? [NSAttributedString.Key : Any], for: .normal)
        tintColor = viewed.viewConfig.mainBlueColor

        insertSegment(withTitle: "First".localized, at: DummyIndexes.first.rawValue, animated: false)
        insertSegment(withTitle: "Second".localized, at: DummyIndexes.second.rawValue, animated: false)
        apportionsSegmentWidthsByContent = true
    }

    override var intrinsicContentSize: CGSize {
        var size:CGSize = super.intrinsicContentSize

        if font != nil {
            size.height = floor(font!.lineHeight + 2 * segmentPadding.height)
        }
        else {
            size.height += segmentPadding.height * 2
        }

        size.width  += segmentPadding.width * CGFloat(numberOfSegments + 1)

        return size
    }

    override func insertSegment(withTitle title: String?, at segment: Int, animated: Bool) {
        super.insertSegment(withTitle: title, at:segment, animated:animated)
    }

    func destroy() {
    }

    deinit {
        destroy()
    }
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromNSAttributedStringKey(_ input: NSAttributedString.Key) -> String {
	return input.rawValue
}
