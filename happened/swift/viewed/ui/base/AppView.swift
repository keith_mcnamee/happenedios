import UIKit

class AppView: UIView, ExtAppView {

    private var viewed: Viewed { return Viewed.instance }

    weak var _heightConstraint:NSLayoutConstraint? = nil
    weak var _widthConstraint:NSLayoutConstraint? = nil
    weak var _topConstraint:NSLayoutConstraint? = nil
    weak var _bottomConstraint:NSLayoutConstraint? = nil
    weak var _leadingConstraint:NSLayoutConstraint? = nil
    weak var _trailingConstraint:NSLayoutConstraint? = nil
    weak var _centerXConstraint:NSLayoutConstraint? = nil
    weak var _centerYConstraint:NSLayoutConstraint? = nil

    func getAppConstraint( _ type: AppConstraint ) -> NSLayoutConstraint? {
        return viewed.uiHelper.getAppConstraint(type, self)
    }

    func addAppConstraint( _ constraint:NSLayoutConstraint, _ toView:UIView?=nil, _ type: AppConstraint? = nil ){
        viewed.uiHelper.addAppConstraint(constraint, self, toView, type)
    }

    func removeAppConstraint( _ type: AppConstraint ) {
        viewed.uiHelper.removeAppConstraint(type, self)
    }

    var identifier:String = "none"

    var frameApplied:Bool = false

    convenience init(){
        self.init(callInitialize: true)
    }

    required init( callInitialize:Bool) {
        super.init(frame: .zero)
        if callInitialize {
            initialize()
        }
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initialize()
    }

    func initialize() {
        initializeView()
        addChildViews()
        applyView()
        self.layoutIfNeeded()
    }

    func initializeView() {
        translatesAutoresizingMaskIntoConstraints = false
        applyDefaultVisuals()
    }

    func addChildViews() {

    }

    func applyView() {

    }

    override func willMove(toSuperview newSuperview: UIView?) {
        super.willMove(toSuperview: newSuperview)
        if !frameApplied {
            frameApplied = true
            applyFrame()
        }
    }

    func applyFrame() {

    }

    func applyDefaultVisuals() {
        backgroundColor = .clear
    }

    func removeAllGestures() {
        if let gestures:[UIGestureRecognizer] = gestureRecognizers {
            for gesture:UIGestureRecognizer in gestures {
                self.removeGestureRecognizer(gesture)
            }
        }
    }

    func destroy() {
        removeAllGestures()
    }

    deinit {
        destroy()
    }
}


protocol ExtAppView: class {

    var _heightConstraint:NSLayoutConstraint? {get set}
    var _widthConstraint:NSLayoutConstraint? {get set}
    var _topConstraint:NSLayoutConstraint? {get set}
    var _bottomConstraint:NSLayoutConstraint? {get set}
    var _leadingConstraint:NSLayoutConstraint? {get set}
    var _trailingConstraint:NSLayoutConstraint? {get set}
    var _centerXConstraint:NSLayoutConstraint? {get set}
    var _centerYConstraint:NSLayoutConstraint? {get set}

    func getAppConstraint( _ type: AppConstraint ) -> NSLayoutConstraint?
    func addAppConstraint( _ constraint:NSLayoutConstraint, _ toView:UIView?, _ type: AppConstraint? )
    func removeAppConstraint( _ type: AppConstraint )
}
