import UIKit

class Button: UIButton, ExtAppView  {

    var viewed: Viewed { return Viewed.instance }

    weak var _heightConstraint:NSLayoutConstraint? = nil
    weak var _widthConstraint:NSLayoutConstraint? = nil
    weak var _topConstraint:NSLayoutConstraint? = nil
    weak var _bottomConstraint:NSLayoutConstraint? = nil
    weak var _leadingConstraint:NSLayoutConstraint? = nil
    weak var _trailingConstraint:NSLayoutConstraint? = nil
    weak var _centerXConstraint:NSLayoutConstraint? = nil
    weak var _centerYConstraint:NSLayoutConstraint? = nil

    private var _lrPadding:CGFloat = 5;
    private var _tbPadding:CGFloat = 0;

    func getAppConstraint( _ type: AppConstraint ) -> NSLayoutConstraint? {
        return viewed.uiHelper.getAppConstraint(type, self)
    }

    func addAppConstraint( _ constraint:NSLayoutConstraint, _ toView:UIView?=nil, _ type: AppConstraint? = nil ){
        viewed.uiHelper.addAppConstraint(constraint, self, toView, type)
    }

    func removeAppConstraint( _ type: AppConstraint ) {
        viewed.uiHelper.removeAppConstraint(type, self)
    }

    var observerID:String? = nil
    var requestType:String?=nil
    var action:Any? = nil

    required init() {
        super.init(frame: .zero)
        initialize()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initialize()
    }

    func initialize() {
        addChildren()
        translatesAutoresizingMaskIntoConstraints = false
        applyDefaultVisuals()
    }

    func addChildren() {

    }

    func setIsEnabled( _ value:Bool ) {
        isEnabled = value
        applyEnabledState()
    }

    func applyEnabledState() {

    }

    func applyDefaultVisuals() {
        setTitle("Button", for: .normal)
        titleLabel?.font = UIFont.font( family: viewed.viewConfig.fontFamily2, pointSizeStyle: .body3)
        setTitleColor(viewed.viewConfig.linkColor, for: .normal)
        setTitleColor(viewed.viewConfig.linkDisabledColor, for: .disabled)
        contentEdgeInsets = UIEdgeInsets(top: CGFloat( 4 ).deviceValue, left: CGFloat( 6 ).deviceValue, bottom: CGFloat( 4 ).deviceValue, right: CGFloat( 6 ).deviceValue)
        layer.cornerRadius = CGFloat(2.5).deviceValue
    }

    var buttonHeight:CGFloat {
        let lineHeight:CGFloat = titleLabel != nil ? titleLabel!.font.lineHeight : 0
        return lineHeight + contentEdgeInsets.top + contentEdgeInsets.bottom
    }

    func addObserverID(observerID:String, requestType:String?=nil, action:Any?=nil) {
        removeAllTargets()
        addTargets()
        self.observerID = observerID
        self.requestType = requestType
        self.action = action
    }

    func removeObserver() {
        removeAllTargets()
        observerID = nil
        requestType = nil
        action = nil
    }

    @objc func touchUpInside(_ sender: Any) {
        if observerID == nil {
            return
        }
        var userInfo:[AnyHashable : Any]? = nil
        if requestType != nil || action != nil {
            userInfo = [:]
            if requestType != nil {
                userInfo![AppViewAttribute.requestType] = requestType!
            }
            if action != nil {
                userInfo![AppViewAttribute.action] = action!
            }
        }
        NotificationCenter.default.post(name: Notification.Name(observerID!), object: nil, userInfo: userInfo)
    }

    func addTargets() {
        addTarget(self, action: #selector(touchUpInside(_:)), for: .touchUpInside)
    }

    func removeAllTargets() {
        removeTarget(nil, action: nil, for: .allEvents)
    }


    func destroy() {
        removeAllTargets()
    }

    deinit {
        destroy()
    }
}
