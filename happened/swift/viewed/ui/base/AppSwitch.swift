import UIKit

class AppSwitch: UISwitch, ExtAppView {

    private var viewed: Viewed { return Viewed.instance }

    weak var _heightConstraint:NSLayoutConstraint? = nil
    weak var _widthConstraint:NSLayoutConstraint? = nil
    weak var _topConstraint:NSLayoutConstraint? = nil
    weak var _bottomConstraint:NSLayoutConstraint? = nil
    weak var _leadingConstraint:NSLayoutConstraint? = nil
    weak var _trailingConstraint:NSLayoutConstraint? = nil
    weak var _centerXConstraint:NSLayoutConstraint? = nil
    weak var _centerYConstraint:NSLayoutConstraint? = nil

    func getAppConstraint( _ type: AppConstraint ) -> NSLayoutConstraint? {
        return viewed.uiHelper.getAppConstraint(type, self)
    }

    func addAppConstraint( _ constraint:NSLayoutConstraint, _ toView:UIView?=nil, _ type: AppConstraint? = nil ){
        viewed.uiHelper.addAppConstraint(constraint, self, toView, type)
    }

    func removeAppConstraint( _ type: AppConstraint ) {
        viewed.uiHelper.removeAppConstraint(type, self)
    }

    var observerID:String? = nil
    var requestType:String?=nil
    var action:Any? = nil

    required init() {
        super.init(frame: .zero)
        initialize()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initialize()
    }

    func initialize() {
        translatesAutoresizingMaskIntoConstraints = false
        applyDefaultVisuals()
    }

    func applyDefaultVisuals() {

    }

    func addObserverID(observerID:String, requestType:String?=nil, action:Any?=nil) {
        removeAllTargets()
        addTargets()
        self.observerID = observerID
        self.requestType = requestType
        self.action = action
    }

    func removeObserver() {
        removeAllTargets()
        observerID = nil
        requestType = nil
        action = nil
    }

    @objc func valueChanged(_ sender: Any) {
        if observerID == nil {
            return
        }
        var userInfo:[AnyHashable : Any] = [AppViewAttribute.value:isOn]
        if requestType != nil {
            userInfo[AppViewAttribute.requestType] = requestType!
        }
        if action != nil {
            userInfo[AppViewAttribute.action] = action!
        }
        NotificationCenter.default.post(name: Notification.Name(observerID!), object: nil, userInfo: userInfo)
    }

    func addTargets() {
        addTarget(self, action: #selector(valueChanged(_:)), for: .valueChanged)
    }

    func removeAllTargets() {
        removeTarget(nil, action: nil, for: .allEvents)
    }


    func destroy() {
        removeAllTargets()
    }

    deinit {
        destroy()
    }
}
