import UIKit

class PageControl:UIPageControl, ExtAppView {

    private var viewed: Viewed { return Viewed.instance }

    weak var _heightConstraint:NSLayoutConstraint? = nil
    weak var _widthConstraint:NSLayoutConstraint? = nil
    weak var _topConstraint:NSLayoutConstraint? = nil
    weak var _bottomConstraint:NSLayoutConstraint? = nil
    weak var _leadingConstraint:NSLayoutConstraint? = nil
    weak var _trailingConstraint:NSLayoutConstraint? = nil
    weak var _centerXConstraint:NSLayoutConstraint? = nil
    weak var _centerYConstraint:NSLayoutConstraint? = nil

    func getAppConstraint( _ type: AppConstraint ) -> NSLayoutConstraint? {
        return viewed.uiHelper.getAppConstraint(type, self)
    }

    func addAppConstraint( _ constraint:NSLayoutConstraint, _ toView:UIView?=nil, _ type: AppConstraint? = nil ){
        viewed.uiHelper.addAppConstraint(constraint, self, toView, type)
    }

    func removeAppConstraint( _ type: AppConstraint ) {
        viewed.uiHelper.removeAppConstraint(type, self)
    }

    required init() {
        super.init(frame: .zero)
        initialize()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initialize()
    }

    func initialize() {
        translatesAutoresizingMaskIntoConstraints = false
        applyDefaultVisuals()
    }

    func applyDefaultVisuals() {
        pageIndicatorTintColor =  viewed.viewConfig.mainRedColor
        currentPageIndicatorTintColor = viewed.viewConfig.mainBlueColor
        let heightConstraint:NSLayoutConstraint = NSLayoutConstraint(item: self, attribute: .height, relatedBy: .lessThanOrEqual, deviceConstant: 16)
        addConstraint(heightConstraint)
    }

    func applyScaling() {
        let naturalScale:CGFloat = 1 * viewed.viewConfig.deviceValue
        var transformValue:CGFloat = naturalScale
        if numberOfPages > viewed.viewConfig.defaultPageControlMaximumPages {
            transformValue = (CGFloat( viewed.viewConfig.defaultPageControlMaximumPages) / CGFloat( numberOfPages )) * naturalScale
        }
        transform = CGAffineTransform(scaleX: transformValue, y: transformValue)
    }

    func destroy() {
    }

    deinit {
        destroy()
    }
}
