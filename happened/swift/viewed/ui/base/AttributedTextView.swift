import UIKit

class AttributedTextView: UITextView, UIGestureRecognizerDelegate, ExtAppView  {

    private var viewed: Viewed { return Viewed.instance }

    weak var _heightConstraint:NSLayoutConstraint? = nil
    weak var _widthConstraint:NSLayoutConstraint? = nil
    weak var _topConstraint:NSLayoutConstraint? = nil
    weak var _bottomConstraint:NSLayoutConstraint? = nil
    weak var _leadingConstraint:NSLayoutConstraint? = nil
    weak var _trailingConstraint:NSLayoutConstraint? = nil
    weak var _centerXConstraint:NSLayoutConstraint? = nil
    weak var _centerYConstraint:NSLayoutConstraint? = nil

    var allowDefaultWidth:Bool = false

    var flexible:Bool = false

    var defaultTextAlignment:NSTextAlignment = .left

    func getAppConstraint( _ type: AppConstraint ) -> NSLayoutConstraint? {
        return viewed.uiHelper.getAppConstraint(type, self)
    }

    func addAppConstraint( _ constraint:NSLayoutConstraint, _ toView:UIView?=nil, _ type: AppConstraint? = nil ){
        viewed.uiHelper.addAppConstraint(constraint, self, toView, type)
    }

    func removeAppConstraint( _ type: AppConstraint ) {
        viewed.uiHelper.removeAppConstraint(type, self)
    }

    var defaultTextColor:UIColor = UIColor()
    var linkColor:UIColor = UIColor()

    var minimumWidth:CGFloat? = nil
    var maximumWidth:CGFloat? = nil

    convenience init( flexible:Bool = false ){
        self.init()
        self.flexible = flexible
        if flexible {
            textContainer.maximumNumberOfLines = 0
        }
    }

    required override init(frame: CGRect, textContainer: NSTextContainer?) {
        super.init(frame: frame, textContainer:textContainer)
        initialize()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initialize()
    }

    func initialize() {
        applyDefaultSetup()
    }

    func applyDefaultSetup() {
        isEditable = false
        isScrollEnabled = false
        textContainer.maximumNumberOfLines = 1
        translatesAutoresizingMaskIntoConstraints = false
        backgroundColor = UIColor.clear
        defaultTextColor = viewed.viewConfig.defaultTextColor
        textColor = viewed.viewConfig.defaultTextColor
        font = UIFont.font()
        linkColor = viewed.viewConfig.linkColor
        textContainerInset = UIEdgeInsets(top: CGFloat(4).deviceValue, left: 0, bottom: CGFloat(4).deviceValue, right: 0);
    }

    override var canBecomeFirstResponder: Bool {
        return false
    }

    func resetPosition(){
        let currentText:NSAttributedString = attributedText
        self.attributedText = NSMutableAttributedString(string: "")
        sizeToFit()
        layoutIfNeeded()
        self.attributedText = currentText
        sizeToFit()
        layoutIfNeeded()
    }

    func applyTextVOs(_ textVOs:[TextVO], withTextAlignment:NSTextAlignment? = nil, additionalAttributes:[AppViewAttribute:Any]?=nil){

        textColor = defaultTextColor

        if self.attributedText.string != "" {
            self.attributedText = NSMutableAttributedString(string: "")
            sizeToFit()
            layoutIfNeeded()
        }

        var useTextAlignment:NSTextAlignment? = withTextAlignment
        var string:String = ""
        var allAttributes:[(attrs:[String : Any], range: NSRange)] = []
        var hasObserver:Bool = false
        for textVO:TextVO in textVOs{
            let range = NSRange(location: string.count, length: textVO.text.count)
            var attributes:[String : Any] = [:]
            var currentFont:UIFont = existingFont

            var useAttributes:[AppViewAttribute:Any] = textVO.attributes
            if additionalAttributes != nil {
                for (key, value) in additionalAttributes! {
                    useAttributes[key] = value
                }
            }

            if useAttributes[.observerID] != nil {
                hasObserver = true
                var customAttribute:[String:Any] = ["observerID":useAttributes[.observerID]!]
                if useAttributes[.requestType] != nil {
                    customAttribute["requestType"] = useAttributes[.requestType]!
                }
                if useAttributes[.action] != nil {
                    customAttribute["action"] = useAttributes[.action]!
                }
                attributes["custom_"+textVO.retrieveID] = customAttribute
                attributes[convertFromNSAttributedStringKey(NSAttributedString.Key.foregroundColor)] = linkColor
            }
            var pointSize:CGFloat? = nil
            if useAttributes[.pointSizeStyle] != nil {
                let value:UIFont.TextStyle? = useAttributes[.pointSizeStyle] as? UIFont.TextStyle
                if value != nil {
                    pointSize = viewed.viewConfig.pointSizes[value!]
                }
            }
            if pointSize == nil {
                if useAttributes[.pointSize] != nil {
                    pointSize = CGFloat.any(useAttributes[.pointSize]!)
                }
            }
            if pointSize != nil {
                currentFont = currentFont.withSize( pointSize! )
            } else {
                currentFont = font != nil ? font! : UIFont.font()
            }

            if useAttributes[.bold] != nil {
                currentFont = UIFont.font(familyName: currentFont.familyName, style: .bold, pointSize: currentFont.pointSize)
            }
            attributes[convertFromNSAttributedStringKey(NSAttributedString.Key.font)] = currentFont
            if attributes[convertFromNSAttributedStringKey(NSAttributedString.Key.foregroundColor)] == nil {
                attributes[convertFromNSAttributedStringKey(NSAttributedString.Key.foregroundColor)] = textColor
            }
            if attributes.count > 0 {
                allAttributes.append((attributes, range: range))
            }
            if let thisTextAlignment = viewed.uiHelper.textAlignAttribute(useAttributes) {
                useTextAlignment = thisTextAlignment
            }
            if let bgColor:UIColor = viewed.uiHelper.backgroundColorAttribute(useAttributes) {
                backgroundColor = bgColor
            }
            string += textVO.text
        }

        let attributedString:NSMutableAttributedString = NSMutableAttributedString(string: string)
        for attribute:(attrs:[String : Any], range: NSRange) in allAttributes {
            attributedString.addAttributes(convertToNSAttributedStringKeyDictionary(attribute.attrs), range: attribute.range)
        }
        frame = CGRect(x: 0, y: 0, width: 0, height: 0)


        self.attributedText = attributedString

        if flexible {
            textContainer.maximumNumberOfLines = 1
            sizeToFit()
            maximumWidth = width
            textContainer.maximumNumberOfLines = 0
            let unbrokenString:String = attributedString.string
            if unbrokenString.range(of:" ") != nil {
                let lineBreakString:String = unbrokenString.replacingOccurrences(of: " ", with: "\n")
                let copiedText:NSAttributedString = self.attributedText
                attributedString.replaceCharacters(in: NSMakeRange(0, attributedString.length), with: lineBreakString)
                self.attributedText = attributedString
                sizeToFit()
                minimumWidth = width
                self.attributedText = copiedText
            }
        }
        sizeToFit()

        if hasObserver {
            let tap = UITapGestureRecognizer(target: self, action: #selector(attributedTextTapped(_:)))
            tap.delegate = self
            self.addGestureRecognizer(tap)
        }

        if useTextAlignment == nil {
            useTextAlignment = defaultTextAlignment
        }
        textAlignment = useTextAlignment!
    }

    @objc func attributedTextTapped(_ sender: UITapGestureRecognizer) {

        var location = sender.location(in: self)
        location.x -= self.textContainerInset.left;
        location.y -= self.textContainerInset.top;

        let characterIndex:Int = Int(layoutManager.characterIndex(for: location, in: self.textContainer, fractionOfDistanceBetweenInsertionPoints: nil))

        if characterIndex < self.textStorage.length {

            let attributes:[String : Any] = convertFromNSAttributedStringKeyDictionary(self.attributedText.attributes(at: characterIndex, effectiveRange: nil))

            for (key, value) in attributes {
                if key.range(of: "custom_")?.lowerBound == key.startIndex {
                    if let attributes:[String:Any] = value as? [String:Any] {
                        if let observerID:String = attributes["observerID"] as? String {
                            let requestType:String? = attributes["requestType"] as? String
                            let action:Any? = attributes["action"]
                            dispatchRequest(observerID: observerID, requestType: requestType, action: action)
                        }
                    }
                }
            }
        }
    }

    func dispatchRequest( observerID:String, requestType:String?, action:Any? ) {
        var userInfo:[AnyHashable : Any]? = nil
        if requestType != nil || action != nil {
            userInfo = [:]
            if requestType != nil {
                userInfo![AppViewAttribute.requestType] = requestType!
            }
            if action != nil {
                userInfo![AppViewAttribute.action] = action!
            }
        }
        NotificationCenter.default.post(name: Notification.Name(observerID), object: nil, userInfo: userInfo)
    }

    var existingFont: UIFont {
        let thisFont:UIFont = font != nil ? font! : UIFont.font()
        return UIFont(name: thisFont.fontName, size: thisFont.pointSize)!
    }

    func getMinimumWidth() -> CGFloat {
        return minimumWidth != nil ? minimumWidth! : width
    }

    func getMaximumWidth() -> CGFloat {
        return maximumWidth != nil ? maximumWidth! : width
    }

    override func addConstraint(_ constraint: NSLayoutConstraint) {
        if !allowDefaultWidth {
            if constraint.firstAttribute == .width && constraint != _widthConstraint {
                if constraint.priority == UILayoutPriority.required && constraint.relation == .equal {
                    if getAppConstraint(.width) != nil {
                        return
                    }
                }
            }
        }
        super.addConstraint(constraint)
    }

    var defaultTextHeight:CGFloat {
        return font != nil ? font!.lineHeight : 0
    }

    func removeAllGestures() {
        if let gestures:[UIGestureRecognizer] = gestureRecognizers {
            for gesture:UIGestureRecognizer in gestures {
                self.removeGestureRecognizer(gesture)
            }
        }
    }


    func destroy() {
        removeAllGestures()
    }

    deinit {
        destroy()
    }
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromNSAttributedStringKey(_ input: NSAttributedString.Key) -> String {
	return input.rawValue
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToNSAttributedStringKeyDictionary(_ input: [String: Any]) -> [NSAttributedString.Key: Any] {
	return Dictionary(uniqueKeysWithValues: input.map { key, value in (NSAttributedString.Key(rawValue: key), value)})
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromNSAttributedStringKeyDictionary(_ input: [NSAttributedString.Key: Any]) -> [String: Any] {
	return Dictionary(uniqueKeysWithValues: input.map {key, value in (key.rawValue, value)})
}
