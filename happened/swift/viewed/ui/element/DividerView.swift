import UIKit

class DividerView: AppView  {

    var viewed: Viewed { return Viewed.instance }

    var lineView: AppView = AppView()

    var color:UIColor?=nil
    var topSpace:CGFloat?=nil
    var bottomSpace:CGFloat?=nil
    var lineHeight:CGFloat?=nil
    var leftPadding:CGFloat?=nil
    var rightPadding:CGFloat?=nil

    convenience init( color:UIColor?=nil, topSpace:CGFloat?=nil, bottomSpace:CGFloat?=nil, lineHeight:CGFloat?=nil, leftPadding:CGFloat?=nil, rightPadding:CGFloat?=nil  ) {
        self.init(callInitialize: false)
        self.color = color
        self.topSpace = topSpace
        self.bottomSpace = bottomSpace
        self.lineHeight = lineHeight
        self.leftPadding = leftPadding
        self.rightPadding = rightPadding
        initialize()
    }

    override func applyDefaultVisuals() {
        super.applyDefaultVisuals()
        color = color != nil ? color : viewed.viewConfig.colorBlue6
        topSpace = topSpace != nil ? topSpace : CGFloat(8).deviceValue
        bottomSpace = bottomSpace != nil ? bottomSpace : CGFloat(8).deviceValue
        leftPadding = leftPadding != nil ? leftPadding : 0
        rightPadding = rightPadding != nil ? rightPadding : 0
        lineHeight = lineHeight != nil ? lineHeight : 1
    }

    override func applyView() {
        super.applyView()
        _ = lineView.addBorder(edges: [.bottom], color: color!, thickness: lineHeight!)
    }

    override func addChildViews() {
        super.addChildViews()

        addLineView()
    }

    private func addLineView() {
        self.addSubview(lineView)

        let topConstraint:NSLayoutConstraint = NSLayoutConstraint(item: lineView, attribute: .top, relatedBy: .equal, toItem: self, attribute: .top, multiplier: 1, constant: topSpace!)
        let leadingConstraint:NSLayoutConstraint =  NSLayoutConstraint(item: lineView, attribute: .leading, relatedBy: .equal, toItem: self, attribute: .leading, multiplier: 1, constant: leftPadding!)
        let trailingConstraint:NSLayoutConstraint = NSLayoutConstraint(item: lineView, attribute: .trailing, relatedBy: .equal, toItem: self, attribute: .trailing, multiplier: 1, constant: -rightPadding!)
        let bottomConstraint:NSLayoutConstraint = NSLayoutConstraint(item: lineView, attribute: .bottom, relatedBy: .equal, toItem: self, attribute: .bottom, multiplier: 1, constant: -bottomSpace!)
        lineView.addAppConstraint(topConstraint, self)
        lineView.addAppConstraint(leadingConstraint, self)
        lineView.addAppConstraint(trailingConstraint, self)
        lineView.addAppConstraint(bottomConstraint, self)

        let heightConstraint:NSLayoutConstraint = NSLayoutConstraint(item: lineView, attribute: .height, actualConstant: 1)
        lineView.addAppConstraint(heightConstraint)
    }
}
