import UIKit

class CompetitionMultipleSelectButton: Button {

    private var _state:MultipleSelectedState = .not

    override func applyDefaultVisuals() {
        super.applyDefaultVisuals()
        self.setTitle("", for: .normal)
        applyState()
    }

    var currentState:MultipleSelectedState {
        get {
            return _state
        }
        set {
            if newValue != _state {
                _state = newValue
                applyState()
            }
        }
    }

    private func applyState(){
        var imageName:String = "all-selected.png"
        if _state == .not {
            imageName = "none-selected.png"
        } else if _state == .partial {
            imageName = "some-selected.png"
        }
        self.setImage(UIImage.scaledToSized(named: imageName, width: CGFloat(26).deviceValue), for: .normal)
    }

    override var buttonHeight:CGFloat {
        let imageHeight:CGFloat? = image(for: .normal)?.size.height
        return imageHeight != nil ? imageHeight! : super.buttonHeight
    }

}
