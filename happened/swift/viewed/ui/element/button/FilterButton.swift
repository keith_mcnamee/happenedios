import UIKit

class FilterButton: Button {

    private var _isFiltered:Bool = false

    override func applyDefaultVisuals() {
        super.applyDefaultVisuals()
        applyFilterState()
    }

    var isFiltered:Bool {
        get {
            return _isFiltered
        }
        set {
            if newValue != _isFiltered {
                _isFiltered = newValue
                applyFilterState()
            }
        }
    }

    private func applyFilterState(){
        if !_isFiltered {
            self.setTitle("filter_off".localized, for: .normal)
            backgroundColor = viewed.viewConfig.mainRedColor
            setTitleColor(.white, for: .normal)
        } else {
            self.setTitle("filter_on".localized, for: .normal)
            backgroundColor = viewed.viewConfig.mainBlueColor
            setTitleColor(.white, for: .normal)
        }
    }

    override func applyEnabledState() {
        if isEnabled {
            alpha = 1
        } else {
            alpha = 0.2
        }
    }

}