import UIKit

class RedButton: Button {

    override func applyDefaultVisuals() {
        super.applyDefaultVisuals()
        backgroundColor = viewed.viewConfig.mainRedColor
        setTitleColor(.white, for: .normal)
    }

    override func addConstraints(_ constraints: [NSLayoutConstraint]) {
        super.addConstraints(constraints)
    }

    override func applyEnabledState() {
        if isEnabled {
            alpha = 1
        } else {
            alpha = 0.2
        }
    }
}
