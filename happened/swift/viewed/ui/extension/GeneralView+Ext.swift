import UIKit

extension CGFloat {
    static func any(_ value:Any?) -> CGFloat? {
        if let int:Int = value as? Int {
            return CGFloat(int)
        }
        if let double:Double = value as? Double {
            return CGFloat(double)
        }
        return nil
    }

    var deviceValue:CGFloat {
        let value:CGFloat = self * Viewed.instance.viewConfig.deviceValue
        return ceil( value )
    }
}

extension UIFont {
    static func font( familyName:String? = nil, family:[AppViewAttribute:String]? = nil, style:AppViewAttribute? = nil, pointSize: CGFloat? = nil, pointSizeStyle: UIFont.TextStyle? = nil ) -> UIFont {
        let viewConfig:ViewConfig = Viewed.instance.viewConfig
        var useFamily:[AppViewAttribute:String]? = family
        if familyName != nil {
            useFamily = viewConfig.fontFamilies[familyName!]
        }
        if useFamily == nil {
            useFamily = viewConfig.fontFamily1
        }
        var useStyle:AppViewAttribute? = style
        if useStyle == nil {
            useStyle = .regular
        }
        let fontName:String? = useFamily![useStyle!]
        var usePointSize:CGFloat? = pointSize
        if usePointSize == nil {
            if pointSizeStyle != nil {
                usePointSize = viewConfig.pointSizes[pointSizeStyle!]
            }
        }
        if usePointSize == nil {
            usePointSize = viewConfig.defaultPointSize
        }

        var font:UIFont? = nil
        if fontName != nil {
            font = UIFont(name: fontName!, size: usePointSize!)
        }
        if font != nil {
            return font!
        }
        font = UIFont.preferredFont(forTextStyle: viewConfig.defaultUIFontTextStyle)
        return UIFont(name: font!.fontName, size: usePointSize!)!
    }
}

extension UIImage {
    static func scaledToSized( named: String, width:CGFloat?=nil, height:CGFloat?=nil ) -> UIImage {
        let image:UIImage = UIImage(named: named)!
        if width == nil && height == nil {
            return image
        }
        var scale:CGFloat = 1
        if width != nil {
            scale = image.size.width / width!
        } else if height != nil {
            scale = image.size.height / height!
        }

        return UIImage(cgImage: image.cgImage!, scale: scale, orientation: image.imageOrientation)
    }
}

extension NSLayoutConstraint {
    public convenience init(item view1: Any, attribute attr1: NSLayoutConstraint.Attribute, relatedBy relation: NSLayoutConstraint.Relation = .equal, toItem view2: Any?=nil, attribute attr2: NSLayoutConstraint.Attribute = .notAnAttribute, multiplier: CGFloat=1, deviceConstant c: CGFloat=0, actualConstant: CGFloat?=nil, priority: UILayoutPriority?=nil) {
        self.init(item: view1, attribute: attr1, relatedBy: relation, toItem: view2, attribute: attr2, multiplier: multiplier, constant: actualConstant != nil ? actualConstant! : c.deviceValue)
        if priority != nil {
            self.priority = priority!
        }
    }
}
extension UIFont.TextStyle {
    public static let title4: UIFont.TextStyle = UIFont.TextStyle( rawValue: "title4" )
    public static let body2: UIFont.TextStyle = UIFont.TextStyle( rawValue: "body2" )
    public static let body3: UIFont.TextStyle = UIFont.TextStyle( rawValue: "body3" )
}
