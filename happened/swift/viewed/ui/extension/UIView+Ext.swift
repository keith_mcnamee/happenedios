import UIKit

extension UIView {

    var x:CGFloat {
        get {
            return frame.origin.x
        }
        set {
            var frame:CGRect = self.frame
            frame.origin.x = newValue
            self.frame = frame
        }
    }

    var y:CGFloat {
        get {
            return frame.origin.y
        }
        set {
            var frame:CGRect = self.frame
            frame.origin.y = newValue
            self.frame = frame
        }
    }

    var width:CGFloat {
        get {
            return frame.size.width
        }
        set {
            var frame:CGRect = self.frame
            frame.size.width = newValue
            self.frame = frame
        }
    }

    var height:CGFloat {
        get {
            return frame.size.height
        }
        set {
            var frame:CGRect = self.frame
            frame.size.height = newValue
            self.frame = frame
        }
    }

    func printHeritage() {
        var superview:UIView? = self
        while superview != nil {
            printDetails( superview )
            superview = superview!.superview
        }
    }

    func printDetails(_ ofView:UIView?=nil) {
        if !App.instance.commonHelper.isDebug {
            return
        }
        let useView:UIView = ofView != nil ? ofView! : self
        let frame:CGRect = useView.frame
        let bounds:CGRect = useView.bounds
        print("**********")
        if let appView:AppView = useView as? AppView {
            print("id: "+appView.identifier)
        }
        print("typeOf: "+NSStringFromClass(type(of: useView)))
        print("isHidden:"+String(isHidden))
        print("frame: x:"+String(Double(frame.origin.x))+", y:"+String(Double(frame.origin.y))+", width:"+String(Double(frame.size.width))+", height:"+String(Double(frame.size.height)))
        print("bounds: x:"+String(Double(bounds.origin.x))+", y:"+String(Double(bounds.origin.y))+", width:"+String(Double(bounds.size.width))+", height:"+String(Double(bounds.size.height)))
        print("center: x:"+String(Double(center.x))+", y:"+String(Double(center.y)))
        //printIsConstrained(attr: .left, ofView: ofView)
        //printIsConstrained(attr: .right, ofView: ofView)
        printIsConstrained(attr: .top, ofView: ofView)
        printIsConstrained(attr: .bottom, ofView: ofView)
        //printIsConstrained(attr: .leading, ofView: ofView)
        //printIsConstrained(attr: .trailing, ofView: ofView)
        //printIsConstrained(attr: .width, ofView: ofView)
        printIsConstrained(attr: .height, ofView: ofView)
        //printIsConstrained(attr: .centerX, ofView: ofView)
        //printIsConstrained(attr: .centerY, ofView: ofView)
    }

    func printIsConstrained(attr: NSLayoutConstraint.Attribute, ofView:UIView?=nil) {
        if !App.instance.commonHelper.isDebug {
            return
        }
        let useView:UIView = ofView != nil ? ofView! : self
        var superview:UIView? = useView
        var level:Int = -1
        var hasConstraint:Bool = false
        var strings:[String] = []
        while superview != nil {
            level += 1
            for checkConstraint:NSLayoutConstraint in superview!.constraints {
                var altItem:UIView? = nil
                var isConstrained:Bool = false
                var isSecond:Bool = false
                if checkConstraint.firstItem as? UIView == useView {
                    if checkConstraint.firstAttribute == attr {
                        isConstrained = true
                        hasConstraint = true
                        altItem = checkConstraint.secondItem as? UIView
                    }
                } else if checkConstraint.secondItem as? UIView == useView {
                    if checkConstraint.secondAttribute == attr {
                        isConstrained = true
                        hasConstraint = true
                        altItem = checkConstraint.firstItem as? UIView
                        isSecond = true
                    }
                }
                if isConstrained {
                    var str:String = "Is constrained"
                    if altItem != nil {
                        str += " to item"
                        if let altAppView:AppView = altItem! as? AppView {
                            str += " id: "+altAppView.identifier+", "
                        }
                        str += " type of: "+NSStringFromClass(type(of: altItem!))
                        str += !isSecond ? ". Is first" : ". Is second"
                    } else {
                        str += " to a non-view"
                    }
                    str += checkConstraint.relation == .equal ? ". Is equal" : (checkConstraint.relation == .greaterThanOrEqual && !isSecond || checkConstraint.relation == .lessThanOrEqual && isSecond) ? ". Is greater than or equal" :  ". Is less than or equal"
                    str += ". Constant: " + String(Double(checkConstraint.constant))
                    str += ". At level "+String(level)
                    let priority:String = checkConstraint.priority == UILayoutPriority.required ? "required" :
                            checkConstraint.priority == UILayoutPriority.defaultHigh ? "high" :
                            checkConstraint.priority == UILayoutPriority.defaultLow ? "low"  :
                                    checkConstraint.priority == UILayoutPriority.fittingSizeLevel ? "FittingSizeLevel" : "unknown"
                    str += ". Priority "+priority
                    strings.append(str)
                }
            }
            superview = superview!.superview
        }

        if strings.count > 0 {
            var type:String? = nil
             switch attr {
                 case .left:
                     type = "left"
                    break
                 case .right:
                     type = "right"
                     break
                 case .top:
                     type = "top"
                     break
                 case .bottom:
                     type = "bottom"
                     break
                 case .leading:
                     type = "leading"
                     break
                 case .trailing:
                     type = "trailing"
                     break
                 case .width:
                     type = "width"
                     break
                 case .height:
                     type = "height"
                     break
                 case .centerX:
                     type = "centerX"
                     break
                 case .centerY:
                     type = "centerY"
                     break
                default:
                    type = "other"
                    break
             }
            print(type! + " constraints:")
            for str:String in strings {
                print(str)
            }
        }
        if !hasConstraint {
            //print("Has no relevant constraints")
        }
    }

    func scaleToScreenWidth( withMargin:Bool=false, allowIncrease:Bool=false ) {
        let screenSize:CGSize = UIScreen.main.bounds.size
        var width:CGFloat = screenSize.width
        if withMargin {
            width -= (Viewed.instance.viewConfig.topLeftMargin.x + Viewed.instance.viewConfig.topLeftMargin.x )
        }
        scaleToFit( width: width, allowIncrease:allowIncrease )
    }

    func scaleToFit( width:CGFloat?=nil, height:CGFloat?=nil, allowIncrease:Bool=false  ) {

        self.transform = CGAffineTransform(scaleX: 1, y: 1)

        if width == nil && height == nil {
            return
        }
        var scaleBy:CGFloat = 1
        if width != nil {
            if self.width > width! || allowIncrease {
                if abs( self.width - width! ) > 1 {
                    scaleBy = width! / self.width
                }
            }
        }
        if height != nil {
            if self.height > height! || allowIncrease {
                if abs( self.height - height! ) > 1 {
                    let scaleByY:CGFloat = height! / self.height
                    if scaleBy == 1 || ( scaleBy < 1 && scaleByY < scaleBy ) || ( scaleBy > 1 && scaleByY > scaleBy ) {
                        scaleBy = scaleByY
                    }
                }
            }
        }
        if scaleBy == 1 {
            return
        }
        self.transform = CGAffineTransform(scaleX: scaleBy, y: scaleBy)
    }

    func addBorder(edges: UIRectEdge, color: UIColor = UIColor.white, thickness: CGFloat = 1.0) -> [UIView] {

        var borders = [UIView]()

        func border() -> UIView {
            let border = UIView(frame: CGRect.zero)
            border.backgroundColor = color
            border.translatesAutoresizingMaskIntoConstraints = false
            return border
        }

        if edges.contains(.top) || edges.contains(.all) {
            let top = border()
            addSubview(top)
            addConstraints(
                    NSLayoutConstraint.constraints(withVisualFormat: "V:|-(0)-[top(==thickness)]",
                            options: [],
                            metrics: ["thickness": thickness],
                            views: ["top": top]))
            addConstraints(
                    NSLayoutConstraint.constraints(withVisualFormat: "H:|-(0)-[top]-(0)-|",
                            options: [],
                            metrics: nil,
                            views: ["top": top]))
            borders.append(top)
        }

        if edges.contains(.left) || edges.contains(.all) {
            let left = border()
            addSubview(left)
            addConstraints(
                    NSLayoutConstraint.constraints(withVisualFormat: "H:|-(0)-[left(==thickness)]",
                            options: [],
                            metrics: ["thickness": thickness],
                            views: ["left": left]))
            addConstraints(
                    NSLayoutConstraint.constraints(withVisualFormat: "V:|-(0)-[left]-(0)-|",
                            options: [],
                            metrics: nil,
                            views: ["left": left]))
            borders.append(left)
        }

        if edges.contains(.right) || edges.contains(.all) {
            let right = border()
            addSubview(right)
            addConstraints(
                    NSLayoutConstraint.constraints(withVisualFormat: "H:[right(==thickness)]-(0)-|",
                            options: [],
                            metrics: ["thickness": thickness],
                            views: ["right": right]))
            addConstraints(
                    NSLayoutConstraint.constraints(withVisualFormat: "V:|-(0)-[right]-(0)-|",
                            options: [],
                            metrics: nil,
                            views: ["right": right]))
            borders.append(right)
        }

        if edges.contains(.bottom) || edges.contains(.all) {
            let bottom = border()
            addSubview(bottom)
            addConstraints(
                    NSLayoutConstraint.constraints(withVisualFormat: "V:[bottom(==thickness)]-(0)-|",
                            options: [],
                            metrics: ["thickness": thickness],
                            views: ["bottom": bottom]))
            addConstraints(
                    NSLayoutConstraint.constraints(withVisualFormat: "H:|-(0)-[bottom]-(0)-|",
                            options: [],
                            metrics: nil,
                            views: ["bottom": bottom]))
            borders.append(bottom)
        }

        return borders
    }

    func removeAll() {
        subviews.forEach({ $0.removeFromSuperview() })
        _ = subviews.map({ $0.removeFromSuperview() })
    }

}
