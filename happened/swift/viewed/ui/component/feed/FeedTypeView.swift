import UIKit

class FeedTypeView: AppView {

    private var viewed: Viewed { return Viewed.instance }

    var mainContentView: AppView = AppView()
    var stateSegment: SegmentedControl = SegmentedControl()

    override func initializeView() {
        super.initializeView()
        mainContentView.identifier = "FeedTypeView - mainContentView"
    }

    override func addChildViews() {
        super.addChildViews()
        addStateSegment()
        addMainContentView()
    }

    private func addStateSegment() {
        self.addSubview(stateSegment)

        let topConstraint:NSLayoutConstraint = NSLayoutConstraint(item: stateSegment, attribute: .top, relatedBy: .equal, toItem: self, attribute: .top, multiplier: 1, constant: 0)
        let trailingConstraint:NSLayoutConstraint = NSLayoutConstraint(item: stateSegment, attribute: .trailing, relatedBy: .equal, toItem: self, attribute: .trailing, multiplier: 1, constant: -viewed.viewConfig.bottomRightMargin.x)
        stateSegment.addAppConstraint(topConstraint, self)
        stateSegment.addAppConstraint(trailingConstraint, self)

    }

    private func addMainContentView() {
        self.addSubview(mainContentView)

        let topConstraint:NSLayoutConstraint = NSLayoutConstraint(item: mainContentView, attribute: .top, relatedBy: .equal, toItem: stateSegment, attribute: .bottom, multiplier: 1, deviceConstant: 8)
        let leadingConstraint:NSLayoutConstraint =  NSLayoutConstraint(item: mainContentView, attribute: .leading, relatedBy: .equal, toItem: self, attribute: .leading, multiplier: 1, constant: 0)
        let trailingConstraint:NSLayoutConstraint = NSLayoutConstraint(item: mainContentView, attribute: .trailing, relatedBy: .equal, toItem: self, attribute: .trailing, multiplier: 1, constant: 0)
        let bottomConstraint:NSLayoutConstraint = NSLayoutConstraint(item: mainContentView, attribute: .bottom, relatedBy: .equal, toItem: self, attribute: .bottom, multiplier: 1, constant: 0)
        mainContentView.addAppConstraint(topConstraint, self)
        mainContentView.addAppConstraint(leadingConstraint, self)
        mainContentView.addAppConstraint(trailingConstraint, self)
        mainContentView.addAppConstraint(bottomConstraint, self)
    }

    var isStateSegmentHidden:Bool {
        get {
            return stateSegment.isHidden
        }
        set {
            stateSegment.isHidden = newValue
            if newValue {
                let heightConstraint:NSLayoutConstraint = NSLayoutConstraint(item: stateSegment, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 0)
                stateSegment.addAppConstraint(heightConstraint)
            } else {
                stateSegment.removeAppConstraint(.height)
            }
        }
    }
}
