import UIKit

class CompetitionAccordionHeaderView:AccordionHeaderView {

    private var viewed: Viewed { return Viewed.instance }

    let selectedBtn:CompetitionMultipleSelectButton = CompetitionMultipleSelectButton()

    override func addChildViews() {
        super.addChildViews()
        addSelectedBtn()

        textView.getAppConstraint(.leading)!.constant = viewed.viewConfig.topLeftMargin.x
    }

    private func addSelectedBtn() {
        self.addSubview(selectedBtn)
        let trailingConstraint:NSLayoutConstraint = NSLayoutConstraint(item: selectedBtn, attribute: .trailing, relatedBy: .equal, toItem: self, attribute: .trailing, multiplier: 1, constant: -viewed.viewConfig.bottomRightMargin.x)
        let centerYConstraint:NSLayoutConstraint = NSLayoutConstraint(item: selectedBtn, attribute: .centerY, relatedBy: .equal, toItem: self, attribute: .centerY, multiplier: 1, constant: 0)
        selectedBtn.addAppConstraint(trailingConstraint, self)
        selectedBtn.addAppConstraint(centerYConstraint, self)

        selectedBtn.setContentHuggingPriority(UILayoutPriority.defaultHigh, for: .horizontal)

        let expandBtnTrailingConstraint:NSLayoutConstraint =  NSLayoutConstraint(item: expandBtn, attribute: .trailing, relatedBy: .equal, toItem: selectedBtn, attribute: .leading, multiplier: 1, deviceConstant: -8)
        expandBtn.addAppConstraint(expandBtnTrailingConstraint, self)
    }

    override var contentHeight:CGFloat {
        return max(selectedBtn.buttonHeight, super.height)
    }
}
