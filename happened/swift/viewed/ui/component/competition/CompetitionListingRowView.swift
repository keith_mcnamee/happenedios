import UIKit

class CompetitionListingRowView: AppView {

    private var viewed: Viewed { return Viewed.instance }

    let bulletPointView:ImageView = ImageView(image: UIImage(named: "bullet-point.png")!)
    let textView:AttributedTextView = AttributedTextView()
    let viewButton:Button = BlueButton()
    let selectedBtn:CompetitionMultipleSelectButton = CompetitionMultipleSelectButton()

    var designatedHeight:CGFloat = 0

    var topBottomPadding:CGFloat = 0

    override func addChildViews() {
        super.addChildViews()
        addBulletPointView()
        addSelectedBtn()
        addViewButton()
        addTextView()
    }

    override func applyDefaultVisuals() {
        super.applyDefaultVisuals()
        topBottomPadding = CGFloat(7).deviceValue
    }

    override func applyView() {
        super.applyView()

        designatedHeight = contentHeight + topBottomPadding * 2

        let heightConstraint:NSLayoutConstraint = NSLayoutConstraint(item: self, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: designatedHeight)
        self.addAppConstraint(heightConstraint)
    }

    private func addBulletPointView() {
        self.addSubview(bulletPointView)

        let leadingConstraint:NSLayoutConstraint = NSLayoutConstraint(item: bulletPointView, attribute: .leading, relatedBy: .equal, toItem: self, attribute: .leading, multiplier: 1, constant: (viewed.viewConfig.topLeftMargin.x + CGFloat(8).deviceValue))
        let centerYConstraint:NSLayoutConstraint = NSLayoutConstraint(item: bulletPointView, attribute: .centerY, relatedBy: .equal, toItem: self, attribute: .centerY, multiplier: 1, constant: 0)
        let widthConstraint:NSLayoutConstraint = NSLayoutConstraint(item: bulletPointView, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, deviceConstant: 16)
        let heightConstraint:NSLayoutConstraint = NSLayoutConstraint(item: bulletPointView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, deviceConstant: 16)
        bulletPointView.addAppConstraint(leadingConstraint, self)
        bulletPointView.addAppConstraint(centerYConstraint, self)
        bulletPointView.addAppConstraint(widthConstraint, self)
        bulletPointView.addAppConstraint(heightConstraint, self)

        bulletPointView.setContentHuggingPriority(UILayoutPriority.defaultHigh, for: .horizontal)
    }

    private func addSelectedBtn() {

        self.addSubview(selectedBtn)
        let trailingConstraint:NSLayoutConstraint = NSLayoutConstraint(item: selectedBtn, attribute: .trailing, relatedBy: .equal, toItem: self, attribute: .trailing, multiplier: 1, constant: -viewed.viewConfig.bottomRightMargin.x)
        let centerYConstraint:NSLayoutConstraint = NSLayoutConstraint(item: selectedBtn, attribute: .centerY, relatedBy: .equal, toItem: self, attribute: .centerY, multiplier: 1, constant: 0)
        selectedBtn.addAppConstraint(trailingConstraint, self)
        selectedBtn.addAppConstraint(centerYConstraint, self)

        selectedBtn.setContentHuggingPriority(UILayoutPriority.defaultHigh, for: .horizontal)
    }

    private func addViewButton() {
        self.addSubview(viewButton)

        let trailingConstraint:NSLayoutConstraint = NSLayoutConstraint(item: viewButton, attribute: .trailing, relatedBy: .equal, toItem: selectedBtn, attribute: .leading, multiplier: 1, deviceConstant: -8)
        let centerYConstraint:NSLayoutConstraint = NSLayoutConstraint(item: viewButton, attribute: .centerY, relatedBy: .equal, toItem: self, attribute: .centerY, multiplier: 1, constant: 0)
        viewButton.addAppConstraint(trailingConstraint, self)
        viewButton.addAppConstraint(centerYConstraint, self)

        viewButton.setContentHuggingPriority(UILayoutPriority.defaultHigh, for: .horizontal)

        viewButton.setTitle("view".localized, for: .normal)

    }

    func addTextView() {

        self.addSubview(textView)

        let leadingConstraint:NSLayoutConstraint = NSLayoutConstraint(item: textView, attribute: .leading, relatedBy: .equal, toItem: bulletPointView, attribute: .trailing, multiplier: 1, deviceConstant: 8)
        let trailingConstraint:NSLayoutConstraint = NSLayoutConstraint(item: textView, attribute: .trailing, relatedBy: .lessThanOrEqual, toItem: viewButton, attribute: .leading, multiplier: 1, deviceConstant: -8)
        let centerYConstraint:NSLayoutConstraint = NSLayoutConstraint(item: textView, attribute: .centerY, relatedBy: .equal, toItem: self, attribute: .centerY, multiplier: 1, constant: 0)
        textView.addAppConstraint(leadingConstraint, self)
        textView.addAppConstraint(trailingConstraint, self)
        textView.addAppConstraint(centerYConstraint, self)

        textView.setContentCompressionResistancePriority(UILayoutPriority.defaultLow, for: .horizontal)
    }

    var contentHeight:CGFloat {
        return max(bulletPointView.getAppConstraint(.height)!.constant, selectedBtn.buttonHeight, viewButton.buttonHeight, textView.defaultTextHeight )
    }

}
