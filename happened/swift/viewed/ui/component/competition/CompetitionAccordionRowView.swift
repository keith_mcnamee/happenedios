import UIKit

class CompetitionAccordionRowView: AccordionRowView {

    override var createHeaderView:AccordionHeaderView {
        return CompetitionAccordionHeaderView()
    }

    var competitionHeaderView:CompetitionAccordionHeaderView {
        return headerView as! CompetitionAccordionHeaderView
    }
}
