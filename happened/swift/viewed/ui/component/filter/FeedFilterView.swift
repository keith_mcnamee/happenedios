import UIKit

class FeedFilterView: InnerBackgroundView {

    private var viewed: Viewed { return Viewed.instance }

    var applyFilterToggleView: AppView = AppView()
    var applyFilterTextView: AttributedTextView = AttributedTextView()
    var applyFilterToggle: AppSwitch = AppSwitch()

    var competitionsView: AppView = AppView()
    var competitionsButtonView: AppView = AppView()
    var competitionsTextView: AttributedTextView = AttributedTextView()
    var competitionsRemoveAllButton: Button = RedButton()
    var competitionsInnerView: AppView = AppView()

    var finalButtonsView: AppView = AppView()
    var cancelBtn: Button = RedButton()
    var selectBtn: Button = BlueButton()

    var useShort = false

    override func initializeView() {
        super.initializeView()

        centerVertical = false

        competitionsView.identifier = "competitionsView"
        competitionsButtonView.identifier = "competitionsButtonView"
        competitionsInnerView.identifier = "competitionsInnerView"
        applyFilterToggleView.identifier = "applyFilterToggleView"
    }

    override func addChildViews() {
        super.addChildViews()
        addApplyFilterToggleView()
        if( !useShort ){
            addCompetitionsView()
        }
        addFinalButtonsView()
    }

    private func addApplyFilterToggleView() {
        innerBgLayer.addSubview(applyFilterToggleView)

        let topConstraint:NSLayoutConstraint = NSLayoutConstraint(item: applyFilterToggleView, attribute: .top, relatedBy: .equal, toItem: innerBgLayer, attribute: .top, multiplier: 1, constant: innerBgLayerTBPadding)
        let leadingConstraint:NSLayoutConstraint =  NSLayoutConstraint(item: applyFilterToggleView, attribute: .leading, relatedBy: .equal, toItem: innerBgLayer, attribute: .leading, multiplier: 1, constant: innerBgLayerRLPadding)
        let trailingConstraint:NSLayoutConstraint = NSLayoutConstraint(item: applyFilterToggleView, attribute: .trailing, relatedBy: .equal, toItem: innerBgLayer, attribute: .trailing, multiplier: 1, constant: -innerBgLayerRLPadding)

        applyFilterToggleView.addAppConstraint(topConstraint, innerBgLayer)
        applyFilterToggleView.addAppConstraint(leadingConstraint, innerBgLayer)
        applyFilterToggleView.addAppConstraint(trailingConstraint, innerBgLayer)

        addApplyFilterTextView()
        addApplyFilterToggle()
    }

    private func addApplyFilterTextView() {
        applyFilterToggleView.addSubview(applyFilterTextView)

        let leadingConstraint:NSLayoutConstraint =  NSLayoutConstraint(item: applyFilterTextView, attribute: .leading, relatedBy: .equal, toItem: applyFilterToggleView, attribute: .leading, multiplier: 1, constant: 0)
        let centerYConstraint:NSLayoutConstraint = NSLayoutConstraint(item: applyFilterTextView, attribute: .centerY, relatedBy: .equal, toItem: applyFilterToggle, attribute: .centerY, multiplier: 1, constant: 0)

        applyFilterTextView.addAppConstraint(leadingConstraint, applyFilterToggleView)
        applyFilterTextView.addAppConstraint(centerYConstraint, applyFilterToggleView)
    }

    private func addApplyFilterToggle() {
        applyFilterToggleView.addSubview(applyFilterToggle)

        let spacer:AppView = viewed.uiHelper.addHorizontalSpacer(toView: applyFilterToggleView, leadingToItem: applyFilterTextView, leadingToAttribute: .trailing)

        let topConstraint:NSLayoutConstraint = NSLayoutConstraint(item: applyFilterToggle, attribute: .top, relatedBy: .equal, toItem: applyFilterToggleView, attribute: .top, multiplier: 1, constant: 0)
        let leadingConstraint:NSLayoutConstraint =  NSLayoutConstraint(item: applyFilterToggle, attribute: .leading, relatedBy: .equal, toItem: spacer, attribute: .trailing, multiplier: 1, constant: 0)
        let trailingConstraint:NSLayoutConstraint = NSLayoutConstraint(item: applyFilterToggle, attribute: .trailing, relatedBy: .equal, toItem: applyFilterToggleView, attribute: .trailing, multiplier: 1, constant: 0)
        let bottomConstraint:NSLayoutConstraint = NSLayoutConstraint(item: applyFilterToggle, attribute: .bottom, relatedBy: .equal, toItem: applyFilterToggleView, attribute: .bottom, multiplier: 1, constant: 0)

        applyFilterToggle.addAppConstraint(topConstraint, applyFilterToggleView)
        applyFilterToggle.addAppConstraint(leadingConstraint, applyFilterToggleView)
        applyFilterToggle.addAppConstraint(trailingConstraint, applyFilterToggleView)
        applyFilterToggle.addAppConstraint(bottomConstraint, applyFilterToggleView)

    }

    private func addCompetitionsView() {
        innerBgLayer.addSubview(competitionsView)

        let topConstraint:NSLayoutConstraint = NSLayoutConstraint(item: competitionsView, attribute: .top, relatedBy: .equal, toItem: applyFilterToggleView, attribute: .bottom, multiplier: 1, deviceConstant: 8)
        let leadingConstraint:NSLayoutConstraint =  NSLayoutConstraint(item: competitionsView, attribute: .leading, relatedBy: .equal, toItem: innerBgLayer, attribute: .leading, multiplier: 1, constant: innerBgLayerRLPadding)
        let trailingConstraint:NSLayoutConstraint = NSLayoutConstraint(item: competitionsView, attribute: .trailing, relatedBy: .equal, toItem: innerBgLayer, attribute: .trailing, multiplier: 1, constant: -innerBgLayerRLPadding)
        competitionsView.addAppConstraint(topConstraint, innerBgLayer)
        competitionsView.addAppConstraint(leadingConstraint, innerBgLayer)
        competitionsView.addAppConstraint(trailingConstraint, innerBgLayer)

        addCompetitionsButtonView()
        addCompetitionsInnerView()
    }

    private func addCompetitionsButtonView() {
        competitionsView.addSubview(competitionsButtonView)

        let topConstraint:NSLayoutConstraint = NSLayoutConstraint(item: competitionsButtonView, attribute: .top, relatedBy: .equal, toItem: competitionsView, attribute: .top, multiplier: 1, constant: 0)
        let leadingConstraint:NSLayoutConstraint =  NSLayoutConstraint(item: competitionsButtonView, attribute: .leading, relatedBy: .equal, toItem: competitionsView, attribute: .leading, multiplier: 1, constant: 0)
        let trailingConstraint:NSLayoutConstraint = NSLayoutConstraint(item: competitionsButtonView, attribute: .trailing, relatedBy: .equal, toItem: competitionsView, attribute: .trailing, multiplier: 1, constant: 0)

        competitionsButtonView.addAppConstraint(topConstraint, competitionsView)
        competitionsButtonView.addAppConstraint(leadingConstraint, competitionsView)
        competitionsButtonView.addAppConstraint(trailingConstraint, competitionsView)

        addCompetitionsTextView()
        addCompetitionsRemoveAllButton()

    }

    private func addCompetitionsTextView() {

        competitionsButtonView.addSubview(competitionsTextView)

        let leadingConstraint:NSLayoutConstraint =  NSLayoutConstraint(item: competitionsTextView, attribute: .leading, relatedBy: .equal, toItem: competitionsButtonView, attribute: .leading, multiplier: 1, constant: 0)
        let centerYConstraint:NSLayoutConstraint = NSLayoutConstraint(item: competitionsTextView, attribute: .centerY, relatedBy: .equal, toItem: competitionsRemoveAllButton, attribute: .centerY, multiplier: 1, constant: 0)

        competitionsTextView.addAppConstraint(leadingConstraint, competitionsButtonView)
        competitionsTextView.addAppConstraint(centerYConstraint, competitionsButtonView)
    }

    private func addCompetitionsRemoveAllButton() {

        competitionsButtonView.addSubview(competitionsRemoveAllButton)

        let spacer:AppView = viewed.uiHelper.addHorizontalSpacer(toView: competitionsButtonView, leadingToItem: competitionsTextView, leadingToAttribute: .trailing, fillWithLowPriority: true)

        let topConstraint:NSLayoutConstraint = NSLayoutConstraint(item: competitionsRemoveAllButton, attribute: .top, relatedBy: .equal, toItem: competitionsButtonView, attribute: .top, multiplier: 1, constant: 0)
        let leadingConstraint:NSLayoutConstraint =  NSLayoutConstraint(item: competitionsRemoveAllButton, attribute: .leading, relatedBy: .equal, toItem: spacer, attribute: .trailing, multiplier: 1, constant: 0)
        let trailingConstraint:NSLayoutConstraint = NSLayoutConstraint(item: competitionsRemoveAllButton, attribute: .trailing, relatedBy: .equal, toItem: competitionsButtonView, attribute: .trailing, multiplier: 1, constant: 0)
        let bottomConstraint:NSLayoutConstraint = NSLayoutConstraint(item: competitionsRemoveAllButton, attribute: .bottom, relatedBy: .equal, toItem: competitionsButtonView, attribute: .bottom, multiplier: 1, constant: 0)

        competitionsRemoveAllButton.addAppConstraint(topConstraint, competitionsButtonView)
        competitionsRemoveAllButton.addAppConstraint(leadingConstraint, competitionsButtonView)
        competitionsRemoveAllButton.addAppConstraint(trailingConstraint, competitionsButtonView)
        competitionsRemoveAllButton.addAppConstraint(bottomConstraint, competitionsButtonView)
    }

    private func addCompetitionsInnerView() {
        competitionsView.addSubview(competitionsInnerView)

        let topConstraint:NSLayoutConstraint = NSLayoutConstraint(item: competitionsInnerView, attribute: .top, relatedBy: .equal, toItem: competitionsButtonView, attribute: .bottom, multiplier: 1, deviceConstant: 1)
        let leadingConstraint:NSLayoutConstraint =  NSLayoutConstraint(item: competitionsInnerView, attribute: .leading, relatedBy: .equal, toItem: competitionsView, attribute: .leading, multiplier: 1, constant: 0)
        let trailingConstraint:NSLayoutConstraint = NSLayoutConstraint(item: competitionsInnerView, attribute: .trailing, relatedBy: .equal, toItem: competitionsView, attribute: .trailing, multiplier: 1, constant: 0)
        let bottomConstraint:NSLayoutConstraint = NSLayoutConstraint(item: competitionsInnerView, attribute: .bottom, relatedBy: .equal, toItem: competitionsView, attribute: .bottom, multiplier: 1, constant: 0)

        competitionsInnerView.addAppConstraint(topConstraint, competitionsView)
        competitionsInnerView.addAppConstraint(leadingConstraint, competitionsView)
        competitionsInnerView.addAppConstraint(trailingConstraint, competitionsView)
        competitionsInnerView.addAppConstraint(bottomConstraint, competitionsView)
    }




    private func addFinalButtonsView() {
        innerBgLayer.addSubview(finalButtonsView)

        let topConstraint:NSLayoutConstraint = NSLayoutConstraint(item: finalButtonsView, attribute: .top, relatedBy: .equal, toItem: !useShort ? competitionsView : applyFilterToggleView, attribute: .bottom, multiplier: 1, deviceConstant: 10)
        let leadingConstraint:NSLayoutConstraint =  NSLayoutConstraint(item: finalButtonsView, attribute: .leading, relatedBy: .equal, toItem: innerBgLayer, attribute: .leading, multiplier: 1, constant: innerBgLayerRLPadding)
        let trailingConstraint:NSLayoutConstraint = NSLayoutConstraint(item: finalButtonsView, attribute: .trailing, relatedBy: .equal, toItem: innerBgLayer, attribute: .trailing, multiplier: 1, constant: -innerBgLayerRLPadding)
        let bottomConstraint:NSLayoutConstraint = NSLayoutConstraint(item: finalButtonsView, attribute: .bottom, relatedBy: .equal, toItem: innerBgLayer, attribute: .bottom, multiplier: 1, constant: -innerBgLayerTBPadding)

        finalButtonsView.addAppConstraint(topConstraint, innerBgLayer)
        finalButtonsView.addAppConstraint(leadingConstraint, innerBgLayer)
        finalButtonsView.addAppConstraint(trailingConstraint, innerBgLayer)
        finalButtonsView.addAppConstraint(bottomConstraint, innerBgLayer)

        addCancelBtn()
        addSelectBtn()
    }

    private func addCancelBtn() {
        finalButtonsView.addSubview(cancelBtn)

        let leadingConstraint:NSLayoutConstraint =  NSLayoutConstraint(item: cancelBtn, attribute: .leading, relatedBy: .equal, toItem: finalButtonsView, attribute: .leading, multiplier: 1, constant: 0)
        let centerYConstraint:NSLayoutConstraint = NSLayoutConstraint(item: cancelBtn, attribute: .centerY, relatedBy: .equal, toItem: selectBtn, attribute: .centerY, multiplier: 1, constant: 0)

        cancelBtn.addAppConstraint(leadingConstraint, finalButtonsView)
        cancelBtn.addAppConstraint(centerYConstraint, finalButtonsView)
    }

    private func addSelectBtn() {
        finalButtonsView.addSubview(selectBtn)

        let spacer:AppView = viewed.uiHelper.addHorizontalSpacer(toView: finalButtonsView, leadingToItem: cancelBtn, leadingToAttribute: .trailing, fillWithLowPriority: true)

        let topConstraint:NSLayoutConstraint = NSLayoutConstraint(item: selectBtn, attribute: .top, relatedBy: .equal, toItem: finalButtonsView, attribute: .top, multiplier: 1, constant: 0)
        let leadingConstraint:NSLayoutConstraint =  NSLayoutConstraint(item: selectBtn, attribute: .leading, relatedBy: .equal, toItem: spacer, attribute: .trailing, multiplier: 1, constant: 0)
        let trailingConstraint:NSLayoutConstraint = NSLayoutConstraint(item: selectBtn, attribute: .trailing, relatedBy: .equal, toItem: finalButtonsView, attribute: .trailing, multiplier: 1, constant: 0)
        let bottomConstraint:NSLayoutConstraint = NSLayoutConstraint(item: selectBtn, attribute: .bottom, relatedBy: .equal, toItem: finalButtonsView, attribute: .bottom, multiplier: 1, constant: 0)

        selectBtn.addAppConstraint(topConstraint, finalButtonsView)
        selectBtn.addAppConstraint(leadingConstraint, finalButtonsView)
        selectBtn.addAppConstraint(trailingConstraint, finalButtonsView)
        selectBtn.addAppConstraint(bottomConstraint, finalButtonsView)
    }
}
