import UIKit

class EventFeedFilterView: FeedFilterView {

    private var viewed: Viewed { return Viewed.instance }

    var happenStateSegment: SegmentedControl = SegmentedControl()
    var eventsView: AppView = AppView()
    var eventsButtonView: AppView = AppView()
    var eventsTextView: AttributedTextView = AttributedTextView()
    var eventsRemoveAllButton: Button = RedButton()
    var eventsInnerView: AppView = AppView()

    override func addChildViews() {
        super.addChildViews()

        if( !useShort ){
            addHappenStateSegment()
            addEventsView()
        }
    }

    private func addHappenStateSegment() {
        innerBgLayer.addSubview(happenStateSegment)

        let topConstraint:NSLayoutConstraint = NSLayoutConstraint(item: happenStateSegment, attribute: .top, relatedBy: .equal, toItem: applyFilterToggleView, attribute: .bottom, multiplier: 1, deviceConstant: 7)
        let leadingConstraint:NSLayoutConstraint =  NSLayoutConstraint(item: happenStateSegment, attribute: .leading, relatedBy: .greaterThanOrEqual, toItem: innerBgLayer, attribute: .leading, multiplier: 1, constant: innerBgLayerRLPadding)
        let trailingConstraint:NSLayoutConstraint = NSLayoutConstraint(item: happenStateSegment, attribute: .trailing, relatedBy: .lessThanOrEqual, toItem: innerBgLayer, attribute: .trailing, multiplier: 1, constant: -innerBgLayerRLPadding)
        let centerXConstraint:NSLayoutConstraint = NSLayoutConstraint(item: happenStateSegment, attribute: .centerX, relatedBy: .equal, toItem: innerBgLayer, attribute: .centerX, multiplier: 1, constant: 0)

        happenStateSegment.addAppConstraint(topConstraint, innerBgLayer)
        happenStateSegment.addAppConstraint(leadingConstraint, innerBgLayer)
        happenStateSegment.addAppConstraint(trailingConstraint, innerBgLayer)
        happenStateSegment.addAppConstraint(centerXConstraint, innerBgLayer)

        let competitionsViewTopConstraint:NSLayoutConstraint = NSLayoutConstraint(item: competitionsView, attribute: .top, relatedBy: .equal, toItem: happenStateSegment, attribute: .bottom, multiplier: 1, deviceConstant: 22)
        competitionsView.addAppConstraint(competitionsViewTopConstraint, innerBgLayer)
    }

    private func addEventsView() {
        innerBgLayer.addSubview(eventsView)

        let topConstraint:NSLayoutConstraint = NSLayoutConstraint(item: eventsView, attribute: .top, relatedBy: .equal, toItem: competitionsView, attribute: .bottom, multiplier: 1, deviceConstant: 11)
        let leadingConstraint:NSLayoutConstraint =  NSLayoutConstraint(item: eventsView, attribute: .leading, relatedBy: .equal, toItem: innerBgLayer, attribute: .leading, multiplier: 1, constant: innerBgLayerRLPadding)
        let trailingConstraint:NSLayoutConstraint = NSLayoutConstraint(item: eventsView, attribute: .trailing, relatedBy: .equal, toItem: innerBgLayer, attribute: .trailing, multiplier: 1, constant: -innerBgLayerRLPadding)

        eventsView.addAppConstraint(topConstraint, innerBgLayer)
        eventsView.addAppConstraint(leadingConstraint, innerBgLayer)
        eventsView.addAppConstraint(trailingConstraint, innerBgLayer)

        addEventsButtonView()
        addEventsInnerView()
    }

    private func addEventsButtonView() {

        eventsView.addSubview(eventsButtonView)

        let topConstraint:NSLayoutConstraint = NSLayoutConstraint(item: eventsButtonView, attribute: .top, relatedBy: .equal, toItem: eventsView, attribute: .top, multiplier: 1, constant: 0)
        let leadingConstraint:NSLayoutConstraint =  NSLayoutConstraint(item: eventsButtonView, attribute: .leading, relatedBy: .equal, toItem: eventsView, attribute: .leading, multiplier: 1, constant: 0)
        let trailingConstraint:NSLayoutConstraint = NSLayoutConstraint(item: eventsButtonView, attribute: .trailing, relatedBy: .equal, toItem: eventsView, attribute: .trailing, multiplier: 1, constant: 0)

        eventsButtonView.addAppConstraint(topConstraint, eventsView)
        eventsButtonView.addAppConstraint(leadingConstraint, eventsView)
        eventsButtonView.addAppConstraint(trailingConstraint, eventsView)

        addEventsTextView()
        addEventsRemoveAllButton()
    }

    private func addEventsTextView() {
        eventsButtonView.addSubview(eventsTextView)

        let leadingConstraint:NSLayoutConstraint =  NSLayoutConstraint(item: eventsTextView, attribute: .leading, relatedBy: .equal, toItem: eventsButtonView, attribute: .leading, multiplier: 1, constant: 0)
        let centerYConstraint:NSLayoutConstraint = NSLayoutConstraint(item: eventsTextView, attribute: .centerY, relatedBy: .equal, toItem: eventsRemoveAllButton, attribute: .centerY, multiplier: 1, constant: 0)

        eventsTextView.addAppConstraint(leadingConstraint, eventsButtonView)
        eventsTextView.addAppConstraint(centerYConstraint, eventsButtonView)
    }

    private func addEventsRemoveAllButton() {
        eventsButtonView.addSubview(eventsRemoveAllButton)

        let spacer:AppView = viewed.uiHelper.addHorizontalSpacer(toView: eventsButtonView, leadingToItem: eventsTextView, leadingToAttribute: .trailing, fillWithLowPriority: true)

        let topConstraint:NSLayoutConstraint = NSLayoutConstraint(item: eventsRemoveAllButton, attribute: .top, relatedBy: .equal, toItem: eventsButtonView, attribute: .top, multiplier: 1, constant: 0)
        let leadingConstraint:NSLayoutConstraint =  NSLayoutConstraint(item: eventsRemoveAllButton, attribute: .leading, relatedBy: .equal, toItem: spacer, attribute: .trailing, multiplier: 1, constant: 0)
        let trailingConstraint:NSLayoutConstraint = NSLayoutConstraint(item: eventsRemoveAllButton, attribute: .trailing, relatedBy: .equal, toItem: eventsButtonView, attribute: .trailing, multiplier: 1, constant: 0)
        let bottomConstraint:NSLayoutConstraint = NSLayoutConstraint(item: eventsRemoveAllButton, attribute: .bottom, relatedBy: .equal, toItem: eventsButtonView, attribute: .bottom, multiplier: 1, constant: 0)

        eventsRemoveAllButton.addAppConstraint(topConstraint, eventsButtonView)
        eventsRemoveAllButton.addAppConstraint(leadingConstraint, eventsButtonView)
        eventsRemoveAllButton.addAppConstraint(trailingConstraint, eventsButtonView)
        eventsRemoveAllButton.addAppConstraint(bottomConstraint, eventsButtonView)
    }

    private func addEventsInnerView() {
        eventsView.addSubview(eventsInnerView)

        let topConstraint:NSLayoutConstraint = NSLayoutConstraint(item: eventsInnerView, attribute: .top, relatedBy: .equal, toItem: eventsButtonView, attribute: .bottom, multiplier: 1, deviceConstant: 1)
        let leadingConstraint:NSLayoutConstraint =  NSLayoutConstraint(item: eventsInnerView, attribute: .leading, relatedBy: .equal, toItem: eventsView, attribute: .leading, multiplier: 1, constant: 0)
        let trailingConstraint:NSLayoutConstraint = NSLayoutConstraint(item: eventsInnerView, attribute: .trailing, relatedBy: .equal, toItem: eventsView, attribute: .trailing, multiplier: 1, constant: 0)
        let bottomConstraint:NSLayoutConstraint = NSLayoutConstraint(item: eventsInnerView, attribute: .bottom, relatedBy: .equal, toItem: eventsView, attribute: .bottom, multiplier: 1, constant: 0)

        eventsInnerView.addAppConstraint(topConstraint, eventsView)
        eventsInnerView.addAppConstraint(leadingConstraint, eventsView)
        eventsInnerView.addAppConstraint(trailingConstraint, eventsView)
        eventsInnerView.addAppConstraint(bottomConstraint, eventsView)

        let finalButtonsViewTopConstraint:NSLayoutConstraint = NSLayoutConstraint(item: finalButtonsView, attribute: .top, relatedBy: .equal, toItem: eventsView, attribute: .bottom, multiplier: 1, deviceConstant: 10)
        finalButtonsView.addAppConstraint(finalButtonsViewTopConstraint, innerBgLayer)
    }
}
