import UIKit

class AccordionHeaderView: AppView  {

    private var viewed: Viewed { return Viewed.instance }

    var designatedHeight:CGFloat = 0

    let textView:AttributedTextView = AttributedTextView()
    var expandBtn: Button = Button()

    var topBottomPadding:CGFloat = 0

    override func applyDefaultVisuals() {
        super.applyDefaultVisuals()
        textView.defaultTextColor = viewed.viewConfig.linkColor
        topBottomPadding = CGFloat(7).deviceValue
        backgroundColor = viewed.viewConfig.colorWhite2
        _ = addBorder(edges: [.bottom], color: viewed.viewConfig.colorBlue6, thickness: 1)

        expandBtn.adjustsImageWhenHighlighted = false
        expandBtn.adjustsImageWhenDisabled = false
        expandBtn.reversesTitleShadowWhenHighlighted = false
        expandBtn.showsTouchWhenHighlighted = false
        expandBtn.setTitle("", for: .normal)
        expandBtn.backgroundColor = .none
    }

    override func addChildViews() {
        super.addChildViews()

        addTextView()
        addExpandBtn()
    }

    override func applyView() {
        super.applyView()

        designatedHeight = contentHeight + topBottomPadding * 2

        let heightConstraint:NSLayoutConstraint = NSLayoutConstraint(item: self, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: designatedHeight)
        self.addAppConstraint(heightConstraint)
    }

    private func addExpandBtn() {
        self.addSubview(expandBtn)

        let topConstraint:NSLayoutConstraint = NSLayoutConstraint(item: expandBtn, attribute: .top, relatedBy: .equal, toItem: self, attribute: .top, multiplier: 1, constant: 0)
        let leadingConstraint:NSLayoutConstraint =  NSLayoutConstraint(item: expandBtn, attribute: .leading, relatedBy: .equal, toItem: self, attribute: .leading, multiplier: 1, constant: 0)
        let trailingConstraint:NSLayoutConstraint = NSLayoutConstraint(item: expandBtn, attribute: .trailing, relatedBy: .equal, toItem: self, attribute: .trailing, multiplier: 1, constant: 0)
        let bottomConstraint:NSLayoutConstraint = NSLayoutConstraint(item: expandBtn, attribute: .bottom, relatedBy: .equal, toItem: self, attribute: .bottom, multiplier: 1, constant: 0)
        expandBtn.addAppConstraint(topConstraint, self)
        expandBtn.addAppConstraint(leadingConstraint, self)
        expandBtn.addAppConstraint(trailingConstraint, self)
        expandBtn.addAppConstraint(bottomConstraint, self)
    }

    func addTextView() {

        self.addSubview(textView)

        let leadingConstraint:NSLayoutConstraint = NSLayoutConstraint(item: textView, attribute: .leading, relatedBy: .equal, toItem: self, attribute: .leading, multiplier: 1, constant: 0)
        let centerYConstraint:NSLayoutConstraint = NSLayoutConstraint(item: textView, attribute: .centerY, relatedBy: .equal, toItem: self, attribute: .centerY, multiplier: 1, constant: 0)
        textView.addAppConstraint(leadingConstraint, self)
        textView.addAppConstraint(centerYConstraint, self)
    }

    var contentHeight:CGFloat {
        return textView.font!.lineHeight
    }

}
