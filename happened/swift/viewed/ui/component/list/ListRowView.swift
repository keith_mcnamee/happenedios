import UIKit

class ListRowView: AppView {

    private var viewed: Viewed { return Viewed.instance }

    var cells: [Int: UIView] = [:]
    var multiColumnCell: UIView? = nil

    var primarySpacer:AppView? = nil
    var lastCell:UIView? = nil
    var lastItem:UIView? = nil

    var topPadding:CGFloat = 0
    var bottomPadding:CGFloat = 0

    var alignCenterY:Bool = false

    var maxSpace:CGFloat = 0
    var minSpace:CGFloat = 0

    weak var _horizontalScrollWidthConstraint:NSLayoutConstraint? = nil

    var isLandscape:Bool = UIDevice.current.orientation.isLandscape
    func applyRow(columnVOs:[ListColumnVO], rowVO:ListRowVO ) {

        if rowVO.isMultiColumn {
            applyMultiColumnRow(rowVO: rowVO)
            return
        }

        let maxWidth:CGFloat = viewed.uiHelper.maxScreenSize
        maxSpace = columnVOs.count > 2 ? maxWidth / CGFloat( (columnVOs.count  - 1 ) ) : maxWidth

        for columnType: Int in cells.keys {
            removeCell(columnType)
        }

        var leadingToItem:UIView = self
        var leadingToAttribute:NSLayoutConstraint.Attribute = .leading
        var leadingConstant:CGFloat = 0

        var index:Int = -1
        for columnVO:ListColumnVO in columnVOs {
            index += 1
            let isFirst:Bool = index == 0
            let isLast:Bool = index == columnVOs.count - 1
            let cell:UIView = rowVO.getUiViewCell(columnVO.id)!
            let nextToItem:UIView = addCell(columnVO: columnVO, cell: cell, leadingToItem:leadingToItem, leadingToAttribute:leadingToAttribute, leadingConstant:leadingConstant, isFirst:isFirst, isLast:isLast)
            leadingToItem = nextToItem
            leadingToAttribute = .trailing
            leadingConstant = minSpace
        }
    }

    func applyMultiColumnRow(rowVO:ListRowVO ) {
        multiColumnCell = rowVO.multiColumnCell as? UIView

        self.addSubview(multiColumnCell!)

        let topConstraint:NSLayoutConstraint = NSLayoutConstraint(item: multiColumnCell!, attribute: .top, relatedBy: !alignCenterY ? .equal : .greaterThanOrEqual, toItem: self, attribute: .top, multiplier: 1, constant: topPadding)
        let leadingConstraint:NSLayoutConstraint =  NSLayoutConstraint(item: multiColumnCell!, attribute: .leading, relatedBy: .equal, toItem: self, attribute: .leading, multiplier: 1, constant: 0)
        let trailingConstraint:NSLayoutConstraint =  NSLayoutConstraint(item: multiColumnCell!, attribute: .trailing, relatedBy: .equal, toItem: self, attribute: .trailing, multiplier: 1, constant: 0)
        let bottomConstraint:NSLayoutConstraint = NSLayoutConstraint(item: multiColumnCell!, attribute: .bottom, relatedBy: !alignCenterY ? .equal : .lessThanOrEqual, toItem: self, attribute: .bottom, multiplier: 1, constant: -bottomPadding)
        (multiColumnCell! as! ExtAppView).addAppConstraint(topConstraint, self, nil)
        (multiColumnCell! as! ExtAppView).addAppConstraint(leadingConstraint, self, nil)
        (multiColumnCell! as! ExtAppView).addAppConstraint(trailingConstraint, self, nil)
        (multiColumnCell! as! ExtAppView).addAppConstraint(bottomConstraint, self, nil)
        if alignCenterY {
            let centerYConstraint:NSLayoutConstraint =  NSLayoutConstraint(item: multiColumnCell!, attribute: .centerY, relatedBy: .equal, toItem: self, attribute: .centerY, multiplier: 1, constant: 0)
            (multiColumnCell! as! ExtAppView).addAppConstraint(centerYConstraint, self, nil)
        }
    }

    func removeCell(_ columnType: Int) {
        if let cell: UIView = cells[columnType] {
            cell.removeFromSuperview()
            cells.removeValue(forKey: columnType)
        }
    }

    func addCell(columnVO: ListColumnVO, cell: UIView, leadingToItem:UIView, leadingToAttribute:NSLayoutConstraint.Attribute, leadingConstant:CGFloat, isFirst:Bool, isLast:Bool ) -> UIView {

        cells[columnVO.id] = cell

        var useLeadingToItem:UIView = leadingToItem
        var useLeadingToAttribute:NSLayoutConstraint.Attribute = leadingToAttribute
        var useLeadingConstant:CGFloat = leadingConstant

        if isFirst && isLast {
            useLeadingToItem = addSpacer( leadingToItem:useLeadingToItem, leadingToAttribute:useLeadingToAttribute )
            useLeadingToAttribute = .trailing
            useLeadingConstant = 0
        }
        if let attributedTextView:AttributedTextView = cell as? AttributedTextView {
            for constraint:NSLayoutConstraint in attributedTextView.constraints {
                attributedTextView.removeConstraint(constraint)
            }
        }

        self.addSubview(cell)

        let topConstraint:NSLayoutConstraint = NSLayoutConstraint(item: cell, attribute: .top, relatedBy: !alignCenterY ? .equal : .greaterThanOrEqual, toItem: self, attribute: .top, multiplier: 1, constant: topPadding)
        let leadingConstraint:NSLayoutConstraint =  NSLayoutConstraint(item: cell, attribute: .leading, relatedBy: .equal, toItem: useLeadingToItem, attribute: useLeadingToAttribute, multiplier: 1, constant: useLeadingConstant)
        let bottomConstraint:NSLayoutConstraint = NSLayoutConstraint(item: cell, attribute: .bottom, relatedBy: !alignCenterY ? .equal : .lessThanOrEqual, toItem: self, attribute: .bottom, multiplier: 1, constant: -bottomPadding)
        (cell as! ExtAppView).addAppConstraint(topConstraint, self, nil)
        (cell as! ExtAppView).addAppConstraint(leadingConstraint, self, nil)
        (cell as! ExtAppView).addAppConstraint(bottomConstraint, self, nil)
        if alignCenterY {
            let centerYConstraint:NSLayoutConstraint =  NSLayoutConstraint(item: cell, attribute: .centerY, relatedBy: .equal, toItem: self, attribute: .centerY, multiplier: 1, constant: 0)
            (cell as! ExtAppView).addAppConstraint(centerYConstraint, self, nil)
        }

        var nextToItem:UIView? = nil
        if !isLast {
            nextToItem = addSpacer( leadingToItem: cell, leadingToAttribute:.trailing )
        } else {
            lastCell = cell
            lastItem = cell
            if isFirst {
                lastItem = addSpacer( leadingToItem: cell, leadingToAttribute:.trailing )
            }
            let trailingConstraint:NSLayoutConstraint =  NSLayoutConstraint(item: lastItem!, attribute: .trailing, relatedBy: .equal, toItem: self, attribute: .trailing, multiplier: 1, constant: 0)
            (lastItem! as! ExtAppView).addAppConstraint(trailingConstraint, self, nil)
            nextToItem = self
        }
        enableSpacer(false)
        enableTrailingConstraint(false)
        return nextToItem!
    }

    func addSpacer( leadingToItem:UIView, leadingToAttribute:NSLayoutConstraint.Attribute ) -> AppView {
        let spacer:AppView = viewed.uiHelper.addHorizontalSpacer(toView: self, leadingToItem: leadingToItem, leadingToAttribute: leadingToAttribute, primarySpacer: primarySpacer, maxWidth: maxSpace)
        if primarySpacer == nil {
            primarySpacer = spacer
        }
        return spacer
    }

    func enableSpacer( _ enable:Bool ) {
        primarySpacer?.getAppConstraint(.width)!.constant = enable ? maxSpace : 0
    }

    func updatePrimarySpacer( _ totalWidth:CGFloat ) {
        var useTotalWidth:CGFloat = 0
        if cells.count > 2 {
            useTotalWidth = totalWidth / CGFloat( (cells.count  - 1 ) )
            if useTotalWidth > maxSpace {
                maxSpace = useTotalWidth
                if primarySpacer != nil {
                    if primarySpacer!.getAppConstraint(.width)!.constant > 0 {
                        primarySpacer!.getAppConstraint(.width)!.constant = maxSpace
                    }
                }
            }
        }
    }

    func enableTrailingConstraint(_ enable:Bool ) {
        (lastItem as? ExtAppView)?.getAppConstraint(.trailing)!.isActive = enable
    }

    var rowWidth:CGFloat {
        return lastCell != nil ? lastCell!.x + lastCell!.width - (lastItem! as! ExtAppView).getAppConstraint(.trailing)!.constant : 0
    }

    func applyWidthConstraint(columnType:Int, maximumWidth:CGFloat, minimumWidth:CGFloat ) {
        let cell:UIView? = cells[columnType]
        if cell == nil {
            return
        }



        let widthConstraint:NSLayoutConstraint = NSLayoutConstraint(item: cell!, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: maximumWidth)
        widthConstraint.priority = UILayoutPriority.defaultLow
        (cell as! ExtAppView).addAppConstraint(widthConstraint, self, nil)

        let minimumWidthConstraint:NSLayoutConstraint = NSLayoutConstraint(item: cell!, attribute: .width, relatedBy: .greaterThanOrEqual, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: minimumWidth)
        cell!.addConstraint(minimumWidthConstraint)

        let maximumWidthConstraint:NSLayoutConstraint = NSLayoutConstraint(item: cell!, attribute: .width, relatedBy: .lessThanOrEqual, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: maximumWidth)
        cell!.addConstraint(maximumWidthConstraint)
    }

    func applyMatchWidthConstraint(columnType:Int, matchCell:UIView ) {
        let cell:UIView? = cells[columnType]
        if cell == nil {
            return
        }

        let widthConstraint:NSLayoutConstraint = NSLayoutConstraint(item: cell!, attribute: .width, relatedBy: .equal, toItem: matchCell, attribute: .width, multiplier: 1, constant: 0)
        (cell as! ExtAppView).addAppConstraint(widthConstraint, superview, nil)

    }

    func enableHorizontalScroll() {

        if _horizontalScrollWidthConstraint == nil {
            let horizontalScrollWidthConstraint :NSLayoutConstraint = NSLayoutConstraint(item: self, attribute: .width, relatedBy: .equal, toItem: .none, attribute: .notAnAttribute, multiplier: 1, constant: 0)
            horizontalScrollWidthConstraint.priority = UILayoutPriority.defaultHigh
            self.addConstraint( horizontalScrollWidthConstraint )
            _horizontalScrollWidthConstraint = horizontalScrollWidthConstraint
            applyHorizontalConstant()
        }
    }

    func applyHorizontalConstant() {
        if _horizontalScrollWidthConstraint != nil {
            let widthValue:CGFloat = UIScreen.main.bounds.width
            _horizontalScrollWidthConstraint!.constant = widthValue
        }
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        if isLandscape != UIDevice.current.orientation.isLandscape {
            isLandscape = UIDevice.current.orientation.isLandscape
            applyHorizontalConstant()
        }
    }

    var isMultiColumn:Bool {
        return multiColumnCell != nil
    }

    override func destroy(){
        cells = [:]
        multiColumnCell = nil
        primarySpacer = nil
        lastItem = nil
        super.destroy()
    }
}
