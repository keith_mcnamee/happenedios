import UIKit

class AccordionRowView:ListRowView {

    private var viewed: Viewed { return Viewed.instance }

    var mainContentView: AppView = AppView()
    var headerView:AccordionHeaderView? = nil

    var id:Int? = nil
    var parentID:Int? = nil
    var childIDs:[Int:NSNull] = [:]

    var _isOpen:Bool = true

    var expansionHeight:CGFloat = 60
    var fixedExpansionHeight:CGFloat?

    override func applyDefaultVisuals() {
    }

    override func initializeView() {
        super.initializeView()

        headerView = createHeaderView

        backgroundColor = .white
        clipsToBounds = true
    }

    override func addChildViews() {
        super.addChildViews()

        addMainContentView()
        addHeaderView()
    }

    override func applyView() {
        super.applyView()
        let heightConstraint:NSLayoutConstraint = NSLayoutConstraint(item: self, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 0)
        self.addAppConstraint(heightConstraint)
    }

    private func addMainContentView() {
        self.addSubview(mainContentView)

        let topConstraint:NSLayoutConstraint = NSLayoutConstraint(item: mainContentView, attribute: .top, relatedBy: .equal, toItem: headerView!, attribute: .bottom, multiplier: 1, constant: 0)
        let leadingConstraint:NSLayoutConstraint =  NSLayoutConstraint(item: mainContentView, attribute: .leading, relatedBy: .equal, toItem: self, attribute: .leading, multiplier: 1, constant: 0)
        let trailingConstraint:NSLayoutConstraint = NSLayoutConstraint(item: mainContentView, attribute: .trailing, relatedBy: .equal, toItem: self, attribute: .trailing, multiplier: 1, constant: 0)
        let bottomConstraint:NSLayoutConstraint = NSLayoutConstraint(item: mainContentView, attribute: .bottom, relatedBy: .equal, toItem: self, attribute: .bottom, multiplier: 1, constant: 0)
        mainContentView.addAppConstraint(topConstraint, self)
        mainContentView.addAppConstraint(leadingConstraint, self)
        mainContentView.addAppConstraint(trailingConstraint, self)
        mainContentView.addAppConstraint(bottomConstraint, self)

        mainContentView.setContentHuggingPriority(UILayoutPriority.defaultLow, for: .vertical)
    }

    private func addHeaderView() {

        self.addSubview(headerView!)

        let topConstraint:NSLayoutConstraint = NSLayoutConstraint(item: headerView!, attribute: .top, relatedBy: .equal, toItem: self, attribute: .top, multiplier: 1, constant: 0)
        let leadingConstraint:NSLayoutConstraint =  NSLayoutConstraint(item: headerView!, attribute: .leading, relatedBy: .equal, toItem: self, attribute: .leading, multiplier: 1, constant: 0)
        let trailingConstraint:NSLayoutConstraint = NSLayoutConstraint(item: headerView!, attribute: .trailing, relatedBy: .equal, toItem: self, attribute: .trailing, multiplier: 1, constant: 0)
        headerView!.addAppConstraint(topConstraint, self)
        headerView!.addAppConstraint(leadingConstraint, self)
        headerView!.addAppConstraint(trailingConstraint, self)
    }

    func applyExpansionHeight(_ value:CGFloat ) {
        expansionHeight = value
        applyCurrentHeight()
    }

    func applyIsOpen(_ value:Bool ){
        _isOpen = value
        applyCurrentHeight()
    }

    func applyCurrentHeight() {
        self.getAppConstraint(.height)!.constant = !_isOpen ? headerView!.designatedHeight : headerView!.designatedHeight + expansionHeight
        setNeedsLayout()
    }

    var createHeaderView:AccordionHeaderView {
        return AccordionHeaderView()
    }

    var isOpen:Bool {
        return _isOpen
    }

}
