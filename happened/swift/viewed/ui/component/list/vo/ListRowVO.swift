import UIKit

class ListRowVO {

    var cells:[Int:ExtAppView]
    var multiColumnCell:ExtAppView? = nil
    var attributes:[AppViewAttribute:Any]

    init (cells:[Int:ExtAppView] = [:], attributes:[AppViewAttribute:Any] = [:] ){
        self.cells = cells
        self.attributes = attributes
    }

    func addListCell(_ columnID:Int?=nil, _ childView:ExtAppView?=nil, xPos:AppViewAttribute = .left, yPos:AppViewAttribute = .top ){
        let cell:ListCellView = ListCellView()
        if childView != nil {
            cell.addChildView(childView!, xPos: xPos, yPos:yPos)
        }
        addCell(columnID, cell )
    }

    func addCell(_ columnID:Int?=nil, _ childView:ExtAppView){
        if columnID != nil {
            cells[ columnID! ] = childView
        } else {
            multiColumnCell = childView
        }
    }

    func getUiViewCell(_ columnID:Int ) -> UIView? {
        return cells[columnID] as? UIView
    }

    var isMultiColumn:Bool {
        return multiColumnCell != nil
    }

    func destroy(){
        cells = [:]
        multiColumnCell = nil
        attributes = [:]
    }

    deinit {
        destroy()
    }
}
