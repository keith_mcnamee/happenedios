import Foundation

class ListColumnVO {

    var id:Int
    var attributes:[AppViewAttribute:Any]

    init ( _ id:Int, attributes:[AppViewAttribute:Any] = [:] ){
        self.id = id
        self.attributes = attributes
    }
}
