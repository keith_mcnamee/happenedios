import UIKit

class FeedStateView: AppView {

    private var viewed: Viewed { return Viewed.instance }

    var specifiedFilterView: AppView = AppView()
    var isSpecifiedTeamsSegment: SegmentedControl = SegmentedControl()
    var filterBtn: FilterButton = FilterButton()
    var mainContentView: AppView = AppView()
    var topPageControlView: AppView = AppView()
    var topPageControl: PageControl = PageControl()
    var topBtnView: AppView = AppView()
    var topPrevBtn: Button = BlueButton()
    var topNextBtn: Button = BlueButton()
    var bottomPageControlView: AppView = AppView()
    var bottomPageControl: PageControl = PageControl()
    var bottomBtnView: AppView = AppView()
    var bottomPrevBtn: Button = BlueButton()
    var bottomNextBtn: Button = BlueButton()
    var noContentContainer: AppView = AppView()

    override func initializeView() {
        super.initializeView()
        self.identifier = "FeedStateView"
    }

    override func addChildViews() {
        super.addChildViews()
        addSpecifiedFilterView()
        addMainContentView()
        addTopPageControlView()
        addBottomPageControlView()
    }

    private func addSpecifiedFilterView() {
        self.addSubview(specifiedFilterView)
        let topConstraint:NSLayoutConstraint = NSLayoutConstraint(item: specifiedFilterView, attribute: .top, relatedBy: .equal, toItem: self, attribute: .top, multiplier: 1, constant: 0)
        let leadingConstraint:NSLayoutConstraint =  NSLayoutConstraint(item: specifiedFilterView, attribute: .leading, relatedBy: .equal, toItem: self, attribute: .leading, multiplier: 1, constant: viewed.viewConfig.topLeftMargin.x)
        let trailingConstraint:NSLayoutConstraint = NSLayoutConstraint(item: specifiedFilterView, attribute: .trailing, relatedBy: .equal, toItem: self, attribute: .trailing, multiplier: 1, constant: -viewed.viewConfig.bottomRightMargin.x)
        specifiedFilterView.addAppConstraint(topConstraint, self)
        specifiedFilterView.addAppConstraint(leadingConstraint, self)
        specifiedFilterView.addAppConstraint(trailingConstraint, self)

        addIsSpecifiedTeamsSegment()
        addFilterBtn()

    }

    private func addIsSpecifiedTeamsSegment() {
        specifiedFilterView.addSubview(isSpecifiedTeamsSegment)

        let trailingConstraint:NSLayoutConstraint =  NSLayoutConstraint(item: isSpecifiedTeamsSegment, attribute: .trailing, relatedBy: .equal, toItem: specifiedFilterView, attribute: .trailing, multiplier: 1, constant: 0)
        let centerYConstraint:NSLayoutConstraint = NSLayoutConstraint(item: isSpecifiedTeamsSegment, attribute: .centerY, relatedBy: .equal, toItem: filterBtn, attribute: .centerY, multiplier: 1, constant: 0)
        isSpecifiedTeamsSegment.addAppConstraint(trailingConstraint, specifiedFilterView)
        isSpecifiedTeamsSegment.addAppConstraint(centerYConstraint, specifiedFilterView)


    }

    private func addFilterBtn() {
        specifiedFilterView.addSubview(filterBtn)

        let topConstraint:NSLayoutConstraint = NSLayoutConstraint(item: filterBtn, attribute: .top, relatedBy: .equal, toItem: specifiedFilterView, attribute: .top, multiplier: 1, constant: 0)
        let leadingConstraint:NSLayoutConstraint =  NSLayoutConstraint(item: filterBtn, attribute: .leading, relatedBy: .equal, toItem: specifiedFilterView, attribute: .leading, multiplier: 1, constant: 0)
        let bottomConstraint:NSLayoutConstraint = NSLayoutConstraint(item: filterBtn, attribute: .bottom, relatedBy: .equal, toItem: specifiedFilterView, attribute: .bottom, multiplier: 1, constant: 0)
        filterBtn.addAppConstraint(topConstraint, specifiedFilterView)
        filterBtn.addAppConstraint(leadingConstraint, specifiedFilterView)
        filterBtn.addAppConstraint(bottomConstraint, specifiedFilterView)
    }

    private func addTopPageControlView() {
        self.addSubview(topPageControlView)
        let topConstraint:NSLayoutConstraint = NSLayoutConstraint(item: topPageControlView, attribute: .top, relatedBy: .equal, toItem: specifiedFilterView, attribute: .bottom, multiplier: 1, deviceConstant: 8)
        let leadingConstraint:NSLayoutConstraint =  NSLayoutConstraint(item: topPageControlView, attribute: .leading, relatedBy: .equal, toItem: self, attribute: .leading, multiplier: 1, constant: viewed.viewConfig.topLeftMargin.x)
        let trailingConstraint:NSLayoutConstraint = NSLayoutConstraint(item: topPageControlView, attribute: .trailing, relatedBy: .equal, toItem: self, attribute: .trailing, multiplier: 1, constant: -viewed.viewConfig.bottomRightMargin.x)
        topPageControlView.addAppConstraint(topConstraint, self)
        topPageControlView.addAppConstraint(leadingConstraint, self)
        topPageControlView.addAppConstraint(trailingConstraint, self)
        addTopBtnView()
        addTopPageControl()
    }

    private func addTopBtnView() {
        topPageControlView.addSubview(topBtnView)
        let topConstraint:NSLayoutConstraint = NSLayoutConstraint(item: topBtnView, attribute: .top, relatedBy: .equal, toItem: topPageControlView, attribute: .top, multiplier: 1, constant: 0)
        let leadingConstraint:NSLayoutConstraint =  NSLayoutConstraint(item: topBtnView, attribute: .leading, relatedBy: .equal, toItem: topPageControlView, attribute: .leading, multiplier: 1, constant: 0)
        let trailingConstraint:NSLayoutConstraint = NSLayoutConstraint(item: topBtnView, attribute: .trailing, relatedBy: .equal, toItem: topPageControlView, attribute: .trailing, multiplier: 1, constant: 0)
        topBtnView.addAppConstraint(topConstraint, topPageControlView)
        topBtnView.addAppConstraint(leadingConstraint, topPageControlView)
        topBtnView.addAppConstraint(trailingConstraint, topPageControlView)
        addTopPrevBtn()
        addTopNextBtn()
    }

    private func addTopPrevBtn() {
        topBtnView.addSubview(topPrevBtn)

        let leadingConstraint:NSLayoutConstraint =  NSLayoutConstraint(item: topPrevBtn, attribute: .leading, relatedBy: .equal, toItem: topBtnView, attribute: .leading, multiplier: 1, constant: 0)
        let centerYConstraint:NSLayoutConstraint = NSLayoutConstraint(item: topPrevBtn, attribute: .centerY, relatedBy: .equal, toItem: topNextBtn, attribute: .centerY, multiplier: 1, constant: 0)

        topPrevBtn.addAppConstraint(leadingConstraint, topBtnView)
        topPrevBtn.addAppConstraint(centerYConstraint, topBtnView)
    }

    private func addTopNextBtn() {
        topBtnView.addSubview(topNextBtn)

        let topConstraint:NSLayoutConstraint = NSLayoutConstraint(item: topNextBtn, attribute: .top, relatedBy: .equal, toItem: topBtnView, attribute: .top, multiplier: 1, constant: 0)
        let leadingConstraint:NSLayoutConstraint =  NSLayoutConstraint(item: topNextBtn, attribute: .leading, relatedBy: .equal, toItem: topPrevBtn, attribute: .trailing, multiplier: 1, constant: viewed.uiHelper.maxScreenSize)
        let trailingConstraint:NSLayoutConstraint = NSLayoutConstraint(item: topNextBtn, attribute: .trailing, relatedBy: .equal, toItem: topBtnView, attribute: .trailing, multiplier: 1, constant: 0)
        let bottomConstraint:NSLayoutConstraint = NSLayoutConstraint(item: topNextBtn, attribute: .bottom, relatedBy: .equal, toItem: topBtnView, attribute: .bottom, multiplier: 1, constant: 0)

        leadingConstraint.priority = UILayoutPriority.defaultLow

        topNextBtn.addAppConstraint(topConstraint, topBtnView)
        topNextBtn.addAppConstraint(leadingConstraint, topBtnView)
        topNextBtn.addAppConstraint(trailingConstraint, topBtnView)
        topNextBtn.addAppConstraint(bottomConstraint, topBtnView)
    }

    private func addTopPageControl() {
        topPageControlView.addSubview(topPageControl)
        let topConstraint:NSLayoutConstraint = NSLayoutConstraint(item: topPageControl, attribute: .top, relatedBy: .equal, toItem: topBtnView, attribute: .bottom, multiplier: 1, constant: 0)
        let bottomConstraint:NSLayoutConstraint = NSLayoutConstraint(item: topPageControl, attribute: .bottom, relatedBy: .equal, toItem: topPageControlView, attribute: .bottom, multiplier: 1, constant: 0)
        let centerXConstraint:NSLayoutConstraint =  NSLayoutConstraint(item: topPageControl, attribute: .centerX, relatedBy: .equal, toItem: topPageControlView, attribute: .centerX, multiplier: 1, constant: 0)
        topPageControl.addAppConstraint(topConstraint, topPageControlView)
        topPageControl.addAppConstraint(bottomConstraint, topPageControlView)
        topPageControl.addAppConstraint(centerXConstraint, topPageControlView)
    }

    private func addMainContentView() {
        self.addSubview(mainContentView)

        let topConstraint:NSLayoutConstraint = NSLayoutConstraint(item: mainContentView, attribute: .top, relatedBy: .equal, toItem: topPageControlView, attribute: .bottom, multiplier: 1, deviceConstant: 8)
        let leadingConstraint:NSLayoutConstraint =  NSLayoutConstraint(item: mainContentView, attribute: .leading, relatedBy: .equal, toItem: self, attribute: .leading, multiplier: 1, constant: 0)
        let trailingConstraint:NSLayoutConstraint = NSLayoutConstraint(item: mainContentView, attribute: .trailing, relatedBy: .equal, toItem: self, attribute: .trailing, multiplier: 1, constant: 0)
        mainContentView.addAppConstraint(topConstraint, self)
        mainContentView.addAppConstraint(leadingConstraint, self)
        mainContentView.addAppConstraint(trailingConstraint, self)
    }

    private func addBottomPageControlView() {
        self.addSubview(bottomPageControlView)
        let topConstraint:NSLayoutConstraint = NSLayoutConstraint(item: bottomPageControlView, attribute: .top, relatedBy: .equal, toItem: mainContentView, attribute: .bottom, multiplier: 1, deviceConstant: 8)
        let leadingConstraint:NSLayoutConstraint =  NSLayoutConstraint(item: bottomPageControlView, attribute: .leading, relatedBy: .equal, toItem: self, attribute: .leading, multiplier: 1, constant: viewed.viewConfig.topLeftMargin.x)
        let trailingConstraint:NSLayoutConstraint = NSLayoutConstraint(item: bottomPageControlView, attribute: .trailing, relatedBy: .equal, toItem: self, attribute: .trailing, multiplier: 1, constant: -viewed.viewConfig.bottomRightMargin.x)
        let bottomConstraint:NSLayoutConstraint = NSLayoutConstraint(item: bottomPageControlView, attribute: .bottom, relatedBy: .equal, toItem: self, attribute: .bottom, multiplier: 1, constant: 0)
        bottomPageControlView.addAppConstraint(topConstraint, self)
        bottomPageControlView.addAppConstraint(leadingConstraint, self)
        bottomPageControlView.addAppConstraint(trailingConstraint, self)
        bottomPageControlView.addAppConstraint(bottomConstraint, self)
        addBottomPageControl()
        addBottomBtnView()
    }

    private func addBottomPageControl() {
        bottomPageControlView.addSubview(bottomPageControl)
        let topConstraint:NSLayoutConstraint = NSLayoutConstraint(item: bottomPageControl, attribute: .top, relatedBy: .equal, toItem: bottomPageControlView, attribute: .top, multiplier: 1, constant: 0)
        let centerXConstraint:NSLayoutConstraint =  NSLayoutConstraint(item: bottomPageControl, attribute: .centerX, relatedBy: .equal, toItem: bottomPageControlView, attribute: .centerX, multiplier: 1, constant: 0)
        bottomPageControl.addAppConstraint(topConstraint, bottomPageControlView)
        bottomPageControl.addAppConstraint(centerXConstraint, bottomPageControlView)
    }

    private func addBottomBtnView() {
        bottomPageControlView.addSubview(bottomBtnView)
        let topConstraint:NSLayoutConstraint = NSLayoutConstraint(item: bottomBtnView, attribute: .top, relatedBy: .equal, toItem: bottomPageControl, attribute: .bottom, multiplier: 1, deviceConstant: 8)
        let leadingConstraint:NSLayoutConstraint =  NSLayoutConstraint(item: bottomBtnView, attribute: .leading, relatedBy: .equal, toItem: bottomPageControlView, attribute: .leading, multiplier: 1, constant: 0)
        let trailingConstraint:NSLayoutConstraint = NSLayoutConstraint(item: bottomBtnView, attribute: .trailing, relatedBy: .equal, toItem: bottomPageControlView, attribute: .trailing, multiplier: 1, constant: 0)
        let bottomConstraint:NSLayoutConstraint = NSLayoutConstraint(item: bottomBtnView, attribute: .bottom, relatedBy: .equal, toItem: bottomPageControlView, attribute: .bottom, multiplier: 1, constant: 0)
        bottomBtnView.addAppConstraint(topConstraint, bottomPageControlView)
        bottomBtnView.addAppConstraint(leadingConstraint, bottomPageControlView)
        bottomBtnView.addAppConstraint(trailingConstraint, bottomPageControlView)
        bottomBtnView.addAppConstraint(bottomConstraint, bottomPageControlView)
        addBottomPrevBtn()
        addBottomNextBtn()
    }

    private func addBottomPrevBtn() {
        bottomBtnView.addSubview(bottomPrevBtn)

        let leadingConstraint:NSLayoutConstraint =  NSLayoutConstraint(item: bottomPrevBtn, attribute: .leading, relatedBy: .equal, toItem: bottomBtnView, attribute: .leading, multiplier: 1, constant: 0)
        let centerYConstraint:NSLayoutConstraint = NSLayoutConstraint(item: bottomPrevBtn, attribute: .centerY, relatedBy: .equal, toItem: bottomNextBtn, attribute: .centerY, multiplier: 1, constant: 0)

        bottomPrevBtn.addAppConstraint(leadingConstraint, bottomBtnView)
        bottomPrevBtn.addAppConstraint(centerYConstraint, bottomBtnView)
    }

    private func addBottomNextBtn() {
        bottomBtnView.addSubview(bottomNextBtn)

        let topConstraint:NSLayoutConstraint = NSLayoutConstraint(item: bottomNextBtn, attribute: .top, relatedBy: .equal, toItem: bottomBtnView, attribute: .top, multiplier: 1, constant: 0)
        let leadingConstraint:NSLayoutConstraint =  NSLayoutConstraint(item: bottomNextBtn, attribute: .leading, relatedBy: .equal, toItem: bottomPrevBtn, attribute: .trailing, multiplier: 1, constant: viewed.uiHelper.maxScreenSize)
        let trailingConstraint:NSLayoutConstraint = NSLayoutConstraint(item: bottomNextBtn, attribute: .trailing, relatedBy: .equal, toItem: bottomBtnView, attribute: .trailing, multiplier: 1, constant: 0)
        let bottomConstraint:NSLayoutConstraint = NSLayoutConstraint(item: bottomNextBtn, attribute: .bottom, relatedBy: .equal, toItem: bottomBtnView, attribute: .bottom, multiplier: 1, constant: 0)

        leadingConstraint.priority = UILayoutPriority.defaultLow

        bottomNextBtn.addAppConstraint(topConstraint, bottomBtnView)
        bottomNextBtn.addAppConstraint(leadingConstraint, bottomBtnView)
        bottomNextBtn.addAppConstraint(trailingConstraint, bottomBtnView)
        bottomNextBtn.addAppConstraint(bottomConstraint, bottomBtnView)
    }

    var pageControlHidden:Bool {
         get {
             return topPageControlView.isHidden
         }
         set {
             topPageControlView.isHidden = newValue
             bottomPageControlView.isHidden = newValue
             if newValue {
                 let topHeightConstraint:NSLayoutConstraint = NSLayoutConstraint(item: topPageControlView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 0)
                 let bottomHeightConstraint:NSLayoutConstraint = NSLayoutConstraint(item: bottomPageControlView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 0)
                 topPageControlView.addAppConstraint(topHeightConstraint)
                 bottomPageControlView.addAppConstraint(bottomHeightConstraint)
             } else {
                 topPageControlView.removeAppConstraint(.height)
                 bottomPageControlView.removeAppConstraint(.height)
             }
         }
    }

    var isSpecifiedTeamsSegmentHidden:Bool {
        get {
            return isSpecifiedTeamsSegment.isHidden
        }
        set {
            isSpecifiedTeamsSegment.isHidden = newValue
            if newValue {
                let heightConstraint:NSLayoutConstraint = NSLayoutConstraint(item: isSpecifiedTeamsSegment, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 0)
                isSpecifiedTeamsSegment.addAppConstraint(heightConstraint)
            } else {
                isSpecifiedTeamsSegment.removeAppConstraint(.height)
            }
        }
    }

    var filterBtnHidden:Bool {
        get {
            return filterBtn.isHidden
        }
        set {
            filterBtn.isHidden = newValue
            if newValue {
                let heightConstraint:NSLayoutConstraint = NSLayoutConstraint(item: filterBtn, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 0)
                filterBtn.addAppConstraint(heightConstraint)
            } else {
                filterBtn.removeAppConstraint(.height)
            }
        }
    }
}
