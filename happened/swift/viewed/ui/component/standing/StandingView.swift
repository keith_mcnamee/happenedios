import UIKit

class StandingView: ScrollContainerView {

    var mainContentView: AppView = AppView()
    var accordionView: AppView = AppView()
    var typeSegment: SegmentedControl = SegmentedControl()

    override var horizontal:Bool {
        return true
    }

    override func initializeView() {
        super.initializeView()
        mainContentView.identifier = "mainContentView"
        accordionView.identifier = "accordionView"
    }

    override func addChildViews() {
        super.addChildViews()

        addMainContentView()
        addAccordionView()
    }

    private func addMainContentView() {
        innerContentView.addSubview(mainContentView)

        let topConstraint:NSLayoutConstraint = NSLayoutConstraint(item: mainContentView, attribute: .top, relatedBy: .equal, toItem: innerContentView, attribute: .top, multiplier: 1, constant: 0)
        let leadingConstraint:NSLayoutConstraint =  NSLayoutConstraint(item: mainContentView, attribute: .leading, relatedBy: .equal, toItem: innerContentView, attribute: .leading, multiplier: 1, constant: 0)
        let trailingConstraint:NSLayoutConstraint = NSLayoutConstraint(item: mainContentView, attribute: .trailing, relatedBy: .equal, toItem: innerContentView, attribute: .trailing, multiplier: 1, constant: 0)

        mainContentView.addAppConstraint(topConstraint, innerContentView)
        mainContentView.addAppConstraint(leadingConstraint, innerContentView)
        mainContentView.addAppConstraint(trailingConstraint, innerContentView)
    }

    private func addAccordionView() {
        innerContentView.addSubview(accordionView)

        let topConstraint:NSLayoutConstraint = NSLayoutConstraint(item: accordionView, attribute: .top, relatedBy: .equal, toItem: mainContentView, attribute: .bottom, multiplier: 1, constant: 0)
        let leadingConstraint:NSLayoutConstraint =  NSLayoutConstraint(item: accordionView, attribute: .leading, relatedBy: .equal, toItem: innerContentView, attribute: .leading, multiplier: 1, constant: 0)
        let trailingConstraint:NSLayoutConstraint = NSLayoutConstraint(item: accordionView, attribute: .trailing, relatedBy: .equal, toItem: innerContentView, attribute: .trailing, multiplier: 1, constant: 0)
        let bottomConstraint:NSLayoutConstraint = NSLayoutConstraint(item: accordionView, attribute: .bottom, relatedBy: .equal, toItem: innerContentView, attribute: .bottom, multiplier: 1, constant: 0)

        accordionView.addAppConstraint(topConstraint, innerContentView)
        accordionView.addAppConstraint(leadingConstraint, innerContentView)
        accordionView.addAppConstraint(trailingConstraint, innerContentView)
        accordionView.addAppConstraint(bottomConstraint, innerContentView)
    }
}

