import UIKit

class ScrollContainerView: AppView {

    var scrollView: ScrollView = ScrollView()
    var _overlayView: AppView = AppView()

    var innerContentView: AppView = AppView()

    var _horizontal:Bool = false

    var horizontal:Bool {
        return _horizontal
    }

    convenience init( horizontal:Bool ) {
        self.init(callInitialize: false)
        self._horizontal = horizontal
        initialize()
    }

    override func applyFrame() {
        super.applyFrame()
        scrollView.frame = self.bounds
    }

    override func initializeView() {
        super.initializeView()

        translatesAutoresizingMaskIntoConstraints = true
        _overlayView.backgroundColor = .clear
        backgroundColor = .white
        _overlayView.isUserInteractionEnabled = false
        innerContentView.identifier = "innerContentView"
        _overlayView.identifier = "_overlayView"
    }

    override func addChildViews() {
        super.addChildViews()
        addScrollView()
        addInnerConstraints( innerContentView )
        addInnerConstraints( _overlayView )
        enableOverlayConstraints(false)

    }

    override func applyView() {
        super.applyView()

        if horizontal {
            let heightConstraint:NSLayoutConstraint = NSLayoutConstraint(item: self, attribute: .height, relatedBy: .equal, toItem: innerContentView, attribute: .height, multiplier: 1, constant: 0)
            self.addAppConstraint(heightConstraint)
        }
    }

    private func addScrollView() {

        self.addSubview(scrollView)

        let topConstraint:NSLayoutConstraint = NSLayoutConstraint(item: scrollView, attribute: .top, relatedBy: .equal, toItem: self, attribute: .top, multiplier: 1, constant: 0)
        let leadingConstraint:NSLayoutConstraint =  NSLayoutConstraint(item: scrollView, attribute: .leading, relatedBy: .equal, toItem: self, attribute: .leading, multiplier: 1, constant: 0)
        let trailingConstraint:NSLayoutConstraint = NSLayoutConstraint(item: scrollView, attribute: .trailing, relatedBy: .equal, toItem: self, attribute: .trailing, multiplier: 1, constant: 0)
        let bottomConstraint:NSLayoutConstraint = NSLayoutConstraint(item: scrollView, attribute: .bottom, relatedBy: .equal, toItem: self, attribute: .bottom, multiplier: 1, constant: 0)

        scrollView.addAppConstraint(topConstraint, self)
        scrollView.addAppConstraint(leadingConstraint, self)
        scrollView.addAppConstraint(trailingConstraint, self)
        scrollView.addAppConstraint(bottomConstraint, self)


        if horizontal {
            let widthConstraint:NSLayoutConstraint = NSLayoutConstraint(item: scrollView, attribute: .width, relatedBy: .lessThanOrEqual, toItem: innerContentView, attribute: .width, multiplier: 1, constant: 0)
            scrollView.addAppConstraint(widthConstraint)
        }
    }

    private func addInnerConstraints( _ forView:AppView ) {

        scrollView.addSubview(forView)

        let topConstraint:NSLayoutConstraint = NSLayoutConstraint(item: forView, attribute: .top, relatedBy: .equal, toItem: scrollView, attribute: .top, multiplier: 1, constant: 0)
        let leadingConstraint:NSLayoutConstraint =  NSLayoutConstraint(item: forView, attribute: .leading, relatedBy: .equal, toItem: scrollView, attribute: .leading, multiplier: 1, constant: 0)
        let trailingConstraint:NSLayoutConstraint = NSLayoutConstraint(item: forView, attribute: .trailing, relatedBy: .equal, toItem: scrollView, attribute: .trailing, multiplier: 1, constant: 0)

        forView.addAppConstraint(topConstraint, scrollView)
        forView.addAppConstraint(leadingConstraint, scrollView)
        forView.addAppConstraint(trailingConstraint, scrollView)

        let widthConstraint:NSLayoutConstraint = NSLayoutConstraint(item: forView, attribute: .width, relatedBy: .equal, toItem: self, attribute: .width, multiplier: 1, constant: 0)
        if horizontal {
            widthConstraint.priority = UILayoutPriority.defaultLow
        }
        forView.addAppConstraint(widthConstraint, self)
    }

    func enableOverlayConstraints( _ enabled:Bool ){
        let forView:AppView = !enabled ? innerContentView : _overlayView
        let altView:AppView = !enabled ? _overlayView : innerContentView

        let forBottomConstraint:NSLayoutConstraint = NSLayoutConstraint(item: forView, attribute: .bottom, relatedBy: .equal, toItem: scrollView, attribute: .bottom, multiplier: 1, constant: 0)
        forView.addAppConstraint(forBottomConstraint, scrollView)

        let forHeightConstraint:NSLayoutConstraint = NSLayoutConstraint(item: forView, attribute: .height, relatedBy: .greaterThanOrEqual, toItem: scrollView, attribute: .height, multiplier: 1, constant: 0)
        forView.addAppConstraint(forHeightConstraint, scrollView)

        altView.removeAppConstraint(.bottom)
        altView.removeAppConstraint(.height)

        updateContentInsetDependantConstraints()
    }

    func updateContentInsetDependantConstraints() {
        innerContentView.getAppConstraint(.height)?.constant = -(scrollView.contentInset.top + scrollView.contentInset.bottom)
        _overlayView.getAppConstraint(.height)?.constant = -(scrollView.contentInset.top + scrollView.contentInset.bottom)
    }


    func resetScroll(animated:Bool = false) {
        scrollView.setContentOffset(CGPoint(x: scrollView.horizontalOffsetForLeading, y: scrollView.verticalOffsetForTop), animated: animated)
    }
}
