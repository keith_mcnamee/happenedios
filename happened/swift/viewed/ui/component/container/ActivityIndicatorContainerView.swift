import UIKit

class ActivityIndicatorContainerView: InnerBackgroundView  {

    var activityIndicator: ActivityIndicatorView = ActivityIndicatorView()

    override func initializeView() {
        super.initializeView()
        activityIndicator.style = .white
        bgLayer.backgroundColor = .black
        innerBgLayer.backgroundColor = .black
    }
    override func addChildViews() {
        super.addChildViews()
        addActivityIndicator()
    }

    private func addActivityIndicator() {
        innerBgLayer.addSubview(activityIndicator)

        let topConstraint:NSLayoutConstraint = NSLayoutConstraint(item: activityIndicator, attribute: .top, relatedBy: .equal, toItem: innerBgLayer, attribute: .top, multiplier: 1, constant: innerBgLayerTBPadding)
        let leadingConstraint:NSLayoutConstraint =  NSLayoutConstraint(item: activityIndicator, attribute: .leading, relatedBy: .greaterThanOrEqual, toItem: innerBgLayer, attribute: .leading, multiplier: 1, constant: innerBgLayerRLPadding)
        let trailingConstraint:NSLayoutConstraint = NSLayoutConstraint(item: activityIndicator, attribute: .trailing, relatedBy: .lessThanOrEqual, toItem: innerBgLayer, attribute: .trailing, multiplier: 1, constant: -innerBgLayerRLPadding)
        let bottomConstraint:NSLayoutConstraint = NSLayoutConstraint(item: activityIndicator, attribute: .bottom, relatedBy: .equal, toItem: innerBgLayer, attribute: .bottom, multiplier: 1, constant: -innerBgLayerTBPadding)

        activityIndicator.addAppConstraint(topConstraint, innerBgLayer)
        activityIndicator.addAppConstraint(leadingConstraint, innerBgLayer)
        activityIndicator.addAppConstraint(trailingConstraint, innerBgLayer)
        activityIndicator.addAppConstraint(bottomConstraint, innerBgLayer)
    }
    
}
