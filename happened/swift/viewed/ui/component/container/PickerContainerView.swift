import UIKit

class PickerContainerView: InnerBackgroundView {

    private var viewed: Viewed { return Viewed.instance }
    
    var pickerView: PickerView = PickerView()
    var finalButtonsView: AppView = AppView()
    var cancelBtn: Button = RedButton()
    var selectBtn: Button = BlueButton()

    override func addChildViews() {
        super.addChildViews()
        addPickerView()
        addFinalButtonsView()
    }

    private func addPickerView() {
        innerBgLayer.addSubview(pickerView)

        let topConstraint:NSLayoutConstraint = NSLayoutConstraint(item: pickerView, attribute: .top, relatedBy: .equal, toItem: innerBgLayer, attribute: .top, multiplier: 1, constant: innerBgLayerTBPadding)
        let leadingConstraint:NSLayoutConstraint =  NSLayoutConstraint(item: pickerView, attribute: .leading, relatedBy: .greaterThanOrEqual, toItem: innerBgLayer, attribute: .leading, multiplier: 1, constant: innerBgLayerRLPadding)
        let trailingConstraint:NSLayoutConstraint = NSLayoutConstraint(item: pickerView, attribute: .trailing, relatedBy: .lessThanOrEqual, toItem: innerBgLayer, attribute: .trailing, multiplier: 1, constant: -innerBgLayerRLPadding)
        pickerView.addAppConstraint(topConstraint, innerBgLayer)
        pickerView.addAppConstraint(leadingConstraint, innerBgLayer)
        pickerView.addAppConstraint(trailingConstraint, innerBgLayer)
    }

    private func addFinalButtonsView() {
        innerBgLayer.addSubview(finalButtonsView)

        let topConstraint:NSLayoutConstraint = NSLayoutConstraint(item: finalButtonsView, attribute: .top, relatedBy: .equal, toItem: pickerView, attribute: .bottom, multiplier: 1, constant: 10)
        let leadingConstraint:NSLayoutConstraint =  NSLayoutConstraint(item: finalButtonsView, attribute: .leading, relatedBy: .equal, toItem: innerBgLayer, attribute: .leading, multiplier: 1, constant: innerBgLayerRLPadding)
        let trailingConstraint:NSLayoutConstraint = NSLayoutConstraint(item: finalButtonsView, attribute: .trailing, relatedBy: .equal, toItem: innerBgLayer, attribute: .trailing, multiplier: 1, constant: -innerBgLayerRLPadding)
        let bottomConstraint:NSLayoutConstraint = NSLayoutConstraint(item: finalButtonsView, attribute: .bottom, relatedBy: .equal, toItem: innerBgLayer, attribute: .bottom, multiplier: 1, constant: -innerBgLayerTBPadding)

        finalButtonsView.addAppConstraint(topConstraint, innerBgLayer)
        finalButtonsView.addAppConstraint(leadingConstraint, innerBgLayer)
        finalButtonsView.addAppConstraint(trailingConstraint, innerBgLayer)
        finalButtonsView.addAppConstraint(bottomConstraint, innerBgLayer)

        addCancelBtn()
        addSelectBtn()
    }

    private func addCancelBtn() {
        finalButtonsView.addSubview(cancelBtn)

        let leadingConstraint:NSLayoutConstraint =  NSLayoutConstraint(item: cancelBtn, attribute: .leading, relatedBy: .equal, toItem: finalButtonsView, attribute: .leading, multiplier: 1, constant: 0)
        let centerYConstraint:NSLayoutConstraint = NSLayoutConstraint(item: cancelBtn, attribute: .centerY, relatedBy: .equal, toItem: selectBtn, attribute: .centerY, multiplier: 1, constant: 0)

        cancelBtn.addAppConstraint(leadingConstraint, finalButtonsView)
        cancelBtn.addAppConstraint(centerYConstraint, finalButtonsView)
    }

    private func addSelectBtn() {
        finalButtonsView.addSubview(selectBtn)

        let spacer:AppView = viewed.uiHelper.addHorizontalSpacer(toView: finalButtonsView, leadingToItem: cancelBtn, leadingToAttribute: .trailing, fillWithLowPriority: true)

        let topConstraint:NSLayoutConstraint = NSLayoutConstraint(item: selectBtn, attribute: .top, relatedBy: .equal, toItem: finalButtonsView, attribute: .top, multiplier: 1, constant: 0)
        let leadingConstraint:NSLayoutConstraint =  NSLayoutConstraint(item: selectBtn, attribute: .leading, relatedBy: .equal, toItem: spacer, attribute: .trailing, multiplier: 1, constant: 0)
        let trailingConstraint:NSLayoutConstraint = NSLayoutConstraint(item: selectBtn, attribute: .trailing, relatedBy: .equal, toItem: finalButtonsView, attribute: .trailing, multiplier: 1, constant: 0)
        let bottomConstraint:NSLayoutConstraint = NSLayoutConstraint(item: selectBtn, attribute: .bottom, relatedBy: .equal, toItem: finalButtonsView, attribute: .bottom, multiplier: 1, constant: 0)

        selectBtn.addAppConstraint(topConstraint, finalButtonsView)
        selectBtn.addAppConstraint(leadingConstraint, finalButtonsView)
        selectBtn.addAppConstraint(trailingConstraint, finalButtonsView)
        selectBtn.addAppConstraint(bottomConstraint, finalButtonsView)
    }
}
