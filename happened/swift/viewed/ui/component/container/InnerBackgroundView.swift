import UIKit

class InnerBackgroundView: AppView  {

    private var viewed: Viewed { return Viewed.instance }

    var bgLayer: AppView = AppView()
    var innerBgLayer: AppView = AppView()

    var innerBgLayerMargin:CGFloat = 16
    var innerBgLayerTBPadding:CGFloat = 12
    var innerBgLayerRLPadding:CGFloat = 12

    var centerVertical = false

    override func initializeView() {
        super.initializeView()

        bgLayer.backgroundColor = UIColor(white: 0.2, alpha: 0.8)

        bgLayer.identifier = "bgLayer"
        innerBgLayer.identifier = "innerBgLayer"

        centerVertical = true

        innerBgLayer.backgroundColor = .white
        innerBgLayer.layer.cornerRadius = 5
    }

    override func addChildViews() {
        super.addChildViews()
        addBgLayer()
        addInnerBgLayer()
    }

    private func addBgLayer() {
        self.addSubview(bgLayer)

        let topConstraint:NSLayoutConstraint = NSLayoutConstraint(item: bgLayer, attribute: .top, relatedBy: .equal, toItem: self, attribute: .top, multiplier: 1, constant: 0)
        let leadingConstraint:NSLayoutConstraint =  NSLayoutConstraint(item: bgLayer, attribute: .leading, relatedBy: .equal, toItem: self, attribute: .leading, multiplier: 1, constant: 0)
        let trailingConstraint:NSLayoutConstraint = NSLayoutConstraint(item: bgLayer, attribute: .trailing, relatedBy: .equal, toItem: self, attribute: .trailing, multiplier: 1, constant: 0)
        let bottomConstraint:NSLayoutConstraint = NSLayoutConstraint(item: bgLayer, attribute: .bottom, relatedBy: .equal, toItem: self, attribute: .bottom, multiplier: 1, constant: 0)

        bgLayer.addAppConstraint(topConstraint, self)
        bgLayer.addAppConstraint(leadingConstraint, self)
        bgLayer.addAppConstraint(trailingConstraint, self)
        bgLayer.addAppConstraint(bottomConstraint, self)

        let heightConstraint:NSLayoutConstraint = NSLayoutConstraint(item: bgLayer, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 0)
        heightConstraint.priority = UILayoutPriority.defaultLow
        bgLayer.addAppConstraint(heightConstraint)
    }

    private func addInnerBgLayer() {
        self.addSubview(innerBgLayer)

        let topSpacer:AppView? = centerVertical ? viewed.uiHelper.addVerticalSpacer(toView: self, topToItem: self, topToAttribute: .top, fillWithLowPriority: true) : nil
        let bottomSpacer:AppView = viewed.uiHelper.addVerticalSpacer(toView: self, topToItem: innerBgLayer, topToAttribute: .bottom, primarySpacer:topSpacer, fillWithLowPriority: true)

        let topToItem:UIView = topSpacer != nil ? topSpacer! : self
        let topToAttribute:NSLayoutConstraint.Attribute = topSpacer != nil ? .bottom : .top
        let topConstraint:NSLayoutConstraint = NSLayoutConstraint(item: innerBgLayer, attribute: .top, relatedBy: .equal, toItem: topToItem, attribute: topToAttribute, multiplier: 1, constant: innerBgLayerMargin)
        let leadingConstraint:NSLayoutConstraint =  NSLayoutConstraint(item: innerBgLayer, attribute: .leading, relatedBy: .greaterThanOrEqual, toItem: self, attribute: .leading, multiplier: 1, constant: innerBgLayerMargin)
        let trailingConstraint:NSLayoutConstraint = NSLayoutConstraint(item: innerBgLayer, attribute: .trailing, relatedBy: .lessThanOrEqual, toItem: self, attribute: .trailing, multiplier: 1, constant: -innerBgLayerMargin)
        let centerXConstraint:NSLayoutConstraint = NSLayoutConstraint(item: innerBgLayer, attribute: .centerX, relatedBy: .equal, toItem: self, attribute: .centerX, multiplier: 1, constant: 0)

        innerBgLayer.addAppConstraint(topConstraint, self)
        innerBgLayer.addAppConstraint(leadingConstraint, self)
        innerBgLayer.addAppConstraint(trailingConstraint, self)
        innerBgLayer.addAppConstraint(centerXConstraint, self)

        let bottomConstraint:NSLayoutConstraint = NSLayoutConstraint(item: bottomSpacer, attribute: .bottom, relatedBy: .equal, toItem: self, attribute: .bottom, multiplier: 1, constant: -innerBgLayerMargin)
        bottomSpacer.addAppConstraint(bottomConstraint, self)


        let widthConstraint:NSLayoutConstraint = NSLayoutConstraint(item: innerBgLayer, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 0)
        widthConstraint.priority = UILayoutPriority.defaultLow
        innerBgLayer.addAppConstraint(widthConstraint)

    }
}
