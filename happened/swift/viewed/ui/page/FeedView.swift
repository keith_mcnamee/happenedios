import UIKit

class FeedView: ScrollContainerView {

    private var viewed: Viewed { return Viewed.instance }

    var seasonGroupBtn: Button = RedButton()
    var mainContentView: AppView = AppView()
    var typeSegment: SegmentedControl = SegmentedControl()

    override func initializeView() {
        super.initializeView()
    }

    override func addChildViews() {
        super.addChildViews()
        addSeasonBtn()
        addTypeSegment()
        addMainContentView()
    }

    private func addSeasonBtn() {
        innerContentView.addSubview(seasonGroupBtn)

        let topConstraint:NSLayoutConstraint = NSLayoutConstraint(item: seasonGroupBtn, attribute: .top, relatedBy: .equal, toItem: innerContentView, attribute: .top, multiplier: 1, constant: viewed.viewConfig.topLeftMargin.y)
        let leadingConstraint:NSLayoutConstraint =  NSLayoutConstraint(item: seasonGroupBtn, attribute: .leading, relatedBy: .equal, toItem: innerContentView, attribute: .leading, multiplier: 1, constant: viewed.viewConfig.topLeftMargin.x)

        seasonGroupBtn.addAppConstraint(topConstraint, innerContentView)
        seasonGroupBtn.addAppConstraint(leadingConstraint, innerContentView)
    }

    private func addTypeSegment() {
        innerContentView.addSubview(typeSegment)

        let topConstraint:NSLayoutConstraint = NSLayoutConstraint(item: typeSegment, attribute: .top, relatedBy: .equal, toItem: innerContentView, attribute: .top, multiplier: 1, constant: viewed.viewConfig.topLeftMargin.y)
        let trailingConstraint:NSLayoutConstraint =  NSLayoutConstraint(item: typeSegment, attribute: .trailing, relatedBy: .equal, toItem: innerContentView, attribute: .trailing, multiplier: 1, constant: -viewed.viewConfig.bottomRightMargin.x)

        typeSegment.addAppConstraint(topConstraint, innerContentView)
        typeSegment.addAppConstraint(trailingConstraint, innerContentView)

    }

    private func addMainContentView() {
        innerContentView.addSubview(mainContentView)

        let topConstraint:NSLayoutConstraint = NSLayoutConstraint(item: mainContentView, attribute: .top, relatedBy: .equal, toItem: seasonGroupBtn, attribute: .bottom, multiplier: 1, deviceConstant: 8)
        let leadingConstraint:NSLayoutConstraint =  NSLayoutConstraint(item: mainContentView, attribute: .leading, relatedBy: .equal, toItem: innerContentView, attribute: .leading, multiplier: 1, constant: 0)
        let trailingConstraint:NSLayoutConstraint = NSLayoutConstraint(item: mainContentView, attribute: .trailing, relatedBy: .equal, toItem: innerContentView, attribute: .trailing, multiplier: 1, constant: 0)
        let bottomConstraint:NSLayoutConstraint = NSLayoutConstraint(item: mainContentView, attribute: .bottom, relatedBy: .lessThanOrEqual, toItem: innerContentView, attribute: .bottom, multiplier: 1, constant: -viewed.viewConfig.bottomRightMargin.y)

        mainContentView.addAppConstraint(topConstraint, innerContentView)
        mainContentView.addAppConstraint(leadingConstraint, innerContentView)
        mainContentView.addAppConstraint(trailingConstraint, innerContentView)
        mainContentView.addAppConstraint(bottomConstraint, innerContentView)
    }
}
