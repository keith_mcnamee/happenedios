import UIKit

class ListingView: ScrollContainerView {

    private var viewed: Viewed { return Viewed.instance }

    var topItemsView: AppView = AppView()
    var seasonBtn: Button = RedButton()
    let listingSeasonTextView: AttributedTextView = AttributedTextView()
    var mainContentView: AppView = AppView()

    override func addChildViews() {
        super.addChildViews()
        addTopItemsView()
        addMainContentView()
    }

    override func applyView() {
        super.applyView()
        _ = topItemsView.addBorder(edges: [.bottom], color: viewed.viewConfig.colorBlue6, thickness: 1)
        listingSeasonTextView.font = UIFont.font(pointSizeStyle: .title1)
    }

    private func addTopItemsView() {
        innerContentView.addSubview(topItemsView)
        let topConstraint:NSLayoutConstraint = NSLayoutConstraint(item: topItemsView, attribute: .top, relatedBy: .equal, toItem: innerContentView, attribute: .top, multiplier: 1, constant: viewed.viewConfig.topLeftMargin.y)
        let leadingConstraint:NSLayoutConstraint =  NSLayoutConstraint(item: topItemsView, attribute: .leading, relatedBy: .equal, toItem: innerContentView, attribute: .leading, multiplier: 1, constant: viewed.viewConfig.topLeftMargin.x)
        let trailingConstraint:NSLayoutConstraint =  NSLayoutConstraint(item: topItemsView, attribute: .trailing, relatedBy: .equal, toItem: innerContentView, attribute: .trailing, multiplier: 1, constant: -viewed.viewConfig.bottomRightMargin.x)

        topItemsView.addAppConstraint(topConstraint, innerContentView)
        topItemsView.addAppConstraint(leadingConstraint, innerContentView)
        topItemsView.addAppConstraint(trailingConstraint, innerContentView)

        addSeasonBtn()
        addListingSeasonTextView()
    }

    private func addSeasonBtn() {
        topItemsView.addSubview(seasonBtn)

        let topConstraint:NSLayoutConstraint = NSLayoutConstraint(item: seasonBtn, attribute: .top, relatedBy: .equal, toItem: topItemsView, attribute: .top, multiplier: 1, constant: 0)
        let leadingConstraint:NSLayoutConstraint =  NSLayoutConstraint(item: seasonBtn, attribute: .leading, relatedBy: .equal, toItem: topItemsView, attribute: .leading, multiplier: 1, constant: 0)

        seasonBtn.addAppConstraint(topConstraint, topItemsView)
        seasonBtn.addAppConstraint(leadingConstraint, topItemsView)
    }

    private func addListingSeasonTextView() {
        topItemsView.addSubview(listingSeasonTextView)

        let topConstraint:NSLayoutConstraint = NSLayoutConstraint(item: listingSeasonTextView, attribute: .top, relatedBy: .equal, toItem: seasonBtn, attribute: .bottom, multiplier: 1, deviceConstant: 0)
        let leadingConstraint:NSLayoutConstraint =  NSLayoutConstraint(item: listingSeasonTextView, attribute: .leading, relatedBy: .greaterThanOrEqual, toItem: topItemsView, attribute: .leading, multiplier: 1, constant: 0)
        let trailingConstraint:NSLayoutConstraint = NSLayoutConstraint(item: listingSeasonTextView, attribute: .trailing, relatedBy: .lessThanOrEqual, toItem: topItemsView, attribute: .trailing, multiplier: 1, constant: 0)
        let bottomConstraint:NSLayoutConstraint = NSLayoutConstraint(item: listingSeasonTextView, attribute: .bottom, relatedBy: .equal, toItem: topItemsView, attribute: .bottom, multiplier: 1, deviceConstant: -4)
        let centerXConstraint:NSLayoutConstraint =  NSLayoutConstraint(item: listingSeasonTextView, attribute: .centerX, relatedBy: .equal, toItem: topItemsView, attribute: .centerX, multiplier: 1, constant: 0)

        listingSeasonTextView.addAppConstraint(topConstraint, topItemsView)
        listingSeasonTextView.addAppConstraint(leadingConstraint, topItemsView)
        listingSeasonTextView.addAppConstraint(trailingConstraint, topItemsView)
        listingSeasonTextView.addAppConstraint(bottomConstraint, topItemsView)
        listingSeasonTextView.addAppConstraint(centerXConstraint, topItemsView)

    }

    private func addMainContentView() {
        innerContentView.addSubview(mainContentView)

        let topConstraint:NSLayoutConstraint = NSLayoutConstraint(item: mainContentView, attribute: .top, relatedBy: .equal, toItem: topItemsView, attribute: .bottom, multiplier: 1, deviceConstant: 8)
        let leadingConstraint:NSLayoutConstraint =  NSLayoutConstraint(item: mainContentView, attribute: .leading, relatedBy: .equal, toItem: innerContentView, attribute: .leading, multiplier: 1, constant: 0)
        let trailingConstraint:NSLayoutConstraint = NSLayoutConstraint(item: mainContentView, attribute: .trailing, relatedBy: .equal, toItem: innerContentView, attribute: .trailing, multiplier: 1, constant: 0)
        let bottomConstraint:NSLayoutConstraint = NSLayoutConstraint(item: mainContentView, attribute: .bottom, relatedBy: .lessThanOrEqual, toItem: innerContentView, attribute: .bottom, multiplier: 1, constant: 0)

        mainContentView.addAppConstraint(topConstraint, innerContentView)
        mainContentView.addAppConstraint(leadingConstraint, innerContentView)
        mainContentView.addAppConstraint(trailingConstraint, innerContentView)
        mainContentView.addAppConstraint(bottomConstraint, innerContentView)
    }

}
