import UIKit

class SettingsView: ScrollContainerView {

    private var viewed: Viewed { return Viewed.instance }

    var topItemsView: AppView = AppView()
    var manageTextView: AttributedTextView = AttributedTextView()
    var mainContentView: AppView = AppView()
    var statusTextView: AttributedTextView = AttributedTextView()

    override func addChildViews() {
        super.addChildViews()

        addTopItemsView()
        addMainContentView()
        addStatusTextView()
    }

    override func applyView() {
        super.applyView()
        _ = topItemsView.addBorder(edges: [.bottom], color: viewed.viewConfig.colorBlue6, thickness: 1)
        statusTextView.textContainer.maximumNumberOfLines = 0
        statusTextView.defaultTextAlignment = .center
    }

    private func addTopItemsView() {
        innerContentView.addSubview(topItemsView)
        let topConstraint:NSLayoutConstraint = NSLayoutConstraint(item: topItemsView, attribute: .top, relatedBy: .equal, toItem: innerContentView, attribute: .top, multiplier: 1, constant: viewed.viewConfig.topLeftMargin.y + 20)
        let leadingConstraint:NSLayoutConstraint =  NSLayoutConstraint(item: topItemsView, attribute: .leading, relatedBy: .equal, toItem: innerContentView, attribute: .leading, multiplier: 1, constant: viewed.viewConfig.topLeftMargin.x)
        let trailingConstraint:NSLayoutConstraint =  NSLayoutConstraint(item: topItemsView, attribute: .trailing, relatedBy: .equal, toItem: innerContentView, attribute: .trailing, multiplier: 1, constant: -viewed.viewConfig.bottomRightMargin.x)

        topItemsView.addAppConstraint(topConstraint, innerContentView)
        topItemsView.addAppConstraint(leadingConstraint, innerContentView)
        topItemsView.addAppConstraint(trailingConstraint, innerContentView)

        addManageTextView()
    }

    private func addManageTextView() {
        topItemsView.addSubview(manageTextView)

        let topConstraint:NSLayoutConstraint = NSLayoutConstraint(item: manageTextView, attribute: .top, relatedBy: .equal, toItem: topItemsView, attribute: .top, multiplier: 1, constant: 0)
        let leadingConstraint:NSLayoutConstraint =  NSLayoutConstraint(item: manageTextView, attribute: .leading, relatedBy: .greaterThanOrEqual, toItem: topItemsView, attribute: .leading, multiplier: 1, constant: 0)
        let trailingConstraint:NSLayoutConstraint = NSLayoutConstraint(item: manageTextView, attribute: .trailing, relatedBy: .lessThanOrEqual, toItem: topItemsView, attribute: .trailing, multiplier: 1, constant: 0)
        let bottomConstraint:NSLayoutConstraint = NSLayoutConstraint(item: manageTextView, attribute: .bottom, relatedBy: .equal, toItem: topItemsView, attribute: .bottom, multiplier: 1, deviceConstant: -8)
        let centerXConstraint:NSLayoutConstraint = NSLayoutConstraint(item: manageTextView, attribute: .centerX, relatedBy: .equal, toItem: topItemsView, attribute: .centerX, multiplier: 1, constant: 0)

        manageTextView.addAppConstraint(topConstraint, topItemsView)
        manageTextView.addAppConstraint(leadingConstraint, topItemsView)
        manageTextView.addAppConstraint(trailingConstraint, topItemsView)
        manageTextView.addAppConstraint(bottomConstraint, topItemsView)
        manageTextView.addAppConstraint(centerXConstraint, topItemsView)

    }

    private func addMainContentView() {
        innerContentView.addSubview(mainContentView)

        let topConstraint:NSLayoutConstraint = NSLayoutConstraint(item: mainContentView, attribute: .top, relatedBy: .equal, toItem: topItemsView, attribute: .bottom, multiplier: 1, deviceConstant: 8)
        let leadingConstraint:NSLayoutConstraint =  NSLayoutConstraint(item: mainContentView, attribute: .leading, relatedBy: .equal, toItem: innerContentView, attribute: .leading, multiplier: 1, constant: 0)
        let trailingConstraint:NSLayoutConstraint = NSLayoutConstraint(item: mainContentView, attribute: .trailing, relatedBy: .equal, toItem: innerContentView, attribute: .trailing, multiplier: 1, constant: 0)

        mainContentView.addAppConstraint(topConstraint, innerContentView)
        mainContentView.addAppConstraint(leadingConstraint, innerContentView)
        mainContentView.addAppConstraint(trailingConstraint, innerContentView)


        let heightConstraint:NSLayoutConstraint = NSLayoutConstraint(item: mainContentView, attribute: .height, priority: UILayoutPriority.defaultLow)
        mainContentView.addConstraint(heightConstraint)
    }

    private func addStatusTextView() {
        innerContentView.addSubview(statusTextView)

        let topConstraint:NSLayoutConstraint = NSLayoutConstraint(item: statusTextView, attribute: .top, relatedBy: .equal, toItem: mainContentView, attribute: .bottom, multiplier: 1, constant: viewed.viewConfig.topLeftMargin.y)
        let leadingConstraint:NSLayoutConstraint =  NSLayoutConstraint(item: statusTextView, attribute: .leading, relatedBy: .greaterThanOrEqual, toItem: innerContentView, attribute: .leading, multiplier: 1, constant: 0)
        let trailingConstraint:NSLayoutConstraint = NSLayoutConstraint(item: statusTextView, attribute: .trailing, relatedBy: .lessThanOrEqual, toItem: innerContentView, attribute: .trailing, multiplier: 1, constant: 0)
        let centerXConstraint:NSLayoutConstraint =  NSLayoutConstraint(item: statusTextView, attribute: .centerX, relatedBy: .equal, toItem: innerContentView, attribute: .centerX, multiplier: 1, constant: 0)
        let bottomConstraint:NSLayoutConstraint = NSLayoutConstraint(item: statusTextView, attribute: .bottom, relatedBy: .lessThanOrEqual, toItem: innerContentView, attribute: .bottom, multiplier: 1, constant: 0)

        statusTextView.addAppConstraint(topConstraint, innerContentView)
        statusTextView.addAppConstraint(leadingConstraint, innerContentView)
        statusTextView.addAppConstraint(trailingConstraint, innerContentView)
        statusTextView.addAppConstraint(centerXConstraint, innerContentView)
        statusTextView.addAppConstraint(bottomConstraint, innerContentView)

    }

    var mainContentViewHidden:Bool {
        get {
            return mainContentView.isHidden
        }
        set {
            mainContentView.isHidden = newValue
            if newValue {
                let heightConstraint:NSLayoutConstraint = NSLayoutConstraint(item: mainContentView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 0)
                mainContentView.addAppConstraint(heightConstraint)
            } else {
                mainContentView.removeAppConstraint(.height)
            }
        }
    }

}
