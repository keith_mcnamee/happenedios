import UIKit

class ListingScenarioView: ScenarioView {

    private var viewed: Viewed { return Viewed.instance }

    var positionTextView: AttributedTextView = AttributedTextView()

    override func addChildViews() {
        super.addChildViews()
        addPositionTextView()
    }

    override func applyView() {
        super.applyView()
        positionTextView.font = UIFont.font(pointSizeStyle: .body)
    }


    private func addPositionTextView() {
        innerContentView.addSubview(positionTextView)

        let topConstraint: NSLayoutConstraint = NSLayoutConstraint(item: positionTextView, attribute: .top, relatedBy: .equal, toItem: topItemsView, attribute: .bottom, multiplier: 1, deviceConstant: 8)
        let leadingConstraint: NSLayoutConstraint = NSLayoutConstraint(item: positionTextView, attribute: .leading, relatedBy: .greaterThanOrEqual, toItem: innerContentView, attribute: .leading, multiplier: 1, constant: viewed.viewConfig.topLeftMargin.x)
        let trailingConstraint: NSLayoutConstraint = NSLayoutConstraint(item: positionTextView, attribute: .trailing, relatedBy: .lessThanOrEqual, toItem: innerContentView, attribute: .trailing, multiplier: 1, constant: -viewed.viewConfig.bottomRightMargin.x)
        let centerXConstraint:NSLayoutConstraint =  NSLayoutConstraint(item: positionTextView, attribute: .centerX, relatedBy: .equal, toItem: innerContentView, attribute: .centerX, multiplier: 1, constant: 0)

        positionTextView.addAppConstraint(topConstraint, innerContentView)
        positionTextView.addAppConstraint(leadingConstraint, innerContentView)
        positionTextView.addAppConstraint(trailingConstraint, innerContentView)
        positionTextView.addAppConstraint(centerXConstraint, innerContentView)


        let stateSegmentTopConstraint: NSLayoutConstraint = NSLayoutConstraint(item: stateSegment, attribute: .top, relatedBy: .equal, toItem: positionTextView, attribute: .bottom, multiplier: 1, deviceConstant: 8)
        stateSegment.addAppConstraint(stateSegmentTopConstraint, innerContentView)
    }

}
