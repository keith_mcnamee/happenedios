import UIKit

class ScenarioView: ScrollContainerView {

    private var viewed: Viewed { return Viewed.instance }

    var topItemsView: AppView = AppView()
    var seasonBtn: Button = RedButton()
    let teamTextView: AttributedTextView = AttributedTextView()
    var listingSeasonTextView: AttributedTextView = AttributedTextView()
    var typeSegment: SegmentedControl = SegmentedControl()
    var stateSegment: SegmentedControl = SegmentedControl()
    var isSpecifiedTeamsSegment: SegmentedControl = SegmentedControl()
    var mainContentView: AppView = AppView()

    override func addChildViews() {
        super.addChildViews()
        addTopItemsView()
        addStateSegment()
        addIsSpecifiedTeamsSegment()
        addMainContentView()
    }

    override func applyView() {
        super.applyView()
        _ = topItemsView.addBorder(edges: [.bottom], color: viewed.viewConfig.colorBlue6, thickness: 1)
        teamTextView.font = UIFont.font(pointSizeStyle: .title1)
        listingSeasonTextView.font = UIFont.font(pointSizeStyle: .body)
    }

    private func addTopItemsView() {
        innerContentView.addSubview(topItemsView)
        let topConstraint:NSLayoutConstraint = NSLayoutConstraint(item: topItemsView, attribute: .top, relatedBy: .equal, toItem: innerContentView, attribute: .top, multiplier: 1, constant: viewed.viewConfig.topLeftMargin.y)
        let leadingConstraint:NSLayoutConstraint =  NSLayoutConstraint(item: topItemsView, attribute: .leading, relatedBy: .equal, toItem: innerContentView, attribute: .leading, multiplier: 1, constant: viewed.viewConfig.topLeftMargin.x)
        let trailingConstraint:NSLayoutConstraint =  NSLayoutConstraint(item: topItemsView, attribute: .trailing, relatedBy: .equal, toItem: innerContentView, attribute: .trailing, multiplier: 1, constant: -viewed.viewConfig.bottomRightMargin.x)

        topItemsView.addAppConstraint(topConstraint, innerContentView)
        topItemsView.addAppConstraint(leadingConstraint, innerContentView)
        topItemsView.addAppConstraint(trailingConstraint, innerContentView)

        addSeasonBtn()
        addTypeSegment()
        addTeamTextView()
        addListingSeasonTextView()
    }

    private func addSeasonBtn() {
        topItemsView.addSubview(seasonBtn)

        let topConstraint:NSLayoutConstraint = NSLayoutConstraint(item: seasonBtn, attribute: .top, relatedBy: .equal, toItem: topItemsView, attribute: .top, multiplier: 1, constant: 0)
        let leadingConstraint:NSLayoutConstraint =  NSLayoutConstraint(item: seasonBtn, attribute: .leading, relatedBy: .equal, toItem: topItemsView, attribute: .leading, multiplier: 1, constant: 0)

        seasonBtn.addAppConstraint(topConstraint, topItemsView)
        seasonBtn.addAppConstraint(leadingConstraint, topItemsView)
    }

    private func addTypeSegment() {
        topItemsView.addSubview(typeSegment)

        let topConstraint:NSLayoutConstraint = NSLayoutConstraint(item: typeSegment, attribute: .top, relatedBy: .equal, toItem: topItemsView, attribute: .top, multiplier: 1, constant: 0)
        let trailingConstraint:NSLayoutConstraint =  NSLayoutConstraint(item: typeSegment, attribute: .trailing, relatedBy: .equal, toItem: topItemsView, attribute: .trailing, multiplier: 1, constant: 0)

        typeSegment.addAppConstraint(topConstraint, topItemsView)
        typeSegment.addAppConstraint(trailingConstraint, topItemsView)
    }

    private func addTeamTextView() {
        topItemsView.addSubview(teamTextView)

        let topConstraint:NSLayoutConstraint = NSLayoutConstraint(item: teamTextView, attribute: .top, relatedBy: .equal, toItem: seasonBtn, attribute: .bottom, multiplier: 1, deviceConstant: 8)
        let leadingConstraint:NSLayoutConstraint =  NSLayoutConstraint(item: teamTextView, attribute: .leading, relatedBy: .greaterThanOrEqual, toItem: topItemsView, attribute: .leading, multiplier: 1, constant: 0)
        let trailingConstraint:NSLayoutConstraint = NSLayoutConstraint(item: teamTextView, attribute: .trailing, relatedBy: .lessThanOrEqual, toItem: topItemsView, attribute: .trailing, multiplier: 1, constant: 0)
        let centerXConstraint:NSLayoutConstraint =  NSLayoutConstraint(item: teamTextView, attribute: .centerX, relatedBy: .equal, toItem: topItemsView, attribute: .centerX, multiplier: 1, constant: 0)

        teamTextView.addAppConstraint(topConstraint, topItemsView)
        teamTextView.addAppConstraint(leadingConstraint, topItemsView)
        teamTextView.addAppConstraint(trailingConstraint, topItemsView)
        teamTextView.addAppConstraint(centerXConstraint, topItemsView)

    }

    private func addListingSeasonTextView() {
        topItemsView.addSubview(listingSeasonTextView)

        let topConstraint:NSLayoutConstraint = NSLayoutConstraint(item: listingSeasonTextView, attribute: .top, relatedBy: .equal, toItem: teamTextView, attribute: .bottom, multiplier: 1, constant: 0)
        let leadingConstraint:NSLayoutConstraint =  NSLayoutConstraint(item: listingSeasonTextView, attribute: .leading, relatedBy: .greaterThanOrEqual, toItem: topItemsView, attribute: .leading, multiplier: 1, constant: 0)
        let trailingConstraint:NSLayoutConstraint = NSLayoutConstraint(item: listingSeasonTextView, attribute: .trailing, relatedBy: .lessThanOrEqual, toItem: topItemsView, attribute: .trailing, multiplier: 1, constant: 0)
        let bottomConstraint:NSLayoutConstraint = NSLayoutConstraint(item: listingSeasonTextView, attribute: .bottom, relatedBy: .equal, toItem: topItemsView, attribute: .bottom, multiplier: 1, deviceConstant: -4)
        let centerXConstraint:NSLayoutConstraint =  NSLayoutConstraint(item: listingSeasonTextView, attribute: .centerX, relatedBy: .equal, toItem: topItemsView, attribute: .centerX, multiplier: 1, constant: 0)

        listingSeasonTextView.addAppConstraint(topConstraint, topItemsView)
        listingSeasonTextView.addAppConstraint(leadingConstraint, topItemsView)
        listingSeasonTextView.addAppConstraint(trailingConstraint, topItemsView)
        listingSeasonTextView.addAppConstraint(bottomConstraint, topItemsView)
        listingSeasonTextView.addAppConstraint(centerXConstraint, topItemsView)

    }

    private func addStateSegment() {
        innerContentView.addSubview(stateSegment)

        let topConstraint:NSLayoutConstraint = NSLayoutConstraint(item: stateSegment, attribute: .top, relatedBy: .equal, toItem: topItemsView, attribute: .bottom, multiplier: 1, deviceConstant: 8)
        let centerXConstraint:NSLayoutConstraint =  NSLayoutConstraint(item: stateSegment, attribute: .centerX, relatedBy: .equal, toItem: innerContentView, attribute: .centerX, multiplier: 1, constant: 0)

        stateSegment.addAppConstraint(topConstraint, innerContentView)
        stateSegment.addAppConstraint(centerXConstraint, innerContentView)
    }

    private func addIsSpecifiedTeamsSegment() {
        innerContentView.addSubview(isSpecifiedTeamsSegment)

        let topConstraint:NSLayoutConstraint = NSLayoutConstraint(item: isSpecifiedTeamsSegment, attribute: .top, relatedBy: .equal, toItem: stateSegment, attribute: .bottom, multiplier: 1, deviceConstant: 8)
        let centerXConstraint:NSLayoutConstraint =  NSLayoutConstraint(item: isSpecifiedTeamsSegment, attribute: .centerX, relatedBy: .equal, toItem: innerContentView, attribute: .centerX, multiplier: 1, deviceConstant: 0)

        isSpecifiedTeamsSegment.addAppConstraint(topConstraint, innerContentView)
        isSpecifiedTeamsSegment.addAppConstraint(centerXConstraint, innerContentView)
    }

    private func addMainContentView() {
        innerContentView.addSubview(mainContentView)

        let topConstraint:NSLayoutConstraint = NSLayoutConstraint(item: mainContentView, attribute: .top, relatedBy: .equal, toItem: isSpecifiedTeamsSegment, attribute: .bottom, multiplier: 1, deviceConstant: 8)
        let leadingConstraint:NSLayoutConstraint =  NSLayoutConstraint(item: mainContentView, attribute: .leading, relatedBy: .equal, toItem: innerContentView, attribute: .leading, multiplier: 1, constant: 0)
        let trailingConstraint:NSLayoutConstraint = NSLayoutConstraint(item: mainContentView, attribute: .trailing, relatedBy: .equal, toItem: innerContentView, attribute: .trailing, multiplier: 1, constant: 0)
        let bottomConstraint:NSLayoutConstraint = NSLayoutConstraint(item: mainContentView, attribute: .bottom, relatedBy: .lessThanOrEqual, toItem: innerContentView, attribute: .bottom, multiplier: 1, constant: 0)

        mainContentView.addAppConstraint(topConstraint, innerContentView)
        mainContentView.addAppConstraint(leadingConstraint, innerContentView)
        mainContentView.addAppConstraint(trailingConstraint, innerContentView)
        mainContentView.addAppConstraint(bottomConstraint, innerContentView)
    }

    var stateSegmentHidden:Bool {
        get {
            return stateSegment.isHidden
        }
        set {
            stateSegment.isHidden = newValue
            if newValue {
                let heightConstraint:NSLayoutConstraint = NSLayoutConstraint(item: stateSegment, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 0)
                stateSegment.addAppConstraint(heightConstraint)
            } else {
                stateSegment.removeAppConstraint(.height)
            }
        }
    }

    var isSpecifiedTeamsSegmentHidden:Bool {
        get {
            return isSpecifiedTeamsSegment.isHidden
        }
        set {
            isSpecifiedTeamsSegment.isHidden = newValue
            if newValue {
                let heightConstraint:NSLayoutConstraint = NSLayoutConstraint(item: isSpecifiedTeamsSegment, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 0)
                isSpecifiedTeamsSegment.addAppConstraint(heightConstraint)
            } else {
                isSpecifiedTeamsSegment.removeAppConstraint(.height)
            }
        }
    }

}