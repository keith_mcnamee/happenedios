import UIKit

class EventScenarioView: ScenarioView {

    private var viewed: Viewed { return Viewed.instance }

    var eventTextView: AttributedTextView = AttributedTextView()
    var overviewView: AppView = AppView()

    var possibleContainerView: AppView = AppView()
    var possibleTextView: AttributedTextView = AttributedTextView()
    var possibleImageView: ImageView = ImageView(image:UIImage.scaledToSized(named: "cross.png", width: CGFloat(26).deviceValue))

    var certainContainerView: AppView = AppView()
    var certainTextView: AttributedTextView = AttributedTextView()
    var certainImageView: ImageView = ImageView(image:UIImage.scaledToSized(named: "cross.png", width: CGFloat(26).deviceValue))
    var overviewDividerView: DividerView = DividerView()

    var resultBodyTextView: AttributedTextView = AttributedTextView()
    var resultWhenTextView: AttributedTextView = AttributedTextView()

    override func addChildViews() {
        super.addChildViews()
        addEventTextView()
        addOverviewView()
        addOverviewDividerView()
        addResultBodyTextView()
        addResultWhenTextView()
    }

    override func applyView() {
        super.applyView()

        applyPossibleCertainImage(certain: false, isYes: false)
        applyPossibleCertainImage(certain: true, isYes: false)
        eventTextView.font = UIFont.font(pointSizeStyle: .title1)

        resultBodyTextView.textContainer.maximumNumberOfLines = 0
    }


    private func addEventTextView() {
        topItemsView.addSubview(eventTextView)

        listingSeasonTextView.removeAppConstraint(.bottom)

        let topConstraint:NSLayoutConstraint = NSLayoutConstraint(item: eventTextView, attribute: .top, relatedBy: .equal, toItem: listingSeasonTextView, attribute: .bottom, multiplier: 1, constant: 0)
        let leadingConstraint:NSLayoutConstraint =  NSLayoutConstraint(item: eventTextView, attribute: .leading, relatedBy: .greaterThanOrEqual, toItem: topItemsView, attribute: .leading, multiplier: 1, constant: 0)
        let trailingConstraint:NSLayoutConstraint = NSLayoutConstraint(item: eventTextView, attribute: .trailing, relatedBy: .lessThanOrEqual, toItem: topItemsView, attribute: .trailing, multiplier: 1, constant: 0)
        let bottomConstraint:NSLayoutConstraint = NSLayoutConstraint(item: eventTextView, attribute: .bottom, relatedBy: .equal, toItem: topItemsView, attribute: .bottom, multiplier: 1, deviceConstant: -4)
        let centerXConstraint:NSLayoutConstraint =  NSLayoutConstraint(item: eventTextView, attribute: .centerX, relatedBy: .equal, toItem: topItemsView, attribute: .centerX, multiplier: 1, constant: 0)

        eventTextView.addAppConstraint(topConstraint, topItemsView)
        eventTextView.addAppConstraint(leadingConstraint, topItemsView)
        eventTextView.addAppConstraint(trailingConstraint, topItemsView)
        eventTextView.addAppConstraint(bottomConstraint, topItemsView)
        eventTextView.addAppConstraint(centerXConstraint, topItemsView)

    }

    private func addOverviewView() {
        innerContentView.addSubview(overviewView)

        let topConstraint:NSLayoutConstraint = NSLayoutConstraint(item: overviewView, attribute: .top, relatedBy: .equal, toItem: topItemsView, attribute: .bottom, multiplier: 1, deviceConstant: 8)
        let leadingConstraint:NSLayoutConstraint =  NSLayoutConstraint(item: overviewView, attribute: .leading, relatedBy: .greaterThanOrEqual, toItem: innerContentView, attribute: .leading, multiplier: 1, constant: viewed.viewConfig.topLeftMargin.x)
        let trailingConstraint:NSLayoutConstraint = NSLayoutConstraint(item: overviewView, attribute: .trailing, relatedBy: .lessThanOrEqual, toItem: innerContentView, attribute: .trailing, multiplier: 1, constant: -viewed.viewConfig.bottomRightMargin.x)
        let centerXConstraint:NSLayoutConstraint = NSLayoutConstraint(item: overviewView, attribute: .centerX, relatedBy: .equal, toItem: innerContentView, attribute: .centerX, multiplier: 1, constant: 0)

        overviewView.addAppConstraint(topConstraint, innerContentView)
        overviewView.addAppConstraint(leadingConstraint, innerContentView)
        overviewView.addAppConstraint(trailingConstraint, innerContentView)
        overviewView.addAppConstraint(centerXConstraint, innerContentView)

        let heightConstraint:NSLayoutConstraint = NSLayoutConstraint(item: overviewView, attribute: .height)
        heightConstraint.priority = UILayoutPriority.defaultLow
        overviewView.addAppConstraint( heightConstraint )

        let widthConstraint:NSLayoutConstraint = NSLayoutConstraint(item: possibleContainerView, attribute: .width)
        widthConstraint.priority = UILayoutPriority.defaultLow
        possibleContainerView.addAppConstraint( widthConstraint )

        addPossibleContainerView()
        addCertainContainerView()

    }

    private func addPossibleContainerView() {
        overviewView.addSubview(possibleContainerView)

        let topConstraint:NSLayoutConstraint = NSLayoutConstraint(item: possibleContainerView, attribute: .top, relatedBy: .equal, toItem: overviewView, attribute: .top, multiplier: 1, constant: 0)
        let leadingConstraint:NSLayoutConstraint =  NSLayoutConstraint(item: possibleContainerView, attribute: .leading, relatedBy: .equal, toItem: overviewView, attribute: .leading, multiplier: 1, constant: 0)
        let trailingConstraint:NSLayoutConstraint = NSLayoutConstraint(item: possibleContainerView, attribute: .trailing, relatedBy: .equal, toItem: overviewView, attribute: .trailing, multiplier: 1, constant: 0)

        possibleContainerView.addAppConstraint(topConstraint, overviewView)
        possibleContainerView.addAppConstraint(leadingConstraint, overviewView)
        possibleContainerView.addAppConstraint(trailingConstraint, overviewView)

        addPossibleTextView()
        addPossibleImageView()
    }

    private func addPossibleTextView() {
        possibleContainerView.addSubview(possibleTextView)

        let topConstraint:NSLayoutConstraint = NSLayoutConstraint(item: possibleTextView, attribute: .top, relatedBy: .greaterThanOrEqual, toItem: possibleContainerView, attribute: .top, multiplier: 1, constant: 0)
        let leadingConstraint:NSLayoutConstraint =  NSLayoutConstraint(item: possibleTextView, attribute: .leading, relatedBy: .equal, toItem: possibleContainerView, attribute: .leading, multiplier: 1, constant: 0)
        let bottomConstraint:NSLayoutConstraint = NSLayoutConstraint(item: possibleTextView, attribute: .bottom, relatedBy: .lessThanOrEqual, toItem: possibleContainerView, attribute: .bottom, multiplier: 1, constant: 0)
        let centerYConstraint:NSLayoutConstraint = NSLayoutConstraint(item: possibleTextView, attribute: .centerY, relatedBy: .equal, toItem: possibleContainerView, attribute: .centerY, multiplier: 1, constant: 0)

        possibleTextView.addAppConstraint(topConstraint, possibleContainerView)
        possibleTextView.addAppConstraint(leadingConstraint, possibleContainerView)
        possibleTextView.addAppConstraint(bottomConstraint, possibleContainerView)
        possibleTextView.addAppConstraint(centerYConstraint, possibleContainerView)

    }

    private func addPossibleImageView() {
        possibleContainerView.addSubview(possibleImageView)

        let topConstraint:NSLayoutConstraint = NSLayoutConstraint(item: possibleImageView, attribute: .top, relatedBy: .greaterThanOrEqual, toItem: possibleContainerView, attribute: .top, multiplier: 1, constant: 0)
        let leadingConstraint:NSLayoutConstraint =  NSLayoutConstraint(item: possibleImageView, attribute: .leading, relatedBy: .greaterThanOrEqual, toItem: possibleTextView, attribute: .trailing, multiplier: 1, deviceConstant: 4)
        let trailingConstraint:NSLayoutConstraint = NSLayoutConstraint(item: possibleImageView, attribute: .trailing, relatedBy: .equal, toItem: possibleContainerView, attribute: .trailing, multiplier: 1, constant: 0)
        let bottomConstraint:NSLayoutConstraint = NSLayoutConstraint(item: possibleImageView, attribute: .bottom, relatedBy: .lessThanOrEqual, toItem: possibleContainerView, attribute: .bottom, multiplier: 1, constant: 0)
        let centerYConstraint:NSLayoutConstraint = NSLayoutConstraint(item: possibleImageView, attribute: .centerY, relatedBy: .equal, toItem: possibleContainerView, attribute: .centerY, multiplier: 1, constant: 0)

        possibleImageView.addAppConstraint(topConstraint, possibleContainerView)
        possibleImageView.addAppConstraint(leadingConstraint, possibleContainerView)
        possibleImageView.addAppConstraint(trailingConstraint, possibleContainerView)
        possibleImageView.addAppConstraint(bottomConstraint, possibleContainerView)
        possibleImageView.addAppConstraint(centerYConstraint, possibleContainerView)

    }

    private func addCertainContainerView() {
        overviewView.addSubview(certainContainerView)

        let topConstraint:NSLayoutConstraint = NSLayoutConstraint(item: certainContainerView, attribute: .top, relatedBy: .equal, toItem: possibleContainerView, attribute: .bottom, multiplier: 1, constant: 0)
        let leadingConstraint:NSLayoutConstraint =  NSLayoutConstraint(item: certainContainerView, attribute: .leading, relatedBy: .equal, toItem: overviewView, attribute: .leading, multiplier: 1, constant: 0)
        let trailingConstraint:NSLayoutConstraint = NSLayoutConstraint(item: certainContainerView, attribute: .trailing, relatedBy: .equal, toItem: overviewView, attribute: .trailing, multiplier: 1, constant: 0)
        let bottomConstraint:NSLayoutConstraint = NSLayoutConstraint(item: certainContainerView, attribute: .bottom, relatedBy: .equal, toItem: overviewView, attribute: .bottom, multiplier: 1, deviceConstant: 0)

        certainContainerView.addAppConstraint(topConstraint, overviewView)
        certainContainerView.addAppConstraint(leadingConstraint, overviewView)
        certainContainerView.addAppConstraint(trailingConstraint, overviewView)
        certainContainerView.addAppConstraint(bottomConstraint, overviewView)

        addCertainTextView()
        addCertainImageView()
    }

    private func addCertainTextView() {
        certainContainerView.addSubview(certainTextView)

        let topConstraint:NSLayoutConstraint = NSLayoutConstraint(item: certainTextView, attribute: .top, relatedBy: .greaterThanOrEqual, toItem: certainContainerView, attribute: .top, multiplier: 1, constant: 0)
        let leadingConstraint:NSLayoutConstraint =  NSLayoutConstraint(item: certainTextView, attribute: .leading, relatedBy: .equal, toItem: certainContainerView, attribute: .leading, multiplier: 1, constant: 0)
        let bottomConstraint:NSLayoutConstraint = NSLayoutConstraint(item: certainTextView, attribute: .bottom, relatedBy: .lessThanOrEqual, toItem: certainContainerView, attribute: .bottom, multiplier: 1, constant: 0)
        let centerYConstraint:NSLayoutConstraint = NSLayoutConstraint(item: certainTextView, attribute: .centerY, relatedBy: .equal, toItem: certainContainerView, attribute: .centerY, multiplier: 1, constant: 0)

        certainTextView.addAppConstraint(topConstraint, certainContainerView)
        certainTextView.addAppConstraint(leadingConstraint, certainContainerView)
        certainTextView.addAppConstraint(bottomConstraint, certainContainerView)
        certainTextView.addAppConstraint(centerYConstraint, certainContainerView)

    }

    private func addCertainImageView() {
        certainContainerView.addSubview(certainImageView)

        let topConstraint:NSLayoutConstraint = NSLayoutConstraint(item: certainImageView, attribute: .top, relatedBy: .greaterThanOrEqual, toItem: certainContainerView, attribute: .top, multiplier: 1, constant: 0)
        let leadingConstraint:NSLayoutConstraint =  NSLayoutConstraint(item: certainImageView, attribute: .leading, relatedBy: .greaterThanOrEqual, toItem: certainTextView, attribute: .trailing, multiplier: 1, deviceConstant: 4)
        let trailingConstraint:NSLayoutConstraint = NSLayoutConstraint(item: certainImageView, attribute: .trailing, relatedBy: .equal, toItem: certainContainerView, attribute: .trailing, multiplier: 1, constant: 0)
        let bottomConstraint:NSLayoutConstraint = NSLayoutConstraint(item: certainImageView, attribute: .bottom, relatedBy: .lessThanOrEqual, toItem: certainContainerView, attribute: .bottom, multiplier: 1, constant: 0)
        let centerYConstraint:NSLayoutConstraint = NSLayoutConstraint(item: certainImageView, attribute: .centerY, relatedBy: .equal, toItem: certainContainerView, attribute: .centerY, multiplier: 1, constant: 0)

        certainImageView.addAppConstraint(topConstraint, certainContainerView)
        certainImageView.addAppConstraint(leadingConstraint, certainContainerView)
        certainImageView.addAppConstraint(trailingConstraint, certainContainerView)
        certainImageView.addAppConstraint(bottomConstraint, certainContainerView)
        certainImageView.addAppConstraint(centerYConstraint, certainContainerView)

    }

    private func addOverviewDividerView() {
        innerContentView.addSubview(overviewDividerView)

        let topConstraint:NSLayoutConstraint = NSLayoutConstraint(item: overviewDividerView, attribute: .top, relatedBy: .equal, toItem: overviewView, attribute: .bottom, multiplier: 1, constant: 0)
        let leadingConstraint:NSLayoutConstraint =  NSLayoutConstraint(item: overviewDividerView, attribute: .leading, relatedBy: .equal, toItem: innerContentView, attribute: .leading, multiplier: 1, constant: viewed.viewConfig.topLeftMargin.x)
        let trailingConstraint:NSLayoutConstraint = NSLayoutConstraint(item: overviewDividerView, attribute: .trailing, relatedBy: .equal, toItem: innerContentView, attribute: .trailing, multiplier: 1, constant: -viewed.viewConfig.bottomRightMargin.x)

        overviewDividerView.addAppConstraint(topConstraint, innerContentView)
        overviewDividerView.addAppConstraint(leadingConstraint, innerContentView)
        overviewDividerView.addAppConstraint(trailingConstraint, innerContentView)
    }

    private func addResultBodyTextView() {
        innerContentView.addSubview(resultBodyTextView)

        let topConstraint:NSLayoutConstraint = NSLayoutConstraint(item: resultBodyTextView, attribute: .top, relatedBy: .equal, toItem: overviewDividerView, attribute: .bottom, multiplier: 1, constant: 0)
        let leadingConstraint:NSLayoutConstraint =  NSLayoutConstraint(item: resultBodyTextView, attribute: .leading, relatedBy: .greaterThanOrEqual, toItem: innerContentView, attribute: .leading, multiplier: 1, constant: viewed.viewConfig.topLeftMargin.x)
        let trailingConstraint:NSLayoutConstraint = NSLayoutConstraint(item: resultBodyTextView, attribute: .trailing, relatedBy: .lessThanOrEqual, toItem: innerContentView, attribute: .trailing, multiplier: 1, constant: -viewed.viewConfig.bottomRightMargin.x)
        let centerXConstraint:NSLayoutConstraint =  NSLayoutConstraint(item: resultBodyTextView, attribute: .centerX, relatedBy: .equal, toItem: innerContentView, attribute: .centerX, multiplier: 1, constant: 0)

        resultBodyTextView.addAppConstraint(topConstraint, innerContentView)
        resultBodyTextView.addAppConstraint(leadingConstraint, innerContentView)
        resultBodyTextView.addAppConstraint(trailingConstraint, innerContentView)
        resultBodyTextView.addAppConstraint(centerXConstraint, innerContentView)
    }

    private func addResultWhenTextView() {
        innerContentView.addSubview(resultWhenTextView)

        let topConstraint:NSLayoutConstraint = NSLayoutConstraint(item: resultWhenTextView, attribute: .top, relatedBy: .equal, toItem: resultBodyTextView, attribute: .bottom, multiplier: 1, constant: 0)
        let leadingConstraint:NSLayoutConstraint =  NSLayoutConstraint(item: resultWhenTextView, attribute: .leading, relatedBy: .greaterThanOrEqual, toItem: innerContentView, attribute: .leading, multiplier: 1, constant: viewed.viewConfig.topLeftMargin.x)
        let trailingConstraint:NSLayoutConstraint = NSLayoutConstraint(item: resultWhenTextView, attribute: .trailing, relatedBy: .lessThanOrEqual, toItem: innerContentView, attribute: .trailing, multiplier: 1, constant: -viewed.viewConfig.bottomRightMargin.x)
        let centerXConstraint:NSLayoutConstraint =  NSLayoutConstraint(item: resultWhenTextView, attribute: .centerX, relatedBy: .equal, toItem: innerContentView, attribute: .centerX, multiplier: 1, constant: 0)

        resultWhenTextView.addAppConstraint(topConstraint, innerContentView)
        resultWhenTextView.addAppConstraint(leadingConstraint, innerContentView)
        resultWhenTextView.addAppConstraint(trailingConstraint, innerContentView)
        resultWhenTextView.addAppConstraint(centerXConstraint, innerContentView)

        let stateSegmentTopConstraint:NSLayoutConstraint = NSLayoutConstraint(item: stateSegment, attribute: .top, relatedBy: .equal, toItem: resultWhenTextView, attribute: .bottom, multiplier: 1, deviceConstant: 8)

        stateSegment.addAppConstraint(stateSegmentTopConstraint, innerContentView)
    }

    func applyPossibleCertainImage(certain:Bool, isYes:Bool ) {
        let imageView:ImageView = certain ? certainImageView : possibleImageView
        let imageName:String = isYes ? "tick.png" : "cross.png"
        imageView.image = UIImage.scaledToSized(named: imageName, width: CGFloat(26).deviceValue)
    }

    var possibleContainerViewHidden:Bool {
        get {
            return possibleContainerView.isHidden
        }
        set {
            possibleContainerView.isHidden = newValue
            if newValue {
                let heightConstraint:NSLayoutConstraint = NSLayoutConstraint(item: possibleContainerView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 0)
                possibleContainerView.addAppConstraint(heightConstraint)
            } else {
                possibleContainerView.removeAppConstraint(.height)
            }
        }
    }

}
