import UIKit

class AppLoadingView: AppView  {

    private var viewed: Viewed { return Viewed.instance }

    let headerImageView:ImageView = ImageView(image: UIImage(named: "launch_header.png")!)
    let decisionsImageView:ImageView = ImageView(image: UIImage(named: "launch_decisions.png")!)
    let guyImageView:ImageView = ImageView(image: UIImage(named: "launch_guy.png")!)

    let activityIndicator: ActivityIndicatorView = ActivityIndicatorView()
    let errorTextView: AttributedTextView = AttributedTextView()
    let statusView:AppView = AppView()

    var innerStatusVerticalConstraint:NSLayoutConstraint? = nil

    var isErrorView:Bool = false
    var isLandscape:Bool = false

    override func initializeView() {
        super.initializeView()
        translatesAutoresizingMaskIntoConstraints = true

        backgroundColor = .white

        activityIndicator.style = .gray
        errorTextView.textContainer.maximumNumberOfLines = 0
    }

    override func addChildViews() {
        super.addChildViews()
        addGuyImageView()
        addDecisionsImageView()
        addHeaderImageView()
        addStatusView()
        applyViewType(isErrorView: false)
        adjustForOrientation()
    }


    private func addGuyImageView() {
        self.addSubview(guyImageView)

        let trailingConstraint:NSLayoutConstraint = NSLayoutConstraint(item: guyImageView, attribute: .trailing, relatedBy: .equal, toItem: self, attribute: .trailing, multiplier: 1, constant: 12)
        let bottomConstraint:NSLayoutConstraint = NSLayoutConstraint(item: guyImageView, attribute: .bottom, relatedBy: .equal, toItem: self, attribute: .bottom, multiplier: 1, constant: -10)

        guyImageView.addAppConstraint(trailingConstraint, self)
        guyImageView.addAppConstraint(bottomConstraint, self)
    }


    private func addDecisionsImageView() {
        self.addSubview(decisionsImageView)

        let topConstraint:NSLayoutConstraint = NSLayoutConstraint(item: decisionsImageView, attribute: .top, relatedBy: .equal, toItem: headerImageView, attribute: .bottom, multiplier: 1, constant: 40)
        let leadingConstraint:NSLayoutConstraint =  NSLayoutConstraint(item: decisionsImageView, attribute: .leading, relatedBy: .equal, toItem: self, attribute: .leading, multiplier: 1, constant: 20)
        let bottomConstraint:NSLayoutConstraint = NSLayoutConstraint(item: decisionsImageView, attribute: .bottom, relatedBy: .lessThanOrEqual, toItem: self, attribute: .bottom, multiplier: 1, constant: -viewed.viewConfig.bottomRightMargin.y)

        topConstraint.priority = UILayoutPriority.defaultLow

        decisionsImageView.addAppConstraint(topConstraint, self)
        decisionsImageView.addAppConstraint(leadingConstraint, self)
        decisionsImageView.addAppConstraint(bottomConstraint, self)
    }


    private func addHeaderImageView() {
        self.addSubview(headerImageView)

        let leadingConstraint:NSLayoutConstraint =  NSLayoutConstraint(item: headerImageView, attribute: .leading, relatedBy: .equal, toItem: self, attribute: .leading, multiplier: 1, constant: 0)

        headerImageView.addAppConstraint(leadingConstraint, self)
    }

    private func addStatusView() {
        self.addSubview(statusView)

    }

    func adjustForOrientation() {
        isLandscape = UIDevice.current.orientation.isLandscape
        let preferSpace:Bool = viewed.viewConfig.deviceWidthProfile.rawValue < DeviceWidthProfile.size2.rawValue
        let statusBarVisible = !UIApplication.shared.isStatusBarHidden
        let headerTopConstant:CGFloat = statusBarVisible ? 24 : preferSpace ? 12 : 4
        let headerTopConstraint:NSLayoutConstraint = NSLayoutConstraint(item: headerImageView, attribute: .top, relatedBy: .equal, toItem: self, attribute: .top, multiplier: 1, constant: headerTopConstant)
        headerImageView.addAppConstraint(headerTopConstraint, self)

        if !isLandscape {
            applyVerticalStatusView()
        } else {
            applyHorizontalStatusView()
        }
        errorTextView.resetPosition()
        applyInnerStatusVerticalConstraint()
    }

    func applyVerticalStatusView() {

        let topConstraint:NSLayoutConstraint  = NSLayoutConstraint(item: statusView, attribute: .top, relatedBy: .equal, toItem: decisionsImageView, attribute: .bottom, multiplier: 1, deviceConstant: 8)
        let leadingConstraint:NSLayoutConstraint  = NSLayoutConstraint(item: statusView, attribute: .leading, relatedBy: .equal, toItem: decisionsImageView, attribute: .leading, multiplier: 1, deviceConstant: 0)
        let trailingConstraint:NSLayoutConstraint  = NSLayoutConstraint(item: statusView, attribute: .trailing, relatedBy: .equal, toItem: guyImageView, attribute: .leading, multiplier: 1, deviceConstant: 30)
        let bottomConstraint:NSLayoutConstraint  = NSLayoutConstraint(item: statusView, attribute: .bottom, relatedBy: .equal, toItem: self, attribute: .bottom, multiplier: 1, deviceConstant: -viewed.viewConfig.bottomRightMargin.y)

        statusView.addAppConstraint(topConstraint, self)
        statusView.addAppConstraint(leadingConstraint, self)
        statusView.addAppConstraint(trailingConstraint, self)
        statusView.addAppConstraint(bottomConstraint, self)

    }

    func applyHorizontalStatusView() {

        let topConstraint:NSLayoutConstraint  = NSLayoutConstraint(item: statusView, attribute: .top, relatedBy: .equal, toItem: headerImageView, attribute: .bottom, multiplier: 1, deviceConstant: 8)
        let leadingConstraint:NSLayoutConstraint  = NSLayoutConstraint(item: statusView, attribute: .leading, relatedBy: .equal, toItem: decisionsImageView, attribute: .trailing, multiplier: 1, deviceConstant: 8)
        let trailingConstraint:NSLayoutConstraint  = NSLayoutConstraint(item: statusView, attribute: .trailing, relatedBy: .equal, toItem: guyImageView, attribute: .leading, multiplier: 1, deviceConstant: 30)
        let bottomConstraint:NSLayoutConstraint  = NSLayoutConstraint(item: statusView, attribute: .bottom, relatedBy: .equal, toItem: decisionsImageView, attribute: .bottom, multiplier: 1, deviceConstant: -8)

        statusView.addAppConstraint(topConstraint, self)
        statusView.addAppConstraint(leadingConstraint, self)
        statusView.addAppConstraint(trailingConstraint, self)
        statusView.addAppConstraint(bottomConstraint, self)
    }

    func applyViewType(isErrorView:Bool ) {
        self.isErrorView = isErrorView
        if !isErrorView {

            errorTextView.removeFromSuperview()
            if activityIndicator.superview == nil {
                statusView.addSubview(activityIndicator)
                let topConstraint:NSLayoutConstraint = NSLayoutConstraint(item: activityIndicator, attribute: .top, relatedBy: .greaterThanOrEqual, toItem: statusView, attribute: .top, multiplier: 1, deviceConstant: 0)
                let leadingConstraint:NSLayoutConstraint =  NSLayoutConstraint(item: activityIndicator, attribute: .leading, relatedBy: .greaterThanOrEqual, toItem: statusView, attribute: .leading, multiplier: 1, deviceConstant: 0)
                let trailingConstraint:NSLayoutConstraint = NSLayoutConstraint(item: activityIndicator, attribute: .trailing, relatedBy: .lessThanOrEqual, toItem: statusView, attribute: .trailing, multiplier: 1, deviceConstant: 0)
                let bottomConstraint:NSLayoutConstraint = NSLayoutConstraint(item: activityIndicator, attribute: .bottom, relatedBy: .lessThanOrEqual, toItem: statusView, attribute: .bottom, multiplier: 1, deviceConstant: 0)
                let centerXConstraint:NSLayoutConstraint = NSLayoutConstraint(item: activityIndicator, attribute: .centerX, relatedBy: .equal, toItem: statusView, attribute: .centerX, multiplier: 1, deviceConstant: 0)

                activityIndicator.addAppConstraint(topConstraint, statusView)
                activityIndicator.addAppConstraint(leadingConstraint, statusView)
                activityIndicator.addAppConstraint(trailingConstraint, statusView)
                activityIndicator.addAppConstraint(bottomConstraint, statusView)
                activityIndicator.addAppConstraint(centerXConstraint, statusView)
            }

        } else {
            activityIndicator.removeFromSuperview()

            if errorTextView.superview == nil {
                statusView.addSubview(errorTextView)
                let topConstraint:NSLayoutConstraint = NSLayoutConstraint(item: errorTextView, attribute: .top, relatedBy: .greaterThanOrEqual, toItem: statusView, attribute: .top, multiplier: 1, deviceConstant: 0)
                let leadingConstraint:NSLayoutConstraint =  NSLayoutConstraint(item: errorTextView, attribute: .leading, relatedBy: .equal, toItem: statusView, attribute: .leading, multiplier: 1, deviceConstant: 0)
                let trailingConstraint:NSLayoutConstraint = NSLayoutConstraint(item: errorTextView, attribute: .trailing, relatedBy: .lessThanOrEqual, toItem: statusView, attribute: .trailing, multiplier: 1, deviceConstant: 0)
                let bottomConstraint:NSLayoutConstraint = NSLayoutConstraint(item: errorTextView, attribute: .bottom, relatedBy: .lessThanOrEqual, toItem: statusView, attribute: .bottom, multiplier: 1, deviceConstant: 0)

                errorTextView.addAppConstraint(topConstraint, statusView)
                errorTextView.addAppConstraint(leadingConstraint, statusView)
                errorTextView.addAppConstraint(trailingConstraint, statusView)
                errorTextView.addAppConstraint(bottomConstraint, statusView)
            }
        }
        applyInnerStatusVerticalConstraint()
    }

    func applyInnerStatusVerticalConstraint() {
        if innerStatusVerticalConstraint != nil {
            statusView.removeConstraint(innerStatusVerticalConstraint!)
            innerStatusVerticalConstraint = nil
        }
        let innerStatusItem:UIView = !isErrorView ? activityIndicator : errorTextView
        if !isLandscape {
            innerStatusVerticalConstraint = NSLayoutConstraint(item: innerStatusItem, attribute: .top, relatedBy: .equal, toItem: statusView, attribute: .top, multiplier: 1, deviceConstant: 0)
        } else if isErrorView {
            innerStatusVerticalConstraint = NSLayoutConstraint(item: innerStatusItem, attribute: .centerY, relatedBy: .equal, toItem: statusView, attribute: .centerY, multiplier: 1, deviceConstant: 0)
        } else {
            innerStatusVerticalConstraint = NSLayoutConstraint(item: innerStatusItem, attribute: .bottom, relatedBy: .equal, toItem: statusView, attribute: .bottom, multiplier: 1, deviceConstant: 0)
        }
        statusView.addConstraint(innerStatusVerticalConstraint!)
    }

}
