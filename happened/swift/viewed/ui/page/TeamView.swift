import UIKit

class TeamView: ScrollContainerView {

    private var viewed: Viewed { return Viewed.instance }

    var topItemsView: AppView = AppView()
    let seasonBtn: Button = RedButton()
    let listingSeasonBtn: Button = RedButton()
    let teamTextView: AttributedTextView = AttributedTextView()
    let listingSeasonTextView: AttributedTextView = AttributedTextView()
    let followingContainerView:AppView = AppView()
    let followingTextView:AttributedTextView = AttributedTextView()
    let followingSwitch:AppSwitch = AppSwitch()
    let receiveAlertsTextView:AttributedTextView = AttributedTextView()
    let receiveAlertsSwitch:AppSwitch = AppSwitch()
    let mainContentView: AppView = AppView()

    override func addChildViews() {
        super.addChildViews()
        addTopItemsView()
        addFollowingContainerView()
        addMainContentView()
    }

    override func applyView() {
        super.applyView()
        _ = topItemsView.addBorder(edges: [.bottom], color: viewed.viewConfig.colorBlue6, thickness: 1)
        _ = followingContainerView.addBorder(edges: [.bottom], color: viewed.viewConfig.colorBlue6, thickness: 1)
        teamTextView.font = UIFont.font(pointSizeStyle: .title1)
        listingSeasonTextView.font = UIFont.font(pointSizeStyle: .title2)
    }

    private func addTopItemsView() {
        innerContentView.addSubview(topItemsView)
        let topConstraint:NSLayoutConstraint = NSLayoutConstraint(item: topItemsView, attribute: .top, relatedBy: .equal, toItem: innerContentView, attribute: .top, multiplier: 1, constant: viewed.viewConfig.topLeftMargin.y)
        let leadingConstraint:NSLayoutConstraint =  NSLayoutConstraint(item: topItemsView, attribute: .leading, relatedBy: .equal, toItem: innerContentView, attribute: .leading, multiplier: 1, constant: viewed.viewConfig.topLeftMargin.x)
        let trailingConstraint:NSLayoutConstraint =  NSLayoutConstraint(item: topItemsView, attribute: .trailing, relatedBy: .equal, toItem: innerContentView, attribute: .trailing, multiplier: 1, constant: -viewed.viewConfig.bottomRightMargin.x)

        topItemsView.addAppConstraint(topConstraint, innerContentView)
        topItemsView.addAppConstraint(leadingConstraint, innerContentView)
        topItemsView.addAppConstraint(trailingConstraint, innerContentView)

        addSeasonBtn()
        addListingSeasonBtn()
        addTeamTextView()
        addListingSeasonTextView()
    }

    private func addSeasonBtn() {
        topItemsView.addSubview(seasonBtn)

        let topConstraint:NSLayoutConstraint = NSLayoutConstraint(item: seasonBtn, attribute: .top, relatedBy: .equal, toItem: topItemsView, attribute: .top, multiplier: 1, constant: 0)
        let leadingConstraint:NSLayoutConstraint =  NSLayoutConstraint(item: seasonBtn, attribute: .leading, relatedBy: .equal, toItem: topItemsView, attribute: .leading, multiplier: 1, constant: 0)

        seasonBtn.addAppConstraint(topConstraint, topItemsView)
        seasonBtn.addAppConstraint(leadingConstraint, topItemsView)
    }

    private func addListingSeasonBtn() {
        topItemsView.addSubview(listingSeasonBtn)

        let topConstraint:NSLayoutConstraint = NSLayoutConstraint(item: listingSeasonBtn, attribute: .top, relatedBy: .equal, toItem: topItemsView, attribute: .top, multiplier: 1, constant: 0)
        let trailingConstraint:NSLayoutConstraint =  NSLayoutConstraint(item: listingSeasonBtn, attribute: .trailing, relatedBy: .equal, toItem: topItemsView, attribute: .trailing, multiplier: 1, constant: 0)

        listingSeasonBtn.addAppConstraint(topConstraint, topItemsView)
        listingSeasonBtn.addAppConstraint(trailingConstraint, topItemsView)
    }

    private func addTeamTextView() {
        topItemsView.addSubview(teamTextView)

        let topConstraint:NSLayoutConstraint = NSLayoutConstraint(item: teamTextView, attribute: .top, relatedBy: .equal, toItem: seasonBtn, attribute: .bottom, multiplier: 1, deviceConstant: 8)
        let leadingConstraint:NSLayoutConstraint =  NSLayoutConstraint(item: teamTextView, attribute: .leading, relatedBy: .greaterThanOrEqual, toItem: topItemsView, attribute: .leading, multiplier: 1, constant: 0)
        let trailingConstraint:NSLayoutConstraint = NSLayoutConstraint(item: teamTextView, attribute: .trailing, relatedBy: .lessThanOrEqual, toItem: topItemsView, attribute: .trailing, multiplier: 1, constant: 0)
        let centerXConstraint:NSLayoutConstraint =  NSLayoutConstraint(item: teamTextView, attribute: .centerX, relatedBy: .equal, toItem: topItemsView, attribute: .centerX, multiplier: 1, constant: 0)

        teamTextView.addAppConstraint(topConstraint, topItemsView)
        teamTextView.addAppConstraint(leadingConstraint, topItemsView)
        teamTextView.addAppConstraint(trailingConstraint, topItemsView)
        teamTextView.addAppConstraint(centerXConstraint, topItemsView)

    }

    private func addListingSeasonTextView() {
        topItemsView.addSubview(listingSeasonTextView)

        let topConstraint:NSLayoutConstraint = NSLayoutConstraint(item: listingSeasonTextView, attribute: .top, relatedBy: .equal, toItem: teamTextView, attribute: .bottom, multiplier: 1, deviceConstant: 0)
        let leadingConstraint:NSLayoutConstraint =  NSLayoutConstraint(item: listingSeasonTextView, attribute: .leading, relatedBy: .greaterThanOrEqual, toItem: topItemsView, attribute: .leading, multiplier: 1, constant: 0)
        let trailingConstraint:NSLayoutConstraint = NSLayoutConstraint(item: listingSeasonTextView, attribute: .trailing, relatedBy: .lessThanOrEqual, toItem: topItemsView, attribute: .trailing, multiplier: 1, constant: 0)
        let bottomConstraint:NSLayoutConstraint = NSLayoutConstraint(item: listingSeasonTextView, attribute: .bottom, relatedBy: .equal, toItem: topItemsView, attribute: .bottom, multiplier: 1, deviceConstant: -4)
        let centerXConstraint:NSLayoutConstraint =  NSLayoutConstraint(item: listingSeasonTextView, attribute: .centerX, relatedBy: .equal, toItem: topItemsView, attribute: .centerX, multiplier: 1, constant: 0)

        listingSeasonTextView.addAppConstraint(topConstraint, topItemsView)
        listingSeasonTextView.addAppConstraint(leadingConstraint, topItemsView)
        listingSeasonTextView.addAppConstraint(trailingConstraint, topItemsView)
        listingSeasonTextView.addAppConstraint(bottomConstraint, topItemsView)
        listingSeasonTextView.addAppConstraint(centerXConstraint, topItemsView)

    }

    private func addFollowingContainerView() {
        innerContentView.addSubview(followingContainerView)

        let topConstraint:NSLayoutConstraint = NSLayoutConstraint(item: followingContainerView, attribute: .top, relatedBy: .equal, toItem: topItemsView, attribute: .bottom, multiplier: 1, deviceConstant: 8)
        let leadingConstraint:NSLayoutConstraint =  NSLayoutConstraint(item: followingContainerView, attribute: .leading, relatedBy: .equal, toItem: innerContentView, attribute: .leading, multiplier: 1, constant: viewed.viewConfig.topLeftMargin.x)
        let trailingConstraint:NSLayoutConstraint = NSLayoutConstraint(item: followingContainerView, attribute: .trailing, relatedBy: .equal, toItem: innerContentView, attribute: .trailing, multiplier: 1, constant: -viewed.viewConfig.bottomRightMargin.x)

        followingContainerView.addAppConstraint(topConstraint, innerContentView)
        followingContainerView.addAppConstraint(leadingConstraint, innerContentView)
        followingContainerView.addAppConstraint(trailingConstraint, innerContentView)

        addFollowingTextView()
        addFollowingSwitch()
        addReceiveAlertsTextView()
        addReceiveAlertsSwitch()
    }

    private func addFollowingTextView() {
        followingContainerView.addSubview(followingTextView)

        let leadingConstraint:NSLayoutConstraint =  NSLayoutConstraint(item: followingTextView, attribute: .leading, relatedBy: .equal, toItem: followingContainerView, attribute: .leading, multiplier: 1, constant: 0)
        let centerYConstraint:NSLayoutConstraint = NSLayoutConstraint(item: followingTextView, attribute: .centerY, relatedBy: .equal, toItem: followingSwitch, attribute: .centerY, multiplier: 1, constant: 0)

        followingTextView.addAppConstraint(leadingConstraint, followingContainerView)
        followingTextView.addAppConstraint(centerYConstraint, followingContainerView)
    }

    private func addFollowingSwitch() {
        followingContainerView.addSubview(followingSwitch)

        let topConstraint:NSLayoutConstraint = NSLayoutConstraint(item: followingSwitch, attribute: .top, relatedBy: .equal, toItem: followingContainerView, attribute: .top, multiplier: 1, constant: 0)
        let trailingConstraint:NSLayoutConstraint = NSLayoutConstraint(item: followingSwitch, attribute: .trailing, relatedBy: .equal, toItem: followingContainerView, attribute: .trailing, multiplier: 1, constant: 0)

        followingSwitch.addAppConstraint(topConstraint, followingContainerView)
        followingSwitch.addAppConstraint(trailingConstraint, followingContainerView)
    }

    private func addReceiveAlertsTextView() {
        followingContainerView.addSubview(receiveAlertsTextView)

        let leadingConstraint:NSLayoutConstraint =  NSLayoutConstraint(item: receiveAlertsTextView, attribute: .leading, relatedBy: .equal, toItem: followingContainerView, attribute: .leading, multiplier: 1, constant: 0)
        let centerYConstraint:NSLayoutConstraint = NSLayoutConstraint(item: receiveAlertsTextView, attribute: .centerY, relatedBy: .equal, toItem: receiveAlertsSwitch, attribute: .centerY, multiplier: 1, constant: 0)

        receiveAlertsTextView.addAppConstraint(leadingConstraint, followingContainerView)
        receiveAlertsTextView.addAppConstraint(centerYConstraint, followingContainerView)
    }

    private func addReceiveAlertsSwitch() {
        followingContainerView.addSubview(receiveAlertsSwitch)

        let topConstraint:NSLayoutConstraint = NSLayoutConstraint(item: receiveAlertsSwitch, attribute: .top, relatedBy: .equal, toItem: followingSwitch, attribute: .bottom, multiplier: 1, deviceConstant: 6)
        let trailingConstraint:NSLayoutConstraint = NSLayoutConstraint(item: receiveAlertsSwitch, attribute: .trailing, relatedBy: .equal, toItem: followingContainerView, attribute: .trailing, multiplier: 1, constant: 0)
        let bottomConstraint:NSLayoutConstraint = NSLayoutConstraint(item: receiveAlertsSwitch, attribute: .bottom, relatedBy: .equal, toItem: followingContainerView, attribute: .bottom, multiplier: 1, deviceConstant: -8)

        receiveAlertsSwitch.addAppConstraint(topConstraint, followingContainerView)
        receiveAlertsSwitch.addAppConstraint(trailingConstraint, followingContainerView)
        receiveAlertsSwitch.addAppConstraint(bottomConstraint, followingContainerView)
    }

    private func addMainContentView() {
        innerContentView.addSubview(mainContentView)

        let topConstraint:NSLayoutConstraint = NSLayoutConstraint(item: mainContentView, attribute: .top, relatedBy: .equal, toItem: followingContainerView, attribute: .bottom, multiplier: 1, deviceConstant: 8)
        let leadingConstraint:NSLayoutConstraint =  NSLayoutConstraint(item: mainContentView, attribute: .leading, relatedBy: .equal, toItem: innerContentView, attribute: .leading, multiplier: 1, constant: 0)
        let trailingConstraint:NSLayoutConstraint = NSLayoutConstraint(item: mainContentView, attribute: .trailing, relatedBy: .equal, toItem: innerContentView, attribute: .trailing, multiplier: 1, constant: 0)
        let bottomConstraint:NSLayoutConstraint = NSLayoutConstraint(item: mainContentView, attribute: .bottom, relatedBy: .lessThanOrEqual, toItem: innerContentView, attribute: .bottom, multiplier: 1, constant: 0)

        mainContentView.addAppConstraint(topConstraint, innerContentView)
        mainContentView.addAppConstraint(leadingConstraint, innerContentView)
        mainContentView.addAppConstraint(trailingConstraint, innerContentView)
        mainContentView.addAppConstraint(bottomConstraint, innerContentView)
    }

}
