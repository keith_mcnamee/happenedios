import UIKit

class CompetitionView: ScrollContainerView {

    private var viewed: Viewed { return Viewed.instance }

    var topItemsView: AppView = AppView()
    var seasonGroupBtn: Button = RedButton()
    var yourLeaguesTextView:AttributedTextView = AttributedTextView()
    var mainContentView: AppView = AppView()

    override func addChildViews() {
        super.addChildViews()
        addTopItemsView()
        addMainContentView()
    }

    override func applyView() {
        super.applyView()
        _ = topItemsView.addBorder(edges: [.bottom], color: viewed.viewConfig.colorBlue6, thickness: 1)
        yourLeaguesTextView.textContainer.maximumNumberOfLines = 0
        yourLeaguesTextView.defaultTextAlignment = .center
    }

    private func addTopItemsView() {
        innerContentView.addSubview(topItemsView)
        let topConstraint:NSLayoutConstraint = NSLayoutConstraint(item: topItemsView, attribute: .top, relatedBy: .equal, toItem: innerContentView, attribute: .top, multiplier: 1, constant: viewed.viewConfig.topLeftMargin.y)
        let leadingConstraint:NSLayoutConstraint =  NSLayoutConstraint(item: topItemsView, attribute: .leading, relatedBy: .equal, toItem: innerContentView, attribute: .leading, multiplier: 1, constant: viewed.viewConfig.topLeftMargin.x)
        let trailingConstraint:NSLayoutConstraint =  NSLayoutConstraint(item: topItemsView, attribute: .trailing, relatedBy: .equal, toItem: innerContentView, attribute: .trailing, multiplier: 1, constant: -viewed.viewConfig.bottomRightMargin.x)

        topItemsView.addAppConstraint(topConstraint, innerContentView)
        topItemsView.addAppConstraint(leadingConstraint, innerContentView)
        topItemsView.addAppConstraint(trailingConstraint, innerContentView)

        let heightConstraint:NSLayoutConstraint = NSLayoutConstraint(item: topItemsView, attribute: .height, priority: UILayoutPriority.defaultLow)
        topItemsView.addAppConstraint(heightConstraint)

        addSeasonBtn()
        addYourLeaguesTextView()
    }

    private func addSeasonBtn() {
        topItemsView.addSubview(seasonGroupBtn)

        let topConstraint:NSLayoutConstraint = NSLayoutConstraint(item: seasonGroupBtn, attribute: .top, relatedBy: .equal, toItem: topItemsView, attribute: .top, multiplier: 1, constant: 0)
        let leadingConstraint:NSLayoutConstraint =  NSLayoutConstraint(item: seasonGroupBtn, attribute: .leading, relatedBy: .equal, toItem: topItemsView, attribute: .leading, multiplier: 1, constant: 0)
        let bottomConstraint:NSLayoutConstraint = NSLayoutConstraint(item: seasonGroupBtn, attribute: .bottom, relatedBy: .lessThanOrEqual, toItem: topItemsView, attribute: .bottom, multiplier: 1, deviceConstant: -8)

        seasonGroupBtn.addAppConstraint(topConstraint, topItemsView)
        seasonGroupBtn.addAppConstraint(leadingConstraint, topItemsView)
        seasonGroupBtn.addAppConstraint(bottomConstraint, topItemsView)
        seasonGroupBtn.addAppConstraint(bottomConstraint, topItemsView)
    }


    private func addYourLeaguesTextView() {
        topItemsView.addSubview(yourLeaguesTextView)

        let topConstraint:NSLayoutConstraint = NSLayoutConstraint(item: yourLeaguesTextView, attribute: .top, relatedBy: .greaterThanOrEqual, toItem: topItemsView, attribute: .top, multiplier: 1, constant: 0)
        let trailingConstraint:NSLayoutConstraint =  NSLayoutConstraint(item: yourLeaguesTextView, attribute: .trailing, relatedBy: .equal, toItem: topItemsView, attribute: .trailing, multiplier: 1, constant: 0)
        let bottomConstraint:NSLayoutConstraint = NSLayoutConstraint(item: yourLeaguesTextView, attribute: .bottom, relatedBy: .lessThanOrEqual, toItem: topItemsView, attribute: .bottom, multiplier: 1, constant: 0)
        let centerYConstraint:NSLayoutConstraint = NSLayoutConstraint(item: yourLeaguesTextView, attribute: .centerY, relatedBy: .equal, toItem: topItemsView, attribute: .centerY, multiplier: 1, constant: 0)

        yourLeaguesTextView.addAppConstraint(topConstraint, topItemsView)
        yourLeaguesTextView.addAppConstraint(trailingConstraint, topItemsView)
        yourLeaguesTextView.addAppConstraint(bottomConstraint, topItemsView)
        yourLeaguesTextView.addAppConstraint(centerYConstraint, topItemsView)

        let widthConstraint:NSLayoutConstraint = NSLayoutConstraint(item: yourLeaguesTextView, attribute: .width, relatedBy: .lessThanOrEqual, deviceConstant: 60)
        yourLeaguesTextView.addConstraint(widthConstraint)
    }

    private func addMainContentView() {
        innerContentView.addSubview(mainContentView)

        let topConstraint:NSLayoutConstraint = NSLayoutConstraint(item: mainContentView, attribute: .top, relatedBy: .equal, toItem: topItemsView, attribute: .bottom, multiplier: 1, constant: 0)
        let leadingConstraint:NSLayoutConstraint =  NSLayoutConstraint(item: mainContentView, attribute: .leading, relatedBy: .equal, toItem: innerContentView, attribute: .leading, multiplier: 1, constant: 0)
        let trailingConstraint:NSLayoutConstraint = NSLayoutConstraint(item: mainContentView, attribute: .trailing, relatedBy: .equal, toItem: innerContentView, attribute: .trailing, multiplier: 1, constant: 0)
        let bottomConstraint:NSLayoutConstraint = NSLayoutConstraint(item: mainContentView, attribute: .bottom, relatedBy: .lessThanOrEqual, toItem: innerContentView, attribute: .bottom, multiplier: 1, constant: 0)

        mainContentView.addAppConstraint(topConstraint, innerContentView)
        mainContentView.addAppConstraint(leadingConstraint, innerContentView)
        mainContentView.addAppConstraint(trailingConstraint, innerContentView)
        mainContentView.addAppConstraint(bottomConstraint, innerContentView)
    }

}
