import Foundation

class PrepareScenarioStateService {

    private var app: App { return App.instance }
    private var happen: Happen { return Happen.instance }
    private var viewed: Viewed { return Viewed.instance }

    var stateVO:ScenarioStateViewVO? = nil

    func prepareVO( stateVO:ScenarioStateViewVO, navigationObserverID:String ) {

        self.stateVO = stateVO

        let filteredFixtureVOs:[Int:FixtureVO] = getFilteredScenarioFixtureVOs()
        let arrayFixtureVOs:[FixtureVO] = happen.happenedHelper.sortFixtureVOs( dicFixtureVOs:filteredFixtureVOs, descendingDate:stateVO.descendingDate )
        let fixturesPerPage:Int = viewed.viewConfig.visibleFixtureSpan

        var allEntryIndex:Int = -1
        var teamEntryIndex:Int = -1

        stateVO.allFixturePageVOs = []
        stateVO.teamFixturePageVOs = []

        var allFixtureVOs:[FixtureVO] = []
        var teamFixtureVOs:[FixtureVO] = []

        for fixtureVO:FixtureVO in arrayFixtureVOs {

            allEntryIndex += 1

            var allFixturePageVO: FixturePageVO? = stateVO.allFixturePageVOs.count > 0 ? stateVO.allFixturePageVOs[ stateVO.allFixturePageVOs.count - 1 ] : nil

            if allFixturePageVO == nil || allFixturePageVO!.firstEntryConstantIndex + fixturesPerPage == allEntryIndex {
                allFixturePageVO = FixturePageVO( pageIndex: stateVO.allFixturePageVOs.count )
                stateVO.allFixturePageVOs.append(allFixturePageVO!)
                allFixturePageVO!.dataRetrieved = true
                allFixturePageVO!.firstEntryConstantIndex = allEntryIndex
            }

            allFixtureVOs.append( fixtureVO )

            if fixtureVO.homeRef == stateVO.teamRef || fixtureVO.awayRef == stateVO.teamRef {
                teamEntryIndex += 1

                var teamFixturePageVO: FixturePageVO? = stateVO.teamFixturePageVOs.count > 0 ? stateVO.teamFixturePageVOs[ stateVO.teamFixturePageVOs.count - 1 ] : nil

                if teamFixturePageVO == nil || teamFixturePageVO!.firstEntryConstantIndex + fixturesPerPage == teamEntryIndex {
                    teamFixturePageVO = FixturePageVO( pageIndex: stateVO.teamFixturePageVOs.count )
                    stateVO.teamFixturePageVOs.append(teamFixturePageVO!)
                    teamFixturePageVO!.dataRetrieved = true
                    teamFixturePageVO!.firstEntryConstantIndex = teamEntryIndex
                }

                teamFixtureVOs.append( fixtureVO )
            }
        }

        if( stateVO.allFixturePageVOs.count == 0 )
        {
            let emptyAllFixturePageVO: FixturePageVO = FixturePageVO( pageIndex: 0 )
            emptyAllFixturePageVO.dataRetrieved = true
            emptyAllFixturePageVO.hasNoEntries = true
            emptyAllFixturePageVO.prepared = true
            stateVO.allFixturePageVOs.append( emptyAllFixturePageVO )
        }
        if( stateVO.teamFixturePageVOs.count == 0 )
        {
            let emptyTeamFixturePageVO: FixturePageVO = FixturePageVO( pageIndex: 0 )
            emptyTeamFixturePageVO.dataRetrieved = true
            emptyTeamFixturePageVO.hasNoEntries = true
            emptyTeamFixturePageVO.prepared = true
            stateVO.teamFixturePageVOs.append( emptyTeamFixturePageVO )
        }

        let listingSeasonVO:ListingSeasonVO = happen.happenedModel.listingSeasonVOs[stateVO.calculatorVO.listingSeasonRef]!
        stateVO.standingVO = viewed.prepareStandingService().prepareStandingVO(listingSeasonVO:listingSeasonVO, calculatorVO: stateVO.calculatorVO, navigationObserverID:navigationObserverID, teamRef: stateVO.teamRef)
        if stateVO.standingVO == nil {
            app.updateErrorAppCommand().command()
            return
        }

        let columnOrder:[FixtureColumnType] = [
                .dateTime,
                .homeTeam,
                .score,
                .awayTeam
        ]

        for pageVO:FixturePageVO in stateVO.allFixturePageVOs {
            viewed.prepareFixturePageService().preparePageVO(pageVO: pageVO, arrayFixtureVOs: allFixtureVOs, entriesPerPage: viewed.viewConfig.visibleFixtureSpan, columnTypes: columnOrder, focusOnTeams: [stateVO.teamRef:NSNull()], singleTeamOnly: false, navigationObserverID: navigationObserverID)
        }

        for pageVO:FixturePageVO in stateVO.teamFixturePageVOs {
            viewed.prepareFixturePageService().preparePageVO(pageVO: pageVO, arrayFixtureVOs: teamFixtureVOs, entriesPerPage: viewed.viewConfig.visibleFixtureSpan, columnTypes: columnOrder, focusOnTeams: [:], singleTeamOnly: true, navigationObserverID: navigationObserverID)
        }
    }
    
    
    private func getFilteredScenarioFixtureVOs() -> [Int:FixtureVO] {

        let inFixtureVOs:[Int:FixtureVO] = stateVO!.calculatorVO.activeFixtureVOs
        
        var filteredFixtureVOs:[Int:FixtureVO] = [:]
        for fixtureVO:FixtureVO in inFixtureVOs.values {
            if stateVO!.fromDate != nil {
                if fixtureVO.sortingDate.timeIntervalSince1970 < stateVO!.fromDate!.timeIntervalSince1970 {
                    continue
                }
            }
            if stateVO!.toDate != nil {
                if fixtureVO.sortingDate.timeIntervalSince1970 > stateVO!.toDate!.timeIntervalSince1970 {
                    continue
                }
            }

            if stateVO!.onlyIncludeRefs != nil {
                if stateVO!.onlyIncludeRefs![fixtureVO.ref] == nil {
                    continue
                }
            }

            if stateVO!.doNotIncludeRefs != nil {
                if stateVO!.doNotIncludeRefs![fixtureVO.ref] != nil {
                    continue
                }
            }
            filteredFixtureVOs[ fixtureVO.ref ] = fixtureVO
        }
        
        return filteredFixtureVOs
    }
}
