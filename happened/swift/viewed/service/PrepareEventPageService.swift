import UIKit

class PrepareEventPageService {

    private var app: App { return App.instance }
    private var balancing: Balancing { return Balancing.instance }
    private var happen: Happen { return Happen.instance }
    private var localization: Localization { return Localization.instance }
    private var viewed: Viewed { return Viewed.instance }

    func preparePageVO( pageVO:EventPageVO, entriesPerPage:Int, feedEntryVOs:[FeedEntryVO] ) {
        pageVO.dateFeedVOs = []
        var dateFeedVO:DateFeedVO?
        var timeFeedVO:TimeFeedVO?
        var competitionFeedVO:CompetitionFeedVO?
        for i in pageVO.firstEntryConstantIndex..<pageVO.firstEntryConstantIndex + entriesPerPage {
            let entryVO:FeedEntryVO? = i < feedEntryVOs.count ? feedEntryVOs[i] : nil
            if entryVO == nil || !entryVO!.requested || entryVO!.invalid || entryVO!.ref == nil {
                continue
            }
            let eventScenarioVO:EventScenarioVO? = happen.happenedModel.eventScenarioVOs[entryVO!.ref!]
            if eventScenarioVO == nil {
                continue
            }
            if timeFeedVO == nil || ( eventScenarioVO!.fromResult && timeFeedVO!.date?.timeIntervalSince1970 != eventScenarioVO!.occurredDate?.timeIntervalSince1970 ) || ( !eventScenarioVO!.fromResult && !timeFeedVO!.anyTime ) {
                let nextDateFeedVO:DateFeedVO? = getNextDateFeedVO(occurredDate: eventScenarioVO!.occurredDate, prevDateVO: dateFeedVO)
                if nextDateFeedVO != nil {
                    dateFeedVO = nextDateFeedVO
                    pageVO.dateFeedVOs.append(dateFeedVO!)
                }
                timeFeedVO = getNextTimeFeedVO(occurredDate: eventScenarioVO!.occurredDate, anyTime: !eventScenarioVO!.fromResult)
                dateFeedVO!.childVOs.append(timeFeedVO!)
                competitionFeedVO = nil
            }
            if competitionFeedVO == nil || ( competitionFeedVO!.seasonRef != eventScenarioVO!.seasonRef || competitionFeedVO!.listingRef != eventScenarioVO!.listingRef ) {
                competitionFeedVO = getNextCompetitionFeedVO(seasonRef: eventScenarioVO!.seasonRef, listingRef: eventScenarioVO!.listingRef)
                timeFeedVO!.childVOs.append(competitionFeedVO!)
            }
            let eventFeedVO:EventFeedVO = getEventFeedVO(eventScenarioRef: entryVO!.ref!)!
            competitionFeedVO!.childVOs.append(eventFeedVO)
        }
        pageVO.prepared = true
    }

    fileprivate func getNextDateFeedVO(occurredDate:Date?, prevDateVO:DateFeedVO? ) -> DateFeedVO? {
        var date12AM:Date?
        var year:Int?
        var text:String?
        if occurredDate != nil {
            date12AM = app.dateHelper.thisMorning12AmDate(date: occurredDate!)
            if date12AM!.timeIntervalSince1970 == prevDateVO?.date12AM?.timeIntervalSince1970 {
                return nil
            }

            let calendar:Calendar = Calendar.current
            year = calendar.component(.year, from: date12AM!)
            let postFormat:String = year == prevDateVO?.year ? " MMMM" : " MMMM yyyy"
            text = app.dateHelper.ordinalDate(date: date12AM!, preFormat: "EEEE ", postFormat: postFormat)
        } else {
            if prevDateVO != nil && prevDateVO!.unscheduled {
                return nil
            }
            text = "Unscheduled".localized
        }
        let textVO:TextVO = TextVO(text!)
        return DateFeedVO([textVO], date12AM: date12AM, year: year)

    }

    fileprivate func getNextTimeFeedVO( occurredDate:Date?, anyTime:Bool = false ) -> TimeFeedVO {

        var text:String?
        var thisAnyTime = anyTime
        if occurredDate != nil {
            let formatter:DateFormatter = DateFormatter()
            formatter.dateFormat = "HH:mm"
            let timeString:String = formatter.string(from: occurredDate!)
            text = localization.localizationHelper.localizeParams("kick_off_time", ["time":timeString], localizationType: LocalizationConstant.NATIVE_TYPE)!
        } else {
            text = "Unscheduled".localized
            thisAnyTime = true
        }
        let textVO:TextVO = TextVO(text!)
        return TimeFeedVO([textVO], date: occurredDate, anyTime: thisAnyTime)
    }

    fileprivate func getNextCompetitionFeedVO( seasonRef:Int, listingRef:Int ) -> CompetitionFeedVO {
        let textVOs:[TextVO] = viewed.viewHelper.createPrimaryCompetitionTextVOs(seasonRef: seasonRef, listingRef: listingRef)
        return CompetitionFeedVO(textVOs, seasonRef: seasonRef, listingRef: listingRef)
    }

    fileprivate func getEventFeedVO( eventScenarioRef:Int ) -> EventFeedVO? {

        let textVOs:[TextVO]? = localization.localizationHelper.getEventScenarioDescription(eventScenarioRef: eventScenarioRef)
        if textVOs == nil {
            return nil
        }

        return EventFeedVO(textVOs!, eventScenarioRef: eventScenarioRef)
    }
}
