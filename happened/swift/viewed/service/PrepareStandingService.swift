import UIKit

class PrepareStandingService {

    private var balancing:Balancing { return Balancing.instance }
    private var happen: Happen { return Happen.instance }
    private var localization: Localization { return Localization.instance }
    private var viewed: Viewed { return Viewed.instance }

    private var listingSeasonVO:ListingSeasonVO?
    private var standingListVO: StandingListVO?
    private var calculatorVO:CalculatorVO?

    private var leagueStructureVO:LeagueStructureBalancingVO?

    func prepareStandingVO( listingSeasonVO:ListingSeasonVO, calculatorVO:CalculatorVO, navigationObserverID:String, teamRef:Int?=nil ) -> StandingListVO? {
        self.listingSeasonVO = listingSeasonVO
        self.calculatorVO = calculatorVO
        self.leagueStructureVO = balancing.balancingHelper.getLeagueStructureVO(listingSeasonVO.seasonRef, listingSeasonVO.listingRef)
        if !calculatorVO.haveData {
            if !listingSeasonVO.haveData {
                return nil
            }
            happen.applyPerformanceCommand( calculatorVO: calculatorVO ).command()
        }
        let(columnTypes, postIndex) = getColumnTypes()
        var columnVOs:[StandingColumnVO] = []
        var rowVOs:[StandingRowVO] = []
        for columnType: StandingColumnType in columnTypes {
            let baseTextID:String = getBaseTextIdForColumnType(columnType: columnType)
            let header:String = localization.localizationHelper.localizeExtended(baseTextID, ext: LocalizationConstant.SHORT_EXTENSION)!
            let description:String = localization.localizationHelper.localizeExtended(baseTextID, ext: LocalizationConstant.LONG_EXTENSION)!
            let columnVO:StandingColumnVO = StandingColumnVO(columnType: columnType, header: [TextVO(header, attributes: [.bold:NSNull()])], description: [TextVO(description)])
            if( columnType != .teamName ) {
                columnVO.attributes[.textAlignment] = [NSTextAlignment.center:NSNull()]
            }
            columnVOs.append(columnVO)
        }

        var splitOccurred:Bool = false
        for teamListingVO:TeamListingVO in calculatorVO.orderedTeamListingVOs {
            if teamListingVO.splitPotIndex != 0 {
                splitOccurred = true
            }
        }

        var teamIndex:Int = -1
        for teamListingVO:TeamListingVO in calculatorVO.orderedTeamListingVOs {
            teamIndex += 1
            var keyValues:[String] = []
            var columnIndex:Int = -1
            for columnVO:StandingColumnVO in columnVOs {
                columnIndex += 1
                if columnIndex == postIndex {
                    columnVO.attributes[.border] = [
                            AppViewAttribute.sides:
                            [AppViewAttribute.left:[
                                    AppViewAttribute.style:[
                                            AppViewAttribute.dashed:NSNull()
                                    ],
                                    AppViewAttribute.backgroundColor:viewed.viewConfig.colorGrey3
                            ]
                            ]
                    ]
                }
                switch( columnVO.columnType ){
                    case .rank:

                        let rank:Int = teamListingVO.performanceVO.rankHigh
                        let prevTeamRank:Int = teamIndex > 0 ? calculatorVO.orderedTeamListingVOs[ teamIndex - 1 ].performanceVO.rankHigh : 0
                        keyValues.append( rank != prevTeamRank ? String(rank) : "-" )
                        break

                    case .teamName:
                        keyValues.append(localization.localizationHelper.getTeamName( teamListingVO.teamRef, seasonRef: teamListingVO.seasonRef))
                        break

                    case .played:
                        keyValues.append(String(teamListingVO.performanceVO.played))
                        break

                    case .remaining:
                        keyValues.append(String(teamListingVO.performanceVO.remaining))
                        break

                    case .awayGoals:
                        keyValues.append(String(teamListingVO.performanceVO.awayGoalsScored))
                        break

                    case .matchesWon:
                        keyValues.append(String(teamListingVO.performanceVO.gamesWon))
                        break

                    case .matchesDrawn:
                        keyValues.append(String(teamListingVO.performanceVO.gamesDrawn))
                        break

                    case .matchesLost:
                        keyValues.append(String(teamListingVO.performanceVO.gamesLost))
                        break

                    case .awayWins:
                        keyValues.append(String(teamListingVO.performanceVO.awayWins))
                        break

                    case .goalsConceded:
                        keyValues.append(String(teamListingVO.performanceVO.goalsAgainst))
                        break

                    case .goalsScored:
                        keyValues.append(String(teamListingVO.performanceVO.goalsFor))
                        break

                    case .goalDifference:
                        keyValues.append(String(teamListingVO.performanceVO.goalDifference))
                        break

                    case .headToHead:
                        keyValues.append(teamListingVO.performanceVO.miniLeagueRank != 0 ? String(teamListingVO.performanceVO.miniLeagueRank) : "-")
                        break

                    case .points:
                        keyValues.append(String(teamListingVO.performanceVO.points))
                        break

                    case .splitPotIndex:
                        keyValues.append(splitOccurred ? String(teamListingVO.splitPotIndex + 1) : "-")
                        break
                }
            }
            let rowVO:StandingRowVO = StandingRowVO()
            columnIndex = -1
            for keyValue:String in keyValues {
                columnIndex += 1
                let columnVO:StandingColumnVO = columnVOs[columnIndex]
                let textVO:TextVO = TextVO(keyValue)
                
                if columnVO.columnType == .teamName {
                    textVO.attributes[.observerID] = navigationObserverID
                    textVO.attributes[.requestType] = AppConstant.SHOW_TEAM_REQUEST
                    textVO.attributes[.action] = teamListingVO.ref
                }
                if teamListingVO.teamRef == teamRef {
                    textVO.attributes[.bold] = NSNull()
                }
                rowVO.columnTextVOs[columnVO.columnType] = [textVO]
            }

            rowVOs.append(rowVO)
        }

        for (eventRef, byTeamRef) in calculatorVO.maximumPlaceEventRefs {
            let eventBalancingVO:EventBalancingVO = balancing.balancingModel.getEventVO(eventRef, seasonRef: listingSeasonVO.seasonRef)!
            if eventBalancingVO.ref == BalancingConfig.EventRefs.win.rawValue || eventBalancingVO.ref == BalancingConfig.EventRefs.groupWin.rawValue {
                continue
            }

            var backgroundColor:UIColor?
            switch eventBalancingVO.ref {
                case BalancingConfig.EventRefs.groupQualify.rawValue:
                    backgroundColor = viewed.viewConfig.standingColorGreen1
                    break
                case BalancingConfig.EventRefs.championsLeague.rawValue,
                     BalancingConfig.EventRefs.automaticChampionsLeague.rawValue,
                     BalancingConfig.EventRefs.promotion.rawValue,
                     BalancingConfig.EventRefs.automaticPromotion.rawValue:
                    backgroundColor = viewed.viewConfig.standingColorGreen2
                    break
                case BalancingConfig.EventRefs.relegation.rawValue,
                     BalancingConfig.EventRefs.automaticRelegation.rawValue:
                    backgroundColor = viewed.viewConfig.standingColorRed1
                    break
                case BalancingConfig.EventRefs.relegationPlayOff.rawValue:
                    backgroundColor = viewed.viewConfig.standingColorRed2
                    break
                case BalancingConfig.EventRefs.championsLeaguePlayOff.rawValue:
                    backgroundColor = viewed.viewConfig.standingColorGreen3
                    break
                case BalancingConfig.EventRefs.europaLeague.rawValue:
                    backgroundColor = viewed.viewConfig.standingColorBlue2
                    break
                case BalancingConfig.EventRefs.europaLeaguePlayOff.rawValue,
                     BalancingConfig.EventRefs.domesticEuropaLeaguePlayOff.rawValue,
                     BalancingConfig.EventRefs.promotionPlayOff.rawValue:
                    backgroundColor = viewed.viewConfig.standingColorYellow1
                    break
                case BalancingConfig.EventRefs.groupEuropaLeague.rawValue:
                    backgroundColor = viewed.viewConfig.standingColorBlue3
                    break
                default:
                    break
            }
            var rowIndex:Int = -1
            var eventRowCount:Int = 0
            for teamListingVO:TeamListingVO in calculatorVO.orderedTeamListingVOs {
                rowIndex += 1
                if byTeamRef.vos[ teamListingVO.teamRef ] != nil {
                    eventRowCount += 1
                    if backgroundColor != nil {
                        rowVOs[rowIndex].attributes[.backgroundColor] = backgroundColor!
                    }
                    if eventRowCount == byTeamRef.vos.count {
                        if eventBalancingVO.ref == BalancingConfig.EventRefs.split.rawValue {
                            rowVOs[rowIndex].attributes[.border] = [
                                    AppViewAttribute.sides:
                                        [AppViewAttribute.bottom:[
                                                AppViewAttribute.style:[
                                                        AppViewAttribute.dashed:NSNull()
                                                ],
                                                AppViewAttribute.backgroundColor:viewed.viewConfig.colorGrey3
                                        ]
                                        ]
                            ]
                        }
                    }
                }
            }
            if backgroundColor != nil {
                var rowIndex:Int = -1
                for teamListingVO:TeamListingVO in calculatorVO.orderedTeamListingVOs {
                    rowIndex += 1
                    if byTeamRef.vos[ teamListingVO.teamRef ] != nil {
                        rowVOs[rowIndex].attributes[.backgroundColor] = backgroundColor!
                    }
                }
            }
        }

        let eventBalancingVOs:[EventBalancingVO] = viewed.viewHelper.getSortedEventBalancingVOs(seasonRef: listingSeasonVO.seasonRef, listingRef: listingSeasonVO.listingRef, includeDuplicates: true)
        var eventIndexes:[Int:Int] = [:]
        var exemptionVOs:[ExemptionTeamVO] = []

        let placeEventHeaderVOs:[TextVO] = [ TextVO( "event".localized ), TextVO( "positions".localized) ]
        var placeEventRowVOs:[[TextVO]] = []
        var eventIndex:Int = -1;
        for eventBalancingVO:EventBalancingVO in eventBalancingVOs {
            eventIndex += 1
            if eventBalancingVO.ref == BalancingConfig.EventRefs.win.rawValue || eventBalancingVO.ref == BalancingConfig.EventRefs.groupWin.rawValue {
                continue
            }
            eventIndexes[ eventBalancingVO.ref ] = eventIndex
            let minimumPlaces:Int = calculatorVO.minimumPlaceEventRefs[ eventBalancingVO.ref ]!.vos.count
            let maximumPlaces:Int = calculatorVO.maximumPlaceEventRefs[ eventBalancingVO.ref ]!.vos.count
            if( maximumPlaces == 0 )
            {
                continue
            }
            let eventName:String = localization.localizationHelper.getCompetitionEventTitle( listingSeasonVO.listingRef, eventBalancingVO.ref, seasonRef: listingSeasonVO.seasonRef )
            let amountPlaces:String = minimumPlaces == maximumPlaces ? String( maximumPlaces ) : String( minimumPlaces ) + viewed.viewConfig.nonBreakCode + viewed.viewConfig.hyphen + viewed.viewConfig.nonBreakCode + String( maximumPlaces )
            let textVOs:[TextVO] = [TextVO(eventName), TextVO(amountPlaces)]
            placeEventRowVOs.append(textVOs)

            for teamListingVO:TeamListingVO in calculatorVO.orderedTeamListingVOs {
                let teamEventVO:EventTeamVO = teamListingVO.eventTeamVOs[ eventBalancingVO.ref ]!
                let exemptionVO:ExemptionTeamVO? = teamEventVO.exemptionRef != nil ? happen.happenedModel.exemptionTeamVOs[ teamEventVO.exemptionRef! ] : nil
                if exemptionVO != nil && elapsed( exemptionVO!.appliedDate ) {
                    exemptionVOs.append( exemptionVO! )
                }
            }
        }

        var adjustVOs:[AdjustTeamVO] = []
        for teamListingVO:TeamListingVO in calculatorVO.orderedTeamListingVOs {

            for  adjustTeamRef:Int in teamListingVO.adjustTeamRefs.keys {
                let adjustTeamVO:AdjustTeamVO = happen.happenedModel.adjustTeamVOs[ adjustTeamRef ]!
                if( elapsed( adjustTeamVO.appliedDate ) )
                {
                    adjustVOs.append( adjustTeamVO )
                }
            }
        }

        exemptionVOs.sort {
            if $0.appliedDate != nil || $1.appliedDate != nil {
                if $0.appliedDate == nil {
                    return true
                }
                if $1.appliedDate == nil {
                    return false
                }
                if $0.appliedDate! < $1.appliedDate! {
                    return true
                }
                if $0.appliedDate! > $1.appliedDate! {
                    return false
                }
            }


            let aTeamName:String = localization.localizationHelper.getTeamName($0.teamRef, seasonRef: $0.seasonRef)
            let bTeamName:String = localization.localizationHelper.getTeamName($1.teamRef, seasonRef: $1.seasonRef)
            if aTeamName > bTeamName {
                return false
            }
            if aTeamName < bTeamName {
                return true
            }

            let aEventIndex:Int = eventIndexes[ $0.eventRef ]!
            let bEventIndex:Int = eventIndexes[ $1.eventRef ]!
            if aEventIndex > bEventIndex {
                return false
            }
            if aEventIndex < bEventIndex {
                return true
            }
            return false

        }

        adjustVOs.sort {

            if $0.appliedDate != nil || $1.appliedDate != nil {
                if $0.appliedDate == nil {
                    return true
                }
                if $1.appliedDate == nil {
                    return false
                }
                if $0.appliedDate! < $1.appliedDate! {
                    return true
                }
                if $0.appliedDate! > $1.appliedDate! {
                    return false
                }
            }

            if abs($0.pointsOffset) < abs($1.pointsOffset) {
                return false
            }
            if abs($0.pointsOffset) > abs($1.pointsOffset) {
                return true
            }

            if $0.pointsOffset < $1.pointsOffset {
                return true
            }
            if $0.pointsOffset > $1.pointsOffset {
                return false
            }

            if abs($0.goalOffset) < abs($1.goalOffset) {
                return false
            }
            if abs($0.goalOffset) > abs($1.goalOffset) {
                return true
            }

            if $0.goalOffset < $1.goalOffset {
                return true
            }
            if $0.goalOffset > $1.goalOffset {
                return false
            }

            let aTeamName:String = localization.localizationHelper.getTeamName($0.teamRef, seasonRef: $0.seasonRef)
            let bTeamName:String = localization.localizationHelper.getTeamName($1.teamRef, seasonRef: $1.seasonRef)
            if aTeamName > bTeamName {
                return false
            }
            if aTeamName < bTeamName {
                return true
            }

            return false
        }

        let dateFormatter:DateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yy"

        let exemptionHeaderVOs:[TextVO] = [ TextVO( "team".localized ), TextVO( "event".localized ), TextVO( "date_applied".localized) ]
        var exemptionRowVOs:[[TextVO]] = []
        for exemptionVO:ExemptionTeamVO in exemptionVOs {
            let dateString:String = dateFormatter.string(from: exemptionVO.appliedDate!)
            let teamName:String =  localization.localizationHelper.getTeamName( exemptionVO.teamRef, seasonRef: exemptionVO.seasonRef )
            let eventName:String = localization.localizationHelper.getCompetitionEventTitle( exemptionVO.listingRef, exemptionVO.eventRef, seasonRef: exemptionVO.seasonRef )
            exemptionRowVOs.append([ TextVO( teamName ), TextVO( eventName ), TextVO( dateString)])
        }

        var haveAgainstPoints = false
        var haveAgainstGoals = false
        var haveForPoints = false
        var haveForGoals = false
        for adjustTeamVO:AdjustTeamVO in adjustVOs {

            if (adjustTeamVO.goalOffset > 0) {
                haveForGoals = true
            }
            else if (adjustTeamVO.goalOffset < 0) {
                haveAgainstGoals = true
            }

            if (adjustTeamVO.pointsOffset > 0) {
                haveForPoints = true
            }
            else if (adjustTeamVO.pointsOffset < 0) {
                haveAgainstPoints = true
            }

        }

        var adjustmentAgainstTitleVOs:[TextVO] = []
        var adjustmentForTitleVOs:[TextVO] = []

        if haveAgainstPoints || haveAgainstGoals {
            let text:String?
            if haveAgainstPoints && haveAgainstGoals {
                text = localization.localizationHelper.localizeParams("value1_value2_deducted", ["value1":"points".localized, "value2":"goals".localized], localizationType: LocalizationConstant.NATIVE_TYPE)!
            } else {
                let valueID:String = haveAgainstPoints ? "points" : "goals"
                text = localization.localizationHelper.localizeParams("value_deducted", ["value":valueID.localized], localizationType: LocalizationConstant.NATIVE_TYPE)!
            }
            adjustmentAgainstTitleVOs.append(TextVO(text!))
        }

        if haveForPoints || haveForGoals {
            let text:String?
            if haveForPoints && haveForGoals {
                text = localization.localizationHelper.localizeParams("value1_value2_awarded", ["value1":"points".localized, "value2":"goals".localized], localizationType: LocalizationConstant.NATIVE_TYPE)!
            } else {
                let valueID:String = haveForPoints ? "points" : "goals"
                text = localization.localizationHelper.localizeParams("value_awarded", ["value":valueID.localized], localizationType: LocalizationConstant.NATIVE_TYPE)!
            }
            adjustmentForTitleVOs.append(TextVO(text!))
        }
        var adjustmentAgainstHeaderVOs:[TextVO] = [ TextVO( "team".localized ) ]
        var adjustmentForHeaderVOs:[TextVO] = [ TextVO( "team".localized ) ]

        if haveAgainstPoints {
            adjustmentAgainstHeaderVOs.append( TextVO( "points".localized ) )
        }
        if haveAgainstGoals {
            adjustmentAgainstHeaderVOs.append( TextVO( "goals".localized ) )
        }
        adjustmentAgainstHeaderVOs.append( TextVO( "date_applied".localized ) )

        if haveForPoints {
            adjustmentForHeaderVOs.append( TextVO( "points".localized ) )
        }
        if haveForGoals {
            adjustmentForHeaderVOs.append( TextVO( "goals".localized ) )
        }
        adjustmentForHeaderVOs.append( TextVO( "date_applied".localized ) )

        var adjustmentAgainstRowVOs:[[TextVO]] = []
        var adjustmentForRowVOs:[[TextVO]] = []

        for adjustTeamVO:AdjustTeamVO in adjustVOs {

            let dateString:String = dateFormatter.string(from: adjustTeamVO.appliedDate!)

            let teamName:String =  localization.localizationHelper.getTeamName( adjustTeamVO.teamRef, seasonRef: adjustTeamVO.seasonRef )

            if adjustTeamVO.pointsOffset < 0 || adjustTeamVO.goalOffset < 0 {
                var againstRow:[TextVO] = [ TextVO( teamName ) ]
                if haveAgainstPoints {
                    var amount:Int = 0
                    if adjustTeamVO.pointsOffset < 0 {
                        amount = abs( adjustTeamVO.pointsOffset )
                    }
                    againstRow.append( TextVO( String( amount ) ) )
                }
                if haveAgainstGoals {
                    var amount:Int = 0
                    if adjustTeamVO.goalOffset < 0 {
                        amount = abs( adjustTeamVO.goalOffset )
                    }
                    againstRow.append( TextVO( String( amount ) ) )
                }
                againstRow.append( TextVO( dateString ) )
                adjustmentAgainstRowVOs.append( againstRow )
            }

            if adjustTeamVO.pointsOffset > 0 || adjustTeamVO.goalOffset > 0 {
                var forRow:[TextVO] = [ TextVO( teamName ) ]
                if haveForPoints {
                    var amount:Int = 0
                    if adjustTeamVO.pointsOffset > 0 {
                        amount = adjustTeamVO.pointsOffset
                    }
                    forRow.append( TextVO( String( amount ) ) )
                }
                if haveForGoals {
                    var amount:Int = 0
                    if adjustTeamVO.goalOffset > 0 {
                        amount = adjustTeamVO.goalOffset
                    }
                    forRow.append( TextVO( String( amount ) ) )
                }
                forRow.append( TextVO( dateString ) )
                adjustmentForRowVOs.append( forRow )
            }
        }
        return StandingListVO(columnVOs: columnVOs, rowVOs: rowVOs, placeEventHeaderVOs: placeEventHeaderVOs, placeEventRowVOs: placeEventRowVOs, exemptionHeaderVOs: exemptionHeaderVOs, exemptionRowVOs: exemptionRowVOs, adjustmentAgainstTitleVOs: adjustmentAgainstTitleVOs, adjustmentAgainstHeaderVOs: adjustmentAgainstHeaderVOs, adjustmentAgainstRowVOs: adjustmentAgainstRowVOs, adjustmentForTitleVOs: adjustmentForTitleVOs, adjustmentForHeaderVOs: adjustmentForHeaderVOs, adjustmentForRowVOs: adjustmentForRowVOs)
    }


    private func getColumnTypes() -> ([StandingColumnType], Int) {

        var keysDictionary:[String:NSNull] = [:]
        for listingDecidedType:String in leagueStructureVO!.listingDecidedOrder! {
            keysDictionary[listingDecidedType] = NSNull()
        }

        var columnTypes:[StandingColumnType] = [
                .rank,
                .teamName,
                .played,
                .remaining
        ]
        if keysDictionary[ BalancingConstant.MATCHES_WON ] == nil
        {
            columnTypes.append( .matchesWon )
        }
        columnTypes.append( .matchesDrawn )
        columnTypes.append( .matchesLost )
        let postIndex:Int = columnTypes.count
        for listingDecidedType:String in leagueStructureVO!.listingDecidedOrder!.reversed() {
            switch listingDecidedType {
                case BalancingConstant.GOAL_DIFFERENCE:
                    columnTypes.append( .goalDifference )
                    break
                case BalancingConstant.AWAY_GOALS:
                    columnTypes.append( .awayGoals )
                    break
                case BalancingConstant.MATCHES_WON:
                    columnTypes.append( .matchesWon )
                    break
                case BalancingConstant.AWAY_WINS:
                    columnTypes.append( .awayWins )
                    break
                case BalancingConstant.GOALS_SCORED:
                    columnTypes.append( .goalsConceded )
                    columnTypes.append( .goalsScored )
                    break
                case BalancingConstant.HEAD_TO_HEAD_PT_GD,
                     BalancingConstant.HEAD_TO_HEAD_PT_GD_GS,
                     BalancingConstant.HEAD_TO_HEAD_PT_GD_AG,
                     BalancingConstant.HEAD_TO_HEAD_PT_GD_GS_AG,
                     BalancingConstant.HEAD_TO_HEAD_GENERIC:
                    columnTypes.append( .headToHead )
                    break
                default:
                    break
            }
        }

        columnTypes.append( .points )

        if calculatorVO!.splSplitVO.isSplSplit
        {
            columnTypes.append( .splitPotIndex )
        }
        return (columnTypes, postIndex)
    }

    private func getBaseTextIdForColumnType(columnType: StandingColumnType) -> String{
    
        switch(columnType) {
            case .rank:
                return "TablePosition"

            case .teamName:
                return "TableTeam"

            case .played:
                return "TablePlayed"

            case .remaining:
                return "TableRemaining"

            case .awayGoals:
                return "TableAwayGoals"

            case .matchesWon:
                return "TableMatchesWon"

            case .matchesDrawn:
                return "TableMatchesDrawn"

            case .matchesLost:
                return "TableMatchesLost"

            case .awayWins:
                return "TableAwayWins"

            case .goalsConceded:
                return "TableGoalsConceded"

            case .goalsScored:
                return "TableGoalsScored"

            case .goalDifference:
                return "TableGoalDifference"

            case .headToHead:
                return "TableHeadToHead"

            case .points:
                return "TablePoints"

            case .splitPotIndex:
                return "TableSplitPotIndex"
        }
    }

    private func elapsed( _ date:Date?) -> Bool {
        if calculatorVO!.date == nil {
            return true//there is no future. everything we can possible know about has elapsed
        }
        if date == nil {
            return false
        }
        return date! <= calculatorVO!.date!
    }
}
