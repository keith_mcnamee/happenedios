import Foundation

class CompetitionListService {

    private var happen: Happen { return Happen.instance }
    private var localization:Localization { return Localization.instance }
    private var preference: Preference { return Preference.instance }

    func createCompetitionRows(seasonGroupRef:Int, skipFilter:Bool = false ) -> [CompetitionRowVO]  {

        func createCompetitionRowVO(_ competitionVO:CompetitionVO, parentCompetitionRef:Int? = nil ) -> CompetitionRowVO {

            let competitionName:String = localization.localizationHelper.getCompetitionName( competitionVO.ref, seasonRef: competitionVO.balancingVO.seasonRef )
            let rowVO: CompetitionRowVO = CompetitionRowVO( competitionRef: competitionVO.ref, textVOs: [TextVO(competitionName)] )

            if !competitionVO.listingParent {
                var childCompetitionVOs:[CompetitionVO] = []
                for competitionRef:Int in competitionVO.childCompetitionRefs.keys {
                    let competitionVO:CompetitionVO = happen.happenedModel.getCompetitionVO(competitionRef, seasonGroupRef: seasonGroupRef)!
                    childCompetitionVOs.append( competitionVO )
                }

                childCompetitionVOs = happen.happenedHelper.orderCompetitionVOs( childCompetitionVOs )
                for childCompetitionVO:CompetitionVO in childCompetitionVOs {
                    let childVO: CompetitionRowVO = createCompetitionRowVO( childCompetitionVO, parentCompetitionRef: rowVO.competitionRef)
                    rowVO.childCompetitionVOs.append(childVO)
                }

            } else {
                var childListingSeasonVOs:[ListingSeasonVO] = []
                for listingSeasonRef:Int in competitionVO.childListingSeasonRefs.keys {
                    childListingSeasonVOs.append( happen.happenedModel.listingSeasonVOs[ listingSeasonRef ]! )
                }
                childListingSeasonVOs = happen.happenedHelper.orderListingSeasonVOs( childListingSeasonVOs )
                for childListingSeasonVO:ListingSeasonVO in childListingSeasonVOs {
                    let listingName:String = localization.localizationHelper.getCompetitionOrListingName( childListingSeasonVO.listingRef, seasonRef: childListingSeasonVO.seasonRef, allowDefault: true)!
                    let listingViewVO: CompetitionListingRowVO = CompetitionListingRowVO( listingRef: childListingSeasonVO.listingRef, listingSeasonRef:childListingSeasonVO.ref, textVOs: [TextVO(listingName)] )
                    rowVO.childListingVOs.append( listingViewVO )
                }
            }

            return rowVO
        }


        func applyFilter(_ rootCompetitionRowVOs:[CompetitionRowVO] ) -> [CompetitionRowVO]{

            func filterRowVOs(_ rowVOs:[CompetitionRowVO] ) {

                for rowVO: CompetitionRowVO in rowVOs {
                    var merge:Bool = rowVO.childCompetitionVOs.count > 0
                    for childCompetitionVO: CompetitionRowVO in rowVO.childCompetitionVOs {
                        if childCompetitionVO.childCompetitionVOs.count != 0 || childCompetitionVO.childListingVOs.count != 1 {
                            merge = false
                        }
                    }
                    if merge {
                        rowVO.childListingVOs = []
                        for childCompetitionVO: CompetitionRowVO in rowVO.childCompetitionVOs {
                            let childListingVO: CompetitionListingRowVO = childCompetitionVO.childListingVOs[0]
                            rowVO.childListingVOs.append(childListingVO)
                        }
                        rowVO.childCompetitionVOs = []
                    }
                }
                for rowVO: CompetitionRowVO in rowVOs {

                    filterRowVOs( rowVO.childCompetitionVOs)

                    if rowVO.childCompetitionVOs.count == 1 {
                        let childCompetitionVO: CompetitionRowVO = rowVO.childCompetitionVOs[0]
                        rowVO.childCompetitionVOs = childCompetitionVO.childCompetitionVOs
                        rowVO.childListingVOs = childCompetitionVO.childListingVOs
                    }
                }
            }

            func checkRowEmpty(_ rowVO: CompetitionRowVO) -> Bool {
                var validRowVOs:[CompetitionRowVO] = []
                for childRowVO: CompetitionRowVO in rowVO.childCompetitionVOs {
                    if !checkRowEmpty(childRowVO) {
                        validRowVOs.append(childRowVO)
                    }
                }
                rowVO.childCompetitionVOs = validRowVOs
                return rowVO.childCompetitionVOs.count == 0 && rowVO.childListingVOs.count == 0
            }

            if !skipFilter {
                filterRowVOs(rootCompetitionRowVOs)
            }

            var returnRowVOs:[CompetitionRowVO] = []
            for rootCompetitionRowVO: CompetitionRowVO in rootCompetitionRowVOs {
                if !checkRowEmpty(rootCompetitionRowVO) {
                    if rootCompetitionRowVOs.count == 1 && !skipFilter {
                        returnRowVOs.append(contentsOf: rootCompetitionRowVO.childCompetitionVOs)
                    } else {
                        returnRowVOs.append(rootCompetitionRowVO)
                    }
                }
            }

            return returnRowVOs
        }

        let seasonGroupVO:SeasonGroupVO = happen.happenedModel.seasonGroupVOs[ seasonGroupRef ]!

        var rootCompetitionVOs:[CompetitionVO] = []
        for rootCompetitionRef:Int in seasonGroupVO.rootCompetitionRefs.keys {
            let rootCompetitionVO:CompetitionVO = happen.happenedModel.getCompetitionVO(rootCompetitionRef, seasonGroupRef: seasonGroupVO.ref)!
            rootCompetitionVOs.append( rootCompetitionVO )
        }
        rootCompetitionVOs = happen.happenedHelper.orderCompetitionVOs(rootCompetitionVOs)

        var rootCompetitionRowVOs:[CompetitionRowVO] = []
        for rootCompetitionVO:CompetitionVO in rootCompetitionVOs {
            let childVO: CompetitionRowVO = createCompetitionRowVO( rootCompetitionVO )
            rootCompetitionRowVOs.append(childVO)
        }

        rootCompetitionRowVOs = applyFilter(rootCompetitionRowVOs)

        return rootCompetitionRowVOs
    }

    func flattenCompetitionVOs( _ rowVOs:[CompetitionRowVO] ) -> [Int: CompetitionRowVO] {
        var dictionary:[Int: CompetitionRowVO] = [:]

        func flatten( _ flattenRowVOs:[CompetitionRowVO] ) {
            for flattenRowVO: CompetitionRowVO in flattenRowVOs {
                dictionary[ flattenRowVO.competitionRef ] = flattenRowVO
                flatten( flattenRowVO.childCompetitionVOs)
            }
        }

        flatten(rowVOs)
        return dictionary
    }

    func flattenListingVOs( _ rowVOs:[CompetitionRowVO] ) -> [Int: CompetitionListingRowVO] {
        var dictionary:[Int: CompetitionListingRowVO] = [:]

        func flatten( _ flattenRowVOs:[CompetitionRowVO] ) {
            for flattenRowVO: CompetitionRowVO in flattenRowVOs {
                if flattenRowVO.childListingVOs.count == 0 {
                    flatten( flattenRowVO.childCompetitionVOs )
                } else {
                    for listingViewVO: CompetitionListingRowVO in flattenRowVO.childListingVOs {
                        dictionary[ listingViewVO.listingRef ] = listingViewVO
                    }
                }
            }
        }

        flatten(rowVOs)
        return dictionary
    }

    func getCompetitionSelectedStates(rowVOs:[Int: CompetitionRowVO] ) -> [Int:MultipleSelectedState] {
        let selectedListingRefs:[Int:NSNull] = preference.preferenceHelper.getListingRefs( allowEmpty: true)
        var competitionSelectedStates:[Int:MultipleSelectedState] = [:]
        for competitionRef: Int in rowVOs.keys {
            competitionSelectedStates[competitionRef] = getCompetitionSelectedState(competitionRef, competitionRowVOs: rowVOs, selectedListingRefs: selectedListingRefs)
        }
        return competitionSelectedStates
    }

    func allSelectedState(rowVOs:[CompetitionRowVO], competitionSelectedStates:[Int:MultipleSelectedState] ) -> MultipleSelectedState {
        var partial = false
        var fully:Bool = true
        for rowVO: CompetitionRowVO in rowVOs {
            let value: MultipleSelectedState = competitionSelectedStates[rowVO.competitionRef]!
            if value != .fully {
                fully = false
            }
            if value != .not {
                partial = true
            }
            if partial && !fully {
                break
            }
        }
        if fully {
            return .fully
        }
        return partial ? .partial : .not
    }

    func getCompetitionSelectedState(_ competitionRef:Int, competitionRowVOs:[Int: CompetitionRowVO], selectedListingRefs:[Int:NSNull] ) -> MultipleSelectedState {

        var partial = false
        var fully = true
        let competitionRowVO: CompetitionRowVO = competitionRowVOs[ competitionRef ]!
        if !competitionRowVO.isListingParent {
            for childCompetitionRowVO: CompetitionRowVO in competitionRowVO.childCompetitionVOs {
                let childValue: MultipleSelectedState = getCompetitionSelectedState( childCompetitionRowVO.competitionRef, competitionRowVOs: competitionRowVOs, selectedListingRefs: selectedListingRefs)
                if childValue != .fully {
                    fully = false
                }
                if childValue != .not {
                    partial = true
                }
                if partial && !fully {
                    break
                }
            }
        } else {
            for childListingViewVO: CompetitionListingRowVO in competitionRowVO.childListingVOs {
                let isSelected:Bool = selectedListingRefs[ childListingViewVO.listingRef ] != nil
                if isSelected {
                    partial = true
                } else {
                    fully = false
                }
                if partial && !fully {
                    break
                }
            }
        }
        if fully {
            return .fully
        }
        return partial ? .partial : .not
    }

    func getListingSelectedStates( listingDictionaryVOs:[Int: CompetitionListingRowVO] ) -> [Int:Bool] {
        let selectedListingRefs:[Int:NSNull] = preference.preferenceHelper.getListingRefs( allowEmpty: true)
        var listingSelectedStates:[Int:Bool] = [:]
        for listingViewVO: CompetitionListingRowVO in listingDictionaryVOs.values {
            let isSelected:Bool = selectedListingRefs[ listingViewVO.listingRef ] != nil
            listingSelectedStates[ listingViewVO.listingRef ] = isSelected
        }
        return listingSelectedStates
    }

    func getDefaultIsOpenStates( competitionSelectedStates:[Int:MultipleSelectedState] ) -> [Int:Bool] {
        var competitionIsOpenStates:[Int:Bool] = [:]
        for (key, value) in competitionSelectedStates{
            competitionIsOpenStates[key] = value != .not
        }
        return competitionIsOpenStates
    }

    func selectCompetition( competitionRef:Int, selected:Bool, competitionDictionaryVOs:[Int: CompetitionRowVO] ) {
        let competitionRowVO: CompetitionRowVO = competitionDictionaryVOs[ competitionRef ]!
        if !competitionRowVO.isListingParent {
            for childVO: CompetitionRowVO in competitionRowVO.childCompetitionVOs {
                selectCompetition( competitionRef:childVO.competitionRef, selected:selected, competitionDictionaryVOs: competitionDictionaryVOs)
            }
        } else {
            for childVO: CompetitionListingRowVO in competitionRowVO.childListingVOs {
                preference.preferenceHelper.modifyListings(childVO.listingRef, selected: selected)
            }
        }
    }

    func selectListing( listingRef:Int, selected:Bool ) {
        preference.preferenceHelper.modifyListings(listingRef, selected: selected)
    }
}
