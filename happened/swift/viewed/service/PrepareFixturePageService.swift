import UIKit

class PrepareFixturePageService {

    private var happen: Happen { return Happen.instance }
    private var localization: Localization { return Localization.instance }
    private var viewed: Viewed { return Viewed.instance }

    func preparePageVO(pageVO: FixturePageVO, feedEntryVOs:[FeedEntryVO]?=nil, fixtureVOs:[Int:FixtureVO]?=nil, arrayFixtureVOs:[FixtureVO]?=nil, entriesPerPage:Int, columnTypes:[FixtureColumnType], focusOnTeams:[Int:NSNull], singleTeamOnly:Bool, navigationObserverID:String ) {
        if (feedEntryVOs == nil || fixtureVOs == nil ) && arrayFixtureVOs == nil {
            return
        }

        var prevRowVO: FixtureRowVO? = nil
        var prevFixtureVO:FixtureVO? = nil
        var pageEntryIndex:Int = -1;
        for i in pageVO.firstEntryConstantIndex..<pageVO.firstEntryConstantIndex + entriesPerPage {
            pageEntryIndex += 1
            var fixtureVO:FixtureVO? = nil
            if feedEntryVOs != nil && fixtureVOs != nil {
                let entryVO:FeedEntryVO? = i < feedEntryVOs!.count ? feedEntryVOs![i] : nil
                if entryVO == nil || !entryVO!.requested || entryVO!.invalid || entryVO!.ref == nil {
                    continue
                }
                fixtureVO = fixtureVOs![ entryVO!.ref! ]
            } else {
                if i >= 0 && i <= arrayFixtureVOs!.count - 1 {
                    fixtureVO = arrayFixtureVOs![ i ]
                }
            }
            if fixtureVO == nil {
                continue
            }
            var rowVO: FixtureRowVO? = pageVO.rowVOs.count > pageEntryIndex ? pageVO.rowVOs[pageEntryIndex] : nil
            if rowVO == nil {
                rowVO = FixtureRowVO()
                pageVO.rowVOs.append(rowVO!)
            }
            if !rowVO!.prepared {
                prepareGameEntryVO(rowVO: rowVO!, fixtureVO: fixtureVO!, columnTypes: columnTypes, focusOnTeams: focusOnTeams, singleTeamOnly: singleTeamOnly, navigationObserverID: navigationObserverID, prevFixtureFeedVO: prevRowVO, prevFixtureVO: prevFixtureVO)
            }
            prevRowVO = rowVO
            prevFixtureVO = fixtureVO
        }
        pageVO.columnVOs = prepareColumns(columnTypes: columnTypes)
        pageVO.prepared = true
    }

    func prepareGameEntryVO(rowVO: FixtureRowVO, fixtureVO:FixtureVO, columnTypes:[FixtureColumnType], focusOnTeams:[Int:NSNull], singleTeamOnly:Bool, navigationObserverID:String, prevFixtureFeedVO: FixtureRowVO?=nil, prevFixtureVO:FixtureVO?=nil ) {

        var newDay:Bool = false
        rowVO.columnTextVOs = [:]
        if prevFixtureVO != nil && prevFixtureVO!.playDate == fixtureVO.playDate {
            rowVO.columnTextVOs[.dateTime] = prevFixtureFeedVO!.columnTextVOs[.dateTime]
        } else if fixtureVO.playDate == nil {
            rowVO.columnTextVOs[.dateTime] = [TextVO( "unscheduled".localized )]
            if prevFixtureVO != nil && prevFixtureVO!.playDate != nil {
                newDay = true
            }
        } else {
            let dateFormatter:DateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd/MM/yy"
            let timeFormatter:DateFormatter = DateFormatter()
            timeFormatter.dateFormat = "hh:mm"+viewed.viewConfig.nonBreakCode+"a"
            timeFormatter.amSymbol = "am".localized
            timeFormatter.pmSymbol = "pm".localized

            let dateString:String = dateFormatter.string(from: fixtureVO.playDate!)
            let timeString:String = timeFormatter.string(from: fixtureVO.playDate!)

            let dateVO:TextVO = TextVO(dateString + " ", attributes: [.pointSizeStyle:UIFont.TextStyle.title2])
            let timeVO:TextVO = TextVO(timeString, attributes: [.pointSizeStyle:UIFont.TextStyle.title3])
            rowVO.columnTextVOs[.dateTime] = [dateVO, timeVO]

            if prevFixtureVO == nil || prevFixtureVO!.playDate == nil {
                newDay = true
            }
            if !newDay {
                let prevDateString:String = dateFormatter.string(from: prevFixtureVO!.playDate!)
                if prevDateString != dateString {
                    newDay = true
                }
            }
        }

        if newDay {
            if !singleTeamOnly {
                rowVO.attributes[.border] = [
                        AppViewAttribute.sides:
                        [AppViewAttribute.top:[
                                AppViewAttribute.style:[
                                        AppViewAttribute.dashed:NSNull()
                                ],
                                AppViewAttribute.backgroundColor:viewed.viewConfig.colorGrey3
                        ]
                        ]
                ]
            }
        }

        var requireCompetitionListing:Bool = false
        for columnType: FixtureColumnType in columnTypes {
            if columnType == .competition {
                requireCompetitionListing = true
                break
            }
        }
        if requireCompetitionListing {
            if prevFixtureVO != nil && prevFixtureVO!.seasonRef == fixtureVO.seasonRef && prevFixtureVO!.listingRef == fixtureVO.listingRef {
                rowVO.columnTextVOs[.competition] = prevFixtureFeedVO!.columnTextVOs[.competition]
            } else {
                let textVOs:[TextVO] = viewed.viewHelper.createPrimaryCompetitionTextVOs(seasonRef: fixtureVO.seasonRef, listingRef: fixtureVO.listingRef)
                var competitionTextVO:TextVO? = nil
                var listingTextVO:TextVO? = nil
                for textVO:TextVO in textVOs {
                    if textVO.replaceID == "competition1" {
                        competitionTextVO = textVO
                    }
                    if textVO.replaceID == "competition2" {
                        listingTextVO = textVO
                    }
                }
                competitionTextVO!.text += " " + viewed.viewConfig.hyphen + " "
                listingTextVO!.text = listingTextVO!.text.splitJoin(" ", viewed.viewConfig.nonBreakCode)
                listingTextVO!.attributes[.observerID] = navigationObserverID
                rowVO.columnTextVOs[.competition] = [competitionTextVO!, listingTextVO!]
            }
        }

        let homeName:String = localization.localizationHelper.getTeamName(fixtureVO.homeRef, seasonRef: fixtureVO.seasonRef)
        let awayName:String = localization.localizationHelper.getTeamName(fixtureVO.awayRef, seasonRef: fixtureVO.seasonRef)

        let homeTeamListingVO:TeamListingVO = happen.happenedModel.getTeamListingVO(fixtureVO.seasonRef, fixtureVO.listingRef, fixtureVO.homeRef)!
        let awayTeamListingVO:TeamListingVO = happen.happenedModel.getTeamListingVO(fixtureVO.seasonRef, fixtureVO.listingRef, fixtureVO.awayRef)!
        let homeTextVO:TextVO = TextVO(homeName, attributes: [.observerID:navigationObserverID, .requestType: AppConstant.SHOW_TEAM_REQUEST, .action: homeTeamListingVO.ref])
        let awayTextVO:TextVO = TextVO(awayName, attributes: [.observerID:navigationObserverID, .requestType: AppConstant.SHOW_TEAM_REQUEST, .action: awayTeamListingVO.ref])

        if focusOnTeams[ fixtureVO.homeRef ] != nil {
            homeTextVO.attributes[.bold] = NSNull()
        }
        if focusOnTeams[ fixtureVO.awayRef ] != nil {
            awayTextVO.attributes[.bold] = NSNull()
        }

        rowVO.columnTextVOs[.homeTeam] = [homeTextVO]
        rowVO.columnTextVOs[.awayTeam] = [awayTextVO]

        var scoreTextVO:TextVO? = nil
        if fixtureVO.resultVO.played {
            scoreTextVO = TextVO( String(fixtureVO.resultVO.homeGoals) + viewed.viewConfig.nonBreakCode + viewed.viewConfig.hyphen + viewed.viewConfig.nonBreakCode + String(fixtureVO.resultVO.awayGoals) )
        } else {
            scoreTextVO = TextVO( "v".localized )
        }
        rowVO.columnTextVOs[.score] = [scoreTextVO!]
        for typeTextVOs:[TextVO] in rowVO.columnTextVOs.values {
            for textVO:TextVO in typeTextVOs {
                if textVO.attributes[.pointSize] == nil {
                    textVO.attributes[.pointSizeStyle] = UIFont.TextStyle.title2
                }
            }
        }

        rowVO.prepared = true
    }

    func prepareColumns( columnTypes:[FixtureColumnType] ) -> [FixtureColumnVO] {
        var columnVOs:[FixtureColumnVO] = []
        for columnType:FixtureColumnType in columnTypes {
            let columnVO:FixtureColumnVO = FixtureColumnVO(columnType: columnType)
            columnVOs.append(columnVO)
        }
        return columnVOs

    }
}
