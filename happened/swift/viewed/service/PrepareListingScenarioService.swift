import Foundation

class PrepareListingScenarioService {

    private var app:App { return App.instance }
    private var balancing: Balancing { return Balancing.instance }
    private var happen: Happen { return Happen.instance }
    private var viewed: Viewed { return Viewed.instance }

    func prepareVO( listingScenarioVO:ListingScenarioVO, navigationObserverID:String ) -> ScenarioViewVO {

        let listingSeasonVO:ListingSeasonVO = happen.happenedModel.getListingSeasonVO(listingScenarioVO.seasonRef, listingScenarioVO.listingRef)!
        let isHistorical:Bool = balancing.balancingHelper.isHistoricalSeason(listingScenarioVO.seasonRef)
        let currentCalculatorVO:CalculatorVO = listingSeasonVO.calculatorVO
        let finalScenarioCalculatorVO:CalculatorVO = listingScenarioVO.finalCalculatorVO
        var splCalculatorVO:CalculatorVO? = listingScenarioVO.splCalculatorVO
        let nextUnplayedDate:Date? = viewed.viewHelper.nextUnplayedFixtureDate( currentCalculatorVO.fixtureVOs )
        let lastPlayedDate:Date = nextUnplayedDate != nil ? Date(timeIntervalSince1970: nextUnplayedDate!.timeIntervalSince1970 - 1) : app.appModel.dayLast
        if !finalScenarioCalculatorVO.calculated {
            happen.applyPerformanceCommand( calculatorVO: finalScenarioCalculatorVO ).command()
        }

        var includeToSplit:Bool = false
        var preSplitFixtureRefs:[Int:NSNull]? = nil
        let hasUpcomingSplit:Bool = finalScenarioCalculatorVO.splSplitVO.isSplSplit && !currentCalculatorVO.splSplitVO.splitOccurred
        let toRelatedSplitID:String = hasUpcomingSplit ? "to_split" : "until_split"
        if finalScenarioCalculatorVO.splSplitVO.isSplSplit {
            if splCalculatorVO == nil {
                splCalculatorVO = happen.happenedHelper.cloneCalculatorVO( finalScenarioCalculatorVO )
                happen.applyPerformanceCommand( calculatorVO: splCalculatorVO! ).command( limitAtSplit: true )

                listingScenarioVO.splCalculatorVO = splCalculatorVO
            }
            includeToSplit = true
            preSplitFixtureRefs = finalScenarioCalculatorVO.splSplitVO.preSplitFixtureRefs
        }

        let scenarioVO:ScenarioViewVO = ScenarioViewVO();

        var currentDoNotIncludeRefs:[Int:NSNull]? = nil
        var finalDoNotIncludeRefs:[Int:NSNull]? = nil

        if !isHistorical {
            if includeToSplit && !hasUpcomingSplit {
                let preCurrentSplitStateVO:ScenarioStateViewVO = ScenarioStateViewVO(
                        calculatorVO: splCalculatorVO!,
                        teamRef: listingScenarioVO.teamRef,
                        fromDate: nil,
                        toDate: lastPlayedDate,
                        title: toRelatedSplitID.localized,
                        descendingDate: true,
                        onlyIncludeRefs: preSplitFixtureRefs,
                        doNotIncludeRefs: nil )

                viewed.prepareScenarioStateService().prepareVO(stateVO: preCurrentSplitStateVO, navigationObserverID:navigationObserverID)

                scenarioVO.addState(preCurrentSplitStateVO)

                currentDoNotIncludeRefs = preSplitFixtureRefs
            }

            let currentStateVO:ScenarioStateViewVO = ScenarioStateViewVO(
                    calculatorVO: currentCalculatorVO,
                    teamRef: listingScenarioVO.teamRef,
                    fromDate: nil,
                    toDate: lastPlayedDate,
                    title: "current".localized,
                    descendingDate: true,
                    onlyIncludeRefs: nil,
                    doNotIncludeRefs: currentDoNotIncludeRefs )

            viewed.prepareScenarioStateService().prepareVO(stateVO: currentStateVO, navigationObserverID:navigationObserverID)

            scenarioVO.addState(currentStateVO)
        }
        if( includeToSplit && (isHistorical || hasUpcomingSplit) ) {

            let postCurrentSplitStateVO:ScenarioStateViewVO = ScenarioStateViewVO(
                    calculatorVO: splCalculatorVO!,
                    teamRef: listingScenarioVO.teamRef,
                    fromDate: nextUnplayedDate,
                    toDate: nil,
                    title: toRelatedSplitID.localized,
                    descendingDate: false,
                    onlyIncludeRefs: preSplitFixtureRefs,
                    doNotIncludeRefs: nil )

            viewed.prepareScenarioStateService().prepareVO(stateVO: postCurrentSplitStateVO, navigationObserverID:navigationObserverID)
            scenarioVO.addState(postCurrentSplitStateVO)

            finalDoNotIncludeRefs = preSplitFixtureRefs
        }

        let finalStateVO:ScenarioStateViewVO = ScenarioStateViewVO(
                calculatorVO: finalScenarioCalculatorVO,
                teamRef: listingScenarioVO.teamRef,
                fromDate: nextUnplayedDate,
                toDate: nil,
                title: "end_of_season".localized,
                descendingDate: false,
                onlyIncludeRefs: nil,
                doNotIncludeRefs: finalDoNotIncludeRefs )

        viewed.prepareScenarioStateService().prepareVO(stateVO: finalStateVO, navigationObserverID:navigationObserverID)
        scenarioVO.addState(finalStateVO, isSelectedState: true)

        return scenarioVO
    }
}
