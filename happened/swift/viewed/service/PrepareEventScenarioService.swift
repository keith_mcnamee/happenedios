import Foundation

class PrepareEventScenarioService {

    private var app:App { return App.instance }
    private var balancing: Balancing { return Balancing.instance }
    private var happen: Happen { return Happen.instance }
    private var viewed: Viewed { return Viewed.instance }

    func prepareVO( eventScenarioVO:EventScenarioVO, navigationObserverID:String ) -> ScenarioViewVO {


        let listingSeasonVO:ListingSeasonVO = happen.happenedModel.getListingSeasonVO(eventScenarioVO.seasonRef, eventScenarioVO.listingRef)!
        let isHistorical:Bool = balancing.balancingHelper.isHistoricalSeason(eventScenarioVO.seasonRef)
        let currentCalculatorVO:CalculatorVO = listingSeasonVO.calculatorVO
        var occurredCalculatorVO:CalculatorVO? = eventScenarioVO.occurredCalculatorVO
        let finalScenarioCalculatorVO:CalculatorVO = eventScenarioVO.finalCalculatorVO
        var splCalculatorVO:CalculatorVO? = eventScenarioVO.splCalculatorVO
        let nextUnplayedDate:Date? = viewed.viewHelper.nextUnplayedFixtureDate( currentCalculatorVO.fixtureVOs )
        let lastPlayedDate:Date = nextUnplayedDate != nil ? Date(timeIntervalSince1970: nextUnplayedDate!.timeIntervalSince1970 - 1) : app.appModel.dayLast
        let nextAfterOccurredDate:Date = !eventScenarioVO.invalid ? Date(timeIntervalSince1970: eventScenarioVO.occurredDate!.timeIntervalSince1970 + 1) : app.appModel.dayLast
        let isSpecificSplit:Bool = eventScenarioVO.eventRef == BalancingConfig.EventRefs.split.rawValue
        if !finalScenarioCalculatorVO.calculated {
            happen.applyPerformanceCommand( calculatorVO: finalScenarioCalculatorVO ).command( limitAtSplit: isSpecificSplit)
        }

        if occurredCalculatorVO == nil {
            occurredCalculatorVO = happen.happenedHelper.cloneCalculatorVO( finalScenarioCalculatorVO )
            occurredCalculatorVO!.date = eventScenarioVO.occurredDate
            happen.applyPerformanceCommand( calculatorVO: occurredCalculatorVO! ).command()

            eventScenarioVO.occurredCalculatorVO = occurredCalculatorVO
        }
        let scenarioVO:ScenarioViewVO = ScenarioViewVO()
        let hasUpcomingSplit:Bool = finalScenarioCalculatorVO.splSplitVO.isSplSplit && !currentCalculatorVO.splSplitVO.splitOccurred
        let includeActualFinalSplit:Bool = isSpecificSplit && currentCalculatorVO.splSplitVO.splitOccurred
        let includeRelatedSplit:Bool = finalScenarioCalculatorVO.splSplitVO.isSplSplit && !isSpecificSplit

        var preSplitFixtureRefs:[Int:NSNull]? = nil
        if finalScenarioCalculatorVO.splSplitVO.isSplSplit {
            if splCalculatorVO == nil {
                if !isSpecificSplit {
                    splCalculatorVO = happen.happenedHelper.cloneCalculatorVO( finalScenarioCalculatorVO )
                    happen.applyPerformanceCommand( calculatorVO: splCalculatorVO! ).command( limitAtSplit: true )
                    eventScenarioVO.splCalculatorVO = splCalculatorVO
                } else {
                    if currentCalculatorVO.splSplitVO.splitOccurred {
                        splCalculatorVO = listingSeasonVO.splitCalculatorVO
                        if splCalculatorVO == nil {
                            splCalculatorVO = happen.happenedHelper.cloneCalculatorVO( currentCalculatorVO )
                            happen.applyPerformanceCommand( calculatorVO: splCalculatorVO! ).command( limitAtSplit: true )

                            listingSeasonVO.splitCalculatorVO = splCalculatorVO
                        }
                    }
                }
            }
            preSplitFixtureRefs = finalScenarioCalculatorVO.splSplitVO.preSplitFixtureRefs
        }

        let dateFormatter:DateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yy hh:mm"+viewed.viewConfig.nonBreakCode+"a"
        let occurredDateString:String = !eventScenarioVO.invalid ? dateFormatter.string(from: eventScenarioVO.occurredDate!) : ""

        let currentTableID:String = eventScenarioVO.upcoming ? "current" : "actual_table"
        let toEndOfSeasonID:String = isSpecificSplit ? eventScenarioVO.upcoming ? "to_split" : "until_split" : eventScenarioVO.upcoming ? "to_end_of_season" : "until_end_of_season"
        let toRelatedSplitID:String = hasUpcomingSplit ?  "to_split" : "until_split"
        let actualSplitID:String = "actual_split"

        let defaultToEndOfSeasonTab:Bool = !isHistorical && !eventScenarioVO.upcoming

        if !eventScenarioVO.upcoming {

            var occurredDoNotIncludeRefs:[Int:NSNull]? = nil
            var finalDoNotIncludeRefs:[Int:NSNull]? = nil
            var finalOnlyIncludeRefs:[Int:NSNull]? = nil

            if includeRelatedSplit && occurredCalculatorVO!.splSplitVO.splitOccurred {

                let preOccurredSplitStateVO:ScenarioStateViewVO = ScenarioStateViewVO(
                        calculatorVO: splCalculatorVO!,
                        teamRef: eventScenarioVO.teamRef,
                        fromDate: nil,
                        toDate: eventScenarioVO.occurredDate,
                        title: toRelatedSplitID.localized,
                        descendingDate: true,
                        onlyIncludeRefs: preSplitFixtureRefs,
                        doNotIncludeRefs: nil )

                viewed.prepareScenarioStateService().prepareVO(stateVO: preOccurredSplitStateVO, navigationObserverID:navigationObserverID)

                scenarioVO.addState(preOccurredSplitStateVO)

                occurredDoNotIncludeRefs = preSplitFixtureRefs
            }

            let occurredStateVO:ScenarioStateViewVO = ScenarioStateViewVO(
                    calculatorVO: occurredCalculatorVO!,
                    teamRef: eventScenarioVO.teamRef,
                    fromDate: nil,
                    toDate: eventScenarioVO.occurredDate,
                    title: occurredDateString,
                    descendingDate: true,
                    onlyIncludeRefs: nil,
                    doNotIncludeRefs: occurredDoNotIncludeRefs )

            viewed.prepareScenarioStateService().prepareVO(stateVO: occurredStateVO, navigationObserverID:navigationObserverID)

            scenarioVO.addState(occurredStateVO, isSelectedState: !defaultToEndOfSeasonTab)

            if includeRelatedSplit && !occurredCalculatorVO!.splSplitVO.splitOccurred {

                let postOccurredSplitStateVO:ScenarioStateViewVO = ScenarioStateViewVO(
                        calculatorVO: splCalculatorVO!,
                        teamRef: eventScenarioVO.teamRef,
                        fromDate: nextAfterOccurredDate,
                        toDate: nil,
                        title: toRelatedSplitID.localized,
                        descendingDate: false,
                        onlyIncludeRefs: preSplitFixtureRefs,
                        doNotIncludeRefs: nil )

                viewed.prepareScenarioStateService().prepareVO(stateVO: postOccurredSplitStateVO, navigationObserverID:navigationObserverID)
                scenarioVO.addState(postOccurredSplitStateVO)

                finalDoNotIncludeRefs = preSplitFixtureRefs

            } else if isSpecificSplit {
                finalOnlyIncludeRefs = preSplitFixtureRefs
            }

            let finalStateVO:ScenarioStateViewVO = ScenarioStateViewVO(
                    calculatorVO: finalScenarioCalculatorVO,
                    teamRef: eventScenarioVO.teamRef,
                    fromDate: nextAfterOccurredDate,
                    toDate: nil,
                    title: toEndOfSeasonID.localized,
                    descendingDate: false,
                    onlyIncludeRefs: finalOnlyIncludeRefs,
                    doNotIncludeRefs: finalDoNotIncludeRefs )

            viewed.prepareScenarioStateService().prepareVO(stateVO: finalStateVO, navigationObserverID:navigationObserverID)
            scenarioVO.addState(finalStateVO, isSelectedState: defaultToEndOfSeasonTab)

            if includeActualFinalSplit {

                let finalSplitStateVO:ScenarioStateViewVO = ScenarioStateViewVO(
                        calculatorVO: splCalculatorVO!,
                        teamRef: eventScenarioVO.teamRef,
                        fromDate: nextAfterOccurredDate,
                        toDate: nil,
                        title: actualSplitID.localized,
                        descendingDate: false,
                        onlyIncludeRefs: preSplitFixtureRefs,
                        doNotIncludeRefs: nil )

                viewed.prepareScenarioStateService().prepareVO(stateVO: finalSplitStateVO, navigationObserverID:navigationObserverID)
                scenarioVO.addState(finalSplitStateVO)
            }

            let currentStateVO:ScenarioStateViewVO = ScenarioStateViewVO(
                    calculatorVO: currentCalculatorVO,
                    teamRef: eventScenarioVO.teamRef,
                    fromDate: nil,
                    toDate: nil,
                    title: currentTableID.localized,
                    descendingDate: false,
                    onlyIncludeRefs: nil,
                    doNotIncludeRefs: nil )

            viewed.prepareScenarioStateService().prepareVO(stateVO: currentStateVO, navigationObserverID:navigationObserverID)

            scenarioVO.addState(currentStateVO)

        } else {

            var occurredDoNotIncludeRefs:[Int:NSNull]? = nil
            var occurredOnlyIncludeRefs:[Int:NSNull]? = nil
            var currentDoNotIncludeRefs:[Int:NSNull]? = nil
            var finalDoNotIncludeRefs:[Int:NSNull]? = nil
            var finalOnlyIncludeRefs:[Int:NSNull]? = nil

            if includeRelatedSplit && currentCalculatorVO.splSplitVO.splitOccurred {

                let preCurrentSplitStateVO:ScenarioStateViewVO = ScenarioStateViewVO(
                        calculatorVO: splCalculatorVO!,
                        teamRef: eventScenarioVO.teamRef,
                        fromDate: nil,
                        toDate: lastPlayedDate,
                        title: toRelatedSplitID.localized,
                        descendingDate: true,
                        onlyIncludeRefs: preSplitFixtureRefs,
                        doNotIncludeRefs: nil )

                viewed.prepareScenarioStateService().prepareVO(stateVO: preCurrentSplitStateVO, navigationObserverID:navigationObserverID)

                scenarioVO.addState(preCurrentSplitStateVO)

                currentDoNotIncludeRefs = preSplitFixtureRefs
            }


            let currentStateVO:ScenarioStateViewVO = ScenarioStateViewVO(
                    calculatorVO: currentCalculatorVO,
                    teamRef: eventScenarioVO.teamRef,
                    fromDate: nil,
                    toDate: lastPlayedDate,
                    title: currentTableID.localized,
                    descendingDate: true,
                    onlyIncludeRefs: nil,
                    doNotIncludeRefs: currentDoNotIncludeRefs )

            viewed.prepareScenarioStateService().prepareVO(stateVO: currentStateVO, navigationObserverID:navigationObserverID)

            scenarioVO.addState(currentStateVO )

            if includeRelatedSplit && !currentCalculatorVO.splSplitVO.splitOccurred && occurredCalculatorVO!.splSplitVO.splitOccurred {

                let postCurrentSplitStateVO:ScenarioStateViewVO = ScenarioStateViewVO(
                        calculatorVO: splCalculatorVO!,
                        teamRef: eventScenarioVO.teamRef,
                        fromDate: nextUnplayedDate,
                        toDate: eventScenarioVO.occurredDate,
                        title: toRelatedSplitID.localized,
                        descendingDate: false,
                        onlyIncludeRefs: preSplitFixtureRefs,
                        doNotIncludeRefs: nil )

                viewed.prepareScenarioStateService().prepareVO(stateVO: postCurrentSplitStateVO, navigationObserverID:navigationObserverID)
                scenarioVO.addState(postCurrentSplitStateVO)

                occurredDoNotIncludeRefs = preSplitFixtureRefs
            } else if isSpecificSplit {
                occurredOnlyIncludeRefs = preSplitFixtureRefs
            }

            let occurredStateVO:ScenarioStateViewVO = ScenarioStateViewVO(
                    calculatorVO: occurredCalculatorVO!,
                    teamRef: eventScenarioVO.teamRef,
                    fromDate: nextUnplayedDate,
                    toDate: eventScenarioVO.occurredDate,
                    title: occurredDateString,
                    descendingDate: false,
                    onlyIncludeRefs: occurredOnlyIncludeRefs,
                    doNotIncludeRefs: occurredDoNotIncludeRefs )

            viewed.prepareScenarioStateService().prepareVO(stateVO: occurredStateVO, navigationObserverID:navigationObserverID)

            scenarioVO.addState(occurredStateVO, isSelectedState: !defaultToEndOfSeasonTab)

            if includeRelatedSplit && !currentCalculatorVO.splSplitVO.splitOccurred && !occurredCalculatorVO!.splSplitVO.splitOccurred {

                let postOccurredSplitStateVO:ScenarioStateViewVO = ScenarioStateViewVO(
                        calculatorVO: splCalculatorVO!,
                        teamRef: eventScenarioVO.teamRef,
                        fromDate: nextAfterOccurredDate,
                        toDate: nil,
                        title: toRelatedSplitID.localized,
                        descendingDate: false,
                        onlyIncludeRefs: preSplitFixtureRefs,
                        doNotIncludeRefs: nil )

                viewed.prepareScenarioStateService().prepareVO(stateVO: postOccurredSplitStateVO, navigationObserverID:navigationObserverID)
                scenarioVO.addState(postOccurredSplitStateVO)

                finalDoNotIncludeRefs = preSplitFixtureRefs
            } else if isSpecificSplit {
                finalOnlyIncludeRefs = preSplitFixtureRefs
            }

            let finalStateVO:ScenarioStateViewVO = ScenarioStateViewVO(
                    calculatorVO: finalScenarioCalculatorVO,
                    teamRef: eventScenarioVO.teamRef,
                    fromDate: nextAfterOccurredDate,
                    toDate: nil,
                    title: toEndOfSeasonID.localized,
                    descendingDate: false,
                    onlyIncludeRefs: finalOnlyIncludeRefs,
                    doNotIncludeRefs: finalDoNotIncludeRefs )

            viewed.prepareScenarioStateService().prepareVO(stateVO: finalStateVO, navigationObserverID:navigationObserverID)
            scenarioVO.addState(finalStateVO, isSelectedState: defaultToEndOfSeasonTab)
        }

        return scenarioVO
    }
}
