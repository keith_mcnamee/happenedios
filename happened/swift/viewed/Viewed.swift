import Foundation

class Viewed {

    static let instance: Viewed = Viewed()

    let viewConfig: ViewConfig = ViewConfig()

    let feedViewHelper: FeedViewHelper = FeedViewHelper()

    let viewHelper: ViewHelper = ViewHelper()

    let uiHelper: UIHelper = UIHelper()

    let eventFeedViewModel: EventFeedViewModel = EventFeedViewModel()

    let fixtureFeedViewModel: FixtureFeedViewModel = FixtureFeedViewModel()

    let viewModel: ViewModel = ViewModel()


    func launchRequestedPageViewCommand() -> LaunchRequestedPageViewCommand {
        return LaunchRequestedPageViewCommand()
    }

    func requirePushAuthorizationViewCommand() -> RequirePushAuthorizationViewCommand {
        return RequirePushAuthorizationViewCommand()
    }

    func resetViewCommand() -> ResetViewCommand {
        return ResetViewCommand()
    }

    func competitionListService() -> CompetitionListService {
        return CompetitionListService()
    }

    func prepareEventPageService() -> PrepareEventPageService {
        return PrepareEventPageService()
    }

    func prepareFixturePageService() -> PrepareFixturePageService {
        return PrepareFixturePageService()
    }

    func prepareEventScenarioService() -> PrepareEventScenarioService {
        return PrepareEventScenarioService()
    }

    func prepareListingScenarioService() -> PrepareListingScenarioService {
        return PrepareListingScenarioService()
    }

    func prepareScenarioStateService() -> PrepareScenarioStateService {
        return PrepareScenarioStateService()
    }

    func prepareStandingService() -> PrepareStandingService {
        return PrepareStandingService()
    }
}
