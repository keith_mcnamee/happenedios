import UIKit

class ViewController: UIViewController {

    private var app:App { return App.instance }

    var observerID:String?
    var tabIdentifier: TabIdentifier?

    let dataChangedNotification:String = NSUUID().uuidString

    enum AppearanceTransitionState {
        case isOut,
             transitionIn,
             isIn,
             transitionOut
    }

    private var _activityIndicatorVC: ActivityIndicatorVC?

    var viewWillFirstAppearApplied:Bool = false
    var viewDidFirstAppearApplied:Bool = false

    var overlayVC:ComponentVC? = nil

    var isPage:Bool = false

    var appearanceTransitionState:AppearanceTransitionState = .isOut

    var isLandscape:Bool = UIDevice.current.orientation.isLandscape

    func createAppView() -> UIView {
        return AppView()
    }

    var appView:AppView {
        return view as! AppView
    }

    /*

    INITIALIZE

    */

    convenience init() {
        self.init(nibName: nil, bundle: nil)
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        view = createAppView()
        initialize()
    }

    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle:nibBundleOrNil)
        view = createAppView()
        initialize()
    }

    func initialize() {
        view.clipsToBounds = true
    }

    /*

    VIEW STATE

    */

    override func viewDidLoad() {
        super.viewDidLoad()
        automaticallyAdjustsScrollViewInsets = false
    }

    func viewWillFirstAppear() {
        viewWillFirstAppearApplied = true
        if isPage {
            addObserver(self, selector: #selector(checkActivityStatus(_:)), name: AppConstant.CHECK_ACTIVITY_STATUS_NOTIFICATION, object: nil)
            self.app.updateActivityViewAppCommand().command(true)
        }
        applyLocalization()
    }

    func viewDidFirstAppear() {
        viewDidFirstAppearApplied = true
        if isPage {
            self.app.updateActivityViewAppCommand().command(false)
        }
    }


    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        applyViewWillAppear()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        applyViewDidAppear()
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        applyViewWillDisappear()
    }

    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        applyViewDidDisappear()
    }


    func applyViewWillAppear() {
        if appearanceTransitionState != .isOut {
            return
        }
        appearanceTransitionState = .transitionIn
        if !viewWillFirstAppearApplied {
            viewWillFirstAppear()
        }
        for childViewController:UIViewController in children {
            if let childVC:ViewController = childViewController as? ViewController {
                childVC.applyViewWillAppear()
            }
        }
    }

    func applyViewDidAppear() {
        if appearanceTransitionState != .transitionIn {
            return
        }
        appearanceTransitionState = .isIn
        if( !viewDidFirstAppearApplied){
            viewDidFirstAppear()
        }
        addObservers()
        checkForActivity()
        resetView()
        addTargets()
        for childViewController:UIViewController in children {
            if let childVC:ViewController = childViewController as? ViewController {
                childVC.applyViewDidAppear()
            }
        }
    }

    func applyViewWillDisappear() {
        if appearanceTransitionState != .isIn {
            return
        }
        appearanceTransitionState = .transitionOut

        removeOverlay(overlayVC)
        removeTargets()
        removeObservers()
        hideChildren()
        for childViewController:UIViewController in children {
            if let childVC:ViewController = childViewController as? ViewController {
                childVC.applyViewWillDisappear()
            }
        }
    }

    func applyViewDidDisappear() {
        if appearanceTransitionState != .transitionOut {
            return
        }
        appearanceTransitionState = .isOut
        for childViewController:UIViewController in children {
            if let childVC:ViewController = childViewController as? ViewController {
                childVC.applyViewDidDisappear()
            }
        }
    }

    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        checkOrientation()
    }

    /*

    MISC HOOKS

    */

    func applyLocalization() {

    }

    func viewReady() {

    }

    func resetView() {

    }

    func addTargets() {

    }

    func removeTargets() {

    }


    /*

    OBSERVER

    */

    func applyObserverID(doUse:Bool=true) {
        if doUse {
            if observerID == nil {
                observerID = NSUUID().uuidString
            }
        } else {
            observerID = nil
        }
    }

    func addObservers() {
        if observerID != nil {
            addObserver(self, selector: #selector(observerIDCalled(notification:)), name: observerID!, object: nil)
        }
        addObserver(self, selector: #selector(overlayHidden(notification:)), name: overlayVC?.closedNotification, object: nil)
        if isPage {
            addObserver(self, selector: #selector(checkActivityStatus(_:)), name: AppConstant.CHECK_ACTIVITY_STATUS_NOTIFICATION, object: nil)
        }
    }

    func removeObservers() {
        NotificationCenter.default.removeObserver(self)
    }

    @objc func observerIDCalled(notification: NSNotification) {
        let requestType:String? = notification.userInfo?[AppViewAttribute.requestType] as? String
        let action:Any? = notification.userInfo?[AppViewAttribute.action]
        let value:Any? = notification.userInfo?[AppViewAttribute.value]
        applyNotificationRequest(requestType: requestType, action: action, value:value)
    }

    func applyNotificationRequest(requestType:String?=nil, action:Any?=nil, value:Any?=nil) {
        if requestType != nil {
            switch requestType! {
            case AppConstant.SHOW_OVERLAY:
                if let componentVC:ComponentVC = action as? ComponentVC {
                    showOverlay(componentVC)
                }
            default:
                break
            }
        }
    }

    func addObserver(_ observer: Any, selector aSelector: Selector, name aName: String?, object anObject: Any?)
    {
        if aName == nil {
            return
        }
        let notification:NSNotification.Name = Notification.Name(aName!)
        NotificationCenter.default.removeObserver(observer, name: notification, object: anObject)
        NotificationCenter.default.addObserver(observer, selector: aSelector, name: notification, object: anObject)
    }


    /*

    ACTIVITY INDICATOR

    */


    func checkForActivity() {
        if isPage {
            if app.appModel.waitForServer || app.appModel.waitForView {
                checkActivityStatus()
            }
        }
    }

    func showViewLoadingActivityIndicator(_ completionHandler: (()->Void)?=nil) {
        self.app.updateActivityViewAppCommand().command(true)
        DispatchQueue.main.async {
            self.app.updateActivityViewAppCommand().command(false)
            completionHandler?()
        }
    }

    func addActivityEndedObserver(selector: Selector?=nil) {
        if selector != nil {
            NotificationCenter.default.addObserver(self, selector: selector!, name: Notification.Name(AppConstant.ACTIVITY_ENDED_NOTIFICATION), object: nil)
        }
    }

    func removeActivityEndedObserver() {
        NotificationCenter.default.removeObserver(self, name: Notification.Name(AppConstant.ACTIVITY_ENDED_NOTIFICATION), object: nil)
    }

    @objc func checkActivityStatus(_ sender: Any?=nil) {
        if app.appModel.waitForServer || app.appModel.waitForView {
            if overlayVC != activityIndicatorVC {
                self.showOverlay(self.activityIndicatorVC)
            }
        } else {
            DispatchQueue.main.async {
                if !self.app.appModel.waitForServer && !self.app.appModel.waitForView {
                    if self.overlayVC == self.activityIndicatorVC {
                        self.removeOverlay(self._activityIndicatorVC)
                    }
                }
            }
        }
    }

    var activityIndicatorVC: ActivityIndicatorVC {
        if _activityIndicatorVC != nil {
            return _activityIndicatorVC!
        }
        _activityIndicatorVC = ActivityIndicatorVC()
        return _activityIndicatorVC!
    }


    /*

    OVERLAY

    */

    var overlayView:AppView? {
        return nil
    }

    func showOverlay( _ componentVC:ComponentVC ) {
        if componentVC == overlayVC {
            if componentVC.appearanceTransitionState == .isIn || componentVC.appearanceTransitionState == .transitionIn {
                return
            }
        }
        removeOverlay(overlayVC)
        overlayVC = componentVC
        addObserver(self, selector: #selector(overlayHidden(notification:)), name: overlayVC!.closedNotification, object: nil)
        let useOverlayView:AppView = enableOverlay()
        overlayVC!.showMe(parentController: self, parentView: useOverlayView)
    }

    func removeOverlay(_ componentVC:ComponentVC? = nil) {
        if componentVC == nil {
            return
        }
        componentVC!.hideMe()
        applyOverlayHidden( componentVC! )
    }

    @objc func overlayHidden(notification: NSNotification) {
        if let componentVC:ComponentVC = notification.userInfo?[AppViewAttribute.value] as? ComponentVC {
            applyOverlayHidden(componentVC)
        }
    }

    func applyOverlayHidden(_ componentVC:ComponentVC) {
        NotificationCenter.default.removeObserver(self, name: Notification.Name(componentVC.closedNotification), object: nil)
        if overlayVC == componentVC {
            overlayVC = nil
            disableOverlay()
        }
    }

    func enableOverlay() -> AppView {
        if overlayView == nil {
            return appView
        }
        overlayView!.isHidden = false
        overlayView!.isUserInteractionEnabled = true
        return overlayView!
    }

    func disableOverlay() {
        if overlayView == nil {
            return
        }
        overlayView!.isHidden = true
        overlayView!.isUserInteractionEnabled = false
    }

    /*

    ORIENTATION

    */

    func checkOrientation(){
        let isLandscape:Bool = UIDevice.current.orientation.isLandscape
        if isLandscape != self.isLandscape {
            self.isLandscape = isLandscape
            orientationDidChange()
        }
    }

    func orientationDidChange() {

    }

    /*

    ON REMOVE

    */


    func hideChildren() {
        _activityIndicatorVC?.hideMe()
        overlayVC?.hideMe();
    }

    func destroy() {
        removeTargets()
        removeObservers()
        _activityIndicatorVC?.destroy()
        _activityIndicatorVC = nil
        overlayVC?.hideMe();
        overlayVC = nil
    }

    deinit {
        destroy()
    }

}
