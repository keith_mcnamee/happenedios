import UIKit

class VerticalScrollVC: ViewController {

    static let PREPARE_BOTTOM_PIN_ATTEMPT:String = NSUUID().uuidString
    static let ATTEMPT_BOTTOM_PIN:String = NSUUID().uuidString

    var wasAtBottom:Bool = false

    override func createAppView() -> ScrollContainerView {
        return ScrollContainerView()
    }

    var scrollView:ScrollContainerView {
        return view as! ScrollContainerView
    }

    override func applyNotificationRequest(requestType:String?=nil, action:Any?=nil, value:Any?=nil) {
        if requestType != nil {
            switch requestType! {
            case VerticalScrollVC.PREPARE_BOTTOM_PIN_ATTEMPT:
                prepareBottomPinAttempt()
                break
            case VerticalScrollVC.ATTEMPT_BOTTOM_PIN:
                attemptBottomPin()
                break
            default:
                break
            }
        }
        super.applyNotificationRequest(requestType: requestType, action: action, value: value)
    }

    func prepareBottomPinAttempt(){
        wasAtBottom = scrollView.scrollView.isScrolledToBottom
    }

    func attemptBottomPin(){
        if !wasAtBottom {
            return
        }
        scrollView.scrollView.setContentOffset(CGPoint(x: scrollView.scrollView.horizontalOffsetForLeading, y: scrollView.scrollView.verticalOffsetForBottom), animated: true)
        self.view.layoutIfNeeded()
    }

    override var overlayView:AppView {
        return scrollView._overlayView
    }

    override func enableOverlay() -> AppView {
        let overlay:AppView = super.enableOverlay()
        scrollView.enableOverlayConstraints(true)
        return overlay
    }

    override func disableOverlay() {
        super.disableOverlay()
        scrollView.enableOverlayConstraints(false)
    }

    override func orientationDidChange() {
        super.orientationDidChange()
        scrollView.updateContentInsetDependantConstraints()
    }

    override func viewDidFirstAppear() {
        super.viewDidFirstAppear()
        scrollView.updateContentInsetDependantConstraints()
    }

}
