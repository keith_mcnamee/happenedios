import UIKit

class ComponentVC: ViewController {

    let closedNotification:String = NSUUID().uuidString

    func showMe(parentController: ViewController, parentView:AppView?=nil ) {

        hideMe()

        let useParentView:AppView = parentView != nil ? parentView! : parentController.appView

        parentController.addChild(self)

        useParentView.addSubview(appView)

        appView.frame = useParentView.bounds
        appView.autoresizingMask = [.flexibleWidth, .flexibleHeight]

        didMove(toParent: self)

        let topConstraint:NSLayoutConstraint = NSLayoutConstraint(item: appView, attribute: .top, relatedBy: .equal, toItem: useParentView, attribute: .top, multiplier: 1, constant: 0)
        let leadingConstraint:NSLayoutConstraint =  NSLayoutConstraint(item: appView, attribute: .leading, relatedBy: .equal, toItem: useParentView, attribute: .leading, multiplier: 1, constant: 0)
        let trailingConstraint:NSLayoutConstraint = NSLayoutConstraint(item: appView, attribute: .trailing, relatedBy: .equal, toItem: useParentView, attribute: .trailing, multiplier: 1, constant: 0)
        let bottomConstraint:NSLayoutConstraint = NSLayoutConstraint(item: appView, attribute: .bottom, relatedBy: .equal, toItem: useParentView, attribute: .bottom, multiplier: 1, constant: 0)

        appView.addAppConstraint(topConstraint, useParentView)
        appView.addAppConstraint(leadingConstraint, useParentView)
        appView.addAppConstraint(trailingConstraint, useParentView)
        appView.addAppConstraint(bottomConstraint, useParentView)

        useParentView.setNeedsLayout()

        if parentController.appearanceTransitionState == .isIn || parentController.appearanceTransitionState == .transitionIn {
            applyViewWillAppear()
            applyViewDidAppear()
        }
    }

    func hideMe(_ sender: Any?=nil) {
        if parent == nil && appView.superview == nil {
            return
        }

        willMove(toParent: nil)
        appView.removeFromSuperview()
        removeFromParent()

        applyViewWillDisappear()
        applyViewDidDisappear()

    }

    override func applyViewDidDisappear() {
        super.applyViewDidDisappear()

        let userInfo:[AnyHashable : Any] = [
                AppViewAttribute.value: self
        ]
        NotificationCenter.default.post(name: Notification.Name(closedNotification), object: nil, userInfo: userInfo)
    }

    override func destroy() {
        hideMe()
        super.destroy()
    }

}
