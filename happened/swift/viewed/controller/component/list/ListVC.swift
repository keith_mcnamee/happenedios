import UIKit
import QuartzCore.CAAnimation

class ListVC: ComponentVC {

    private var app:App { return App.instance }
    private var viewed: Viewed { return Viewed.instance }

    var columnVOs:[ListColumnVO] = []
    var rowVOs:[ListRowVO] = []
    var rows:[ListRowView] = []
    var columnAttributes: [Int: [AppViewAttribute:Any]] = [:]

    var allowHorizontalScrolling:Bool = false
    var minimumRowWidth:CGFloat = 0

    var linesView:AppView? = nil

    var prevPosition:CGPoint = CGPoint()

    var totalRowWidth:CGFloat = 0

    var topRowPadding:CGFloat? = nil
    var bottomRowPadding:CGFloat? = nil
    var minCellSpace:CGFloat? = nil
    var leftRowPadding:CGFloat = 0
    var rightRowPadding:CGFloat = 0

    var alignCenterY:Bool = false

    convenience init( columnVOs:[ListColumnVO] = [], rowVOs:[ListRowVO] = [] ) {
        self.init(nibName: nil, bundle: nil)
        self.columnVOs = columnVOs
        self.rowVOs = rowVOs
    }

    func createRowView() -> ListRowView {
        return ListRowView()
    }

    override func viewDidFirstAppear() {
        super.viewDidFirstAppear()
        initializeList( isFirstAppear:true )
        if columnVOs.count > 0 && rowVOs.count > 0 && rows.count == 0 {
            createView()
        }
    }

    func initializeList( isFirstAppear:Bool = false ) {

    }

    func createView() {
        appView.removeAll()
        applyGeneralView()
        createRows()
        applyAttributes()
        applyRowWidths()
        appView.layoutIfNeeded()

        addLines()
        checkLayerPosition()
    }

    func applyGeneralView() {

    }

    func createRows() {

        let nextView:UIView = appView
        var nextToItem:UIView = appView
        var nextToAttribute:NSLayoutConstraint.Attribute = .top
        let nextToConstant:CGFloat = 0

        var firstItem:UIView? = nil
        rows = []
        for rowVO:ListRowVO in rowVOs {

            let row: ListRowView = createRow(parentView:nextView, toItem: nextToItem, toAttribute: nextToAttribute, toConstant: nextToConstant)
            applyRow(row:row, rowVO: rowVO)

            if firstItem == nil {
                firstItem = row
            }

            nextToItem = row
            nextToAttribute = .bottom
        }

        let lastItem:UIView? = nextToItem != appView ? nextToItem : nil
        if lastItem != nil {
            let bottomConstraint:NSLayoutConstraint = NSLayoutConstraint(item: lastItem!, attribute: .bottom, relatedBy: .lessThanOrEqual, toItem: appView, attribute: .bottom, multiplier: 1, constant: 0)
            (lastItem as? ExtAppView)?.addAppConstraint(bottomConstraint, appView, nil)
        }
    }

    func applyAttributes() {
        var index:Int = -1
        for rowVO: ListRowVO in rowVOs {
            index += 1
            let row:ListRowView = rows[index]
            if let bgColor:UIColor = viewed.uiHelper.backgroundColorAttribute(rowVO.attributes) {
                row.backgroundColor = bgColor
            }
            for columnVO:ListColumnVO in columnVOs {
                var textView:AttributedTextView? = nil
                if rowVO.isMultiColumn {
                    textView = rowVO.multiColumnCell as? AttributedTextView
                } else {
                    textView = rowVO.getUiViewCell(columnVO.id) as? AttributedTextView
                }
                if textView != nil {
                    let alignment:NSTextAlignment? = viewed.uiHelper.textAlignAttribute(columnVO.attributes)
                    if alignment != nil {
                        textView!.textAlignment = alignment!
                    }
                }
                if rowVO.isMultiColumn {
                    break
                }
            }
        }
    }

    func applyRowWidths() {


        var columnMaximumWidths:[Int:CGFloat] = [:]
        var columnMinimumWidths:[Int:CGFloat] = [:]
        var primaryRow:ListRowView? = nil
        for row: ListRowView in rows {
            var haveAllColumns:Bool = true
            for columnVO: ListColumnVO in columnVOs {
                if row.cells[columnVO.id] == nil {
                    haveAllColumns = false
                    break
                }
            }
            if haveAllColumns {
                primaryRow = row
                break
            }
        }
        minimumRowWidth = 0
        for columnVO: ListColumnVO in columnVOs {
            var maximumWidth:CGFloat = 0
            var minimumWidth:CGFloat = 0
            for row: ListRowView in rows {
                if let cell: UIView = row.cells[columnVO.id] {
                    var useMinimumWidth:CGFloat = cell.width
                    var useMaximumWidth:CGFloat = cell.width

                    if let listCellView:ListCellView = cell as? ListCellView {
                        useMinimumWidth = listCellView.minimumWidth
                        useMaximumWidth = listCellView.maximumWidth
                    } else if let attributedTextView:AttributedTextView = cell as? AttributedTextView {
                        useMinimumWidth = attributedTextView.getMinimumWidth()
                        useMaximumWidth = attributedTextView.getMaximumWidth()
                    }
                    if useMaximumWidth > maximumWidth {
                        maximumWidth = useMaximumWidth
                    }
                    if useMinimumWidth > minimumWidth {
                        minimumWidth = useMinimumWidth
                    }
                }
            }
            columnMaximumWidths[columnVO.id] = maximumWidth
            columnMinimumWidths[columnVO.id] = minimumWidth
            minimumRowWidth += minimumWidth
        }
        let usePrimaryRow:ListRowView? = primaryRow != nil ? primaryRow! : rows.count > 0 ? rows[0] : nil
        if usePrimaryRow != nil {
            for (columnType, maximumWidth) in columnMaximumWidths {
                let minimumWidth:CGFloat = columnMinimumWidths[columnType]!
                usePrimaryRow!.applyWidthConstraint(columnType: columnType, maximumWidth: maximumWidth, minimumWidth: minimumWidth)
            }
            for row: ListRowView in rows {
                if row == usePrimaryRow {
                    continue
                }
                for columnVO: ListColumnVO in columnVOs {
                    row.applyMatchWidthConstraint(columnType: columnVO.id, matchCell: usePrimaryRow!.cells[columnVO.id]!)
                }
            }
        }

        appView.layoutIfNeeded()

        for row: ListRowView in rows {
            row.enableSpacer(true )
            row.enableTrailingConstraint(true)
        }

        if allowHorizontalScrolling {
            for row: ListRowView in rows {
                let widthConstraint:NSLayoutConstraint = NSLayoutConstraint(item: row, attribute: .width, relatedBy: .greaterThanOrEqual, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: minimumRowWidth)
                row.addAppConstraint(widthConstraint, row.superview!)
            }
            applyMinimumWidthToParent()
        }
    }

    override func showMe(parentController: ViewController, parentView:AppView?=nil) {
        super.showMe(parentController: parentController, parentView: parentView)
        applyMinimumWidthToParent()
    }


    func applyMinimumWidthToParent() {
        if allowHorizontalScrolling && minimumRowWidth > 0 && appView.superview != nil {
            let parentWidthConstraint:NSLayoutConstraint = NSLayoutConstraint(item: appView.superview!, attribute: .width, relatedBy: .greaterThanOrEqual, toItem: .none, attribute: .notAnAttribute, multiplier: 1, constant: minimumRowWidth)
            appView.superview!.addConstraint(parentWidthConstraint)
        }
    }

    func createRow(parentView:UIView, toItem:UIView, toAttribute:NSLayoutConstraint.Attribute, toConstant:CGFloat ) -> ListRowView {

        let row: ListRowView = createRowView()

        row.topPadding = topRowPadding != nil ? topRowPadding! : row.topPadding
        row.bottomPadding = bottomRowPadding != nil ? bottomRowPadding! : row.bottomPadding
        row.minSpace = minCellSpace != nil ? minCellSpace! : row.minSpace
        row.alignCenterY = alignCenterY

        rows.append(row)
        parentView.addSubview(row)

        let topConstraint:NSLayoutConstraint = NSLayoutConstraint(item: row, attribute: .top, relatedBy: .equal, toItem: toItem, attribute: toAttribute, multiplier: 1, constant: toConstant)
        let leadingConstraint:NSLayoutConstraint =  NSLayoutConstraint(item: row, attribute: .leading, relatedBy: .equal, toItem: parentView, attribute: .leading, multiplier: 1, constant: leftRowPadding)
        let trailingConstraint:NSLayoutConstraint = NSLayoutConstraint(item: row, attribute: .trailing, relatedBy: .equal, toItem: parentView, attribute: .trailing, multiplier: 1, constant: -rightRowPadding)
        row.addAppConstraint(topConstraint, parentView)
        row.addAppConstraint(leadingConstraint, parentView)
        row.addAppConstraint(trailingConstraint, parentView)

        row.setNeedsLayout()

        return row
    }

    func applyRow(row: ListRowView, rowVO:ListRowVO ) {
        row.applyRow(columnVOs:columnVOs, rowVO:rowVO)
    }

    func removeLinesView() {
        if linesView != nil {
            linesView!.removeFromSuperview()
            linesView = nil
        }
    }

    func addLinesView() {
        linesView = AppView()
        linesView!.isUserInteractionEnabled = false
        appView.addSubview(linesView!)
        let topConstraint:NSLayoutConstraint = NSLayoutConstraint(item: linesView!, attribute: .top, relatedBy: .equal, toItem: appView, attribute: .top, multiplier: 1, constant: 0)
        let leadingConstraint:NSLayoutConstraint =  NSLayoutConstraint(item: linesView!, attribute: .leading, relatedBy: .equal, toItem: appView, attribute: .leading, multiplier: 1, constant: 0)
        let trailingConstraint:NSLayoutConstraint = NSLayoutConstraint(item: linesView!, attribute: .trailing, relatedBy: .equal, toItem: appView, attribute: .trailing, multiplier: 1, constant: 0)
        let bottomConstraint:NSLayoutConstraint = NSLayoutConstraint(item: linesView!, attribute: .bottom, relatedBy: .equal, toItem: appView, attribute: .bottom, multiplier: 1, constant: 0)
        linesView!.addAppConstraint(topConstraint, appView)
        linesView!.addAppConstraint(leadingConstraint, appView)
        linesView!.addAppConstraint(trailingConstraint, appView)
        linesView!.addAppConstraint(bottomConstraint, appView)
        appView.layoutIfNeeded()
    }

    var hLineX:CGFloat {
        return 0
    }

    var vLineY:CGFloat {
        return rows[0].y / 2
    }

    var hLineWidth:CGFloat {
        return rows[0].width
    }

    var vLineHeight:CGFloat {
        return rows[rows.count - 1].y + rows[rows.count - 1].height - rows[0].y
    }

    func addLines() {
        removeLinesView()
        if rows.count == 0 {
            return
        }

        if !haveLines {
            return
        }
        addLinesView()

        let firstRow:ListRowView = rows[0]
        let lineWidth:CGFloat = hLineWidth
        let lineHeight:CGFloat = vLineHeight

        var index:Int = -1
        var xPos:CGFloat = linesView!.layer.position.x + hLineX
        let topRowPadding:CGFloat = self.topRowPadding != nil ? self.topRowPadding! : 0
        var yPos:CGFloat = topRowPadding
        for rowVO:ListRowVO in rowVOs {
            index += 1
            let row:ListRowView = rows[index]

            yPos = row.y + topRowPadding
            let borderSides:[AppViewAttribute:Any] = viewed.uiHelper.sidesAttributes(rowVO.attributes[.border])

            if borderSides[.top] != nil {
                addHorizontalLine(xPos: xPos, yPos: yPos, lineWidth: lineWidth, attributes: borderSides[.top] )
            }

            if borderSides[.bottom] != nil {
                addHorizontalLine(xPos: xPos, yPos: yPos + row.height, lineWidth: lineWidth, attributes: borderSides[.bottom] )
            }
        }

        xPos = leftRowPadding
        yPos = linesView!.layer.position.y + vLineY
        for columnVO: ListColumnVO in columnVOs {
            let column: UIView? = firstRow.cells[columnVO.id]
            if columnVO.attributes.count == 0 || column == nil {
                continue
            }

            xPos = column!.x + leftRowPadding
            let borderSides:[AppViewAttribute:Any] = viewed.uiHelper.sidesAttributes(columnVO.attributes[.border])
            if borderSides[.left] != nil {
                addVerticalLine(xPos: xPos, yPos: yPos, lineHeight: lineHeight, attributes: borderSides[.left])
            } else if borderSides[.right] != nil {
                addVerticalLine(xPos: xPos + column!.width, yPos: yPos, lineHeight: lineHeight, attributes: borderSides[.right])
            }
        }
    }

    var haveLines:Bool {
        if rows.count == 0 {
            return false
        }
        for rowVO:ListRowVO in rowVOs {
            let borderSides:[AppViewAttribute:Any] = viewed.uiHelper.sidesAttributes(rowVO.attributes[.border])

            if borderSides[.top] != nil {
                return true
            }

            if borderSides[.bottom] != nil {
                return true
            }
        }
        for columnVO: ListColumnVO in columnVOs {
            let borderSides:[AppViewAttribute:Any] = viewed.uiHelper.sidesAttributes(columnVO.attributes[.border])
            if borderSides[.left] != nil {
                return true
            } else if borderSides[.right] != nil {
                return true
            }
        }
        return false
    }

    func addHorizontalLine(xPos:CGFloat, yPos:CGFloat, lineWidth:CGFloat, attributes:Any?=nil) {

        let lineStyle: AppViewAttribute = viewed.uiHelper.lineStyleAttribute(attributes)
        let pointSize:CGFloat = viewed.uiHelper.pointSizeAttribute(attributes, defaultAttribute:1)!
        let backgroundColor:UIColor = viewed.uiHelper.backgroundColorAttribute(attributes, defaultAttribute:UIColor(red: 0, green: 0, blue: 0, alpha: 1))!

        let shapeLayer:CAShapeLayer = CAShapeLayer()

        shapeLayer.strokeColor = backgroundColor.cgColor;
        shapeLayer.fillColor = nil;

        shapeLayer.bounds = CGRect(x: 0 , y: 0, width: lineWidth, height: 1)
        shapeLayer.position = CGPoint(x:xPos, y:yPos)
        shapeLayer.strokeStart = 0
        shapeLayer.strokeEnd = 0.5
        shapeLayer.lineWidth = pointSize
        if lineStyle == .dashed {
            shapeLayer.lineDashPattern = viewed.viewConfig.lineDashPattern
        }
        shapeLayer.path = UIBezierPath(roundedRect: shapeLayer.bounds, cornerRadius: 0).cgPath

        linesView!.layer.addSublayer(shapeLayer)
    }

    func addVerticalLine(xPos:CGFloat, yPos:CGFloat, lineHeight:CGFloat, attributes:Any?=nil) {

        let lineStyle: AppViewAttribute = viewed.uiHelper.lineStyleAttribute(attributes)
        let pointSize:CGFloat = viewed.uiHelper.pointSizeAttribute(attributes, defaultAttribute:1)!
        let backgroundColor:UIColor = viewed.uiHelper.backgroundColorAttribute(attributes, defaultAttribute:UIColor(red: 0, green: 0, blue: 0, alpha: 1))!

        let shapeLayer:CAShapeLayer = CAShapeLayer()

        shapeLayer.strokeColor = backgroundColor.cgColor;
        shapeLayer.fillColor = nil;

        shapeLayer.bounds = CGRect(x: 0, y: 0 , width: 0, height: lineHeight)
        shapeLayer.position = CGPoint(x:xPos, y:yPos)
        shapeLayer.strokeStart = 0
        shapeLayer.strokeEnd = 0.5
        shapeLayer.lineWidth = pointSize
        if lineStyle == .dashed{
            shapeLayer.lineDashPattern = viewed.viewConfig.lineDashPattern
        }
        shapeLayer.path = UIBezierPath(roundedRect: shapeLayer.bounds, cornerRadius: 0).cgPath

        linesView!.layer.addSublayer(shapeLayer)
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        checkLayerPosition()
        checkSpacer()
    }

    override func applyViewDidAppear() {
        super.applyViewDidAppear()
        checkLayerPosition()
    }

    func checkSpacer() {
        let currentWidth:CGFloat = appView.width
        if currentWidth > totalRowWidth {
            totalRowWidth = currentWidth
            for row: ListRowView in rows {
                row.updatePrimarySpacer( totalRowWidth )
            }
        }
    }

    func checkLayerPosition() {
        if linesView != nil && ( linesView!.layer.position.x != prevPosition.x || linesView!.layer.position.y != prevPosition.y ) {
            appView.bringSubviewToFront(linesView!)
            prevPosition = CGPoint(x: linesView!.layer.position.x, y: linesView!.layer.position.y)
            addLines()
        }
    }

    override func destroy(){
        removeLinesView()
        for row: ListRowView in rows {
            row.destroy()
        }
        rows = []
        columnVOs = []
        rowVOs = []
        super.destroy()
    }
}
