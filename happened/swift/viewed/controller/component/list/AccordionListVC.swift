import UIKit

class AccordionListVC: ListVC {

    static let OPEN_CLOSE_REQUEST:String = NSUUID().uuidString

    var expandDuration:TimeInterval = 0.2
    var viewYPos:CGFloat = 0

    var verticalScrollObserverID:String?
    var defaultIsOpen:Bool = false

    var accordionRows:[Int: AccordionRowView] = [:]

    convenience init( verticalScrollObserverID:String  ) {
        self.init(nibName: nil, bundle: nil)
        self.verticalScrollObserverID = verticalScrollObserverID
    }

    override func createRowView() -> AccordionRowView {
        return AccordionRowView()
    }

    override func createRows() {
        accordionRows = [:]
        super.createRows()
    }

    func createRow( parentView:UIView, toItem:UIView, toAttribute:NSLayoutConstraint.Attribute, toConstant:CGFloat, fromTop:Bool, id:Int, parentID:Int?=nil ) -> AccordionRowView {

        let row: AccordionRowView =  super.createRow(parentView: parentView, toItem: toItem, toAttribute: toAttribute, toConstant: toConstant) as! AccordionRowView
        if !fromTop {
            (parentView as! ExtAppView).removeAppConstraint(.top)
            let bottomConstraint:NSLayoutConstraint = NSLayoutConstraint(item: row, attribute: .bottom, relatedBy: .equal, toItem: toItem, attribute: toAttribute, multiplier: 1, constant: toConstant)
            row.addAppConstraint(bottomConstraint, parentView)
        }
        row.id = id
        accordionRows[id] = row
        if parentID != nil {
            row.parentID = parentID
            if let parentItem: AccordionRowView = accordionRows[parentID!] {
                parentItem.childIDs[id] = NSNull()
            }
        }
        row.applyCurrentHeight()
        row.headerView!.expandBtn.addObserverID(observerID: observerID!, requestType: AccordionListVC.OPEN_CLOSE_REQUEST, action: id)
        forceExpandContract(id, defaultIsOpen)

        return row
    }

    override func applyNotificationRequest(requestType:String?=nil, action:Any?=nil, value:Any?=nil) {
        if requestType != nil {
            switch requestType! {
            case AccordionListVC.OPEN_CLOSE_REQUEST:
                if let id:Int = action as? Int {
                    expandContract(id)
                }
                break
            default:
                break
            }
        }
        super.applyNotificationRequest(requestType: requestType, action: action, value: value)
    }

    func expandContract(_ id:Int ) {
        let row:AccordionRowView? = accordionRows[id]
        if row == nil {
            return
        }
        applyIsOpen(id, !row!.isOpen)
        viewYPos = view.y

        var isLast:Bool = false
        var expandDuration:TimeInterval = self.expandDuration
        func animationComplete( _ value:Bool) {
            if isLast {
                self.attemptBottomPin()
            }
        }
        if row!.isOpen {
            var checkRow:AccordionRowView? = row
            isLast = true
            while checkRow != nil {
                if checkRow!.superview == nil || checkRow!.y + checkRow!.height != checkRow!.superview!.height {
                    isLast = false
                    break
                }
                if checkRow!.parentID == nil {
                    break
                }
                checkRow = accordionRows[checkRow!.parentID!]
            }
            if isLast {
                expandDuration = 0
                prepareBottomPinAttempt()
            }
        }
        UIView.animate(withDuration: expandDuration, animations: {
                self.view.layoutIfNeeded()
                self.view.y = self.viewYPos
            }, completion: animationComplete
        )

    }

    func forceExpandContract(_ id:Int, _ value:Bool) {

        let row:AccordionRowView? = accordionRows[id]
        if row == nil {
            return
        }
        applyIsOpen(id, value)
        view.layoutIfNeeded()
    }

    func applyIsOpen(_ id:Int, _ value:Bool){
        let row:AccordionRowView? = accordionRows[id]
        if row == nil {
            return
        }
        if value == row!.isOpen {
            return
        }
        row!.applyIsOpen( value )
        if !value {
            for childID:Int in row!.childIDs.keys {
                applyIsOpen( childID, false )
            }
        }
        if row!.parentID != nil {
            applyCurrentExpansionHeight(row!.parentID!)
        }
    }

    func prepareBottomPinAttempt() {
        if verticalScrollObserverID == nil {
            return
        }
        let userInfo:[AnyHashable : Any] = [
                AppViewAttribute.requestType: VerticalScrollVC.PREPARE_BOTTOM_PIN_ATTEMPT
        ]
        NotificationCenter.default.post(name: Notification.Name(verticalScrollObserverID!), object: nil, userInfo: userInfo)
    }

    func attemptBottomPin() {
        if verticalScrollObserverID == nil {
            return
        }
        let userInfo:[AnyHashable : Any] = [
                AppViewAttribute.requestType: VerticalScrollVC.ATTEMPT_BOTTOM_PIN
        ]
        NotificationCenter.default.post(name: Notification.Name(verticalScrollObserverID!), object: nil, userInfo: userInfo)
    }

    func applyExpansionHeight(_ id:Int, _ value:CGFloat){
        let row:AccordionRowView? = accordionRows[id]
        if row == nil {
            return
        }
        if value + row!.headerView!.designatedHeight == row!.getAppConstraint(.height)!.constant {
            return
        }
        row!.applyExpansionHeight( value )
        if row!.parentID != nil {
            applyCurrentExpansionHeight(row!.parentID!)
        }
    }

    func applyCurrentExpansionHeight(_ id:Int){
        let row:AccordionRowView? = accordionRows[id]
        if row == nil {
            return
        }
        if row!.fixedExpansionHeight != nil {
            applyExpansionHeight( id, row!.fixedExpansionHeight!)
            return
        }
        var expansionHeight:CGFloat = 0
        for childID:Int in row!.childIDs.keys {
            if let childItem:AccordionRowView = accordionRows[childID] {
                expansionHeight += childItem.getAppConstraint(.height)!.constant
            }
        }
        applyExpansionHeight( id, expansionHeight)
    }

    override func destroy(){
        for row:AccordionRowView in accordionRows.values {
            row.destroy()
        }
        accordionRows = [:]
        super.destroy()
    }

}
