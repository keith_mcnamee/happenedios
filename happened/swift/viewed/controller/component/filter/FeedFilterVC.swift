import UIKit

class FeedFilterVC: ComponentVC, UIGestureRecognizerDelegate {

    private var app:App { return App.instance }
    private var preference:Preference { return Preference.instance }

    private var viewed: Viewed { return Viewed.instance }

    static let COMPETITION_VALUE_CHANGED_REQUEST:String = NSUUID().uuidString

    var feedVO:FeedViewVO?
    var listingRefs:[Int:NSNull] = [:]

    var on:Bool = false
    var listingRefsByCompetitionRef:[Int:IntDicVOsVO] = [:]
    var competitionTextVOs:[TextVO] = []
    var allCompetitionSelected:Bool = false
    var isSelectedCompetitionRefs:[Int:Bool] = [:]

    var producedFilterVO:FeedFilterVO? = nil

    private var _listCompetitionsVC: FilterSwitchListVC?

    override func initialize() {
        super.initialize()
        applyObserverID()
    }

    override func createAppView() -> FeedFilterView {
        return FeedFilterView()
    }

    var feedFilterView:FeedFilterView {
        return view as! FeedFilterView
    }

    var listCompetitionVC: FilterSwitchListVC {
        if _listCompetitionsVC != nil {
            return _listCompetitionsVC!
        }
        _listCompetitionsVC = FilterSwitchListVC(valueObserverID: observerID!, changeRequestID: FeedFilterVC.COMPETITION_VALUE_CHANGED_REQUEST)
        _listCompetitionsVC!.showMe(parentController: self, parentView: feedFilterView.competitionsInnerView)
        return _listCompetitionsVC!
    }

    override func applyNotificationRequest(requestType:String?=nil, action:Any?=nil, value:Any?=nil) {
        if requestType != nil {
            switch requestType! {
                case FeedFilterVC.COMPETITION_VALUE_CHANGED_REQUEST:
                    if let newValue:Bool = value as? Bool {
                        let competitionRef:Int? = action as? Int
                        competitionValueChanged(competitionRef, newValue: newValue)
                    }
                    break
                default:
                    break
            }
        }
        super.applyNotificationRequest(requestType: requestType, action: action, value:value)
    }

    func competitionValueChanged(_ competitionRef:Int?, newValue:Bool) {
        if competitionRef == nil {
            allCompetitionSelected = newValue
            if allCompetitionSelected {
                for ref:Int in isSelectedCompetitionRefs.keys {
                    isSelectedCompetitionRefs[ref] = true
                }
            }
        } else {
            isSelectedCompetitionRefs[competitionRef!] = newValue
            if !newValue {
                allCompetitionSelected = false
            }
        }
        applyCurrentCompetitionState()
    }

    func applyCurrentCompetitionState() {
        if isSelectedCompetitionRefs.count < 1 && allCompetitionSelected {
            allCompetitionSelected = false
        } else if _listCompetitionsVC != nil && _listCompetitionsVC!.isSelectedRefs.count < 1 && isSelectedCompetitionRefs.count >= 1 && !allCompetitionSelected {
            var found:Bool = false
            for value:Bool in isSelectedCompetitionRefs.values {
                if value {
                    found = true
                    break
                }
            }
            if !found {
                allCompetitionSelected = true
            }
        }
        if allCompetitionSelected {
            for ref:Int in isSelectedCompetitionRefs.keys {
                if !isSelectedCompetitionRefs[ref]! {
                    isSelectedCompetitionRefs[ref] = true
                }
            }
        }
        applyCurrentCompetitionStateHook()
        _listCompetitionsVC?.allSelected = allCompetitionSelected
        _listCompetitionsVC?.isSelectedRefs = isSelectedCompetitionRefs
        _listCompetitionsVC?.applyCurrent()
        applyEnabled()
    }

    func applyCurrentCompetitionStateHook() {

    }

    override func resetView() {
        super.resetView()
        resetCompetitionList()
        prepareData()
        applyToView()
        applyEnabled()
    }

    func prepareData() {
        applyNonFilteredState()
        if feedVO!.isFiltered {
            applyInitialFilteredState()
        }
        on = true
    }

    func applyEnabled(){
        let haveCompetitionSelected:Bool = allCompetitionSelected || selectedCompetitionRefs.count > 0
        feedFilterView.competitionsRemoveAllButton.setIsEnabled( on && haveCompetitionSelected )
        feedFilterView.selectBtn.setIsEnabled( canSubmit )
        _listCompetitionsVC?.enable( on )
    }

    var canSubmit:Bool {
        return allCompetitionSelected || selectedCompetitionRefs.count > 0
    }

    func applyToView() {
        feedFilterView.applyFilterToggle.isOn = on
        createCompetitionState()
    }

    func resetCompetitionList() {
        let (listingRefsByCompetitionRef, competitionTextVOs) = viewed.viewHelper.createFilterCompetitionListValues( seasonGroupRef:feedVO!.seasonGroupRef, listingRefs:listingRefs )
        self.listingRefsByCompetitionRef = listingRefsByCompetitionRef
        self.competitionTextVOs = competitionTextVOs
    }


    func applyNonFilteredState() {
        allCompetitionSelected = false
        isSelectedCompetitionRefs = [:]

        let userListingRefs:[Int:Any] = preference.preferenceHelper.getListingRefs()
        for textVO:TextVO in competitionTextVOs {
            var selected:Bool = false
            let listingRefs:[Int:Any] = listingRefsByCompetitionRef[ textVO.attributes[.action]! as! Int ]!.vos
            for listingRef:Int in listingRefs.keys {
                if userListingRefs[listingRef] != nil || feedVO!.isTeamFeed {
                    selected = true
                    break
                }
            }
            isSelectedCompetitionRefs[textVO.attributes[.action]! as! Int] = selected
        }
    }

    func applyInitialFilteredState() {
        let filterVO:FeedFilterVO = feedVO!.filterVO!

        allCompetitionSelected = filterVO.allCompetitionSelected
        for competitionRef:Int in isSelectedCompetitionRefs.keys {
            var selected:Bool = filterVO.allCompetitionSelected
            if !selected {
                let listingRefs:[Int:Any] = listingRefsByCompetitionRef[ competitionRef ]!.vos
                for listingRef:Int in listingRefs.keys {
                    if filterVO.listingRefs[listingRef] != nil {
                        selected = true
                        break
                    }
                }
            }
            isSelectedCompetitionRefs[competitionRef] = selected
        }
    }

    func createCompetitionState() {
        applyCurrentCompetitionState()
        listCompetitionVC.applyCurrentState(textVOs: competitionTextVOs, allSelected: allCompetitionSelected, isSelectedRefs: isSelectedCompetitionRefs)
    }

    override func addTargets() {
        feedFilterView.selectBtn.addTarget(self, action: #selector(selectBtnPressed(_:)), for: .touchUpInside)
        feedFilterView.cancelBtn.addTarget(self, action: #selector(cancelBtnPressed(_:)), for: .touchUpInside)
        feedFilterView.applyFilterToggle.addTarget(self, action: #selector(applyFilterToggled(_:)), for: .valueChanged)
        feedFilterView.competitionsRemoveAllButton.addTarget(self, action: #selector(competitionsRemoveAllPressed(_:)), for: .touchUpInside)

        let tap = UITapGestureRecognizer(target: self, action: #selector(backgroundTapped(_:)))
        tap.delegate = self
        feedFilterView.bgLayer.addGestureRecognizer(tap)
        super.addTargets()
    }

    override func removeTargets() {
        feedFilterView.selectBtn.removeTarget(nil, action: nil, for: .allEvents)
        feedFilterView.cancelBtn.removeTarget(nil, action: nil, for: .allEvents)
        feedFilterView.applyFilterToggle.removeTarget(nil, action: nil, for: .allEvents)
        feedFilterView.competitionsRemoveAllButton.removeTarget(nil, action: nil, for: .allEvents)
        feedFilterView.bgLayer.removeAllGestures()
        super.removeTargets()
    }

    override func applyLocalization() {
        feedFilterView.applyFilterTextView.applyTextVOs([TextVO("apply_filter".localized)])
        feedFilterView.competitionsTextView.applyTextVOs([TextVO("Leagues".localized)])
        feedFilterView.competitionsRemoveAllButton.setTitle("remove_all".localized, for: .normal)

        feedFilterView.cancelBtn.setTitle("cancel".localized, for: .normal)
        feedFilterView.selectBtn.setTitle("select".localized, for: .normal)
        super.applyLocalization()
    }

    @objc func applyFilterToggled(_ sender: Any) {
        on = feedFilterView.applyFilterToggle.isOn
        applyEnabled()
    }

    @objc func competitionsRemoveAllPressed(_ sender: Any) {
        allCompetitionSelected = false
        for competitionRef:Int in isSelectedCompetitionRefs.keys {
            isSelectedCompetitionRefs[competitionRef] = false
        }
        applyCurrentCompetitionState()
    }

    @objc func backgroundTapped(_ sender: Any) {
        hideMe()
    }

    @objc func cancelBtnPressed(_ sender: Any) {
        hideMe()
    }

    @objc func selectBtnPressed(_ sender: Any) {
        if didFilterChange {
            createFilter(addFilterString:true)
            NotificationCenter.default.post(name: Notification.Name(dataChangedNotification), object: nil)
        }
        hideMe()
    }

    func createFilter( addFilterString:Bool ) {
        if !on {
            producedFilterVO = nil
            return
        }
        createNewFilterVO()
        if let filterVO:FeedFilterVO = producedFilterVO {
            filterVO.allCompetitionSelected = allCompetitionSelected
            filterVO.listingRefs = selectedListingRefs
            if addFilterString {
                filterVO.filterString = viewed.viewHelper.constructFilterString(filterVO)
            }
        }
    }

    func createNewFilterVO() {
        producedFilterVO = FeedFilterVO()
    }

    var didFilterChange:Bool {
        if !on {
            return feedVO!.isFiltered
        }
        if !feedVO!.isFiltered {
            return true
        }
        if allCompetitionSelected != feedVO!.filterVO!.allCompetitionSelected {
            return true
        }
        let listingRefs:[Int:NSNull] = selectedListingRefs
        if listingRefs.count != feedVO!.filterVO!.listingRefs.count {
            return true
        }
        for listingRef:Int in listingRefs.keys {
            if feedVO!.filterVO!.listingRefs[listingRef] == nil {
                return true
            }
        }
        return false
    }

    var selectedCompetitionRefs:[Int:NSNull] {
        var refs:[Int:NSNull] = [:]
        for (competitionRef, value) in isSelectedCompetitionRefs {
            if value {
                refs[competitionRef] = NSNull()
            }
        }
        return refs
    }

    var selectedListingRefs:[Int:NSNull] {
        var refs:[Int:NSNull] = [:]
        for competitionRef in selectedCompetitionRefs.keys {
            let listingRefs:[Int:Any] = listingRefsByCompetitionRef[competitionRef]!.vos
            for listingRef:Int in listingRefs.keys {
                refs[listingRef] = NSNull()
            }
        }
        return refs
    }

    override func destroy() {
        _listCompetitionsVC?.destroy()
        super.destroy()
    }
}
