import UIKit

class FilterSwitchListVC:ListVC {

    var valueObserverID:String? = nil
    var changeRequestID:String? = nil

    var enabled:Bool = false
    var allSelected:Bool = false
    var isSelectedRefs:[Int:Bool] = [:]

    enum Columns:Int {
        case name = 0,
             uiSwitch = 1
    }

    var allRow:ListRowView? = nil
    var actionRows:[Int: ListRowView] = [:]

    convenience init( valueObserverID:String, changeRequestID:String ) {
        self.init(nibName: nil, bundle: nil)
        self.valueObserverID = valueObserverID
        self.changeRequestID = changeRequestID
    }

    override func applyGeneralView() {
        super.applyGeneralView()

        bottomRowPadding = 8
        alignCenterY = true
    }


    func applyCurrentState( textVOs:[TextVO], allSelected:Bool, isSelectedRefs:[Int:Bool]) {

        actionRows = [:]

        let nameColumnVO: ListColumnVO = ListColumnVO(Columns.name.rawValue)
        let uiSwitchColumnVO: ListColumnVO = ListColumnVO(Columns.uiSwitch.rawValue)

        let columnVOs:[ListColumnVO] = [nameColumnVO, uiSwitchColumnVO]
        var rowVOs:[ListRowVO] = []

        let headerRowVO:ListRowVO = ListRowVO()
        let headerNameColumn: AttributedTextView = AttributedTextView()
        headerNameColumn.applyTextVOs([TextVO("all".localized, attributes: [.bold:NSNull()])])
        headerRowVO.addCell( nameColumnVO.id, headerNameColumn )

        let headerUiSwitchColumn: AppSwitch = AppSwitch()
        headerUiSwitchColumn.addObserverID(observerID: valueObserverID!, requestType: changeRequestID!)
        headerRowVO.addCell( uiSwitchColumnVO.id,  headerUiSwitchColumn )

        rowVOs.append(headerRowVO)

        for textVO:TextVO in textVOs {
            let action:Int = textVO.attributes[.action] as! Int
            let rowVO:ListRowVO = ListRowVO()
            let nameColumn: AttributedTextView = AttributedTextView()
            nameColumn.applyTextVOs([textVO])
            nameColumn.textContainer.maximumNumberOfLines = 2
            rowVO.addCell( nameColumnVO.id, nameColumn )

            let uiSwitchColumn: AppSwitch = AppSwitch()
            uiSwitchColumn.addObserverID(observerID: valueObserverID!, requestType: changeRequestID!, action: action)
            rowVO.addCell( uiSwitchColumnVO.id, uiSwitchColumn )

            rowVOs.append(rowVO)
        }

        self.columnVOs = columnVOs
        self.rowVOs = rowVOs
        self.allSelected = allSelected
        self.isSelectedRefs = isSelectedRefs

        createView()
        applyCurrent()
        applyEnabled()
    }

    override func applyRow(row: ListRowView, rowVO:ListRowVO ) {
        super.applyRow(row: row, rowVO: rowVO)
        row.getAppConstraint(.top)!.constant = 2

        let uiSwitch: AppSwitch = row.cells[Columns.uiSwitch.rawValue] as! AppSwitch
        let action:Int? = uiSwitch.action as? Int
        if action == nil {
            allRow = row
        } else {
            actionRows[action!] = row
        }
    }

    func enable(_ value:Bool) {
        enabled = value
        applyEnabled()
    }

    func applyEnabled() {
        allSwitch?.isEnabled = enabled
        for action:Int in actionRows.keys {
            actionSwitch(action)?.isEnabled = enabled
        }
    }

    func applyCurrent() {
        allSwitch?.isOn = allSelected
        for action:Int in actionRows.keys {
            actionSwitch(action)?.isOn = allSelected || (isSelectedRefs[action] != nil && isSelectedRefs[action]!)
        }
    }

    var allSwitch: AppSwitch? {
        return allRow?.cells[Columns.uiSwitch.rawValue] as? AppSwitch
    }

    func actionSwitch( _ action:Int ) -> AppSwitch? {
        return actionRows[action]?.cells[Columns.uiSwitch.rawValue] as? AppSwitch
    }

    override func destroy(){
        actionRows = [:]
        allRow = nil
        super.destroy()
    }
}
