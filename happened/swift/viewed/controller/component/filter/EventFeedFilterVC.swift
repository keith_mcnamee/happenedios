import UIKit

class EventFeedFilterVC: FeedFilterVC {

    private var app:App { return App.instance }
    private var balancing:Balancing { return Balancing.instance }
    private var happen:Happen { return Happen.instance }
    private var localization:Localization { return Localization.instance }
    private var viewed:Viewed { return Viewed.instance }

    static let EVENT_VALUE_CHANGED_REQUEST:String = NSUUID().uuidString

    enum HappenedStateIndexes:Int {
        case toHappen = 0,
             toNotHappen = 1,
             both = 2
    }

    var toHappenState:ToHappenedState = .toHappenOnly
    var eventTextVOs:[TextVO] = []
    var allEventSelected:Bool = false
    var isSelectedEventRefs:[Int:Bool] = [:]

    var eventListInitialized:Bool = false

    private var _listEventVC: FilterSwitchListVC?

    override func createAppView() -> EventFeedFilterView {
        return EventFeedFilterView()
    }

    var eventFeedFilterView:EventFeedFilterView {
        return view as! EventFeedFilterView
    }

    var eventFeedFilterVO:EventFeedFilterVO? {
        return feedVO!.filterVO as? EventFeedFilterVO
    }

    var listEventVC: FilterSwitchListVC {
        if _listEventVC != nil {
            return _listEventVC!
        }
        _listEventVC = FilterSwitchListVC(valueObserverID: observerID!, changeRequestID: EventFeedFilterVC.EVENT_VALUE_CHANGED_REQUEST)
        _listEventVC!.showMe(parentController: self, parentView: eventFeedFilterView.eventsInnerView)
        return _listEventVC!
    }

    override func applyNotificationRequest(requestType:String?=nil, action:Any?=nil, value:Any?=nil) {
        if requestType != nil {
            switch requestType! {
            case EventFeedFilterVC.EVENT_VALUE_CHANGED_REQUEST:
                if let newValue:Bool = value as? Bool {
                    let eventRef:Int? = action as? Int
                    eventValueChanged(eventRef, newValue: newValue)
                }
                break
            default:
                break
            }
        }
        super.applyNotificationRequest(requestType: requestType, action: action, value: value)
    }

    override func applyCurrentCompetitionStateHook() {
        super.applyCurrentCompetitionStateHook()
        updateEventList()
    }

    func eventValueChanged(_ eventRef:Int?, newValue:Bool) {
        if eventRef == nil {
            allEventSelected = newValue
            if allEventSelected {
                for ref:Int in isSelectedEventRefs.keys {
                    isSelectedEventRefs[ref] = true
                }
            }
        } else {
            isSelectedEventRefs[eventRef!] = newValue
            if !newValue {
                allEventSelected = false
            }
        }
        applyCurrentEventState()
    }

    func applyCurrentEventState() {
        if isSelectedEventRefs.count < 1 && allEventSelected {
            allEventSelected = false
        } else if listEventVC.isSelectedRefs.count < 1 && isSelectedEventRefs.count >= 1 && !allEventSelected {
            var found:Bool = false
            for value:Bool in isSelectedEventRefs.values {
                if value {
                    found = true
                    break
                }
            }
            if !found {
                allEventSelected = true
            }
        }
        if allEventSelected {
            for ref:Int in isSelectedEventRefs.keys {
                if !isSelectedEventRefs[ref]! {
                    isSelectedEventRefs[ref] = true
                }
            }
        }

        _listEventVC?.allSelected = allEventSelected
        _listEventVC?.isSelectedRefs = isSelectedEventRefs
        _listEventVC?.applyCurrent()
        applyEnabled()
    }

    override func applyNonFilteredState() {
        super.applyNonFilteredState()
        toHappenState = .toHappenOnly
        allEventSelected = true
    }


    func applyEventListNonFilteredState() {
        for eventRef:Int in isSelectedEventRefs.keys {
            isSelectedEventRefs[eventRef] = allEventSelected
        }
    }

    override func applyInitialFilteredState() {
        super.applyInitialFilteredState()
        toHappenState = eventFeedFilterVO!.toHappenState
        allEventSelected = eventFeedFilterVO!.allEventSelected
    }

    func applyEventListInitialFilteredState() {
        for eventRef:Int in isSelectedEventRefs.keys {
            var selected:Bool = eventFeedFilterVO!.allEventSelected
            if !selected {
                if eventFeedFilterVO!.eventRefs[eventRef] != nil {
                    selected = true
                    break
                }
            }
            isSelectedEventRefs[eventRef] = selected
        }
    }


    override func prepareData() {
        eventListInitialized = false
        super.prepareData()
        updateEventList()
        applyEventListNonFilteredState()
        if feedVO!.isFiltered {
            applyEventListInitialFilteredState()
        }
    }

    override func applyToView() {
        super.applyToView()
        initializeHappenedSegment()
        eventListInitialized = true
        createEventState()
    }

    override func applyEnabled(){
        let haveEventSelected:Bool = allEventSelected || selectedEventRefs.count > 0

        eventFeedFilterView.happenStateSegment.isEnabled = on
        eventFeedFilterView.eventsRemoveAllButton.setIsEnabled( on && haveEventSelected )

        _listEventVC?.enable( on )
        super.applyEnabled()
    }

    override var canSubmit:Bool {
        return allEventSelected || selectedEventRefs.count > 0
    }

    func initializeHappenedSegment() {
        eventFeedFilterView.happenStateSegment.removeAllSegments()

        eventFeedFilterView.happenStateSegment.insertSegment(withTitle: "to_happen_only".localized, at: HappenedStateIndexes.toHappen.rawValue, animated: false)
        eventFeedFilterView.happenStateSegment.insertSegment(withTitle: "to_not_happen_only".localized, at: HappenedStateIndexes.toNotHappen.rawValue, animated: false)
        eventFeedFilterView.happenStateSegment.insertSegment(withTitle: "both".localized, at: HappenedStateIndexes.both.rawValue, animated: false)

        var selectedSegment:Int = HappenedStateIndexes.toHappen.rawValue
        if toHappenState == .toNotHappenOnly {
            selectedSegment = HappenedStateIndexes.toNotHappen.rawValue
        } else if toHappenState == .both {
            selectedSegment = HappenedStateIndexes.both.rawValue
        }
        eventFeedFilterView.happenStateSegment.selectedSegmentIndex = selectedSegment
    }

    override func addTargets() {
        eventFeedFilterView.happenStateSegment.addTarget(self, action: #selector(happenedStateChanged(_:)), for: .valueChanged)
        eventFeedFilterView.eventsRemoveAllButton.addTarget(self, action: #selector(eventsRemoveAllPressed(_:)), for: .touchUpInside)
        super.addTargets()
    }

    override func removeTargets() {
        eventFeedFilterView.happenStateSegment.removeTarget(nil, action: nil, for: .allEvents)
        eventFeedFilterView.eventsRemoveAllButton.removeTarget(nil, action: nil, for: .allEvents)
        super.removeTargets()
    }

    @objc func happenedStateChanged(_ sender: Any) {
        var toHappenState:ToHappenedState = .toHappenOnly
        let selectedSegment:Int = eventFeedFilterView.happenStateSegment.selectedSegmentIndex
        if selectedSegment == HappenedStateIndexes.toNotHappen.rawValue {
            toHappenState = .toNotHappenOnly
        } else if selectedSegment == HappenedStateIndexes.both.rawValue {
            toHappenState = .both
        }
        self.toHappenState = toHappenState
    }

    @objc func eventsRemoveAllPressed(_ sender: Any) {
        allEventSelected = false
        for eventRef:Int in isSelectedEventRefs.keys {
            isSelectedEventRefs[eventRef] = false
        }
        applyCurrentEventState()
    }

    func updateEventList() {
        var eventBalancingVOs:[EventBalancingVO] = []
        var eventRefs:[Int:NSNull] = [:]
        let seasonRefs:[Int:NSNull] = balancing.balancingHelper.getAllSeasonRefs(seasonGroupRef: feedVO!.seasonGroupRef)

        for (competitionRef, selected) in isSelectedCompetitionRefs {
            if !selected {
                continue
            }
            let listingRefs:[Int:Any] = listingRefsByCompetitionRef[competitionRef]!.vos
            for seasonRef:Int in seasonRefs.keys {
                for listingRef:Int in listingRefs.keys {
                    if let listingSeasonVO:ListingSeasonVO = happen.happenedModel.getListingSeasonVO(seasonRef, listingRef) {
                        for eventRef:Int in listingSeasonVO.validEventRefs.keys {
                            if eventRefs[eventRef] == nil {
                                eventRefs[eventRef] = NSNull()
                                eventBalancingVOs.append(balancing.balancingModel.getEventVO(eventRef, seasonRef: seasonRef)!)
                            }
                        }
                    }
                }
            }
        }

        var weighted:[Int:NSNull] = [:]
        weighted[BalancingConfig.EventRefs.win.rawValue] = NSNull()
        weighted[BalancingConfig.EventRefs.groupWin.rawValue] = NSNull()

        eventBalancingVOs = viewed.viewHelper.sortEventBalancingVOs( eventBalancingVOs, weightEventRefs:weighted )

        let existingTextVOs:[TextVO] = eventTextVOs
        let wasAllEventSelected:Bool = allEventSelected
        var existingCheckList:[Int:Bool] = isSelectedEventRefs

        var textVOs:[TextVO] = []
        var checkList:[Int:Bool] = [:]
        let seasonRef:Int? = balancing.balancingHelper.getBestSeasonRef(seasonGroupRef:feedVO!.seasonGroupRef)
        var listDidChange:Bool = false
        for eventRef:Int in existingCheckList.keys {
            if eventRefs[eventRef] == nil {
                listDidChange = true
                break
            }
        }
        for eventBalancingVO:EventBalancingVO in eventBalancingVOs {
            var textVO:TextVO? = nil

            if existingCheckList[ eventBalancingVO.ref ] != nil {
                checkList[ eventBalancingVO.ref ] = existingCheckList[ eventBalancingVO.ref ]
                for compareTextVO:TextVO in existingTextVOs {
                    if compareTextVO.attributes[.action] != nil && compareTextVO.attributes[.action] is Int && compareTextVO.attributes[.action]! as! Int == eventBalancingVO.ref {
                        textVO = compareTextVO
                    }
                }
            } else {
                checkList[ eventBalancingVO.ref ] = wasAllEventSelected
                listDidChange = true
            }
            if textVO == nil{
                let name:String = localization.localizationHelper.getEventName(eventBalancingVO.ref, seasonRef: seasonRef, ext: LocalizationConstant.GENERIC_EXTENSION)
                textVO = TextVO(name, attributes: [.action: eventBalancingVO.ref])
            }
            textVOs.append( textVO! )
        }
        if listDidChange {
            eventTextVOs = textVOs
            isSelectedEventRefs = checkList
            if eventListInitialized {
                createEventState()
                applyEnabled()
            }
        }
    }

    func createEventState() {
        applyCurrentEventState()
        listEventVC.applyCurrentState(textVOs: eventTextVOs, allSelected: allEventSelected, isSelectedRefs: isSelectedEventRefs)
    }

    var selectedEventRefs:[Int:NSNull] {
        var refs:[Int:NSNull] = [:]
        for (eventRef, value) in isSelectedEventRefs {
            if value {
                refs[eventRef] = NSNull()
            }
        }
        return refs
    }

    override func createFilter( addFilterString:Bool=false ) {
        super.createFilter(addFilterString:false)
        if let filterVO:EventFeedFilterVO = producedFilterVO as? EventFeedFilterVO {
            filterVO.toHappenState = toHappenState
            filterVO.allEventSelected = allEventSelected
            filterVO.eventRefs = selectedEventRefs
            if addFilterString {
                filterVO.filterString = viewed.viewHelper.constructEventFilterString(filterVO)
            }
        }
    }

    override func createNewFilterVO() {
        producedFilterVO = EventFeedFilterVO()
    }

    override var didFilterChange:Bool {
        if super.didFilterChange {
            return true
        }

        if !on && !feedVO!.isFiltered {
            return false
        }

        if toHappenState != eventFeedFilterVO!.toHappenState {
            return true
        }

        if allEventSelected != eventFeedFilterVO!.allEventSelected {
            return true
        }

        if isSelectedEventRefs.count != eventFeedFilterVO!.eventRefs.count {
            return true
        }
        for eventRef:Int in isSelectedEventRefs.keys {
            if eventFeedFilterVO!.eventRefs[eventRef] == nil {
                return true
            }
        }
        return false
    }


    override func applyLocalization() {
        eventFeedFilterView.eventsTextView.applyTextVOs([TextVO("Events".localized)])
        eventFeedFilterView.eventsRemoveAllButton.setTitle("remove_all".localized, for: .normal)
        super.applyLocalization()
    }

    override func destroy() {
        _listEventVC?.destroy()
        super.destroy()
    }
}
