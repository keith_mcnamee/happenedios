import UIKit

class TeamListVC: ListVC {

    private var balancing: Balancing { return Balancing.instance }
    private var happen: Happen { return Happen.instance }
    private var localization: Localization { return Localization.instance }
    private var viewed: Viewed { return Viewed.instance }

    var navigationObserverID:String?

    enum Columns:Int {
        case scenarioName = 0,
             possible = 1,
             definite = 2,
             result = 3,
             viewBtn = 4
    }

    var teamListingVO: TeamListingVO?

    var haveUnscheduledFixtures:Bool = false
    var haveUnknown:Bool = false

    convenience init( teamListingVO: TeamListingVO, navigationObserverID:String ) {

        self.init(nibName: nil, bundle: nil)

        self.teamListingVO = teamListingVO
        self.navigationObserverID = navigationObserverID

        haveUnscheduledFixtures = happen.happenedHelper.haveUnscheduledFixtures(teamListingVO.seasonRef, teamListingVO.listingRef)

        constructColumnVOs()
        constructRowVOs()

        self.rowVOs = rowVOs
    }

    override func applyGeneralView() {
        super.applyGeneralView()

        leftRowPadding = viewed.viewConfig.topLeftMargin.x
        rightRowPadding = viewed.viewConfig.bottomRightMargin.x
        bottomRowPadding = CGFloat(2).deviceValue
        minCellSpace = CGFloat(1).deviceValue
    }

    func constructColumnVOs() {
        let columnVOs:[ListColumnVO] = [
                ListColumnVO(Columns.scenarioName.rawValue),
                ListColumnVO(Columns.possible.rawValue),
                ListColumnVO(Columns.definite.rawValue),
                ListColumnVO(Columns.result.rawValue),
                ListColumnVO(Columns.viewBtn.rawValue)
        ]
        self.columnVOs = columnVOs
    }

    func constructRowVOs() {
        var rowVOs:[ListRowVO] = []

        rowVOs.append(contentsOf: constructListingRowVOs())
        rowVOs.append(contentsOf: constructEventRowVOs( toHappen: true))
        rowVOs.append(contentsOf: constructEventRowVOs( toHappen: false))

        if haveUnknown {

            let disclaimerRowVO:ListRowVO = ListRowVO()
            let disclaimerTextView: AttributedTextView = AttributedTextView()
            disclaimerTextView.textContainer.maximumNumberOfLines = 0
            disclaimerTextView.applyTextVOs([TextVO("unknown_disclaimer".localized)])
            disclaimerRowVO.addCell(nil, disclaimerTextView)
            rowVOs.append(disclaimerRowVO)
        }

        self.rowVOs = rowVOs
    }

    func constructListingRowVOs() -> [ListRowVO]{

        let isHistorical:Bool = balancing.balancingHelper.isHistoricalSeason(teamListingVO!.seasonRef)

        let titleRowVO:ListRowVO = ListRowVO()

        let titleTextView: AttributedTextView = AttributedTextView()
        titleTextView.font = UIFont.font(pointSizeStyle: .title2)
        titleTextView.textContainer.maximumNumberOfLines = 0
        titleTextView.applyTextVOs([TextVO("league_position".localized)])
        titleRowVO.addCell(nil, titleTextView)


        var allRowVOs:[ListRowVO] = [
                titleRowVO
        ]

        let bestRowVO:ListRowVO = ListRowVO()
        var rowVOs:[ListRowVO] = [
                bestRowVO
        ]
        if !isHistorical {
            rowVOs.append(ListRowVO())
        }
        for rowVO:ListRowVO in rowVOs {
            let isBest:Bool = isHistorical || rowVO === bestRowVO
            let listingScenarioRef:Int? = isBest ? teamListingVO!.bestListingScenarioRef : teamListingVO!.worstListingScenarioRef
            let listingScenarioVO:ListingScenarioVO? = listingScenarioRef != nil ? happen.happenedModel.listingScenarioVOs[listingScenarioRef!] : nil

            let scenarioNameID:String = isHistorical ? "final" : isBest ? "best" : "worst"
            let scenarioNameTextView: AttributedTextView = AttributedTextView( flexible: true )
            scenarioNameTextView.font = UIFont.font(pointSizeStyle: .body2)
            scenarioNameTextView.applyTextVOs([TextVO(scenarioNameID.localized)])
            let resultTextView: AttributedTextView = AttributedTextView( flexible: true )
            resultTextView.font = UIFont.font(pointSizeStyle: .body2)

            var viewBtn:Button? = nil

            if listingScenarioVO == nil || listingScenarioVO!.invalid {
                resultTextView.applyTextVOs([TextVO("can_not_determine".localized)])
            } else {
                let position:String = NumberFormatter.localizedString(from: NSNumber(value: listingScenarioVO!.position!), number: .ordinal)
                resultTextView.applyTextVOs([TextVO(position)])

                viewBtn = BlueButton()
                viewBtn!.setTitle("view".localized, for: .normal)
                viewBtn!.addObserverID(observerID: navigationObserverID!, requestType: AppConstant.SHOW_LISTING_SCENARIO_REQUEST, action: listingScenarioRef)
            }

            rowVO.addListCell(Columns.scenarioName.rawValue, scenarioNameTextView, yPos: .centerY)
            rowVO.addListCell(Columns.possible.rawValue)
            rowVO.addListCell(Columns.definite.rawValue)
            rowVO.addListCell(Columns.result.rawValue, resultTextView, xPos: .centerX, yPos: .centerY)
            rowVO.addListCell(Columns.viewBtn.rawValue, viewBtn, xPos: .centerX, yPos: .centerY)
        }
        allRowVOs.append(contentsOf: rowVOs)
        return allRowVOs
    }


    func constructEventRowVOs( toHappen:Bool ) -> [ListRowVO]{

        let dividerRowVO:ListRowVO = ListRowVO()
        let dividerView:DividerView = DividerView()
        dividerRowVO.addCell(nil, dividerView)

        let titleRowVO:ListRowVO = ListRowVO()

        let titleTextView: AttributedTextView = AttributedTextView()
        titleTextView.font = UIFont.font(pointSizeStyle: .title2)
        titleTextView.textContainer.maximumNumberOfLines = 0
        let titleID:String = toHappen ? "has_it_happened" : "impossible"
        titleTextView.applyTextVOs([TextVO(titleID.localized)])
        titleRowVO.addCell(nil, titleTextView)


        var allRowVOs:[ListRowVO] = [
                dividerRowVO,
                titleRowVO
        ]

        var rowVOs:[ListRowVO] = []
        let headerRowVO:ListRowVO = ListRowVO()

        let eventHeaderTextView: AttributedTextView = AttributedTextView( flexible: true )
        eventHeaderTextView.applyTextVOs([TextVO("event".localized, attributes: [.bold:NSNull()])])

        let possibleMaybeHeaderTextView: AttributedTextView = AttributedTextView( flexible: true )
        possibleMaybeHeaderTextView.applyTextVOs([TextVO((toHappen ? "possible" : "maybe").localized, attributes: [.bold:NSNull()])])

        let definiteHeaderTextView: AttributedTextView = AttributedTextView( flexible: true )
        definiteHeaderTextView.applyTextVOs([TextVO("definite".localized, attributes: [.bold:NSNull()])])

        let fromDateHeaderTextView: AttributedTextView = AttributedTextView( flexible: true )
        fromDateHeaderTextView.applyTextVOs([TextVO("from_date".localized, attributes: [.bold:NSNull()])])

        headerRowVO.addListCell(Columns.scenarioName.rawValue, eventHeaderTextView)
        headerRowVO.addListCell(Columns.possible.rawValue, possibleMaybeHeaderTextView, xPos: .centerX)
        headerRowVO.addListCell(Columns.definite.rawValue, definiteHeaderTextView, xPos: .centerX)
        headerRowVO.addListCell(Columns.result.rawValue, fromDateHeaderTextView, xPos: .centerX)
        headerRowVO.addListCell(Columns.viewBtn.rawValue)

        rowVOs.append(headerRowVO)
        let weightEventRefs:[Int:NSNull] = [
                BalancingConfig.EventRefs.win.rawValue: NSNull(),
                BalancingConfig.EventRefs.groupWin.rawValue: NSNull()
        ]
        let eventBalancingVOs:[EventBalancingVO] = viewed.viewHelper.getSortedEventBalancingVOs(seasonRef: teamListingVO!.seasonRef, listingRef: teamListingVO!.listingRef, includeDuplicates: false, weightEventRefs: weightEventRefs)

        for eventBalancingVO:EventBalancingVO in eventBalancingVOs{
            let eventTeamVO:EventTeamVO? = teamListingVO!.eventTeamVOs[eventBalancingVO.ref]
            if eventTeamVO == nil {
                continue
            }
            let exempt:Bool = eventTeamVO != nil ? eventTeamVO!.exemptionRef != nil : false

            let eventName:String = localization.localizationHelper.getCompetitionEventTitle(teamListingVO!.listingRef, eventBalancingVO.ref, seasonRef: teamListingVO!.seasonRef, ext: LocalizationConstant.TITLE_EXTENSION, competitionExtension: LocalizationConstant.NO_THE_EXTENSION)
            let eventNameTextView: AttributedTextView = AttributedTextView( flexible: true )
            eventNameTextView.font = UIFont.font(pointSizeStyle: .body2)
            eventNameTextView.applyTextVOs([TextVO(eventName)])

            var possibleImageName:String? = nil
            var certainImageName:String? = nil

            let primaryRef:Int? = eventTeamVO != nil ? toHappen ? eventTeamVO!.scenarioToHappenRef : eventTeamVO!.scenarioNotHappenRef : nil
            let secondaryRef:Int? = eventTeamVO != nil ? toHappen ? eventTeamVO!.scenarioNotHappenRef : eventTeamVO!.scenarioToHappenRef : nil
            let primaryVO:EventScenarioVO? = primaryRef != nil ? happen.happenedModel.eventScenarioVOs[primaryRef!] : nil
            let secondaryVO:EventScenarioVO? = secondaryRef != nil ? happen.happenedModel.eventScenarioVOs[secondaryRef!] : nil
            let invalid:Bool = primaryVO == nil || primaryVO!.invalid

            var fromDateText:String = ""
            var viewBtn:Button? = nil
            if exempt {
                possibleImageName = "cross.png"
                certainImageName = "cross.png"
                fromDateText = "ineligible".localized
            } else {

                let possibleIsYes:Bool = !happen.happenedHelper.eventScenarioCanNotOccur(primaryVO: primaryVO, secondaryVO: secondaryVO)
                possibleImageName = possibleIsYes ? "tick.png" : "cross.png"

                let certainIsYes:Bool = happen.happenedHelper.eventScenarioOccurred(primaryVO: primaryVO, secondaryVO: secondaryVO)
                certainImageName = certainIsYes ? "tick.png" : "cross.png"

                if invalid {
                    let invalidID:String = !possibleIsYes ? "-" : haveUnscheduledFixtures ? "unknown" : "can_not_determine"
                    fromDateText = invalidID.localized
                    if possibleIsYes && haveUnscheduledFixtures {
                        haveUnknown = true
                    }
                } else {

                    let dateFormatter:DateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "dd/MM/yy hh:mm"+viewed.viewConfig.nonBreakCode+"a"
                    fromDateText =  dateFormatter.string(from: primaryVO!.occurredDate!)

                    viewBtn = BlueButton()
                    viewBtn!.setTitle("view".localized, for: .normal)
                    viewBtn!.addObserverID(observerID: navigationObserverID!, requestType: AppConstant.SHOW_EVENT_SCENARIO_REQUEST, action: primaryRef!)
                }
            }
            let fromDateTextView: AttributedTextView = AttributedTextView( flexible: true )
            fromDateTextView.font = UIFont.font(pointSizeStyle: .body2)
            fromDateTextView.applyTextVOs([TextVO(fromDateText)])
            let possibleImageView: ImageView = ImageView(image:UIImage.scaledToSized(named: possibleImageName!, width: CGFloat(26).deviceValue))
            let certainImageView: ImageView = ImageView(image:UIImage.scaledToSized(named: certainImageName!, width: CGFloat(26).deviceValue))

            let rowVO:ListRowVO = ListRowVO()

            rowVO.addListCell(Columns.scenarioName.rawValue, eventNameTextView, yPos: .centerY)
            rowVO.addListCell(Columns.possible.rawValue, possibleImageView, xPos: .centerX, yPos: .centerY)
            rowVO.addListCell(Columns.definite.rawValue, certainImageView, xPos: .centerX, yPos: .centerY)
            rowVO.addListCell(Columns.result.rawValue, fromDateTextView, xPos: .centerX, yPos: .centerY)
            rowVO.addListCell(Columns.viewBtn.rawValue, viewBtn, xPos: .centerX, yPos: .centerY)

            rowVOs.append(rowVO)
        }
        allRowVOs.append(contentsOf: rowVOs)
        return allRowVOs
    }

}
