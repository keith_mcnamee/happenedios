import UIKit

class PickerVC: ComponentVC, UIPickerViewDelegate, UIPickerViewDataSource {

    private var viewed:Viewed { return Viewed.instance }

    var vos:[RefTextViewVO]?
    var ref:Int?

    convenience init( vos:[RefTextViewVO], ref:Int ) {
        self.init(nibName: nil, bundle: nil)

        self.vos = vos
        self.ref = ref
    }

    override func createAppView() -> PickerContainerView {
        return PickerContainerView()
    }

    var pickerContainerView: PickerContainerView {
        return view as! PickerContainerView
    }

    override func applyLocalization() {
        pickerContainerView.cancelBtn.setTitle("cancel".localized, for: .normal)
        pickerContainerView.selectBtn.setTitle("select".localized, for: .normal)
        super.applyLocalization()
    }

    override func viewDidFirstAppear() {
        super.viewDidFirstAppear()
        initializePicker()

    }

    override func resetView() {
        selectCurrentRow()
        super.resetView()
    }

    override func addTargets() {
        pickerContainerView.selectBtn.addTarget(self, action: #selector(selectBtnPressed(_:)), for: .touchUpInside)
        pickerContainerView.cancelBtn.addTarget(self, action: #selector(cancelBtnPressed(_:)), for: .touchUpInside)
        super.addTargets()
    }

    override func removeTargets() {
        pickerContainerView.selectBtn.removeAllTargets()
        pickerContainerView.cancelBtn.removeAllTargets()
        super.removeTargets()
    }

    func initializePicker() {

        pickerContainerView.pickerView.delegate = self
        pickerContainerView.pickerView.dataSource = self
    }

    func selectCurrentRow() {
        var row:Int = 0
        for vo:RefTextViewVO in vos! {
            if vo.ref == ref {
                break
            }
            row += 1
        }
        if row >= vos!.count{
            if row != 0 {
                row = 0
                ref = vos![row].ref
                NotificationCenter.default.post(name: Notification.Name(dataChangedNotification), object: nil)
            }
        }
        pickerContainerView.pickerView.selectRow(row, inComponent: 0, animated: false)
    }

    @objc func cancelBtnPressed(_ sender: Any) {
        hideMe()
    }

    @objc func selectBtnPressed(_ sender: Any) {
        let row:Int = pickerContainerView.pickerView.selectedRow(inComponent: 0)
        let thisRef:Int = vos![row].ref
        if thisRef == ref {
            hideMe()
            return
        }
        ref = thisRef
        NotificationCenter.default.post(name: Notification.Name(dataChangedNotification), object: nil)
        hideMe()
    }

    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return vos!.count
    }

    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }

    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {

        var pickerLabel = view as? UILabel;

        if (pickerLabel == nil)
        {
            pickerLabel = UILabel()

            pickerLabel?.font = pickerContainerView.pickerView.font!
            pickerLabel?.textAlignment = .center
        }

        pickerLabel?.text = vos![row].text

        return pickerLabel!;
    }

    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return floor(pickerContainerView.pickerView.font!.lineHeight + 2 * pickerContainerView.pickerView.rowPadding.height.deviceValue)
    }

}
