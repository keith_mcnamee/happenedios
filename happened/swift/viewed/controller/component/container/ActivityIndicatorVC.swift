import UIKit

class ActivityIndicatorVC: ComponentVC {

    convenience init() {
        self.init(nibName: nil, bundle: nil)
    }

    override func createAppView() -> ActivityIndicatorContainerView {
        return ActivityIndicatorContainerView()
    }

    var activityIndicator: ActivityIndicatorContainerView {
        return view as! ActivityIndicatorContainerView
    }
}
