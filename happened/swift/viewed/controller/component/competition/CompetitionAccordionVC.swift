import UIKit

class CompetitionAccordionVC: AccordionListVC {

    static let ALL_COMPETITION_SELECTED:String = NSUUID().uuidString
    static let COMPETITION_SELECTED:String = NSUUID().uuidString
    static let LISTING_SELECTED:String = NSUUID().uuidString

    private var app:App { return App.instance }
    private var balancing:Balancing { return Balancing.instance }
    private var happen: Happen { return Happen.instance }
    private var localization: Localization { return Localization.instance }
    private var server: Server { return Server.instance }
    private var preference: Preference { return Preference.instance }
    private var viewed: Viewed { return Viewed.instance }

    private var competitionListService:CompetitionListService?

    var navigationObserverID:String?
    var pageObserverID:String?
    var seasonGroupRef:Int?

    var arrayCompetitionRowVOs:[CompetitionRowVO]? = nil
    var competitionRowVOs:[Int: CompetitionRowVO]? = nil
    var listingRowVOs:[Int: CompetitionListingRowVO]? = nil
    var competitionSelectedStates:[Int:MultipleSelectedState]? = nil
    var listingSelectedStates:[Int:Bool]? = nil

    var listingRows:[Int: CompetitionListingRowView] = [:]

    var haveData:Bool = false

    var validSeasonWasRequired:Bool = false

    weak var allSelectedBtn:CompetitionMultipleSelectButton?

    var competitionAccordionRows:[Int: CompetitionAccordionRowView] {
        return accordionRows as! [Int: CompetitionAccordionRowView]
    }

    override func createRowView() -> CompetitionAccordionRowView {
        return CompetitionAccordionRowView()
    }

    convenience init( seasonGroupRef:Int, navigationObserverID:String, pageObserverID:String, verticalScrollObserverID:String  ) {

        self.init( verticalScrollObserverID: verticalScrollObserverID )

        self.seasonGroupRef = seasonGroupRef
        self.navigationObserverID = navigationObserverID
        self.pageObserverID = pageObserverID

        competitionListService = viewed.competitionListService()

    }

    override func initialize() {
        super.initialize()
        applyObserverID()
    }

    override func initializeList( isFirstAppear:Bool = false ) {
        super.initializeList()
        if !requireMissingData() {
            validSeasonWasRequired = false
            if !isFirstAppear {
                applyInitializeList()
            } else {
                showViewLoadingActivityIndicator(applyInitializeList)
            }
        }
    }

    func applyInitializeList() {
        haveData = true
        arrayCompetitionRowVOs = competitionListService!.createCompetitionRows( seasonGroupRef: seasonGroupRef!, skipFilter: false)
        competitionRowVOs = competitionListService!.flattenCompetitionVOs( arrayCompetitionRowVOs! )
        listingRowVOs = competitionListService!.flattenListingVOs( arrayCompetitionRowVOs! )
        createView()
        applyCurrentState()
    }

    override func resetView() {
        super.resetView()
        if haveData {
            applyCurrentState()
        }
    }

    func applyCurrentState() {

        _ = preference.preferenceHelper.getListingRefs()
        applySelectedStates()

        var defaultIsOpenStates:[Int:Bool] = competitionListService!.getDefaultIsOpenStates(competitionSelectedStates: competitionSelectedStates!)
        for (id, row) in accordionRows {
            forceExpandContract(id, defaultIsOpenStates[id]!)
            row.setNeedsLayout()
        }
        view.layoutIfNeeded()
    }

    @objc func missingDataReceived(_ sender: Any?=nil) {
        removeActivityEndedObserver()
        initializeList()
    }

    func requireMissingData() -> Bool {
        let seasonRefs:[Int:NSNull] = balancing.balancingHelper.getAllSeasonRefs(seasonGroupRef: seasonGroupRef!)
        var requiredSeasonGroupRefs:[Int:NSNull] = [:]
        for seasonRef:Int in seasonRefs.keys {
            if let seasonVO:SeasonVO = happen.happenedModel.seasonVOs[seasonRef] {
                if !seasonVO.haveData {
                    requiredSeasonGroupRefs[seasonGroupRef!] = NSNull()
                    break
                }
            } else if happen.happenedModel.nonExistentSeasonRefs[seasonRef] == nil {
                requiredSeasonGroupRefs[seasonGroupRef!] = NSNull()
                break
            }
        }
        if requiredSeasonGroupRefs.count > 0 {
            if !validSeasonWasRequired {
                addActivityEndedObserver(selector: #selector(missingDataReceived(_:)))
                server.validSeasonServerCommand().command(seasonGroupRefs: requiredSeasonGroupRefs)
            } else {
                app.updateErrorAppCommand().command()
            }
            return true
        }
        return false
    }

    override func applyGeneralView() {
        super.applyGeneralView()
        addAllRow()
    }

    func addAllRow() {

        let allSelectedBtn:CompetitionMultipleSelectButton = CompetitionMultipleSelectButton()
        self.allSelectedBtn = allSelectedBtn

        view.addSubview(allSelectedBtn)
        var topConstraint:NSLayoutConstraint? = nil
        var leadingConstraint:NSLayoutConstraint? = nil
        var trailingConstraint:NSLayoutConstraint? = nil
        var centerYConstraint:NSLayoutConstraint? = nil
        topConstraint = NSLayoutConstraint(item: allSelectedBtn, attribute: .top, relatedBy: .equal, toItem: appView, attribute: .top, multiplier: 1, deviceConstant: 7)
        trailingConstraint = NSLayoutConstraint(item: allSelectedBtn, attribute: .trailing, relatedBy: .equal, toItem: appView, attribute: .trailing, multiplier: 1, constant: -viewed.viewConfig.bottomRightMargin.x)
        allSelectedBtn.addAppConstraint(topConstraint!, appView)
        allSelectedBtn.addAppConstraint(trailingConstraint!, appView)

        allSelectedBtn.addObserverID(observerID: observerID!, requestType: CompetitionAccordionVC.ALL_COMPETITION_SELECTED)


        let allSelectedTextView:AttributedTextView = AttributedTextView()

        allSelectedTextView.applyTextVOs([TextVO("all".localized)])

        view.addSubview(allSelectedTextView)
        leadingConstraint = NSLayoutConstraint(item: allSelectedTextView, attribute: .leading, relatedBy: .equal, toItem: appView, attribute: .leading, multiplier: 1, constant: viewed.viewConfig.topLeftMargin.x )
        centerYConstraint = NSLayoutConstraint(item: allSelectedTextView, attribute: .centerY, relatedBy: .equal, toItem: allSelectedBtn, attribute: .centerY, multiplier: 1, constant: 0)
        allSelectedTextView.addAppConstraint(leadingConstraint!, appView)
        allSelectedTextView.addAppConstraint(centerYConstraint!, appView)

        allSelectedTextView.setNeedsLayout()
        allSelectedBtn.setNeedsLayout()
    }

    func applySelectedStates() {
        competitionSelectedStates = competitionListService!.getCompetitionSelectedStates(rowVOs: competitionRowVOs!)
        listingSelectedStates = competitionListService!.getListingSelectedStates(listingDictionaryVOs: listingRowVOs!)
        allSelectedBtn!.currentState = competitionListService!.allSelectedState(rowVOs: arrayCompetitionRowVOs!, competitionSelectedStates: competitionSelectedStates!)
        for (id, state) in competitionSelectedStates! {
            if let row:CompetitionAccordionRowView = competitionAccordionRows[id] {
                row.competitionHeaderView.selectedBtn.currentState = state
            }
        }
        for (id, state) in listingSelectedStates! {
            if let listingRow: CompetitionListingRowView = listingRows[id] {
                listingRow.selectedBtn.currentState = state ? .fully : .not
            }
        }
    }

    override func createRows() {
        accordionRows = [:]
        let nextView:UIView = appView
        var nextToItem:UIView = allSelectedBtn!
        let nextToAttribute:NSLayoutConstraint.Attribute = .bottom
        var nextToConstant:CGFloat = 7

        for rowVO:CompetitionRowVO in arrayCompetitionRowVOs! {

            let row: CompetitionAccordionRowView = createRow( parentView:nextView, toItem: nextToItem, toAttribute: nextToAttribute, toConstant: nextToConstant, fromTop: true, id: rowVO.competitionRef ) as! CompetitionAccordionRowView
            applyCompetitionRow(row:row, rowVO: rowVO)
            createChildRows(row: row, rowVO: rowVO)

            nextToItem = row
            nextToConstant = 0
        }

        let bottomConstraint:NSLayoutConstraint = NSLayoutConstraint(item: nextToItem, attribute: .bottom, relatedBy: .lessThanOrEqual, toItem: appView, attribute: .bottom, multiplier: 1, constant: 0)
        (nextToItem as! ExtAppView).addAppConstraint(bottomConstraint, appView, nil)
    }

    override func applyRowWidths() {
        //skipped
    }

    func createChildRows(row: CompetitionAccordionRowView, rowVO: CompetitionRowVO) {

        let nextView:UIView = row.mainContentView
        var nextToItem:UIView = row.mainContentView
        var nextToAttribute:NSLayoutConstraint.Attribute = .bottom
        let nextToConstant:CGFloat = 0

        if rowVO.childCompetitionVOs.count != 0 {
            for childVO: CompetitionRowVO in rowVO.childCompetitionVOs.reversed() {

                let childRow: CompetitionAccordionRowView = createRow( parentView:nextView, toItem: nextToItem, toAttribute: nextToAttribute, toConstant: nextToConstant, fromTop: false, id: childVO.competitionRef, parentID: rowVO.competitionRef) as! CompetitionAccordionRowView
                applyCompetitionRow(row:childRow, rowVO: childVO)
                createChildRows(row: childRow, rowVO:childVO)

                nextToItem = childRow
                nextToAttribute = .top
            }
        } else {
            var fixedHeight:CGFloat = 0
            for childVO: CompetitionListingRowVO in rowVO.childListingVOs.reversed() {

                let childRow: CompetitionListingRowView = CompetitionListingRowView()
                nextView.addSubview(childRow)

                let bottomConstraint:NSLayoutConstraint = NSLayoutConstraint(item: childRow, attribute: .bottom, relatedBy: .equal, toItem: nextToItem, attribute: nextToAttribute, multiplier: 1, constant: nextToConstant)
                let leadingConstraint:NSLayoutConstraint =  NSLayoutConstraint(item: childRow, attribute: .leading, relatedBy: .equal, toItem: nextView, attribute: .leading, multiplier: 1, constant: 0)
                let trailingConstraint:NSLayoutConstraint = NSLayoutConstraint(item: childRow, attribute: .trailing, relatedBy: .equal, toItem: nextView, attribute: .trailing, multiplier: 1, constant: 0)
                childRow.addAppConstraint(bottomConstraint, nextView)
                childRow.addAppConstraint(leadingConstraint, nextView)
                childRow.addAppConstraint(trailingConstraint, nextView)

                listingRows[childVO.listingRef] = childRow
                childRow.viewButton.addObserverID(observerID: navigationObserverID!, requestType: AppConstant.SHOW_LISTING_REQUEST, action: childVO.listingSeasonRef)
                childRow.selectedBtn.addObserverID(observerID: observerID!, requestType: CompetitionAccordionVC.LISTING_SELECTED, action: childVO.listingRef)
                childRow.textView.applyTextVOs(childVO.textVOs)

                fixedHeight += childRow.height

                nextToItem = childRow
                nextToAttribute = .top
            }
            row.fixedExpansionHeight = fixedHeight
            applyCurrentExpansionHeight( rowVO.competitionRef )
        }

        row.setNeedsLayout()
    }

    func applyCompetitionRow(row: CompetitionAccordionRowView, rowVO:CompetitionRowVO ) {
        row.headerView!.textView.applyTextVOs(rowVO.textVOs)

        row.competitionHeaderView.selectedBtn.addObserverID(observerID: observerID!, requestType: CompetitionAccordionVC.COMPETITION_SELECTED, action: rowVO.competitionRef)
    }

    override func applyNotificationRequest(requestType:String?=nil, action:Any?=nil, value:Any?=nil) {
        if requestType != nil {
            switch requestType! {
            case CompetitionAccordionVC.ALL_COMPETITION_SELECTED:
                allCompetitionSelected()
                break
            case CompetitionAccordionVC.COMPETITION_SELECTED:
                if let competitionRef:Int = action as? Int {
                    competitionSelected(competitionRef)
                }
            case CompetitionAccordionVC.LISTING_SELECTED:
                if let listingRef:Int = action as? Int {
                    listingSelected(listingRef)
                }
                break
            default:
                break
            }
        }
        super.applyNotificationRequest(requestType: requestType, action: action, value: value)
    }

    func allCompetitionSelected() {
        let selected:Bool = allSelectedBtn!.currentState != .fully ? true : false
        for rowVO: CompetitionRowVO in arrayCompetitionRowVOs! {
            competitionListService!.selectCompetition(competitionRef: rowVO.competitionRef, selected: selected, competitionDictionaryVOs: competitionRowVOs!)
        }
        applySelectedStates()
    }

    func competitionSelected(_ competitionRef:Int ) {
        let row: CompetitionAccordionRowView? = competitionAccordionRows[competitionRef]
        if row == nil {
            return
        }
        let selected:Bool = row!.competitionHeaderView.selectedBtn.currentState != .fully ? true : false
        competitionListService!.selectCompetition(competitionRef: competitionRef, selected: selected, competitionDictionaryVOs: competitionRowVOs!)
        applySelectedStates()
    }

    func listingSelected(_ listingRef:Int ) {
        let row: CompetitionListingRowView? = listingRows[listingRef]
        if row == nil {
            return
        }
        let selected:Bool = row!.selectedBtn.currentState != .fully ? true : false
        competitionListService!.selectListing(listingRef: listingRef, selected: selected)
        applySelectedStates()
    }

    override func destroy(){
        allSelectedBtn = nil
        listingRows = [:]
        super.destroy()
    }
}
