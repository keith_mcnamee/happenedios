import UIKit

class FixtureFeedTypeVC: FeedTypeVC {

    private var server: Server { return Server.instance }

    var _happenedStateVC: FixtureFeedStateVC?
    var _upcomingStateVC: FixtureFeedStateVC?

    override func defineFeedStates() {

        feedTypeView.stateSegment.insertSegment(withTitle: "results_feed".localized, at: StateIndexes.happened.rawValue, animated: false)
        feedTypeView.stateSegment.insertSegment(withTitle: "fixtures_feed".localized, at: StateIndexes.upcoming.rawValue, animated: false)

        feedTypeView.stateSegment.selectedSegmentIndex = StateIndexes.happened.rawValue
    }

    override func changeState(_ stateIndex:Int) {
        feedTypeView.stateSegment.selectedSegmentIndex = stateIndex
        super.changeState(stateIndex)
    }

    override var stateVC: FeedStateVC {
        let thisIsHappenedState:IsHappenedState = isHappenedState
        if thisIsHappenedState != .upcomingOnly {
            if _happenedStateVC != nil {
                return _happenedStateVC!
            }
        } else {
            if _upcomingStateVC != nil {
                return _upcomingStateVC!
            }
        }

        let vc: FixtureFeedStateVC = FixtureFeedStateVC(feedType: feedType!, seasonGroupRef: seasonGroupRef!, isHappenedState: thisIsHappenedState, isTeamFeed: isTeamFeed!, navigationObserverID: navigationObserverID!, pageObserverID:pageObserverID!, typeObserverID: observerID!)
        if thisIsHappenedState != .upcomingOnly {
            _happenedStateVC = vc
        } else {
            _upcomingStateVC = vc
        }

        return vc
    }

    override var isUpcoming:Bool {
        return !isBoth && feedTypeView.stateSegment.selectedSegmentIndex != StateIndexes.happened.rawValue
    }
}
