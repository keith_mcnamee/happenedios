import UIKit

class FeedTypeVC: ComponentVC {

    private var app:App { return App.instance }

    static let CHANGE_STATE_REQUEST:String = NSUUID().uuidString

    enum StateIndexes:Int {
        case happened = 0,
             upcoming = 1
    }

    var navigationObserverID:String?
    var pageObserverID:String?

    private var balancing: Balancing { return Balancing.instance }
    private var viewed: Viewed { return Viewed.instance }

    private var _initialized:Bool = false

    var feedType:FeedType?
    var seasonGroupRef:Int?
    var isTeamFeed:Bool?

    var _mainContentVC:FeedStateVC?

    convenience init(feedType:FeedType, seasonGroupRef:Int, isTeamFeed:Bool, navigationObserverID:String, pageObserverID:String  ) {

        self.init(nibName: nil, bundle: nil)

        self.feedType = feedType
        self.seasonGroupRef = seasonGroupRef
        self.isTeamFeed = isTeamFeed
        self.navigationObserverID = navigationObserverID
        self.pageObserverID = pageObserverID

    }

    override func initialize() {
        super.initialize()
        applyObserverID()
    }

    override func createAppView() -> FeedTypeView {
        return FeedTypeView()
    }

    var feedTypeView:FeedTypeView {
        return view as! FeedTypeView
    }

    override func applyNotificationRequest(requestType:String?=nil, action:Any?=nil, value:Any?=nil) {
        if requestType != nil {
            switch requestType! {
            case AppConstant.CHANGE_STATE_REQUEST:
                if let stateIndex:Int = action as? Int {
                    changeState(stateIndex)
                }
                break
            default:
                break
            }
        }
        super.applyNotificationRequest(requestType: requestType, action: action, value: value)
    }

    func changeState(_ stateIndex:Int) {
        updateStateView()
    }

    override func viewDidFirstAppear() {
        super.viewDidFirstAppear()
        initializeFeedState()
    }

    func initializeFeedState() {
        feedTypeView.stateSegment.removeAllSegments()
        if isLatestSeasonGroup {
            feedTypeView.isStateSegmentHidden = false
            feedTypeView.stateSegment.addTarget(self, action: #selector(updateStateView(_:)), for: .valueChanged)
            defineFeedStates()
        } else {
            feedTypeView.isStateSegmentHidden = true
            feedTypeView.stateSegment.removeTarget(nil, action: nil, for: .allEvents)
            applyDummyFeedState()
        }

        feedTypeView.stateSegment.selectedSegmentIndex = primarySegmentIndex

        updateStateView()
    }

    func callUpdateStateView(_ sender: Any?=nil) {
        showViewLoadingActivityIndicator(updateStateView)
    }

    func updateStateView() {
        updateStateView(nil)
    }

    @objc func updateStateView(_ sender: Any?=nil) {
        let nextStateVC:FeedStateVC = stateVC!
        if _mainContentVC == nextStateVC {
            return
        }
        _mainContentVC?.hideMe()
        _mainContentVC = nextStateVC
        _mainContentVC?.showMe( parentController: self, parentView: feedTypeView.mainContentView)
    }

    func defineFeedStates() {

    }

    func applyDummyFeedState() {
        feedTypeView.stateSegment.insertSegment(withTitle: "Dummy for override", at: StateIndexes.happened.rawValue, animated: false)
    }

    var stateVC: FeedStateVC? {
        return nil
    }

    var stateContainerView: UIView? {
        return view
    }

    var isHappenedState:IsHappenedState {
        if isBoth {
            return .both
        } else if isUpcoming {
            return .upcomingOnly
        }
        return .happenedOnly
    }

    var isUpcoming:Bool {
        return false
    }

    var isBoth:Bool {
        return !isLatestSeasonGroup
    }

    var isLatestSeasonGroup:Bool {
        return seasonGroupRef == balancing.balancingModel.latestSeasonGroupRef
    }

    var primarySegmentIndex:Int {
        return StateIndexes.happened.rawValue
    }
}
