import UIKit

class EventFeedTypeVC: FeedTypeVC {

    private var viewed: Viewed { return Viewed.instance }

    var _happenedStateVC:EventFeedStateVC?
    var _upcomingStateVC:EventFeedStateVC?

    override func defineFeedStates() {

        feedTypeView.stateSegment.insertSegment(withTitle: "happened_feed".localized, at: StateIndexes.happened.rawValue, animated: false)
        feedTypeView.stateSegment.insertSegment(withTitle: "upcoming_feed".localized, at: StateIndexes.upcoming.rawValue, animated: false)
    }

    override func changeState(_ stateIndex:Int) {
        feedTypeView.stateSegment.selectedSegmentIndex = stateIndex
        super.changeState(stateIndex)
    }

    override var primarySegmentIndex:Int {
        let primaryIsHappened:Bool = viewed.feedViewHelper.primaryIsHappened(feedType: feedType!, feedSource: !isTeamFeed! ? .listing : .specificTeam, seasonGroupRef: seasonGroupRef!)
        return primaryIsHappened ? 0 : 1
    }

    override var stateVC: FeedStateVC {
        let thisIsHappenedState:IsHappenedState = isHappenedState
        if thisIsHappenedState != .upcomingOnly {
            if _happenedStateVC != nil {
                return _happenedStateVC!
            }
        } else {
            if _upcomingStateVC != nil {
                return _upcomingStateVC!
            }
        }

        let vc:EventFeedStateVC = EventFeedStateVC(feedType: feedType!, seasonGroupRef: seasonGroupRef!, isHappenedState: thisIsHappenedState, isTeamFeed: isTeamFeed!, navigationObserverID: navigationObserverID!, pageObserverID:pageObserverID!, typeObserverID: observerID!)
        if thisIsHappenedState != .upcomingOnly {
            _happenedStateVC = vc
        } else {
            _upcomingStateVC = vc
        }

        return vc
    }

    override var isUpcoming:Bool {
        return !isBoth && feedTypeView.stateSegment.selectedSegmentIndex != StateIndexes.happened.rawValue
    }

    override func destroy() {
        _happenedStateVC?.destroy()
        _upcomingStateVC?.destroy()
        super.destroy()
    }
}
