import UIKit

class StandingListVC: ListVC {

    private var viewed: Viewed { return Viewed.instance }

    var standingColumnOrder:[StandingColumnVO]? = nil
    var standingRowVOs:[StandingRowVO]? = nil

    convenience init( standingColumnOrder:[StandingColumnVO], standingRowVOs:[StandingRowVO] ) {
        self.init(nibName: nil, bundle: nil)
        self.standingColumnOrder = standingColumnOrder
        self.standingRowVOs = standingRowVOs
    }

    override func initialize() {
        super.initialize()

        allowHorizontalScrolling = true
    }

    override func applyGeneralView() {
        super.applyGeneralView()

        leftRowPadding = viewed.viewConfig.topLeftMargin.x
        rightRowPadding = viewed.viewConfig.bottomRightMargin.x
    }

    override func viewWillFirstAppear() {
        super.viewWillFirstAppear()

        var columnVOs:[ListColumnVO] = []
        var rowVOs:[ListRowVO] = []

        let headerRowVO:ListRowVO = ListRowVO()
        for standingColumnVO:StandingColumnVO in standingColumnOrder! {
            let columnVO:ListColumnVO = ListColumnVO(standingColumnVO.columnType.rawValue)
            columnVO.attributes = standingColumnVO.attributes
            columnVOs.append(columnVO)

            let headerRowColumn: AttributedTextView = AttributedTextView(flexible: true)
            headerRowColumn.font = UIFont.font(pointSizeStyle: .body2)
            headerRowColumn.applyTextVOs(standingColumnVO.header)
            headerRowVO.addCell( standingColumnVO.columnType.rawValue, headerRowColumn )
        }
        rowVOs.append(headerRowVO)
        for standingRowVO:StandingRowVO in standingRowVOs! {
            let rowVO:ListRowVO = ListRowVO()
            rowVO.attributes = standingRowVO.attributes
            for standingColumnVO:StandingColumnVO in standingColumnOrder! {
                let textVOs:[TextVO] = standingRowVO.columnTextVOs[standingColumnVO.columnType] != nil ? standingRowVO.columnTextVOs[standingColumnVO.columnType]! : [TextVO("")]
                let column: AttributedTextView = AttributedTextView(flexible: true)
                column.font = UIFont.font(pointSizeStyle: .body2)
                column.applyTextVOs(textVOs)
                rowVO.addCell( standingColumnVO.columnType.rawValue, column )
            }
            rowVOs.append(rowVO)
        }

        self.columnVOs = columnVOs
        self.rowVOs = rowVOs

        createView()
    }

    override var vLineY:CGFloat {
        return (rows[0].y + rows[0].height) / 2
    }

    override var vLineHeight:CGFloat {
        return rows[rows.count - 1].y + rows[rows.count - 1].height - rows[0].y - rows[0].height
    }
}
