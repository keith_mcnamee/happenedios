import UIKit

class StandingContainerVC: ComponentVC {

    private var app: App { return App.instance }

    var _mainContentVC:StandingListVC?
    var _accordionVC:StandingAccordionVC?

    var standingListVO: StandingListVO?

    var verticalScrollObserverID:String?

    convenience init( standingListVO: StandingListVO, verticalScrollObserverID:String  ) {
        self.init(nibName: nil, bundle: nil)
        self.standingListVO = standingListVO
        self.verticalScrollObserverID = verticalScrollObserverID
    }

    override func createAppView() -> StandingView {
        return StandingView()
    }

    var standingView:StandingView {
        return view as! StandingView
    }

    override func viewWillFirstAppear() {
        super.viewWillFirstAppear()
        mainContentVC.showMe(parentController: self, parentView: standingView.mainContentView)
        accordionVC.showMe(parentController: self, parentView: standingView.accordionView)
    }

    var mainContentVC: StandingListVC {
        if _mainContentVC != nil {
            return _mainContentVC!
        }
        _mainContentVC = StandingListVC( standingColumnOrder: standingListVO!.columnVOs, standingRowVOs: standingListVO!.rowVOs)
        return _mainContentVC!
    }

    var accordionVC: StandingAccordionVC {
        if _accordionVC != nil {
            return _accordionVC!
        }
        _accordionVC = StandingAccordionVC( standingListVO: standingListVO!, verticalScrollObserverID: verticalScrollObserverID! )
        return _accordionVC!
    }

    override func destroy() {
        _mainContentVC?.destroy()
        _accordionVC?.destroy()
        _mainContentVC = nil
        _accordionVC = nil
        super.destroy()
    }

}
