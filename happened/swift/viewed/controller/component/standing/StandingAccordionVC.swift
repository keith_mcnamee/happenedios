import UIKit

class StandingAccordionVC:AccordionListVC {

    private var viewed: Viewed { return Viewed.instance }

    enum Rows:Int {
        case keys = 0,
             details = 1
    }

    var standingListVO:StandingListVO? = nil

    var keysVC:ListVC? = nil
    var placeEventVC:ListVC? = nil
    var adjustmentAgainstVC:ListVC? = nil
    var adjustmentForVC:ListVC? = nil
    var exemptionVC:ListVC? = nil

    convenience init(standingListVO: StandingListVO, verticalScrollObserverID:String ) {
        self.init(verticalScrollObserverID: verticalScrollObserverID)
        self.standingListVO = standingListVO
    }

    override func initialize() {
        super.initialize()
        applyObserverID()
        allowHorizontalScrolling = true
    }

    override func applyGeneralView() {
        super.applyGeneralView()

        leftRowPadding = viewed.viewConfig.topLeftMargin.x
        rightRowPadding = viewed.viewConfig.bottomRightMargin.x
    }

    override func viewDidFirstAppear() {
        super.viewDidFirstAppear()
        createView()
        applyStandingListVO()
    }

    override func createRows() {

        let nextView:UIView = appView
        var nextToItem:UIView = appView
        var nextToAttribute:NSLayoutConstraint.Attribute = .top
        let nextToConstant:CGFloat = 0

        let keysRow: AccordionRowView = createRow( parentView:nextView, toItem: nextToItem, toAttribute: nextToAttribute, toConstant: nextToConstant, fromTop: true, id: Rows.keys.rawValue )
        keysRow.headerView!.textView.applyTextVOs([TextVO("table_keys".localized)])

        nextToItem = keysRow
        nextToAttribute = .bottom

        let detailsRow: AccordionRowView = createRow( parentView:nextView, toItem: nextToItem, toAttribute: nextToAttribute, toConstant: nextToConstant, fromTop: true, id: Rows.details.rawValue)
        detailsRow.headerView!.textView.applyTextVOs([TextVO("table_details".localized)])

        let lastItem:AppView = detailsRow

        let bottomConstraint:NSLayoutConstraint = NSLayoutConstraint(item: lastItem, attribute: .bottom, relatedBy: .lessThanOrEqual, toItem: appView, attribute: .bottom, multiplier: 1, constant: 0)
        lastItem.addAppConstraint(bottomConstraint, appView)
    }

    override func applyRowWidths() {
        //skipped
    }

    var keysRow: AccordionRowView? {
        return accordionRows[Rows.keys.rawValue]
    }

    var detailsRow: AccordionRowView? {
        return accordionRows[Rows.details.rawValue]
    }

    func applyStandingListVO() {

        destroySubLists()

        applyKeys()

        var toItem:AppView = applyPlaceEvent()
        toItem = applyAdjustmentAgainst( toItem: toItem )
        toItem = applyAdjustmentFor( toItem: toItem)
        applyExemption( toItem: toItem )
    }

    func applyKeys() {

        enum KeyRows:Int {
            case keys = 0,
                 description = 1
        }
        var columnVOs:[ListColumnVO] = []
        var rowVOs:[ListRowVO] = []
        let headerRowVO:ListRowVO = ListRowVO()


        let keysColumnVO:ListColumnVO = ListColumnVO(KeyRows.keys.rawValue)
        let descriptionColumnVO:ListColumnVO = ListColumnVO(KeyRows.description.rawValue)

        columnVOs.append(keysColumnVO)
        columnVOs.append(descriptionColumnVO)

        let headerRowKeysColumn: AttributedTextView = AttributedTextView(flexible: true)
        headerRowKeysColumn.applyTextVOs(createHeaderTextVOs("key".localized))
        headerRowVO.addCell( KeyRows.keys.rawValue, headerRowKeysColumn )

        let headerRowDescriptionColumn: AttributedTextView = AttributedTextView(flexible: true)
        headerRowDescriptionColumn.defaultTextAlignment = .center
        headerRowDescriptionColumn.applyTextVOs(createHeaderTextVOs("description".localized))
        headerRowVO.addCell( KeyRows.description.rawValue,  headerRowDescriptionColumn )

        rowVOs.append(headerRowVO)

        for columnVO:StandingColumnVO in standingListVO!.columnVOs {
            let rowVO:ListRowVO = ListRowVO()

            let rowKeysColumn: AttributedTextView = AttributedTextView(flexible: true)
            rowKeysColumn.font = UIFont.font(pointSizeStyle: .body2)
            var rowKeysTextVOs:[TextVO] = []
            for rowKeysTextVO:TextVO in columnVO.header {
                rowKeysTextVOs.append(TextVO(rowKeysTextVO.text))
            }
            rowKeysColumn.applyTextVOs(rowKeysTextVOs)
            rowVO.addCell( KeyRows.keys.rawValue,  rowKeysColumn )

            let rowDescriptionColumn: AttributedTextView = AttributedTextView(flexible: true)
            rowDescriptionColumn.defaultTextAlignment = .center
            rowDescriptionColumn.font = UIFont.font(pointSizeStyle: .body2)
            rowDescriptionColumn.applyTextVOs(columnVO.description)
            rowVO.addCell( KeyRows.description.rawValue,  rowDescriptionColumn )

            rowVOs.append(rowVO)
        }

        keysVC = ListVC(columnVOs: columnVOs, rowVOs: rowVOs)
        keysVC!.allowHorizontalScrolling = true
        keysVC!.showMe(parentController: self, parentView: keysRow!.mainContentView)
        keysVC!.appView.removeAppConstraint( .bottom )
        keysRow!.mainContentView.layoutIfNeeded()
        keysRow!.expansionHeight = keysVC!.appView.y + keysVC!.appView.height
    }

    func applyPlaceEvent() -> AppView {
        let titleTextView: AttributedTextView = AttributedTextView(flexible: true)
        titleTextView.applyTextVOs(createHeaderTextVOs("event_places".localized))

        detailsRow!.mainContentView.addSubview(titleTextView)
        let titleTopConstraint:NSLayoutConstraint = NSLayoutConstraint(item: titleTextView, attribute: .top, relatedBy: .equal, toItem: detailsRow!.mainContentView, attribute: .top, multiplier: 1, constant: 0)
        let titleLeadingConstraint:NSLayoutConstraint =  NSLayoutConstraint(item: titleTextView, attribute: .leading, relatedBy: .equal, toItem: detailsRow!.mainContentView, attribute: .leading, multiplier: 1, constant: 0)
        let titleTrailingConstraint:NSLayoutConstraint = NSLayoutConstraint(item: titleTextView, attribute: .trailing, relatedBy: .equal, toItem: detailsRow!.mainContentView, attribute: .trailing, multiplier: 1, constant: 0)
        titleTextView.addAppConstraint(titleTopConstraint, detailsRow!.mainContentView)
        titleTextView.addAppConstraint(titleLeadingConstraint, detailsRow!.mainContentView)
        titleTextView.addAppConstraint(titleTrailingConstraint, detailsRow!.mainContentView)


        var columnVOs:[ListColumnVO] = []
        var rowVOs:[ListRowVO] = []
        let headerRowVO:ListRowVO = ListRowVO()

        var index:Int = -1;
        for textVO:TextVO in standingListVO!.placeEventHeaderVOs {
            index += 1
            let columnVO:ListColumnVO = ListColumnVO(index)
            columnVOs.append(columnVO)
            let headerColumn: AttributedTextView = AttributedTextView(flexible: true)
            if index != 0 {
                headerColumn.defaultTextAlignment = .center
            }
            headerColumn.applyTextVOs(viewed.viewHelper.cloneTextVOs([textVO], withAttributes: [.bold:NSNull()]))
            headerRowVO.addCell( index,  headerColumn )
        }

        rowVOs.append(headerRowVO)

        for textVOs:[TextVO] in standingListVO!.placeEventRowVOs {
            let rowVO:ListRowVO = ListRowVO()

            index = -1;
            for textVO:TextVO in textVOs {
                index += 1
                let column: AttributedTextView = AttributedTextView()
                column.font = UIFont.font(pointSizeStyle: .body2)
                if index != 0 {
                    column.defaultTextAlignment = .center
                }
                column.applyTextVOs([textVO])
                rowVO.addCell( index,  column )
            }

            rowVOs.append(rowVO)
        }

        placeEventVC = ListVC(columnVOs: columnVOs, rowVOs: rowVOs)
        placeEventVC!.allowHorizontalScrolling = true
        placeEventVC!.showMe(parentController: self, parentView: detailsRow!.mainContentView)
        let topConstraint:NSLayoutConstraint = NSLayoutConstraint(item: placeEventVC!.appView, attribute: .top, relatedBy: .equal, toItem: titleTextView, attribute: .bottom, multiplier: 1, constant: 0)
        placeEventVC!.appView.addAppConstraint(topConstraint, detailsRow!.mainContentView)
        placeEventVC!.appView.removeAppConstraint( .bottom )
        detailsRow!.mainContentView.layoutIfNeeded()
        detailsRow!.expansionHeight = placeEventVC!.appView.y + placeEventVC!.appView.height

        return placeEventVC!.appView
    }

    func applyAdjustmentAgainst(toItem:AppView) -> AppView {

        if standingListVO!.adjustmentAgainstRowVOs.count == 0 {
            return toItem
        }

        let titleTextView: AttributedTextView = AttributedTextView(flexible: true)
        titleTextView.applyTextVOs(viewed.viewHelper.cloneTextVOs(standingListVO!.adjustmentAgainstTitleVOs, withAttributes: [.bold:NSNull()]))

        detailsRow!.mainContentView.addSubview(titleTextView)
        let titleTopConstraint:NSLayoutConstraint = NSLayoutConstraint(item: titleTextView, attribute: .top, relatedBy: .equal, toItem: toItem, attribute: .bottom, multiplier: 1, constant: 0)
        let titleLeadingConstraint:NSLayoutConstraint =  NSLayoutConstraint(item: titleTextView, attribute: .leading, relatedBy: .equal, toItem: detailsRow!.mainContentView, attribute: .leading, multiplier: 1, constant: 0)
        let titleTrailingConstraint:NSLayoutConstraint = NSLayoutConstraint(item: titleTextView, attribute: .trailing, relatedBy: .equal, toItem: detailsRow!.mainContentView, attribute: .trailing, multiplier: 1, constant: 0)
        titleTextView.addAppConstraint(titleTopConstraint, detailsRow!.mainContentView)
        titleTextView.addAppConstraint(titleLeadingConstraint, detailsRow!.mainContentView)
        titleTextView.addAppConstraint(titleTrailingConstraint, detailsRow!.mainContentView)


        var columnVOs:[ListColumnVO] = []
        var rowVOs:[ListRowVO] = []
        let headerRowVO:ListRowVO = ListRowVO()

        var index:Int = -1;
        for textVO:TextVO in standingListVO!.adjustmentAgainstHeaderVOs {
            index += 1
            let columnVO:ListColumnVO = ListColumnVO(index)
            columnVOs.append(columnVO)
            let headerColumn: AttributedTextView = AttributedTextView(flexible: true)
            if index != 0 {
                headerColumn.defaultTextAlignment = .center
            }
            headerColumn.applyTextVOs(viewed.viewHelper.cloneTextVOs([textVO], withAttributes: [.bold:NSNull()]))
            headerRowVO.addCell( index,  headerColumn )
        }

        rowVOs.append(headerRowVO)

        for textVOs:[TextVO] in standingListVO!.adjustmentAgainstRowVOs {
            let rowVO:ListRowVO = ListRowVO()

            index = -1;
            for textVO:TextVO in textVOs {
                index += 1
                let column: AttributedTextView = AttributedTextView(flexible: true)
                column.font = UIFont.font(pointSizeStyle: .body2)
                if index != 0 {
                    column.defaultTextAlignment = .center
                }
                column.applyTextVOs([textVO])
                rowVO.addCell( index,  column )
            }

            rowVOs.append(rowVO)
        }

        adjustmentAgainstVC = ListVC(columnVOs: columnVOs, rowVOs: rowVOs)
        adjustmentAgainstVC!.allowHorizontalScrolling = true
        adjustmentAgainstVC!.showMe(parentController: self, parentView: detailsRow!.mainContentView)
        let topConstraint:NSLayoutConstraint = NSLayoutConstraint(item: adjustmentAgainstVC!.appView, attribute: .top, relatedBy: .equal, toItem: titleTextView, attribute: .bottom, multiplier: 1, constant: 0)
        adjustmentAgainstVC!.appView.addAppConstraint(topConstraint, detailsRow!.mainContentView)
        adjustmentAgainstVC!.appView.removeAppConstraint( .bottom )
        detailsRow!.mainContentView.layoutIfNeeded()
        detailsRow!.expansionHeight = adjustmentAgainstVC!.appView.y + adjustmentAgainstVC!.appView.height

        return adjustmentAgainstVC!.appView
    }

    func applyAdjustmentFor(toItem:AppView) -> AppView {

        if standingListVO!.adjustmentForRowVOs.count == 0 {
            return toItem
        }

        let titleTextView: AttributedTextView = AttributedTextView(flexible: true)
        titleTextView.applyTextVOs(viewed.viewHelper.cloneTextVOs(standingListVO!.adjustmentForTitleVOs, withAttributes: [.bold:NSNull()]))

        detailsRow!.mainContentView.addSubview(titleTextView)
        let titleTopConstraint:NSLayoutConstraint = NSLayoutConstraint(item: titleTextView, attribute: .top, relatedBy: .equal, toItem: toItem, attribute: .bottom, multiplier: 1, constant: 0)
        let titleLeadingConstraint:NSLayoutConstraint =  NSLayoutConstraint(item: titleTextView, attribute: .leading, relatedBy: .equal, toItem: detailsRow!.mainContentView, attribute: .leading, multiplier: 1, constant: 0)
        let titleTrailingConstraint:NSLayoutConstraint = NSLayoutConstraint(item: titleTextView, attribute: .trailing, relatedBy: .equal, toItem: detailsRow!.mainContentView, attribute: .trailing, multiplier: 1, constant: 0)
        titleTextView.addAppConstraint(titleTopConstraint, detailsRow!.mainContentView)
        titleTextView.addAppConstraint(titleLeadingConstraint, detailsRow!.mainContentView)
        titleTextView.addAppConstraint(titleTrailingConstraint, detailsRow!.mainContentView)


        var columnVOs:[ListColumnVO] = []
        var rowVOs:[ListRowVO] = []
        let headerRowVO:ListRowVO = ListRowVO()

        var index:Int = -1;
        for textVO:TextVO in standingListVO!.adjustmentForHeaderVOs {
            index += 1
            let columnVO:ListColumnVO = ListColumnVO(index)
            columnVOs.append(columnVO)
            let headerColumn: AttributedTextView = AttributedTextView(flexible: true)
            if index != 0 {
                headerColumn.defaultTextAlignment = .center
            }
            headerColumn.applyTextVOs(viewed.viewHelper.cloneTextVOs([textVO], withAttributes: [.bold:NSNull()]))
            headerRowVO.addCell( index,  headerColumn )
        }

        rowVOs.append(headerRowVO)

        for textVOs:[TextVO] in standingListVO!.adjustmentForRowVOs {
            let rowVO:ListRowVO = ListRowVO()

            index = -1;
            for textVO:TextVO in textVOs {
                index += 1
                let column: AttributedTextView = AttributedTextView(flexible: true)
                column.font = UIFont.font(pointSizeStyle: .body2)
                if index != 0 {
                    column.defaultTextAlignment = .center
                }
                column.applyTextVOs([textVO])
                rowVO.addCell( index,  column )
            }

            rowVOs.append(rowVO)
        }

        adjustmentForVC = ListVC(columnVOs: columnVOs, rowVOs: rowVOs)
        adjustmentForVC!.allowHorizontalScrolling = true
        adjustmentForVC!.showMe(parentController: self, parentView: detailsRow!.mainContentView)
        let topConstraint:NSLayoutConstraint = NSLayoutConstraint(item: adjustmentForVC!.appView, attribute: .top, relatedBy: .equal, toItem: titleTextView, attribute: .bottom, multiplier: 1, constant: 0)
        adjustmentForVC!.appView.addAppConstraint(topConstraint, detailsRow!.mainContentView)
        adjustmentForVC!.appView.removeAppConstraint( .bottom )
        detailsRow!.mainContentView.layoutIfNeeded()
        detailsRow!.expansionHeight = adjustmentForVC!.appView.y + adjustmentForVC!.appView.height

        return adjustmentForVC!.appView
    }

    func applyExemption(toItem:AppView) {

        if standingListVO!.exemptionRowVOs.count == 0 {
            return
        }

        let titleTextView: AttributedTextView = AttributedTextView(flexible: true)
        titleTextView.applyTextVOs(createHeaderTextVOs("ineligible".localized))

        detailsRow!.mainContentView.addSubview(titleTextView)
        let titleTopConstraint:NSLayoutConstraint = NSLayoutConstraint(item: titleTextView, attribute: .top, relatedBy: .equal, toItem: toItem, attribute: .bottom, multiplier: 1, constant: 0)
        let titleLeadingConstraint:NSLayoutConstraint =  NSLayoutConstraint(item: titleTextView, attribute: .leading, relatedBy: .equal, toItem: detailsRow!.mainContentView, attribute: .leading, multiplier: 1, constant: 0)
        let titleTrailingConstraint:NSLayoutConstraint = NSLayoutConstraint(item: titleTextView, attribute: .trailing, relatedBy: .equal, toItem: detailsRow!.mainContentView, attribute: .trailing, multiplier: 1, constant: 0)
        titleTextView.addAppConstraint(titleTopConstraint, detailsRow!.mainContentView)
        titleTextView.addAppConstraint(titleLeadingConstraint, detailsRow!.mainContentView)
        titleTextView.addAppConstraint(titleTrailingConstraint, detailsRow!.mainContentView)


        var columnVOs:[ListColumnVO] = []
        var rowVOs:[ListRowVO] = []
        let headerRowVO:ListRowVO = ListRowVO()

        var index:Int = -1;
        for textVO:TextVO in standingListVO!.exemptionHeaderVOs {
            index += 1
            let columnVO:ListColumnVO = ListColumnVO(index)
            columnVOs.append(columnVO)
            let headerColumn: AttributedTextView = AttributedTextView(flexible: true)
            if index != 0 {
                headerColumn.defaultTextAlignment = .center
            }
            headerColumn.applyTextVOs(viewed.viewHelper.cloneTextVOs([textVO], withAttributes: [.bold:NSNull()]))
            headerRowVO.addCell( index,  headerColumn )
        }

        rowVOs.append(headerRowVO)

        for textVOs:[TextVO] in standingListVO!.exemptionRowVOs {
            let rowVO:ListRowVO = ListRowVO()

            index = -1;
            for textVO:TextVO in textVOs {
                index += 1
                let column: AttributedTextView = AttributedTextView(flexible: true)
                column.font = UIFont.font(pointSizeStyle: .body2)
                if index != 0 {
                    column.defaultTextAlignment = .center
                }
                column.applyTextVOs([textVO])
                rowVO.addCell( index,  column )
            }

            rowVOs.append(rowVO)
        }

        exemptionVC = ListVC(columnVOs: columnVOs, rowVOs: rowVOs)
        exemptionVC!.showMe(parentController: self, parentView: detailsRow!.mainContentView)
        let topConstraint:NSLayoutConstraint = NSLayoutConstraint(item: exemptionVC!.appView, attribute: .top, relatedBy: .equal, toItem: titleTextView, attribute: .bottom, multiplier: 1, constant: 0)
        exemptionVC!.appView.addAppConstraint(topConstraint, detailsRow!.mainContentView)
        exemptionVC!.appView.removeAppConstraint( .bottom )
        detailsRow!.mainContentView.layoutIfNeeded()
        detailsRow!.expansionHeight = exemptionVC!.appView.y + exemptionVC!.appView.height
    }

    func createHeaderTextVOs( _ text:String ) -> [TextVO] {
        return [TextVO(text, attributes: [.bold:NSNull()])]
    }

    func destroySubLists() {
        keysVC?.destroy()
        placeEventVC?.destroy()
        adjustmentAgainstVC?.destroy()
        adjustmentForVC?.destroy()
        exemptionVC?.destroy()
        keysVC = nil
        placeEventVC = nil
        adjustmentAgainstVC = nil
        adjustmentForVC = nil
        exemptionVC = nil
    }

    override func destroy() {
        destroySubLists()
        super.destroy()
    }

}
