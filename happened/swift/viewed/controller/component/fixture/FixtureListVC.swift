import UIKit

class FixtureListVC: ListVC {

    private var viewed: Viewed { return Viewed.instance }

    override func initialize() {
        super.initialize()

        allowHorizontalScrolling = true
    }

    override func applyGeneralView() {
        super.applyGeneralView()
        leftRowPadding = viewed.viewConfig.topLeftMargin.x
        rightRowPadding = viewed.viewConfig.bottomRightMargin.x
    }

    func applyCurrentState( pageVO: FixturePageVO) {

        var columnVOs:[ListColumnVO] = []
        var rowVOs:[ListRowVO] = []
        for fixtureColumnVO:FixtureColumnVO in pageVO.columnVOs {
            let columnVO:ListColumnVO = ListColumnVO(fixtureColumnVO.columnType.rawValue)
            columnVO.attributes = fixtureColumnVO.attributes
            columnVOs.append(columnVO)
        }
        var index:Int = -1
        for fixtureRowVO:FixtureRowVO in pageVO.rowVOs {
            index += 1
            let rowVO:ListRowVO = ListRowVO()
            rowVO.attributes = fixtureRowVO.attributes
            let even:Bool = floor( Double(CGFloat(index)) / 2 ) == Double(CGFloat(index)) / 2
            if even {
                rowVO.attributes[.backgroundColor] = viewed.viewConfig.colorGrey1
            }
            for fixtureColumnVO:FixtureColumnVO in pageVO.columnVOs {
                let textVOs:[TextVO] = fixtureRowVO.columnTextVOs[fixtureColumnVO.columnType] != nil ? fixtureRowVO.columnTextVOs[fixtureColumnVO.columnType]! : [TextVO("")]
                let textView: AttributedTextView = AttributedTextView(flexible: true)
                textView.applyTextVOs(textVOs, additionalAttributes: [.pointSizeStyle:UIFont.TextStyle.body2])
                rowVO.addCell( fixtureColumnVO.columnType.rawValue, textView )
            }
            rowVOs.append(rowVO)
        }

        self.columnVOs = columnVOs
        self.rowVOs = rowVOs

        createView()
    }
}
