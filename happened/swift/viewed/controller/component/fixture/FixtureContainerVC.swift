import UIKit

class FixtureContainerVC: ComponentVC {

    private var app: App { return App.instance }

    var _mainContentVC:FixtureListVC?

    func applyCurrentState(pageVO: FixturePageVO) {

        scrollView.resetScroll()
        mainContentVC.applyCurrentState(pageVO: pageVO )

    }

    var mainContentVC: FixtureListVC {
        if _mainContentVC != nil {
            return _mainContentVC!
        }
        _mainContentVC = FixtureListVC()
        _mainContentVC!.showMe(parentController: self, parentView: scrollView.innerContentView)

        return _mainContentVC!
    }

    override func createAppView() -> ScrollContainerView {
        return ScrollContainerView(horizontal: true)
    }

    var scrollView: ScrollContainerView {
        return view as! ScrollContainerView
    }

    override func destroy() {
        _mainContentVC?.destroy()
        super.destroy()
    }
}
