import UIKit

class SettingsListVC:ListVC {

    var localization: Localization { return Localization.instance }
    var preference: Preference { return Preference.instance }
    private var viewed: Viewed { return Viewed.instance }

    enum Columns:Int {
        case team = 0,
             following = 1,
             receiveAlerts = 2
    }

    static let FOLLOWING_VALUE_CHANGED_REQUEST:String = NSUUID().uuidString
    static let RECEIVE_ALERTS_VALUE_CHANGED_REQUEST:String = NSUUID().uuidString

    var valueObserverID:String? = nil
    var followingSwitches:[Int: AppSwitch] = [:]
    var receiveAlertsSwitches:[Int: AppSwitch] = [:]
    var teamSubscriptions:[Int:Bool] = [:]


    override func initialize() {
        super.initialize()
        applyObserverID()
    }

    override func applyGeneralView() {
        super.applyGeneralView()

        bottomRowPadding = 8
        leftRowPadding = viewed.viewConfig.topLeftMargin.x
        rightRowPadding = viewed.viewConfig.bottomRightMargin.x
    }

    func removeAll() {

        followingSwitches = [:]
        receiveAlertsSwitches = [:]
        self.teamSubscriptions = [:]

        self.columnVOs = []
        self.rowVOs = []

        appView.removeAll()
    }

    func applyCurrentState( teamSubscriptions:[Int:Bool] ) {

        followingSwitches = [:]
        receiveAlertsSwitches = [:]
        self.teamSubscriptions = teamSubscriptions

        var teamNames:[Int:String] = [:]
        var teamRefsArray:[Int] = []
        for teamRef:Int in teamSubscriptions.keys {
            let teamName:String = localization.localizationHelper.getTeamName(teamRef, seasonRef: nil)
            teamNames[teamRef] = teamName
            teamRefsArray.append(teamRef)
        }

        teamRefsArray.sort {
            let aTeamName:String = teamNames[$0]!
            let bTeamName:String = teamNames[$1]!
            if aTeamName > bTeamName {
                return false
            }
            if aTeamName < bTeamName {
                return true
            }
            return false
        }

        let columnVOs:[ListColumnVO] = [ListColumnVO(Columns.team.rawValue), ListColumnVO(Columns.following.rawValue), ListColumnVO(Columns.receiveAlerts.rawValue)]

        let headerTeamColumn: AttributedTextView = AttributedTextView(flexible: true)
        headerTeamColumn.applyTextVOs([TextVO("team".localized, attributes: [.bold:NSNull()])])

        let headerFollowingColumn: AttributedTextView = AttributedTextView(flexible: true)
        headerFollowingColumn.applyTextVOs([TextVO("following".localized, attributes: [.bold:NSNull()])], withTextAlignment: .center)

        let headerReceiveAlertsColumn: AttributedTextView = AttributedTextView(flexible: true)
        headerReceiveAlertsColumn.applyTextVOs([TextVO("receive_alerts".localized, attributes: [.bold:NSNull()])], withTextAlignment: .center)

        let headerRowVO:ListRowVO = ListRowVO()
        headerRowVO.addListCell(Columns.team.rawValue, headerTeamColumn)
        headerRowVO.addListCell(Columns.following.rawValue, headerFollowingColumn, xPos: .centerX)
        headerRowVO.addListCell(Columns.receiveAlerts.rawValue, headerReceiveAlertsColumn, xPos: .centerX)

        var rowVOs:[ListRowVO] = [ headerRowVO ]

        for teamRef:Int in teamRefsArray {
            let teamName:String = teamNames[teamRef]!

            let teamColumn: AttributedTextView = AttributedTextView(flexible: true)
            teamColumn.applyTextVOs([TextVO(teamName)])

            let followingColumn: AppSwitch = AppSwitch()
            followingColumn.addObserverID(observerID: observerID!, requestType: SettingsListVC.FOLLOWING_VALUE_CHANGED_REQUEST, action: teamRef)

            let receiveAlertsColumn: AppSwitch = AppSwitch()
            receiveAlertsColumn.addObserverID(observerID: observerID!, requestType: SettingsListVC.RECEIVE_ALERTS_VALUE_CHANGED_REQUEST, action: teamRef)

            followingSwitches[teamRef] = followingColumn
            receiveAlertsSwitches[teamRef] = receiveAlertsColumn


            let rowVO:ListRowVO = ListRowVO()
            rowVO.addListCell(Columns.team.rawValue, teamColumn, yPos: .centerY)
            rowVO.addListCell(Columns.following.rawValue, followingColumn, xPos: .centerX, yPos: .centerY)
            rowVO.addListCell(Columns.receiveAlerts.rawValue, receiveAlertsColumn, xPos: .centerX, yPos: .centerY)
            rowVOs.append(rowVO)
        }

        self.columnVOs = columnVOs
        self.rowVOs = rowVOs

        createView()

        for teamRef:Int in teamRefsArray {
            followingSwitches[teamRef]!.isOn = true
            receiveAlertsSwitches[teamRef]!.isOn = teamSubscriptions[teamRef]!
        }
    }

    override func applyNotificationRequest(requestType:String?=nil, action:Any?=nil, value:Any?=nil) {
        if requestType != nil {
            switch requestType! {
            case SettingsListVC.FOLLOWING_VALUE_CHANGED_REQUEST:
                if let teamRef:Int = action as? Int {
                    if let newValue:Bool = value as? Bool {
                        followingValueChanged(teamRef, newValue: newValue)
                    }
                }
            case SettingsListVC.RECEIVE_ALERTS_VALUE_CHANGED_REQUEST:
                if let teamRef:Int = action as? Int {
                    if let newValue:Bool = value as? Bool {
                        receiveAlertsValueChanged(teamRef, newValue: newValue)
                    }
                }
                break
            default:
                break
            }
        }
        super.applyNotificationRequest(requestType: requestType, action: action, value: value)
    }

    func followingValueChanged(_ teamRef:Int, newValue:Bool) {
        if let receiveAlertsSwitch:AppSwitch = receiveAlertsSwitches[teamRef] {
            receiveAlertsSwitch.isOn = false
            receiveAlertsSwitch.isEnabled = newValue
        }
        let teamSubscriptions:[Int:Bool] = preference.preferenceHelper.getTeamSubscriptions()
        let wasSubscribed:Bool = teamSubscriptions[teamRef] != nil && teamSubscriptions[teamRef]!
        preference.preferenceHelper.modifyTeams(teamRef, selected: newValue, subscribed: false)
        if wasSubscribed && !newValue {
            viewed.requirePushAuthorizationViewCommand().command(viewController: self, on: false, subscriptionsDidChange: true, allowNotSupportedError: false)
        }
    }

    func receiveAlertsValueChanged(_ teamRef:Int, newValue:Bool) {
        preference.preferenceHelper.modifyTeams(teamRef, selected: true, subscribed: newValue)
        viewed.requirePushAuthorizationViewCommand().command(viewController: self, on: newValue, subscriptionsDidChange: true)
    }

    override func destroy(){
        followingSwitches = [:]
        receiveAlertsSwitches = [:]
        teamSubscriptions = [:]
        super.destroy()
    }
}
