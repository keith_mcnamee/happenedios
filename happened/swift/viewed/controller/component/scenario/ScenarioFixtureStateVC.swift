import UIKit

class ScenarioFixtureStateVC: FixtureFeedStateVC {

    convenience init(pageVOs:[FixturePageVO], seasonRef:Int ) {

        self.init(nibName: nil, bundle: nil)

        let seasonBalancingVO:SeasonBalancingVO = balancing.balancingModel.seasonVOs[seasonRef]!
        let seasonGroupRef:Int = balancing.balancingHelper.getBestSeasonGroupRef(seasonBalancingVO.seasonGroupRefs!)!

        self.feedVO = FeedViewVO(feedType: .fixture, feedSource: .scenario, seasonGroupRef: seasonGroupRef, isHappenedState: .scenario)
        profileVO.pageVOs = pageVOs
    }

    override func resetProfile(){
        profileVO.currentPageIndex = 0
        profileVO.prevPageVO = nil
    }

    override func initializeIsSpecifiedTeamsSegment() {
        feedStateView.isSpecifiedTeamsSegmentHidden = true
    }

    override func initializeFeed( fromDataReceived:Bool = false  ) {
        feedStateView.filterBtnHidden = true
        feedStateView.pageControlHidden = true
        resetProfile()
        applyInitializeFeed()
    }

    override func applyInitializeFeed() {
        let hasNoEntries: Bool = profileVO.pageVOs.count == 0
        if hasNoEntries {
            addNoDataPage()
        }

        displayFeed()
    }

    override func showCurrentPage() {

        let currentPageVO:FeedPageVO = getCurrentPageVO()

        if profileVO.prevPageVO != nil && currentPageVO.pageIndex == profileVO.prevPageVO!.pageIndex {
            return
        }
        hidePageContent()

        if currentPageVO.hasNoEntries{
            showNoContentPage()
        } else {
            displayCurrentPageContent()
        }
    }

    override func defineTableColumns() {
        //
    }

    override var noContentTextVOs:[TextVO] {
        return [TextVO("no_fixtures".localized)]
    }

}
