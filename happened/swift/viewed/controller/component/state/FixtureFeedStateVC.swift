import UIKit

class FixtureFeedStateVC: FeedStateVC {

    enum IsSpecifiedIndexes:Int {
        case myTeams = 0,
             allTeams = 1
    }

    var columnOrder:[FixtureColumnType]?

    var fixtureContainerVCs:[FeedSource:[FilteredState:[Int:FixtureContainerVC]]] = [:]

    override func requestData(entriesFrom:Int?=nil) {
        server.fixtureFeedServerCommand().command(feedSource: feedVO!.feedSource, seasonGroupRef: feedVO!.seasonGroupRef, isHappenedState: feedVO!.isHappenedState, listingRefs: requestListingRefs, teamRefs: requestTeamRefs, filterVO: feedVO!.filterVO, entriesFrom: entriesFrom, entriesSpan:entriesPerPage)
    }

    override var entriesPerPage:Int {
        return viewed.viewConfig.visibleFixtureSpan
    }

    override func createPageVO( pageIndex:Int ) -> FixturePageVO {
        return FixturePageVO(pageIndex: pageIndex)
    }

    override func initialize() {
        super.initialize()
        defineTableColumns()
    }

    var fixtureProfileVO:FixtureFeedStateProfileVO {
        return profileVO as! FixtureFeedStateProfileVO
    }

    override var createProfileVO:FeedStateProfileVO {
        return FixtureFeedStateProfileVO()
    }

    func defineTableColumns() {
        columnOrder = [
                .dateTime,
                .homeTeam,
                .score,
                .awayTeam,
                .competition
        ]
        /*columnOrder = [
                .dateTime,
                .score
        ]*/
    }

    override func resetProfile(){
        super.resetProfile()
        fixtureProfileVO.fixtureVOs = nil
        let filteredState:FilteredState = feedVO!.isFiltered ? .filtered : .unfiltered
        if let byFeedSource:[FilteredState:[Int:FixtureContainerVC]] = fixtureContainerVCs[feedVO!.feedSource] {
            if let byFilteredState:[Int:FixtureContainerVC] = byFeedSource[filteredState] {
                for fixtureContainerVC:FixtureContainerVC in byFilteredState.values {
                    fixtureContainerVC.destroy()
                }
                fixtureContainerVCs[feedVO!.feedSource]![filteredState] = [:]
            }
        }
    }

    override func applyLocalToFeed() {
        if !haveDataLocally {
            let seasonRefs:[Int:NSNull] = balancing.balancingHelper.getAllSeasonRefs(seasonGroupRef: feedVO!.seasonGroupRef)
            let listingRefs:[Int:NSNull] = relevantListingRefs
            if happen.happenedHelper.haveCompleteListingSeasons(seasonRefs, listingRefs) {
                let happenedOnly: Bool = feedVO!.isHappenedState == .happenedOnly
                let upcomingOnly: Bool = feedVO!.isHappenedState == .upcomingOnly


                let focusOnTeams:[Int:NSNull]? = feedVO!.feedSource == .specificTeam ? preference.preferenceHelper.getTeamRefs() : nil
                let sortedFixtureVOs:[FixtureVO] = happen.happenedHelper.storedFixtureList(seasonRefs: seasonRefs, listingRefs: listingRefs, includeHappened: !upcomingOnly, includeUpcoming: !happenedOnly, descendingDate: !upcomingOnly, focusOnTeams:focusOnTeams)
                feedVO!.amountAvailable = sortedFixtureVOs.count
                var index:Int = -1
                feedVO!.entryVOs = []
                fixtureProfileVO.fixtureVOs = [:]
                for fixtureVO:FixtureVO in sortedFixtureVOs {
                    index += 1
                    feedVO!.entryVOs.append(FeedEntryVO(index, ref: fixtureVO.ref, requested: true))
                    fixtureProfileVO.fixtureVOs![fixtureVO.ref] = fixtureVO
                }
            }
        }
    }

    override func displayCurrentPageContent() {

        let filteredState:FilteredState = feedVO!.isFiltered ? .filtered : .unfiltered
        if fixtureContainerVCs[feedVO!.feedSource] == nil {
            fixtureContainerVCs[feedVO!.feedSource] = [:]
        }
        if fixtureContainerVCs[feedVO!.feedSource]![filteredState] == nil {
            fixtureContainerVCs[feedVO!.feedSource]![filteredState] = [:]
        }
        var currentVC:FixtureContainerVC? = fixtureContainerVCs[feedVO!.feedSource]![filteredState]![profileVO.currentPageIndex]
        if currentVC == nil {
            currentVC = FixtureContainerVC()
            fixtureContainerVCs[feedVO!.feedSource]![filteredState]![profileVO.currentPageIndex] = currentVC!
            let currentPageVO: FixturePageVO = getCurrentPageVO() as! FixturePageVO
            if !currentPageVO.prepared {
                viewed.prepareFixturePageService().preparePageVO(pageVO: currentPageVO, feedEntryVOs: feedVO!.entryVOs, fixtureVOs: useFixtureVOs, entriesPerPage: entriesPerPage, columnTypes: columnOrder!, focusOnTeams: focusOnTeams, singleTeamOnly: singleTeamOnly, navigationObserverID: navigationObserverID!)
            }
            currentVC!.applyCurrentState(pageVO: currentPageVO )
        }

        currentVC!.showMe(parentController: self, parentView: feedStateView.mainContentView)


        super.displayCurrentPageContent()
    }

    override func addTargets() {
        feedStateView.isSpecifiedTeamsSegment.addTarget(self, action: #selector(isSpecifiedTeamsChanged(_:)), for: .valueChanged)
        super.addTargets()
    }

    override func removeTargets() {
        feedStateView.isSpecifiedTeamsSegment.removeTarget(nil, action: nil, for: .allEvents)
        super.removeTargets()
    }

    override var noContentTextVOs:[TextVO] {
        let haveTeams:Bool = preference.preferenceHelper.getTeamRefs().count > 0
        let textID:String = !isTeamFeed ? isLatestSeasonGroup ? "no_this_year_game_entries" : "no_previous_year_game_entries" : haveTeams ? "no_teams_some_game_entries" : "no_teams_none_game_entries"
        let replaceWith:[String:String] = [
                "this_state": feedVO!.isHappenedState != .upcomingOnly ? "results_lower".localized : "fixtures_lower".localized,
                "alt_state": feedVO!.isHappenedState != .upcomingOnly ? "fixtures_lower".localized : "results_lower".localized,
                "competitions": "competitions_lower".localized,
                "more_teams": "more_teams_lower".localized,
                "find": "find".localized,
                "year": localization.localizationHelper.getSeasonGroupName( feedVO!.seasonGroupRef )
        ]
        let observerIDs:[String:String]  = [
                "alt_state": typeObserverID!,
                "competitions": TabBarController.OBSERVER_ID,
                "more_teams": TabBarController.OBSERVER_ID,
                "find": TabBarController.OBSERVER_ID
        ]
        let requestTypes:[String:String]  = [
                "alt_state": AppConstant.CHANGE_STATE_REQUEST,
                "competitions": AppConstant.SHOW_TAB_REQUEST,
                "more_teams": AppConstant.SHOW_TAB_REQUEST,
                "find": AppConstant.SHOW_TAB_REQUEST
        ]
        let actions:[String:Any]  = [
                "alt_state": feedVO!.isHappenedState != .upcomingOnly ? FeedTypeVC.StateIndexes.upcoming.rawValue : FeedTypeVC.StateIndexes.happened.rawValue,
                "competitions": TabIdentifier.findTeams,
                "more_teams": TabIdentifier.findTeams,
                "find": TabIdentifier.findTeams
        ]

        return localization.localizationHelper.localizeVOs(
                textID,
                replaceWith,
                observerIDs: observerIDs,
                requestTypes: requestTypes,
                actions: actions,
                localizationType: LocalizationConstant.NATIVE_TYPE
        )!
    }

    override func initializeIsSpecifiedTeamsSegment() {
        if !feedVO!.isTeamFeed {
            feedStateView.isSpecifiedTeamsSegmentHidden = true
            return
        }
        feedStateView.isSpecifiedTeamsSegment.removeAllSegments()
        feedStateView.isSpecifiedTeamsSegment.insertSegment(withTitle: myTeamsTitle, at: IsSpecifiedIndexes.myTeams.rawValue, animated: false)
        feedStateView.isSpecifiedTeamsSegment.insertSegment(withTitle: "all_teams".localized, at: IsSpecifiedIndexes.allTeams.rawValue, animated: false)
        var selectedSegmentIndex:Int = IsSpecifiedIndexes.myTeams.rawValue
        if feedVO!.feedSource == .allTeam {
            selectedSegmentIndex = IsSpecifiedIndexes.allTeams.rawValue
        }
        feedStateView.isSpecifiedTeamsSegment.selectedSegmentIndex = selectedSegmentIndex
    }

    var myTeamsTitle:String {
        return "my_teams_only".localized
    }

    @objc func isSpecifiedTeamsChanged(_ sender: Any?=nil) {
        let newSegmentIndex:Int = feedStateView.isSpecifiedTeamsSegment.selectedSegmentIndex
        var newSource:FeedSource? = nil
        if newSegmentIndex == IsSpecifiedIndexes.myTeams.rawValue && feedVO!.feedSource != .specificTeam {
            newSource = .specificTeam
        } else if newSegmentIndex == IsSpecifiedIndexes.allTeams.rawValue && feedVO!.feedSource != .allTeam {
            newSource = .allTeam
        }
        if newSource != nil {
            feedVO = viewed.feedViewHelper.getFeedVO(feedType: feedVO!.feedType, feedSource: newSource!, seasonGroupRef: feedVO!.seasonGroupRef, isHappenedState: feedVO!.isHappenedState, filterVO: nil, createIfNotMatched: true)!
            if !haveProfileVO() {
                initializeFeed()
            } else {
                displayFeed()
            }
        }
    }

    var isSpecifiedTeamsValue:Bool{
        return feedStateView.isSpecifiedTeamsSegment.selectedSegmentIndex == IsSpecifiedIndexes.myTeams.rawValue
    }

    var focusOnTeams:[Int:NSNull] {
        return feedVO!.feedSource == .allTeam ? preference.preferenceHelper.getTeamRefs() : [:]
    }

    var singleTeamOnly:Bool {
        return feedVO!.feedSource == .specificTeam && preference.preferenceHelper.getTeamRefs().count <= 1
    }

    var useFixtureVOs:[Int:FixtureVO] {
        return fixtureProfileVO.fixtureVOs != nil ? fixtureProfileVO.fixtureVOs! : happen.happenedModel.activeFixtureVOs
    }

    override var haveDataLocally:Bool {
        return fixtureProfileVO.fixtureVOs != nil
    }

    func destroyFixtureContainers() {
        for byFeedSource:[FilteredState:[Int:FixtureContainerVC]] in fixtureContainerVCs.values {
            for byFilteredState:[Int:FixtureContainerVC] in byFeedSource.values {
                for fixtureContainerVC:FixtureContainerVC in byFilteredState.values {
                    fixtureContainerVC.destroy()
                }
            }
        }
        fixtureContainerVCs = [:]
    }

    override func destroy() {
        destroyFixtureContainers()
        super.destroy()
    }
}
