import UIKit

class EventFeedStateVC: FeedStateVC {

    override func requestData(entriesFrom:Int?=nil) {
        server.eventFeedServerCommand().command(feedSource: feedVO!.feedSource, seasonGroupRef: feedVO!.seasonGroupRef, isHappenedState: feedVO!.isHappenedState, listingRefs: requestListingRefs, teamRefs: requestTeamRefs, filterVO:eventFilerVO, entriesFrom:entriesFrom, entriesSpan:entriesPerPage)
    }

    var eventFilerVO: EventFeedFilterVO? {
        return feedVO!.filterVO as? EventFeedFilterVO
    }

    override func createPageVO( pageIndex:Int ) -> FeedPageVO {
        return EventPageVO(pageIndex: pageIndex)
    }

    override var entriesPerPage:Int {
        return viewed.viewConfig.visibleEventScenarioSpan
    }

    override var birthFilterVC:FeedFilterVC {
        return EventFeedFilterVC()
    }

    override func displayCurrentPageContent() {
        let currentPageVO:EventPageVO = getCurrentPageVO() as! EventPageVO

        if !currentPageVO.prepared {
            viewed.prepareEventPageService().preparePageVO( pageVO: currentPageVO, entriesPerPage:entriesPerPage, feedEntryVOs: feedVO!.entryVOs)
        }

        var topConstraint:NSLayoutConstraint? = nil
        var trailingConstraint:NSLayoutConstraint? = nil
        var leadingConstraint:NSLayoutConstraint? = nil

        var previousItem:UIView = feedStateView.mainContentView
        var previousBaseAttribute:NSLayoutConstraint.Attribute = .top

        let leftMargin:CGFloat = viewed.viewConfig.topLeftMargin.x
        let rightMargin:CGFloat = viewed.viewConfig.bottomRightMargin.x

        var dateIndex:Int = -1
        for dateFeedVO:DateFeedVO in currentPageVO.dateFeedVOs {
            dateIndex += 1
            let dateConstant:CGFloat = dateIndex == 0 ? 0 : CGFloat( 10 ).deviceValue
            let dateTextView:AttributedTextView = AttributedTextView()
            dateTextView.applyTextVOs(dateFeedVO.textVOs, additionalAttributes: [.pointSizeStyle:UIFont.TextStyle.title3])

            feedStateView.mainContentView.addSubview(dateTextView)

            topConstraint = NSLayoutConstraint(item: dateTextView, attribute: .top, relatedBy: .equal, toItem: previousItem, attribute: previousBaseAttribute, multiplier: 1, constant: dateConstant)
            leadingConstraint = NSLayoutConstraint(item: dateTextView, attribute: .leading, relatedBy: .equal, toItem: feedStateView.mainContentView, attribute: .leading, multiplier: 1, constant: leftMargin)
            trailingConstraint = NSLayoutConstraint(item: dateTextView, attribute: .trailing, relatedBy: .equal, toItem: feedStateView.mainContentView, attribute: .trailing, multiplier: 1, constant: -rightMargin)
            dateTextView.addAppConstraint(topConstraint!, feedStateView.mainContentView)
            dateTextView.addAppConstraint(leadingConstraint!, feedStateView.mainContentView)
            dateTextView.addAppConstraint(trailingConstraint!, feedStateView.mainContentView)

            previousItem = dateTextView
            previousBaseAttribute = .bottom

            var timeIndex:Int = -1
            for timeFeedVO:TimeFeedVO in dateFeedVO.childVOs {

                timeIndex += 1
                let timeConstant:CGFloat = timeIndex == 0 ? 0 : CGFloat( 10 ).deviceValue
                let timeTextView:AttributedTextView = AttributedTextView()
                timeTextView.applyTextVOs(timeFeedVO.textVOs)
                feedStateView.mainContentView.addSubview(timeTextView)

                topConstraint = NSLayoutConstraint(item: timeTextView, attribute: .top, relatedBy: .equal, toItem: previousItem, attribute: previousBaseAttribute, multiplier: 1, constant: timeConstant)
                leadingConstraint = NSLayoutConstraint(item: timeTextView, attribute: .leading, relatedBy: .equal, toItem: feedStateView.mainContentView, attribute: .leading, multiplier: 1, constant: leftMargin)
                trailingConstraint = NSLayoutConstraint(item: timeTextView, attribute: .trailing, relatedBy: .equal, toItem: feedStateView.mainContentView, attribute: .trailing, multiplier: 1, constant: -rightMargin)
                timeTextView.addAppConstraint(topConstraint!, feedStateView.mainContentView)
                timeTextView.addAppConstraint(leadingConstraint!, feedStateView.mainContentView)
                timeTextView.addAppConstraint(trailingConstraint!, feedStateView.mainContentView)

                previousItem = timeTextView

                var competitionIndex:Int = -1
                for competitionFeedVO:CompetitionFeedVO in timeFeedVO.childVOs {
                    competitionIndex += 1
                    let competitionConstant:CGFloat = competitionIndex == 0 ? 0 : CGFloat( 10 ).deviceValue
                    let competitionTextView: AttributedTextView = AttributedTextView()
                    competitionTextView.applyTextVOs(viewed.viewHelper.cloneTextVOs(competitionFeedVO.textVOs, withAttributes:[.observerID:navigationObserverID!], cloneIfAttributes:[.requestType, .action]), additionalAttributes: [.pointSizeStyle:UIFont.TextStyle.title2])
                    feedStateView.mainContentView.addSubview(competitionTextView)

                    topConstraint = NSLayoutConstraint(item: competitionTextView, attribute: .top, relatedBy: .equal, toItem: previousItem, attribute: previousBaseAttribute, multiplier: 1, constant: competitionConstant)
                    leadingConstraint = NSLayoutConstraint(item: competitionTextView, attribute: .leading, relatedBy: .equal, toItem: feedStateView.mainContentView, attribute: .leading, multiplier: 1, constant: leftMargin)
                    trailingConstraint = NSLayoutConstraint(item: competitionTextView, attribute: .trailing, relatedBy: .equal, toItem: feedStateView.mainContentView, attribute: .trailing, multiplier: 1, constant: -rightMargin)
                    competitionTextView.addAppConstraint(topConstraint!, feedStateView.mainContentView)
                    competitionTextView.addAppConstraint(leadingConstraint!, feedStateView.mainContentView)
                    competitionTextView.addAppConstraint(trailingConstraint!, feedStateView.mainContentView)

                    previousItem = competitionTextView

                    var eventIndex:Int = -1
                    for eventFeedVO:EventFeedVO in competitionFeedVO.childVOs {
                        eventIndex += 1
                        let eventConstant:CGFloat = eventIndex == 0 ? CGFloat( 10 ).deviceValue : CGFloat( 10 ).deviceValue
                        let imageView:ImageView = ImageView(image: UIImage(named: "bullet-point.png")!)

                        feedStateView.mainContentView.addSubview(imageView)

                        topConstraint = NSLayoutConstraint(item: imageView, attribute: .top, relatedBy: .equal, toItem: previousItem, attribute: previousBaseAttribute, multiplier: 1, constant: eventConstant)
                        leadingConstraint = NSLayoutConstraint(item: imageView, attribute: .leading, relatedBy: .equal, toItem: feedStateView.mainContentView, attribute: .leading, multiplier: 1, constant: (leftMargin + CGFloat( 4 ).deviceValue))
                        let widthConstraint:NSLayoutConstraint = NSLayoutConstraint(item: imageView, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, deviceConstant: 16)
                        let heightConstraint:NSLayoutConstraint = NSLayoutConstraint(item: imageView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, deviceConstant: 16)
                        imageView.addAppConstraint(topConstraint!, feedStateView.mainContentView)
                        imageView.addAppConstraint(leadingConstraint!, feedStateView.mainContentView)
                        imageView.addAppConstraint(widthConstraint)
                        imageView.addAppConstraint(heightConstraint)

                        imageView.setContentHuggingPriority(UILayoutPriority.defaultHigh, for: .horizontal)
                        imageView.setContentHuggingPriority(UILayoutPriority.defaultHigh, for: .vertical)



                        let viewButton:Button = BlueButton()
                        viewButton.setTitle("view".localized, for: .normal)

                        feedStateView.mainContentView.addSubview(viewButton)

                        topConstraint = NSLayoutConstraint(item: viewButton, attribute: .top, relatedBy: .equal, toItem: previousItem, attribute: previousBaseAttribute, multiplier: 1, deviceConstant: 8)
                        trailingConstraint = NSLayoutConstraint(item: viewButton, attribute: .trailing, relatedBy: .equal, toItem: feedStateView.mainContentView, attribute: .trailing, multiplier: 1, constant: -rightMargin)
                        viewButton.addAppConstraint(topConstraint!, feedStateView.mainContentView)
                        viewButton.addAppConstraint(trailingConstraint!, feedStateView.mainContentView)

                        viewButton.setContentHuggingPriority(UILayoutPriority.defaultHigh, for: .horizontal)

                        viewButton.addObserverID(observerID: navigationObserverID!, requestType: AppConstant.SHOW_EVENT_SCENARIO_REQUEST, action: eventFeedVO.eventScenarioRef)


                        let eventTextView: AttributedTextView = AttributedTextView()
                        eventTextView.textContainer.maximumNumberOfLines = 0
                        eventTextView.applyTextVOs(viewed.viewHelper.cloneTextVOs(eventFeedVO.textVOs, withAttributes:[.observerID:navigationObserverID!], cloneIfAttributes:[.requestType, .action]))
                        feedStateView.mainContentView.addSubview(eventTextView)

                        topConstraint = NSLayoutConstraint(item: eventTextView, attribute: .top, relatedBy: .equal, toItem: previousItem, attribute: previousBaseAttribute, multiplier: 1, constant: 0)
                        leadingConstraint = NSLayoutConstraint(item: eventTextView, attribute: .leading, relatedBy: .equal, toItem: imageView, attribute: .trailing, multiplier: 1, deviceConstant: 8)
                        trailingConstraint = NSLayoutConstraint(item: eventTextView, attribute: .trailing, relatedBy: .equal, toItem: viewButton, attribute: .leading, multiplier: 1, deviceConstant: -8)
                        eventTextView.addAppConstraint(topConstraint!, feedStateView.mainContentView)
                        eventTextView.addAppConstraint(leadingConstraint!, feedStateView.mainContentView)
                        eventTextView.addAppConstraint(trailingConstraint!, feedStateView.mainContentView)

                        eventTextView.setContentCompressionResistancePriority(UILayoutPriority.defaultLow, for: .horizontal)

                        previousItem = eventTextView
                    }
                }
            }
        }
        let lastItem:UIView? = previousItem != feedStateView.mainContentView ? previousItem : nil

        if lastItem != nil {
            let bottomConstraint:NSLayoutConstraint = NSLayoutConstraint(item: lastItem!, attribute: .bottom, relatedBy: .equal, toItem: feedStateView.mainContentView, attribute: .bottom, multiplier: 1, constant: 0)
            (lastItem as! ExtAppView).addAppConstraint(bottomConstraint, feedStateView.mainContentView, nil)
        }

        feedStateView.mainContentView.layoutIfNeeded()

        super.displayCurrentPageContent()
    }

    override var noContentTextVOs:[TextVO] {
        let haveTeams:Bool = preference.preferenceHelper.getTeamRefs().count > 0
        let textID:String = !isTeamFeed ? isLatestSeasonGroup ? "no_this_year_event_entries" : "no_previous_year_event_entries" : haveTeams ? "no_teams_some_event_entries" : "no_teams_none_event_entries"
        let replaceWith:[String:String] = [
                "this_state": feedVO!.isHappenedState != .upcomingOnly ? "happened_lower".localized : "upcoming_lower".localized,
                "alt_state": feedVO!.isHappenedState != .upcomingOnly ? "upcoming_lower".localized : "happened_lower".localized,
                "competitions": "competitions_lower".localized,
                "more_teams": "more_teams_lower".localized,
                "find": "find".localized,
                "year": localization.localizationHelper.getSeasonGroupName( feedVO!.seasonGroupRef )
        ]
        let observerIDs:[String:String]  = [
                "alt_state": typeObserverID!,
                "competitions": TabBarController.OBSERVER_ID,
                "more_teams": TabBarController.OBSERVER_ID,
                "find": TabBarController.OBSERVER_ID
        ]
        let requestTypes:[String:String]  = [
                "alt_state": AppConstant.CHANGE_STATE_REQUEST,
                "competitions": AppConstant.SHOW_TAB_REQUEST,
                "more_teams": AppConstant.SHOW_TAB_REQUEST,
                "find": AppConstant.SHOW_TAB_REQUEST
        ]
        let actions:[String:Any]  = [
                "alt_state": feedVO!.isHappenedState != .upcomingOnly ? FeedTypeVC.StateIndexes.upcoming.rawValue : FeedTypeVC.StateIndexes.happened.rawValue,
                "competitions": TabIdentifier.findTeams,
                "more_teams": TabIdentifier.findTeams,
                "find": TabIdentifier.findTeams
        ]

        return localization.localizationHelper.localizeVOs(
                textID,
                replaceWith,
                observerIDs: observerIDs,
                requestTypes: requestTypes,
                actions: actions,
                localizationType: LocalizationConstant.NATIVE_TYPE
        )!
    }

    override func initializeIsSpecifiedTeamsSegment() {
        feedStateView.isSpecifiedTeamsSegment.isHidden = true
    }
}
