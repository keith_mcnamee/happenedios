import UIKit

class FeedStateVC: ComponentVC {

    var app:App { return App.instance }
    var balancing:Balancing { return Balancing.instance }
    var happen: Happen { return Happen.instance }
    var localization: Localization { return Localization.instance }
    var preference: Preference { return Preference.instance }
    var server: Server { return Server.instance }
    var viewed: Viewed { return Viewed.instance }

    var navigationObserverID:String?
    var pageObserverID:String?
    var typeObserverID:String?

    private var _filterVC: FeedFilterVC?

    var isTeamFeed:Bool = false
    var feedVO:FeedViewVO?

    var profileVOs:[FeedSource:[FilteredState:FeedStateProfileVO]] = [:]

    var forceContentPosition:CGFloat? = nil

    var validSeasonWasRequired:Bool = false
    var validListingWasRequired:Bool = false
    var validTeamWasRequired:Bool = false

    convenience init(feedType:FeedType, seasonGroupRef:Int, isHappenedState: IsHappenedState, isTeamFeed:Bool, navigationObserverID:String, pageObserverID:String, typeObserverID:String  ) {

        self.init(nibName: nil, bundle: nil)

        self.isTeamFeed = isTeamFeed
        self.feedVO = viewed.feedViewHelper.getFeedVO(feedType: feedType, feedSource: defaultFeedSource!, seasonGroupRef: seasonGroupRef, isHappenedState: isHappenedState, createIfNotMatched: true)!
        self.navigationObserverID = navigationObserverID
        self.pageObserverID = pageObserverID
        self.typeObserverID = typeObserverID
    }

    override func createAppView() -> FeedStateView {
        return FeedStateView()
    }

    var feedStateView:FeedStateView {
        return view as! FeedStateView
    }

    var profileVO:FeedStateProfileVO {
        let filteredState:FilteredState = feedVO!.isFiltered ? .filtered : .unfiltered
        if profileVOs[feedVO!.feedSource] == nil {
            profileVOs[feedVO!.feedSource] = [:]
        }
        if profileVOs[feedVO!.feedSource]![filteredState] == nil {
            profileVOs[feedVO!.feedSource]![filteredState] = createProfileVO
        }
        let vo:FeedStateProfileVO = profileVOs[feedVO!.feedSource]![filteredState]!
        vo.filterString = feedVO!.filterVO?.filterString
        return vo
    }

    var createProfileVO:FeedStateProfileVO {
        return FeedStateProfileVO()
    }

    func haveProfileVO( filterString:String?=nil ) -> Bool {
        let filteredState:FilteredState = feedVO!.isFiltered ? .filtered : .unfiltered
        if profileVOs[feedVO!.feedSource] == nil {
            return false
        }
        if  let vo:FeedStateProfileVO = profileVOs[feedVO!.feedSource]![filteredState] {
            if vo.filterString == filterString {
                return true
            }
        }
        return false
    }

    func resetProfile(){
        profileVO.currentPageIndex = 0
        profileVO.pageVOs = []
        profileVO.prevPageVO = nil
        profileVO.filterString = nil
    }

    override func hideChildren() {
        _filterVC?.hideMe()
        super.hideChildren()
    }

    override func applyLocalization() {
        super.applyLocalization()
        feedStateView.topPrevBtn.setTitle("previous".localized, for: .normal)
        feedStateView.topNextBtn.setTitle("next".localized, for: .normal)
        feedStateView.bottomPrevBtn.setTitle("previous".localized, for: .normal)
        feedStateView.bottomNextBtn.setTitle("next".localized, for: .normal)
    }

    override func addTargets() {
        feedStateView.filterBtn.addTarget(self, action: #selector(filerBtnPressed(_:)), for: .touchUpInside)
        feedStateView.topPrevBtn.addTarget(self, action: #selector(previousPage(_:)), for: .touchUpInside)
        feedStateView.topNextBtn.addTarget(self, action: #selector(nextPage(_:)), for: .touchUpInside)
        feedStateView.bottomPrevBtn.addTarget(self, action: #selector(previousPage(_:)), for: .touchUpInside)
        feedStateView.bottomNextBtn.addTarget(self, action: #selector(nextPage(_:)), for: .touchUpInside)
        super.addTargets()
    }

    override func removeTargets() {
        feedStateView.filterBtn.removeTarget(nil, action: nil, for: .allEvents)
        feedStateView.topPrevBtn.removeTarget(nil, action: nil, for: .allEvents)
        feedStateView.topNextBtn.removeTarget(nil, action: nil, for: .allEvents)
        feedStateView.bottomPrevBtn.removeTarget(nil, action: nil, for: .allEvents)
        feedStateView.bottomNextBtn.removeTarget(nil, action: nil, for: .allEvents)
        super.removeTargets()
    }

    @objc func filerBtnPressed(_ sender: Any) {

        filterVC.feedVO = feedVO!
        filterVC.listingRefs = listingRefsForFilter

        let userInfo:[AnyHashable : Any] = [
                AppViewAttribute.requestType: AppConstant.SHOW_OVERLAY,
                AppViewAttribute.action: filterVC
        ]
        NotificationCenter.default.post(name: Notification.Name(pageObserverID!), object: nil, userInfo: userInfo)
    }

    override func viewDidFirstAppear() {
        super.viewDidFirstAppear()
        initializeIsSpecifiedTeamsSegment()
        initializeFeed()
    }

    override func addObservers() {
        addObserver(self, selector: #selector(filterChanged(_:)), name: _filterVC?.dataChangedNotification, object: nil)
        super.addObservers()
    }

    @objc func filterChanged(_ sender:Any? = nil) {
        let producedFilterVO:FeedFilterVO? = _filterVC!.producedFilterVO
        feedVO = viewed.feedViewHelper.getFeedVO(feedType: feedVO!.feedType, feedSource: feedVO!.feedSource, seasonGroupRef: feedVO!.seasonGroupRef, isHappenedState: feedVO!.isHappenedState, filterVO: producedFilterVO, createIfNotMatched: true)!
        if !haveProfileVO(filterString: producedFilterVO?.filterString) {
            initializeFeed()
        } else {
            displayFeed()
        }
    }

    func initializeIsSpecifiedTeamsSegment() {

    }

    func initializeFeed(fromDataReceived:Bool = false  ) {
        feedStateView.filterBtn.isFiltered = feedVO!.isFiltered
        feedStateView.pageControlHidden = true
        resetProfile()
        applyLocalToFeed()
        if !requireMissingData() {
            validSeasonWasRequired = false
            validListingWasRequired = false
            validTeamWasRequired = false

            if feedVO!.amountAvailable == nil {
                if !fromDataReceived {
                    addActivityEndedObserver(selector: #selector(initialDataReceived(_:)))
                    requestData()
                } else {
                    app.updateErrorAppCommand().command()
                }
            } else {
                applyInitializeFeed()
            }
        }
    }

    func applyInitializeFeed() {
        let amountEntriesAvailable: Int = feedVO!.amountAvailable!
        let hasNoEntries: Bool = amountEntriesAvailable == 0 || entriesPerPage == 0
        if hasNoEntries {
            addNoDataPage()
        } else {
            let amtPages: Int = Int(ceil(Double(amountEntriesAvailable) / Double(entriesPerPage)))
            var nextEntryConstantIndex: Int = 0
            for i in 0 ..< amtPages {
                let pageVO: FeedPageVO = createPageVO(pageIndex: i)
                pageVO.firstEntryConstantIndex = nextEntryConstantIndex
                if haveDataLocally || viewed.feedViewHelper.haveFeedEntries(feedVO!, entriesSpan: entriesPerPage, entriesFrom: pageVO.firstEntryConstantIndex) {
                    pageVO.dataRetrieved = true
                }
                nextEntryConstantIndex += entriesPerPage
                profileVO.pageVOs.append(pageVO)
            }
        }
        displayFeed()
    }

    func applyLocalToFeed() {

    }

    func displayFeed() {
        applyPageState()
        showCurrentPage()
    }

    func applyPageState() {
        feedStateView.filterBtn.isFiltered = feedVO!.isFiltered
        feedStateView.topPageControl.numberOfPages = profileVO.pageVOs.count
        feedStateView.topPageControl.applyScaling()
        feedStateView.bottomPageControl.numberOfPages = profileVO.pageVOs.count
        feedStateView.bottomPageControl.applyScaling()
    }

    func showCurrentPage() {


        let currentPageVO:FeedPageVO = getCurrentPageVO()

        if profileVO.prevPageVO != nil && currentPageVO.pageIndex == profileVO.prevPageVO!.pageIndex {
            return
        }
        hidePageContent()

        if !currentPageVO.dataRetrieved {
            addActivityEndedObserver(selector: #selector(pageDataReceived(_:)))
            requestData(entriesFrom:currentPageVO.firstEntryConstantIndex)
        } else {
            if currentPageVO.hasNoEntries {
                showNoContentPage()
            } else {
                displayCurrentPageContent()
            }
        }
    }

    func hidePageContent() {
        removeAllFromMainContent()
        feedStateView.pageControlHidden = true
    }

    func removeAllFromMainContent() {
        feedStateView.mainContentView.removeAll()
    }

    @objc func previousPage(_ sender: Any) {
        profileVO.currentPageIndex -= 1
        loadCurrentPage()
    }

    @objc func nextPage(_ sender: Any) {
        profileVO.currentPageIndex += 1
        loadCurrentPage()
    }

    func loadCurrentPage() {
        removeAllFromMainContent()
        showCurrentPage()
    }

    func getCurrentPageVO() -> FeedPageVO {
        if profileVO.pageVOs.count == 0 {
            addNoDataPage()
        }
        if profileVO.currentPageIndex < 0 {
            profileVO.currentPageIndex = 0
        } else if profileVO.currentPageIndex > profileVO.pageVOs.count - 1 {
            profileVO.currentPageIndex = profileVO.pageVOs.count - 1
        }
        return profileVO.pageVOs[ profileVO.currentPageIndex ]
    }

    func addNoDataPage() {
        let pageVO:FeedPageVO = createPageVO(pageIndex: profileVO.pageVOs.count)
        pageVO.hasNoEntries = true
        pageVO.prepared = true
        pageVO.dataRetrieved = true
        profileVO.pageVOs.append(pageVO)
    }

    func showNoContentPage() {
        feedStateView.mainContentView.isHidden = false

        let leftMargin:CGFloat = viewed.viewConfig.topLeftMargin.x
        let rightMargin:CGFloat = viewed.viewConfig.bottomRightMargin.x

        let noContentTextView: AttributedTextView = AttributedTextView()
        noContentTextView.textContainer.maximumNumberOfLines = 0
        noContentTextView.applyTextVOs(noContentTextVOs, withTextAlignment: .center)
        feedStateView.mainContentView.addSubview(noContentTextView)

        let topConstraint:NSLayoutConstraint = NSLayoutConstraint(item: noContentTextView, attribute: .top, relatedBy: .equal, toItem: feedStateView.mainContentView, attribute: .top, multiplier: 1, constant: 0)
        let leadingConstraint:NSLayoutConstraint = NSLayoutConstraint(item: noContentTextView, attribute: .leading, relatedBy: .greaterThanOrEqual, toItem: feedStateView.mainContentView, attribute: .leading, multiplier: 1, constant: leftMargin)
        let trailingConstraint:NSLayoutConstraint = NSLayoutConstraint(item: noContentTextView, attribute: .trailing, relatedBy: .lessThanOrEqual, toItem: feedStateView.mainContentView, attribute: .trailing, multiplier: 1, constant: -rightMargin)
        let bottomConstraint:NSLayoutConstraint = NSLayoutConstraint(item: noContentTextView, attribute: .bottom, relatedBy: .equal, toItem: feedStateView.mainContentView, attribute: .bottom, multiplier: 1, constant: 0)
        let centerXConstraint:NSLayoutConstraint =  NSLayoutConstraint(item: noContentTextView, attribute: .centerX, relatedBy: .equal, toItem: feedStateView.mainContentView, attribute: .centerX, multiplier: 1, constant: 0)

        noContentTextView.addAppConstraint(topConstraint, feedStateView.mainContentView)
        noContentTextView.addAppConstraint(trailingConstraint, feedStateView.mainContentView)
        noContentTextView.addAppConstraint(leadingConstraint, feedStateView.mainContentView)
        noContentTextView.addAppConstraint(bottomConstraint, feedStateView.mainContentView)
        noContentTextView.addAppConstraint(centerXConstraint, feedStateView.mainContentView)
    }

    var noContentTextVOs:[TextVO] {
        return []
    }

    func feedPageRequestNotification(stateID:Int) {
        NotificationCenter.default.post(name: Notification.Name(navigationObserverID!), object: nil, userInfo: [AppViewAttribute.requestType: FeedVC.CHANGE_PAGE_REQUEST, AppViewAttribute.action:stateID])
    }

    func changeState(stateID:Int) {
        NotificationCenter.default.post(name: Notification.Name(typeObserverID!), object: nil, userInfo: [AppViewAttribute.requestType: FeedTypeVC.CHANGE_STATE_REQUEST, AppViewAttribute.action:stateID])
    }

    func displayCurrentPageContent() {
        let currentPageVO:FeedPageVO = getCurrentPageVO()
        feedStateView.mainContentView.isHidden = false
        if profileVO.pageVOs.count > 1 {
            feedStateView.pageControlHidden = false
            feedStateView.topPageControl.currentPage = currentPageVO.pageIndex
            feedStateView.bottomPageControl.currentPage = currentPageVO.pageIndex
            feedStateView.topPrevBtn.setIsEnabled( currentPageVO.pageIndex > 0 )
            feedStateView.topNextBtn.setIsEnabled( currentPageVO.pageIndex < profileVO.pageVOs.count - 1 )
            feedStateView.bottomPrevBtn.setIsEnabled( currentPageVO.pageIndex > 0 )
            feedStateView.bottomNextBtn.setIsEnabled( currentPageVO.pageIndex < profileVO.pageVOs.count - 1 )
        }
    }

    func createPageVO(pageIndex:Int ) -> FeedPageVO {
        return FeedPageVO(pageIndex: pageIndex)
    }

    @objc func initialDataReceived(_ sender: Any?=nil) {
        removeActivityEndedObserver()
        initializeFeed( fromDataReceived: true)
    }

    @objc func missingDataReceived(_ sender: Any?=nil) {
        removeActivityEndedObserver()
        initializeFeed()
    }

    @objc func pageDataReceived(_ sender: Any?=nil) {
        removeActivityEndedObserver()
        for pageVO:FeedPageVO in profileVO.pageVOs {
            if !pageVO.dataRetrieved {
                if viewed.feedViewHelper.haveFeedEntries(feedVO!, entriesSpan: entriesPerPage, entriesFrom: pageVO.firstEntryConstantIndex) {
                    pageVO.dataRetrieved = true
                }
            }
        }
        showCurrentPage()
    }

    func requestData(entriesFrom:Int?=nil) {
        //for override
    }

    func requireMissingData() -> Bool {
        let seasonRefs:[Int:NSNull] = balancing.balancingHelper.getAllSeasonRefs(seasonGroupRef: feedVO!.seasonGroupRef)
        var validSeasonRefs:[Int:NSNull] = [:]
        var requiredSeasonGroupRefs:[Int:NSNull] = [:]
        feedStateView.filterBtn.setIsEnabled( true )
        feedStateView.isSpecifiedTeamsSegment.isEnabled = true
        for seasonRef:Int in seasonRefs.keys {
            if let seasonVO:SeasonVO = happen.happenedModel.seasonVOs[seasonRef] {
                if !seasonVO.haveData {
                    requiredSeasonGroupRefs[feedVO!.seasonGroupRef] = NSNull()
                    break
                } else {
                    validSeasonRefs[seasonRef] = NSNull()
                }
            } else if happen.happenedModel.nonExistentSeasonRefs[seasonRef] == nil {
                requiredSeasonGroupRefs[feedVO!.seasonGroupRef] = NSNull()
                break
            }
        }
        if requiredSeasonGroupRefs.count > 0 {
            if !validSeasonWasRequired {
                validSeasonWasRequired = true
                addActivityEndedObserver(selector: #selector(missingDataReceived(_:)))
                server.validSeasonServerCommand().command(seasonGroupRefs: requiredSeasonGroupRefs)
            } else {
                app.updateErrorAppCommand().command()
            }
            return true
        }
        if validSeasonRefs.count == 0 {
            if feedVO!.amountAvailable == nil {
                feedStateView.filterBtn.setIsEnabled( false )
                feedStateView.isSpecifiedTeamsSegment.isEnabled = false
                feedVO!.amountAvailable = 0
            }
        }
        if feedVO!.isTeamFeed {
            var requiredTeamRefs:[Int:NSNull] = [:]
            let teamRefs:[Int:NSNull] = preference.preferenceHelper.getTeamRefs()
            var validTeamRefs:[Int:NSNull] = [:]
            if teamRefs.count > 0 {
                for teamRef:Int in teamRefs.keys {
                    if let teamVO:TeamVO = happen.happenedModel.teamVOs[teamRef] {
                        if !teamVO.havePartialData && !teamVO.haveData {
                            requiredTeamRefs[teamRef] = NSNull()
                        } else {
                            validTeamRefs[teamRef] = NSNull()
                        }
                    } else if happen.happenedModel.nonExistentTeamRefs[teamRef] == nil {
                        requiredTeamRefs[teamRef] = NSNull()
                    }
                }
                if requiredTeamRefs.count > 0 {
                    if !validTeamWasRequired {
                        validTeamWasRequired = true
                        addActivityEndedObserver(selector: #selector(missingDataReceived(_:)))
                        server.validTeamListingServerCommand().command(teamRefs:requiredTeamRefs )
                    } else {
                        app.updateErrorAppCommand().command()
                    }
                    return true
                }
            }
            if validTeamRefs.count == 0 {
                if feedVO!.amountAvailable == nil {
                    feedStateView.filterBtn.setIsEnabled( false )
                    feedStateView.isSpecifiedTeamsSegment.isEnabled = false
                    feedVO!.amountAvailable = 0
                }
            }
        }
        var requiredListingRefs:[Int:NSNull] = [:]
        let listingRefs:[Int:NSNull] = relevantListingRefs
        var validListingRefs:[Int:NSNull] = [:]
        if listingRefs.count > 0 {
            for listingRef:Int in listingRefs.keys {
                if let listingVO:ListingVO = happen.happenedModel.listingVOs[listingRef] {
                    if !listingVO.haveData {
                        requiredListingRefs[listingRef] = NSNull()
                    } else {
                        validListingRefs[listingRef] = NSNull()
                    }
                } else if happen.happenedModel.nonExistentListingRefs[listingRef] == nil {
                    requiredListingRefs[listingRef] = NSNull()
                }
            }
            if requiredListingRefs.count > 0 {
                if !validListingWasRequired {
                    validListingWasRequired = true
                    addActivityEndedObserver(selector: #selector(missingDataReceived(_:)))
                    server.validListingServerCommand().command(listingRefs:requiredListingRefs )
                } else {
                    app.updateErrorAppCommand().command()
                }
                return true
            }
        }
        if validListingRefs.count == 0 {
            if feedVO!.amountAvailable == nil {
                feedStateView.filterBtn.setIsEnabled( false )
                feedStateView.isSpecifiedTeamsSegment.isEnabled = false
                feedVO!.amountAvailable = 0
            }
        }
        return false
    }

    var listingRefsForTeams:[Int:NSNull] {

        let seasonRefs:[Int:NSNull] = balancing.balancingHelper.getAllSeasonRefs(seasonGroupRef: feedVO!.seasonGroupRef)
        var listingRefs:[Int:NSNull] = [Int:NSNull]()
        let teamRefs:[Int:NSNull] = preference.preferenceHelper.getTeamRefs()
        for teamRef:Int in teamRefs.keys {
            for seasonRef:Int in seasonRefs.keys {
                let teamSeasonVO:TeamSeasonVO = happen.happenedModel.getTeamSeasonVO(teamRef, seasonRef)!
                for listingRef:Int in teamSeasonVO.teamListingRefs.keys {
                    listingRefs[listingRef] = NSNull()
                }
            }
        }
        return listingRefs
    }

    var listingRefsForSeasonGroup:[Int:NSNull] {

        var listingRefs:[Int:NSNull] = [Int:NSNull]()
        let seasonRefs:[Int:NSNull] = balancing.balancingHelper.getAllSeasonRefs(seasonGroupRef: feedVO!.seasonGroupRef)
        for seasonRef:Int in seasonRefs.keys {
            let seasonVO:SeasonVO = happen.happenedModel.seasonVOs[seasonRef]!
            for listingRef:Int in seasonVO.listingSeasonRefs.keys{
                listingRefs[listingRef] = NSNull()
            }
        }

        return listingRefs
    }

    var unfilteredListingRefs:[Int:NSNull] {
        if !feedVO!.isTeamFeed {
            return preference.preferenceHelper.getListingRefs()
        }
        return listingRefsForTeams
    }

    var listingRefsForFilter:[Int:NSNull] {
        if !feedVO!.isTeamFeed {
            return listingRefsForSeasonGroup
        }
        return listingRefsForTeams
    }

    var relevantListingRefs: [Int:NSNull] {
        if !feedVO!.isFiltered {
            return unfilteredListingRefs
        }
        if !feedVO!.filterVO!.allCompetitionSelected {
            return feedVO!.filterVO!.listingRefs
        }

        if feedVO!.isTeamFeed {
            return listingRefsForTeams
        }

        return listingRefsForSeasonGroup
    }

    var relevantTeamRefs: [Int:NSNull] {
        if !feedVO!.isTeamFeed {
            return [:]
        }
        return preference.preferenceHelper.getTeamRefs()
    }

    var requestListingRefs: [Int:NSNull]? {
        return (feedVO!.feedSource != .specificTeam || feedVO!.isFiltered) ? relevantListingRefs : nil
    }

    var requestTeamRefs: [Int:NSNull]? {
        return feedVO!.feedSource == .specificTeam ? relevantTeamRefs : nil
    }

    var entriesPerPage:Int {
        return 0
    }

    var isLatestSeasonGroup:Bool {
        return feedVO!.seasonGroupRef == balancing.balancingModel.latestSeasonGroupRef
    }

    var haveDataLocally:Bool {
        return false
    }

    var filterVC: FeedFilterVC {
        if _filterVC != nil {
            return _filterVC!
        }
        _filterVC = birthFilterVC
        addObserver(self, selector: #selector(filterChanged(_:)), name: _filterVC!.dataChangedNotification, object: nil)
        return _filterVC!
    }

    var birthFilterVC:FeedFilterVC {
        return FeedFilterVC()
    }

    var defaultFeedSource:FeedSource? {
        return isTeamFeed ? .specificTeam : .listing
    }

}
