import Foundation

class FeedStateProfileVO {

    var currentPageIndex:Int = 0
    var pageVOs:[FeedPageVO] = []
    var prevPageVO:FeedPageVO? = nil
    var filterString:String? = nil

    var initialized:Bool {
        return pageVOs.count > 0
    }

}
