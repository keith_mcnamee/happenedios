import UIKit

class ScenarioVC: VerticalScrollVC {

    var app:App { return App.instance }
    var balancing: Balancing { return Balancing.instance }
    var happen: Happen { return Happen.instance }
    var localization:Localization { return Localization.instance }
    var server: Server { return Server.instance }
    var viewed: Viewed { return Viewed.instance }

    enum TypeIndexes:Int {
        case standing = 0,
             fixture = 1
    }

    enum IsSpecifiedIndexes:Int {
        case allTeams = 0,
             thisTeam = 1
    }

    var navigationObserverID:String?

    var scenarioRef: Int?
    var scenarioVO: ScenarioTypeVO?

    var seasonRef: Int?
    var listingRef: Int?
    var teamRef: Int?
    var teamListingVO: TeamListingVO?

    var seasonVOs:[SeasonViewVO]?
    var byRefSeasonVOs:[Int:SeasonViewVO]?

    var _standingViewControllers: [Int:IntDicVOsVO] = [:]
    var _allFixtureStateControllers: [Int:IntDicVOsVO] = [:]
    var _teamFixtureStateControllers: [Int:IntDicVOsVO] = [:]

    var _mainContentVC: ComponentVC?
    var _seasonPickerVC: PickerVC?

    var scenarioViewVOs:[Int:ScenarioViewVO] = [:]

    var waitedForServer:Bool = false
    var viewReadyApplied:Bool = false

    convenience init( scenarioRef:Int, navigationObserverID:String ) {

        self.init(nibName: nil, bundle: nil)

        self.scenarioRef = scenarioRef

        self.navigationObserverID = navigationObserverID

        edgesForExtendedLayout = []

    }

    override func createAppView() -> ScenarioView {
        return ScenarioView()
    }

    var scenarioView:ScenarioView {
        return view as! ScenarioView
    }

    override func initialize() {
        super.initialize()
        applyObserverID()
    }

    override func viewWillFirstAppear() {
        super.viewWillFirstAppear()

        applyCurrentState()
        initializeTypeSegment()

        initializeScenario()
    }

    override func viewDidFirstAppear() {
        super.viewDidFirstAppear()
        if !viewReadyApplied {
            applyViewReady()
        }
        applyScaling()
    }

    func initializeScenario(dataExpected:Bool = false ) {
        if !validScenarioElements && scenarioRef == nil {
            return
        }
        var listingSeasonVO:ListingSeasonVO? = nil
        teamListingVO = nil
        if validScenarioElements {
            scenarioVO = scenarioVoFromElements
            if scenarioVO != nil {
                teamListingVO = happen.happenedModel.getTeamListingVO(scenarioVO!.seasonRef, scenarioVO!.listingRef, scenarioVO!.teamRef)
                listingSeasonVO = happen.happenedModel.getListingSeasonVO(scenarioVO!.seasonRef, scenarioVO!.listingRef)
                if teamListingVO != nil && listingSeasonVO != nil {
                    scenarioRef = scenarioVO!.ref
                } else {
                    scenarioVO = nil
                }
            }
        } else {
            scenarioVO = scenarioVoFromRef
            if scenarioVO != nil {
                teamListingVO = happen.happenedModel.getTeamListingVO(scenarioVO!.seasonRef, scenarioVO!.listingRef, scenarioVO!.teamRef)
                listingSeasonVO = happen.happenedModel.getListingSeasonVO(scenarioVO!.seasonRef, scenarioVO!.listingRef)
                if teamListingVO != nil && listingSeasonVO != nil {
                    takeElementsFromScenarioVO()
                } else {
                    scenarioVO = nil
                }
            }
        }


        applyViewInitialized()
        if scenarioVO == nil || teamListingVO == nil || !teamListingVO!.haveScenario || listingSeasonVO == nil || !listingSeasonVO!.haveData {
            waitedForServer = true
            if !dataExpected {
                addActivityEndedObserver(selector: #selector(dataReceived(_:)))
                if validScenarioElements {
                    server.teamScenarioServerCommand().command(seasonRef: seasonRef!, listingRef: listingRef!, teamRef: teamRef!)
                } else {
                    requestServerByRef()
                }
            } else {
                app.updateErrorAppCommand().command()
            }
        } else {
            applyViewReady()
        }
    }

    var validScenarioElements:Bool {
        return false
    }

    var scenarioVoFromElements:ScenarioTypeVO? {
        return nil
    }

    func altSeasonScenarioVO( _ seasonRef:Int ) -> ScenarioTypeVO? {
        return nil
    }

    var altScenarioVO: ScenarioTypeVO? {
        return nil //Only used for events
    }

    func altSeasonAltScenarioVO( _ seasonRef:Int ) -> ScenarioTypeVO? {
        return nil //Only used for events
    }

    func applyAlt() {

    }

    var scenarioVoFromRef:ScenarioTypeVO? {
        return nil
    }

    func takeElementsFromScenarioVO() {
        seasonRef = scenarioVO!.seasonRef
        listingRef = scenarioVO!.listingRef
        teamRef = scenarioVO!.teamRef
    }

    func requestServerByRef() {

    }

    func applyViewInitialized() {
        if !viewWillFirstAppearApplied {
            return
        }
        if teamListingVO == nil {
            self.navigationItem.title = ""
            scenarioView.seasonBtn.setTitle("", for: .normal)
            return
        }

        let teamName:String = localization.localizationHelper.getTeamName(teamListingVO!.teamRef, seasonRef: teamListingVO!.seasonRef)
        self.navigationItem.title = teamName
        determineSeasonData()
        let seasonName:String = byRefSeasonVOs![teamListingVO!.seasonRef] != nil ? byRefSeasonVOs![teamListingVO!.seasonRef]!.text : ""
        scenarioView.seasonBtn.setTitle(seasonName, for: .normal)
        let listingName:String = localization.localizationHelper.getCompetitionOrListingName(teamListingVO!.listingRef, seasonRef: teamListingVO!.seasonRef, allowDefault: true)!
        scenarioView.teamTextView.applyTextVOs([TextVO(teamName)], withTextAlignment: .center)
        scenarioView.listingSeasonTextView.applyTextVOs([TextVO(listingName), TextVO(" "), TextVO(seasonName)], withTextAlignment: .center)

        scenarioView.typeSegment.selectedSegmentIndex = TypeIndexes.standing.rawValue
    }

    func prepareViewVO() -> ScenarioViewVO? {
        return nil
    }

    func applyViewReady() {
        if !viewWillFirstAppearApplied || scenarioVO == nil || teamListingVO == nil || !teamListingVO!.haveScenario  {
            return
        }
        viewReadyApplied = true

        if scenarioViewVOs[scenarioVO!.seasonRef] == nil {
            scenarioViewVOs[scenarioVO!.seasonRef] = prepareViewVO()!
        }
        initializeStateSegment()
        initializeIsSpecifiedSegment()

        scenarioView.layoutIfNeeded()
        applyScaling()

        applyCurrentState()
    }

    func determineSeasonData() {
        let teamVO:TeamVO = happen.happenedModel.teamVOs[teamListingVO!.teamRef]!
        var seasonRefs:[Int:NSNull] = [:]
        for seasonRef:Int in teamVO.teamSeasonVOs.keys {
            if let scenarioVO:ScenarioTypeVO = altSeasonScenarioVO(seasonRef) {
                if !scenarioVO.invalid {
                    seasonRefs[seasonRef] = NSNull()
                }
            }
            if seasonRefs[seasonRef] == nil {
                if let altEventScenarioVO:ScenarioTypeVO = altSeasonAltScenarioVO(seasonRef) {
                    if !altEventScenarioVO.invalid {
                        seasonRefs[seasonRef] = NSNull()
                    }
                }
            }
        }
        seasonVOs = viewed.viewHelper.getSeasonViewVOs(seasonRefs)
        byRefSeasonVOs = [:]
        for seasonVO:SeasonViewVO in seasonVOs! {
            byRefSeasonVOs![seasonVO.ref] = seasonVO
        }
    }

    var nextVC: ComponentVC? {
        if currentStateVO == nil {
            return nil
        }
        var vc:ComponentVC? = nil
        if scenarioView.typeSegment.selectedSegmentIndex == TypeIndexes.standing.rawValue {
            vc = standingVC
        } else if scenarioView.isSpecifiedTeamsSegment.selectedSegmentIndex == IsSpecifiedIndexes.allTeams.rawValue {
            vc = allFixtureVC
        } else {
            vc = teamFixtureVC
        }
        return vc!
    }

    var standingVC: StandingContainerVC? {
        if currentStateVO == nil {
            return nil
        }
        let containerVO: IntDicVOsVO? = _standingViewControllers[teamListingVO!.seasonRef]
        var vc: StandingContainerVC? = containerVO?.vos[scenarioView.stateSegment.selectedSegmentIndex] as? StandingContainerVC
        if vc != nil {
            return vc!
        }

        vc = StandingContainerVC( standingListVO: currentStateVO!.standingVO!, verticalScrollObserverID: observerID! )
        if _standingViewControllers[teamListingVO!.seasonRef] == nil {
            _standingViewControllers[teamListingVO!.seasonRef] = IntDicVOsVO()
        }
        _standingViewControllers[teamListingVO!.seasonRef]!.vos[scenarioView.stateSegment.selectedSegmentIndex] = vc!
        return vc!
    }

    var allFixtureVC: ScenarioFixtureStateVC? {
        if currentStateVO == nil {
            return nil
        }
        let containerVO: IntDicVOsVO? = _allFixtureStateControllers[teamListingVO!.seasonRef]
        var vc: ScenarioFixtureStateVC? = containerVO?.vos[scenarioView.stateSegment.selectedSegmentIndex] as? ScenarioFixtureStateVC
        if vc != nil {
            return vc!
        }

        vc = ScenarioFixtureStateVC( pageVOs:currentStateVO!.allFixturePageVOs, seasonRef: teamListingVO!.seasonRef)
        if _allFixtureStateControllers[teamListingVO!.seasonRef] == nil {
            _allFixtureStateControllers[teamListingVO!.seasonRef] = IntDicVOsVO()
        }
        _allFixtureStateControllers[teamListingVO!.seasonRef]!.vos[scenarioView.stateSegment.selectedSegmentIndex] = vc!
        return vc!
    }

    var teamFixtureVC: ScenarioFixtureStateVC? {
        if currentStateVO == nil {
            return nil
        }
        let containerVO: IntDicVOsVO? = _teamFixtureStateControllers[teamListingVO!.seasonRef]
        var vc: ScenarioFixtureStateVC? = containerVO?.vos[scenarioView.stateSegment.selectedSegmentIndex] as? ScenarioFixtureStateVC
        if vc != nil {
            return vc!
        }

        vc = ScenarioFixtureStateVC( pageVOs:currentStateVO!.teamFixturePageVOs, seasonRef: teamListingVO!.seasonRef )
        if _teamFixtureStateControllers[teamListingVO!.seasonRef] == nil {
            _teamFixtureStateControllers[teamListingVO!.seasonRef] = IntDicVOsVO()
        }
        _teamFixtureStateControllers[teamListingVO!.seasonRef]!.vos[scenarioView.stateSegment.selectedSegmentIndex] = vc!
        return vc!
    }

    func displayScenarioContent(waitedForView:Bool=false ) {
        let vc:ComponentVC? = nextVC
        if vc != nil && vc != _mainContentVC {
            if waitedForServer || vc!.viewDidFirstAppearApplied || waitedForView {
                _mainContentVC?.hideMe()
                _mainContentVC = vc
                _mainContentVC!.showMe(parentController: self, parentView: scenarioView.mainContentView)
            } else {
                showViewLoadingActivityIndicator(jumpToDisplayScenarioContent)
            }
        }

        waitedForServer = false
    }

    func jumpToDisplayScenarioContent() {
        displayScenarioContent( waitedForView:true )
    }

    @objc func dataReceived(_ sender: Any?=nil) {
        removeActivityEndedObserver()
        initializeScenario( dataExpected: true )
    }

    override func addTargets() {

        scenarioView.seasonBtn.addTarget(self, action: #selector(showSeasonPicker(_:)), for: .touchUpInside)
        scenarioView.typeSegment.addTarget(self, action: #selector(applyCurrentState(_:)), for: .valueChanged)
        scenarioView.stateSegment.addTarget(self, action: #selector(applyCurrentState(_:)), for: .valueChanged)
        scenarioView.isSpecifiedTeamsSegment.addTarget(self, action: #selector(applyCurrentState(_:)), for: .valueChanged)
        super.addTargets()
    }

    override func removeTargets() {
        super.removeTargets()
        scenarioView.seasonBtn.removeAllTargets()
        scenarioView.typeSegment.removeTarget(nil, action: nil, for: .allEvents)
        scenarioView.typeSegment.removeTarget(nil, action: nil, for: .allEvents)
        scenarioView.typeSegment.removeTarget(nil, action: nil, for: .allEvents)
    }

    @objc func applyCurrentState(_ sender: Any?=nil) {

        scenarioView.isSpecifiedTeamsSegmentHidden = scenarioView.typeSegment.selectedSegmentIndex != TypeIndexes.fixture.rawValue
        scenarioView.stateSegmentHidden = doHideStateSegment

        if currentViewVO == nil {
            return
        }

        displayScenarioContent()
    }

    @objc func seasonSelected(_ sender: Any) {
        if seasonPickerVC.ref! == scenarioVO!.seasonRef {
            return
        }

        _mainContentVC?.hideMe()
        takeElementsFromScenarioVO()
        seasonRef = seasonPickerVC.ref!
        if scenarioVoFromElements == nil {
            if altScenarioVO != nil {
                applyAlt()
            }
        }
        scenarioRef = nil
        scenarioVO = nil
        initializeScenario()
    }

    @objc func showSeasonPicker(_ sender: Any) {
        seasonPickerVC.ref = seasonRef
        let userInfo:[AnyHashable : Any] = [
                AppViewAttribute.requestType: AppConstant.SHOW_OVERLAY,
                AppViewAttribute.action: seasonPickerVC
        ]
        NotificationCenter.default.post(name: Notification.Name(observerID!), object: nil, userInfo: userInfo)
    }

    private var seasonPickerVC: PickerVC {
        if _seasonPickerVC != nil {
            return _seasonPickerVC!
        }
        _seasonPickerVC = PickerVC(vos: seasonVOs!, ref: scenarioVO!.seasonRef)
        addObserver(self, selector: #selector(seasonSelected(_:)), name: _seasonPickerVC!.dataChangedNotification, object: nil)
        return _seasonPickerVC!
    }

    override func addObservers() {
        addObserver(self, selector: #selector(seasonSelected(_:)), name: _seasonPickerVC?.dataChangedNotification, object: nil)
        super.addObservers()
    }

    private func initializeTypeSegment() {
        scenarioView.typeSegment.removeAllSegments()
        scenarioView.typeSegment.insertSegment(withTitle: "table".localized, at: TypeIndexes.standing.rawValue, animated: false)
        scenarioView.typeSegment.insertSegment(withTitle: "fixtures".localized, at: TypeIndexes.fixture.rawValue, animated: false)
        scenarioView.typeSegment.selectedSegmentIndex = TypeIndexes.standing.rawValue
    }

    private func initializeStateSegment() {

        scenarioView.stateSegment.removeAllSegments()
        let viewVO:ScenarioViewVO = scenarioViewVOs[scenarioVO!.seasonRef]!
        var index:Int = -1;
        for stateVO:ScenarioStateViewVO in viewVO.stateVOs {
            index += 1
            scenarioView.stateSegment.insertSegment(withTitle: stateVO.title, at: index, animated: false)
        }
        scenarioView.stateSegment.selectedSegmentIndex = viewVO.selectedStateIndex
    }

    private func initializeIsSpecifiedSegment() {
        scenarioView.isSpecifiedTeamsSegment.removeAllSegments()
        scenarioView.isSpecifiedTeamsSegment.insertSegment(withTitle: "all_fixtures".localized, at: IsSpecifiedIndexes.allTeams.rawValue, animated: false)
        let thisTeamName:String = localization.localizationHelper.getTeamName(teamListingVO!.teamRef, seasonRef: teamListingVO!.seasonRef)
        let thisTeamFixturesTitle:String = localization.localizationHelper.localizeParams("team_fixtures", ["team":thisTeamName], localizationType: LocalizationConstant.NATIVE_TYPE)!
        scenarioView.isSpecifiedTeamsSegment.insertSegment(withTitle: thisTeamFixturesTitle, at: IsSpecifiedIndexes.thisTeam.rawValue, animated: false)
        scenarioView.isSpecifiedTeamsSegment.selectedSegmentIndex = IsSpecifiedIndexes.allTeams.rawValue
    }

    func applyScaling() {
        scenarioView.stateSegment.scaleToScreenWidth(withMargin: true)
        scenarioView.isSpecifiedTeamsSegment.scaleToScreenWidth(withMargin: true)
    }

    override func orientationDidChange() {
        super.orientationDidChange()
        applyScaling()
    }

    var doHideStateSegment:Bool {
        return currentViewVO == nil || currentViewVO!.stateVOs.count < 2
    }

    var currentViewVO:ScenarioViewVO? {
        return teamListingVO != nil ? scenarioViewVOs[teamListingVO!.seasonRef] : nil
    }

    var currentStateVO:ScenarioStateViewVO? {
        return currentViewVO != nil ? currentViewVO!.stateVOs[scenarioView.stateSegment.selectedSegmentIndex] : nil
    }

    var isHistoricalSeason:Bool {
        return teamListingVO != nil ? balancing.balancingHelper.isHistoricalSeason(teamListingVO!.seasonRef) : false
    }

    override func hideChildren() {
        _seasonPickerVC?.hideMe()
        super.hideChildren()
    }

    override func destroy() {
        for vc:IntDicVOsVO in _standingViewControllers.values {
            for stateVC:Any in vc.vos.values {
                (stateVC as! StandingContainerVC).destroy()
            }
        }
        for vc:IntDicVOsVO in _allFixtureStateControllers.values {
            for stateVC:Any in vc.vos.values {
                (stateVC as! ScenarioFixtureStateVC).destroy()
            }
        }
        for vc:IntDicVOsVO in _teamFixtureStateControllers.values {
            for stateVC:Any in vc.vos.values {
                (stateVC as! ScenarioFixtureStateVC).destroy()
            }
        }
        _standingViewControllers = [:]
        _allFixtureStateControllers = [:]
        _teamFixtureStateControllers = [:]
        _mainContentVC?.destroy()
        _seasonPickerVC?.destroy()
        _seasonPickerVC = nil
        scenarioViewVOs = [:]
        super.destroy()
    }
}
