import UIKit

class FeedVC: VerticalScrollVC {

    static let CHANGE_PAGE_REQUEST:String = NSUUID().uuidString

    private var app:App { return App.instance }

    private var balancing: Balancing { return Balancing.instance }
    private var viewed: Viewed { return Viewed.instance }

    enum TypeIndexes:Int {
        case event = 0,
             fixture = 1
    }

    var navigationObserverID:String?

    private var currentSeasonGroupRef:Int?
    private var isTeamFeed:Bool?
    private var seasonGroupVOs:[SeasonGroupViewVO]?
    private var byRefSeasonGroupVOs:[Int:SeasonGroupViewVO]?

    private var eventViewControllers:[Int: EventFeedTypeVC] = [:]
    private var fixtureViewControllers:[Int: FixtureFeedTypeVC] = [:]
    private var _mainContentVC: FeedTypeVC?
    private var _seasonGroupPickerVC: PickerVC?

    convenience init( navigationObserverID:String, isTeamFeed: Bool ) {
        self.init(nibName: nil, bundle: nil)

        self.isTeamFeed = isTeamFeed
        self.navigationObserverID = navigationObserverID
    }

    override func createAppView() -> FeedView {
        return FeedView()
    }

    var feedView:FeedView {
        return view as! FeedView
    }

    override func initialize() {
        super.initialize()
        applyObserverID()
    }

    override func viewWillFirstAppear() {
        super.viewWillFirstAppear()
        determineSeasonGroupData()
        feedView.seasonGroupBtn.setTitle(byRefSeasonGroupVOs![currentSeasonGroupRef!]!.text, for: .normal)
        initializeFeedTypeSegment()
    }

    override func viewDidFirstAppear() {
        super.viewDidFirstAppear()
        DispatchQueue.main.async {
            //self.showListing(40)
            //self.showListing(16)
            //self.showTeam(911)
            //self.showListingScenario(1)
            //self.showEventScenario(949)
            //self.showTab(TabIdentifier.settings)
        }
    }

    func checkForDataReset() {
        if viewed.feedViewHelper.isDataReset(teamOnly: isTeamFeed!) {
            resetMainContentViewControllers()
            currentSeasonGroupRef = balancing.balancingModel.getDefaultVO()!.seasonGroupRef!
            _seasonGroupPickerVC?.ref = currentSeasonGroupRef
            feedView.typeSegment.selectedSegmentIndex = TypeIndexes.event.rawValue
        }
    }

    override func resetView() {
        super.resetView()
        checkForDataReset()
        applyCurrentSettings()
    }

    private func showListing(_ listingSeasonRef:Int) {

        let userInfo:[AnyHashable : Any] = [
                AppViewAttribute.requestType: AppConstant.SHOW_LISTING_REQUEST,
                AppViewAttribute.action: listingSeasonRef
        ]
        NotificationCenter.default.post(name: Notification.Name(navigationObserverID!), object: nil, userInfo: userInfo)
    }

    private func showTeam(_ teamListingRef:Int) {

        let userInfo:[AnyHashable : Any] = [
                AppViewAttribute.requestType: AppConstant.SHOW_TEAM_REQUEST,
                AppViewAttribute.action: teamListingRef
        ]
        NotificationCenter.default.post(name: Notification.Name(navigationObserverID!), object: nil, userInfo: userInfo)
    }

    private func showListingScenario(_ listingScenarioRef:Int) {

        let userInfo:[AnyHashable : Any] = [
                AppViewAttribute.requestType: AppConstant.SHOW_LISTING_SCENARIO_REQUEST,
                AppViewAttribute.action: listingScenarioRef
        ]
        NotificationCenter.default.post(name: Notification.Name(navigationObserverID!), object: nil, userInfo: userInfo)
    }

    private func showEventScenario(_ eventScenarioRef:Int) {

        let userInfo:[AnyHashable : Any] = [
                AppViewAttribute.requestType: AppConstant.SHOW_EVENT_SCENARIO_REQUEST,
                AppViewAttribute.action: eventScenarioRef
        ]
        NotificationCenter.default.post(name: Notification.Name(navigationObserverID!), object: nil, userInfo: userInfo)
    }

    private func showTab(_ tabIdentifier:TabIdentifier) {

        let userInfo:[AnyHashable : Any] = [
                AppViewAttribute.requestType: AppConstant.SHOW_TAB_REQUEST,
                AppViewAttribute.action: tabIdentifier
        ]
        NotificationCenter.default.post(name: Notification.Name(TabBarController.OBSERVER_ID), object: nil, userInfo: userInfo)
    }

    private func determineSeasonGroupData() {
        currentSeasonGroupRef = currentSeasonGroupRef != nil ? currentSeasonGroupRef : balancing.balancingModel.getDefaultVO()!.seasonGroupRef!
        seasonGroupVOs = viewed.viewHelper.getSeasonGroupViewVOs()
        byRefSeasonGroupVOs = [:]
        for seasonGroupVO:SeasonGroupViewVO in seasonGroupVOs! {
            byRefSeasonGroupVOs![seasonGroupVO.ref] = seasonGroupVO
        }
    }

    private func initializeFeedTypeSegment() {
        feedView.typeSegment.removeAllSegments()
        feedView.typeSegment.insertSegment(withTitle: "event_feed".localized, at: TypeIndexes.event.rawValue, animated: false)
        feedView.typeSegment.insertSegment(withTitle: "game_feed".localized, at: TypeIndexes.fixture.rawValue, animated: false)
        feedView.typeSegment.selectedSegmentIndex = TypeIndexes.event.rawValue
    }

    @objc func applyCurrentSettings(_ sender: Any?=nil) {

        feedView.seasonGroupBtn.setTitle(byRefSeasonGroupVOs![currentSeasonGroupRef!]!.text, for: .normal)

        var nextChildViewController: FeedTypeVC? = nil
        if feedView.typeSegment.selectedSegmentIndex == TypeIndexes.event.rawValue {
            nextChildViewController = eventVC
        } else {
            nextChildViewController = fixtureVC
        }
        if nextChildViewController == _mainContentVC {
            return
        }
        _mainContentVC?.hideMe()
        _mainContentVC = nextChildViewController
        _mainContentVC?.showMe( parentController: self, parentView: feedView.mainContentView )

    }

    private var eventVC: EventFeedTypeVC {
        var vc: EventFeedTypeVC? = eventViewControllers[currentSeasonGroupRef!]
        if vc != nil {
            return vc!
        }

        vc = EventFeedTypeVC(feedType: .event, seasonGroupRef: currentSeasonGroupRef!, isTeamFeed: isTeamFeed!, navigationObserverID: navigationObserverID!, pageObserverID:observerID!)

        eventViewControllers[currentSeasonGroupRef!] = vc

        return vc!
    }

    private var fixtureVC: FixtureFeedTypeVC {
        var vc: FixtureFeedTypeVC? = fixtureViewControllers[currentSeasonGroupRef!]
        if vc != nil {
            return vc!
        }

        vc = FixtureFeedTypeVC( feedType: .fixture, seasonGroupRef: currentSeasonGroupRef!, isTeamFeed: isTeamFeed!, navigationObserverID: navigationObserverID!, pageObserverID:observerID!)
        fixtureViewControllers[currentSeasonGroupRef!] = vc

        return vc!
    }

    private var seasonGroupPickerVC: PickerVC {
        if _seasonGroupPickerVC != nil {
            return _seasonGroupPickerVC!
        }
        _seasonGroupPickerVC = PickerVC(vos: seasonGroupVOs!, ref: currentSeasonGroupRef!)
        addObserver(self, selector: #selector(seasonGroupSelected(_:)), name: _seasonGroupPickerVC!.dataChangedNotification, object: nil)
        return _seasonGroupPickerVC!
    }

    @objc func showSeasonGroupPicker(_ sender: Any) {
        seasonGroupPickerVC.ref = currentSeasonGroupRef!
        let userInfo:[AnyHashable : Any] = [
                AppViewAttribute.requestType: AppConstant.SHOW_OVERLAY,
                AppViewAttribute.action: seasonGroupPickerVC
        ]
        NotificationCenter.default.post(name: Notification.Name(observerID!), object: nil, userInfo: userInfo)
    }

    @objc func seasonGroupSelected(_ sender: Any) {
        currentSeasonGroupRef = seasonGroupPickerVC.ref
        applyCurrentSettings()
    }

    override func addObservers() {
        addObserver(self, selector: #selector(seasonGroupSelected(_:)), name: _seasonGroupPickerVC?.dataChangedNotification, object: nil)
        super.addObservers()
    }

    override func addTargets() {
        feedView.seasonGroupBtn.addTarget(self, action: #selector(showSeasonGroupPicker(_:)), for: .touchUpInside)
        feedView.typeSegment.addTarget(self, action: #selector(applyCurrentSettings(_:)), for: .valueChanged)
        super.addTargets()
    }

    override func removeTargets() {
        feedView.seasonGroupBtn.removeAllTargets()
        feedView.typeSegment.removeTarget(nil, action: nil, for: .allEvents)
        super.removeTargets()
    }

    override func hideChildren() {
        _seasonGroupPickerVC?.hideMe()
        super.hideChildren()
    }

    func resetMainContentViewControllers() {
        for vc:EventFeedTypeVC in eventViewControllers.values {
            vc.destroy()
        }
        eventViewControllers = [:]
        for vc: FixtureFeedTypeVC in fixtureViewControllers.values {
            vc.destroy()
        }
        eventViewControllers = [:]
        fixtureViewControllers = [:]
        _mainContentVC = nil
    }

    deinit {
        resetMainContentViewControllers()
        _seasonGroupPickerVC?.destroy()
        _seasonGroupPickerVC = nil
    }

}
