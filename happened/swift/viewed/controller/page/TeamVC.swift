import UIKit

class TeamVC: VerticalScrollVC {

    private var app:App { return App.instance }
    private var happen: Happen { return Happen.instance }
    private var localization:Localization { return Localization.instance }
    private var preference: Preference { return Preference.instance }
    private var server: Server { return Server.instance }
    private var viewed: Viewed { return Viewed.instance }

    var teamListingRef: Int?
    var seasonRef: Int?
    var listingRef: Int?
    var teamRef: Int?
    var teamListingVO: TeamListingVO?
    var listingSeasonRef: Int?

    private var seasonVOs:[SeasonViewVO]?
    private var byRefSeasonVOs:[Int:SeasonViewVO]?

    private var listingSeasonVOs:[RefTextViewVO]?
    private var byRefListingSeasonVOs:[Int: RefTextViewVO]?

    private var _teamSeasonViewControllers: [Int:TeamListVC] = [:]
    private var _mainContentVC: TeamListVC?
    private var _seasonPickerVC: PickerVC?
    private var _listingSeasonPickerVC: PickerVC?

    var waitedForServer:Bool = false
    var viewReadyApplied:Bool = false

    var navigationObserverID:String?

    convenience init( _ teamListingRef:Int, navigationObserverID:String  ) {

        self.init(nibName: nil, bundle: nil)

        self.teamListingRef = teamListingRef

        self.navigationObserverID = navigationObserverID

        edgesForExtendedLayout = []

    }

    override func createAppView() -> TeamView {
        return TeamView()
    }

    var teamView:TeamView {
        return view as! TeamView
    }

    override func initialize() {
        super.initialize()
        applyObserverID()
    }

    override func viewWillFirstAppear() {
        super.viewWillFirstAppear()
        initializeTeam()
    }


    override func applyViewWillAppear() {
        super.applyViewWillAppear()
        if teamListingVO != nil {
            applyFollowing()
        }
    }

    override func viewDidFirstAppear() {
        super.viewDidFirstAppear()
        if !viewReadyApplied {
            applyViewReady()
        }
    }

    override func applyLocalization() {
        super.applyLocalization()
        teamView.followingTextView.applyTextVOs([TextVO("following".localized)])
        teamView.receiveAlertsTextView.applyTextVOs([TextVO("receive_alerts".localized)])
    }

    func initializeTeam( dataExpected:Bool = false ) {
        if ( seasonRef == nil || listingRef == nil || teamRef == nil ) && teamListingRef == nil {
            return
        }
        if seasonRef != nil && listingRef != nil && teamRef != nil {
            let listingSeasonVO:ListingSeasonVO? = happen.happenedModel.getListingSeasonVO(seasonRef!, listingRef!)
            if listingSeasonVO != nil {
                listingSeasonRef = listingSeasonVO!.ref
                teamListingVO = happen.happenedModel.getTeamListingVO(seasonRef!, listingRef!, teamRef!)
                if teamListingVO != nil {
                    teamListingRef = teamListingVO!.ref
                } else {
                    listingSeasonRef = nil
                }
            }
        } else {
            teamListingVO = happen.happenedModel.teamListingVOs[teamListingRef!]
            if teamListingVO != nil {
                let listingSeasonVO:ListingSeasonVO? = happen.happenedModel.getListingSeasonVO(teamListingVO!.seasonRef, teamListingVO!.listingRef)
                if listingSeasonVO != nil {
                    listingSeasonRef = listingSeasonVO!.ref
                    seasonRef = teamListingVO!.seasonRef
                    listingRef = teamListingVO!.listingRef
                    teamRef = teamListingVO!.teamRef
                } else {
                    teamListingVO = nil
                }
            }
        }

        applyViewInitialized()
        if teamListingVO == nil || !teamListingVO!.haveScenario {
            waitedForServer = true
            if !dataExpected {
                addActivityEndedObserver(selector: #selector(dataReceived(_:)))
                if seasonRef != nil && listingRef != nil && teamRef != nil {
                    server.teamScenarioServerCommand().command(seasonRef: seasonRef!, listingRef: listingRef!, teamRef: teamRef!)
                } else {
                    server.teamScenarioServerCommand().command(teamListingRef: teamListingRef!)
                }
            } else {
                app.updateErrorAppCommand().command()
            }
        } else {
            applyViewReady()
        }
    }

    func applyViewInitialized() {
        if !viewWillFirstAppearApplied {
            return
        }
        teamView.listingSeasonBtn.isHidden = true
        if teamListingVO == nil {
            self.navigationItem.title = ""
            teamView.seasonBtn.setTitle("", for: .normal)
            teamView.listingSeasonBtn.setTitle("", for: .normal)
            return
        }
        applyFollowing()
        let teamName:String = localization.localizationHelper.getTeamName(teamListingVO!.teamRef, seasonRef: teamListingVO!.seasonRef)
        self.navigationItem.title = teamName
        determineSeasonData()
        let seasonName:String = byRefSeasonVOs![teamListingVO!.seasonRef] != nil ? byRefSeasonVOs![teamListingVO!.seasonRef]!.text : ""
        teamView.seasonBtn.setTitle(seasonName, for: .normal)
        if listingSeasonRef != nil {
            let listingSeasonVO:ListingSeasonVO? = happen.happenedModel.listingSeasonVOs[listingSeasonRef!]
            if listingSeasonVO != nil && listingSeasonVO!.haveData {
                determineListingData()
                teamView.listingSeasonBtn.setTitle(byRefListingSeasonVOs![listingSeasonRef!]!.text, for: .normal)
                if listingSeasonVOs!.count > 1{
                    teamView.listingSeasonBtn.isHidden = false
                }
            }
        }
        let listingName:String = localization.localizationHelper.getCompetitionOrListingName(teamListingVO!.listingRef, seasonRef: teamListingVO!.seasonRef, allowDefault: true)!
        teamView.teamTextView.applyTextVOs([TextVO(teamName)], withTextAlignment: .center)
        teamView.listingSeasonTextView.applyTextVOs([TextVO(listingName), TextVO(" "), TextVO(seasonName)], withTextAlignment: .center)
    }

    func applyViewReady() {
        if !viewWillFirstAppearApplied || teamListingVO == nil || !teamListingVO!.haveScenario {
            return
        }
        viewReadyApplied = true

        displayTeamContent()
    }

    private func determineSeasonData() {
        let teamVO:TeamVO = happen.happenedModel.teamVOs[teamListingVO!.teamRef]!
        var seasonRefs:[Int:NSNull] = [:]
        for seasonRef:Int in teamVO.teamSeasonVOs.keys {
            seasonRefs[seasonRef] = NSNull()
        }
        seasonVOs = viewed.viewHelper.getSeasonViewVOs(seasonRefs)
        byRefSeasonVOs = [:]
        for seasonVO:SeasonViewVO in seasonVOs! {
            byRefSeasonVOs![seasonVO.ref] = seasonVO
        }
    }

    private func determineListingData() {
        let teamVO:TeamVO = happen.happenedModel.teamVOs[teamListingVO!.teamRef]!
        let teamSeasonVO:TeamSeasonVO = teamVO.teamSeasonVOs[teamListingVO!.seasonRef]!
        var listingSeasonRefs:[Int:NSNull] = [:]
        for listingRef:Int in teamSeasonVO.teamListingRefs.keys {
            let listingSeasonVO:ListingSeasonVO = happen.happenedModel.getListingSeasonVO(self.seasonRef!, listingRef)!
            listingSeasonRefs[listingSeasonVO.ref] = NSNull()
        }
        listingSeasonVOs = viewed.viewHelper.getListingSeasonViewVOs(listingSeasonRefs)
        listingSeasonPickerVC.vos = listingSeasonVOs
        byRefListingSeasonVOs = [:]
        for listingSeasonVO:RefTextViewVO in listingSeasonVOs! {
            byRefListingSeasonVOs![listingSeasonVO.ref] = listingSeasonVO
        }
    }

    var nextVC: TeamListVC {
        var vc: TeamListVC? = _teamSeasonViewControllers[listingSeasonRef!]
        if vc != nil {
            return vc!
        }

        vc = TeamListVC( teamListingVO: teamListingVO!, navigationObserverID: navigationObserverID!)

        _teamSeasonViewControllers[listingSeasonRef!] = vc!
        return vc!
    }

    func displayTeamContent( waitedForView:Bool=false ) {

        let vc:TeamListVC? = nextVC
        if vc != nil && vc != _mainContentVC {
            if waitedForServer || vc!.viewDidFirstAppearApplied || waitedForView {
                _mainContentVC?.hideMe()
                _mainContentVC = vc
                nextVC.showMe(parentController: self, parentView: teamView.mainContentView)
            } else {
                showViewLoadingActivityIndicator(jumpToDisplayTeamContent)
            }
        }

        waitedForServer = false
    }

    func jumpToDisplayTeamContent() {
        displayTeamContent( waitedForView:true )
    }

    @objc func dataReceived(_ sender: Any?=nil) {
        removeActivityEndedObserver()
        initializeTeam( dataExpected: true )
    }

    @objc func followingToggled(_ sender: Any) {
        if teamListingVO == nil {
            return
        }
        let on:Bool = teamView.followingSwitch.isOn
        let teamSubscriptions:[Int:Bool] = preference.preferenceHelper.getTeamSubscriptions()
        let wasSubscribed:Bool = teamSubscriptions[teamListingVO!.teamRef] != nil && teamSubscriptions[teamListingVO!.teamRef]!
        preference.preferenceHelper.modifyTeams(teamListingVO!.teamRef, selected: on, subscribed: false)
        applyFollowing()
        if wasSubscribed && !on {
            viewed.requirePushAuthorizationViewCommand().command(viewController: self, on: false, subscriptionsDidChange: true, allowNotSupportedError: false)
        }
    }

    @objc func receiveAlertsToggled(_ sender: Any) {
        if teamListingVO == nil {
            return
        }
        let on:Bool = teamView.receiveAlertsSwitch.isOn
        preference.preferenceHelper.modifyTeams(teamListingVO!.teamRef, selected: true, subscribed: on)
        viewed.requirePushAuthorizationViewCommand().command(viewController: self, on: on, subscriptionsDidChange: true)
    }

    func applyFollowing() {
        let teamSubscriptions:[Int:Bool] = preference.preferenceHelper.getTeamSubscriptions()
        let value:Bool? = teamSubscriptions[teamListingVO!.teamRef]
        if value == nil {
            teamView.followingSwitch.isOn = false
            teamView.receiveAlertsSwitch.isOn = false
            teamView.receiveAlertsSwitch.isEnabled = false
            return
        }
        teamView.followingSwitch.isOn = true
        teamView.receiveAlertsSwitch.isEnabled = true
        teamView.receiveAlertsSwitch.isOn = value!
    }

    override func addTargets() {

        teamView.seasonBtn.addTarget(self, action: #selector(showSeasonPicker(_:)), for: .touchUpInside)
        teamView.listingSeasonBtn.addTarget(self, action: #selector(showListingSeasonPicker(_:)), for: .touchUpInside)
        teamView.followingSwitch.addTarget(self, action: #selector(followingToggled(_:)), for: .valueChanged)
        teamView.receiveAlertsSwitch.addTarget(self, action: #selector(receiveAlertsToggled(_:)), for: .valueChanged)
        super.addTargets()
    }

    override func removeTargets() {
        teamView.seasonBtn.removeAllTargets()
        teamView.listingSeasonBtn.removeAllTargets()
        super.removeTargets()
    }

    @objc func seasonSelected(_ sender: Any) {
        if seasonPickerVC.ref! == teamListingVO!.seasonRef {
            return
        }

        _mainContentVC?.hideMe()
        seasonRef = seasonPickerVC.ref!
        teamRef = teamListingVO!.teamRef
        listingRef = viewed.viewHelper.getPreferredListingRef(seasonRef: seasonRef!, listingRef: teamListingVO!.listingRef, teamRef: teamRef!)!
        teamListingRef = nil
        teamListingVO = nil
        listingSeasonRef = nil
        initializeTeam()
    }

    @objc func listingSeasonSelected(_ sender: Any) {
        if listingSeasonPickerVC.ref! == listingSeasonRef! {
            return
        }

        let listingSeasonVO:ListingSeasonVO = happen.happenedModel.listingSeasonVOs[listingSeasonPickerVC.ref!]!

        _mainContentVC?.hideMe()
        seasonRef = teamListingVO!.seasonRef
        listingRef = listingSeasonVO.listingRef
        teamRef = teamListingVO!.teamRef
        teamListingRef = nil
        teamListingVO = nil
        listingSeasonRef = nil
        initializeTeam()
    }

    @objc func showSeasonPicker(_ sender: Any) {

        seasonPickerVC.ref = seasonRef
        let userInfo:[AnyHashable : Any] = [
                AppViewAttribute.requestType: AppConstant.SHOW_OVERLAY,
                AppViewAttribute.action: seasonPickerVC
        ]
        NotificationCenter.default.post(name: Notification.Name(observerID!), object: nil, userInfo: userInfo)
    }

    @objc func showListingSeasonPicker(_ sender: Any) {
        listingSeasonPickerVC.ref = listingSeasonRef
        let userInfo:[AnyHashable : Any] = [
                AppViewAttribute.requestType: AppConstant.SHOW_OVERLAY,
                AppViewAttribute.action: listingSeasonPickerVC
        ]
        NotificationCenter.default.post(name: Notification.Name(observerID!), object: nil, userInfo: userInfo)
    }

    private var seasonPickerVC: PickerVC {
        if _seasonPickerVC != nil {
            return _seasonPickerVC!
        }
        _seasonPickerVC = PickerVC(vos: seasonVOs!, ref: teamListingVO!.seasonRef)
        addObserver(self, selector: #selector(seasonSelected(_:)), name: _seasonPickerVC!.dataChangedNotification, object: nil)
        return _seasonPickerVC!
    }

    private var listingSeasonPickerVC: PickerVC {
        if _listingSeasonPickerVC != nil {
            return _listingSeasonPickerVC!
        }
        _listingSeasonPickerVC = PickerVC(vos: listingSeasonVOs!, ref: listingSeasonRef!)
        addObserver(self, selector: #selector(listingSeasonSelected(_:)), name: _listingSeasonPickerVC!.dataChangedNotification, object: nil)
        return _listingSeasonPickerVC!
    }

    override func addObservers() {
        addObserver(self, selector: #selector(seasonSelected(_:)), name: _seasonPickerVC?.dataChangedNotification, object: nil)
        addObserver(self, selector: #selector(listingSeasonSelected(_:)), name: _listingSeasonPickerVC?.dataChangedNotification, object: nil)
        super.addObservers()
    }

    override func hideChildren() {
        _seasonPickerVC?.hideMe()
        _listingSeasonPickerVC?.hideMe()
        super.hideChildren()
    }

    override func destroy() {
        for vc:TeamListVC in _teamSeasonViewControllers.values {
            vc.destroy()
        }
        _teamSeasonViewControllers = [:]
        _mainContentVC?.destroy()
        _seasonPickerVC?.destroy()
        _seasonPickerVC = nil
        _listingSeasonPickerVC?.destroy()
        _listingSeasonPickerVC = nil
        super.destroy()
    }
}
