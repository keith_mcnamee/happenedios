import UIKit

class AppLoadingVC: ViewController {

    private var _errorStatus: AppErrorStatus = .noError

    convenience init( ) {
        self.init(nibName: nil, bundle: nil)
    }

    override func createAppView() -> AppLoadingView {
        return AppLoadingView()
    }

    var appLoadingView:AppLoadingView {
        return view as! AppLoadingView
    }

    var errorStatus: AppErrorStatus {
        set {
            _errorStatus = newValue
            updateDisplay()
        }
        get {
            return _errorStatus
        }
    }

    private func updateDisplay() {
        if _errorStatus == .noError {
            appLoadingView.applyViewType(isErrorView: false)
        } else {
            appLoadingView.applyViewType(isErrorView: true)
            if _errorStatus == .connectionError {
                appLoadingView.errorTextView.applyTextVOs([TextVO("connection_error".localized)])
            } else {
                appLoadingView.errorTextView.applyTextVOs([TextVO("data_error".localized)])
            }
        }
    }

    override func orientationDidChange() {
        super.orientationDidChange()
        appLoadingView.adjustForOrientation()
    }
}
