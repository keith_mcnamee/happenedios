import UIKit

class CompetitionVC: VerticalScrollVC {

    private var balancing: Balancing { return Balancing.instance }
    private var viewed: Viewed { return Viewed.instance }

    var navigationObserverID:String?

    private var currentSeasonGroupRef:Int?
    private var seasonGroupVOs:[SeasonGroupViewVO]?
    private var byRefSeasonGroupVOs:[Int:SeasonGroupViewVO]?

    private var listViewControllers:[Int: CompetitionAccordionVC] = [:]
    private var _mainContentVC: CompetitionAccordionVC?
    private var _seasonGroupPickerVC: PickerVC?

    convenience init( navigationObserverID:String? = nil ) {
        self.init(nibName: nil, bundle: nil)

        self.navigationObserverID = navigationObserverID
    }

    override func createAppView() -> CompetitionView {
        return CompetitionView()
    }

    var competitionView:CompetitionView {
        return view as! CompetitionView
    }

    override func initialize() {
        super.initialize()
        applyObserverID()
    }

    override func viewWillFirstAppear() {
        super.viewWillFirstAppear()
        determineSeasonGroupData()
        competitionView.seasonGroupBtn.setTitle(byRefSeasonGroupVOs![currentSeasonGroupRef!]!.text, for: .normal)
    }

    override func applyLocalization() {
        super.applyLocalization()
        competitionView.yourLeaguesTextView.applyTextVOs([TextVO("your_leagues".localized)])
    }

    override func resetView() {
        super.resetView()
        applyCurrentSettings()
    }

    private func determineSeasonGroupData() {
        currentSeasonGroupRef = currentSeasonGroupRef != nil ? currentSeasonGroupRef : balancing.balancingModel.getDefaultVO()!.seasonGroupRef!
        seasonGroupVOs = viewed.viewHelper.getSeasonGroupViewVOs()
        byRefSeasonGroupVOs = [:]
        for seasonGroupVO:SeasonGroupViewVO in seasonGroupVOs! {
            byRefSeasonGroupVOs![seasonGroupVO.ref] = seasonGroupVO
        }
    }

    func applyCurrentSettings(_ sender: Any?=nil) {

        competitionView.seasonGroupBtn.setTitle(byRefSeasonGroupVOs![currentSeasonGroupRef!]!.text, for: .normal)

        let nextChildViewController: CompetitionAccordionVC = listVC

        if nextChildViewController == _mainContentVC {
            return
        }
        _mainContentVC?.hideMe()
        _mainContentVC = nextChildViewController
        _mainContentVC!.showMe( parentController: self, parentView: competitionView.mainContentView )
    }

    var listVC: CompetitionAccordionVC {

        var vc: CompetitionAccordionVC? = listViewControllers[currentSeasonGroupRef!]
        if vc != nil {
            return vc!
        }

        vc = CompetitionAccordionVC(seasonGroupRef: currentSeasonGroupRef!, navigationObserverID: navigationObserverID!, pageObserverID:observerID!, verticalScrollObserverID:observerID!)

        listViewControllers[currentSeasonGroupRef!] = vc

        return vc!
    }

    private var seasonGroupPickerVC: PickerVC {
        if _seasonGroupPickerVC != nil {
            return _seasonGroupPickerVC!
        }
        _seasonGroupPickerVC = PickerVC(vos: seasonGroupVOs!, ref: currentSeasonGroupRef!)
        addObserver(self, selector: #selector(seasonGroupSelected(_:)), name: _seasonGroupPickerVC!.dataChangedNotification, object: nil)
        return _seasonGroupPickerVC!
    }

    @objc func showSeasonGroupPicker(_ sender: Any) {
        seasonGroupPickerVC.ref = currentSeasonGroupRef!
        let userInfo:[AnyHashable : Any] = [
                AppViewAttribute.requestType: AppConstant.SHOW_OVERLAY,
                AppViewAttribute.action: seasonGroupPickerVC
        ]
        NotificationCenter.default.post(name: Notification.Name(observerID!), object: nil, userInfo: userInfo)
    }

    @objc func seasonGroupSelected(_ sender: Any) {
        currentSeasonGroupRef = seasonGroupPickerVC.ref
        applyCurrentSettings()
    }

    override func addObservers() {
        addObserver(self, selector: #selector(seasonGroupSelected(_:)), name: _seasonGroupPickerVC?.dataChangedNotification, object: nil)
        super.addObservers()
    }

    override func addTargets() {
        competitionView.seasonGroupBtn.addTarget(self, action: #selector(showSeasonGroupPicker(_:)), for: .touchUpInside)
        super.addTargets()
    }

    override func removeTargets() {
        competitionView.seasonGroupBtn.removeAllTargets()
        super.removeTargets()
    }

    override func hideChildren() {
        _seasonGroupPickerVC?.hideMe()
        super.hideChildren()
    }

    deinit {
        for vc: CompetitionAccordionVC in listViewControllers.values {
            vc.destroy()
        }
        listViewControllers = [:]
        _seasonGroupPickerVC?.destroy()
        _seasonGroupPickerVC = nil
        _mainContentVC = nil
    }

}
