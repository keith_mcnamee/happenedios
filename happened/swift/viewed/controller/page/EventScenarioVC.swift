import UIKit

class EventScenarioVC: ScenarioVC {

    var eventRef: Int?
    var toHappen: Bool?

    var eventScenarioVO: EventScenarioVO? {
        get {
            return scenarioVO as? EventScenarioVO
        }
        set {
            scenarioVO = newValue
        }
    }

    convenience init( _ listingScenarioRef:Int, navigationObserverID:String  ) {

        self.init( scenarioRef:listingScenarioRef, navigationObserverID:navigationObserverID )

    }

    override func createAppView() -> EventScenarioView {
        return EventScenarioView()
    }

    var eventScenarioView:EventScenarioView {
        return view as! EventScenarioView
    }

    override var validScenarioElements:Bool {
        return seasonRef != nil && listingRef != nil && teamRef != nil && eventRef != nil && toHappen != nil
    }

    override var scenarioVoFromElements:ScenarioTypeVO? {
        return happen.happenedModel.getEventScenarioVO(seasonRef!, listingRef!, teamRef!, eventRef!, toHappen!)
    }

    override func altSeasonScenarioVO( _ seasonRef:Int ) -> ScenarioTypeVO? {
        return happen.happenedModel.getEventScenarioVO(seasonRef, self.listingRef!, self.teamRef!, self.eventRef!, self.toHappen!)
    }

    override var altScenarioVO: ScenarioTypeVO? {
        return happen.happenedModel.getEventScenarioVO(seasonRef!, listingRef!, teamRef!, eventRef!, !toHappen!)
    }

    override func altSeasonAltScenarioVO( _ seasonRef:Int ) -> ScenarioTypeVO? {
        return happen.happenedModel.getEventScenarioVO(seasonRef, self.listingRef!, self.teamRef!, self.eventRef!, !self.toHappen!)
    }

    override func applyAlt() {
        self.toHappen! = !self.toHappen!
    }

    override var scenarioVoFromRef:ScenarioTypeVO? {
        return happen.happenedModel.eventScenarioVOs[scenarioRef!]
    }

    override func takeElementsFromScenarioVO() {
        super.takeElementsFromScenarioVO()
        eventRef = eventScenarioVO!.eventRef
        toHappen = eventScenarioVO!.toHappen
    }

    override func requestServerByRef() {
        server.teamScenarioServerCommand().command(eventScenarioRef: scenarioRef!)
    }

    override func prepareViewVO() -> ScenarioViewVO {
        return viewed.prepareEventScenarioService().prepareVO(eventScenarioVO: eventScenarioVO!, navigationObserverID:navigationObserverID!)
    }

    override func applyViewReady() {
        if !viewWillFirstAppearApplied || eventScenarioVO == nil || teamListingVO == nil || !teamListingVO!.haveScenario  {
            return
        }
        let eventName:String = localization.localizationHelper.getCompetitionEventTitle( eventScenarioVO!.listingRef, eventScenarioVO!.eventRef, seasonRef: eventScenarioVO!.seasonRef)
        eventScenarioView.eventTextView.applyTextVOs([TextVO(eventName)])
        applyPossibleCertainResults()

        let descriptionVOs:[TextVO] = localization.localizationHelper.getEventScenarioDescription(eventScenarioRef: eventScenarioVO!.ref)!
        eventScenarioView.resultBodyTextView.applyTextVOs(descriptionVOs)

        let dateFormatter:DateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yy hh:mm"+viewed.viewConfig.nonBreakCode+"a"
        let occurredDateString:String = !eventScenarioVO!.invalid ? dateFormatter.string(from: eventScenarioVO!.occurredDate!) : ""

        eventScenarioView.resultWhenTextView.applyTextVOs([TextVO("when".localized), TextVO(" "), TextVO(occurredDateString, attributes: [.bold:NSNull(), .pointSizeStyle:UIFont.TextStyle.title1])])

        super.applyViewReady()
    }

    func applyPossibleCertainResults() {
        if eventScenarioVO == nil {
            return
        }

        let eventBalancingVO:EventBalancingVO = balancing.balancingModel.getEventVO(eventScenarioVO!.eventRef, seasonRef: eventScenarioVO!.seasonRef)!

        eventScenarioView.possibleContainerViewHidden = isHistoricalSeason

        var possibleID:String = ""
        var certainID:String = ""
        if eventScenarioVO!.toHappen {
            if isHistoricalSeason {
                certainID = "did_it_happen"
            } else {
                possibleID = "can_it_happen"
                certainID = "has_it_happened"
            }
        } else {
            if isHistoricalSeason {
                certainID = eventBalancingVO.positive! ? "did_it_fail_to_happen" : "was_it_avoided"
            } else {
                possibleID = eventBalancingVO.positive! ? "can_they_miss_out" : "can_it_be_avoided"
                certainID = "can_it_be_ruled_out"
            }
        }
        eventScenarioView.possibleTextView.applyTextVOs([TextVO(possibleID.localized)])
        eventScenarioView.certainTextView.applyTextVOs([TextVO(certainID.localized)])

        let possibleIsYes:Bool = !happen.happenedHelper.eventScenarioCanNotOccur(primaryVO: eventScenarioVO, secondaryVO: altScenarioVO as? EventScenarioVO)
        let certainIsYes:Bool = happen.happenedHelper.eventScenarioOccurred(primaryVO: eventScenarioVO, secondaryVO: altScenarioVO as? EventScenarioVO)

        eventScenarioView.applyPossibleCertainImage(certain: false, isYes: possibleIsYes)
        eventScenarioView.applyPossibleCertainImage(certain: true, isYes: certainIsYes)
    }
}
