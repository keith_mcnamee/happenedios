import UIKit

class ListingVC: VerticalScrollVC {

    private var app:App { return App.instance }
    private var happen: Happen { return Happen.instance }
    private var localization:Localization { return Localization.instance }
    private var server: Server { return Server.instance }
    private var viewed: Viewed { return Viewed.instance }

    var navigationObserverID:String?

    var listingSeasonRef: Int?
    var seasonRef: Int?
    var listingRef: Int?
    var listingSeasonVO: ListingSeasonVO?
    private var seasonVOs:[SeasonViewVO]?
    private var byRefSeasonVOs:[Int:SeasonViewVO]?

    private var _seasonViewControllers: [Int:StandingContainerVC] = [:]
    private var _mainContentVC: StandingContainerVC?
    private var _seasonPickerVC: PickerVC?

    var waitedForServer:Bool = false
    var viewReadyApplied:Bool = false

    convenience init( _ listingSeasonRef:Int, navigationObserverID:String  ) {

        self.init(nibName: nil, bundle: nil)

        self.listingSeasonRef = listingSeasonRef
        self.navigationObserverID = navigationObserverID

        edgesForExtendedLayout = []

    }

    override func createAppView() -> ListingView {
        return ListingView()
    }

    var listingView:ListingView {
        return view as!ListingView
    }

    override func initialize() {
        super.initialize()
        applyObserverID()
    }

    override func viewWillFirstAppear() {
        super.viewWillFirstAppear()
        initializeListing()
    }

    override func viewDidFirstAppear() {
        super.viewDidFirstAppear()
        if !viewReadyApplied {
            applyViewReady()
        }
    }

    func initializeListing( dataExpected:Bool = false ) {
        if ( seasonRef == nil || listingRef == nil ) && listingSeasonRef == nil {
            return
        }

        if seasonRef != nil && listingRef != nil {
            listingSeasonVO = happen.happenedModel.getListingSeasonVO(seasonRef!, listingRef!)
            if listingSeasonVO != nil {
                listingSeasonRef = listingSeasonVO!.ref
            }
        } else {
            listingSeasonVO = happen.happenedModel.listingSeasonVOs[listingSeasonRef!]
            if listingSeasonVO != nil {
                seasonRef = listingSeasonVO!.seasonRef
                listingRef = listingSeasonVO!.listingRef
            }
        }

        applyViewInitialized()
        if listingSeasonVO == nil || !listingSeasonVO!.haveData {
            waitedForServer = true
            if !dataExpected {
                addActivityEndedObserver(selector: #selector(dataReceived(_:)))
                if seasonRef != nil && listingRef != nil {
                    server.listingSeasonServerCommand().command(seasonRef: seasonRef!, listingRef: listingRef!)
                } else {
                    server.listingSeasonServerCommand().command(listingSeasonRef: listingSeasonRef!)
                }
            } else {
                app.updateErrorAppCommand().command()
            }
        } else {
            applyViewReady()
        }
    }

    func applyViewInitialized() {
        if !viewWillFirstAppearApplied {
            return
        }
        if listingSeasonVO == nil {
            self.navigationItem.title = ""
            listingView.seasonBtn.setTitle("", for: .normal)
            return
        }
        let listingName:String = localization.localizationHelper.getCompetitionOrListingName(listingSeasonVO!.listingRef, seasonRef: listingSeasonVO!.seasonRef, allowDefault: true)!
        self.navigationItem.title = listingName
        determineSeasonData()
        let seasonName:String = byRefSeasonVOs![listingSeasonVO!.seasonRef] != nil ? byRefSeasonVOs![listingSeasonVO!.seasonRef]!.text : ""
        listingView.seasonBtn.setTitle(seasonName, for: .normal)
        listingView.listingSeasonTextView.applyTextVOs([TextVO(listingName), TextVO(" "), TextVO(seasonName)], withTextAlignment: .center)
    }

    func applyViewReady() {
        if !viewDidFirstAppearApplied || listingSeasonVO == nil || !listingSeasonVO!.haveData {
            return
        }
        viewReadyApplied = true
        displayListingContent()
    }

    @objc func dataReceived(_ sender: Any?=nil) {
        removeActivityEndedObserver()
        initializeListing( dataExpected: true )
    }

    private func determineSeasonData() {
        let listingVO:ListingVO = happen.happenedModel.listingVOs[listingSeasonVO!.listingRef]!
        var seasonRefs:[Int:NSNull] = [:]
        for seasonRef:Int in listingVO.listingSeasonRefs.keys {
            seasonRefs[seasonRef] = NSNull()
        }
        seasonVOs = viewed.viewHelper.getSeasonViewVOs(seasonRefs)
        byRefSeasonVOs = [:]
        for seasonVO:SeasonViewVO in seasonVOs! {
            byRefSeasonVOs![seasonVO.ref] = seasonVO
        }
    }

    var nextVC: StandingContainerVC? {
        var vc: StandingContainerVC? = _seasonViewControllers[listingSeasonVO!.seasonRef]
        if vc != nil {
            return vc!
        }

        let vo:StandingListVO? = standingListVO
        if vo == nil {
            return nil
        }
        vc = StandingContainerVC( standingListVO: vo!, verticalScrollObserverID: observerID! )

        _seasonViewControllers[listingSeasonVO!.seasonRef] = vc!

        return vc!
    }

    override func addObservers() {
        addObserver(self, selector: #selector(seasonSelected(_:)), name: _seasonPickerVC?.dataChangedNotification, object: nil)
        super.addObservers()
    }

    func displayListingContent( waitedForView:Bool=false ) {
        let vc:StandingContainerVC? = nextVC
        if vc == nil {
            app.updateErrorAppCommand().command()
            return
        } else if vc != _mainContentVC {
            if waitedForServer || vc!.viewDidFirstAppearApplied || waitedForView {
                _mainContentVC?.hideMe()
                _mainContentVC = vc
                nextVC!.showMe(parentController: self, parentView: listingView.mainContentView)
            } else {
                showViewLoadingActivityIndicator(jumpToDisplayListingContent)
            }
        }

        waitedForServer = false
    }

    func jumpToDisplayListingContent() {
        displayListingContent( waitedForView:true )
    }

    var standingListVO: StandingListVO? {
        return viewed.prepareStandingService().prepareStandingVO(listingSeasonVO:listingSeasonVO!, calculatorVO: listingSeasonVO!.calculatorVO, navigationObserverID:navigationObserverID!)
    }

    override func addTargets() {

        listingView.seasonBtn.addTarget(self, action: #selector(showSeasonPicker(_:)), for: .touchUpInside)
        super.addTargets()
    }

    @objc func showSeasonPicker(_ sender: Any) {
        seasonPickerVC.ref = seasonRef
        let userInfo:[AnyHashable : Any] = [
                AppViewAttribute.requestType: AppConstant.SHOW_OVERLAY,
                AppViewAttribute.action: seasonPickerVC
        ]
        NotificationCenter.default.post(name: Notification.Name(observerID!), object: nil, userInfo: userInfo)
    }

    override func removeTargets() {
        listingView.seasonBtn.removeAllTargets()
        super.removeTargets()
    }

    @objc func seasonSelected(_ sender: Any) {
        if seasonPickerVC.ref! == listingSeasonVO!.seasonRef {
            return
        }

        _mainContentVC?.hideMe()

        seasonRef = seasonPickerVC.ref!
        listingRef = listingSeasonVO!.listingRef
        listingSeasonRef = nil
        listingSeasonVO = nil

        initializeListing()
    }

    private var seasonPickerVC: PickerVC {
        if _seasonPickerVC != nil {
            return _seasonPickerVC!
        }
        _seasonPickerVC = PickerVC(vos: seasonVOs!, ref: listingSeasonVO!.seasonRef)
        addObserver(self, selector: #selector(seasonSelected(_:)), name: _seasonPickerVC!.dataChangedNotification, object: nil)
        return _seasonPickerVC!
    }

    override func hideChildren() {
        _seasonPickerVC?.hideMe()
        super.hideChildren()
    }

    override func destroy() {
        for vc:StandingContainerVC in _seasonViewControllers.values {
            vc.destroy()
        }
        _seasonViewControllers = [:]
        _seasonPickerVC?.destroy()
        _seasonPickerVC = nil
        _mainContentVC = nil
        super.destroy()
    }
}
