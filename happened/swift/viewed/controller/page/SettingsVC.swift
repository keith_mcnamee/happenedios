import UIKit

class SettingsVC: VerticalScrollVC {

    var localization: Localization { return Localization.instance }
    private var preference: Preference { return Preference.instance }
    private var viewed: Viewed { return Viewed.instance }

    private var _mainContentVC: SettingsListVC?

    convenience init( tabIdentifier: TabIdentifier) {
        self.init(nibName: nil, bundle: nil)

        self.tabIdentifier = tabIdentifier
    }

    override func createAppView() -> SettingsView {
        return SettingsView()
    }

    var settingsView:SettingsView {
        return view as! SettingsView
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        viewed.viewModel.visibleTab = tabIdentifier
        viewed.launchRequestedPageViewCommand().checkForWaitingTab()
    }

    override func applyViewWillDisappear() {
        super.applyViewWillDisappear()
        if viewed.viewModel.visibleTab == tabIdentifier {
            viewed.viewModel.visibleTab = nil
        }
    }

    override func applyLocalization() {
        super.applyLocalization()
        settingsView.manageTextView.applyTextVOs([TextVO("manage_teams".localized)], additionalAttributes: [.pointSizeStyle:UIFont.TextStyle.title1])
    }

    override func resetView() {
        super.resetView()
        let teamSubscriptions:[Int:Bool] = preference.preferenceHelper.getTeamSubscriptions()
        if teamSubscriptions.count == 0 {
            _mainContentVC?.removeAll()
            settingsView.mainContentViewHidden = true
            showFindTeams( haveTeams: false )
        } else {
            settingsView.mainContentViewHidden = false
            mainContentVC.applyCurrentState(teamSubscriptions: teamSubscriptions)
            showFindTeams( haveTeams: true )
        }
    }

    var mainContentVC: SettingsListVC {
        if _mainContentVC != nil {
            return _mainContentVC!
        }
        _mainContentVC = SettingsListVC()
        _mainContentVC!.showMe(parentController: self, parentView: settingsView.mainContentView)
        return _mainContentVC!
    }

    func showFindTeams( haveTeams:Bool ) {

        let textID:String = !haveTeams ? "not_following" : "find_more"
        let replaceWith:[String:String] = [
                "find": "find".localized
        ]
        let observerIDs:[String:String]  = [
                "find": TabBarController.OBSERVER_ID
        ]
        let requestTypes:[String:String]  = [
                "find": AppConstant.SHOW_TAB_REQUEST
        ]
        let actions:[String:Any]  = [
                "find": TabIdentifier.findTeams
        ]

        let textVOs:[TextVO] = localization.localizationHelper.localizeVOs(
                textID,
                replaceWith,
                observerIDs: observerIDs,
                requestTypes: requestTypes,
                actions: actions,
                localizationType: LocalizationConstant.NATIVE_TYPE
        )!
        settingsView.statusTextView.applyTextVOs(textVOs)
    }

    override func destroy() {
        _mainContentVC?.destroy()
        _mainContentVC = nil
        super.destroy()
    }
}
