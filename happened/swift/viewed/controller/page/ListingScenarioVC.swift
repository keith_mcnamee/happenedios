import UIKit

class ListingScenarioVC: ScenarioVC {

    var best: Bool?

    var listingScenarioVO: ListingScenarioVO? {
        get {
            return scenarioVO as? ListingScenarioVO
        }
        set {
            scenarioVO = newValue
        }
    }

    convenience init( _ listingScenarioRef:Int, navigationObserverID:String  ) {

        self.init( scenarioRef:listingScenarioRef, navigationObserverID:navigationObserverID )

    }

    override func createAppView() -> ListingScenarioView {
        return ListingScenarioView()
    }

    var listingScenarioView:ListingScenarioView {
        return view as! ListingScenarioView
    }

    override var validScenarioElements:Bool {
        return seasonRef != nil && listingRef != nil && teamRef != nil && best != nil
    }

    override var scenarioVoFromElements:ScenarioTypeVO? {
        return happen.happenedModel.getListingScenarioVO(seasonRef!, listingRef!, teamRef!, best!)
    }

    override func altSeasonScenarioVO( _ seasonRef:Int ) -> ScenarioTypeVO? {
        return happen.happenedModel.getListingScenarioVO(seasonRef, self.listingRef!, self.teamRef!, self.best!)
    }

    override var scenarioVoFromRef:ScenarioTypeVO? {
        return happen.happenedModel.listingScenarioVOs[scenarioRef!]
    }

    override func takeElementsFromScenarioVO() {
        super.takeElementsFromScenarioVO()
        best = listingScenarioVO!.best
    }

    override func requestServerByRef() {
        server.teamScenarioServerCommand().command(listingScenarioRef: scenarioRef!)
    }

    override func prepareViewVO() -> ScenarioViewVO {
        return viewed.prepareListingScenarioService().prepareVO(listingScenarioVO: listingScenarioVO!, navigationObserverID:navigationObserverID!)
    }

    override func applyViewReady() {
        if !viewWillFirstAppearApplied || listingScenarioVO == nil || teamListingVO == nil || !teamListingVO!.haveScenario  {
            return
        }

        let positionTextID:String = isHistoricalSeason ? "final_position" : listingScenarioVO!.best ? "best_position" : "worst_position"
        let positionValue:Int = listingScenarioVO!.position != nil ? listingScenarioVO!.position! : 0
        let position:String = NumberFormatter.localizedString(from: NSNumber(value: positionValue), number: .ordinal)
        listingScenarioView.positionTextView.applyTextVOs([TextVO(positionTextID.localized), TextVO(" "), TextVO(position, attributes:[.bold:NSNull(), .pointSizeStyle:UIFont.TextStyle.title1])], withTextAlignment: .center)

        super.applyViewReady()

    }
}
