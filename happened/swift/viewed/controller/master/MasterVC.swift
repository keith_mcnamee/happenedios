import UIKit

class MasterVC: UIViewController {

    private var app:App { return App.instance }

    private var _appLoadingVC:AppLoadingVC? = nil
    private var _tabBarController: TabBarController? = nil

    private var appLoadingViewController: AppLoadingVC {
        if _appLoadingVC != nil {
            return _appLoadingVC!
        }

        _appLoadingVC = AppLoadingVC()

        self.add(asChildViewController: _appLoadingVC!)

        return _appLoadingVC!
    }

    private var appTabBarController: TabBarController {
        if _tabBarController != nil {
            return _tabBarController!
        }

        _tabBarController = TabBarController()

        self.add(asChildViewController: _tabBarController!)

        return _tabBarController!
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(updateView(_:)), name: Notification.Name(AppConstant.MASTER_DATA_UPDATE_NOTIFICATION), object: nil)
        app.updateInitializingAppCommand().command(true)
    }

   @objc func updateView(_ sender:Any?=nil) {
        if( !app.appModel.initializing && app.appModel.errorStatus == .noError ) {
            remove(asChildViewController: appLoadingViewController)
            add(asChildViewController: appTabBarController)
        } else {
            if( _tabBarController != nil){
                remove(asChildViewController: _tabBarController!)
                _tabBarController = nil
            }
            appLoadingViewController.errorStatus = app.appModel.errorStatus
            add(asChildViewController: appLoadingViewController)
        }
    }

    private func add(asChildViewController viewController: UIViewController) {
        if viewController.parent != nil {
            return
        }
        addChild(viewController)

        view.addSubview(viewController.view)

        viewController.view.frame = view.bounds
        viewController.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]

        viewController.didMove(toParent: self)
    }

    private func remove(asChildViewController viewController: UIViewController) {

        if viewController.parent == nil {
            return
        }
        viewController.willMove(toParent: nil)

        viewController.view.removeFromSuperview()

        viewController.removeFromParent()
    }

    override var supportedInterfaceOrientations:UIInterfaceOrientationMask {
        return UIInterfaceOrientationMask.all
    }
}

