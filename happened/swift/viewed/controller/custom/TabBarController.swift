import UIKit

class TabBarController: UITabBarController, UITabBarControllerDelegate  {

    static var OBSERVER_ID:String = NSUUID().uuidString

    private var viewed: Viewed { return Viewed.instance }

    convenience init( ) {
        self.init(nibName: nil, bundle: nil)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        delegate = self
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        addObservers()
        self.viewControllers = [latestVC, myTeamsVC, competitionVC, settingsVC]
    }

    override func viewDidAppear(_ animated: Bool) {
        viewed.viewModel.tabBarDisplayed = true
        super.viewDidAppear(animated)
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        viewed.viewModel.tabBarDisplayed = false
        removeObservers()
    }

    func addObservers() {
        NotificationCenter.default.addObserver(self, selector: #selector(observerIDCalled(notification:)), name: Notification.Name( TabBarController.OBSERVER_ID ), object: nil)
    }

    func removeObservers() {
        NotificationCenter.default.removeObserver(self)
    }

    @objc func observerIDCalled(notification: NSNotification) {
        let requestType:String? = notification.userInfo?[AppViewAttribute.requestType] as? String
        let action:Any? = notification.userInfo?[AppViewAttribute.action]
        let value:Any? = notification.userInfo?[AppViewAttribute.value]
        applyNotificationRequest(requestType: requestType, action: action, value:value)
    }

    func applyNotificationRequest(requestType:String?=nil, action:Any?=nil, value:Any?=nil) {
        if requestType != nil {
            switch requestType! {
                case AppConstant.SHOW_TAB_REQUEST:
                    if let tabIdentifier: TabIdentifier = action as? TabIdentifier {
                        changeTab(tabIdentifier)
                    }
                    break
                default:
                    break
            }
        }
    }

    func changeTab(_ tabIdentifier: TabIdentifier) {
        if viewControllers == nil {
            return
        }
        var index:Int = 0
        for uiViewController:UIViewController in viewControllers! {
            var checkTabIdentifier:TabIdentifier? = (uiViewController as? ViewController)?.tabIdentifier
            if checkTabIdentifier == nil {
                checkTabIdentifier = (uiViewController as? NavigationController)?.tabIdentifier
            }
            if checkTabIdentifier == tabIdentifier {
                selectedIndex = index
            }
            index += 1
        }
    }

    var latestVC:NavigationController {
        let vc = NavigationController(tabIdentifier: .latest)
        vc.tabBarItem = UITabBarItem(title: "latest_tab".localized, image: UIImage(named: "latest.png"), selectedImage: UIImage(named: "latest.png"))
        let mainVC:FeedVC = FeedVC(navigationObserverID: vc.observerID, isTeamFeed: false)
        mainVC.isPage = true
        vc.viewControllers = [mainVC]
        return vc
    }

    var myTeamsVC:NavigationController {
        let vc = NavigationController(tabIdentifier: .myTeams)
        vc.tabBarItem = UITabBarItem(title: "my_teams_tab".localized, image: UIImage(named: "my_teams.png"), selectedImage: UIImage(named: "my_teams.png"))
        let mainVC:FeedVC = FeedVC(navigationObserverID: vc.observerID, isTeamFeed: true)
        mainVC.isPage = true
        vc.viewControllers = [mainVC]
        return vc
    }

    var competitionVC:NavigationController {
        let vc = NavigationController(tabIdentifier: .findTeams)
        vc.tabBarItem = UITabBarItem(title: "find_teams_tab".localized, image: UIImage(named: "find_teams.png"), selectedImage: UIImage(named: "find_teams.png"))
        let mainVC:CompetitionVC = CompetitionVC(navigationObserverID: vc.observerID)
        mainVC.isPage = true
        vc.viewControllers = [mainVC]
        return vc
    }

    var settingsVC:SettingsVC {
        let vc = SettingsVC(tabIdentifier: .settings)
        vc.tabBarItem = UITabBarItem(title: "settings_tab".localized, image: UIImage(named: "settings.png"), selectedImage: UIImage(named: "settings.png"))
        vc.isPage = true
        return vc
    }
}
