import UIKit

class NavigationController:UINavigationController {

    private var viewed:Viewed { return Viewed.instance }

    var tabIdentifier: TabIdentifier?
    var observerID:String = NSUUID().uuidString

    convenience init( tabIdentifier: TabIdentifier? = nil) {
        self.init(nibName: nil, bundle: nil)
        self.tabIdentifier = tabIdentifier
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        interactivePopGestureRecognizer?.isEnabled = false
        addObservers()
    }

    override func viewDidAppear(_ animated: Bool) {
        viewed.viewModel.visibleTab = tabIdentifier
        viewed.launchRequestedPageViewCommand().checkForWaitingTab()
        super.viewDidAppear(animated)
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if viewed.viewModel.visibleTab == tabIdentifier {
            viewed.viewModel.visibleTab = nil
        }
        removeObservers()
    }


    func addObservers() {
        NotificationCenter.default.addObserver(self, selector: #selector(observerIDCalled(notification:)), name: Notification.Name( AppConstant.SHOW_PAGE_REQUEST ), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(observerIDCalled(notification:)), name: Notification.Name( observerID ), object: nil)
    }

    func removeObservers() {
        NotificationCenter.default.removeObserver(self)
    }

    @objc func observerIDCalled(notification: NSNotification) {
        let requestType:String? = notification.userInfo?[AppViewAttribute.requestType] as? String
        let action:Any? = notification.userInfo?[AppViewAttribute.action]
        let value:Any? = notification.userInfo?[AppViewAttribute.value]
        applyNotificationRequest(requestType: requestType, action: action, value:value)
    }

    func applyNotificationRequest(requestType:String?=nil, action:Any?=nil, value:Any?=nil) {
        if requestType != nil {
            switch requestType! {
            case AppConstant.SHOW_LISTING_REQUEST:
                if let listingSeasonRef:Int = action as? Int {
                    showListing(listingSeasonRef)
                }
                break
            case AppConstant.SHOW_TEAM_REQUEST:
                if let teamListingRef:Int = action as? Int {
                    showTeam(teamListingRef)
                }
                break
            case AppConstant.SHOW_LISTING_SCENARIO_REQUEST:
                if let listingScenarioRef:Int = action as? Int {
                    showListingScenario(listingScenarioRef)
                }
                break
            case AppConstant.SHOW_EVENT_SCENARIO_REQUEST:
                if let eventScenarioRef:Int = action as? Int {
                    showEventScenario(eventScenarioRef)
                }
                break
            default:
                break
            }
        }
    }

    func showListing(_ listingSeasonRef: Int) {
        let listingVC:ListingVC = ListingVC(listingSeasonRef, navigationObserverID: observerID)
        listingVC.isPage = true;
        pushViewController(listingVC, animated: true)
    }

    func showTeam(_ teamListingRef: Int) {
        let teamVC:TeamVC = TeamVC(teamListingRef, navigationObserverID: observerID)
        teamVC.isPage = true;
        pushViewController(teamVC, animated: true)
    }

    func showListingScenario(_ listingScenarioRef: Int) {
        let listingScenarioVC:ListingScenarioVC = ListingScenarioVC(listingScenarioRef, navigationObserverID: observerID)
        listingScenarioVC.isPage = true;
        pushViewController(listingScenarioVC, animated: true)
    }

    func showEventScenario(_ eventScenarioRef: Int) {
        let eventScenarioVC:EventScenarioVC = EventScenarioVC(eventScenarioRef, navigationObserverID: observerID)
        eventScenarioVC.isPage = true;
        self.pushViewController(eventScenarioVC, animated: true)
    }
}
