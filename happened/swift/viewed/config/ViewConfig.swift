import UIKit

class ViewConfig {


    static var ME:ViewConfig? = nil
    static var testyItems:[UIView] = []
    static var testyItem:UIView? = nil

    func testy() {
        if !App.instance.commonHelper.isDebug {
            return
        }
        for item:UIView in ViewConfig.testyItems {
            item.printDetails()
        }
        var currentText:String? = (ViewConfig.testyItem as? UILabel)?.text
        currentText = currentText == nil ? (ViewConfig.testyItem as? AttributedTextView)?.attributedText.string : nil
        if currentText != nil {
            print("currentText: " + currentText!)
        }
        ViewConfig.testyItem?.printHeritage()
    }

    let homeDefaultIsHappened:Bool = true

    let visibleEventScenarioSpan:Int = 50

    let visibleFixtureSpan:Int = 50

    let nonBreakCode:String = "\u{00A0}"

    let apostrophe:String = "\u{0027}"

    let hyphen:String = "\u{2011}"

    let ampersand:String = "\u{0026}"

    let fontFamilies:[String:[AppViewAttribute:String]]
    let fontFamily1:[AppViewAttribute:String]
    let fontFamily2:[AppViewAttribute:String]

    let pointSizes:[UIFont.TextStyle:CGFloat]
    let defaultUIFontTextStyle:UIFont.TextStyle
    let defaultPointSize:CGFloat

    let topLeftMargin:CGPoint
    let bottomRightMargin:CGPoint

    let standingColorBlue1:UIColor = UIColor(red: 87/255, green: 61/255, blue: 255/255, alpha: 1)
    let standingColorBlue2:UIColor = UIColor(red: 204/255, green: 249/255, blue: 255/255, alpha: 1)
    let standingColorBlue3:UIColor = UIColor(red: 204/255, green: 204/255, blue: 255/255, alpha: 1)
    let standingColorGreen1:UIColor = UIColor(red: 204/255, green: 255/255, blue: 204/255, alpha: 1)
    let standingColorGreen2:UIColor = UIColor(red: 187/255, green: 243/255, blue: 187/255, alpha: 1)
    let standingColorGreen3:UIColor = UIColor(red: 204/255, green: 249/255, blue: 204/255, alpha: 1)
    let standingColorRed1:UIColor = UIColor(red: 255/255, green: 187/255, blue: 187/255, alpha: 1)
    let standingColorRed2:UIColor = UIColor(red: 255/255, green: 204/255, blue: 204/255, alpha: 1)
    let standingColorYellow1:UIColor = UIColor(red: 255/255, green: 255/255, blue: 187/255, alpha: 1)

    let colorWhite2:UIColor = UIColor(red: 245/255, green: 245/255, blue: 245/255, alpha: 1)

    let colorGrey1:UIColor = UIColor(red: 233/255, green: 234/255, blue: 237/255, alpha: 1)
    let colorGrey2:UIColor = UIColor(red: 153/255, green: 153/255, blue: 153/255, alpha: 1)
    let colorGrey3:UIColor = UIColor(red: 158/255, green: 158/255, blue: 158/255, alpha: 1)
    let colorGrey4:UIColor = UIColor(red: 51/255, green: 51/255, blue: 51/255, alpha: 1)
    let colorGrey5:UIColor = UIColor(red: 58/255, green: 58/255, blue: 58/255, alpha: 1)
    let colorGrey6:UIColor = UIColor(red: 79/255, green: 79/255, blue: 79/255, alpha: 1)

    let colorBlue1:UIColor = UIColor(red: 244/255, green: 246/255, blue: 247/255, alpha: 1)
    let colorBlue2:UIColor = UIColor(red: 211/255, green: 214/255, blue: 219/255, alpha: 1)
    let colorBlue3:UIColor = UIColor(red: 109/255, green: 132/255, blue: 180/255, alpha: 1)
    let colorBlue4:UIColor = UIColor(red: 159/255, green: 168/255, blue: 176/255, alpha: 1)
    let colorBlue5:UIColor = UIColor(red: 39/255, green: 68/255, blue: 118/255, alpha: 1)
    let colorBlue6:UIColor = UIColor(red: 215/255, green: 227/255, blue: 255/255, alpha: 1)
    let colorBlue7:UIColor = UIColor(red: 0/255, green: 99/255, blue: 167/255, alpha: 1)
    let colorBlue8:UIColor = UIColor(red: 0/255, green: 95/255, blue: 165/255, alpha: 1)

    let colorRed1:UIColor = UIColor(red: 211/255, green: 21/255, blue: 30/255, alpha: 1)
    let colorRed2:UIColor = UIColor(red: 176/255, green: 43/255, blue: 40/255, alpha: 1)
    let colorRed3:UIColor = UIColor(red: 128/255, green: 33/255, blue: 33/255, alpha: 1)
    let colorRed4:UIColor = UIColor(red: 211/255, green: 21/255, blue: 30/255, alpha: 1)


    let lineDashPattern:[NSNumber] = [6,3]

    let defaultPageControlMaximumPages:Int = 18

    var deviceWidthProfile: DeviceWidthProfile
    var deviceValue:CGFloat

    let defaultTextColor:UIColor

    let linkColor:UIColor
    let linkDisabledColor:UIColor

    let mainRedColor: UIColor
    let mainDisabledRedColor: UIColor
    let mainBlueColor: UIColor
    let mainDisabledBlueButtonColor: UIColor

    init(){

        let screenBounds: CGRect = UIScreen.main.bounds
        let minimumWidth:CGFloat = min(screenBounds.width, screenBounds.height)

        if minimumWidth >= 1024 {
            //iPad pro (12.9 inch)
            deviceWidthProfile = .size4
        } else if minimumWidth >= 768 {
            //iPad pro (9.7 inch)
            //iPad Air 2
            //iPad air
            deviceWidthProfile = .size3
        } else if minimumWidth >= 414 {
            //iPhone 7 plus
            //iPhone 6s plus
            deviceWidthProfile = .size2
        } else if minimumWidth >= 375 {
            //iPhone 7
            //iPhone 6s
            //iPhone 6
            deviceWidthProfile = .size1
        } else {
            //iPhone 5s
            //iPhone 5
            //iPhone SE
            deviceWidthProfile = .size0
        }



        let gillSans:[AppViewAttribute:String] = [
                .regular: "GillSans",
                .bold: "GillSans-Bold",
                .italic: "GillSans-Italic"
        ]

        let helveticaNeue:[AppViewAttribute:String] = [
                .regular: "HelveticaNeue-Medium",
                .bold: "HelveticaNeue-Bold",
                .italic: "HelveticaNeue-Italic"
        ]

        fontFamilies = ["Gill Sans":gillSans, "Helvetica Neue":helveticaNeue]

        fontFamily1 = helveticaNeue
        fontFamily2 = gillSans

        switch deviceWidthProfile {
            case .size4:
                pointSizes = [.body: 36, .body2:24, .body3:36, .title1: 54, .title2: 48, .title3: 42, .title4:54]
                deviceValue = 3
                break
            case .size3:
                pointSizes = [.body: 24, .body2:16, .body3:24, .title1: 36, .title2: 32, .title3: 28, .title4:36]
                deviceValue = 2
                break
            case .size2:
                pointSizes = [.body: 17, .body2:13, .body3:18, .title1: 27, .title2: 24, .title3: 21, .title4:27]
                deviceValue = 1.6
                break
            case .size1:
                pointSizes = [.body: 16, .body2:12, .body3:16, .title1: 24, .title2: 21, .title3: 19, .title4:24]
                deviceValue = 1.4
                break
            default:
                pointSizes = [.body: 12, .body2:10, .body3:14, .title1: 18, .title2: 16, .title3: 14, .title4:24]
                deviceValue = 1
                break
        }
        defaultUIFontTextStyle = .body
        defaultPointSize = pointSizes[defaultUIFontTextStyle]!

        defaultTextColor = colorGrey5
        linkColor = colorBlue7
        linkDisabledColor = colorGrey3

        mainRedColor = colorRed1
        mainDisabledRedColor = colorRed3

        mainBlueColor = colorBlue8
        mainDisabledBlueButtonColor = colorBlue5


        topLeftMargin = CGPoint(x: ceil( 8 * deviceValue ), y: ceil( 8 * deviceValue ))
        bottomRightMargin = CGPoint(x: ceil( 8 * deviceValue ), y: ceil( 8 * deviceValue ))

        ViewConfig.ME = self
    }
}
