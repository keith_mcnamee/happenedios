import Foundation

enum FeedType {
    case event, fixture
}

enum FeedSource {
    case listing, specificTeam, allTeam, scenario
}

enum FilteredState {
    case unfiltered, filtered
}

enum AppConstraint {
    case height,
         width,
         top,
         bottom,
         leading,
         trailing,
         centerX,
         centerY
}

enum AppViewAttribute {
    case observerID,
            requestType,
            action,
            value,
            regular,
            bold,
            italic,
            pointSize,
            pointSizeStyle,
            backgroundColor,
            color,
            border,
            sides,
            top,
            centerY,
            bottom,
            left,
            centerX,
            right,
            style,
            dashed,
            none,
            textAlignment,
            verticalPosition
}

enum FixtureColumnType:Int {
    case dateTime = 0,
         homeTeam = 1,
         score = 2,
         awayTeam = 3,
         competition = 4
}

enum StandingColumnType:Int {
    case rank = 0,
         teamName = 1,
         played = 2,
         remaining = 3,
         matchesWon = 4,
         matchesDrawn = 5,
         matchesLost = 6,
         goalDifference = 7,
         awayGoals = 8,
         awayWins = 9,
         goalsScored = 10,
         goalsConceded = 11,
         headToHead = 12,
         points = 13,
         splitPotIndex = 14
}

enum TabIdentifier {
    case latest, myTeams, findTeams, settings
}

enum MultipleSelectedState {
    case not, partial, fully
}

enum DeviceWidthProfile:Int {
    case size0 = 0,
         size1 = 1,
         size2 = 2,
         size3 = 3,
         size4 = 4
}

class ViewConstant {

    static let HOME:String = "home"
    static let REFRESH:String = "refresh"
    static let EVENT_SCENARIO_HAPPENED_FEED:String = "happened"
    static let EVENT_SCENARIO_UPCOMING_FEED:String = "upcoming"
    static let FIXTURE_HAPPENED_FEED:String = "results"
    static let FIXTURE_UPCOMING_FEED:String = "fixtures"
    static let SEASON:String = "season"
    static let COMPETITION:String = "competition"
    static let LEAGUE:String = "league"
    static let TEAM:String = "team"
    static let EVENT:String = "event"
    static let EVENT_SCENARIO:String = "eventScenario"
    static let LISTING_SCENARIO:String = "listingScenario"
    static let LIST_SEASON:String = "listSeason"
    static let LIST_TEAM_SEASON:String = "listTeamSeason"
    static let LIST_TEAM_LEAGUE:String = "listTeamLeague"
    static let LIST_EVENT_SCENARIO:String = "listEventScenario"
    static let BEST:String = "best"
    static let WORST:String = "worst"
    static let TO_HAPPEN:String = "tohappen"
    static let NOT_HAPPEN:String = "nothappen"
    static let HOME_FEED_VIEW:String = "homeFeedView"
    static let EVENT_SCENARIO_HAPPENED_FEED_VIEW:String = "eventScenarioHappenedFeedView"
    static let EVENT_SCENARIO_UPCOMING_FEED_VIEW:String = "eventScenarioUpcomingFeedView"
    static let FIXTURE_HAPPENED_FEED_VIEW:String = "fixtureHappenedFeedView"
    static let FIXTURE_UPCOMING_FEED_VIEW:String = "fixtureUpcomingFeedView"
    static let COMPETITION_VIEW:String = "competitionView"
    static let LISTING_VIEW:String = "listingView"
    static let TEAM_VIEW:String = "teamView"
    static let LISTING_SCENARIO_VIEW:String = "listingScenarioView"
    static let EVENT_SCENARIO_VIEW:String = "eventScenarioView"
    static let SEASON_GROUP_LIST_VIEW:String = "seasonGroupListView"
    static let SEASON_LISTING_LIST_VIEW:String = "seasonListingListView"
    static let LISTING_SEASON_LIST_VIEW:String = "listingSeasonListView"
    static let TEAM_SEASON_LIST_VIEW:String = "teamSeasonListView"
    static let TEAM_LISTING_LIST_VIEW:String = "teamListingListView"
    static let EVENT_SCENARIO_LIST_VIEW:String = "eventScenarioListView"
    static let FACEBOOK_BUTTON_TRACK:String = "facebookButton"
    static let TWITTER_BUTTON_TRACK:String = "twitterButton"
    static let LIST_VIEW_TRACK:String = "listView"
    static let INVALID_TRACK:String = "invalid"
    static let LATEST_SEASON_TRACK:String = "latestSeason"
    static let FILTER:String = "filter"
    static let STAGE:String = "stage"
    static let STAGE_END:String = "end"
}
