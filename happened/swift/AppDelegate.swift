import UIKit
import UserNotifications

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    private var app:App { return App.instance }
    private var preference:Preference { return Preference.instance }
    private var viewed:Viewed { return Viewed.instance }

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.

        NotificationCenter.default.addObserver(self, selector: #selector(appMovedToForeground), name: UIApplication.didBecomeActiveNotification, object: nil)

        app.processAppCommand().command()

        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }

    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {

        var deviceToken:String? = deviceToken.reduce("", {$0 + String(format: "%02X", $1)})
        if deviceToken == "" {
            deviceToken = nil
        }

        app.appModel.deviceToken = deviceToken
        self.app.updatePushStatusAppCommand().command(forceServerUpdate: true)
    }

    public func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Swift.Void) {
        let dictionary:[String:Any]? = userInfo["aps"] as? [String:Any]
        let eventScenarioRef:Int? = dictionary?["event_scenario_ref"] as? Int
        if eventScenarioRef != nil {
            viewed.launchRequestedPageViewCommand().launchWithProperties(pageID: AppConstant.SHOW_EVENT_SCENARIO_REQUEST, action: eventScenarioRef!, tabIdentifier: .myTeams)
        }
    }

    @objc func appMovedToForeground() {
        let pushAuthorizationStatus:PushAuthorizationStatus = self.preference.preferenceModel.pushAuthorizationStatus
        if app.appModel.deviceToken == nil && ( pushAuthorizationStatus == .denied || pushAuthorizationStatus == .authorized ) {
            UNUserNotificationCenter.current().requestAuthorization(options:[.badge, .alert, .sound]) { (granted, error) in }
            UIApplication.shared.registerForRemoteNotifications()
        } else {
            self.app.updatePushStatusAppCommand().command()
        }
    }

}

