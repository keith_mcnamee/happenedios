import Foundation

class
        ArrayHelper {

    private var app:App { return App.instance }

    func strArFromSeparatedString(_ string:String, separator:String = ",", defaultValue:[String]? = nil )->[String]? {
        var strArr:[String] = string.components(separatedBy: separator)

        for i in 0..<strArr.count {
            strArr[i] = strArr[i].trim()
        }
        if strArr.count == 0 {
            return defaultValue
        }
        return strArr
    }

    func strArrFromStrStrDicStringEntry(_ strStrDic:[String : String], _ key:String, separator:String = ",", defaultValue:[String]? = nil ) -> [String]?{
        let string:String? = stringFromStrStrDic(strStrDic, key)
        if( string == nil )
        {
            return defaultValue
        }
        return strArFromSeparatedString( string!, separator:separator )
    }

    func intArrToIntNulDic(_ arr:[Int] ) -> [Int: NSNull] {
        var ind:[Int : NSNull] = [:]
        for (item) in arr {
            ind[item] = NSNull()
        }
        return ind
    }

    func intNulDicFromStrStrDicStringEntry(_ strStrDic:[String : String], _ key:String, separator:String  = ",", defaultValue:[Int: NSNull]? = nil ) -> [Int: NSNull]?{
        let strArr:[String]? = strArrFromStrStrDicStringEntry(strStrDic, key, separator:separator )
        if( strArr == nil )
        {
            return defaultValue
        }
        let intArr:[Int] = strArrToIntArr(strArr!)
        if intArr.count == 0 {
            return defaultValue
        }
        return intArrToIntNulDic( intArr )
    }

    func stringFromNsDic(_ nsDic:NSDictionary, _ key:String, defaultValue:String? = nil ) -> String? {
        if let value = nsDic.object(forKey: key) {
            if let stringValue:String = value as? String {
                return stringValue
            }
            if let doubleValue:Double = value as? Double {
                return String(doubleValue)
            }
            if let boolValue:Bool = value as? Bool {
                return boolValue ? "1" : "0"
            }
        }
        return defaultValue
    }

    func stringFromStrStrDic(_ strStrDic:[String : String], _ key:String, defaultValue:String? = nil ) -> String? {
        if let stringValue:String = strStrDic[ key ] {
            return stringValue
        }
        return defaultValue
    }

    func doubleFromNsDic(_ nsDic:NSDictionary, _ key:String, defaultValue:Double? = nil ) -> Double? {
        if let value = nsDic.object(forKey: key) {
            if let doubleValue:Double = value as? Double {
                return doubleValue
            }
            if let boolValue:Bool = value as? Bool {
                return boolValue ? 1 : 0
            }
            if let stringValue:String = value as? String {
                if let doubleValue = Double(stringValue) {
                    return doubleValue
                }
                if stringValue == "" {
                    return 0
                }
            }
        }
        return defaultValue
    }

    func doubleFromStrStrDic(_ strStrDic:[String : String], _ key:String, defaultValue:Double? = nil ) -> Double? {
        if let stringValue:String = strStrDic[ key ] {
            if let doubleValue = Double(stringValue) {
                return doubleValue
            }
            if stringValue == "" {
                return 0
            }
        }
        return defaultValue
    }

    func intFromNsDic(_ nsDic:NSDictionary, _ key:String, defaultValue:Int? = nil ) -> Int? {
        if let value = nsDic.object(forKey: key) {
            if let intValue:Int = value as? Int {
                return intValue
            }
            if let doubleValue:Double = doubleFromNsDic(nsDic, key) {
                return Int(doubleValue)
            }
            if let boolValue:Bool = value as? Bool {
                return boolValue ? 1 : 0
            }
            if let stringValue:String = value as? String {
                if let doubleValue = Double(stringValue) {
                    return Int(doubleValue)
                }
                if stringValue == "" {
                    return 0
                }
            }
        }
        return defaultValue
    }

    func intFromStrStrDic(_ strStrDic:[String : String], _ key:String, defaultValue:Int? = nil ) -> Int? {
        if let stringValue:String = strStrDic[ key ] {
            if let intValue = Int(stringValue) {
                return intValue
            }
            if let doubleValue = Double(stringValue) {
                return Int(doubleValue)
            }
            if stringValue == "" {
                return 0
            }
        }
        return defaultValue
    }

    func booleanFromNsDic(_ nsDic:NSDictionary, _ key:String, defaultValue:Bool? = nil ) -> Bool? {
        if let value = nsDic.object(forKey: key) {
            if let boolValue:Bool = value as? Bool {
                return boolValue
            }
            if let intValue:Int = value as? Int {
                return intValue == 0
            }
            if let stringValue:String = value as? String {
                return stringValue.bool
            }
            return true
        }
        return defaultValue
    }

    func booleanFromStrStrDic(_ strStrDic:[String : String], _ key:String, defaultValue:Bool? = nil ) -> Bool? {
        if var stringValue:String = strStrDic[ key ] {
            stringValue = stringValue.lowercased()
            return stringValue != "" && stringValue != "false" && stringValue != "0"
        }
        return defaultValue
    }

    func dateFromNsDicInterval(_ nsDic:NSDictionary, _ key:String, defaultValue:Date? = nil, requireDay1:Bool=false ) -> Date? {
        if let intervalValue:Double = doubleFromNsDic(nsDic, key) {
            let date:Date = Date(timeIntervalSince1970: intervalValue)
            if requireDay1 {
                if date.timeIntervalSince1970 < app.appModel.day1.timeIntervalSince1970 {
                    return defaultValue
                }
            }
            return date
        }
        return defaultValue
    }

    func dateFromStrStrDic(_ strStrDic:[String : String], _ key:String, defaultValue:Date? = nil, requireDay1:Bool=false ) -> Date? {
        let string:String? = stringFromStrStrDic(strStrDic, key)
        if( string == nil )
        {
            return defaultValue
        }

        let date:Date? = app.dateHelper.dateFromStringDefault(string!)
        if date == nil {
            return defaultValue
        }
        if requireDay1 {
            if date!.timeIntervalSince1970 < app.appModel.day1.timeIntervalSince1970 {
                return defaultValue
            }
        }
        return date
    }

    func intArrFromNsDic(_ nsDic:NSDictionary, _ key:String, defaultValue:[Int]? = nil ) -> [Int]? {
        if let intArr:[Int] = nsDic.object(forKey: key) as? [Int] {
            return intArr
        }
        return defaultValue
    }

    func intNulArrFromNsDicArr(_ nsDic:NSDictionary, _ key:String, defaultValue:[Int:NSNull]? = nil ) -> [Int:NSNull]? {
        if let intArr:[Int] = intArrFromNsDic( nsDic, key ) {
            return intArrToIntNulDic(intArr)
        }
        return defaultValue
    }

    func nsDicFromNsDic(_ nsDic:NSDictionary, _ key:String, defaultValue:NSDictionary? = nil ) -> NSDictionary? {
        if let nsDic:NSDictionary = nsDic.object(forKey: key) as? NSDictionary {
            return nsDic
        }
        return defaultValue
    }

    func nsArrFromNsDic(_ nsDic:NSDictionary, _ key:String, defaultValue:NSArray? = nil ) -> NSArray? {
        if let nsArr:NSArray = nsDic.object(forKey: key) as? NSArray {
            return nsArr
        }
        return defaultValue
    }

    func validValueFromStrArr(_ value:String, _ validOptions:[String] )->Bool {
        for validOption:String in validOptions {
            if value == validOption {
                return true
            }
        }
        return false
    }

    func validValuesFromStrArr(_ values:[String], _ validOptions:[String] )->[String] {
        var validValues:[String] = []
        for value:String in values {
            if validValueFromStrArr( value, validOptions ) {
                validValues.append(value)
            }
        }
        return validValues
    }

    func validKeysFromIntDic(_ checkKeys:[Int:Any], _ validOptions:[Int:Any] )->[Int:NSNull] {
        var validKeys:[Int:NSNull] = [:]
        for key:Int in checkKeys.keys {
            if validOptions[key] != nil {
                validKeys[key] = NSNull()
            }
        }
        return validKeys
    }

    func intDicContainsAllKeys(_ inIntDic:[Int : Any], _ forIntDic:[Int : Any] )->Bool {
        for key in forIntDic.keys {
            if inIntDic[key] == nil {
                return false
            }
        }
        return true
    }

    func mergeIntNulDics(_ intNulDicA:[Int : Any], _ intNulDicB:[Int : Any] )->[Int : NSNull] {
        var intNulDic:[Int : NSNull] = cloneIntNulDic(intNulDicA)
        for key:Int in intNulDicB.keys {
            intNulDic[key] = NSNull()
        }
        return intNulDic
    }

    //It appears these may be unnecessary in swift/ios but I'm leaving it in in case I'm copying code to another language
    func cloneIntNulDic(_ intNulDicA:[Int : Any] )->[Int : NSNull] {
        var intNulDicB:[Int : NSNull] = [:]
        for key:Int in intNulDicA.keys {
            intNulDicB[key] = NSNull()
        }
        return intNulDicB
    }

    func cloneStrArr(_ strArrA:[String] )->[String] {
        var strArrB:[String] = []
        for str:String in strArrA {
            strArrB.append(str)
        }
        return strArrB
    }

    func intDicToIntArr(_ intDic:[Int : Any] )->[Int] {
        return [Int](intDic.keys)
    }

    func hasMatchingKeys( _ dictionary1:[Int : Any], _ dictionary2:[Int : Any] ) -> Bool {
        
        if( dictionary1.count != dictionary2.count ) {
            return false
        }

        for key:Int in dictionary1.keys  {
            if dictionary2[key] == nil {
                return false
            }
        }
        for key:Int in dictionary2.keys  {
            if dictionary1[key] == nil {
                return false
            }
        }

        return true
    }

    func sortKeysByValue( _ dictionary:[Int:Int] ) -> [Int] {

        let sortedKeys = dictionary.sorted{ $0.value < $1.value }

        var returnKeys:[Int] = []
        for (key, _) in sortedKeys {
            returnKeys.append(key)
        }
        return returnKeys
    }

    func firstKeyFromIntDic(_ intDic:[Int : Any] )->Int {
        if intDic.count == 0 {
            return 0
        }
        var keys:[Int] = []
        for key:Int in intDic.keys {
            keys.append(key)
        }

        keys.sort { $0 < $1 }

        return keys[0]
    }

    func strArrToIntArr(_ strArr:[String] ) -> [Int] {
        var intArr:[Int] = []
        for string:String in strArr {
            if let int:Int = Int(string) {
                intArr.append(int)
            }
        }
        return intArr
    }

    func appendOrNil(_ item:Any?, _ array:[Any]? ) -> [Any]? {
        if item == nil {
            return array
        }
        var useItems:[Any] = array != nil ? array! : []
        useItems.append(item!)
        return useItems
    }
}
