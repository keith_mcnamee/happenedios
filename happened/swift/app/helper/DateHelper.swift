import Foundation

class DateHelper {

    private var app:App { return App.instance }

    func dateFromString( _ string:String ) -> Date? {
        return app.appModel.dateFormatter.date(from: string)
    }

    func dateFromInt( _ int:Int ) -> Date! {
        return Date(timeIntervalSince1970: Double(int))
    }

    func dateFromStringDefault( _ string:String, defaultValue:Date? = nil ) -> Date? {
        if let value = dateFromString(string) {
            return value
        }
        return defaultValue
    }

    func ordinalDate(date:Date, preFormat:String?=nil, postFormat:String?=nil) -> String {
        var dateString:String = ""
        if preFormat != nil {
            let preFormatter:DateFormatter = DateFormatter()
            preFormatter.dateFormat = preFormat!
            dateString = dateString + "\(preFormatter.string(from: date))"
        }

        let calendar:Calendar = Calendar.current
        let dateComponents:Int = calendar.component(.day, from: date)
        let numberFormatter:NumberFormatter = NumberFormatter()
        numberFormatter.numberStyle = .ordinal
        let day:String? = numberFormatter.string(from: dateComponents as NSNumber)
        if day != nil {
            dateString = dateString + "\(day!)"
        }
        if postFormat != nil {
            let postFormatter:DateFormatter = DateFormatter()
            postFormatter.dateFormat = postFormat!
            dateString = dateString + "\(postFormatter.string(from: date))"
        }
        return dateString
    }

    func thisMorning12AmDate(date:Date) -> Date {

        let inputFormatter:DateFormatter = DateFormatter()
        inputFormatter.dateFormat = "yyyy"
        let year:String = inputFormatter.string(from: date)
        inputFormatter.dateFormat = "MM"
        let month:String = inputFormatter.string(from: date)
        inputFormatter.dateFormat = "dd"
        let day:String = inputFormatter.string(from: date)
        let localTimeZone:TimeZone = TimeZone.ReferenceType.local
        let secondsFromGMT:Int = localTimeZone.secondsFromGMT(for: date)
        let outputFormatter:DateFormatter = DateFormatter()
        outputFormatter.timeZone = TimeZone(secondsFromGMT: secondsFromGMT)
        outputFormatter.dateFormat = "yyyy-MM-dd"
        let returnDate:Date = outputFormatter.date(from: year+"-"+month+"-"+day)!

        return returnDate
    }

}
