import Foundation

class FileHelper {

    private var app:App { return App.instance }

    func fileURL(_ name: String, _ ext: String, forceOverwrite:Bool=false) -> URL? {

        var finalFileURL:URL? = documentDirectoryFileURL(name, ext)

        if finalFileURL != nil {
            
            if (finalFileURL! as NSURL).checkResourceIsReachableAndReturnError(nil) && !forceOverwrite {
                // The file already exists, so just return the URL
                return finalFileURL
            } else {
                // Copy the initial file from the application bundle to the documents directory
                
                if let bundleURL = Bundle.main.url(forResource: name, withExtension: ext) {
                    finalFileURL = copyToDocumentDirectory(bundleURL, name, ext)
                    if finalFileURL != nil {
                        return finalFileURL
                    }
                    app.updateErrorAppCommand().command()
                    return nil
                } else {
                    app.updateErrorAppCommand().command()
                }
            }
        } else {
            app.updateErrorAppCommand().command()
        }

        return nil
    }

    func copyToDocumentDirectory(_ fileURL: URL, _ name: String, _ ext: String) -> URL? {

        let fileManager = FileManager.default

        let finalFileURL = documentDirectoryFileURL(name, ext)

        if finalFileURL != nil {

            if (finalFileURL! as NSURL).checkResourceIsReachableAndReturnError(nil) {
                do {
                    try fileManager.removeItem(at: finalFileURL!)
                } catch {
                    return nil
                }
            }

            do {
                try fileManager.copyItem(at: fileURL, to: finalFileURL!)
            } catch {
                return nil
            }
            return finalFileURL
        }
        return nil
    }

    func documentDirectoryFileURL(_ name: String, _ ext: String) -> URL? {

        let directoryURL: URL? = documentDirectoryURL

        if directoryURL != nil {
            return directoryURL!.appendingPathComponent(name + String("." + ext))
        }
        return nil
    }

    var documentDirectoryURL:URL? {

        let fileManager = FileManager.default

        let urls = fileManager.urls(for: .documentDirectory, in: .userDomainMask)

        return urls.first
    }
}
