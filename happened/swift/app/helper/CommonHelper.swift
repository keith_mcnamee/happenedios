import Foundation

class CommonHelper {

    private var app:App { return App.instance }

    var isDebug:Bool {
        return app.appConfig.debugMode
    }

    func stringClassFromString(_ className: String) -> AnyClass! {

        let namespace = Bundle.main.infoDictionary!["CFBundleExecutable"] as! String;

        let cls: AnyClass = NSClassFromString("\(namespace).\(className)")!;

        return cls;
    }

}
