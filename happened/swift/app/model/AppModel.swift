import Foundation

class AppModel {

    var initializing:Bool = false
    var waitForServer:Bool = false
    var waitForView:Bool = false
    var errorStatus: AppErrorStatus = .noError
    var serverURL:String?

    let dateFormatter:DateFormatter
    let day1:Date
    let dayLast:Date

    var deviceToken:String? = nil
    var appBasicApplied:Bool = false

    var currentNavigationObserverID:String?=nil

    init(_ appConfig: AppConfig, _ dateHelper:DateHelper) {
        dateFormatter = DateFormatter();
        dateFormatter.timeZone = TimeZone(identifier: appConfig.dateTimezone)
        dateFormatter.dateFormat = appConfig.dateFormat

        day1 = dateFormatter.date(from: appConfig.day1)!

        dayLast = dateHelper.dateFromInt(Int.max)
    }

}
