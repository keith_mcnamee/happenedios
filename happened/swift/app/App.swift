import Foundation

class App {

    static let instance:App = App()


    let appConfig: AppConfig = AppConfig();


    let arrayHelper:ArrayHelper = ArrayHelper();

    let commonHelper:CommonHelper = CommonHelper();

    let dateHelper:DateHelper = DateHelper();

    let fileHelper:FileHelper = FileHelper();


    let appModel:AppModel

    init() {
        appModel = AppModel(appConfig, dateHelper);
    }

    func processAppCommand() -> ProcessAppCommand! {
        return ProcessAppCommand();
    }

    func updatePushStatusAppCommand() -> UpdatePushStatusAppCommand! {
        return UpdatePushStatusAppCommand();
    }

    func updateErrorAppCommand() -> UpdateErrorAppCommand! {
        return UpdateErrorAppCommand();
    }

    func updateInitializingAppCommand() -> UpdateInitializingAppCommand! {
        return UpdateInitializingAppCommand();
    }

    func updateActivityServerAppCommand() -> UpdateActivityServerAppCommand! {
        return UpdateActivityServerAppCommand();
    }

    func updateActivityViewAppCommand() -> UpdateActivityViewAppCommand! {
        return UpdateActivityViewAppCommand();
    }

}
