import Foundation

class AppConstant {

    static let MASTER_DATA_UPDATE_NOTIFICATION:String = NSUUID().uuidString
    static let CHECK_ACTIVITY_STATUS_NOTIFICATION:String = NSUUID().uuidString
    static let ACTIVITY_ENDED_NOTIFICATION:String = NSUUID().uuidString

    static let SHOW_TAB_REQUEST:String = NSUUID().uuidString
    static let SHOW_PAGE_REQUEST:String = NSUUID().uuidString
    static let SHOW_LISTING_REQUEST:String = NSUUID().uuidString
    static let SHOW_TEAM_REQUEST:String = NSUUID().uuidString
    static let SHOW_LISTING_SCENARIO_REQUEST:String = NSUUID().uuidString
    static let SHOW_EVENT_SCENARIO_REQUEST:String = NSUUID().uuidString

    static let CHANGE_STATE_REQUEST:String = NSUUID().uuidString

    static let CLOSE_REQUEST:String = NSUUID().uuidString
    static let SAVE_REQUEST:String = NSUUID().uuidString
    static let SAVE_AND_CLOSE_REQUEST:String = NSUUID().uuidString
    static let SHOW_OVERLAY:String = NSUUID().uuidString
    static let SHOW_ACTIVITY_INDICATOR:String = NSUUID().uuidString
}

enum AppErrorStatus {
    case noError, dataError, connectionError
}

enum PushAuthorizationStatus: Int {

    case
            unknown = 1,
            notRequested = 2,
            denied = 3,
            authorized = 4,
            notSupported = 5
}