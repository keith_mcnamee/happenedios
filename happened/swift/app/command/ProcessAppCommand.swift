import Foundation

class ProcessAppCommand {

    private var app:App { return App.instance }
    private var balancing:Balancing { return Balancing.instance }
    private var localization:Localization { return Localization.instance }
    private var preference:Preference { return Preference.instance }
    private var server:Server { return Server.instance }

    func command() {
        if app.commonHelper.isDebug {
            let directoryURL: URL? = app.fileHelper.documentDirectoryURL

            if directoryURL != nil {
                print(directoryURL!)
            }
        }
        app.updateInitializingAppCommand().command(true)
        preference.parsePreferenceCommand().command()
        localization.initializeLocalizationCommand().command()

        balancing.balancingModel.version = preference.preferenceModel.balancingVersion
        balancing.parseBalancingCommand().command()
        if app.appModel.errorStatus != .noError {
            return
        }

        app.appModel.appBasicApplied = true

        app.updatePushStatusAppCommand().command(forceServerUpdate: true)

        server.baseServerCommand().command()
    }
}
