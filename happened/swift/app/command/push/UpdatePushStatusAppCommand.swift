import Foundation
import UserNotifications

class UpdatePushStatusAppCommand {

    private var app:App { return App.instance }
    private var localization:Localization { return Localization.instance }
    private var preference:Preference { return Preference.instance }
    private var server:Server { return Server.instance }

    func command( forceServerUpdate:Bool=false ){

        if !app.appModel.appBasicApplied {
            return
        }

        var authorizationStatus: PushAuthorizationStatus = .unknown

        UNUserNotificationCenter.current().getNotificationSettings(){
            (settings:UNNotificationSettings) in
            switch settings.authorizationStatus {
                case .notDetermined:
                    authorizationStatus = .notRequested
                    break
                case .denied:
                    authorizationStatus = .denied
                    break
                case .authorized:
                    authorizationStatus = .authorized
                    break
                default:
                    break
            }
            if authorizationStatus == .authorized {
                for pushSetting:UNAuthorizationOptions in self.app.appConfig.requiredPushSettings {
                    var setting: UNNotificationSetting? = nil
                    switch pushSetting {
                        case UNAuthorizationOptions.badge:
                            setting = settings.badgeSetting
                            break
                        case UNAuthorizationOptions.sound:
                            setting = settings.soundSetting
                            break
                        case UNAuthorizationOptions.alert:
                            setting = settings.alertSetting
                            break
                        case UNAuthorizationOptions.carPlay:
                            setting = settings.carPlaySetting
                            break
                        default:
                            break
                    }
                    if setting! == .notSupported {
                        authorizationStatus = .notSupported
                        break
                    } else if setting! == .disabled {
                        authorizationStatus = .denied
                        break
                    }
                }
            }

            if self.localization.localizationModel.currentType == nil {
                self.localization.determineLocalizationCommand().command()
            }

            let localizationType:String = self.localization.localizationModel.currentType!

            let deviceToken:String? = self.app.appModel.deviceToken

            let existingAuthorizationStatus:PushAuthorizationStatus = self.preference.preferenceModel.pushAuthorizationStatus
            let existingDeviceToken:String? = self.preference.preferenceModel.deviceToken
            let existingLocalizationType:String? = self.preference.preferenceModel.localizationType

            self.preference.preferenceModel.pushAuthorizationStatus = authorizationStatus
            self.preference.preferenceModel.deviceToken = deviceToken
            self.preference.preferenceModel.localizationType = localizationType

            if authorizationStatus == .unknown {
                return
            }

            if existingDeviceToken != nil && deviceToken != nil && existingDeviceToken != deviceToken {
                self.server.updatePushStatusServerCommand().command(deviceToken: existingDeviceToken!, pushEnabled: false)
            }

            if deviceToken != nil {
                let pushEnabled:Bool = authorizationStatus == .authorized
                let wasEnabled:Bool = existingAuthorizationStatus == .authorized
                if forceServerUpdate || pushEnabled != wasEnabled || deviceToken != existingDeviceToken || localizationType != existingLocalizationType {
                    let teamSubscriptions:[Int:Bool] = self.preference.preferenceHelper.getTeamSubscriptions()
                    var teamRefs:[Int:NSNull] = [:]
                    for (teamRef, value) in teamSubscriptions {
                        if value {
                            teamRefs[teamRef] = NSNull()
                        }
                    }
                    self.server.updatePushStatusServerCommand().command(deviceToken: deviceToken!, pushEnabled: pushEnabled, localizationType: localizationType, subscribeTeamRefs: teamRefs)
                }
            }

        }

    }

}
