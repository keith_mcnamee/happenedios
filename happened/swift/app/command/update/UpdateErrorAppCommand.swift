import Foundation

class UpdateErrorAppCommand: NSObject {

    private var app:App { return App.instance }

    func command(_ isError:Bool = true ) {

        DispatchQueue.main.async {
            if self.currentReachabilityStatus == .notReachable {
                self.app.appModel.errorStatus = .connectionError
            } else if !isError {
                self.app.appModel.errorStatus = .noError
            } else {
                self.app.appModel.errorStatus = .dataError
            }
            NotificationCenter.default.post(name: Notification.Name(AppConstant.MASTER_DATA_UPDATE_NOTIFICATION), object: nil)
        }
    }
}
