import Foundation

class UpdateActivityServerAppCommand {

    private var app:App { return App.instance }

    func command( _ wait:Bool ) {
        let hadActivity:Bool = app.appModel.waitForServer || app.appModel.waitForView
        self.app.appModel.waitForServer = wait
        var haveActivity:Bool = app.appModel.waitForServer || app.appModel.waitForView

        NotificationCenter.default.post(name: Notification.Name(AppConstant.CHECK_ACTIVITY_STATUS_NOTIFICATION), object: nil)
        if hadActivity && !haveActivity {
            DispatchQueue.main.async {
                haveActivity = self.app.appModel.waitForServer || self.app.appModel.waitForView
                if !haveActivity {
                    NotificationCenter.default.post(name: Notification.Name(AppConstant.ACTIVITY_ENDED_NOTIFICATION), object: nil)
                }
            }
        }
    }
}
