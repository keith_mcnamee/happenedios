import Foundation

class UpdateInitializingAppCommand {

    private var app:App { return App.instance }

    func command(_ initializing:Bool = false ) {
        DispatchQueue.main.async {
            if self.app.appModel.errorStatus == .noError {
                self.app.appModel.initializing = initializing
                self.app.appModel.waitForServer = false
                NotificationCenter.default.post(name: Notification.Name(AppConstant.MASTER_DATA_UPDATE_NOTIFICATION), object: nil)
            }
        }
    }
}
