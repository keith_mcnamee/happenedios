import Foundation

extension String {

    func indexOf(value:String, subFromStart:Int?=0, subFromEnd:Int?=0, distFrom:Int?=nil, lastIndexOf:Bool=false, indexFromEnd:Bool=false) -> Int? {
        let subs:String = substring(subFromStart: subFromStart, subFromEnd: subFromEnd, distFrom: distFrom)
        var rng:Range<String.Index>? = nil
        let backwards:Bool = lastIndexOf || indexFromEnd
        if !backwards {
            rng = subs.range(of: value)
        } else {
            rng = subs.range(of: value, options: String.CompareOptions.backwards)
        }
        if rng == nil {
            return nil
        }
        var dist = subs.distance(from: subs.startIndex, to: rng!.lowerBound)
        if subFromStart != nil {
            dist += subFromStart!
        }
        if !indexFromEnd {
            return dist
        }
        let fullDist:Int = distance(from: startIndex, to: endIndex)
        return fullDist - dist - 1
    }

    func substring(subFromStart:Int?=0, subFromEnd:Int?=0, distFrom:Int?=nil) -> String {
        var subs:String = self
        if (subFromStart == nil || subFromStart! <= 0) && (subFromEnd == nil || subFromEnd! <= 0) && (distFrom == nil || distFrom! < 0) {
            return subs
        }
        if subFromStart != nil && subFromStart! > 0 {
            let dis:Int = subs.distance(from: subs.startIndex, to: subs.endIndex)
            if( subFromStart! > dis ){
                return ""
            }
            let ind:String.Index = subs.index(subs.startIndex, offsetBy: subFromStart!)
            subs = String(subs.suffix(from: ind))
        }
        if subFromEnd != nil && subFromEnd! > 0 {
            let dis:Int = subs.distance(from: subs.startIndex, to: subs.endIndex)
            if( subFromEnd! > dis ){
                return ""
            }
            let ind:String.Index = subs.index(subs.endIndex, offsetBy: -subFromEnd!)
            subs = String(subs.prefix(upTo: ind))
        }

        if distFrom != nil {
            if (distFrom! > 0) {
                let dis:Int = subs.distance(from: subs.startIndex, to: subs.endIndex)
                if( distFrom! > dis ){
                    return subs
                }
                if (subFromStart != nil && subFromStart! > 0) || (subFromEnd == nil || subFromEnd! <= 0) {
                    let ind:String.Index = subs.index(subs.startIndex, offsetBy: distFrom!)
                    subs = String(subs.prefix(upTo: ind))
                } else {
                    let ind:String.Index = subs.index(subs.endIndex, offsetBy: -distFrom!)
                    subs = String(subs.suffix(from: ind))
                }
            } else {
                return ""
            }
        }
        return subs
    }

    func trim() -> String
    {
        return trimmingCharacters(in: CharacterSet.whitespaces)
    }

    func split(_ separator:String) -> [String] {
        return components(separatedBy: separator)
    }

    func splitJoin(_ splitSeparator:String, _ joinSeparator:String) -> String {
        return split(splitSeparator).joined(separator: joinSeparator)
    }

    var bool:Bool {
        return self != "" && self.lowercased() != "false" && self != "0"
    }

    var double:Double {
        return Double(self) != nil ? Double(self)! : 0
    }

    var int:Int {
        return Int(double)
    }
    var localized: String {
        return NSLocalizedString(self, comment: "")
    }
}