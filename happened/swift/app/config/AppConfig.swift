import Foundation
import UserNotifications

class AppConfig {

    let dateFormat:String = "yyyy-MM-dd HH:mm:ss"

    let dateTimezone:String = "UTC"

    let day1:String = "1970-01-03 00:00:00"

    let debugMode:Bool = false

    let requiredPushSettings:[UNAuthorizationOptions] = [.alert]

}
