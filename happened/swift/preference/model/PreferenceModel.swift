import Foundation

class PreferenceModel {

    private var app:App { return App.instance }

    var url:URL?
    var preference:NSMutableDictionary?

    var localizationVersions:[String:Int] {
        set
        {
            appPreferences.setValue(newValue, forKey: "localizationVersion")
            writeToFile()
        }
        get {
            return appPreferences.value(forKey: "localizationVersion") as! [String:Int];
        }
    }

    var serverURL:String {
        set
        {
            appPreferences.setValue(newValue, forKey: "serverURL")
            writeToFile()
        }
        get {
            return appPreferences.value(forKey: "serverURL") as! String
        }
    }

    var balancingVersion:Int {
        set
        {
            appPreferences.setValue(newValue, forKey: "balancingVersion")
            writeToFile()
        }
        get {
            return appPreferences.value(forKey: "balancingVersion") as! Int
        }
    }

    var pushAuthorizationStatus:PushAuthorizationStatus {
        set
        {
            let intValue:Int = newValue.rawValue
            devicePreferences.setValue(intValue, forKey: "pushAuthorizationStatus")
            writeToFile()
        }
        get {
            let intValue:Int = devicePreferences.value(forKey: "pushAuthorizationStatus") as! Int
            switch intValue {
                case PushAuthorizationStatus.notRequested.rawValue:
                     return .notRequested
                case PushAuthorizationStatus.denied.rawValue:
                    return .denied
                case PushAuthorizationStatus.authorized.rawValue:
                    return .authorized
                case PushAuthorizationStatus.notSupported.rawValue:
                    return .notSupported
                default:
                    return .unknown
            }
        }
    }

    var deviceToken:String? {
        set
        {
            var value:String? = newValue
            if value == nil {
                value = ""
            }
            devicePreferences.setValue(value, forKey: "deviceToken")
            writeToFile()
        }
        get {
            let value:String = devicePreferences.value(forKey: "deviceToken") as! String
            if value == "" {
                return nil
            }
            return value
        }
    }

    var localizationType:String? {
        set
        {
            var value:String? = newValue
            if value == nil {
                value = ""
            }
            devicePreferences.setValue(value, forKey: "localizationType")
            writeToFile()
        }
        get {
            let value:String = devicePreferences.value(forKey: "localizationType") as! String
            if value == "" {
                return nil
            }
            return value
        }
    }

    func getLocalizationVersion(_ localizationType:String) -> Int? {
        if let version = localizationVersions[localizationType] {
            return version
        }
        return nil
    }

    func setLocalizationVersion(_ localizationType:String, _ value:Int) {
        localizationVersions[localizationType] = value
        writeToFile()
    }

    var appPreferences:NSMutableDictionary {
        get {
            return preference!.value(forKey: "app") as! NSMutableDictionary
        }
    }

    var userPreferences:NSMutableDictionary {
        get {
            return preference!.value(forKey: "user") as! NSMutableDictionary
        }
    }

    var devicePreferences:NSMutableDictionary {
        get {
            return preference!.value(forKey: "device") as! NSMutableDictionary
        }
    }

    func writeToFile() {
        preference!.write(toFile: url!.path, atomically: true)
    }

}
