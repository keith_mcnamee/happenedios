import Foundation

class Preference {

    static let instance:Preference = Preference()

    let preferenceConfig:PreferenceConfig = PreferenceConfig();
    let preferenceHelper:PreferenceHelper = PreferenceHelper();
    let preferenceModel:PreferenceModel = PreferenceModel();

    func parsePreferenceCommand() -> ParsePreferenceCommand {
        return ParsePreferenceCommand();
    }

}
