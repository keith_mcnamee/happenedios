import Foundation

class ParsePreferenceCommand {

    private var app:App { return App.instance }
    private var preference:Preference { return Preference.instance }

    func command() {
        let preferencesURL = app.fileHelper.fileURL(preference.preferenceConfig.fileName, preference.preferenceConfig.fileExt)!
        preference.preferenceModel.url = preferencesURL

        let preferences = NSMutableDictionary(contentsOfFile:preferencesURL.path)!

        preference.preferenceModel.preference = preferences

        app.appModel.serverURL = preference.preferenceModel.serverURL
    }

}
