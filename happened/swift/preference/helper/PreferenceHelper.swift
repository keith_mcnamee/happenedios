import Foundation

class PreferenceHelper {

    private var app:App { return App.instance }
    private var balancing:Balancing { return Balancing.instance }
    private var preference:Preference { return Preference.instance }
    private var viewed:Viewed { return Viewed.instance }


    func setListingRefs(_ newValue:[Int:NSNull] ) {
        let intArr:[Int] = app.arrayHelper.intDicToIntArr(newValue)
        preference.preferenceModel.userPreferences.setValue(intArr, forKey: "listing")
        preference.preferenceModel.writeToFile()
        viewed.feedViewHelper.resetFeeds()
    }

    func getListingRefs( allowEmpty:Bool = false) -> [Int:NSNull] {
        let intArr:[Int] = preference.preferenceModel.userPreferences.value(forKey: "listing") as! [Int]
        var refs:[Int:NSNull] = app.arrayHelper.intArrToIntNulDic(intArr)
        if refs.count == 0 && !allowEmpty {
            refs = balancing.balancingModel.getDefaultVO()!.listingRefs!
            setListingRefs( refs )
        }
        return refs
    }

    func getTeamRefs() -> [Int:NSNull] {
        var refs:[Int:NSNull] = [:]
        let subscriptions:[Int:Bool] = getTeamSubscriptions()
        for key:Int in subscriptions.keys {
            refs[key] = NSNull()
        }
        return refs
    }

    func setTeamSubscriptions(_ newValue:[Int:Bool] ) {
        var stringBool:[String:Bool] = [:]
        for (key, value) in newValue {
            stringBool[String(key)] = value
        }
        preference.preferenceModel.userPreferences.setValue(stringBool, forKey: "team")
        preference.preferenceModel.writeToFile()
    }

    func getTeamSubscriptions() -> [Int:Bool] {
        let stringBool:[String:Bool] = preference.preferenceModel.userPreferences.value(forKey: "team") as! [String:Bool]
        var intBool:[Int:Bool] = [:]
        for (key, value) in stringBool {
            if let intKey:Int = Int(key) {
                intBool[intKey] = value
            }
        }
        return intBool
    }

    func modifyListings(_ listingRef:Int, selected:Bool) {
        var refs:[Int:NSNull] = getListingRefs( allowEmpty: true )
        if selected {
            refs[listingRef] = NSNull()
        } else {
            refs.removeValue(forKey: listingRef)
        }
        setListingRefs( refs )
    }

    func modifyTeams(_ teamRef:Int, selected:Bool, subscribed:Bool) {
        var subscriptions:[Int:Bool] = getTeamSubscriptions()
        let hadSelected:Bool = subscriptions[teamRef] != nil
        if selected {
            subscriptions[teamRef] = subscribed
        } else {
            subscriptions.removeValue(forKey: teamRef)
        }
        setTeamSubscriptions( subscriptions )
        let haveSelected:Bool = subscriptions[teamRef] != nil

        if hadSelected != haveSelected {
            viewed.feedViewHelper.resetFeeds(teamOnly: true)
        }
    }
}
