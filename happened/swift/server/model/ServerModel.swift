import Foundation

class ServerModel {

    var queueVOs:[ServerQueueVO] = []

    func removeDependentQueueItems() {
        var independentVOs:[ServerQueueVO] = []
        for queueVO:ServerQueueVO in queueVOs {
            if queueVO.independent {
                independentVOs.append(queueVO)
            }
        }
        queueVOs = independentVOs
    }

}
