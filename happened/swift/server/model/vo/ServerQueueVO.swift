import Foundation

class ServerQueueVO {

    let params : NSDictionary
    let requestType : String
    let viewHandler: (()->Void)?

    var independent : Bool
    var inProgress : Bool = false

    init(params:NSDictionary, requestType:String, independent:Bool, viewHandler: (()->Void)?=nil){
        self.params = params
        self.requestType = requestType
        self.independent = independent
        self.viewHandler = viewHandler
    }

}
