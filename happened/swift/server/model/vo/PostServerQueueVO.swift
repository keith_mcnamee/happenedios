import Foundation

class PostServerQueueVO: ServerQueueVO {

    let completionHandler : (NSDictionary)->Void

    init(params:NSDictionary, independent:Bool, completionHandler: @escaping (NSDictionary)->Void, viewHandler: (()->Void)?=nil){

        self.completionHandler = completionHandler

        super.init(params: params, requestType:ServerConstant.POST_REQUEST_TYPE, independent:independent, viewHandler: viewHandler)
    }
}
