import Foundation

class DownloadServerQueueVO: ServerQueueVO {

    let fileName:String
    let fileExt:String
    let completionHandler : ()->Void

    init(params:NSDictionary, independent:Bool, fileName:String, fileExt:String, completionHandler: @escaping ()->Void, viewHandler: (()->Void)?=nil){

        self.fileName = fileName
        self.fileExt = fileExt
        self.completionHandler = completionHandler

        super.init(params:params, requestType:ServerConstant.DOWNLOAD_REQUEST_TYPE, independent:independent, viewHandler: viewHandler)
    }
}
