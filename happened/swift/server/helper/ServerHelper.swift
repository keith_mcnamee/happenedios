import Foundation

class ServerHelper {

    private var app:App { return App.instance }
    private var balancing:Balancing { return Balancing.instance }
    private var happen: Happen { return Happen.instance }

    func missingRequestVOs( seasonRefs:[Int:Any]=[:], listingRefs:[Int:Any]=[:], teamRefs:[Int:Any]?=nil, partialDataOK:Bool = false, requireScenario:Bool=false ) -> [HappenedServerRequestVO] {

        var happenedRequestVOs:[HappenedServerRequestVO] = []

        func addSeasons(_ happenedRequestVO: HappenedServerRequestVO) {
            var found = false
            for compareHappenedRequestVO in happenedRequestVOs {
                if compareHappenedRequestVO.seasonRefs!.count == 0 {
                    continue
                }
                if app.arrayHelper.hasMatchingKeys( happenedRequestVO.listingRefs!, compareHappenedRequestVO.listingRefs! ) {
                    if teamRefs == nil || app.arrayHelper.hasMatchingKeys( happenedRequestVO.teamRefs!, compareHappenedRequestVO.teamRefs! ) {
                        found = true
                        compareHappenedRequestVO.seasonRefs = app.arrayHelper.mergeIntNulDics( compareHappenedRequestVO.seasonRefs!, happenedRequestVO.seasonRefs! )
                    }
                }
            }
            if !found {
                happenedRequestVOs.append(happenedRequestVO)
            }
        }

        func addListingSeasons(_ happenedRequestVO: HappenedServerRequestVO) {
            var found = false
            for compareHappenedRequestVO in happenedRequestVOs {
                if compareHappenedRequestVO.listingRefs!.count == 0 {
                    continue
                }
                if app.arrayHelper.hasMatchingKeys( happenedRequestVO.seasonRefs!, compareHappenedRequestVO.seasonRefs! ) {
                    if teamRefs == nil || app.arrayHelper.hasMatchingKeys( happenedRequestVO.teamRefs!, compareHappenedRequestVO.teamRefs! ) {
                        found = true
                        compareHappenedRequestVO.listingRefs = app.arrayHelper.mergeIntNulDics( compareHappenedRequestVO.listingRefs!, happenedRequestVO.listingRefs! )
                    }
                }
            }
            if !found {
                happenedRequestVOs.append(happenedRequestVO)
            }
        }

        func addTeamListings(_ happenedRequestVO: HappenedServerRequestVO) {
            var found = false
            for compareHappenedRequestVO in happenedRequestVOs {
                if compareHappenedRequestVO.teamRefs!.count == 0 {
                    continue
                }
                if app.arrayHelper.hasMatchingKeys( happenedRequestVO.seasonRefs!, compareHappenedRequestVO.seasonRefs! ) {
                    if app.arrayHelper.hasMatchingKeys( happenedRequestVO.listingRefs!, compareHappenedRequestVO.listingRefs! ) {
                        found = true
                        compareHappenedRequestVO.listingRefs = app.arrayHelper.mergeIntNulDics( compareHappenedRequestVO.listingRefs!, happenedRequestVO.listingRefs! )
                    }
                }
            }
            if !found {
                happenedRequestVOs.append(happenedRequestVO)
            }
        }

        for seasonRef in seasonRefs.keys {
            let seasonVO:SeasonVO? = happen.happenedModel.seasonVOs[seasonRef]
            let haveSeasonData:Bool = seasonVO != nil && ( !seasonVO!.haveData || !partialDataOK  )
            if !haveSeasonData {
                let happenedRequestVO: HappenedServerRequestVO = HappenedServerRequestVO()
                happenedRequestVO.seasonRefs = [seasonRef:NSNull()]
                happenedRequestVO.listingRefs = app.arrayHelper.cloneIntNulDic(listingRefs)
                if teamRefs != nil{
                    happenedRequestVO.teamRefs = app.arrayHelper.cloneIntNulDic(teamRefs!)
                }

                addSeasons(happenedRequestVO)
                continue
            }
            for listingRef in listingRefs.keys {
                if happen.happenedHelper.hasNonExistentListingSeason( seasonRef, listingRef ) {
                    continue
                }
                let listingSeasonRef:Int? = seasonVO!.listingSeasonRefs[ listingRef ]
                let listingSeasonVO:ListingSeasonVO? = listingSeasonRef != nil ? happen.happenedModel.getListingSeasonVO( seasonRef, listingRef ) : nil
                let haveListingSeasonData:Bool = listingSeasonVO != nil && ( listingSeasonVO!.haveData || ( partialDataOK && listingSeasonVO!.havePartialData ) )
                if !haveListingSeasonData {
                    let happenedRequestVO: HappenedServerRequestVO = HappenedServerRequestVO()
                    happenedRequestVO.seasonRefs = [seasonRef:NSNull()]
                    happenedRequestVO.listingRefs = [listingRef:NSNull()]
                    if teamRefs != nil{
                        happenedRequestVO.teamRefs = app.arrayHelper.cloneIntNulDic(teamRefs!)
                    }
                    addListingSeasons(happenedRequestVO)
                    continue
                }
                if teamRefs != nil {
                    for teamRef in teamRefs!.keys {
                        let teamListingRef:Int? = listingSeasonVO!.teamListingRefs[ teamRef ]
                        let teamListingVO:TeamListingVO? = teamListingRef != nil ? happen.happenedModel.getTeamListingVO( seasonRef, listingRef, teamRef ) : nil
                        let haveTeamListingData:Bool = teamListingVO != nil && ( teamListingVO!.haveScenario || (!requireScenario && (teamListingVO!.haveData || ( partialDataOK && teamListingVO!.havePartialData ) )) )
                        if !haveTeamListingData {
                            let happenedRequestVO: HappenedServerRequestVO = HappenedServerRequestVO()
                            happenedRequestVO.seasonRefs = [seasonRef:NSNull()]
                            happenedRequestVO.listingRefs = [teamRef:NSNull()]
                            happenedRequestVO.teamRefs = [teamRef:NSNull()]
                            addTeamListings(happenedRequestVO)
                        }
                    }
                }
            }
        }
        return happenedRequestVOs
    }

    func applyEventScenarioFilterToRequest( _ serverRequestVO:ServerRequestVO, _ filterVO: EventFeedFilterVO) {

        if !filterVO.allEventSelected {
            serverRequestVO.eventRefs = app.arrayHelper.cloneIntNulDic( filterVO.eventRefs )
        }
        serverRequestVO.toHappenState = filterVO.toHappenState
    }

}
