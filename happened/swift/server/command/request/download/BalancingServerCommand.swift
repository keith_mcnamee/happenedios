import Foundation

class BalancingServerCommand {

    private var app:App { return App.instance }
    private var balancing:Balancing { return Balancing.instance }
    private var server:Server { return Server.instance }
    private var preference:Preference { return Preference.instance }

    let serverRequestVO:ServerRequestVO = ServerRequestVO()

    func command() {

        handleView()

        balancing.balancingModel.initialized = false

        serverRequestVO.returnFile = true

        let constructServerService = server.constructServerService( serverRequestVO )
        constructServerService.constructDownloadBalancing()
        server.requestServerCommand().downloadFileCommand(params: constructServerService.params, fileName:balancing.balancingConfig.fileName, fileExt:balancing.balancingConfig.fileExt, completionHandler:handleServerResponse, viewHandler: handleView)
    }

    func handleView() {
        app.updateInitializingAppCommand().command(true)
    }

    func handleServerResponse() {
        balancing.parseBalancingCommand().command()

        preference.preferenceModel.balancingVersion = balancing.balancingModel.version!

        proceed()
    }

    private func proceed() {
        server.baseServerCommand().command()
    }

}
