import Foundation

class LocalizationServerCommand {

    private var app:App { return App.instance }
    private var localization:Localization { return Localization.instance }
    private var server:Server { return Server.instance }
    private var preference:Preference { return Preference.instance }

    private let serverRequestVO:ServerRequestVO = ServerRequestVO()

    func command() {

        handleView()

        let localizationVO:LocalizationVO = localization.localizationModel.typeVO()!
        localizationVO.initialized = false

        serverRequestVO.returnFile = true
        serverRequestVO.localizationType = localizationVO.type

        let constructServerService = server.constructServerService(serverRequestVO)
        constructServerService.constructDownloadLocalization()
        server.requestServerCommand().downloadFileCommand(params: constructServerService.params, fileName:localizationVO.fileName!, fileExt:localization.localizationConfig.fileExt, completionHandler:handleServerResponse, viewHandler: handleView)
    }

    func handleView() {
        app.updateInitializingAppCommand().command(true)
    }

    func handleServerResponse() {
        localization.parseLocalizationCommand().command()

        let localizationType:String = serverRequestVO.localizationType!

        let localizationVO:LocalizationVO = localization.localizationModel.typeVO(localizationType)!

        preference.preferenceModel.setLocalizationVersion(localizationVO.type, localizationVO.version!)

        proceed()
    }

    private func proceed() {
        server.balancingServerCommand().command()
    }

}
