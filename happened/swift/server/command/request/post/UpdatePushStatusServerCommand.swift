import Foundation

class UpdatePushStatusServerCommand {

    private var app:App { return App.instance }
    private var server:Server { return Server.instance }

    private let serverRequestVO:ServerRequestVO = ServerRequestVO()

    func command(deviceToken:String, pushEnabled:Bool, localizationType:String?=nil, subscribeTeamRefs:[Int:Any]?=nil ){

        serverRequestVO.deviceToken = deviceToken
        serverRequestVO.pushEnabled = pushEnabled
        serverRequestVO.localizationType = localizationType
        serverRequestVO.subscribeTeamRefs = subscribeTeamRefs

        let constructServerService:ConstructServerService = server.constructServerService( serverRequestVO )
        constructServerService.constructSubscribe()
        server.requestServerCommand().postDataCommand(params:constructServerService.params, independent: true, completionHandler:handleServerResponse)
    }

    func handleServerResponse(_ values:NSDictionary) {
        //
    }
}
