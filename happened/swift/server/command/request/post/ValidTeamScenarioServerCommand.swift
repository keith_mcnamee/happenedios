import Foundation

class ValidTeamScenarioServerCommand {

    private var app:App { return App.instance }
    private var server:Server { return Server.instance }

    private let serverRequestVO:ServerRequestVO = ServerRequestVO()

    func command( teamRefs:[Int:NSNull]) {

        handleView()

        serverRequestVO.happenedRequestVO.teamRefs = teamRefs

        let constructServerService:ConstructServerService = server.constructServerService( serverRequestVO )
        constructServerService.constructValidTeamScenario()
        server.requestServerCommand().postDataCommand(params:constructServerService.params, completionHandler:handleServerResponse, viewHandler: handleView)
    }

    func handleView() {
        app.updateActivityServerAppCommand().command(true)
    }

    func handleServerResponse(_ values:NSDictionary) {
        server.parseServerCommand( values:values, serverRequestVO:serverRequestVO).parseValidTeamScenarioCommand()
    }

}
