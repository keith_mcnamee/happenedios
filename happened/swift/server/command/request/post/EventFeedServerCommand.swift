import Foundation

class EventFeedServerCommand {

    private var app:App { return App.instance }
    private var balancing:Balancing { return Balancing.instance }
    private var preference:Preference { return Preference.instance }
    private var server:Server { return Server.instance }
    private var viewed: Viewed { return Viewed.instance }

    private let serverRequestVO:ServerRequestVO = ServerRequestVO()

    private var seasonGroupRef:Int?
    private var filterVO: EventFeedFilterVO?
    private var feedSource:FeedSource?

    func command(feedSource:FeedSource, seasonGroupRef:Int, isHappenedState: IsHappenedState, listingRefs:[Int:NSNull]? = nil, teamRefs:[Int:NSNull]? = nil, filterVO: EventFeedFilterVO? = nil, entriesFrom:Int?=nil, entriesSpan:Int?=nil ) {

        self.feedSource = feedSource
        self.seasonGroupRef = seasonGroupRef
        self.filterVO = filterVO

        handleView()

        let seasonRefs:[Int:NSNull] = balancing.balancingModel.seasonGroupVOs[seasonGroupRef]!.seasonRefs
        serverRequestVO.happenedRequestVO.seasonRefs = seasonRefs
        serverRequestVO.happenedRequestVO.listingRefs = listingRefs
        serverRequestVO.happenedRequestVO.teamRefs = teamRefs

        if filterVO?.filterString != nil {
            server.serverHelper.applyEventScenarioFilterToRequest(serverRequestVO, filterVO!)
        }

        serverRequestVO.isHappenedState = isHappenedState
        serverRequestVO.entriesFrom = entriesFrom
        serverRequestVO.entriesSpan = entriesSpan != nil ? entriesSpan : viewed.viewConfig.visibleEventScenarioSpan
        serverRequestVO.checkAssistData = true

        let constructServerService:ConstructServerService = server.constructServerService( serverRequestVO )
        constructServerService.constructEventFeed()
        server.requestServerCommand().postDataCommand(params:constructServerService.params, completionHandler:handleServerResponse, viewHandler: handleView)
    }

    func handleView() {
        app.updateActivityServerAppCommand().command(true)
    }

    func handleServerResponse(_ values:NSDictionary) {
        server.parseServerCommand( values:values, serverRequestVO:serverRequestVO ).parseEventFeedCommand( feedSource: feedSource!, seasonGroupRef:seasonGroupRef!, filterVO:filterVO)
    }

}
