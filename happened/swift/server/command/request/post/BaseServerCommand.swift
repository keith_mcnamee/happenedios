import Foundation

class BaseServerCommand {

    private var app:App { return App.instance }
    private var balancing:Balancing { return Balancing.instance }
    private var happen: Happen { return Happen.instance }
    private var localization:Localization { return Localization.instance }
    private var preference:Preference { return Preference.instance }
    private var server:Server { return Server.instance }
    private var viewed: Viewed { return Viewed.instance }

    private let serverRequestVO:ServerRequestVO = ServerRequestVO()

    private var seasonGroupRef:Int?

    func command(entriesSpan:Int?=nil) {

        happen.happenedModel.initialized = false

        self.seasonGroupRef = balancing.balancingModel.getDefaultVO()!.seasonGroupRef!

        handleView()

        let seasonRefs:[Int:NSNull] = balancing.balancingModel.seasonGroupVOs[seasonGroupRef!]!.seasonRefs
        serverRequestVO.happenedRequestVO.seasonRefs = seasonRefs
        serverRequestVO.happenedRequestVO.listingRefs = preference.preferenceHelper.getListingRefs()

        serverRequestVO.returnValidSeason = true
        serverRequestVO.returnValidSeasonListing = true
        serverRequestVO.returnValidListingSeason = true
        serverRequestVO.returnValidTeamScenario = false
        serverRequestVO.returnEventScenarioFeed = true
        serverRequestVO.isHappenedState = .happenedOnly
        if seasonGroupRef != balancing.balancingModel.latestSeasonGroupRef {
            serverRequestVO.isHappenedState = .both
        } else if !viewed.viewConfig.homeDefaultIsHappened{
            serverRequestVO.isHappenedState = .upcomingOnly
        }
        serverRequestVO.isAltIfEmpty = true
        serverRequestVO.checkAssistData = true
        serverRequestVO.entriesSpan = entriesSpan != nil ? entriesSpan : viewed.viewConfig.visibleEventScenarioSpan

        let constructServerService:ConstructServerService = server.constructServerService( serverRequestVO )
        constructServerService.constructBase()
        server.requestServerCommand().postDataCommand(params:constructServerService.params, completionHandler:handleServerResponse, viewHandler: handleView)
    }

    func handleView() {
        app.updateInitializingAppCommand().command(true)
    }

    func handleServerResponse(_ values:NSDictionary) {
        server.parseServerCommand( values:values, serverRequestVO:serverRequestVO).parseBaseCommand( seasonGroupRef: seasonGroupRef! )
        server.serverModel.removeDependentQueueItems()
    }
}
