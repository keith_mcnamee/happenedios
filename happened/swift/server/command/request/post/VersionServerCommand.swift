import Foundation

class VersionServerCommand {

    private var app:App { return App.instance }
    private var localization:Localization { return Localization.instance }
    private var server:Server { return Server.instance }

    private let serverRequestVO:ServerRequestVO = ServerRequestVO()

    func command( seasonRef:Int, listingRef:Int ) {

        handleView()

        let localizationVO:LocalizationVO = localization.localizationModel.typeVO()!
        serverRequestVO.localizationType = localizationVO.type

        let constructServerService:ConstructServerService = server.constructServerService( serverRequestVO )
        constructServerService.constructValidListing()
        server.requestServerCommand().postDataCommand(params:constructServerService.params, completionHandler:handleServerResponse, viewHandler: handleView)
    }

    func handleView() {
        app.updateActivityServerAppCommand().command(true)
    }

    func handleServerResponse(_ values:NSDictionary) {
        //nada
    }

}
