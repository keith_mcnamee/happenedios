import Foundation

class TeamScenarioServerCommand {

    private var app:App { return App.instance }
    private var happen: Happen { return Happen.instance }
    private var server:Server { return Server.instance }

    private let serverRequestVO:ServerRequestVO = ServerRequestVO()

    var fullListingSeasonRequested:Bool = false
    var fullTeamScenarioRequested:Bool = false

    func command( seasonRef:Int?=nil, listingRef:Int?=nil, teamRef:Int?=nil, teamListingRef:Int?=nil, listingScenarioRef:Int?=nil, eventScenarioRef:Int?=nil ) {
        if ( seasonRef == nil || listingRef == nil || teamRef == nil ) && teamListingRef == nil && listingScenarioRef == nil && eventScenarioRef == nil {
            return
        }

        handleView()

        fullListingSeasonRequested = true
        if seasonRef != nil && listingRef != nil && teamRef != nil {
            serverRequestVO.happenedRequestVO.seasonRefs = [seasonRef!:NSNull()]
            serverRequestVO.happenedRequestVO.listingRefs = [listingRef!:NSNull()]
            serverRequestVO.happenedRequestVO.teamRefs = [teamRef!:NSNull()]
            if let listingSeasonVO:ListingSeasonVO = happen.happenedModel.getListingSeasonVO(seasonRef!, listingRef!) {
                fullListingSeasonRequested = !listingSeasonVO.haveData
            }
        } else if listingScenarioRef != nil {
            serverRequestVO.happenedRequestVO.listingScenarioRefs = [listingScenarioRef!:NSNull()]
        } else if eventScenarioRef != nil {
            serverRequestVO.happenedRequestVO.eventScenarioRefs = [eventScenarioRef!:NSNull()]
        } else {
            serverRequestVO.happenedRequestVO.teamListingRefs = [teamListingRef!:NSNull()]
        }

        let constructServerService:ConstructServerService = server.constructServerService( serverRequestVO )
        constructServerService.constructTeamScenario( fullListingSeasonRequested: fullListingSeasonRequested)
        fullTeamScenarioRequested = serverRequestVO.returnValidTeamScenario != nil ? serverRequestVO.returnValidTeamScenario! : false
        server.requestServerCommand().postDataCommand(params:constructServerService.params, completionHandler:handleServerResponse, viewHandler: handleView)
    }

    func handleView() {
        app.updateActivityServerAppCommand().command(true)
    }

    func handleServerResponse(_ values:NSDictionary) {
        server.parseServerCommand( values:values, serverRequestVO:serverRequestVO).parseTeamScenarioCommand( fullListingSeasonRequested: fullListingSeasonRequested, fullTeamScenarioRequested:fullTeamScenarioRequested)
    }

}
