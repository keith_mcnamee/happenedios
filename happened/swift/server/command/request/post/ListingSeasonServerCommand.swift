import Foundation

class ListingSeasonServerCommand {

    private var app:App { return App.instance }
    private var server:Server { return Server.instance }

    private let serverRequestVO:ServerRequestVO = ServerRequestVO()

    func command(seasonRef:Int?=nil, listingRef:Int?=nil, listingSeasonRef:Int?=nil ) {
        if ( seasonRef == nil || listingRef == nil ) && listingSeasonRef == nil {
            return
        }

        handleView()

        if seasonRef != nil && listingRef != nil {
            serverRequestVO.happenedRequestVO.seasonRefs = [seasonRef!:NSNull()]
            serverRequestVO.happenedRequestVO.listingRefs = [listingRef!:NSNull()]
        } else {
            serverRequestVO.happenedRequestVO.listingSeasonRefs = [listingSeasonRef!:NSNull()]
        }

        let constructServerService:ConstructServerService = server.constructServerService( serverRequestVO )
        constructServerService.constructListingSeason()
        server.requestServerCommand().postDataCommand(params:constructServerService.params, completionHandler:handleServerResponse, viewHandler: handleView)
    }

    func handleView() {
        app.updateActivityServerAppCommand().command(true)
    }

    func handleServerResponse(_ values:NSDictionary) {
        server.parseServerCommand( values:values, serverRequestVO:serverRequestVO).parseListingSeasonCommand()
    }
}
