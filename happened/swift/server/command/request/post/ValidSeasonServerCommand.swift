import Foundation

class ValidSeasonServerCommand {

    private var app:App { return App.instance }
    private var balancing:Balancing { return Balancing.instance }
    private var server:Server { return Server.instance }

    private let serverRequestVO:ServerRequestVO = ServerRequestVO()

    private var seasonGroupRefs:[Int:NSNull]?

    func command( seasonGroupRefs:[Int:NSNull] ) {

        self.seasonGroupRefs = seasonGroupRefs

        handleView()

        let seasonRefs:[Int:NSNull] = balancing.balancingHelper.getAllSeasonRefs(seasonGroupRefs: seasonGroupRefs)
        serverRequestVO.happenedRequestVO.seasonRefs = seasonRefs

        let constructServerService:ConstructServerService = server.constructServerService( serverRequestVO )
        constructServerService.constructValidSeason()
        server.requestServerCommand().postDataCommand(params:constructServerService.params, completionHandler:handleServerResponse, viewHandler: handleView)
    }

    func handleView() {
        app.updateActivityServerAppCommand().command(true)
    }

    func handleServerResponse(_ values:NSDictionary) {
        server.parseServerCommand( values:values, serverRequestVO:serverRequestVO).parseValidSeasonCommand( seasonGroupRefs:seasonGroupRefs! )
    }

}
