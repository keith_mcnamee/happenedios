import Foundation

class RequestServerCommand {

    private var app:App { return App.instance }
    private var balancing:Balancing { return Balancing.instance }
    private var happen: Happen { return Happen.instance }
    private var localization:Localization { return Localization.instance }
    private var preference:Preference { return Preference.instance }
    private var server:Server { return Server.instance }
    private var viewed:Viewed { return Viewed.instance }

    var thisQueueVO:ServerQueueVO?

    //The native URLSession does have have asynchronous functionality invalidateAndCancel method is unreliable for
    //stopping response handling dor queued items. Because of that I'm keeping a queueing method I wrote myself in

    func postDataCommand(params:NSDictionary, independent:Bool=false, completionHandler: @escaping (NSDictionary)->Void, viewHandler: (()->Void)?=nil ) {

        let queueVO:ServerQueueVO = PostServerQueueVO(params:params, independent:independent, completionHandler:completionHandler, viewHandler: viewHandler)
        applyRequest(queueVO)
    }

    func downloadFileCommand(params:NSDictionary, independent:Bool=false, fileName:String, fileExt:String, completionHandler: @escaping ()->Void, viewHandler: (()->Void)?=nil ) {

        let queueVO:ServerQueueVO = DownloadServerQueueVO(params:params, independent:independent, fileName:fileName, fileExt:fileExt, completionHandler:completionHandler, viewHandler: viewHandler)
        applyRequest(queueVO)
    }


    func nextRequest() {
        if server.serverModel.queueVOs.count == 0 {
            return
        }
        thisQueueVO = server.serverModel.queueVOs[0]
        if thisQueueVO!.inProgress {
            return
        }

        thisQueueVO!.inProgress = true
        thisQueueVO!.viewHandler?()

        switch thisQueueVO!.requestType {
            case ServerConstant.POST_REQUEST_TYPE:
                postRequest(thisQueueVO!)
            break
            case ServerConstant.DOWNLOAD_REQUEST_TYPE:
                downloadRequest(thisQueueVO!)
            break
            default:
                handleError()
            break
        }
    }

    func handlePostResponse(_ data: Data?, response: URLResponse?, error: Error?) {

        guard let _:Data = data, let _:URLResponse = response, error == nil else {
            //dispatch failed error message
            handleError()
            return
        }

        var values:NSDictionary?

        let jsonObject:Any? = try? JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers)
        if jsonObject != nil {
            values = jsonObject! as? NSDictionary
            if( values == nil ) {
                handleError()
                return
            }
        } else {
            handleError()
            return
        }

        if server.serverModel.queueVOs.count == 0 {
            return
        }
        let queueVO:PostServerQueueVO? = server.serverModel.queueVOs[0] as? PostServerQueueVO
        if queueVO == nil {
            return
        }
        if queueVO !== thisQueueVO {
            return
        }
        queueVO!.inProgress = false

        if !checkRequestFailed(values!) {
            if !checkLocationChange(values!) {
                if !checkVersionChange(values!) {
                    queueVO!.completionHandler(values!)
                }
            }
        }
        nextQueueItem( removeQueueVO:queueVO )
    }


    func handleDownloadResponse(_ tempURL: URL?, response: URLResponse?, error: Error?) {

        guard let _:URL = tempURL, let _:URLResponse = response, error == nil else {
            handleError()
            return
        }

        if server.serverModel.queueVOs.count == 0 {
            return
        }
        let queueVO:DownloadServerQueueVO? = server.serverModel.queueVOs[0] as? DownloadServerQueueVO
        if queueVO == nil {
            return
        }
        if queueVO !== thisQueueVO {
            return
        }
        queueVO!.inProgress = false

        let localUrl:URL? = app.fileHelper.copyToDocumentDirectory(tempURL!, queueVO!.fileName, queueVO!.fileExt)

        if localUrl == nil {
            handleError()
            return
        }

        queueVO!.completionHandler()
        nextQueueItem( removeQueueVO:queueVO )
    }

    private func nextQueueItem( removeQueueVO:ServerQueueVO? = nil ) {
        if server.serverModel.queueVOs.count != 0 {
            let firstQueueVO:ServerQueueVO? = server.serverModel.queueVOs[0]
            if firstQueueVO === removeQueueVO {
                server.serverModel.queueVOs.remove(at: 0)
            }
        }
        if server.serverModel.queueVOs.count != 0 {
            server.requestServerCommand().nextRequest()
        } else {
            server.completeServerCommand().command()
        }
    }

    func handleError() {
        server.serverModel.queueVOs.removeAll()
        app.updateErrorAppCommand().command()
    }

    private func applyRequest(_ queueVO:ServerQueueVO ) {

        server.serverModel.queueVOs.append(queueVO)
        if server.serverModel.queueVOs.count != 1 {
            return
        }
        nextRequest()
    }

    private func postRequest(_ queueVO:ServerQueueVO) {

        let serverURL:String = app.appModel.serverURL!

        let url:URL = URL(string: serverURL)!
        let session:URLSession = URLSession.shared

        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.cachePolicy = NSURLRequest.CachePolicy.reloadIgnoringCacheData

        do {
            request.httpBody = try JSONSerialization.data(withJSONObject: queueVO.params)

        } catch {
            app.updateErrorAppCommand().command()
            return
        }

        let task = session.dataTask(with: request, completionHandler: handlePostResponse)
        if app.commonHelper.isDebug && Bool(request.httpBody != nil){
            if let string:String = String(data: request.httpBody!, encoding: String.Encoding.utf8) {
                print(serverURL+"?params="+string)
            }
        }

        DispatchQueue.main.async {
            task.resume()
        }
    }

    private func downloadRequest(_ queueVO:ServerQueueVO) {
        let serverURL:String = app.appModel.serverURL!
        let sessionConfig = URLSessionConfiguration.default
        let session = URLSession(configuration: sessionConfig)
        let url:URL = URL(string: serverURL)!
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.cachePolicy = NSURLRequest.CachePolicy.reloadIgnoringCacheData
        do {
            request.httpBody = try JSONSerialization.data(withJSONObject: queueVO.params)

        } catch {
            app.updateErrorAppCommand().command()
            return
        }
        let task = session.downloadTask(with: request, completionHandler: handleDownloadResponse)

        DispatchQueue.main.async {
            task.resume()
        }
    }

    private func checkRequestFailed(_ values:NSDictionary) -> Bool {
        let requestFailed:Bool = app.arrayHelper.booleanFromNsDic(values, ServerConstant.REQUEST_FAILED, defaultValue: false)!
        if requestFailed {
            handleError()
            return true
        }
        return false
    }

    private func checkLocationChange(_ values:NSDictionary) -> Bool {
        if let locationChange:NSDictionary = values.object(forKey: ServerConstant.LOCATION_CHANGE_PARAM) as? NSDictionary {
            if let url:String = app.arrayHelper.stringFromNsDic(locationChange, ServerConstant.URL_PARAM) {
                let permanent:Bool = app.arrayHelper.booleanFromNsDic(locationChange, ServerConstant.PERMANENT_PARAM, defaultValue: false)!
                app.appModel.serverURL = url
                if permanent {
                    preference.preferenceModel.serverURL = url
                }
                server.serverModel.removeDependentQueueItems()
                server.baseServerCommand().command()
                return true
            }
        }
        return false
    }

    private func checkVersionChange(_ values:NSDictionary) -> Bool {

        var localizationRequired:Bool = false
        var balancingRequired:Bool = false
        var happenRequired:Bool = false

        if let localizationArray:[NSDictionary] = values.object(forKey: ServerConstant.LOCALIZATION_PARAM) as? [NSDictionary] {
            for localizationDictionary in localizationArray {
                if let type:String = app.arrayHelper.stringFromNsDic(localizationDictionary, ServerConstant.TYPE_PARAM) {
                    if type == localization.localizationModel.currentType! {
                        if let version:Int = app.arrayHelper.intFromNsDic(localizationDictionary, ServerConstant.VERSION_IN_PARAM) {
                            let localizationVO:LocalizationVO = localization.localizationModel.typeVO()!
                            if version != localizationVO.version {
                                localizationRequired = true
                                localizationVO.reset()
                                localizationVO.version = version
                            }
                        }
                        break;
                    }
                }
            }
        }

        if let balancingValues:NSDictionary = values.object(forKey: ServerConstant.BALANCING_PARAM) as? NSDictionary {
            if let version:Int = app.arrayHelper.intFromNsDic(balancingValues, ServerConstant.VERSION_IN_PARAM) {
                if version != balancing.balancingModel.version {
                    balancingRequired = true
                    balancing.balancingModel.reset()
                    balancing.balancingModel.version = version
                }
            }
        }

        if let version:Int = app.arrayHelper.intFromNsDic(values, ServerConstant.CONTENT_VERSION_PARAM) {
            if version != happen.happenedModel.clientVersion {
                if happen.happenedModel.clientVersion != nil {
                    happen.happenedModel.reset()
                    viewed.resetViewCommand().command()
                    happenRequired = true
                }
                happen.happenedModel.clientVersion = version
            }
        }

        if localizationRequired {
            server.serverModel.removeDependentQueueItems()
            server.localizationServerCommand().command()
            return true
        }
        if balancingRequired {
            server.serverModel.removeDependentQueueItems()
            server.balancingServerCommand().command()
            return true
        }
        if happenRequired {
            server.serverModel.removeDependentQueueItems()
            server.baseServerCommand().command()
            return true
        }
        return false
    }

}
