import Foundation

class ParseServerCommand {

    private var app:App { return App.instance }
    private var balancing:Balancing { return Balancing.instance }
    private var happen: Happen { return Happen.instance }
    private var viewed: Viewed { return Viewed.instance }

    private var values:NSDictionary
    private var serverRequestVO:ServerRequestVO

    init(values:NSDictionary, serverRequestVO:ServerRequestVO ) {
        self.values = values
        self.serverRequestVO = serverRequestVO
    }

    func parseBaseCommand( seasonGroupRef:Int ) {
        insertValid( seasonGroupRefs: [seasonGroupRef:NSNull()] )
        insertSeasonValues( fullListingSeasonRequested:serverRequestVO.returnRelevantListingSeason )
        createNonExistentListingSeasons()
        updateScenarios( fullTeamScenarioRequested: serverRequestVO.returnTeamDetails )
        updateFixtureFeed( feedSource:.listing, seasonGroupRef: seasonGroupRef )
        updateEventFeed( feedSource:.listing, seasonGroupRef:seasonGroupRef )
        happen.happenedModel.initialized = true
    }

    func parseEventFeedCommand(feedSource:FeedSource, seasonGroupRef:Int, filterVO: EventFeedFilterVO?=nil ) {
        insertValid( seasonGroupRefs: [seasonGroupRef:NSNull()] )
        insertSeasonValues()
        createNonExistentListingSeasons()
        updateEventFeed( feedSource:feedSource, seasonGroupRef:seasonGroupRef, filterVO:filterVO )
    }

    func parseFixtureFeedCommand(feedSource:FeedSource, seasonGroupRef:Int, filterVO: FeedFilterVO?=nil ) {
        insertValid( seasonGroupRefs: [seasonGroupRef:NSNull()] )
        insertSeasonValues()
        createNonExistentListingSeasons()
        updateFixtureFeed( feedSource:feedSource, seasonGroupRef:seasonGroupRef, filterVO:filterVO)
    }

    func parseListingSeasonCommand() {
        insertValid()
        insertSeasonValues(fullListingSeasonRequested:true)
        createNonExistentListingSeasons()
        updateScenarios()
    }

    func parseTeamScenarioCommand(fullListingSeasonRequested:Bool = false, fullTeamScenarioRequested:Bool = false ) {
        insertValid( fullTeamScenarioRequested: fullTeamScenarioRequested )
        if fullListingSeasonRequested {
            insertSeasonValues(fullListingSeasonRequested:true)
            createNonExistentListingSeasons()
        }
        createNonExistentTeamSeasons()
        updateScenarios(fullTeamScenarioRequested:true)
    }

    func parseValidListingCommand() {
        insertValid()
        createNonExistentListingSeasons()
    }

    func parseValidSeasonCommand( seasonGroupRefs:[Int:NSNull] ) {
        insertValid( seasonGroupRefs: seasonGroupRefs )
    }

    func parseValidTeamListingCommand() {
        insertValid()
        createNonExistentTeamSeasons()
    }

    func parseValidTeamScenarioCommand() {
        insertValid( fullTeamScenarioRequested:true )
        createNonExistentTeamSeasons()
    }

    private func insertValid( seasonGroupRefs:[Int:NSNull]? = nil, fullTeamScenarioRequested:Bool = false  ) {
        parseValidSeasons( seasonGroupRefs: seasonGroupRefs )
        parseValidSeasonListings()
        parseValidListingSeasons()
        parseValidTeamListings( fullTeamScenarioRequested:fullTeamScenarioRequested )
        parseValidListingSeasonEvents()
    }

    private func insertSeasonValues( fullListingSeasonRequested:Bool = false ) {
        parseListingSeasons( fullListingSeasonRequested:fullListingSeasonRequested )
        parseTeamListings( fullListingSeasonRequested:fullListingSeasonRequested )
        parseFixtures( fullListingSeasonRequested:fullListingSeasonRequested )
        parseAdjustTeams()
        parsePlaceEvents()
        parseExemptionTeams()
        applyListingSeasons()
    }

    private func updateScenarios( fullTeamScenarioRequested:Bool = false ) {
        parseEventScenarios()
        parseListingScenarios()
        if fullTeamScenarioRequested {
            markTeamListings()
        }
    }

    private func createNonExistentListingSeasons() {
        let happenedRequestVOs:[HappenedServerRequestVO]? = serverRequestVO.allHappenedRequestVOs
        if happenedRequestVOs == nil {
            return
        }
        for happenedRequestVO: HappenedServerRequestVO in happenedRequestVOs! {
            if happenedRequestVO.seasonRefs == nil || happenedRequestVO.listingRefs == nil {
                continue
            }
            for listingRef in happenedRequestVO.listingRefs!.keys {
                if happen.happenedModel.listingVOs[listingRef] == nil {
                    happen.happenedModel.nonExistentListingRefs[ listingRef ] = NSNull()
                }
            }
            for seasonRef in happenedRequestVO.seasonRefs!.keys {
                for listingRef in happenedRequestVO.listingRefs!.keys {
                    if happen.happenedModel.getListingSeasonVO( seasonRef, listingRef ) == nil {
                        if happen.happenedModel.nonExistentListingSeasons[ seasonRef ] == nil {
                            happen.happenedModel.nonExistentListingSeasons[ seasonRef ] = IntDicVOsVO()
                        }
                        happen.happenedModel.nonExistentListingSeasons[ seasonRef ]!.vos[ listingRef ] = NSNull()
                    }
                }
            }
        }
    }

    private func createNonExistentTeamSeasons() {
        let happenedRequestVOs:[HappenedServerRequestVO]? = serverRequestVO.allHappenedRequestVOs
        if happenedRequestVOs == nil {
            return
        }
        for happenedRequestVO: HappenedServerRequestVO in happenedRequestVOs! {
            if happenedRequestVO.seasonRefs == nil || happenedRequestVO.teamRefs == nil {
                continue
            }
            for teamRef in happenedRequestVO.teamRefs!.keys {
                if happen.happenedModel.teamVOs[teamRef] == nil {
                    happen.happenedModel.nonExistentTeamRefs[ teamRef ] = NSNull()
                }
            }
            for seasonRef in happenedRequestVO.seasonRefs!.keys {
                for teamRef in happenedRequestVO.teamRefs!.keys {
                    if happen.happenedModel.getTeamSeasonVO( seasonRef, teamRef ) == nil {
                        if happen.happenedModel.nonExistentTeamSeasons[ seasonRef ] == nil {
                            happen.happenedModel.nonExistentTeamSeasons[ seasonRef ] = IntDicVOsVO()
                        }
                        happen.happenedModel.nonExistentTeamSeasons[ seasonRef ]!.vos[ teamRef ] = NSNull()
                    }
                }
            }
        }
    }

    private func parseValidSeasons( seasonGroupRefs:[Int:NSNull]? = nil) {
        let dictionary:[Int:NSNull]? = app.arrayHelper.intNulArrFromNsDicArr(values, ServerConstant.VALID_SEASON_PARAM)
        if dictionary == nil {
            return
        }
        for seasonRef:Int in dictionary!.keys {
            _ = initializeSeasonVO(seasonRef)
        }
        if seasonGroupRefs != nil {
            for seasonGroupRef:Int in seasonGroupRefs!.keys {
                if let seasonGroupBalancingVO:SeasonGroupBalancingVO = balancing.balancingModel.seasonGroupVOs[seasonGroupRef] {
                    for seasonRef:Int in seasonGroupBalancingVO.seasonRefs.keys {
                        if dictionary![seasonRef] == nil {
                            happen.happenedModel.nonExistentSeasonRefs[seasonRef] = NSNull()
                        }
                    }
                }
            }
        }
    }

    private func parseValidSeasonListings() {
        let dictionary:NSDictionary? = app.arrayHelper.nsDicFromNsDic(values, ServerConstant.VALID_SEASON_LISTING_PARAM)
        if dictionary == nil {
            return
        }
        for seasonKey:String in dictionary!.allKeys as! [String] {
            let seasonRef:Int? = Int(seasonKey)
            if seasonRef == nil {
                continue
            }
            let seasonVO:SeasonVO? = initializeSeasonVO(seasonRef!)
            if seasonRef == nil {
                continue
            }
            seasonVO!.haveData = true

            let listingDictionary:NSDictionary? = dictionary![seasonKey]! as? NSDictionary
            if listingDictionary == nil {
                continue
            }
            for listingKey:String in listingDictionary!.allKeys as! [String] {
                let listingRef:Int? = Int(listingKey)
                if listingRef == nil {
                    continue
                }
                let listingSeasonRef:Int? = app.arrayHelper.intFromNsDic(listingDictionary!, listingKey)
                if listingSeasonRef == nil {
                    continue
                }
                _ = initializeListingSeasonVO(listingSeasonRef!, seasonRef!, listingRef!)
            }
        }
    }

    private func parseValidListingSeasons() {
        let dictionary:NSDictionary? = app.arrayHelper.nsDicFromNsDic(values, ServerConstant.VALID_LISTING_SEASON_PARAM)
        if dictionary == nil {
            return
        }
        for listingKey:String in dictionary!.allKeys as! [String] {
            let listingRef:Int? = Int(listingKey)
            if listingRef == nil {
                continue
            }

            let listingVO:ListingVO? = initializeListingVO(listingRef!)
            if listingVO == nil {
                continue
            }
            listingVO!.haveData = true

            let seasonDictionary:NSDictionary? = dictionary![listingKey]! as? NSDictionary
            if seasonDictionary == nil {
                continue
            }
            for seasonKey:String in seasonDictionary!.allKeys as! [String] {
                let seasonRef:Int? = Int(seasonKey)
                if seasonRef == nil {
                    continue
                }
                let listingSeasonRef:Int? = app.arrayHelper.intFromNsDic(seasonDictionary!, seasonKey)
                if listingSeasonRef == nil {
                    continue
                }
                _ = initializeListingSeasonVO(listingSeasonRef!, seasonRef!, listingRef!)
            }
        }
    }

    private func parseValidTeamListings(fullTeamScenarioRequested:Bool = false ) {
        let dictionary:NSDictionary? = app.arrayHelper.nsDicFromNsDic(values, ServerConstant.VALID_TEAM_SEASON_PARAM)
        if dictionary == nil {
            return
        }
        for seasonKey:String in dictionary!.allKeys as! [String] {
            let seasonRef:Int? = Int(seasonKey)
            if seasonRef == nil {
                continue
            }
            let listingDictionary:NSDictionary? = dictionary![seasonKey]! as? NSDictionary
            if listingDictionary == nil {
                continue
            }
            for listingKey:String in listingDictionary!.allKeys as! [String] {
                let listingRef:Int? = Int(listingKey)
                if listingRef == nil {
                    continue
                }
                let teamDictionary:NSDictionary? = listingDictionary![listingKey]! as? NSDictionary
                if teamDictionary == nil {
                    continue
                }
                for teamKey:String in teamDictionary!.allKeys as! [String] {
                    let teamRef:Int? = Int(teamKey)
                    if teamRef == nil {
                        continue
                    }

                    let teamVO:TeamVO? = initializeTeamVO(teamRef!)
                    if teamVO == nil {
                        continue
                    }

                    teamVO!.havePartialData = true
                    if fullTeamScenarioRequested {
                        teamVO!.haveData = true
                    }

                    let teamSeasonVO:TeamSeasonVO? = initializeTeamSeasonVO(seasonRef!, teamRef!)
                    if teamSeasonVO == nil {
                        continue
                    }
                    if fullTeamScenarioRequested {
                        teamSeasonVO!.haveData = true
                    }

                    let teamListingTypeDictionary:NSDictionary? = teamDictionary![teamKey]! as? NSDictionary
                    if teamListingTypeDictionary == nil {
                        continue
                    }

                    let listingSeasonRef:Int? = app.arrayHelper.intFromNsDic(teamListingTypeDictionary!, ServerConstant.LISTING_SEASON_IN_PARAM)
                    if listingSeasonRef == nil {
                        continue
                    }

                    let listingSeasonVO:ListingSeasonVO? = initializeListingSeasonVO(listingSeasonRef!, seasonRef!, listingRef!)
                    if listingSeasonVO == nil {
                        continue
                    }

                    let teamListingRef:Int? = app.arrayHelper.intFromNsDic(teamListingTypeDictionary!, ServerConstant.REF_PARAM)
                    if teamListingRef == nil {
                        continue
                    }
                    if initializeTeamListingVO(teamListingRef!, seasonRef!, listingRef!, teamRef!) != nil {
                        if fullTeamScenarioRequested {

                            if let bestDictionary:NSDictionary  = app.arrayHelper.nsDicFromNsDic(teamListingTypeDictionary!, ServerConstant.BEST_PARAM) {
                                if let bestScenarioRef:Int = app.arrayHelper.intFromNsDic(bestDictionary, ServerConstant.YES_PARAM) {
                                    _ = initializeListingScenarioVO(bestScenarioRef, seasonRef!, listingRef!, teamRef!, true)
                                }
                                if let worstScenarioRef:Int = app.arrayHelper.intFromNsDic(bestDictionary, ServerConstant.NO_PARAM) {
                                    _ = initializeListingScenarioVO(worstScenarioRef, seasonRef!, listingRef!, teamRef!, false)
                                }
                            }

                            if let eventDictionary:NSDictionary  = app.arrayHelper.nsDicFromNsDic(teamListingTypeDictionary!, ServerConstant.EVENT_PARAM) {
                                for eventKey:String in eventDictionary.allKeys as! [String] {
                                    let eventRef:Int? = Int(eventKey)
                                    if eventRef == nil {
                                        continue
                                    }
                                    let eventTypeDictionary:NSDictionary? = eventDictionary[eventKey]! as? NSDictionary
                                    if eventTypeDictionary == nil {
                                        continue
                                    }
                                    if let toHappenRef:Int = app.arrayHelper.intFromNsDic(eventTypeDictionary!, ServerConstant.YES_PARAM) {
                                        _ = initializeEventScenarioVO(toHappenRef, seasonRef!, listingRef!, teamRef!, eventRef!, true)
                                    }
                                    if let notHappenRef:Int = app.arrayHelper.intFromNsDic(eventTypeDictionary!, ServerConstant.NO_PARAM) {
                                        _ = initializeEventScenarioVO(notHappenRef, seasonRef!, listingRef!, teamRef!, eventRef!, false)
                                    }
                                }
                            }
                        }

                    }
                }
            }
        }
    }

    private func parseValidListingSeasonEvents() {
        let dictionary:NSDictionary? = app.arrayHelper.nsDicFromNsDic(values, ServerConstant.VALID_LISTING_SEASON_EVENT_PARAM)
        if dictionary == nil {
            return
        }
        for listingSeasonKey:String in dictionary!.allKeys as! [String] {
            let listingSeasonRef:Int? = Int(listingSeasonKey)
            if listingSeasonRef == nil {
                continue
            }
            let validEventRefs:[Int:NSNull]? = app.arrayHelper.intNulArrFromNsDicArr(dictionary!, listingSeasonKey)
            if validEventRefs == nil {
                continue
            }
            let listingSeasonVO:ListingSeasonVO? = happen.happenedModel.listingSeasonVOs[listingSeasonRef!]
            if listingSeasonVO == nil {
                continue
            }
            listingSeasonVO!.validEventRefs = validEventRefs!
        }
    }

    private func parseListingSeasons( fullListingSeasonRequested:Bool = false ) {
        let array:NSArray? = app.arrayHelper.nsArrFromNsDic(values, ServerConstant.LISTING_SEASON_IN_PARAM)
        if array == nil {
            return
        }
        for entry in array! {
            let dictionary:NSDictionary? = entry as? NSDictionary
            if dictionary == nil {
                continue
            }
            let ref:Int? = app.arrayHelper.intFromNsDic(dictionary!, ServerConstant.REF_PARAM)
            let seasonRef:Int? = app.arrayHelper.intFromNsDic(dictionary!, ServerConstant.SEASON_REF_PARAM)
            let listingRef:Int? = app.arrayHelper.intFromNsDic(dictionary!, ServerConstant.LISTING_REF_PARAM)
            if ref == nil {
                continue
            }
            if seasonRef == nil {
                continue
            }
            if listingRef == nil {
                continue
            }
            let existingVO:ListingSeasonVO? = happen.happenedModel.listingSeasonVOs[ref!]
            if existingVO != nil {
                if existingVO!.haveData || Bool( existingVO!.havePartialData && !fullListingSeasonRequested ) {
                    continue
                }
            }
            if let vo:ListingSeasonVO = initializeListingSeasonVO(ref!, seasonRef!, listingRef!) {
                vo.haveData = fullListingSeasonRequested
            }
        }
    }

    private func parseTeamListings( fullListingSeasonRequested:Bool = false ) {
        let array:NSArray? = app.arrayHelper.nsArrFromNsDic(values, ServerConstant.TEAM_LISTING_IN_PARAM)
        if array == nil {
            return
        }
        for entry in array! {
            let dictionary: NSDictionary? = entry as? NSDictionary
            if dictionary == nil {
                continue
            }
            let ref:Int? = app.arrayHelper.intFromNsDic(dictionary!, ServerConstant.REF_PARAM)
            let seasonRef:Int? = app.arrayHelper.intFromNsDic(dictionary!, ServerConstant.SEASON_REF_PARAM)
            let listingRef:Int? = app.arrayHelper.intFromNsDic(dictionary!, ServerConstant.LISTING_REF_PARAM)
            let teamRef:Int? = app.arrayHelper.intFromNsDic(dictionary!, ServerConstant.TEAM_REF_PARAM)
            let globalRank:Int = app.arrayHelper.intFromNsDic(dictionary!, ServerConstant.GLOBAL_RANK_PARAM, defaultValue:0)!
            if ref == nil {
                continue
            }
            if seasonRef == nil {
                continue
            }
            if listingRef == nil {
                continue
            }
            if teamRef == nil {
                continue
            }
            let existingVO:TeamListingVO? = happen.happenedModel.teamListingVOs[ref!]
            if existingVO != nil {
                if existingVO!.haveData || Bool( existingVO!.havePartialData && !fullListingSeasonRequested ) {
                    continue
                }
            }
            if let vo:TeamListingVO = initializeTeamListingVO(ref!, seasonRef!, listingRef!, teamRef!) {

                vo.globalRank = globalRank
                vo.havePartialData = true
                vo.haveData = fullListingSeasonRequested
            }
        }
    }

    private func parseFixtures(fullListingSeasonRequested:Bool = false ) {
        let array:NSArray? = app.arrayHelper.nsArrFromNsDic(values, ServerConstant.FIXTURE_PARAM)
        if array == nil {
            return
        }
        parseFixtureEntries(array!, fullListingSeasonRequested:fullListingSeasonRequested)
    }

    private func parseFixtureEntries(_ array:NSArray, fullListingSeasonRequested:Bool = false ) {
        for entry in array {
            let dictionary: NSDictionary? = entry as? NSDictionary
            if dictionary == nil {
                continue
            }
            let ref:Int? = app.arrayHelper.intFromNsDic(dictionary!, ServerConstant.REF_PARAM)
            let seasonRef:Int? = app.arrayHelper.intFromNsDic(dictionary!, ServerConstant.SEASON_REF_PARAM)
            let listingRef:Int? = app.arrayHelper.intFromNsDic(dictionary!, ServerConstant.LISTING_REF_PARAM)
            let homeRef:Int? = app.arrayHelper.intFromNsDic(dictionary!, ServerConstant.HOME_REF_PARAM)
            let awayRef:Int? = app.arrayHelper.intFromNsDic(dictionary!, ServerConstant.AWAY_REF_PARAM)
            let playDate:Date? = app.arrayHelper.dateFromNsDicInterval(dictionary!, ServerConstant.PLAY_DATE_PARAM, requireDay1: true)
            var homeGoals:Int = app.arrayHelper.intFromNsDic(dictionary!, ServerConstant.HOME_GOALS_PARAM, defaultValue:0)!
            var awayGoals:Int = app.arrayHelper.intFromNsDic(dictionary!, ServerConstant.AWAY_GOALS_PARAM, defaultValue:0)!
            var played:Bool = app.arrayHelper.booleanFromNsDic(dictionary!, ServerConstant.PLAYED_PARAM, defaultValue:false)!
            let ignoreFixture:Bool = app.arrayHelper.booleanFromNsDic(dictionary!, ServerConstant.IGNORE_FIXTURE_PARAM, defaultValue:false)!

            if ref == nil {
                continue
            }
            if seasonRef == nil {
                continue
            }
            if listingRef == nil {
                continue
            }
            if homeRef == nil {
                continue
            }
            if awayRef == nil {
                continue
            }
            if ignoreFixture || Bool(playDate == nil) {
                played = false
            }
            if !played {
                homeGoals = 0
                awayGoals = 0
            }

            let existingFixtureVO:FixtureVO? = happen.happenedModel.fixtureVOs[ref!]
            if Bool( existingFixtureVO != nil ) && existingFixtureVO!.haveData {
                continue
            }
            if let vo:FixtureVO = initializeFixtureVO(ref!, seasonRef!, listingRef!, homeRef!, awayRef!, ignoreFixture) {
                vo.playDate = playDate
                vo.resultVO.homeGoals = homeGoals
                vo.resultVO.awayGoals = awayGoals
                vo.resultVO.played = played
                vo.haveData = fullListingSeasonRequested
            }
        }
    }

    private func parseAdjustTeams() {
        let array:NSArray? = app.arrayHelper.nsArrFromNsDic(values, ServerConstant.ADJUST_TEAM_PARAM)
        if array == nil {
            return
        }
        for entry in array! {
            let dictionary: NSDictionary? = entry as? NSDictionary
            if dictionary == nil {
                continue
            }
            let ref:Int? = app.arrayHelper.intFromNsDic(dictionary!, ServerConstant.REF_PARAM)
            let seasonRef:Int? = app.arrayHelper.intFromNsDic(dictionary!, ServerConstant.SEASON_REF_PARAM)
            let listingRef:Int? = app.arrayHelper.intFromNsDic(dictionary!, ServerConstant.LISTING_REF_PARAM)
            let teamRef:Int? = app.arrayHelper.intFromNsDic(dictionary!, ServerConstant.TEAM_REF_PARAM)
            let appliedDate:Date? = app.arrayHelper.dateFromNsDicInterval(dictionary!, ServerConstant.APPLIED_DATE_PARAM, requireDay1: true)
            let pointsOffset:Int = app.arrayHelper.intFromNsDic(dictionary!, ServerConstant.POINTS_OFFSET_PARAM, defaultValue:0)!
            let goalForOffset:Int = app.arrayHelper.intFromNsDic(dictionary!, ServerConstant.GOAL_FOR_OFFSET_PARAM, defaultValue:0)!
            let goalAgainstOffset:Int = app.arrayHelper.intFromNsDic(dictionary!, ServerConstant.GOAL_AGAINST_OFFSET_PARAM, defaultValue:0)!
            let awayGoalOffset:Int = app.arrayHelper.intFromNsDic(dictionary!, ServerConstant.AWAY_GOAL_OFFSET_PARAM, defaultValue:0)!
            let winOffset:Int = app.arrayHelper.intFromNsDic(dictionary!, ServerConstant.WIN_OFFSET_PARAM, defaultValue:0)!
            let drawOffset:Int = app.arrayHelper.intFromNsDic(dictionary!, ServerConstant.DRAW_OFFSET_PARAM, defaultValue:0)!
            let lossOffset:Int = app.arrayHelper.intFromNsDic(dictionary!, ServerConstant.LOSS_OFFSET_PARAM, defaultValue:0)!
            let awayWinOffset:Int = app.arrayHelper.intFromNsDic(dictionary!, ServerConstant.AWAY_WIN_OFFSET_PARAM, defaultValue:0)!
            let adjustAgainstTeamRef:Int? = app.arrayHelper.intFromNsDic(dictionary!, ServerConstant.ADJUST_AGAINST_TEAM_REF_PARAM)

            if ref == nil {
                continue
            }
            if seasonRef == nil {
                continue
            }
            if listingRef == nil {
                continue
            }
            if teamRef == nil {
                continue
            }

            if let vo:AdjustTeamVO = initializeAdjustTeamVO(ref!, seasonRef!, listingRef!, teamRef!) {
                vo.appliedDate = appliedDate
                vo.pointsOffset = pointsOffset
                vo.goalForOffset = goalForOffset
                vo.goalAgainstOffset = goalAgainstOffset
                vo.awayGoalOffset = awayGoalOffset
                vo.winOffset = winOffset
                vo.drawOffset = drawOffset
                vo.lossOffset = lossOffset
                vo.awayWinOffset = awayWinOffset
                vo.adjustAgainstTeamRef = adjustAgainstTeamRef
            }
        }
    }

    private func parsePlaceEvents() {
        let array:NSArray? = app.arrayHelper.nsArrFromNsDic(values, ServerConstant.PLACE_EVENT_PARAM)
        if array == nil {
            return
        }
        for entry in array! {
            let dictionary: NSDictionary? = entry as? NSDictionary
            if dictionary == nil {
                continue
            }
            let ref:Int? = app.arrayHelper.intFromNsDic(dictionary!, ServerConstant.REF_PARAM)
            let seasonRef:Int? = app.arrayHelper.intFromNsDic(dictionary!, ServerConstant.SEASON_REF_PARAM)
            let listingRef:Int? = app.arrayHelper.intFromNsDic(dictionary!, ServerConstant.LISTING_REF_PARAM)
            let eventRef:Int? = app.arrayHelper.intFromNsDic(dictionary!, ServerConstant.EVENT_REF_PARAM)
            let securedDate:Date? = app.arrayHelper.dateFromNsDicInterval(dictionary!, ServerConstant.SECURED_DATE_PARAM, requireDay1: true)
            let removedDate:Date? = app.arrayHelper.dateFromNsDicInterval(dictionary!, ServerConstant.REMOVED_DATE_PARAM, requireDay1: true)
            let placesForEvent:Int = app.arrayHelper.intFromNsDic(dictionary!, ServerConstant.PLACES_FOR_EVENT_PARAM, defaultValue: 0)!
            let permanentPlace:Bool = app.arrayHelper.booleanFromNsDic(dictionary!, ServerConstant.PERMANENT_PLACE_PARAM, defaultValue: false)!

            if ref == nil {
                continue
            }
            if seasonRef == nil {
                continue
            }
            if listingRef == nil {
                continue
            }
            if eventRef == nil {
                continue
            }
            if placesForEvent < 1 {
                continue
            }

            if let vo:PlaceEventVO = initializePlaceEventVO(ref!, seasonRef!, listingRef!, eventRef!) {
                vo.securedDate = securedDate
                vo.removedDate = removedDate
                vo.placesForEvent = placesForEvent
                vo.permanentPlace = permanentPlace
            }
        }
    }

    private func parseExemptionTeams() {
        let array:NSArray? = app.arrayHelper.nsArrFromNsDic(values, ServerConstant.EXEMPTION_TEAM_PARAM)
        if array == nil {
            return
        }
        for entry in array! {
            let dictionary: NSDictionary? = entry as? NSDictionary
            if dictionary == nil {
                continue
            }
            let ref: Int? = app.arrayHelper.intFromNsDic(dictionary!, ServerConstant.REF_PARAM)
            let seasonRef: Int? = app.arrayHelper.intFromNsDic(dictionary!, ServerConstant.SEASON_REF_PARAM)
            let listingRef: Int? = app.arrayHelper.intFromNsDic(dictionary!, ServerConstant.LISTING_REF_PARAM)
            let teamRef: Int? = app.arrayHelper.intFromNsDic(dictionary!, ServerConstant.TEAM_REF_PARAM)
            let eventRef:Int? = app.arrayHelper.intFromNsDic(dictionary!, ServerConstant.EVENT_REF_PARAM)
            let appliedDate:Date? = app.arrayHelper.dateFromNsDicInterval(dictionary!, ServerConstant.APPLIED_DATE_PARAM, requireDay1: true)
            let eventReceivedByListing:Bool = app.arrayHelper.booleanFromNsDic(dictionary!, ServerConstant.EVENT_RECEIVED_BY_LISTING, defaultValue: false)!

            if ref == nil {
                continue
            }
            if seasonRef == nil {
                continue
            }
            if listingRef == nil {
                continue
            }
            if teamRef == nil {
                continue
            }
            if eventRef == nil {
                continue
            }
            if appliedDate == nil {
                continue
            }

            if let vo:ExemptionTeamVO = initializeExemptionTeamVO(ref!, seasonRef!, listingRef!, teamRef!, eventRef!) {
                vo.appliedDate = appliedDate
                vo.eventReceivedByListing = eventReceivedByListing
            }
        }
    }

    private func applyListingSeasons() {
        for listingSeasonVO:ListingSeasonVO in happen.happenedModel.listingSeasonVOs.values {
            if !listingSeasonVO.haveData || listingSeasonVO.calculatorVO.haveData {
                continue
            }
            for teamListingRef:Int in listingSeasonVO.teamListingRefs.values{
                if let teamListingVO:TeamListingVO = happen.happenedModel.teamListingVOs[teamListingRef] {
                    listingSeasonVO.calculatorVO.teamListingVOs[teamListingVO.teamRef] = teamListingVO
                }
            }
            for fixtureRef:Int in listingSeasonVO.fixtureRefs.keys {
                if let fixtureVO:FixtureVO = happen.happenedModel.fixtureVOs[fixtureRef] {
                    listingSeasonVO.calculatorVO.fixtureVOs[fixtureVO.ref] = fixtureVO
                    if !fixtureVO.resultVO.ignoreFixture {
                        listingSeasonVO.calculatorVO.activeFixtureVOs[fixtureVO.ref] = fixtureVO
                    } else {
                        listingSeasonVO.calculatorVO.parkedFixtureVOs[fixtureVO.ref] = fixtureVO
                    }
                }
            }
            listingSeasonVO.calculatorVO.haveData = true
            happen.applyPerformanceCommand( calculatorVO: listingSeasonVO.calculatorVO ).command()
        }
    }

    private func parseEventScenarios() {
        let array:NSArray? = app.arrayHelper.nsArrFromNsDic(values, ServerConstant.EVENT_SCENARIO_PARAM)
        if array == nil {
            return
        }
        parseEventScenarioEntries(array!)
    }

    private func parseEventScenarioEntries( _ array:NSArray) {
        for entry in array {
            let dictionary: NSDictionary? = entry as? NSDictionary
            if dictionary == nil {
                continue
            }
            let ref: Int? = app.arrayHelper.intFromNsDic(dictionary!, ServerConstant.REF_PARAM)
            let seasonRef: Int? = app.arrayHelper.intFromNsDic(dictionary!, ServerConstant.SEASON_REF_PARAM)
            let listingRef: Int? = app.arrayHelper.intFromNsDic(dictionary!, ServerConstant.LISTING_REF_PARAM)
            let teamRef: Int? = app.arrayHelper.intFromNsDic(dictionary!, ServerConstant.TEAM_REF_PARAM)
            let eventRef:Int? = app.arrayHelper.intFromNsDic(dictionary!, ServerConstant.EVENT_REF_PARAM)

            let toHappen:Bool = app.arrayHelper.booleanFromNsDic( dictionary!, ServerConstant.TO_HAPPEN_PARAM, defaultValue:false )!

            let invalid:Bool = app.arrayHelper.booleanFromNsDic( dictionary!, ServerConstant.INVALID_PARAM, defaultValue:false )!
            let listStringResults:String? = app.arrayHelper.stringFromNsDic( dictionary!, ServerConstant.LIST_STRING_RESULTS_PARAM )

            let upcoming:Bool = app.arrayHelper.booleanFromNsDic( dictionary!, ServerConstant.UPCOMING_IN_PARAM, defaultValue:false )!
            let fromResult:Bool = app.arrayHelper.booleanFromNsDic( dictionary!, ServerConstant.FROM_RESULT_PARAM, defaultValue:false )!
            let occurredDate:Date? = app.arrayHelper.dateFromNsDicInterval(dictionary!, ServerConstant.OCCURRED_DATE_PARAM, requireDay1: true)
            let occurredFixtureRef:Int? = app.arrayHelper.intFromNsDic( dictionary!, ServerConstant.OCCURRED_FIXTURE_REF_PARAM )

            if ref == nil || seasonRef == nil || listingRef == nil || teamRef == nil || eventRef == nil {
                continue
            }
            if happen.happenedModel.getTeamListingVO(seasonRef!, listingRef!, teamRef!) == nil {
                continue
            }

            let existingVO:EventScenarioVO? = happen.happenedModel.eventScenarioVOs[ ref! ]
            if existingVO != nil && existingVO!.haveData {
                continue
            }

            let resultVOs:[Int:ResultVO] = listStringResults != nil ? parseResultsString( listStringResults! ) : [:]
            let listingSeasonVO:ListingSeasonVO = happen.happenedModel.getListingSeasonVO(seasonRef!, listingRef!)!
            let calculatorVO:CalculatorVO = happen.happenedHelper.cloneCalculatorVO( listingSeasonVO.calculatorVO, resultVOs:resultVOs )

            let vo:EventScenarioVO = EventScenarioVO( ref!, seasonRef!, listingRef!, teamRef!, eventRef!, toHappen, finalCalculatorVO:calculatorVO )

            vo.haveData = listStringResults != nil
            vo.invalid = invalid

            vo.upcoming = upcoming
            vo.fromResult = fromResult
            vo.occurredDate = occurredDate
            vo.occurredFixtureRef = occurredFixtureRef

            addEventScenarioVO( vo )
        }
    }

    private func parseListingScenarios() {

        let array:NSArray? = app.arrayHelper.nsArrFromNsDic(values, ServerConstant.LISTING_SCENARIO_PARAM)
        if array == nil {
            return
        }
        for entry in array! {
            let dictionary: NSDictionary? = entry as? NSDictionary
            if dictionary == nil {
                continue
            }
            let ref:Int? = app.arrayHelper.intFromNsDic( dictionary!, ServerConstant.REF_PARAM )
            let seasonRef:Int? = app.arrayHelper.intFromNsDic( dictionary!, ServerConstant.SEASON_REF_PARAM )
            let listingRef:Int? = app.arrayHelper.intFromNsDic( dictionary!, ServerConstant.LISTING_REF_PARAM )
            let teamRef:Int? = app.arrayHelper.intFromNsDic( dictionary!, ServerConstant.TEAM_REF_PARAM )

            let best:Bool = app.arrayHelper.booleanFromNsDic( dictionary!, ServerConstant.BEST_PARAM, defaultValue:false )!

            let position:Int? = app.arrayHelper.intFromNsDic( dictionary!, ServerConstant.POSITION_PARAM )

            let invalid:Bool = app.arrayHelper.booleanFromNsDic( dictionary!, ServerConstant.INVALID_PARAM, defaultValue:false )! || position == nil
            let listStringResults:String? = app.arrayHelper.stringFromNsDic( dictionary!, ServerConstant.LIST_STRING_RESULTS_PARAM )


            if ref == nil || seasonRef == nil || listingRef == nil || teamRef == nil {
                continue
            }
            let teamListingVO:TeamListingVO? = happen.happenedModel.getTeamListingVO(seasonRef!, listingRef!, teamRef!)
            if teamListingVO == nil {
                continue
            }

            let resultVOs:[Int:ResultVO] = listStringResults != nil ? parseResultsString( listStringResults! ) : [:]
            let listingSeasonVO:ListingSeasonVO = happen.happenedModel.getListingSeasonVO(seasonRef!, listingRef!)!
            let calculatorVO:CalculatorVO = happen.happenedHelper.cloneCalculatorVO( listingSeasonVO.calculatorVO, resultVOs:resultVOs )

            let vo = ListingScenarioVO( ref!, seasonRef!, listingRef!, teamRef!, best, finalCalculatorVO:calculatorVO )

            vo.invalid = invalid
            vo.position = position

            addListingScenarioVO( vo )
        }
    }

    private func updateEventFeed(feedSource:FeedSource, seasonGroupRef:Int, filterVO: EventFeedFilterVO?=nil ) {

        let dictionary:NSDictionary? = app.arrayHelper.nsDicFromNsDic(values, ServerConstant.EVENT_SCENARIO_FEED_PARAM)
        if dictionary == nil {
            return
        }
        let feedEntries:NSArray = app.arrayHelper.nsArrFromNsDic(dictionary!, ServerConstant.FEED_ENTRIES_PARAM, defaultValue: [])!
        let endOfList:Bool = app.arrayHelper.booleanFromNsDic( dictionary!, ServerConstant.END_OF_LIST_PARAM, defaultValue:false )!
        var isHappenedState:IsHappenedState = serverRequestVO.isHappenedState
        let isAltIfEmpty:Bool = serverRequestVO.isAltIfEmpty
        let amountAvailable:Int? = app.arrayHelper.intFromNsDic( dictionary!, ServerConstant.NO_OF_AVAILABLE_ENTRIES_PARAM )
        var altUsed:Bool = false
        if isHappenedState != .both {
            if feedEntries.count > 0 {
                let feedEntry:NSDictionary = feedEntries[0] as! NSDictionary
                let thisUpcoming:Bool = app.arrayHelper.booleanFromNsDic( feedEntry, ServerConstant.UPCOMING_IN_PARAM, defaultValue:false )!
                if (isHappenedState == .upcomingOnly) != thisUpcoming {
                    altUsed = true
                    isHappenedState = isHappenedState == .upcomingOnly ? .happenedOnly : .upcomingOnly
                }
            } else {
                if !isAltIfEmpty {

                    let feedViewVO:FeedViewVO = viewed.feedViewHelper.getFeedVO( feedType: .event, feedSource:feedSource, seasonGroupRef: seasonGroupRef, isHappenedState: isHappenedState, filterVO: filterVO, createIfNotMatched:true)!
                    feedViewVO.endOfList = true
                    if amountAvailable != nil {
                        feedViewVO.amountAvailable = amountAvailable
                    }
                    return
                }
                altUsed = true
                isHappenedState = isHappenedState == .upcomingOnly ? .happenedOnly : .upcomingOnly
            }
        }

        if altUsed {
            //No entries found for e.g. happened
            let thisIsHappenedState:IsHappenedState = isHappenedState == .upcomingOnly ? .happenedOnly : .upcomingOnly
            let feedViewVO:FeedViewVO = viewed.feedViewHelper.getFeedVO( feedType: .event, feedSource:feedSource, seasonGroupRef: seasonGroupRef, isHappenedState: thisIsHappenedState, filterVO: filterVO, createIfNotMatched:true)!
            feedViewVO.endOfList = true
        }
        let feedViewVO:FeedViewVO = viewed.feedViewHelper.getFeedVO( feedType: .event, feedSource:feedSource, seasonGroupRef: seasonGroupRef, isHappenedState: isHappenedState, filterVO: filterVO, createIfNotMatched:true)!

        parseEventScenarioEntries( feedEntries )

        addToFeed(feedViewVO: feedViewVO, feedEntries: feedEntries, endOfList: endOfList, amountAvailable: amountAvailable)
    }

    private func updateFixtureFeed(feedSource:FeedSource, seasonGroupRef:Int, filterVO: FeedFilterVO?=nil ) {

        let dictionary:NSDictionary? = app.arrayHelper.nsDicFromNsDic(values, ServerConstant.FIXTURE_FEED_PARAM)
        if dictionary == nil {
            return
        }
        let feedEntries:NSArray = app.arrayHelper.nsArrFromNsDic(dictionary!, ServerConstant.FEED_ENTRIES_PARAM, defaultValue: [])!
        let endOfList:Bool = app.arrayHelper.booleanFromNsDic( dictionary!, ServerConstant.END_OF_LIST_PARAM, defaultValue:false )!
        let amountAvailable:Int? = app.arrayHelper.intFromNsDic( dictionary!, ServerConstant.NO_OF_AVAILABLE_ENTRIES_PARAM )

        let isHappenedState:IsHappenedState = serverRequestVO.isHappenedState

        let feedViewVO:FeedViewVO = viewed.feedViewHelper.getFeedVO( feedType: .fixture, feedSource:feedSource, seasonGroupRef: seasonGroupRef, isHappenedState: isHappenedState, filterVO: filterVO, createIfNotMatched:true)!
        parseFixtureEntries(feedEntries)

        addToFeed(feedViewVO: feedViewVO, feedEntries: feedEntries, endOfList: endOfList, amountAvailable: amountAvailable)
    }

    private func addToFeed( feedViewVO:FeedViewVO, feedEntries:NSArray, endOfList:Bool, amountAvailable:Int? ) {

        var thisEndOfList:Bool = endOfList

        let startIndex:Int = serverRequestVO.entriesFrom != nil ? serverRequestVO.entriesFrom! : 0
        let entriesSpan:Int = serverRequestVO.entriesSpan != nil ? serverRequestVO.entriesSpan! : feedEntries.count

        if entriesSpan >= 1 {
            for i in (0...(entriesSpan - 1)) {
                let insertAt:Int = startIndex + i
                if amountAvailable != nil && insertAt >= amountAvailable! - 1 {
                    thisEndOfList = true
                    if insertAt >= amountAvailable! {
                        break
                    }
                }
                var ref:Int? = nil
                var invalid:Bool = false
                if i < feedEntries.count {
                    if let feedEntry:NSDictionary = feedEntries[i] as? NSDictionary {
                        ref = app.arrayHelper.intFromNsDic(feedEntry, ServerConstant.REF_PARAM )
                    }
                    if ref == nil {
                        invalid = true
                    }
                } else if thisEndOfList {
                    break
                } else {
                    invalid = true
                }
                while( feedViewVO.entryVOs.count < insertAt - 1 ){
                    feedViewVO.entryVOs.append(FeedEntryVO(feedViewVO.entryVOs.count, requested: false))
                }
                let vo:FeedEntryVO = FeedEntryVO(insertAt, ref: ref, requested: true, invalid: invalid)
                if feedViewVO.entryVOs.count > vo.index {
                    feedViewVO.entryVOs[vo.index] = vo
                } else {
                    feedViewVO.entryVOs.append(vo)
                }
            }
        }

        feedViewVO.endOfList = thisEndOfList
        if amountAvailable != nil {
            feedViewVO.amountAvailable = amountAvailable
        }
    }

    private func addEventScenarioVO ( _ vo:EventScenarioVO ) {
        happen.happenedModel.eventScenarioVOs[ vo.ref ] = vo
        if let eventTeamVO:EventTeamVO = happen.happenedModel.getEventTeamVO( vo.seasonRef, vo.listingRef, vo.teamRef, vo.eventRef ) {
            if vo.toHappen{
                eventTeamVO.scenarioToHappenRef = vo.ref
            } else {
                eventTeamVO.scenarioNotHappenRef = vo.ref
            }
        }
    }

    private func addListingScenarioVO ( _ vo:ListingScenarioVO ) {
        happen.happenedModel.listingScenarioVOs[ vo.ref ] = vo
        if let teamListingVO:TeamListingVO = happen.happenedModel.getTeamListingVO( vo.seasonRef, vo.listingRef, vo.teamRef ) {
            if vo.best{
                teamListingVO.bestListingScenarioRef = vo.ref
            } else {
                teamListingVO.worstListingScenarioRef = vo.ref
            }
        }
    }

    private func markTeamListings() {
        if let happenedRequestVOs:[HappenedServerRequestVO] = serverRequestVO.allHappenedRequestVOs {
            for happenedRequestVO: HappenedServerRequestVO in happenedRequestVOs {
                if happenedRequestVO.seasonRefs != nil {
                    for seasonRef:Int in happenedRequestVO.seasonRefs!.keys {
                        if happenedRequestVO.listingRefs != nil {
                            for listingRef:Int in happenedRequestVO.listingRefs!.keys {
                                if happenedRequestVO.teamRefs != nil {
                                    for teamRef:Int in happenedRequestVO.teamRefs!.keys {
                                        if let teamListingVO:TeamListingVO = happen.happenedModel.getTeamListingVO(seasonRef, listingRef, teamRef) {
                                            teamListingVO.haveData = true
                                            teamListingVO.haveScenario = true
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                if happenedRequestVO.listingScenarioRefs != nil {
                    for listingScenarioRef:Int in happenedRequestVO.listingScenarioRefs!.keys {
                        if let listingScenarioVO:ListingScenarioVO = happen.happenedModel.listingScenarioVOs[listingScenarioRef] {
                            if let teamListingVO:TeamListingVO = happen.happenedModel.getTeamListingVO(listingScenarioVO.seasonRef, listingScenarioVO.listingRef, listingScenarioVO.teamRef) {
                                teamListingVO.haveData = true
                                teamListingVO.haveScenario = true
                            }
                        }
                    }
                }
                if happenedRequestVO.eventScenarioRefs != nil {
                    for eventScenarioRef:Int in happenedRequestVO.eventScenarioRefs!.keys {
                        if let eventScenarioVO:EventScenarioVO = happen.happenedModel.eventScenarioVOs[eventScenarioRef] {
                            if let teamListingVO:TeamListingVO = happen.happenedModel.getTeamListingVO(eventScenarioVO.seasonRef, eventScenarioVO.listingRef, eventScenarioVO.teamRef) {
                                teamListingVO.haveData = true
                                teamListingVO.haveScenario = true
                            }
                        }
                    }
                }
                if happenedRequestVO.teamListingRefs != nil {
                    for teamListingRef:Int in happenedRequestVO.teamListingRefs!.keys {
                        if let teamListingVO:TeamListingVO = happen.happenedModel.teamListingVOs[teamListingRef] {
                            teamListingVO.haveData = true
                            teamListingVO.haveScenario = true
                        }
                    }
                }
            }
        }

    }

    private func initializeSeasonGroupVO(_ ref:Int) -> SeasonGroupVO? {
        var vo:SeasonGroupVO? = happen.happenedModel.seasonGroupVOs[ref]
        if vo != nil {
            return vo
        }
        let balancingVO:SeasonGroupBalancingVO? = balancing.balancingModel.seasonGroupVOs[ref]
        if balancingVO == nil {
            return nil
        }
        vo = SeasonGroupVO(ref)
        happen.happenedModel.seasonGroupVOs[ref] = vo
        return vo
    }

    private func initializeSeasonVO(_ ref:Int) -> SeasonVO? {
        var vo:SeasonVO? = happen.happenedModel.seasonVOs[ref]
        if vo != nil {
            return vo
        }
        let balancingVO:SeasonBalancingVO? = balancing.balancingModel.seasonVOs[ref]
        if balancingVO == nil {
            return nil
        }

        vo = SeasonVO(ref)

        for seasonGroupRef:Int in balancingVO!.seasonGroupRefs!.keys {
            let seasonGroupVO:SeasonGroupVO? = initializeSeasonGroupVO(seasonGroupRef)
            if seasonGroupVO == nil {
                return nil
            }
        }
        happen.happenedModel.seasonVOs[vo!.ref] = vo

        return vo
    }

    private func initializeCompetitionVO(_ ref:Int, seasonGroupRef:Int?=nil, seasonRef:Int?=nil) -> CompetitionVO? {

        var vo:CompetitionVO? = happen.happenedModel.getCompetitionVO(ref, seasonGroupRef: seasonGroupRef)

        if vo != nil {
            return vo
        }
        if seasonGroupRef != nil {
            if seasonRef == nil {
                return nil
            }
            let seasonVO:SeasonVO? = initializeSeasonVO(seasonRef!)
            if seasonVO == nil {
                return nil
            }
            let seasonBalancingVO:SeasonBalancingVO = balancing.balancingModel.seasonVOs[seasonRef!]!
            if seasonBalancingVO.seasonGroupRefs![seasonGroupRef!] == nil {
                return nil
            }
        }

        let balancingVO:CompetitionBalancingVO? = balancing.balancingModel.getCompetitionVO(ref, seasonRef: seasonRef)

        if balancingVO == nil {
            return nil
        }

        let balancingCloneVO:CompetitionBalancingVO = balancingVO!.birthCompetitionClone( balancingVO!.ref, seasonRef: balancingVO!.seasonRef )
        balancingCloneVO.onHiatus = balancingVO!.onHiatus

        vo = CompetitionVO(ref, balancingVO: balancingCloneVO, seasonGroupRef: seasonGroupRef)
        if vo!.seasonGroupRef == nil {
            vo!.seasonGroupVOs = [:]
            happen.happenedModel.competitionVOs[vo!.ref] = vo
            return vo
        }
        if vo!.balancingVO.onHiatus {
            return nil
        }
        let rootVO:CompetitionVO? = initializeCompetitionVO(ref)
        if rootVO == nil {
            return nil
        }

        if vo!.balancingVO.parentCompetitionRef != nil {
            let parentVO:CompetitionVO? = initializeCompetitionVO(vo!.balancingVO.parentCompetitionRef!, seasonGroupRef: seasonGroupRef, seasonRef:seasonRef)
            if parentVO == nil {
                return nil
            }
            parentVO!.childCompetitionRefs[vo!.ref] = NSNull()
            if Bool( parentVO!.balancingVO.competitionType == BalancingConstant.FOCAL_COMPETITION_TYPE ) || Bool( parentVO!.balancingVO.competitionType == BalancingConstant.SUB_COMPETITION_TYPE ) {
                vo!.balancingVO.competitionType = BalancingConstant.SUB_COMPETITION_TYPE
            } else if vo!.balancingVO.competitionType != BalancingConstant.FOCAL_COMPETITION_TYPE {
                vo!.balancingVO.competitionType = BalancingConstant.HIERARCHY_COMPETITION_TYPE
            }
            balancing.parseBalancingHelper.cloneCompetitionToNil(vo!.balancingVO, parentVO!.balancingVO)
        } else {
            let seasonGroupVO:SeasonGroupVO = happen.happenedModel.seasonGroupVOs[seasonGroupRef!]!
            seasonGroupVO.rootCompetitionRefs[vo!.ref] = NSNull()
        }

        balancing.parseBalancingHelper.applyCompetitionDefault(vo!.balancingVO)

        rootVO!.seasonGroupVOs![vo!.seasonGroupRef!] = vo!

        return vo
    }


    private func initializeListingVO(_ ref:Int) -> ListingVO? {
        var vo:ListingVO? = happen.happenedModel.listingVOs[ref]
        if vo != nil {
            return vo
        }
        let balancingVO:ListingBalancingVO? = balancing.balancingModel.getListingVO(ref)
        if balancingVO == nil {
            return nil
        }
        vo = ListingVO(ref)
        happen.happenedModel.listingVOs[vo!.ref] = vo

        return vo
    }

    private func initializeTeamVO(_ ref:Int) -> TeamVO? {
        var vo:TeamVO? = happen.happenedModel.teamVOs[ref]
        if vo != nil {
            return vo
        }
        let balancingVO:TeamBalancingVO? = balancing.balancingModel.getTeamVO(ref)
        if balancingVO == nil {
            return nil
        }
        vo = TeamVO(ref)
        happen.happenedModel.teamVOs[vo!.ref] = vo
        return vo
    }

    private func initializeListingSeasonVO(_ ref:Int, _ seasonRef:Int, _ listingRef:Int) -> ListingSeasonVO? {
        var vo:ListingSeasonVO? = happen.happenedModel.listingSeasonVOs[ref]
        if vo != nil {
            if vo!.seasonRef != seasonRef {
                return nil
            }
            if vo!.listingRef != listingRef {
                return nil
            }
            return vo
        }

        vo = happen.happenedModel.getListingSeasonVO(seasonRef, listingRef)
        if vo != nil {
            return nil
        }

        let seasonVO:SeasonVO? = initializeSeasonVO(seasonRef)
        let listingVO:ListingVO? = initializeListingVO(listingRef)

        if Bool(seasonVO == nil) || Bool(listingVO == nil) {
            return nil
        }
        let seasonBalancingVO:SeasonBalancingVO = balancing.balancingModel.seasonVOs[seasonRef]!
        let listingBalancingVO:ListingBalancingVO? = balancing.balancingModel.getListingVO(listingRef, seasonRef: seasonRef)

        if listingBalancingVO == nil {
            return nil
        }
        vo = ListingSeasonVO(ref, seasonRef, listingRef)
        vo!.havePartialData = true

        let orderedSeasonGroupRefs:[Int] = balancing.balancingHelper.getOrderedSeasonGroupRefs(seasonBalancingVO.seasonGroupRefs!)
        var addedToCompetition:Bool = false
        for seasonGroupRef:Int in orderedSeasonGroupRefs {
            let competitionVO:CompetitionVO? = initializeCompetitionVO(listingBalancingVO!.parentCompetitionRef!, seasonGroupRef: seasonGroupRef, seasonRef: vo!.seasonRef)
            if  competitionVO == nil{
                continue
            }
            addedToCompetition = true
            competitionVO!.childListingSeasonRefs[vo!.ref] = NSNull()
        }
        if !addedToCompetition {
            return nil
        }

        seasonVO!.listingSeasonRefs[vo!.listingRef] = vo!.ref
        listingVO!.listingSeasonRefs[vo!.seasonRef] = vo!.ref

        happen.happenedModel.listingSeasonVOs[vo!.ref] = vo

        return vo
    }

    private func initializeTeamListingVO(_ ref:Int, _ seasonRef:Int, _ listingRef:Int, _ teamRef:Int) -> TeamListingVO? {
        var vo:TeamListingVO? = happen.happenedModel.teamListingVOs[ref]
        let listingSeasonVO:ListingSeasonVO? = happen.happenedModel.getListingSeasonVO(seasonRef, listingRef)
        if vo != nil {
            if vo!.seasonRef != seasonRef {
                return nil
            }
            if vo!.listingRef != listingRef {
                return nil
            }
            if vo!.teamRef != teamRef {
                return nil
            }
            listingSeasonVO!.teamListingRefs[vo!.teamRef] = vo!.ref
            return vo
        }

        vo = happen.happenedModel.getTeamListingVO(seasonRef, listingRef, teamRef)
        if vo != nil {
            return nil
        }

        let teamSeasonVO:TeamSeasonVO? = initializeTeamSeasonVO(seasonRef, teamRef)
        if teamSeasonVO == nil {
            return nil
        }
        vo = TeamListingVO(ref, seasonRef, listingRef, teamRef)
        if listingSeasonVO != nil {
            listingSeasonVO!.teamListingRefs[vo!.teamRef] = vo!.ref
        }

        teamSeasonVO!.teamListingRefs[vo!.listingRef] = vo!.ref

        happen.happenedModel.teamListingVOs[vo!.ref] = vo

        return vo
    }

    private func initializeFixtureVO(_ ref:Int, _ seasonRef:Int, _ listingRef:Int, _ homeRef:Int, _ awayRef:Int, _ ignoreFixture:Bool) -> FixtureVO? {
        var vo:FixtureVO? = happen.happenedModel.fixtureVOs[ref]
        if vo != nil {
            if vo!.seasonRef != seasonRef {
                return nil
            }
            if vo!.listingRef != listingRef {
                return nil
            }
            if vo!.homeRef != homeRef {
                return nil
            }
            if vo!.awayRef != awayRef {
                return nil
            }
            if vo!.resultVO.ignoreFixture != ignoreFixture {
                return nil
            }
            return vo
        }
        let homeTeamListingVO:TeamListingVO? = happen.happenedModel.getTeamListingVO(seasonRef, listingRef, homeRef)
        if homeTeamListingVO == nil {
            return nil
        }
        let awayTeamListingVO:TeamListingVO? = happen.happenedModel.getTeamListingVO(seasonRef, listingRef, awayRef)
        if awayTeamListingVO == nil {
            return nil
        }
        vo = FixtureVO(ref, seasonRef, listingRef, homeRef, awayRef)
        vo!.resultVO.ignoreFixture = ignoreFixture

        let listingSeasonVO:ListingSeasonVO = happen.happenedModel.getListingSeasonVO(seasonRef, listingRef)!

        listingSeasonVO.fixtureRefs[vo!.ref] = NSNull()
        happen.happenedModel.fixtureVOs[vo!.ref] = vo
        if !vo!.resultVO.ignoreFixture {
            happen.happenedModel.activeFixtureVOs[vo!.ref] = vo
        } else {
            happen.happenedModel.parkedFixtureVOs[vo!.ref] = vo
        }

        return vo
    }

    private func initializeAdjustTeamVO(_ ref:Int, _ seasonRef:Int, _ listingRef:Int, _ teamRef:Int) -> AdjustTeamVO? {
        var vo:AdjustTeamVO? = happen.happenedModel.adjustTeamVOs[ref]
        if vo != nil {
            if vo!.seasonRef != seasonRef {
                return nil
            }
            if vo!.listingRef != listingRef {
                return nil
            }
            if vo!.teamRef != teamRef {
                return nil
            }
            return vo
        }
        let teamListingVO:TeamListingVO? = happen.happenedModel.getTeamListingVO(seasonRef, listingRef, teamRef)
        if teamListingVO == nil {
            return nil
        }

        vo = AdjustTeamVO(ref, seasonRef, listingRef, teamRef)

        teamListingVO!.adjustTeamRefs[vo!.ref] = NSNull()
        happen.happenedModel.adjustTeamVOs[vo!.ref] = vo

        return vo
    }

    private func initializePlaceEventVO(_ ref:Int, _ seasonRef:Int, _ listingRef:Int, _ eventRef:Int) -> PlaceEventVO? {
        var vo:PlaceEventVO? = happen.happenedModel.placeEventVOs[ref]
        if vo != nil {
            if vo!.seasonRef != seasonRef {
                return nil
            }
            if vo!.listingRef != listingRef {
                return nil
            }
            if vo!.eventRef != eventRef {
                return nil
            }
            return vo
        }
        let eventListingVO:EventListingVO? = initializeEventListingVO(seasonRef, listingRef, eventRef)
        if eventListingVO == nil {
            return nil
        }

        vo = PlaceEventVO(ref, seasonRef, listingRef, eventRef)

        eventListingVO!.placeEventRefs[vo!.ref] = NSNull()

        happen.happenedModel.placeEventVOs[vo!.ref] = vo

        return vo
    }

    private func initializeExemptionTeamVO(_ ref:Int, _ seasonRef:Int, _ listingRef:Int, _ teamRef:Int, _ eventRef:Int) -> ExemptionTeamVO? {
        var vo:ExemptionTeamVO? = happen.happenedModel.exemptionTeamVOs[ref]
        if vo != nil {
            if vo!.seasonRef != seasonRef {
                return nil
            }
            if vo!.listingRef != listingRef {
                return nil
            }
            if vo!.teamRef != teamRef {
                return nil
            }
            if vo!.eventRef != eventRef {
                return nil
            }
            return vo
        }

        let eventTeamVO:EventTeamVO? = initializeEventTeamVO(seasonRef, listingRef, teamRef, eventRef)
        if eventTeamVO == nil {
            return nil
        }

        vo = ExemptionTeamVO(ref, seasonRef, listingRef, teamRef, eventRef)

        eventTeamVO!.exemptionRef = vo!.ref

        happen.happenedModel.exemptionTeamVOs[vo!.ref] = vo

        return vo
    }

    private func initializeListingScenarioVO(_ ref:Int, _ seasonRef:Int, _ listingRef:Int, _ teamRef:Int, _ best:Bool, teamListingRef:Int?=nil) -> ListingScenarioVO? {
        var vo:ListingScenarioVO? = happen.happenedModel.listingScenarioVOs[ref]
        if vo != nil {
            if vo!.seasonRef != seasonRef {
                return nil
            }
            if vo!.listingRef != listingRef {
                return nil
            }
            if vo!.teamRef != teamRef {
                return nil
            }
            if vo!.best != best {
                return nil
            }
            return vo
        }

        vo = happen.happenedModel.getListingScenarioVO(seasonRef, listingRef, teamRef, best)
        if vo != nil {
            return nil
        }

        var teamListingVO:TeamListingVO? = happen.happenedModel.getTeamListingVO(seasonRef, listingRef, teamRef)
        if teamListingVO == nil {
            if teamListingRef != nil {
                teamListingVO = happen.happenedModel.teamListingVOs[teamListingRef!]
            }
            if teamListingVO == nil {
                return nil
            }
        }
        let listingSeasonVO:ListingSeasonVO = happen.happenedModel.getListingSeasonVO(seasonRef, listingRef)!
        let calculatorVO:CalculatorVO = happen.happenedHelper.cloneCalculatorVO( listingSeasonVO.calculatorVO )

        vo = ListingScenarioVO(ref, seasonRef, listingRef, teamRef, best, finalCalculatorVO:calculatorVO)

        if best {
            teamListingVO!.bestListingScenarioRef = vo!.ref
        } else {
            teamListingVO!.worstListingScenarioRef = vo!.ref
        }

        happen.happenedModel.listingScenarioVOs[vo!.ref] = vo
        return vo
    }

    private func initializeEventScenarioVO(_ ref:Int, _ seasonRef:Int, _ listingRef:Int, _ teamRef:Int, _ eventRef:Int, _ toHappen:Bool, teamListingRef:Int?=nil) -> EventScenarioVO? {

        var vo:EventScenarioVO? = happen.happenedModel.eventScenarioVOs[ref]
        let eventTeamVO:EventTeamVO? = initializeEventTeamVO(seasonRef, listingRef, teamRef, eventRef, teamListingRef:teamListingRef)
        if vo != nil {
            if vo!.seasonRef != seasonRef {
                return nil
            }
            if vo!.listingRef != listingRef {
                return nil
            }
            if vo!.teamRef != teamRef {
                return nil
            }
            if vo!.eventRef != eventRef {
                return nil
            }
            if vo!.toHappen != toHappen {
                return nil
            }
        } else {

            vo = happen.happenedModel.getEventScenarioVO(seasonRef, listingRef, teamRef, eventRef, toHappen)
            if vo != nil {
                return nil
            }

            if eventTeamVO == nil {
                return nil
            }
            let listingSeasonVO:ListingSeasonVO = happen.happenedModel.getListingSeasonVO(seasonRef, listingRef)!
            let calculatorVO:CalculatorVO = happen.happenedHelper.cloneCalculatorVO( listingSeasonVO.calculatorVO )

            vo = EventScenarioVO(ref, seasonRef, listingRef, teamRef, eventRef, toHappen, finalCalculatorVO:calculatorVO)

            happen.happenedModel.eventScenarioVOs[vo!.ref] = vo
        }

        if eventTeamVO != nil {
            if toHappen {
                eventTeamVO!.scenarioToHappenRef = vo!.ref
            } else {
                eventTeamVO!.scenarioNotHappenRef = vo!.ref
            }
        }

        return vo
    }

    private func initializeTeamSeasonVO(_ seasonRef:Int, _ teamRef:Int) -> TeamSeasonVO? {
        var vo:TeamSeasonVO? = happen.happenedModel.getTeamSeasonVO(teamRef, seasonRef)
        if vo != nil {
            return vo
        }
        let seasonVO:SeasonVO? = initializeSeasonVO(seasonRef)
        if seasonVO == nil {
            return nil
        }
        let teamVO:TeamVO? = initializeTeamVO(teamRef)
        if teamVO == nil {
            return nil
        }
        vo = TeamSeasonVO(seasonRef, teamRef)
        teamVO!.teamSeasonVOs[seasonRef] = vo
        return vo
    }

    private func initializeEventListingVO(_ seasonRef:Int, _ listingRef:Int, _ eventRef:Int) -> EventListingVO? {
        let listingSeasonVO:ListingSeasonVO? = happen.happenedModel.getListingSeasonVO(seasonRef, listingRef)
        if listingSeasonVO == nil {
            return nil
        }
        var vo:EventListingVO? = happen.happenedModel.getEventListingVO(seasonRef, listingRef, eventRef)
        if vo != nil {
            for teamRef:Int in listingSeasonVO!.teamListingRefs.keys {
                _ = initializeEventTeamVO(vo!.seasonRef, vo!.listingRef, teamRef, vo!.eventRef, eventListingConfirmed: true)
            }
            return vo
        }
        vo = EventListingVO(seasonRef, listingRef, eventRef)

        listingSeasonVO!.eventListingVOs[eventRef] = vo

        for teamRef:Int in listingSeasonVO!.teamListingRefs.keys {
            _ = initializeEventTeamVO(vo!.seasonRef, vo!.listingRef, teamRef, vo!.eventRef)
        }

        return vo
    }

    private func initializeEventTeamVO(_ seasonRef:Int, _ listingRef:Int, _ teamRef:Int, _ eventRef:Int, eventListingConfirmed:Bool=false, teamListingRef:Int?=nil) -> EventTeamVO? {
        var vo:EventTeamVO? = happen.happenedModel.getEventTeamVO(seasonRef, listingRef, teamRef, eventRef)
        if vo != nil {
            return vo
        }
        var teamListingVO:TeamListingVO? = happen.happenedModel.getTeamListingVO(seasonRef, listingRef, teamRef)
        if teamListingVO == nil {
            if teamListingRef != nil {
                teamListingVO = happen.happenedModel.teamListingVOs[teamListingRef!]
            }
            if teamListingVO == nil {
                return nil
            }
        }
        if !eventListingConfirmed {
            let eventListingVO:EventListingVO? = initializeEventListingVO(seasonRef, listingRef, eventRef)
            if eventListingVO == nil {
                return nil
            }
        }
        vo = EventTeamVO(seasonRef, listingRef, teamRef, eventRef)
        teamListingVO!.eventTeamVOs[eventRef] = vo
        return vo
    }

    private func parseResultsString( _ resultsString:String ) -> [Int:ResultVO] {
        
        var resultVOs:[Int:ResultVO] = [:]
    
        let fixtureStrings:[String] = resultsString.split(".")
    
        for fixtureString in  fixtureStrings {
            let fixtureArray:[String] = fixtureString.split( ":" )
            if fixtureArray.count < 5 {
                continue
            }
        
            if let ref:Int = Int( fixtureArray[ 0 ] ) {

                let resultVO:ResultVO = ResultVO( ref )

                let ignoreFixture = fixtureArray[ 1 ].bool
                let played = fixtureArray[ 2 ].bool
                let homeGoals = fixtureArray[ 3 ].int
                let awayGoals = fixtureArray[ 4 ].int

                resultVO.ignoreFixture = ignoreFixture
                resultVO.played = played
                resultVO.homeGoals = homeGoals
                resultVO.awayGoals = awayGoals

                resultVOs[ resultVO.fixtureRef ] = resultVO
            }
        }
        
        return resultVOs
    }

}
