import Foundation

class CompleteServerCommand {

    private var app:App { return App.instance }
    private var server:Server { return Server.instance }

    func command() {
        if server.serverModel.queueVOs.count != 0 {
            return
        }
        if app.appModel.initializing {
            app.updateInitializingAppCommand().command()
            return
        }
        if app.appModel.waitForServer {
            app.updateActivityServerAppCommand().command( false )
        }
    }
}
