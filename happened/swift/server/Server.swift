import Foundation

class Server {

    static let instance:Server = Server()

    let serverHelper:ServerHelper = ServerHelper();

    let serverModel:ServerModel = ServerModel();

    func completeServerCommand() -> CompleteServerCommand {
        return CompleteServerCommand();
    }

    func parseServerCommand( values:NSDictionary, serverRequestVO:ServerRequestVO ) -> ParseServerCommand {
        return ParseServerCommand( values:values, serverRequestVO:serverRequestVO );
    }

    func requestServerCommand() -> RequestServerCommand {
        return RequestServerCommand();
    }

    func balancingServerCommand() -> BalancingServerCommand {
        return BalancingServerCommand();
    }

    func localizationServerCommand() -> LocalizationServerCommand {
        return LocalizationServerCommand();
    }

    func baseServerCommand() -> BaseServerCommand {
        return BaseServerCommand();
    }

    func eventFeedServerCommand() -> EventFeedServerCommand {
        return EventFeedServerCommand();
    }

    func fixtureFeedServerCommand() -> FixtureFeedServerCommand {
        return FixtureFeedServerCommand();
    }

    func listingSeasonServerCommand() -> ListingSeasonServerCommand {
        return ListingSeasonServerCommand();
    }

    func teamScenarioServerCommand() -> TeamScenarioServerCommand {
        return TeamScenarioServerCommand();
    }

    func updatePushStatusServerCommand() -> UpdatePushStatusServerCommand {
        return UpdatePushStatusServerCommand();
    }

    func validListingServerCommand() -> ValidListingServerCommand {
        return ValidListingServerCommand();
    }

    func validSeasonServerCommand() -> ValidSeasonServerCommand {
        return ValidSeasonServerCommand();
    }

    func validTeamListingServerCommand() -> ValidTeamListingServerCommand {
        return ValidTeamListingServerCommand();
    }

    func validTeamScenarioServerCommand() -> ValidTeamScenarioServerCommand {
        return ValidTeamScenarioServerCommand();
    }

    func versionServerCommand() -> VersionServerCommand {
        return VersionServerCommand();
    }

    func constructServerService( _ serverRequestVO:ServerRequestVO ) -> ConstructServerService {
        return ConstructServerService( serverRequestVO );
    }
}
