import Foundation

class ConstructServerService {

    private var app:App { return App.instance }
    private var balancing:Balancing { return Balancing.instance }
    private var happen: Happen { return Happen.instance }
    private var localization:Localization { return Localization.instance }
    private var viewed: Viewed { return Viewed.instance }

    let serverRequestVO:ServerRequestVO
    let params:NSMutableDictionary = NSMutableDictionary()

    init( _ serverRequestVO:ServerRequestVO ){
        self.serverRequestVO = serverRequestVO
    }

    func constructDownloadBalancing() {
        createProductType(ServerConstant.BALANCING_PRODUCT_TYPE_VALUE)
        addReturnFileToParams()
        addVersionToParams()
    }

    func constructDownloadLocalization() {
        createProductType(ServerConstant.LOCALIZATION_PRODUCT_TYPE_VALUE)
        addReturnFileToParams()
        addLocalizationTypeParams()
        addVersionToParams()
    }

    func constructBase() {

        createProductType(ServerConstant.BASE_PRODUCT_TYPE_VALUE)
        addClientVersion()
        addHappenedRequestToParams()
        addRelevantToParams()
        addAssistDataToParams()
        addValidToParams()
        addEventScenarioFeedToParams()
        addFixtureToParams()
        addTeamDetailsToParams()
    }

    func constructEventFeed() {
        createProductType(ServerConstant.EVENT_SCENARIO_FEED_PRODUCT_TYPE_VALUE)
        addHappenedRequestToParams()
        addAssistDataToParams()
        addValidToParams()
        addEventScenarioFeedToParams()
    }

    func constructFixtureFeed() {
        createProductType(ServerConstant.FIXTURE_FEED_PRODUCT_TYPE_VALUE)
        addHappenedRequestToParams()
        addAssistDataToParams()
        addValidToParams()
        addFixtureToParams()
    }

    func constructListingSeason() {

        createProductType(ServerConstant.LISTING_SEASON_PRODUCT_TYPE_VALUE)
        addClientVersion()
        addHappenedRequestToParams()
        addValidToParams()
        addEventScenarioFeedToParams()
    }

    func constructTeamScenario(fullListingSeasonRequested:Bool = false ) {
        createProductType(ServerConstant.TEAM_SCENARIO_PRODUCT_TYPE_VALUE)
        if fullListingSeasonRequested {
            addClientVersion()
            params[ServerConstant.RETURN_LISTING_DETAILS] = true
        }
        addHappenedRequestToParams()
        addValidToParams(isTeamSpecific:true)
    }

    func constructValidListing() {
        createProductType(ServerConstant.VALID_LISTING_PRODUCT_TYPE_VALUE)
        addHappenedRequestToParams()
    }

    func constructValidSeason() {
        createProductType(ServerConstant.VALID_SEASON_PRODUCT_TYPE_VALUE)
        addHappenedRequestToParams()
    }

    func constructValidTeamListing() {
        createProductType(ServerConstant.VALID_TEAM_LISTING_PRODUCT_TYPE_VALUE)
        addHappenedRequestToParams()
    }

    func constructValidTeamScenario() {
        createProductType(ServerConstant.VALID_TEAM_SCENARIO_PRODUCT_TYPE_VALUE)
        addHappenedRequestToParams()
    }

    func constructVersion() {
        createProductType(ServerConstant.VERSION_PRODUCT_TYPE_VALUE)
    }

    func constructSubscribe() {
        createProductType(ServerConstant.SUBSCRIBE_PRODUCT_TYPE_VALUE)

        let deviceToken:String = serverRequestVO.deviceToken!
        let pushEnabled:Bool = serverRequestVO.pushEnabled != nil && serverRequestVO.pushEnabled! ? true : false
        params[ServerConstant.DEVICE_TOKEN] = deviceToken
        params[ServerConstant.PUSH_ENABLED_PARAM] = pushEnabled
        if serverRequestVO.localizationType != nil {
            params[ServerConstant.LOCALIZATION_TYPE_PARAM] = serverRequestVO.localizationType!
        }

        if serverRequestVO.subscribeTeamRefs != nil {
            if serverRequestVO.subscribeTeamRefs!.count != 0 {
                params[ServerConstant.SUBSCRIBED_TEAM_PARAM] = app.arrayHelper.intDicToIntArr(serverRequestVO.subscribeTeamRefs!)
            }
        }
    }


    private func createProductType(_ type:String) {
        params.removeAllObjects()
        params[ServerConstant.PRODUCT_TYPE_PARAM] = type
    }

    private func addClientVersion() {
        let clientVersion:Int? = happen.happenedModel.clientVersion
        if clientVersion != nil {
            params[ServerConstant.CLIENT_VERSION_PARAM] = clientVersion
        }
    }

    private func addHappenedRequestToParams() {

        func addRefs(_ dictionary:NSMutableDictionary, _ param:String, refs:[Int:Any]?) {
            if refs != nil {
                if refs!.count != 0 {
                    dictionary[param] = app.arrayHelper.intDicToIntArr(refs!)
                }
            }
        }

        if let happenedRequestVOs:[HappenedServerRequestVO] = serverRequestVO.allHappenedRequestVOs {
            let happenedRequestArray:NSMutableArray = NSMutableArray()
            for happenedRequestVO: HappenedServerRequestVO in happenedRequestVOs {
                let happenedRequestDictionary:NSMutableDictionary = NSMutableDictionary()

                addRefs(happenedRequestDictionary, ServerConstant.SEASON_GROUP_PARAM, refs: happenedRequestVO.seasonGroupRefs)
                addRefs(happenedRequestDictionary, ServerConstant.SEASON_PARAM, refs: happenedRequestVO.seasonRefs)
                addRefs(happenedRequestDictionary, ServerConstant.COMPETITION_PARAM, refs: happenedRequestVO.competitionRefs)
                addRefs(happenedRequestDictionary, ServerConstant.LISTING_PARAM, refs: happenedRequestVO.listingRefs)
                addRefs(happenedRequestDictionary, ServerConstant.CLUB_PARAM, refs: happenedRequestVO.clubRefs)
                addRefs(happenedRequestDictionary, ServerConstant.TEAM_PARAM, refs: happenedRequestVO.teamRefs)
                addRefs(happenedRequestDictionary, ServerConstant.LISTING_SEASON_OUT_PARAM, refs: happenedRequestVO.listingSeasonRefs)
                addRefs(happenedRequestDictionary, ServerConstant.TEAM_LISTING_OUT_PARAM, refs: happenedRequestVO.teamListingRefs)
                addRefs(happenedRequestDictionary, ServerConstant.LISTING_SCENARIO_OUT_PARAM, refs: happenedRequestVO.listingScenarioRefs)
                addRefs(happenedRequestDictionary, ServerConstant.EVENT_SCENARIO_OUT_PARAM, refs: happenedRequestVO.eventScenarioRefs)

                if happenedRequestDictionary.count != 0 {
                    happenedRequestArray.add(happenedRequestDictionary)
                }
            }
            if happenedRequestArray.count != 0 {
                params[ServerConstant.HAPPENED_REQUEST_PARAM] = happenedRequestArray
            }
        }
    }

    private func addRelevantToParams() {
        if !serverRequestVO.returnRelevantListingSeason {
            return
        }
        params[ServerConstant.RETURN_RELEVANT_LISTING_SEASON] = true
    }

    private func addAssistDataToParams() {
        if !serverRequestVO.checkAssistData {
            return
        }
        var returnAssistData = false
        if let happenedRequestVOs:[HappenedServerRequestVO] = serverRequestVO.allHappenedRequestVOs {
            for happenedRequestVO: HappenedServerRequestVO in happenedRequestVOs {
                if Bool(happenedRequestVO.seasonRefs == nil) || Bool(happenedRequestVO.listingRefs == nil) {
                    returnAssistData = true
                } else {
                    if !happen.happenedHelper.haveCompleteListingSeasons(happenedRequestVO.seasonRefs!, happenedRequestVO.listingRefs!) {
                        returnAssistData = true
                    }
                }
            }
        } else {
            returnAssistData = true
        }
        if !returnAssistData {
            return
        }
        params[ServerConstant.RETURN_ASSIST_DATA] = true
    }

    private func addValidToParams( isTeamSpecific:Bool = false ) {
        var returnValidSeasonListing:Bool = serverRequestVO.returnValidSeasonListing != nil ? serverRequestVO.returnValidSeasonListing! : false
        var returnValidListingSeason:Bool = serverRequestVO.returnValidListingSeason != nil ? serverRequestVO.returnValidListingSeason! : false
        var returnValidTeamListing:Bool = serverRequestVO.returnValidTeamListing != nil ? serverRequestVO.returnValidTeamListing! : false
        var returnValidTeamScenario:Bool = serverRequestVO.returnValidTeamScenario != nil ? serverRequestVO.returnValidTeamScenario! : false
        if let happenedRequestVOs:[HappenedServerRequestVO] = serverRequestVO.allHappenedRequestVOs {
            for happenedRequestVO: HappenedServerRequestVO in happenedRequestVOs {

                if serverRequestVO.returnValidSeasonListing == nil && !returnValidSeasonListing {
                    let seasonRefs:[Int:NSNull] = balancing.balancingHelper.getAllSeasonRefs(seasonGroupRefs: happenedRequestVO.seasonGroupRefs, seasonRefs: happenedRequestVO.seasonRefs)
                    for seasonRef:Int in seasonRefs.keys {
                        let seasonVO:SeasonVO? = happen.happenedModel.seasonVOs[seasonRef]
                        if Bool(seasonVO == nil) || !seasonVO!.haveData {
                            returnValidSeasonListing = true
                            break
                        }
                    }
                }

                if serverRequestVO.returnValidListingSeason == nil && !returnValidListingSeason {
                    let listingRefs:[Int:NSNull] = balancing.balancingHelper.getAllListingRefs(competitionRefs: happenedRequestVO.competitionRefs, listingRefs: happenedRequestVO.listingRefs, seasonRefs: happenedRequestVO.seasonRefs)
                    for listingRef:Int in listingRefs.keys {
                        let listingVO:ListingVO? = happen.happenedModel.listingVOs[listingRef]
                        if Bool(listingVO == nil) || !listingVO!.haveData {
                            returnValidListingSeason = true
                            break
                        }
                    }
                }

                if serverRequestVO.returnValidTeamScenario == nil && !returnValidTeamScenario {
                    let teamRefs:[Int:NSNull] = balancing.balancingHelper.getAllTeamRefs(clubRefs: happenedRequestVO.clubRefs, teamRefs: happenedRequestVO.teamRefs, seasonRefs: happenedRequestVO.seasonRefs)
                    for teamRef:Int in teamRefs.keys {
                        let teamVO:TeamVO? = happen.happenedModel.teamVOs[teamRef]
                        if Bool(teamVO == nil) || (!isTeamSpecific && !teamVO!.havePartialData ) || (isTeamSpecific && (!teamVO!.havePartialData || !teamVO!.haveData) ) {
                            if !isTeamSpecific {
                                returnValidTeamListing = true
                            } else {
                                returnValidTeamScenario = true
                            }
                            break
                        }
                    }
                    if (happenedRequestVO.listingScenarioRefs != nil && happenedRequestVO.listingScenarioRefs!.count > 0) || (happenedRequestVO.eventScenarioRefs != nil && happenedRequestVO.eventScenarioRefs!.count > 0 ) {
                        returnValidTeamScenario = true
                    }
                }

            }
        }
        serverRequestVO.returnValidSeasonListing = returnValidSeasonListing ? returnValidSeasonListing : serverRequestVO.returnValidSeasonListing
        serverRequestVO.returnValidListingSeason = returnValidListingSeason ? returnValidListingSeason : serverRequestVO.returnValidListingSeason
        serverRequestVO.returnValidTeamListing = returnValidTeamListing ? returnValidTeamListing : serverRequestVO.returnValidTeamListing
        serverRequestVO.returnValidTeamScenario = returnValidTeamScenario ? returnValidTeamScenario : serverRequestVO.returnValidTeamScenario

        if serverRequestVO.returnValidSeason != nil && serverRequestVO.returnValidSeason! {
            params[ServerConstant.RETURN_VALID_SEASON] = true
        }

        if returnValidSeasonListing {
            params[ServerConstant.RETURN_VALID_SEASON_LISTING] = true
        }

        if returnValidListingSeason {
            params[ServerConstant.RETURN_VALID_LISTING_SEASON] = true
        }

        if returnValidTeamScenario {
            params[ServerConstant.RETURN_VALID_TEAM_SCENARIO] = true
        } else  if returnValidTeamListing {
            params[ServerConstant.RETURN_VALID_TEAM_LISTING] = true
        }
    }

    private func addEventScenarioFeedToParams() {

        if !serverRequestVO.returnEventScenarioFeed && Bool(params.object(forKey: ServerConstant.PRODUCT_TYPE_PARAM) as? String != ServerConstant.EVENT_SCENARIO_FEED_PRODUCT_TYPE_VALUE) {
            return
        }

        if serverRequestVO.returnEventScenarioFeed {
            params[ServerConstant.RETURN_EVENT_SCENARIO_FEED] = true
        }

        if serverRequestVO.entriesFrom != nil {
            params[ServerConstant.LIMIT_FROM_PARAM] = serverRequestVO.entriesFrom!
        } else {
            params[ServerConstant.COUNT_ENTRIES] = true
        }

        if serverRequestVO.entriesSpan != nil {
            params[ ServerConstant.LIMIT_SPAN_PARAM ] = serverRequestVO.entriesSpan
        }

        if serverRequestVO.isHappenedState == .both {
            params[ServerConstant.BOTH_PARAM] = true
        } else if serverRequestVO.isHappenedState == .upcomingOnly {
            params[ ServerConstant.UPCOMING_OUT_PARAM ] = true
        }

        if serverRequestVO.isAltIfEmpty {
            params[ ServerConstant.ALT_IF_EMPTY ] = true
        }

        if serverRequestVO.useListingAsFeed {
            params[ ServerConstant.USE_LISTING_AS_FEED ] = true
        }

        if serverRequestVO.eventRefs != nil {
            if serverRequestVO.eventRefs!.count != 0 {
                params[ ServerConstant.EVENT_FILTER ] = app.arrayHelper.intDicToIntArr(serverRequestVO.eventRefs!)
            }
        }

        if serverRequestVO.toHappenState != nil {
            var toHappenStateValue:Int = 0
            if serverRequestVO.toHappenState == .toHappenOnly {
                toHappenStateValue = 1
            } else if serverRequestVO.toHappenState == .toNotHappenOnly {
                toHappenStateValue = 2
            }
            params[ServerConstant.TO_HAPPEN_STATE] = toHappenStateValue
        }
    }

    private func addFixtureToParams() {

        if !serverRequestVO.returnFixtureFeed && Bool(params.object(forKey: ServerConstant.PRODUCT_TYPE_PARAM) as? String != ServerConstant.FIXTURE_FEED_PRODUCT_TYPE_VALUE) {
            return
        }

        if serverRequestVO.returnFixtureFeed {
            params[ServerConstant.RETURN_FIXTURE_FEED] = true
        }

        if serverRequestVO.entriesFrom != nil {
            params[ServerConstant.LIMIT_FROM_PARAM] = serverRequestVO.entriesFrom!
        } else {
            params[ServerConstant.COUNT_ENTRIES] = true
        }

        let entriesSpan:Int = serverRequestVO.entriesSpan != nil ? serverRequestVO.entriesSpan! : viewed.viewConfig.visibleFixtureSpan
        params[ ServerConstant.LIMIT_SPAN_PARAM ] = entriesSpan


        if serverRequestVO.isHappenedState == .both {
            params[ServerConstant.BOTH_PARAM] = true
        } else if serverRequestVO.isHappenedState == .upcomingOnly {
            params[ ServerConstant.UPCOMING_OUT_PARAM ] = true
        }

        if serverRequestVO.useListingAsFeed {
            params[ ServerConstant.USE_LISTING_AS_FEED ] = true
        }
    }

    private func addTeamDetailsToParams() {
        if !serverRequestVO.returnTeamDetails {
            return
        }
        params[ServerConstant.RETURN_TEAM_DETAILS] = true
    }

    private func addReturnFileToParams() {
        if !serverRequestVO.returnFile {
            return
        }
        params[ServerConstant.RETURN_FILE_PARAM] = true
    }

    private func addVersionToParams() {
        if serverRequestVO.version == nil {
            return
        }
        params[ServerConstant.VERSION_OUT_PARAM] = serverRequestVO.version!
    }

    private func addLocalizationTypeParams() {
        if serverRequestVO.localizationType == nil {
            return
        }
        params[ServerConstant.LOCALIZATION_TYPE_PARAM] = serverRequestVO.localizationType!
    }
}
