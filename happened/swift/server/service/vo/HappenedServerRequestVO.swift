import Foundation

class HappenedServerRequestVO {

    var seasonGroupRefs:[Int:NSNull]?
    var seasonRefs:[Int:NSNull]?
    var competitionRefs:[Int:NSNull]?
    var listingRefs:[Int:NSNull]?
    var clubRefs:[Int:NSNull]?
    var teamRefs:[Int:NSNull]?
    var listingSeasonRefs:[Int:NSNull]?
    var teamListingRefs:[Int:NSNull]?
    var listingScenarioRefs:[Int:NSNull]?
    var eventScenarioRefs:[Int:NSNull]?

    var hasData:Bool {
        if seasonGroupRefs != nil {
            if seasonGroupRefs!.count > 0 {
                return true
            }
        }
        if seasonRefs != nil {
            if seasonRefs!.count > 0 {
                return true
            }
        }
        if competitionRefs != nil {
            if competitionRefs!.count > 0 {
                return true
            }
        }
        if listingRefs != nil {
            if listingRefs!.count > 0 {
                return true
            }
        }
        if clubRefs != nil {
            if clubRefs!.count > 0 {
                return true
            }
        }
        if teamRefs != nil {
            if teamRefs!.count > 0 {
                return true
            }
        }
        if listingSeasonRefs != nil {
            if listingSeasonRefs!.count > 0 {
                return true
            }
        }
        if teamListingRefs != nil {
            if teamListingRefs!.count > 0 {
                return true
            }
        }
        if listingScenarioRefs != nil {
            if listingScenarioRefs!.count > 0 {
                return true
            }
        }
        if eventScenarioRefs != nil {
            if eventScenarioRefs!.count > 0 {
                return true
            }
        }
        return false
    }

}
