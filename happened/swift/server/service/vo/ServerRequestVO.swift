import Foundation

class ServerRequestVO {

    var happenedRequestVO: HappenedServerRequestVO = HappenedServerRequestVO()
    var happenedRequestVOs:[HappenedServerRequestVO]? //Typically its easier to use a single entry but there are occasions when I may want to use more
    var returnBalancing:Bool = false
    var returnLocalization:Bool = false
    var returnValidSeason:Bool?
    var returnValidSeasonListing:Bool?
    var returnValidListingSeason:Bool?
    var returnValidTeamScenario:Bool?
    var returnValidTeamListing:Bool?
    var returnEventScenarioFeed:Bool = false
    var returnFixtureFeed:Bool = false
    var isHappenedState:IsHappenedState = .happenedOnly
    var isAltIfEmpty:Bool = false
    var useListingAsFeed:Bool = false
    var checkAssistData:Bool = false
    var returnAllListings:Bool = false
    var returnRelevantListingSeason:Bool = false
    var returnTeamDetails:Bool = false
    var eventRefs:[Int:NSNull]?
    var toHappenState:ToHappenedState?
    var entriesFrom:Int?
    var entriesSpan:Int?
    var returnFile:Bool = false
    var localizationType:String?
    var version:Int?
    var filterString:String? // For internal use
    var deviceToken:String? = nil
    var subscribeTeamRefs:[Int:Any]?
    var pushEnabled:Bool? = nil

    var allHappenedRequestVOs:[HappenedServerRequestVO]? {
        if !happenedRequestVO.hasData {
            return happenedRequestVOs
        }
        var vos:[HappenedServerRequestVO] = [happenedRequestVO]
        if happenedRequestVOs != nil {
            vos.append(contentsOf: happenedRequestVOs!)
        }
        return vos
    }
}
