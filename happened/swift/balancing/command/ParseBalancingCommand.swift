import Foundation

class ParseBalancingCommand: NSObject, XMLParserDelegate {

    private var app:App { return App.instance }
    private var balancing:Balancing { return Balancing.instance }
    private var localization:Localization { return Localization.instance }
    private var preference:Preference { return Preference.instance }


    func command() {

        let balancingURL:URL = app.fileHelper.fileURL(balancing.balancingConfig.fileName, balancing.balancingConfig.fileExt)!

        let xmlParser:XMLParser = XMLParser(contentsOf: balancingURL)!
        xmlParser.delegate = self

        xmlParser.parse()

        if !balancing.parseBalancingHelper.isBalancingValid {
            app.updateErrorAppCommand().command()
            return
        }

        applySeasonsToSeasonsGroups()
        applySeasonGroupOrder()
        applySeasonOrder()
        applyLatestSeasonGroup()

        applyCompetitions()
        applyListings()
        applyClubs()
        applyTeams()
        applyEvents()

        if !balancing.parseBalancingHelper.isBalancingValid {
            app.updateErrorAppCommand().command()
            return
        }
        applySeasonTypeOnHiatus()
        applyListingsToCompetitions()
        applyCompetitionsToCompetitions()
        applyTeamsToClubs()
        applyExtendedOnHiatus()
        applyDefaults()
        applyAllSubSeasons()
        applyDefaultVOs()
        updatePreferences()

        if !balancing.parseBalancingHelper.isBalancingValid {
            app.updateErrorAppCommand().command()
            return
        }
        balancing.balancingModel.initialized = true
    }


    @objc func parser(_ parser: XMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [String : String]) {
        if elementName == BalancingConstant.ITEM_ELEMENT {
            let category = attributeDict[BalancingConstant.CATEGORY_ATTRIBUTE]
            if category == BalancingConstant.HAPPENED_CATEGORY {
                let type = attributeDict[BalancingConstant.TYPE_ATTRIBUTE]
                if type != nil
                {
                    switch type! {
                    case BalancingConstant.DEFAULT_TYPE:
                        parseDefault( attributeDict )
                        break

                    case BalancingConstant.SEASON_GROUP_HAPPENED_TYPE:
                        parseSeasonGroup( attributeDict )
                        break

                    case BalancingConstant.SEASON_HAPPENED_TYPE:
                        parseSeason( attributeDict )
                        break

                    case BalancingConstant.COMPETITION_HAPPENED_TYPE:
                        parseCompetition( attributeDict )
                        break

                    case BalancingConstant.LISTING_HAPPENED_TYPE:
                        parseListing( attributeDict )
                        break

                    case BalancingConstant.CLUB_HAPPENED_TYPE:
                        parseClub( attributeDict )
                        break

                    case BalancingConstant.TEAM_HAPPENED_TYPE:
                        parseTeam( attributeDict )
                        break

                    case BalancingConstant.EVENT_HAPPENED_TYPE:
                        parseEvent( attributeDict )
                        break

                    case BalancingConstant.STRUCTURE_LISTING_HAPPENED_TYPE:
                        parseStructure( attributeDict )
                        break

                    case BalancingConstant.TYPE_CLUB_HAPPENED_TYPE:
                        parseTypeClub( attributeDict )
                        break

                    case BalancingConstant.TYPE_TEAM_HAPPENED_TYPE:
                        parseTypeTeam( attributeDict )
                        break

                    default:
                        break
                    }
                }
            } else {
                //only interested in happened
            }
        }
        else
        {
            //only interested in item
        }
    }

    private func parseDefault( _ attributeDict: [String : String] ) {
        let param:String? = app.arrayHelper.stringFromStrStrDic(attributeDict, BalancingConstant.PARAM_ATTRIBUTE)
        let seasonGroupRef:Int? = app.arrayHelper.intFromStrStrDic(attributeDict, BalancingConstant.SEASON_GROUP_REF_HAPPENED_ATTRIBUTE)
        let seasonRefs:[Int: NSNull]? = app.arrayHelper.intNulDicFromStrStrDicStringEntry(attributeDict, BalancingConstant.SEASON_REF_HAPPENED_ATTRIBUTE)
        let typeTeamRefs:[Int: NSNull]? = app.arrayHelper.intNulDicFromStrStrDicStringEntry(attributeDict, BalancingConstant.TYPE_TEAM_REF_HAPPENED_ATTRIBUTE)
        let competitionRefs:[Int: NSNull]? = app.arrayHelper.intNulDicFromStrStrDicStringEntry(attributeDict, BalancingConstant.COMPETITION_REF_HAPPENED_ATTRIBUTE)
        let listingRefs:[Int: NSNull]? = app.arrayHelper.intNulDicFromStrStrDicStringEntry(attributeDict, BalancingConstant.LISTING_REF_HAPPENED_ATTRIBUTE)

        let genericVO = balancing.balancingModel.defaultVO

        if genericVO.paramTypeVOs == nil {
            genericVO.paramTypeVOs = [:]
        }
        var vo:DefaultBalancingVO?
        if Bool(param == nil) || Bool(param! == BalancingConstant.GENERIC_TYPE) {
            vo = genericVO
        } else {
            vo = DefaultBalancingVO( param )
            genericVO.paramTypeVOs![ vo!.param ] = vo
        }
        vo!.initialized = true

        vo!.seasonGroupRef = seasonGroupRef;
        vo!.seasonRefs = seasonRefs;
        vo!.typeTeamRefs = typeTeamRefs;
        vo!.competitionRefs = competitionRefs;
        vo!.listingRefs = listingRefs;

    }

    private func parseHappenType(_ vo: HappenedBalancingVO, _ attributeDict: [String : String] ) {
        let recognizer:String? = app.arrayHelper.stringFromStrStrDic(attributeDict, BalancingConstant.RECOGNIZER_ATTRIBUTE)
        let baseTextID:String? = app.arrayHelper.stringFromStrStrDic(attributeDict, BalancingConstant.BASE_TEXT_ID_ATTRIBUTE)
        let publicID:String? = app.arrayHelper.stringFromStrStrDic(attributeDict, BalancingConstant.PUBLIC_ID_HAPPENED_ATTRIBUTE)

        vo.recognizer = recognizer;
        vo.baseTextID = baseTextID;
        vo.publicID = publicID;
    }

    private func parseSeasonType(_ vo:SeasonTypeBalancingVO, _ attributeDict: [String : String] ) {
        parseHappenType(vo, attributeDict)

        let added:Bool = app.arrayHelper.booleanFromStrStrDic(attributeDict, BalancingConstant.ADDED_HAPPENED_ATTRIBUTE, defaultValue: false)!
        let removed:Bool = app.arrayHelper.booleanFromStrStrDic(attributeDict, BalancingConstant.REMOVED_HAPPENED_ATTRIBUTE, defaultValue: false)!

        vo.added = added
        vo.removed = removed
    }

    private func parseSeasonGroup( _ attributeDict: [String : String] ) {
        let ref:Int? = app.arrayHelper.intFromStrStrDic(attributeDict, BalancingConstant.SEASON_GROUP_REF_HAPPENED_ATTRIBUTE)

        let nominalDate:Date? = app.arrayHelper.dateFromStrStrDic(attributeDict, BalancingConstant.NOMINAL_DATE_SEASON_GROUP_ATTRIBUTE, requireDay1:true)
        let latest:Bool = app.arrayHelper.booleanFromStrStrDic(attributeDict, BalancingConstant.LATEST_SEASON_GROUP_ATTRIBUTE, defaultValue:false)!

        if ref == nil {
            return;
        }
        if nominalDate == nil {
            return;
        }

        let vo:SeasonGroupBalancingVO = SeasonGroupBalancingVO( ref! )
        vo.nominalDate = nominalDate
        vo.latest = latest

        parseHappenType(vo, attributeDict)
        balancing.balancingModel.seasonGroupVOs[ vo.ref ] = vo
    }

    private func parseSeason( _ attributeDict: [String : String] ) {
        let ref:Int? = app.arrayHelper.intFromStrStrDic(attributeDict, BalancingConstant.SEASON_REF_HAPPENED_ATTRIBUTE)

        let startDate:Date? = app.arrayHelper.dateFromStrStrDic(attributeDict, BalancingConstant.START_DATE_SEASON_ATTRIBUTE, requireDay1:true)
        let endDate:Date? = app.arrayHelper.dateFromStrStrDic(attributeDict, BalancingConstant.END_DATE_SEASON_ATTRIBUTE, requireDay1: true)
        let seasonGroupRefs:[Int: NSNull]? = app.arrayHelper.intNulDicFromStrStrDicStringEntry(attributeDict, BalancingConstant.SEASON_GROUP_REFS_HAPPENED_ATTRIBUTE)

        if ref == nil {
            return;
        }
        if startDate == nil {
            return;
        }
        if endDate == nil {
            return;
        }
        if seasonGroupRefs == nil {
            return;
        }

        let vo:SeasonBalancingVO = SeasonBalancingVO( ref! )
        vo.startDate = startDate
        vo.endDate = endDate
        vo.seasonGroupRefs = seasonGroupRefs
        parseHappenType(vo, attributeDict)

        balancing.balancingModel.seasonVOs[ vo.ref ] = vo

    }

    private func parseCompetition( _ attributeDict: [String : String] ) {
        let ref:Int? = app.arrayHelper.intFromStrStrDic(attributeDict, BalancingConstant.COMPETITION_REF_HAPPENED_ATTRIBUTE)
        let seasonRef:Int? = app.arrayHelper.intFromStrStrDic(attributeDict, BalancingConstant.SEASON_REF_HAPPENED_ATTRIBUTE)

        let focal:Bool? = app.arrayHelper.booleanFromStrStrDic(attributeDict, BalancingConstant.FOCAL_COMPETITION_ATTRIBUTE)
        var competitionType:String? = nil
        if focal != nil {
            competitionType = ""
            if focal! != false {
                competitionType = BalancingConstant.FOCAL_COMPETITION_TYPE
            }
        }

        let structureRef:Int? = app.arrayHelper.intFromStrStrDic(attributeDict, BalancingConstant.STRUCTURE_LISTING_REF_HAPPENED_ATTRIBUTE)
        let parentCompetitionRef:Int? = app.arrayHelper.intFromStrStrDic(attributeDict, BalancingConstant.PARENT_COMPETITION_REF_HAPPENED_ATTRIBUTE)
        let reverseRound:Int? = app.arrayHelper.intFromStrStrDic(attributeDict, BalancingConstant.REVERSE_ROUND_COMPETITION_ATTRIBUTE)
        let listPriority:Int? = app.arrayHelper.intFromStrStrDic(attributeDict, BalancingConstant.LIST_PRIORITY_HAPPENED_ATTRIBUTE)
        let showPriority:Int? = app.arrayHelper.intFromStrStrDic(attributeDict, BalancingConstant.SHOW_PRIORITY_COMPETITION_ATTRIBUTE)
        let applyGlobalRankAtEnd:Bool? = app.arrayHelper.booleanFromStrStrDic(attributeDict, BalancingConstant.APPLY_GLOBAL_RANK_AT_END_COMPETITION_ATTRIBUTE)
        let numberOfTeams:Int? = app.arrayHelper.intFromStrStrDic(attributeDict, BalancingConstant.NUMBER_OF_TEAMS_COMPETITION_ATTRIBUTE)

        let typeClubRefs:[Int: NSNull]? = app.arrayHelper.intNulDicFromStrStrDicStringEntry(attributeDict, BalancingConstant.TYPE_CLUB_REFS_HAPPENED_ATTRIBUTE)
        let typeTeamRefs:[Int: NSNull]? = app.arrayHelper.intNulDicFromStrStrDicStringEntry(attributeDict, BalancingConstant.TYPE_TEAM_REFS_HAPPENED_ATTRIBUTE)

        if ref == nil {
            return;
        }

        let vo:CompetitionBalancingVO = CompetitionBalancingVO( ref!, seasonRef: seasonRef )

        vo.competitionType = competitionType
        vo.structureRef = structureRef
        vo.parentCompetitionRef = parentCompetitionRef
        vo.reverseRound = reverseRound
        vo.listPriority = listPriority
        vo.showPriority = showPriority
        vo.applyGlobalRankAtEnd = applyGlobalRankAtEnd
        vo.numberOfTeams = numberOfTeams
        vo.typeClubRefs = typeClubRefs
        vo.typeTeamRefs = typeTeamRefs

        parseSeasonType(vo, attributeDict)

        var existingVO:CompetitionBalancingVO? = balancing.balancingModel.competitionVOs[ vo.ref ]

        if vo.seasonRef == nil {
            if existingVO != nil {
                vo.seasonVOs = existingVO!.seasonVOs
            }
            balancing.balancingModel.competitionVOs[vo.ref] = vo
        } else {
            if existingVO == nil {
                existingVO = CompetitionBalancingVO( vo.ref, isShell: true)
                balancing.balancingModel.competitionVOs[existingVO!.ref] = existingVO
            }
            existingVO!.seasonVOs![vo.seasonRef!] = vo
        }

    }

    private func parseListing( _ attributeDict: [String : String] ) {
        let ref:Int? = app.arrayHelper.intFromStrStrDic(attributeDict, BalancingConstant.LISTING_REF_HAPPENED_ATTRIBUTE)
        let seasonRef:Int? = app.arrayHelper.intFromStrStrDic(attributeDict,  BalancingConstant.SEASON_REF_HAPPENED_ATTRIBUTE)

        let parentCompetitionRef:Int? = app.arrayHelper.intFromStrStrDic(attributeDict, BalancingConstant.PARENT_COMPETITION_REF_HAPPENED_ATTRIBUTE)
        let listPriority:Int? = app.arrayHelper.intFromStrStrDic(attributeDict, BalancingConstant.LIST_PRIORITY_HAPPENED_ATTRIBUTE)

        if ref == nil {
            return;
        }

        let vo:ListingBalancingVO = ListingBalancingVO( ref!, seasonRef: seasonRef )

        vo.parentCompetitionRef = parentCompetitionRef
        vo.listPriority = listPriority

        parseSeasonType(vo, attributeDict)

        var existingVO:ListingBalancingVO? = balancing.balancingModel.listingVOs[ vo.ref ]

        if vo.seasonRef == nil {
            if existingVO != nil {
                vo.seasonVOs = existingVO!.seasonVOs
            }
            balancing.balancingModel.listingVOs[vo.ref] = vo
        } else {
            if existingVO == nil {
                existingVO = ListingBalancingVO( vo.ref, isShell: true )
                balancing.balancingModel.listingVOs[existingVO!.ref] = existingVO
            }
            existingVO!.seasonVOs![vo.seasonRef!] = vo
        }

    }

    private func parseClub( _ attributeDict: [String : String] ) {
        let ref:Int? = app.arrayHelper.intFromStrStrDic(attributeDict, BalancingConstant.CLUB_REF_HAPPENED_ATTRIBUTE)
        let seasonRef:Int? = app.arrayHelper.intFromStrStrDic(attributeDict, BalancingConstant.SEASON_REF_HAPPENED_ATTRIBUTE)

        let typeClubRef:Int? = app.arrayHelper.intFromStrStrDic(attributeDict, BalancingConstant.TYPE_CLUB_REF_HAPPENED_ATTRIBUTE)
        let hashtag:String? = app.arrayHelper.stringFromStrStrDic(attributeDict, BalancingConstant.HASHTAG_HAPPENED_ATTRIBUTE)

        if ref == nil {
            return;
        }

        let vo:ClubBalancingVO = ClubBalancingVO( ref!, seasonRef: seasonRef )
        vo.typeClubRef = typeClubRef
        vo.hashtag = hashtag

        parseSeasonType(vo, attributeDict)

        var existingVO:ClubBalancingVO? = balancing.balancingModel.clubVOs[ vo.ref ]

        if vo.seasonRef == nil {
            if existingVO != nil {
                vo.seasonVOs = existingVO!.seasonVOs
            }
            balancing.balancingModel.clubVOs[vo.ref] = vo
        } else {
            if existingVO == nil {
                existingVO = ClubBalancingVO( vo.ref, isShell: true )
                balancing.balancingModel.clubVOs[existingVO!.ref] = existingVO
            }
            existingVO!.seasonVOs![vo.seasonRef!] = vo
        }
    }

    private func parseTeam( _ attributeDict: [String : String] ) {
        let ref:Int? = app.arrayHelper.intFromStrStrDic(attributeDict, BalancingConstant.TEAM_REF_HAPPENED_ATTRIBUTE)
        let seasonRef:Int? = app.arrayHelper.intFromStrStrDic(attributeDict, BalancingConstant.SEASON_REF_HAPPENED_ATTRIBUTE)

        let clubRef:Int? = app.arrayHelper.intFromStrStrDic(attributeDict, BalancingConstant.CLUB_REF_HAPPENED_ATTRIBUTE)
        let typeTeamRef:Int? = app.arrayHelper.intFromStrStrDic(attributeDict, BalancingConstant.TYPE_TEAM_REF_HAPPENED_ATTRIBUTE)

        if ref == nil {
            return;
        }

        let vo:TeamBalancingVO = TeamBalancingVO( ref!, seasonRef: seasonRef)

        vo.clubRef = clubRef
        vo.typeTeamRef = typeTeamRef

        parseSeasonType(vo, attributeDict)

        var existingVO:TeamBalancingVO? = balancing.balancingModel.teamVOs[ vo.ref ]

        if vo.seasonRef == nil {
            if existingVO != nil {
                vo.seasonVOs = existingVO!.seasonVOs
            }
            balancing.balancingModel.teamVOs[vo.ref] = vo
        } else {
            if existingVO == nil {
                existingVO = TeamBalancingVO( vo.ref, isShell: true )
                balancing.balancingModel.teamVOs[existingVO!.ref] = existingVO
            }
            existingVO!.seasonVOs![vo.seasonRef!] = vo
        }

    }

    private func parseEvent( _ attributeDict: [String : String] ) {
        let ref:Int? = app.arrayHelper.intFromStrStrDic(attributeDict, BalancingConstant.EVENT_REF_HAPPENED_ATTRIBUTE)
        let seasonRef:Int? = app.arrayHelper.intFromStrStrDic(attributeDict, BalancingConstant.SEASON_REF_HAPPENED_ATTRIBUTE)

        let priority:Int? = app.arrayHelper.intFromStrStrDic(attributeDict, BalancingConstant.PRIORITY_EVENT_ATTRIBUTE)
        let followsEventRef:Int? = app.arrayHelper.intFromStrStrDic(attributeDict, BalancingConstant.FOLLOWS_EVENT_REF_EVENT_ATTRIBUTE)
        let splSplit:Bool? = app.arrayHelper.booleanFromStrStrDic(attributeDict, BalancingConstant.SPL_SPLIT_EVENT_ATTRIBUTE)
        let positive:Bool? = app.arrayHelper.booleanFromStrStrDic(attributeDict, BalancingConstant.POSITIVE_EVENT_ATTRIBUTE)

        if ref == nil {
            return;
        }

        let vo:EventBalancingVO = EventBalancingVO( ref!, seasonRef: seasonRef )
        vo.priority = priority
        vo.followsEventRef = followsEventRef
        vo.splSplit = splSplit
        vo.positive = positive

        parseSeasonType(vo, attributeDict)

        var existingVO = balancing.balancingModel.eventVOs[ vo.ref ]

        if vo.seasonRef == nil {
            if existingVO != nil {
                vo.seasonVOs = existingVO!.seasonVOs
            }
            balancing.balancingModel.eventVOs[vo.ref] = vo
        } else {
            if existingVO == nil {
                existingVO = EventBalancingVO( vo.ref, isShell: true )
                balancing.balancingModel.eventVOs[existingVO!.ref] = existingVO
            }
            existingVO!.seasonVOs![vo.seasonRef!] = vo
        }

    }

    private func parseStructure(_ attributeDict: [String : String] ) {
        let ref:Int? = app.arrayHelper.intFromStrStrDic(attributeDict, BalancingConstant.STRUCTURE_LISTING_REF_HAPPENED_ATTRIBUTE)
        let gamesAgainstType:String? = app.arrayHelper.stringFromStrStrDic(attributeDict, BalancingConstant.GAMES_AGAINST_TYPE_STRUCTURE_ATTRIBUTE)
        let listingDecidedTypes:[String]? = app.arrayHelper.strArrFromStrStrDicStringEntry(attributeDict, BalancingConstant.LISTING_DECIDED_TYPES_STRUCTURE_ATTRIBUTE)

        if ref == nil {
            return;
        }

        if gamesAgainstType == nil {
            return;
        }

        if listingDecidedTypes == nil {
            return;
        }

        let leagueStructure:Bool = app.arrayHelper.booleanFromStrStrDic(attributeDict, BalancingConstant.LEAGUE_STRUCTURE_ATTRIBUTE, defaultValue: false)!
        let defaultVO:StructureBalancingVO = leagueStructure ? balancing.balancingConfig.defaultLeagueStructureBalancingVO : balancing.balancingConfig.defaultKoRoundStructureBalancingVO

        let dummyGoalsPerWin:Int = app.arrayHelper.intFromStrStrDic(attributeDict, BalancingConstant.DUMMY_GOALS_PER_WIN_STRUCTURE_ATTRIBUTE, defaultValue:defaultVO.dummyGoalsForDraw)!
        let dummyGoalsPerDraw:Int = app.arrayHelper.intFromStrStrDic(attributeDict, BalancingConstant.DUMMY_GOALS_PER_DRAW_STRUCTURE_ATTRIBUTE, defaultValue:defaultVO.dummyGoalsForLoss)!
        let dummyGoalsPerLoss:Int = app.arrayHelper.intFromStrStrDic(attributeDict, BalancingConstant.DUMMY_GOALS_PER_LOSS_STRUCTURE_ATTRIBUTE, defaultValue:defaultVO.dummyGoalsForDraw)!
        var vo:StructureBalancingVO
        if leagueStructure {
            let leagueDefaultVO:LeagueStructureBalancingVO = defaultVO as! LeagueStructureBalancingVO
            let pointsPerWin:Int = app.arrayHelper.intFromStrStrDic(attributeDict, BalancingConstant.POINTS_PER_WIN_STRUCTURE_ATTRIBUTE, defaultValue:leagueDefaultVO.pointsPerWin)!
            let pointsPerDraw:Int = app.arrayHelper.intFromStrStrDic(attributeDict, BalancingConstant.POINTS_PER_DRAW_STRUCTURE_ATTRIBUTE, defaultValue:leagueDefaultVO.pointsPerDraw)!
            let pointsPerLoss:Int = app.arrayHelper.intFromStrStrDic(attributeDict, BalancingConstant.POINTS_PER_LOSS_STRUCTURE_ATTRIBUTE, defaultValue:leagueDefaultVO.pointsPerLoss)!
            let validGamesAgainst:Bool = app.arrayHelper.validValueFromStrArr(gamesAgainstType!,  balancing.balancingConfig.leagueGamesAgainstTypes)
            let listingDecidedOrder:[String] = app.arrayHelper.validValuesFromStrArr(listingDecidedTypes!, balancing.balancingConfig.leagueListingDecidedTypes)

            if !validGamesAgainst {
                return;
            }

            if listingDecidedOrder.count == 0 {
                return;
            }

            vo = LeagueStructureBalancingVO(ref!)
            vo.gamesAgainstType = gamesAgainstType
            vo.listingDecidedOrder = listingDecidedOrder

            let leagueVO:LeagueStructureBalancingVO = vo as! LeagueStructureBalancingVO
            leagueVO.pointsPerWin = pointsPerWin
            leagueVO.pointsPerDraw = pointsPerDraw
            leagueVO.pointsPerLoss = pointsPerLoss
        }
        else
        {
            let validGamesAgainst:Bool = app.arrayHelper.validValueFromStrArr(gamesAgainstType!, balancing.balancingConfig.koGamesAgainstTypes)
            let listingDecidedOrder:[String] = app.arrayHelper.validValuesFromStrArr(listingDecidedTypes!, balancing.balancingConfig.koListingDecidedTypes)

            if !validGamesAgainst {
                return;
            }

            if listingDecidedOrder.count == 0 {
                return;
            }

            vo = KoRoundStructureBalancingVO(ref!)
            vo.gamesAgainstType = gamesAgainstType
            vo.listingDecidedOrder = listingDecidedOrder
        }
        vo.dummyGoalsForWin = dummyGoalsPerWin
        vo.dummyGoalsForDraw = dummyGoalsPerDraw
        vo.dummyGoalsForLoss = dummyGoalsPerLoss

        parseHappenType(vo, attributeDict)
        if leagueStructure {
            balancing.balancingModel.leagueStructureVOs[ vo.ref ] = vo as? LeagueStructureBalancingVO
        } else {
            balancing.balancingModel.koRoundStructureVOs[ vo.ref ] = vo as? KoRoundStructureBalancingVO
        }
    }

    private func parseTypeClub( _ attributeDict: [String : String] ) {

        let ref:Int? = app.arrayHelper.intFromStrStrDic(attributeDict, BalancingConstant.TYPE_CLUB_REF_HAPPENED_ATTRIBUTE)

        if ref == nil {
            return;
        }

        let vo: HappenedBalancingVO = HappenedBalancingVO( ref! )

        parseHappenType(vo, attributeDict)
        balancing.balancingModel.typeClubVOs[ vo.ref ] = vo

    }

    private func parseTypeTeam( _ attributeDict: [String : String] ) {

        let ref:Int? = app.arrayHelper.intFromStrStrDic(attributeDict, BalancingConstant.TYPE_TEAM_REF_HAPPENED_ATTRIBUTE)

        if ref == nil {
            return;
        }

        let vo: HappenedBalancingVO = HappenedBalancingVO( ref! )

        parseHappenType(vo, attributeDict)
        balancing.balancingModel.typeTeamVOs[ vo.ref ] = vo

    }

    private func applySeasonsToSeasonsGroups() {

        for seasonVO:SeasonBalancingVO in balancing.balancingModel.seasonVOs.values {
            for seasonGroupRef:Int in seasonVO.seasonGroupRefs!.keys {
                if let seasonGroupVO:SeasonGroupBalancingVO = balancing.balancingModel.seasonGroupVOs[seasonGroupRef] {
                    seasonGroupVO.seasonRefs[seasonVO.ref] = NSNull()
                }
                else
                {
                    balancing.balancingModel.seasonVOs.removeValue(forKey: seasonVO.ref)
                }
            }
        }

        for seasonGroupVO:SeasonGroupBalancingVO in balancing.balancingModel.seasonGroupVOs.values {
            if seasonGroupVO.seasonRefs.isEmpty {
                balancing.balancingModel.seasonGroupVOs.removeValue(forKey: seasonGroupVO.ref)
            }
        }
    }

    private func applySeasonGroupOrder() {
        var arrayVOs:[SeasonGroupBalancingVO] = []

        for value:SeasonGroupBalancingVO in balancing.balancingModel.seasonGroupVOs.values {
            arrayVOs.append(value)
        }
        arrayVOs.sort {
            return $0.nominalDate!.timeIntervalSince1970 > $1.nominalDate!.timeIntervalSince1970
        }

        var orderedRefs:[Int] = []
        for vo:SeasonGroupBalancingVO in arrayVOs {
            orderedRefs.append(vo.ref)
        }

        balancing.balancingModel.orderedSeasonGroupRefs = orderedRefs;
    }

    private func applySeasonOrder() {
        var arrayVOs:[SeasonBalancingVO] = []

        for value:SeasonBalancingVO in balancing.balancingModel.seasonVOs.values {
            arrayVOs.append(value)
        }
        arrayVOs.sort {
            if $0.startDate!.timeIntervalSince1970 != $1.startDate!.timeIntervalSince1970 {
                return $0.startDate!.timeIntervalSince1970 > $1.startDate!.timeIntervalSince1970
            }
            return $0.endDate!.timeIntervalSince1970 > $1.endDate!.timeIntervalSince1970
        }

        var orderedRefs:[Int] = []
        for vo:SeasonBalancingVO in arrayVOs {
            orderedRefs.append(vo.ref)
        }

        balancing.balancingModel.orderedSeasonRefs = orderedRefs;
    }

    private func applyLatestSeasonGroup() {
        var latestRef:Int?

        for seasonGroupRef:Int in balancing.balancingModel.orderedSeasonGroupRefs {
            let seasonGroupVO: SeasonGroupBalancingVO = balancing.balancingModel.seasonGroupVOs[seasonGroupRef]!
            if Bool(seasonGroupVO.latest != nil) && Bool(seasonGroupVO.latest!) {
                latestRef = seasonGroupVO.ref
                break
            }
        }
        for seasonGroupRef:Int in balancing.balancingModel.orderedSeasonGroupRefs {
            let seasonGroupVO:SeasonGroupBalancingVO = balancing.balancingModel.seasonGroupVOs[seasonGroupRef]!
            if latestRef == nil {
                seasonGroupVO.latest = true
                latestRef = seasonGroupVO.ref
            } else if latestRef != seasonGroupVO.ref {
                seasonGroupVO.latest = false
            }
        }
        balancing.balancingModel.latestSeasonGroupRef = latestRef
    }

    private func applyCompetitions() {
        for competitionVO:CompetitionBalancingVO in balancing.balancingModel.competitionVOs.values {
            verifyCompetitionVO(competitionVO)
            for competitionSeasonVO: CompetitionBalancingVO in competitionVO.competitionSeasonVOs().values {
                if balancing.balancingModel.seasonVOs[competitionSeasonVO.seasonRef!] == nil {
                    competitionVO.seasonVOs!.removeValue(forKey: competitionSeasonVO.seasonRef!)
                    continue;
                }
                verifyCompetitionVO(competitionSeasonVO)
            }
            let orderedSeasonRefs: [Int] = balancing.balancingHelper.getOrderedSeasonRefs(competitionVO.seasonVOs!)

            if competitionVO.isShell {
                if competitionVO.seasonVOs!.count == 0 {
                    balancing.balancingModel.competitionVOs.removeValue(forKey: competitionVO.ref)
                    continue
                }
                competitionVO.isShell = false
                let bestSeasonRef: Int = orderedSeasonRefs[0]
                let bestCompetitionSeasonVO: CompetitionBalancingVO = competitionVO.competitionSeasonVOs()[bestSeasonRef]!
                balancing.parseBalancingHelper.cloneCompetitionToNil(competitionVO, bestCompetitionSeasonVO)
            }
        }

        for competitionVO:CompetitionBalancingVO in balancing.balancingModel.competitionVOs.values {
            verifyCompetitionVOAgainstCompetitionVO(competitionVO)
            for competitionSeasonVO: CompetitionBalancingVO in competitionVO.competitionSeasonVOs().values {
                verifyCompetitionVOAgainstCompetitionVO(competitionSeasonVO)
            }

            let orderedSeasonRefs: [Int] = balancing.balancingHelper.getOrderedSeasonRefs(competitionVO.seasonVOs!)
            if competitionVO.parentCompetitionRef == nil {
                for seasonRef:Int in orderedSeasonRefs {
                    let competitionSeasonVO: CompetitionBalancingVO = competitionVO.competitionSeasonVOs()[seasonRef]!
                    if competitionSeasonVO.parentCompetitionRef != nil {
                        competitionVO.parentCompetitionRef = competitionSeasonVO.parentCompetitionRef
                        break
                    }
                }
            }
            var prevVO:CompetitionBalancingVO = competitionVO
            for seasonRef:Int in orderedSeasonRefs {
                let competitionSeasonVO: CompetitionBalancingVO = competitionVO.competitionSeasonVOs()[seasonRef]!
                competitionSeasonVO.parentCompetitionRef = competitionVO.parentCompetitionRef
                balancing.parseBalancingHelper.cloneCompetitionToNil(competitionSeasonVO, prevVO)
                prevVO = competitionSeasonVO
            }

        }
    }

    private func verifyCompetitionVO(_ competitionVO:CompetitionBalancingVO) {
        if competitionVO.typeClubRefs != nil {
            for typeClubRef: Int in competitionVO.typeClubRefs!.keys {
                if balancing.balancingModel.typeClubVOs[typeClubRef] == nil {
                    competitionVO.typeClubRefs!.removeValue(forKey: typeClubRef)
                }
            }
            if competitionVO.typeClubRefs!.count == 0 {
                competitionVO.typeClubRefs = nil
            }
        }
        if competitionVO.typeTeamRefs != nil {
            for typeClubRef: Int in competitionVO.typeTeamRefs!.keys {
                if balancing.balancingModel.typeClubVOs[typeClubRef] == nil {
                    competitionVO.typeTeamRefs!.removeValue(forKey: typeClubRef)
                }
            }
            if competitionVO.typeTeamRefs!.count == 0 {
                competitionVO.typeTeamRefs = nil
            }
        }
        if competitionVO.structureRef != nil {
            if balancing.balancingModel.leagueStructureVOs[competitionVO.structureRef!] == nil {
                competitionVO.structureRef = nil
            }
        }
        if competitionVO.parentCompetitionRef != nil {
            if competitionVO.parentCompetitionRef == competitionVO.ref{
                competitionVO.parentCompetitionRef = nil
            }
        }
    }

    private func verifyCompetitionVOAgainstCompetitionVO(_ competitionVO:CompetitionBalancingVO) {

        if competitionVO.parentCompetitionRef != nil {
            if balancing.balancingModel.competitionVOs[competitionVO.parentCompetitionRef!] == nil {
                competitionVO.parentCompetitionRef = nil
            }
        }
    }

    private func applyListings() {
        for listingVO:ListingBalancingVO in balancing.balancingModel.listingVOs.values {
            verifyListingVO(listingVO)
            for listingSeasonVO: ListingBalancingVO in listingVO.listingSeasonVOs().values {
                verifyListingVO(listingSeasonVO)
            }

            let orderedSeasonRefs:[Int] = balancing.balancingHelper.getOrderedSeasonRefs(listingVO.seasonVOs!)

            if listingVO.isShell {
                if listingVO.seasonVOs!.count == 0 {
                    balancing.balancingModel.listingVOs.removeValue(forKey: listingVO.ref)
                    continue
                }
                listingVO.isShell = false
                let bestSeasonRef:Int = orderedSeasonRefs[0]
                let bestListingSeasonVO: ListingBalancingVO = listingVO.listingSeasonVOs()[bestSeasonRef]!
                balancing.parseBalancingHelper.cloneListingToNil(listingVO, bestListingSeasonVO)
            }

            if listingVO.parentCompetitionRef == nil {
                balancing.balancingModel.listingVOs.removeValue(forKey: listingVO.ref)
                continue
            }

            if listingVO.parentCompetitionRef == nil {
                for seasonRef:Int in orderedSeasonRefs {
                    let listingSeasonVO: ListingBalancingVO = listingVO.listingSeasonVOs()[seasonRef]!
                    if listingSeasonVO.parentCompetitionRef != nil {
                        listingVO.parentCompetitionRef = listingSeasonVO.parentCompetitionRef
                        break
                    }
                }
            }
            var prevVO:ListingBalancingVO = listingVO
            for seasonRef:Int in orderedSeasonRefs {
                let listingSeasonVO: ListingBalancingVO = listingVO.listingSeasonVOs()[seasonRef]!
                listingSeasonVO.parentCompetitionRef = listingVO.parentCompetitionRef
                balancing.parseBalancingHelper.cloneListingToNil(listingSeasonVO, prevVO)
                prevVO = listingSeasonVO
            }
        }
    }


    private func verifyListingVO(_ listingVO:ListingBalancingVO) {
        if listingVO.parentCompetitionRef != nil {
            if balancing.balancingModel.competitionVOs[listingVO.parentCompetitionRef!] == nil {
                listingVO.parentCompetitionRef = nil
            }
        }
    }

    private func applyClubs() {
        for clubVO:ClubBalancingVO in balancing.balancingModel.clubVOs.values {
            verifyClubVO(clubVO)
            for clubSeasonVO: ClubBalancingVO in clubVO.clubSeasonVOs().values {
                verifyClubVO(clubSeasonVO)
            }

            let orderedSeasonRefs:[Int] = balancing.balancingHelper.getOrderedSeasonRefs(clubVO.seasonVOs!)

            if clubVO.isShell {
                if clubVO.seasonVOs!.count == 0 {
                    balancing.balancingModel.clubVOs.removeValue(forKey: clubVO.ref)
                    continue
                }
                clubVO.isShell = false
                let bestSeasonRef:Int = orderedSeasonRefs[0]
                let bestClubSeasonVO: ClubBalancingVO = clubVO.clubSeasonVOs()[bestSeasonRef]!
                balancing.parseBalancingHelper.cloneClubToNil(clubVO, bestClubSeasonVO)
            }

            var prevVO:ClubBalancingVO = clubVO
            for seasonRef:Int in orderedSeasonRefs {
                let clubSeasonVO: ClubBalancingVO = clubVO.clubSeasonVOs()[seasonRef]!
                balancing.parseBalancingHelper.cloneClubToNil(clubSeasonVO, prevVO)
                prevVO = clubSeasonVO
            }
        }
    }

    private func verifyClubVO(_ clubVO:ClubBalancingVO) {
        if clubVO.typeClubRef != nil {
            if balancing.balancingModel.typeClubVOs[clubVO.typeClubRef!] == nil {
                clubVO.typeClubRef = nil
            }
        }
    }

    private func applyTeams() {
        for teamVO:TeamBalancingVO in balancing.balancingModel.teamVOs.values {
            verifyTeamVO(teamVO)
            for teamSeasonVO: TeamBalancingVO in teamVO.teamSeasonVOs().values {
                verifyTeamVO(teamSeasonVO)
            }

            let orderedSeasonRefs:[Int] = balancing.balancingHelper.getOrderedSeasonRefs(teamVO.seasonVOs!)

            if teamVO.isShell {
                if teamVO.seasonVOs!.count == 0 {
                    balancing.balancingModel.teamVOs.removeValue(forKey: teamVO.ref)
                    continue
                }
                teamVO.isShell = false
                let bestSeasonRef:Int = orderedSeasonRefs[0]
                let bestTeamSeasonVO: TeamBalancingVO = teamVO.teamSeasonVOs()[bestSeasonRef]!
                balancing.parseBalancingHelper.cloneTeamToNil(teamVO, bestTeamSeasonVO)
            }
            if Bool(teamVO.clubRef == nil) {
                balancing.balancingModel.teamVOs.removeValue(forKey: teamVO.ref)
                continue
            }

            if teamVO.clubRef == nil {
                for seasonRef:Int in orderedSeasonRefs {
                    let teamSeasonVO: TeamBalancingVO = teamVO.teamSeasonVOs()[seasonRef]!
                    if teamSeasonVO.clubRef != nil {
                        teamVO.clubRef = teamSeasonVO.clubRef
                        break
                    }
                }
            }

            var prevVO:TeamBalancingVO = teamVO
            for seasonRef:Int in orderedSeasonRefs {
                let teamSeasonVO: TeamBalancingVO = teamVO.teamSeasonVOs()[seasonRef]!
                teamSeasonVO.clubRef = teamVO.clubRef
                balancing.parseBalancingHelper.cloneTeamToNil(teamSeasonVO, prevVO)
                prevVO = teamSeasonVO
            }
        }
    }

    private func verifyTeamVO(_ teamVO:TeamBalancingVO) {
        if teamVO.clubRef != nil {
            if balancing.balancingModel.clubVOs[teamVO.clubRef!] == nil {
                teamVO.clubRef = nil
            }
        }
        if teamVO.typeTeamRef != nil {
            if balancing.balancingModel.typeTeamVOs[teamVO.typeTeamRef!] == nil {
                teamVO.typeTeamRef = nil
            }
        }
    }

    private func applyEvents() {
        for eventVO:EventBalancingVO in balancing.balancingModel.eventVOs.values {
            verifyEventVO(eventVO)
            for eventSeasonVO: EventBalancingVO in eventVO.eventSeasonVOs().values {
                verifyEventVO(eventSeasonVO)
            }

            let orderedSeasonRefs: [Int] = balancing.balancingHelper.getOrderedSeasonRefs(eventVO.seasonVOs!)

            if eventVO.isShell {
                if eventVO.seasonVOs!.count == 0 {
                    balancing.balancingModel.eventVOs.removeValue(forKey: eventVO.ref)
                    continue
                }
                eventVO.isShell = false
                let bestSeasonRef: Int = orderedSeasonRefs[0]
                let bestEventSeasonVO: EventBalancingVO = eventVO.eventSeasonVOs()[bestSeasonRef]!
                balancing.parseBalancingHelper.cloneEventToNil(eventVO, bestEventSeasonVO)
            }
        }

        for eventVO:EventBalancingVO in balancing.balancingModel.eventVOs.values {

            verifyEventVOAgainstEventVO(eventVO)
            for eventSeasonVO: EventBalancingVO in eventVO.eventSeasonVOs().values {
                verifyEventVOAgainstEventVO(eventSeasonVO)
            }

            var prevVO:EventBalancingVO = eventVO
            let orderedSeasonRefs: [Int] = balancing.balancingHelper.getOrderedSeasonRefs(eventVO.seasonVOs!)
            for seasonRef:Int in orderedSeasonRefs {
                let eventSeasonVO: EventBalancingVO = eventVO.eventSeasonVOs()[seasonRef]!
                balancing.parseBalancingHelper.cloneEventToNil(eventSeasonVO, prevVO)
                prevVO = eventSeasonVO
            }
        }
    }

    private func verifyEventVO(_ eventVO:EventBalancingVO) {
        if eventVO.followsEventRef != nil {
            if eventVO.followsEventRef == eventVO.ref {
                eventVO.followsEventRef = nil
            }
        }
    }

    private func verifyEventVOAgainstEventVO(_ eventVO:EventBalancingVO) {
        if eventVO.followsEventRef != nil {
            if balancing.balancingModel.eventVOs[eventVO.followsEventRef!] == nil {
                eventVO.followsEventRef = nil
            }
        }
    }

    private func applySeasonTypeOnHiatus() {
        applyOnHiatus(balancing.balancingModel.competitionVOs)
        applyOnHiatus(balancing.balancingModel.listingVOs)
        applyOnHiatus(balancing.balancingModel.clubVOs)
        applyOnHiatus(balancing.balancingModel.teamVOs)
        applyOnHiatus(balancing.balancingModel.eventVOs)
    }

    private func applyOnHiatus(_ seasonTypeVOs:[Int: SeasonTypeBalancingVO] ) {
        for seasonTypeVO:SeasonTypeBalancingVO in seasonTypeVOs.values {
            var modifierFound: Bool = false
            var includeFromStart: Bool = false
            if seasonTypeVO.added || seasonTypeVO.removed {
                modifierFound = true
                if seasonTypeVO.added {
                    includeFromStart = true
                }
            }
            if !modifierFound {
                for seasonTypeSeasonVO:SeasonTypeBalancingVO in seasonTypeVO.seasonVOs!.values {
                    if seasonTypeSeasonVO.added || seasonTypeSeasonVO.removed {
                        modifierFound = true
                        if seasonTypeSeasonVO.added && !seasonTypeSeasonVO.removed {
                            includeFromStart = true
                        }
                        break
                    }
                }
            }

            if !modifierFound {
                continue
            }

            var isAdding:Bool = false
            var prevWasAdded:Bool = false
            for i in -1..<balancing.balancingModel.orderedSeasonRefs.count {
                var currentSeasonTypeVO:SeasonTypeBalancingVO?
                var seasonRef:Int?
                if i == -1 {
                    currentSeasonTypeVO = seasonTypeVO
                } else {
                    seasonRef = balancing.balancingModel.orderedSeasonRefs[i]
                    currentSeasonTypeVO = seasonTypeVO.seasonVOs![seasonRef!]
                }

                var nextWillRemove:Bool = false
                if i < balancing.balancingModel.orderedSeasonRefs.count - 1 {
                    let nextSeasonRef  = balancing.balancingModel.orderedSeasonRefs[i + 1]
                    if let nextSeasonTypeVO:SeasonTypeBalancingVO = seasonTypeVO.seasonVOs![nextSeasonRef] {
                        if nextSeasonTypeVO.removed {
                            nextWillRemove = true
                        }
                    }
                }

                if Bool(prevWasAdded || nextWillRemove) && Bool(currentSeasonTypeVO == nil ) {
                    let bestSeasonRef:Int? = balancing.balancingHelper.getBestSeasonRefFromSelection(seasonTypeVO.seasonVOs!, forSeasonRef: seasonRef!)
                    let bestSeasonTypeVO:SeasonTypeBalancingVO = bestSeasonRef == nil ? seasonTypeVO : seasonTypeVO.seasonVOs![bestSeasonRef!]!
                    currentSeasonTypeVO = bestSeasonTypeVO.birthSeasonTypeClone(bestSeasonTypeVO.ref, seasonRef: seasonRef!)
                    seasonTypeVO.seasonVOs![seasonRef!] = currentSeasonTypeVO
                }

                prevWasAdded = false
                if currentSeasonTypeVO != nil {
                    if isAdding == false {
                        if currentSeasonTypeVO!.removed || includeFromStart {
                            isAdding = true
                            includeFromStart = false
                        }
                    }
                    if isAdding == false {
                        currentSeasonTypeVO!.onHiatus = true
                    }
                    if currentSeasonTypeVO!.added {
                        isAdding = false
                        prevWasAdded = true
                    }
                }
            }
        }
    }

    private func applyListingsToCompetitions() {
        for listingVO:ListingBalancingVO in balancing.balancingModel.listingVOs.values {

            if listingVO.parentCompetitionRef == nil {
                continue
            }
            let competitionVO:CompetitionBalancingVO = balancing.balancingModel.competitionVOs[listingVO.parentCompetitionRef!]!
            for i in -1..<balancing.balancingModel.orderedSeasonRefs.count {
                var currentListingVO: ListingBalancingVO?
                var currentCompetitionVO: CompetitionBalancingVO?
                var seasonRef: Int?
                if i == -1 {
                    currentListingVO = listingVO
                    currentCompetitionVO = competitionVO
                } else {
                    seasonRef = balancing.balancingModel.orderedSeasonRefs[i]
                    currentListingVO = listingVO.listingSeasonVOs()[seasonRef!]
                    currentCompetitionVO = competitionVO.competitionSeasonVOs()[seasonRef!]
                }

                if Bool(currentListingVO == nil) && Bool(currentCompetitionVO == nil) {
                    continue
                }

                if currentListingVO == nil {
                    let bestSeasonRef:Int? = balancing.balancingHelper.getBestSeasonRefFromSelection(listingVO.seasonVOs!, forSeasonRef: seasonRef!)
                    currentListingVO = bestSeasonRef == nil ? listingVO : listingVO.listingSeasonVOs()[bestSeasonRef!]!
                }

                if currentCompetitionVO == nil {
                    let bestSeasonRef:Int? = balancing.balancingHelper.getBestSeasonRefFromSelection(competitionVO.seasonVOs!, forSeasonRef: seasonRef!)
                    let bestCompetitionVO:CompetitionBalancingVO = bestSeasonRef == nil ? competitionVO : competitionVO.competitionSeasonVOs()[bestSeasonRef!]!
                    currentCompetitionVO = bestCompetitionVO.birthCompetitionClone(bestCompetitionVO.ref, seasonRef: seasonRef!)
                    competitionVO.seasonVOs![seasonRef!] = currentCompetitionVO!
                }
                if !currentListingVO!.onHiatus && !currentCompetitionVO!.onHiatus {
                    currentCompetitionVO!.childListingRefs[currentListingVO!.ref] = NSNull()
                }
            }
        }
    }

    private func applyCompetitionsToCompetitions() {
        for competitionAVO:CompetitionBalancingVO in balancing.balancingModel.competitionVOs.values {
            if competitionAVO.parentCompetitionRef == nil {
                continue
            }
            let competitionBVO:CompetitionBalancingVO = balancing.balancingModel.competitionVOs[competitionAVO.parentCompetitionRef!]!
            for i in -1..<balancing.balancingModel.orderedSeasonRefs.count {
                var currentCompetitionAVO: CompetitionBalancingVO?
                var currentCompetitionBVO: CompetitionBalancingVO?
                var seasonRef: Int?
                if i == -1 {
                    currentCompetitionAVO = competitionAVO
                    currentCompetitionBVO = competitionBVO
                } else {
                    seasonRef = balancing.balancingModel.orderedSeasonRefs[i]
                    currentCompetitionAVO = competitionAVO.competitionSeasonVOs()[seasonRef!]
                    currentCompetitionBVO = competitionBVO.competitionSeasonVOs()[seasonRef!]
                }

                if Bool(currentCompetitionAVO == nil) && Bool(currentCompetitionBVO == nil) {
                    continue
                }

                if currentCompetitionAVO == nil {
                    let bestSeasonRef:Int? = balancing.balancingHelper.getBestSeasonRefFromSelection(competitionAVO.seasonVOs!, forSeasonRef: seasonRef!)
                    currentCompetitionAVO = bestSeasonRef == nil ? competitionAVO : competitionAVO.competitionSeasonVOs()[bestSeasonRef!]!
                }

                if currentCompetitionBVO == nil {
                    let bestSeasonRef:Int? = balancing.balancingHelper.getBestSeasonRefFromSelection(competitionBVO.seasonVOs!, forSeasonRef: seasonRef!)
                    let bestCompetitionBVO:CompetitionBalancingVO = bestSeasonRef == nil ? competitionBVO : competitionBVO.competitionSeasonVOs()[bestSeasonRef!]!
                    currentCompetitionBVO = bestCompetitionBVO.birthCompetitionClone(bestCompetitionBVO.ref, seasonRef: seasonRef!)
                    competitionBVO.seasonVOs![seasonRef!] = currentCompetitionBVO!
                }
                if !currentCompetitionAVO!.onHiatus && !currentCompetitionBVO!.onHiatus {
                    currentCompetitionBVO!.childCompetitionRefs[currentCompetitionAVO!.ref] = NSNull()
                }
            }
        }
    }

    private func applyTeamsToClubs() {
        for teamVO:TeamBalancingVO in balancing.balancingModel.teamVOs.values {
            if teamVO.clubRef == nil {
                continue
            }
            let clubVO:ClubBalancingVO = balancing.balancingModel.clubVOs[teamVO.clubRef!]!
            for i in -1..<balancing.balancingModel.orderedSeasonRefs.count {
                var currentTeamVO: TeamBalancingVO?
                var currentClubVO: ClubBalancingVO?
                var seasonRef: Int?
                if i == -1 {
                    currentTeamVO = teamVO
                    currentClubVO = clubVO
                } else {
                    seasonRef = balancing.balancingModel.orderedSeasonRefs[i]
                    currentTeamVO = teamVO.teamSeasonVOs()[seasonRef!]
                    currentClubVO = clubVO.clubSeasonVOs()[seasonRef!]
                }

                if Bool(currentTeamVO == nil) && Bool(currentClubVO == nil) {
                    continue
                }

                if currentTeamVO == nil {
                    let bestSeasonRef:Int? = balancing.balancingHelper.getBestSeasonRefFromSelection(teamVO.seasonVOs!, forSeasonRef: seasonRef!)
                    currentTeamVO = bestSeasonRef == nil ? teamVO : teamVO.teamSeasonVOs()[bestSeasonRef!]!
                }

                if currentClubVO == nil {
                    let bestSeasonRef:Int? = balancing.balancingHelper.getBestSeasonRefFromSelection(clubVO.seasonVOs!, forSeasonRef: seasonRef!)
                    let bestClubVO:ClubBalancingVO = bestSeasonRef == nil ? clubVO : clubVO.clubSeasonVOs()[bestSeasonRef!]!
                    currentClubVO = bestClubVO.birthClubClone(bestClubVO.ref, seasonRef: seasonRef!)
                    clubVO.seasonVOs![seasonRef!] = currentClubVO!
                }
                if !currentTeamVO!.onHiatus && !currentClubVO!.onHiatus {
                    currentClubVO!.teamRefs[currentTeamVO!.ref] = NSNull()
                }
            }
        }
    }

    private func applyExtendedOnHiatus() {
        while true {
            var modified:Bool = false

            for competitionVO:CompetitionBalancingVO in balancing.balancingModel.competitionVOs.values {
                if !competitionVO.onHiatus && balancing.parseBalancingHelper.isCompetitionOnHiatus(competitionVO.ref) {
                    competitionVO.onHiatus = true
                    modified = true
                }

                for seasonRef:Int in balancing.balancingModel.orderedSeasonRefs {

                    let bestSeasonRef:Int? = balancing.balancingHelper.getBestSeasonRefFromSelection(competitionVO.seasonVOs!, forSeasonRef: seasonRef)
                    let bestCompetitionVO: CompetitionBalancingVO = bestSeasonRef == nil ? competitionVO : competitionVO.competitionSeasonVOs()[bestSeasonRef!]!
                    let onHiatus = balancing.parseBalancingHelper.isCompetitionOnHiatus(competitionVO.ref, seasonRef: seasonRef)

                    if !bestCompetitionVO.onHiatus {
                        if onHiatus {
                            var competitionSeasonVO:CompetitionBalancingVO? = competitionVO.competitionSeasonVOs()[seasonRef]
                            if competitionSeasonVO == nil {
                                competitionSeasonVO = bestCompetitionVO.birthCompetitionClone(competitionVO.ref, seasonRef: seasonRef)
                                competitionVO.seasonVOs![seasonRef] = competitionSeasonVO
                            }
                            competitionSeasonVO!.onHiatus = true
                            modified = true
                        }
                    } else {
                        if !onHiatus {
                            var competitionSeasonVO:CompetitionBalancingVO? = competitionVO.competitionSeasonVOs()[seasonRef]
                            if competitionSeasonVO == nil {
                                competitionSeasonVO = bestCompetitionVO.birthCompetitionClone(competitionVO.ref, seasonRef: seasonRef)
                                competitionVO.seasonVOs![seasonRef] = competitionSeasonVO
                            }
                            modified = true
                        }
                    }
                }
            }
            if !modified {
                break
            }
        }

        for clubVO:ClubBalancingVO in balancing.balancingModel.clubVOs.values {
            var onHiatus:Bool = true
            for teamRef:Int in clubVO.teamRefs.keys {
                let teamVO:TeamBalancingVO = balancing.balancingModel.teamVOs[teamRef]!
                if !teamVO.onHiatus {
                    onHiatus = false
                    break
                }
            }
            if onHiatus {
                clubVO.onHiatus = true
            }
            for clubSeasonVO:ClubBalancingVO in clubVO.clubSeasonVOs().values {
                var onHiatusSeason:Bool = true
                for teamRef:Int in clubSeasonVO.teamRefs.keys {
                    let teamVO:TeamBalancingVO = balancing.balancingModel.teamVOs[teamRef]!
                    let bestSeasonRef:Int? = balancing.balancingHelper.getBestSeasonRefFromSelection(teamVO.seasonVOs!, forSeasonRef: clubSeasonVO.seasonRef!)
                    let teamSeasonVO:TeamBalancingVO = bestSeasonRef == nil ? teamVO : teamVO.teamSeasonVOs()[bestSeasonRef!]!
                    if !teamSeasonVO.onHiatus {
                        onHiatusSeason = false
                        break
                    }
                }
                if onHiatusSeason {
                    clubSeasonVO.onHiatus = true
                }
            }
        }
    }


    private func applyDefaults() {

        for seasonGroupVO:SeasonGroupBalancingVO in balancing.balancingModel.seasonGroupVOs.values {
            balancing.parseBalancingHelper.applySeasonGroupDefault(seasonGroupVO)
        }

        for seasonVO:SeasonBalancingVO in balancing.balancingModel.seasonVOs.values {
            balancing.parseBalancingHelper.applySeasonDefault(seasonVO)
        }

        //Don't apply competition default. Defaults are applied to cloned vos in  happenCompetitionSeasonGroups

        for listingVO:ListingBalancingVO in balancing.balancingModel.listingVOs.values {
            balancing.parseBalancingHelper.applyListingDefault(listingVO)
        }

        for clubVO:ClubBalancingVO in balancing.balancingModel.clubVOs.values {
            balancing.parseBalancingHelper.applyClubDefault(clubVO)
        }

        for teamVO:TeamBalancingVO in balancing.balancingModel.teamVOs.values {
            balancing.parseBalancingHelper.applyTeamDefault(teamVO)
        }

        for eventVO:EventBalancingVO in balancing.balancingModel.eventVOs.values {
            balancing.parseBalancingHelper.applyEventDefault(eventVO)
        }

        for leagueStructureVO:LeagueStructureBalancingVO in balancing.balancingModel.leagueStructureVOs.values {
            balancing.parseBalancingHelper.applyLeagueStructureDefault(leagueStructureVO)
        }

        for koRoundStructureVO:KoRoundStructureBalancingVO in balancing.balancingModel.koRoundStructureVOs.values {
            balancing.parseBalancingHelper.applyStructureDefault(koRoundStructureVO)
        }

        for typeClubVO: HappenedBalancingVO in balancing.balancingModel.typeClubVOs.values {
            balancing.parseBalancingHelper.applyHappenDefault(typeClubVO)
        }

        for typeTeamVO: HappenedBalancingVO in balancing.balancingModel.typeTeamVOs.values {
            balancing.parseBalancingHelper.applyHappenDefault(typeTeamVO)
        }
    }


    private func applyAllSubSeasons() {

        let orderedSeasonRefs = balancing.balancingModel.orderedSeasonRefs;

        for competitionVO:CompetitionBalancingVO in balancing.balancingModel.competitionVOs.values {
            var currentCompetitionSeasonVO:CompetitionBalancingVO?
            for seasonRef:Int in orderedSeasonRefs {
                if competitionVO.seasonVOs![seasonRef] != nil {
                    currentCompetitionSeasonVO = competitionVO.competitionSeasonVOs()[seasonRef]
                    continue
                }
                if currentCompetitionSeasonVO == nil {
                    currentCompetitionSeasonVO = competitionVO.birthCompetitionClone(competitionVO.ref, seasonRef: seasonRef)
                    currentCompetitionSeasonVO!.onHiatus = competitionVO.onHiatus
                }
                competitionVO.seasonVOs![seasonRef] = currentCompetitionSeasonVO
            }
        }

        for listingVO:ListingBalancingVO in balancing.balancingModel.listingVOs.values {
            var currentListingSeasonVO:ListingBalancingVO?
            for seasonRef:Int in orderedSeasonRefs {
                if listingVO.seasonVOs![seasonRef] != nil {
                    currentListingSeasonVO = listingVO.listingSeasonVOs()[seasonRef]
                    continue
                }
                if currentListingSeasonVO == nil {
                    currentListingSeasonVO = listingVO.birthListingClone(listingVO.ref, seasonRef: seasonRef)
                    currentListingSeasonVO!.onHiatus = listingVO.onHiatus
                }
                listingVO.seasonVOs![seasonRef] = currentListingSeasonVO
            }
        }

        for clubVO:ClubBalancingVO in balancing.balancingModel.clubVOs.values {
            var currentClubSeasonVO:ClubBalancingVO?
            for seasonRef:Int in orderedSeasonRefs {
                if clubVO.seasonVOs![seasonRef] != nil {
                    currentClubSeasonVO = clubVO.clubSeasonVOs()[seasonRef]
                    continue
                }
                if currentClubSeasonVO == nil {
                    currentClubSeasonVO = clubVO.birthClubClone(clubVO.ref, seasonRef: seasonRef)
                    currentClubSeasonVO!.onHiatus = clubVO.onHiatus
                }
                clubVO.seasonVOs![seasonRef] = currentClubSeasonVO
            }
        }

        for teamVO:TeamBalancingVO in balancing.balancingModel.teamVOs.values {
            var currentTeamSeasonVO:TeamBalancingVO?
            for seasonRef:Int in orderedSeasonRefs {
                if teamVO.seasonVOs![seasonRef] != nil {
                    currentTeamSeasonVO = teamVO.teamSeasonVOs()[seasonRef]
                    continue
                }
                if currentTeamSeasonVO == nil {
                    currentTeamSeasonVO = teamVO.birthTeamClone(teamVO.ref, seasonRef: seasonRef)
                    currentTeamSeasonVO!.onHiatus = teamVO.onHiatus
                }
                teamVO.seasonVOs![seasonRef] = currentTeamSeasonVO
            }
        }

        for eventVO:EventBalancingVO in balancing.balancingModel.eventVOs.values {
            var currentEventSeasonVO:EventBalancingVO?
            for seasonRef:Int in orderedSeasonRefs {
                if eventVO.seasonVOs![seasonRef] != nil {
                    currentEventSeasonVO = eventVO.eventSeasonVOs()[seasonRef]
                    continue
                }
                if currentEventSeasonVO == nil {
                    currentEventSeasonVO = eventVO.birthEventClone(eventVO.ref, seasonRef: seasonRef)
                    currentEventSeasonVO!.onHiatus = eventVO.onHiatus
                }
                eventVO.seasonVOs![seasonRef] = currentEventSeasonVO
            }
        }
    }

    private func applyDefaultVOs() {
        let defaultVO:DefaultBalancingVO = balancing.balancingModel.defaultVO
        applySeasonGroupAndSeasonDefaults(defaultVO)
        applyCompetitionAndListingDefaults(defaultVO)
        applyTypeTeamDefaults(defaultVO)
        if defaultVO.listingRefs == nil {
            defaultVO.reset()
            return
        }
        balancing.parseBalancingHelper.applyDefaultDefault(defaultVO)

        for defaultTypeVO:DefaultBalancingVO in defaultVO.paramTypeVOs!.values {
            applySeasonGroupAndSeasonDefaults(defaultTypeVO)
            if defaultTypeVO.seasonGroupRef == nil {
                defaultTypeVO.seasonGroupRef = defaultVO.seasonGroupRef
                defaultTypeVO.seasonRefs = app.arrayHelper.cloneIntNulDic(defaultVO.seasonRefs!)
            }
            applyCompetitionAndListingDefaults(defaultTypeVO)
            if defaultTypeVO.competitionRefs == nil {
                defaultTypeVO.competitionRefs = app.arrayHelper.cloneIntNulDic(defaultVO.competitionRefs!)
            }
            if defaultTypeVO.listingRefs == nil {
                defaultTypeVO.listingRefs = app.arrayHelper.cloneIntNulDic(defaultVO.listingRefs!)
            }
            applyTypeTeamDefaults(defaultTypeVO)
            if defaultTypeVO.typeTeamRefs == nil {
                defaultTypeVO.typeTeamRefs = app.arrayHelper.cloneIntNulDic(defaultVO.typeTeamRefs!)
            }
        }
        let localizationType:String = localization.localizationModel.currentType!
        if defaultVO.paramTypeVOs![localizationType] == nil {
            defaultVO.paramTypeVOs![localizationType] = defaultVO.birthClone(localizationType)
        }
    }

    private func applySeasonGroupAndSeasonDefaults(_ defaultVO:DefaultBalancingVO) {
        var seasonGroupRef:Int? = defaultVO.seasonGroupRef
        var seasonRefs:[Int: NSNull]? = defaultVO.seasonRefs
        if seasonGroupRef != nil {
            if let seasonGroupVO:SeasonGroupBalancingVO = balancing.balancingModel.seasonGroupVOs[seasonGroupRef!] {
                seasonRefs = app.arrayHelper.cloneIntNulDic(seasonGroupVO.seasonRefs)
            } else {
                seasonGroupRef = nil
            }
        }
        if Bool( seasonGroupRef == nil ) && Bool( seasonRefs != nil) {
            for seasonRef:Int in seasonRefs!.keys {
                if balancing.balancingModel.seasonVOs[seasonRef] == nil {
                    seasonRefs!.removeValue(forKey: seasonRef)
                }
            }
            if seasonRefs!.count != 0 {
                let seasonRef:Int = app.arrayHelper.firstKeyFromIntDic(seasonRefs!)
                let seasonVO:SeasonBalancingVO = balancing.balancingModel.seasonVOs[seasonRef]!
                var seasonGroupRefs:[Int: NSNull] = [:]
                for checkSeasonGroupRef:Int in seasonVO.seasonGroupRefs!.keys {
                    let seasonGroupVO:SeasonGroupBalancingVO = balancing.balancingModel.seasonGroupVOs[checkSeasonGroupRef]!
                    if app.arrayHelper.intDicContainsAllKeys(seasonGroupVO.seasonRefs, seasonRefs!) {
                        seasonGroupRefs[seasonGroupVO.ref] = NSNull()
                    }
                }
                if seasonGroupRefs.count != 0 {
                    seasonGroupRef = balancing.balancingHelper.getBestSeasonGroupRef(seasonGroupRefs)!
                    let seasonGroupVO:SeasonGroupBalancingVO = balancing.balancingModel.seasonGroupVOs[seasonGroupRef!]!
                    seasonRefs = app.arrayHelper.cloneIntNulDic(seasonGroupVO.seasonRefs)
                }
            }
        }
        if Bool( seasonGroupRef == nil ) && Bool( seasonRefs == nil) {
            seasonGroupRef = balancing.balancingModel.latestSeasonGroupRef!
            let seasonGroupVO:SeasonGroupBalancingVO = balancing.balancingModel.seasonGroupVOs[seasonGroupRef!]!
            seasonRefs = app.arrayHelper.cloneIntNulDic(seasonGroupVO.seasonRefs)
        }
        defaultVO.seasonGroupRef = seasonGroupRef
        defaultVO.seasonRefs = seasonRefs
    }


    private func applyCompetitionAndListingDefaults(_ defaultVO:DefaultBalancingVO) {
        var competitionRefs:[Int: NSNull]? = defaultVO.competitionRefs
        var listingRefs:[Int: NSNull]? = defaultVO.listingRefs
        if listingRefs == nil {
            listingRefs = [:]
        }
        if competitionRefs != nil {
            for competitionRef:Int in competitionRefs!.keys {
                if balancing.balancingModel.competitionVOs[competitionRef] == nil {
                    competitionRefs!.removeValue(forKey: competitionRef)
                    continue
                }
            }
            listingRefs = balancing.balancingHelper.getAllListingRefs(competitionRefs: competitionRefs, seasonRefs: defaultVO.seasonRefs)
            if competitionRefs!.count == 0 {
                competitionRefs = nil
            }
        }
        if listingRefs!.count == 0 {
            listingRefs = nil
        }
        defaultVO.competitionRefs = competitionRefs
        defaultVO.listingRefs = listingRefs
    }

    private func applyTypeTeamDefaults(_ defaultVO:DefaultBalancingVO) {
        if defaultVO.typeTeamRefs != nil {
            for typeTeamRef:Int in defaultVO.typeTeamRefs!.keys {
                if balancing.balancingModel.typeTeamVOs[typeTeamRef] == nil {
                    defaultVO.typeTeamRefs!.removeValue(forKey: typeTeamRef)
                }
            }
            if defaultVO.typeTeamRefs!.count == 0 {
                defaultVO.typeTeamRefs = nil
            }
        }
    }

    private func updatePreferences() {
        _ = preference.preferenceHelper.getListingRefs()
    }
}
