import Foundation

class BalancingConfig {

    let fileName:String = "balancing"
    let fileExt:String = "xml"

    let leagueGamesAgainstTypes:[String]
    let koGamesAgainstTypes:[String]
    let leagueListingDecidedTypes:[String]
    let koListingDecidedTypes:[String]

    let defaultLeagueStructureBalancingVO:LeagueStructureBalancingVO
    let defaultKoRoundStructureBalancingVO:KoRoundStructureBalancingVO

    enum EventRefs:Int {
        case
                win = 1,
                promotion = 2,
                automaticPromotion = 3,
                relegation = 4,
                automaticRelegation = 5,
                promotionPlayOff = 6,
                relegationPlayOff = 7,
                split = 8,
                groupQualify = 9,
                championsLeague = 10,
                automaticChampionsLeague = 11,
                championsLeaguePlayOff = 12,
                europaLeague = 13,
                europaLeaguePlayOff = 14,
                domesticEuropaLeaguePlayOff = 15,
                groupWin = 16,
                groupEuropaLeague = 17
    }

    init() {

        leagueGamesAgainstTypes = [
                BalancingConstant.VAL_1_HOME_1_AWAY_LEAGUE_AGAINST_STRUCTURE_VALUE,
                BalancingConstant.VAL_1_NEUTRAL_LEAGUE_AGAINST_STRUCTURE_VALUE,
                BalancingConstant.VAL_2_HOME_2_AWAY_LEAGUE_AGAINST_STRUCTURE_VALUE
        ]

        koGamesAgainstTypes = [
                BalancingConstant.VAL_1_HOME_1_AWAY_KO_AGAINST_STRUCTURE_VALUE,
                BalancingConstant.VAL_1_NEUTRAL_KO_AGAINST_STRUCTURE_VALUE,
                BalancingConstant.VAL_1_HOME_OR_AWAY_AND_1_REPLAY_KO_AGAINST_STRUCTURE_VALUE
        ]

        leagueListingDecidedTypes = [
                BalancingConstant.GOAL_DIFFERENCE,
                BalancingConstant.GOALS_SCORED,
                BalancingConstant.AWAY_GOALS,
                BalancingConstant.MATCHES_WON,
                BalancingConstant.AWAY_WINS,
                BalancingConstant.GOALS_SCORED,
                BalancingConstant.HEAD_TO_HEAD_PT_GD,
                BalancingConstant.HEAD_TO_HEAD_PT_GD_GS,
                BalancingConstant.HEAD_TO_HEAD_PT_GD_AG,
                BalancingConstant.HEAD_TO_HEAD_PT_GD_GS_AG,
                BalancingConstant.GLOBAL_RANK
        ]

        koListingDecidedTypes = [
                BalancingConstant.PENALTIES,
                BalancingConstant.EXTRA_TIME_PENALTIES
        ]

        defaultLeagueStructureBalancingVO = LeagueStructureBalancingVO(0)
        defaultLeagueStructureBalancingVO.pointsPerWin = 3
        defaultLeagueStructureBalancingVO.pointsPerDraw = 1
        defaultLeagueStructureBalancingVO.pointsPerLoss = 0
        defaultLeagueStructureBalancingVO.dummyGoalsForWin = 1
        defaultLeagueStructureBalancingVO.dummyGoalsForLoss = 0
        defaultLeagueStructureBalancingVO.dummyGoalsForDraw = 0

        defaultKoRoundStructureBalancingVO = KoRoundStructureBalancingVO(0)
        defaultKoRoundStructureBalancingVO.dummyGoalsForWin = 1
        defaultKoRoundStructureBalancingVO.dummyGoalsForLoss = 0
        defaultKoRoundStructureBalancingVO.dummyGoalsForDraw = 0
    }

}
