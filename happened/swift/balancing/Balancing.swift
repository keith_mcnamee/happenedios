import Foundation

class Balancing {

    static let instance:Balancing = Balancing()

    let balancingConfig:BalancingConfig = BalancingConfig();

    let balancingHelper:BalancingHelper = BalancingHelper();

    let parseBalancingHelper:ParseBalancingHelper = ParseBalancingHelper();

    let balancingModel:BalancingModel = BalancingModel();

    func parseBalancingCommand() -> ParseBalancingCommand {
        return ParseBalancingCommand();
    }
}
