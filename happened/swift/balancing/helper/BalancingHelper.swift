import Foundation

class BalancingHelper {

    private var app:App { return App.instance }
    private var balancing:Balancing { return Balancing.instance }
    private var happen: Happen { return Happen.instance }

    func getBestMatchOrderSeasonRefs( _ seasonRef:Int? = nil ) -> [Int] {
        let orderedSeasonRefs:[Int] = balancing.balancingModel.orderedSeasonRefs
        if seasonRef == nil {
            return orderedSeasonRefs
        }
        var before:[Int] = []
        var after:[Int] = []
        var all:[Int] = []
        for checkSeasonRef:Int in orderedSeasonRefs {
            if checkSeasonRef == seasonRef! {
                all.append(checkSeasonRef)
            } else if all.count == 0 {
                before.append(checkSeasonRef)
            } else {
                after.append(checkSeasonRef)
            }
        }
        if all.count == 0 {
            before.reverse()
        }
        all.append(contentsOf: before)
        all.append(contentsOf: after)
        return all
    }

    func getBestMatchOrderSeasonGroupRefs( _ seasonGroupRef:Int? = nil ) -> [Int] {
        let orderedSeasonGroupRefs:[Int] = balancing.balancingModel.orderedSeasonGroupRefs
        if seasonGroupRef == nil {
            return orderedSeasonGroupRefs
        }
        var before:[Int] = []
        var after:[Int] = []
        var all:[Int] = []
        for checkSeasonGroupRef:Int in orderedSeasonGroupRefs {
            if checkSeasonGroupRef == seasonGroupRef! {
                all.append(checkSeasonGroupRef)
            } else if all.count == 0 {
                before.append(checkSeasonGroupRef)
            } else {
                after.append(checkSeasonGroupRef)
            }
        }
        if all.count == 0 {
            before.reverse()
        }
        all.append(contentsOf: before)
        all.append(contentsOf: after)
        return all
    }

    func getBestSeasonRefFromSelection(_ items:[Int:AnyObject], forSeasonRef:Int? = nil) -> Int? {

        let orderedSeasonRefs = balancing.balancingModel.orderedSeasonRefs

        var best:Int?
        var found = false

        for seasonRef:Int in orderedSeasonRefs {

            if items[seasonRef] != nil {
                best = seasonRef
                if forSeasonRef == nil {
                    return seasonRef
                }
            }
            if forSeasonRef != nil {
                if seasonRef == forSeasonRef! {
                    found = true
                    break
                }
            }
        }
        if found {
            return best
        }

        return nil
    }

    func getBestSeasonRef( seasonGroupRef:Int?=nil ) -> Int? {
        let useSeasonGroupRef:Int = seasonGroupRef != nil ? seasonGroupRef! : balancing.balancingModel.getDefaultVO()!.seasonGroupRef!
        let seasonGroupBalancingVO:SeasonGroupBalancingVO? = balancing.balancingModel.seasonGroupVOs[ useSeasonGroupRef ]
        if seasonGroupBalancingVO == nil {
            return nil
        }
        return getBestSeasonRefFromSelection(seasonGroupBalancingVO!.seasonRefs)
    }

    func isHistoricalSeason( _ seasonRef:Int ) -> Bool {
        let latestSeasonRefs:[Int:NSNull] = getAllSeasonRefs(seasonGroupRef: balancing.balancingModel.latestSeasonGroupRef)
        return latestSeasonRefs[seasonRef] == nil
    }

    func getOrderedSeasonRefs(_ items:[Int:AnyObject]) -> [Int] {

        var seasonRefs:[Int] = []
        if items.count == 0 {
            return seasonRefs
        }
        if items.count == 1 {
            let firstKey = app.arrayHelper.firstKeyFromIntDic(items)
            if balancing.balancingModel.seasonVOs[firstKey] != nil{
                seasonRefs.append(firstKey)
            }
            return seasonRefs
        }

        let orderedSeasonRefs = balancing.balancingModel.orderedSeasonRefs

        for seasonRef:Int in orderedSeasonRefs {
            if items[seasonRef] != nil {
                seasonRefs.append(seasonRef)
            }
        }

        return seasonRefs
    }

    func getOrderedSeasonGroupRefs(_ items:[Int:AnyObject]) -> [Int] {

        var seasonGroupRefs:[Int] = []
        if items.count == 0 {
            return seasonGroupRefs
        }
        if items.count == 1 {
            let firstKey = app.arrayHelper.firstKeyFromIntDic(items)
            if balancing.balancingModel.seasonGroupVOs[firstKey] != nil{
                seasonGroupRefs.append(firstKey)
            }
            return seasonGroupRefs
        }

        let orderedSeasonGroupRefs = balancing.balancingModel.orderedSeasonGroupRefs

        for seasonGroupRef:Int in orderedSeasonGroupRefs {
            if items[seasonGroupRef] != nil {
                seasonGroupRefs.append(seasonGroupRef)
            }
        }

        return seasonGroupRefs
    }


    func getAllSeasonRefs( seasonGroupRefs:[Int:Any]?=nil, seasonRefs:[Int:Any]? = nil, seasonGroupRef:Int?=nil, seasonRef:Int? = nil) -> [Int:NSNull] {
        var allSeasonGroupRefs:[Int:NSNull] = seasonGroupRefs != nil ? app.arrayHelper.cloneIntNulDic(seasonGroupRefs!) : [:]
        var allSeasonRefs:[Int:NSNull] = seasonRefs != nil ? app.arrayHelper.cloneIntNulDic(seasonRefs!) : [:]
        if seasonGroupRef != nil {
            allSeasonGroupRefs[seasonGroupRef!] = NSNull()
        }
        if seasonRef != nil {
            allSeasonRefs[seasonRef!] = NSNull()
        }
        for seasonGroupRef in allSeasonGroupRefs.keys {
            let seasonGroupVO:SeasonGroupBalancingVO = balancing.balancingModel.seasonGroupVOs[seasonGroupRef]!
            allSeasonRefs = app.arrayHelper.mergeIntNulDics(allSeasonRefs, seasonGroupVO.seasonRefs)
        }
        return allSeasonRefs
    }


    func getAllListingRefs(competitionRefs:[Int:Any]? = nil, listingRefs:[Int:Any]? = nil, seasonRefs:[Int:Any]? = nil) -> [Int:NSNull] {
        var refs:[Int:NSNull] = listingRefs != nil ? app.arrayHelper.cloneIntNulDic(listingRefs!) : [:]
        if competitionRefs != nil {
            for competitionRef in competitionRefs!.keys {
                let competitionVO:CompetitionBalancingVO = balancing.balancingModel.competitionVOs[competitionRef]!
                var useCompetitionVOs:[CompetitionBalancingVO] = []
                if seasonRefs == nil {
                    if !competitionVO.onHiatus {
                        useCompetitionVOs.append(competitionVO)
                    }
                } else {
                    for seasonRef:Int in seasonRefs!.keys {
                        let useCompetitionVO:CompetitionBalancingVO = competitionVO.competitionSeasonVOs()[seasonRef]!
                        if !useCompetitionVO.onHiatus {
                            useCompetitionVOs.append(useCompetitionVO)
                        }
                    }
                }
                for useCompetitionVO:CompetitionBalancingVO in useCompetitionVOs {
                    if useCompetitionVO.childCompetitionRefs.count != 0 {
                        refs = app.arrayHelper.mergeIntNulDics(refs, getAllListingRefs(competitionRefs: useCompetitionVO.childCompetitionRefs, seasonRefs: seasonRefs) )
                    }
                    if useCompetitionVO.childListingRefs.count != 0 {
                        refs = app.arrayHelper.mergeIntNulDics(refs, useCompetitionVO.childListingRefs)
                    }
                }
            }
        }
        return refs
    }

    func getAllTeamRefs(clubRefs:[Int:Any]? = nil, teamRefs:[Int:Any]? = nil, seasonRefs:[Int:Any]? = nil) -> [Int:NSNull] {
        var refs:[Int:NSNull] = teamRefs != nil ? app.arrayHelper.cloneIntNulDic(teamRefs!) : [:]
        if clubRefs != nil {
            for clubRef in clubRefs!.keys {
                let clubVO:ClubBalancingVO = balancing.balancingModel.clubVOs[clubRef]!
                var useClubVOs:[ClubBalancingVO] = []
                if seasonRefs == nil {
                    if !clubVO.onHiatus {
                        useClubVOs.append(clubVO)
                    }
                } else {
                    for seasonRef:Int in seasonRefs!.keys {
                        let useClubVO:ClubBalancingVO = clubVO.clubSeasonVOs()[seasonRef]!
                        if !useClubVO.onHiatus {
                            useClubVOs.append(useClubVO)
                        }
                    }
                }
                for useClubVO:ClubBalancingVO in useClubVOs {
                    if useClubVO.teamRefs.count != 0 {
                        refs = app.arrayHelper.mergeIntNulDics(refs, getAllTeamRefs(clubRefs: useClubVO.teamRefs, seasonRefs: seasonRefs) )
                    }
                }
            }
        }
        return refs
    }

    func getBestSeasonGroupRef(_ items:[Int:AnyObject], forSeasonGroupRef:Int? = nil) -> Int? {

        let orderedSeasonGroupRefs = balancing.balancingModel.orderedSeasonGroupRefs

        var best:Int?
        var found = false

        for seasonGroupRef:Int in orderedSeasonGroupRefs {

            if items[seasonGroupRef] != nil {
                best = seasonGroupRef
                if forSeasonGroupRef == nil {
                    return seasonGroupRef
                }
            }
            if forSeasonGroupRef != nil {
                if seasonGroupRef == forSeasonGroupRef! {
                    found = true
                    break
                }
            }
        }
        if found {
            return best
        }

        return nil
    }

    func parentCompetitionVO(_ seasonRef:Int, _ listingRef:Int ) -> CompetitionBalancingVO? {
        if let happenedVO:CompetitionVO = happen.happenedHelper.parentCompetitionVO(seasonRef, listingRef) {
            return happenedVO.balancingVO
        }
        return nil
    }

    func getLeagueStructureVO(_ seasonRef:Int, _ listingRef:Int ) -> LeagueStructureBalancingVO? {
        if let competitionVO:CompetitionBalancingVO = parentCompetitionVO(seasonRef, listingRef) {
            return balancing.balancingModel.leagueStructureVOs[competitionVO.structureRef!]
        }
        return nil
    }

    func getClubVO(_ teamRef:Int, seasonRef:Int? = nil) -> ClubBalancingVO? {
        let teamVO:TeamBalancingVO? = balancing.balancingModel.getTeamVO(teamRef, seasonRef: seasonRef)
        if( teamVO == nil )
        {
            return nil
        }
        return balancing.balancingModel.getClubVO( teamVO!.clubRef!, seasonRef: seasonRef)
    }

}
