import Foundation

class ParseBalancingHelper {

    private var app:App { return App.instance }
    private var balancing:Balancing { return Balancing.instance }

    var isBalancingValid:Bool {
        get {
            let balancingModel:BalancingModel = balancing.balancingModel
            if !balancingModel.defaultVO.initialized {
                return false
            }
            if balancingModel.seasonGroupVOs.isEmpty {
                return false
            }
            if balancingModel.seasonVOs.isEmpty {
                return false
            }
            if balancingModel.competitionVOs.isEmpty {
                return false
            }
            if balancingModel.listingVOs.isEmpty {
                return false
            }
            if balancingModel.clubVOs.isEmpty {
                return false
            }
            if balancingModel.teamVOs.isEmpty {
                return false
            }
            if balancingModel.eventVOs.isEmpty {
                return false
            }
            if balancingModel.leagueStructureVOs.isEmpty {
                return false
            }
            if balancingModel.typeClubVOs.isEmpty {
                return false
            }
            if balancingModel.typeTeamVOs.isEmpty {
                return false
            }
            return true
        }
    }

    func cloneHappenToNil(_ toVO: HappenedBalancingVO, _ fromVO: HappenedBalancingVO) {
        if Bool(toVO.baseTextID == nil) && Bool(fromVO.baseTextID != nil) {
            toVO.baseTextID = fromVO.baseTextID
        }
        if Bool(toVO.publicID == nil) && Bool(fromVO.publicID != nil) {
            toVO.publicID = fromVO.publicID
        }
        if Bool(toVO.recognizer == nil) && Bool(fromVO.recognizer != nil) {
            toVO.recognizer = fromVO.recognizer
        }
    }

    func cloneCompetitionToNil(_ toVO:CompetitionBalancingVO, _ fromVO:CompetitionBalancingVO) {
        cloneHappenToNil(toVO, fromVO)

        if Bool(toVO.competitionType == nil) && Bool(fromVO.competitionType != nil) {
            toVO.competitionType = fromVO.competitionType
        }

        if Bool(toVO.structureRef == nil) && Bool(fromVO.structureRef != nil) {
            toVO.structureRef = fromVO.structureRef
        }

        if Bool(toVO.parentCompetitionRef == nil) && Bool(fromVO.parentCompetitionRef != nil) {
            toVO.parentCompetitionRef = fromVO.parentCompetitionRef
        }

        if Bool(toVO.reverseRound == nil) && Bool(fromVO.reverseRound != nil) {
            toVO.reverseRound = fromVO.reverseRound
        }

        if Bool(toVO.listPriority == nil) && Bool(fromVO.listPriority != nil) {
            toVO.listPriority = fromVO.listPriority
        }

        if Bool(toVO.showPriority == nil) && Bool(fromVO.showPriority != nil) {
            toVO.showPriority = fromVO.showPriority
        }

        if Bool(toVO.applyGlobalRankAtEnd == nil) && Bool(fromVO.applyGlobalRankAtEnd != nil) {
            toVO.applyGlobalRankAtEnd = fromVO.applyGlobalRankAtEnd
        }

        if Bool(toVO.numberOfTeams == nil) && Bool(fromVO.numberOfTeams != nil) {
            toVO.numberOfTeams = fromVO.numberOfTeams
        }

        if Bool(toVO.numberOfTeams == nil) && Bool(fromVO.numberOfTeams != nil) {
            toVO.numberOfTeams = fromVO.numberOfTeams
        }

        if Bool(toVO.typeClubRefs == nil) && Bool(fromVO.typeClubRefs != nil) {
            toVO.typeClubRefs = app.arrayHelper.cloneIntNulDic(fromVO.typeClubRefs!)
        }

        if Bool(toVO.typeTeamRefs == nil) && Bool(fromVO.typeTeamRefs != nil) {
            toVO.typeTeamRefs = app.arrayHelper.cloneIntNulDic(fromVO.typeTeamRefs!)
        }
    }

    func cloneListingToNil(_ toVO:ListingBalancingVO, _ fromVO:ListingBalancingVO) {
        cloneHappenToNil(toVO, fromVO)

        if Bool(toVO.parentCompetitionRef == nil) && Bool(fromVO.parentCompetitionRef != nil) {
            toVO.parentCompetitionRef = fromVO.parentCompetitionRef
        }

        if Bool(toVO.listPriority == nil) && Bool(fromVO.listPriority != nil) {
            toVO.listPriority = fromVO.listPriority
        }
    }

    func cloneClubToNil(_ toVO:ClubBalancingVO, _ fromVO:ClubBalancingVO) {
        cloneHappenToNil(toVO, fromVO)

        if Bool(toVO.typeClubRef == nil) && Bool(fromVO.typeClubRef != nil) {
            toVO.typeClubRef = fromVO.typeClubRef
        }

        if Bool(toVO.hashtag == nil) && Bool(fromVO.hashtag != nil) {
            toVO.hashtag = fromVO.hashtag
        }
    }

    func cloneTeamToNil(_ toVO:TeamBalancingVO, _ fromVO:TeamBalancingVO) {
        cloneHappenToNil(toVO, fromVO)

        if Bool(toVO.clubRef == nil) && Bool(fromVO.clubRef != nil) {
            toVO.clubRef = fromVO.clubRef
        }

        if Bool(toVO.typeTeamRef == nil) && Bool(fromVO.typeTeamRef != nil) {
            toVO.typeTeamRef = fromVO.typeTeamRef
        }
    }

    func cloneEventToNil(_ toVO:EventBalancingVO, _ fromVO:EventBalancingVO) {
        cloneHappenToNil(toVO, fromVO)

        if Bool(toVO.priority == nil) && Bool(fromVO.priority != nil) {
            toVO.priority = fromVO.priority
        }

        if Bool(toVO.followsEventRef == nil) && Bool(fromVO.followsEventRef != nil) {
            toVO.followsEventRef = fromVO.followsEventRef
        }

        if Bool(toVO.splSplit == nil) && Bool(fromVO.splSplit != nil) {
            toVO.splSplit = fromVO.splSplit
        }

        if Bool(toVO.positive == nil) && Bool(fromVO.positive != nil) {
            toVO.positive = fromVO.positive
        }
    }

    func applyHappenDefault(_ vo: HappenedBalancingVO) {
        if vo.baseTextID == nil {
            vo.baseTextID = ""
        }
        if vo.publicID == nil {
            vo.publicID = ""
        }
        if vo.recognizer == nil {
            vo.recognizer = ""
        }
    }

    func applySeasonGroupDefault(_ vo:SeasonGroupBalancingVO) {
        applyHappenDefault(vo)

        if vo.nominalDate == nil {
            vo.nominalDate = Date()
        }

        if vo.latest == nil {
            vo.latest = false
        }
    }

    func applySeasonDefault(_ vo:SeasonBalancingVO) {
        applyHappenDefault(vo)

        if vo.startDate == nil {
            vo.startDate = Date()
        }

        if vo.endDate == nil {
            vo.endDate = Date()
        }

        if vo.seasonGroupRefs == nil {
            vo.seasonGroupRefs = [:]
        }
    }

    func applyCompetitionDefault(_ vo:CompetitionBalancingVO) {
        applyHappenDefault(vo)

        //n.b. parentCompetitionRef default value is not set. nil is used to determine root rather than 0

        if vo.competitionType == nil {
            vo.competitionType = ""
        }

        if vo.structureRef == nil {
            vo.structureRef = 0
        }

        if vo.reverseRound == nil {
            vo.reverseRound = 0
        }

        if vo.listPriority == nil {
            vo.listPriority = 0
        }

        if vo.showPriority == nil {
            vo.showPriority = 0
        }

        if vo.applyGlobalRankAtEnd == nil {
            vo.applyGlobalRankAtEnd = false
        }

        if vo.numberOfTeams == nil {
            vo.numberOfTeams = 0
        }

        if vo.typeClubRefs == nil {
            vo.typeClubRefs = [:]
        }

        if vo.typeTeamRefs == nil {
            vo.typeTeamRefs = [:]
        }
    }

    func applyListingDefault(_ vo:ListingBalancingVO) {
        applyHappenDefault(vo)

        if vo.parentCompetitionRef == nil {
            vo.parentCompetitionRef = 0
        }

        if vo.listPriority == nil {
            vo.listPriority = 0
        }
    }

    func applyClubDefault(_ vo:ClubBalancingVO) {
        applyHappenDefault(vo)

        if vo.typeClubRef == nil {
            vo.typeClubRef = 0
        }

        if vo.hashtag == nil {
            vo.hashtag = ""
        }
    }

    func applyTeamDefault(_ vo:TeamBalancingVO) {
        applyHappenDefault(vo)

        if vo.clubRef == nil {
            vo.clubRef = 0
        }

        if vo.typeTeamRef == nil {
            vo.typeTeamRef = 0
        }
    }

    func applyEventDefault(_ vo:EventBalancingVO) {
        applyHappenDefault(vo)

        if vo.priority == nil {
            vo.priority = 0
        }

        if vo.followsEventRef == nil {
            vo.followsEventRef = 0
        }

        if vo.splSplit == nil {
            vo.splSplit = false
        }

        if vo.positive == nil {
            vo.positive = false
        }
    }

    func applyStructureDefault(_ vo:StructureBalancingVO) {
        applyHappenDefault(vo)

        if vo.structureType == nil {
            vo.structureType = ""
        }

        if vo.gamesAgainstType == nil {
            vo.gamesAgainstType = ""
        }

        if vo.dummyGoalsForWin == nil {
            vo.dummyGoalsForWin = 0
        }

        if vo.dummyGoalsForDraw == nil {
            vo.dummyGoalsForDraw = 0
        }

        if vo.dummyGoalsForLoss == nil {
            vo.dummyGoalsForLoss = 0
        }

        if vo.listingDecidedOrder == nil {
            vo.listingDecidedOrder = []
        }
    }

    func applyLeagueStructureDefault(_ vo:LeagueStructureBalancingVO) {
        applyStructureDefault(vo)

        if vo.pointsPerWin == nil {
            vo.pointsPerWin = 0
        }

        if vo.pointsPerDraw == nil {
            vo.pointsPerDraw = 0
        }

        if vo.pointsPerLoss == nil {
            vo.pointsPerLoss = 0
        }
    }

    func applyDefaultDefault(_ vo:DefaultBalancingVO) {

        if vo.seasonGroupRef == nil {
            vo.seasonGroupRef = 0
        }

        if vo.seasonRefs == nil {
            vo.seasonRefs = [:]
        }

        if vo.typeTeamRefs == nil {
            vo.typeTeamRefs = [:]
        }

        if vo.competitionRefs == nil {
            vo.competitionRefs = [:]
        }

        if vo.listingRefs == nil {
            vo.listingRefs = [:]
        }
    }

    func isCompetitionOnHiatus(_ competitionRef:Int, seasonRef:Int?=nil) -> Bool {
        let balancingModel:BalancingModel = balancing.balancingModel
        let balancingHelper:BalancingHelper = balancing.balancingHelper
        let baseCompetitionVO = balancingModel.competitionVOs[competitionRef]!
        var competitionVO = baseCompetitionVO
        if seasonRef != nil {
            let bestSeasonRef:Int? = balancingHelper.getBestSeasonRefFromSelection(baseCompetitionVO.seasonVOs!, forSeasonRef: seasonRef!)
            competitionVO = bestSeasonRef == nil ? baseCompetitionVO : baseCompetitionVO.competitionSeasonVOs()[bestSeasonRef!]!
        }
        if competitionVO.onHiatus {
            return true
        }
        for childListingRef: Int in competitionVO.childListingRefs.keys {
            if let baseChildListingVO: ListingBalancingVO = balancingModel.listingVOs[childListingRef] {
                var childListingVO: ListingBalancingVO = baseChildListingVO
                if seasonRef != nil {
                    let bestSeasonRef:Int? = balancingHelper.getBestSeasonRefFromSelection(baseChildListingVO.seasonVOs!, forSeasonRef: seasonRef!)
                    childListingVO = bestSeasonRef == nil ? baseChildListingVO : baseChildListingVO.listingSeasonVOs()[bestSeasonRef!]!
                }
                if !childListingVO.onHiatus {
                    return false
                }
            }
        }
        for childCompetitionRef: Int in competitionVO.childCompetitionRefs.keys {
            if let baseChildCompetitionVO: CompetitionBalancingVO = balancingModel.competitionVOs[childCompetitionRef] {
                var childCompetitionVO: CompetitionBalancingVO = baseChildCompetitionVO
                if seasonRef != nil {
                    let bestSeasonRef:Int? = balancingHelper.getBestSeasonRefFromSelection(baseChildCompetitionVO.seasonVOs!, forSeasonRef: seasonRef!)
                    childCompetitionVO = bestSeasonRef == nil ? baseChildCompetitionVO : baseChildCompetitionVO.competitionSeasonVOs()[bestSeasonRef!]!
                }
                if !childCompetitionVO.onHiatus {
                    return false
                }
            }
        }
        return true
    }
}
