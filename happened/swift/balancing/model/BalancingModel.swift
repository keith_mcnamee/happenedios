import Foundation

class BalancingModel {

    private var localization:Localization { return Localization.instance }

    var version:Int?

    var defaultVO:DefaultBalancingVO = DefaultBalancingVO()

    var seasonGroupVOs:[Int: SeasonGroupBalancingVO] = [:]

    var seasonVOs:[Int: SeasonBalancingVO] = [:]

    var competitionVOs:[Int: CompetitionBalancingVO] = [:]

    var listingVOs:[Int: ListingBalancingVO] = [:]

    var clubVOs:[Int: ClubBalancingVO] = [:]

    var teamVOs:[Int: TeamBalancingVO] = [:]

    var eventVOs:[Int: EventBalancingVO] = [:]

    var leagueStructureVOs:[Int: LeagueStructureBalancingVO] = [:]

    var koRoundStructureVOs:[Int: KoRoundStructureBalancingVO] = [:]

    var typeClubVOs:[Int: HappenedBalancingVO] = [:]

    var typeTeamVOs:[Int: HappenedBalancingVO] = [:]


    var orderedSeasonGroupRefs:[Int] = []

    var orderedSeasonRefs:[Int] = []

    var latestSeasonGroupRef:Int?

    var initialized:Bool = false


    func reset()
    {
        defaultVO.reset()
        seasonGroupVOs.removeAll()
        seasonVOs.removeAll()
        competitionVOs.removeAll()
        listingVOs.removeAll()
        clubVOs.removeAll()
        teamVOs.removeAll()
        eventVOs.removeAll()
        leagueStructureVOs.removeAll()
        koRoundStructureVOs.removeAll()
        typeClubVOs.removeAll()
        typeClubVOs.removeAll()

        orderedSeasonGroupRefs.removeAll()
        orderedSeasonRefs.removeAll()
        latestSeasonGroupRef = nil
        initialized = false
    }

    var primarySeasonRef:Int? {
        get {
            if orderedSeasonRefs.count == 0 {
                return nil
            }
            return orderedSeasonRefs[0]
        }
    }

    func getDefaultVO(_ param:String? = nil) -> DefaultBalancingVO? {
        let useParam:String = param != nil ? param! : localization.localizationModel.currentType!
        if let childVO:DefaultBalancingVO = defaultVO.paramTypeVOs![useParam] {
            return childVO
        }
        return nil
    }

    func getCompetitionVO(_ competitionRef:Int, seasonRef:Int? = nil) -> CompetitionBalancingVO? {
        if let competitionVO:CompetitionBalancingVO = competitionVOs[competitionRef] {
            if seasonRef == nil {
                return competitionVO
            }
            return competitionVO.seasonVOs![seasonRef!] as? CompetitionBalancingVO
        }
        return nil
    }

    func getListingVO(_ listingRef:Int, seasonRef:Int? = nil) -> ListingBalancingVO? {
        if let listingVO:ListingBalancingVO = listingVOs[listingRef] {
            if seasonRef == nil {
                return listingVO
            }
            return listingVO.seasonVOs![seasonRef!] as? ListingBalancingVO
        }
        return nil
    }

    func getClubVO(_ clubRef:Int, seasonRef:Int? = nil) -> ClubBalancingVO? {
        if let clubVO:ClubBalancingVO = clubVOs[clubRef] {
            if seasonRef == nil {
                return clubVO
            }
            return clubVO.seasonVOs![seasonRef!] as? ClubBalancingVO
        }
        return nil
    }

    func getTeamVO(_ teamRef:Int, seasonRef:Int? = nil) -> TeamBalancingVO? {
        if let teamVO:TeamBalancingVO = teamVOs[teamRef] {
            if seasonRef == nil {
                return teamVO
            }
            return teamVO.seasonVOs![seasonRef!] as? TeamBalancingVO
        }
        return nil
    }

    func getEventVO(_ eventRef:Int, seasonRef:Int? = nil) -> EventBalancingVO? {
        if let eventVO:EventBalancingVO = eventVOs[eventRef] {
            if seasonRef == nil {
                return eventVO
            }
            return eventVO.seasonVOs![seasonRef!] as? EventBalancingVO
        }
        return nil
    }
}
