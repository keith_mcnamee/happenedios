import Foundation

class SeasonTypeBalancingVO: HappenedBalancingVO {

    let seasonRef:Int?

    var isShell:Bool

    var added:Bool = false

    var removed:Bool = false

    var onHiatus:Bool = false

    var seasonVOs:[Int: SeasonTypeBalancingVO]?

    init(_ ref:Int, seasonRef:Int? = nil, isShell:Bool = false) {
        self.seasonRef = seasonRef
        self.isShell = isShell

        super.init(ref)
    }

    func birthSeasonTypeClone(_ ref:Int, seasonRef:Int? = nil) -> SeasonTypeBalancingVO {
        let vo:SeasonTypeBalancingVO = birthSeasonType(ref, seasonRef: seasonRef)
        clone(vo)
        return vo
    }

    func birthSeasonType(_ ref:Int, seasonRef:Int? = nil) -> SeasonTypeBalancingVO {
        return SeasonTypeBalancingVO(ref, seasonRef: seasonRef)
    }
}
