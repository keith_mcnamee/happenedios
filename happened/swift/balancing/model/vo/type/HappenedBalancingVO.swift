import Foundation

class HappenedBalancingVO {

    let ref:Int

    var baseTextID:String?

    var publicID:String?

    var recognizer:String?

    init(_ ref:Int) {
        self.ref = ref
    }

    func birthClone(_ ref:Int) -> HappenedBalancingVO {
        let vo: HappenedBalancingVO = birth(ref)
        clone(vo)
        return vo
    }

    func birth(_ ref:Int) -> HappenedBalancingVO {
        return HappenedBalancingVO(ref)
    }

    func clone(_ vo: HappenedBalancingVO) {
        vo.baseTextID = baseTextID
        vo.publicID = publicID
        vo.recognizer = recognizer
    }
}
