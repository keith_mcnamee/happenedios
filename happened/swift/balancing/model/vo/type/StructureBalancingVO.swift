import Foundation

class StructureBalancingVO: HappenedBalancingVO {


    var structureType:String?

    var gamesAgainstType:String?

    var dummyGoalsForWin:Int?

    var dummyGoalsForDraw:Int?

    var dummyGoalsForLoss:Int?

    var listingDecidedOrder:[String]?

}
