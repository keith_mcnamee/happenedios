import Foundation

class KoRoundStructureBalancingVO: StructureBalancingVO {

    override init(_ ref:Int) {
        super.init(ref)

        structureType = BalancingConstant.KO_ROUND_STRUCTURE_TYPE
    }
}
