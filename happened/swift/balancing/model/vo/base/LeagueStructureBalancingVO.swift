import Foundation

class LeagueStructureBalancingVO: StructureBalancingVO {

    var pointsPerWin:Int?

    var pointsPerDraw:Int?

    var pointsPerLoss:Int?

    override init(_ ref:Int) {
        super.init(ref)

        structureType = BalancingConstant.LEAGUE_STRUCTURE_TYPE
    }

}
