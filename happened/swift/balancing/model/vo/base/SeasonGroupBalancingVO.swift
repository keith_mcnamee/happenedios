import Foundation

class SeasonGroupBalancingVO: HappenedBalancingVO {

    var nominalDate:Date?

    var latest:Bool?

    var seasonRefs:[Int: NSNull] = [:]

}
