import Foundation

class SeasonBalancingVO: HappenedBalancingVO {

    var startDate:Date?

    var endDate:Date?

    var seasonGroupRefs:[Int: NSNull]?

    func birthSeasonClone(_ ref:Int) -> SeasonBalancingVO {
        return birthClone(ref) as! SeasonBalancingVO
    }

    override func birth(_ ref:Int) -> HappenedBalancingVO {
        return SeasonBalancingVO(ref)
    }

    override func clone(_ vo: HappenedBalancingVO) {
        super.clone(vo)
        (vo as? SeasonBalancingVO)?.startDate = startDate;
        (vo as? SeasonBalancingVO)?.endDate = endDate;
        (vo as? SeasonBalancingVO)?.seasonGroupRefs = seasonGroupRefs;
    }

}
