import Foundation

class TeamBalancingVO: SeasonTypeBalancingVO {

    var clubRef:Int?

    var typeTeamRef:Int?

    override init(_ ref:Int, seasonRef:Int? = nil, isShell:Bool = false) {

        super.init(ref, seasonRef: seasonRef, isShell:isShell)

        if seasonRef == nil {
            seasonVOs = [:]
        }
    }

    func teamSeasonVOs() -> [Int: TeamBalancingVO] {
        return seasonVOs as! [Int: TeamBalancingVO]
    }

    func birthTeamClone(_ ref:Int, seasonRef:Int? = nil) -> TeamBalancingVO {
        return birthSeasonTypeClone(ref, seasonRef: seasonRef) as! TeamBalancingVO
    }

    override func birthSeasonType(_ ref:Int, seasonRef:Int? = nil) -> SeasonTypeBalancingVO {
        return TeamBalancingVO(ref, seasonRef: seasonRef)
    }

    //n.b. seasonVOs not cloned to prevent inevitable season inception
    override func clone(_ vo: HappenedBalancingVO) {
        super.clone(vo)
        (vo as? TeamBalancingVO)?.clubRef = clubRef;
        (vo as? TeamBalancingVO)?.typeTeamRef = typeTeamRef;
    }

}
