import Foundation

class ClubBalancingVO: SeasonTypeBalancingVO {

    private var app:App { return App.instance }

    var typeClubRef:Int?

    var hashtag:String?

    var teamRefs:[Int: NSNull] = [:]

    override init(_ ref:Int, seasonRef:Int? = nil, isShell:Bool = false) {

        super.init(ref, seasonRef: seasonRef, isShell:isShell)

        if seasonRef == nil {
            seasonVOs = [:]
        }
    }

    func clubSeasonVOs() -> [Int: ClubBalancingVO] {
        return seasonVOs as! [Int: ClubBalancingVO]
    }

    func birthClubClone(_ ref:Int, seasonRef:Int? = nil) -> ClubBalancingVO {
        return birthSeasonTypeClone(ref, seasonRef: seasonRef) as! ClubBalancingVO
    }

    override func birthSeasonType(_ ref:Int, seasonRef:Int? = nil) -> SeasonTypeBalancingVO {
        return ClubBalancingVO(ref, seasonRef: seasonRef)
    }

    //n.b. seasonVOs not cloned to prevent inevitable season inception
    override func clone(_ vo: HappenedBalancingVO) {
        super.clone(vo)
        (vo as? ClubBalancingVO)?.typeClubRef = typeClubRef;
        (vo as? ClubBalancingVO)?.hashtag = hashtag;
        (vo as? ClubBalancingVO)?.teamRefs = app.arrayHelper.cloneIntNulDic(teamRefs)
    }

}
