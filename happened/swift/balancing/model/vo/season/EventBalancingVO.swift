import Foundation

class EventBalancingVO: SeasonTypeBalancingVO {

    var priority:Int?

    var followsEventRef:Int?

    var splSplit:Bool?

    var positive:Bool?

    override init(_ ref:Int, seasonRef:Int? = nil, isShell:Bool = false) {

        super.init(ref, seasonRef: seasonRef, isShell:isShell)

        if seasonRef == nil {
            seasonVOs = [Int: EventBalancingVO]()
        }
    }

    func eventSeasonVOs() -> [Int: EventBalancingVO] {
        return seasonVOs as! [Int: EventBalancingVO]
    }

    func birthEventClone(_ ref:Int, seasonRef:Int? = nil) -> EventBalancingVO {
        return birthSeasonTypeClone(ref, seasonRef: seasonRef) as! EventBalancingVO
    }

    override func birthSeasonType(_ ref:Int, seasonRef:Int? = nil) -> SeasonTypeBalancingVO {
        return EventBalancingVO(ref, seasonRef: seasonRef)
    }

    //n.b. seasonVOs not cloned to prevent inevitable season inception
    override func clone(_ vo: HappenedBalancingVO) {
        super.clone(vo)
        (vo as? EventBalancingVO)?.priority = priority;
        (vo as? EventBalancingVO)?.followsEventRef = followsEventRef;
        (vo as? EventBalancingVO)?.splSplit = splSplit;
        (vo as? EventBalancingVO)?.positive = positive;
    }
}
