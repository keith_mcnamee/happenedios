import Foundation

class CompetitionBalancingVO: SeasonTypeBalancingVO {

    private var app:App { return App.instance }


    var competitionType:String?

    var structureRef:Int?

    var parentCompetitionRef:Int?


    var reverseRound:Int?

    var listPriority:Int?

    var showPriority:Int?

    var applyGlobalRankAtEnd:Bool?

    var numberOfTeams:Int?

    var typeClubRefs:[Int: NSNull]?

    var typeTeamRefs:[Int: NSNull]?

    var childCompetitionRefs:[Int: NSNull] = [:]

    var childListingRefs:[Int: NSNull] = [:]

    override init(_ ref:Int, seasonRef:Int? = nil, isShell:Bool = false) {

        super.init(ref, seasonRef: seasonRef, isShell:isShell)

        if seasonRef == nil {
            seasonVOs = [:]
        }
    }

    func competitionSeasonVOs() -> [Int: CompetitionBalancingVO] {
        return seasonVOs as! [Int: CompetitionBalancingVO]
    }

    func birthCompetitionClone(_ ref:Int, seasonRef:Int? = nil) -> CompetitionBalancingVO {
        return birthSeasonTypeClone(ref, seasonRef: seasonRef) as! CompetitionBalancingVO
    }

    override func birthSeasonType(_ ref:Int, seasonRef:Int? = nil) -> SeasonTypeBalancingVO {
        return CompetitionBalancingVO(ref, seasonRef: seasonRef)
    }

    //n.b. seasonVOs not cloned to prevent inevitable season inception
    override func clone(_ vo: HappenedBalancingVO) {
        super.clone(vo)
        (vo as? CompetitionBalancingVO)?.competitionType = competitionType
        (vo as? CompetitionBalancingVO)?.structureRef = structureRef
        (vo as? CompetitionBalancingVO)?.parentCompetitionRef = parentCompetitionRef
        (vo as? CompetitionBalancingVO)?.reverseRound = reverseRound
        (vo as? CompetitionBalancingVO)?.listPriority = listPriority
        (vo as? CompetitionBalancingVO)?.showPriority = showPriority
        (vo as? CompetitionBalancingVO)?.applyGlobalRankAtEnd = applyGlobalRankAtEnd
        (vo as? CompetitionBalancingVO)?.numberOfTeams = numberOfTeams
        (vo as? CompetitionBalancingVO)?.typeClubRefs = typeClubRefs != nil ? app.arrayHelper.cloneIntNulDic(typeClubRefs!) : nil
        (vo as? CompetitionBalancingVO)?.typeTeamRefs = typeTeamRefs != nil ? app.arrayHelper.cloneIntNulDic(typeTeamRefs!) : nil
        (vo as? CompetitionBalancingVO)?.childCompetitionRefs = app.arrayHelper.cloneIntNulDic(childCompetitionRefs)
        (vo as? CompetitionBalancingVO)?.childListingRefs = app.arrayHelper.cloneIntNulDic(childListingRefs)
    }
}
