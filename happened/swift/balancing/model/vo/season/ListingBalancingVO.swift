import Foundation

class ListingBalancingVO: SeasonTypeBalancingVO {

    var parentCompetitionRef:Int?

    var listPriority:Int?

    override init(_ ref:Int, seasonRef:Int? = nil, isShell:Bool = false) {

        super.init(ref, seasonRef: seasonRef, isShell:isShell)

        if seasonRef == nil {
            seasonVOs = [:]
        }
    }

    func listingSeasonVOs() -> [Int: ListingBalancingVO] {
        return seasonVOs as! [Int: ListingBalancingVO]
    }

    func birthListingClone(_ ref:Int, seasonRef:Int? = nil) -> ListingBalancingVO {
        return birthSeasonTypeClone(ref, seasonRef: seasonRef) as! ListingBalancingVO
    }

    override func birthSeasonType(_ ref:Int, seasonRef:Int? = nil) -> SeasonTypeBalancingVO {
        return ListingBalancingVO(ref, seasonRef: seasonRef)
    }

    //n.b. seasonVOs not cloned to prevent inevitable season inception
    override func clone(_ vo: HappenedBalancingVO) {
        super.clone(vo)
        (vo as? ListingBalancingVO)?.parentCompetitionRef = parentCompetitionRef;
        (vo as? ListingBalancingVO)?.listPriority = listPriority;
    }
}
