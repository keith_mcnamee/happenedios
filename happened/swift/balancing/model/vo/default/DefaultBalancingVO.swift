import Foundation

class DefaultBalancingVO {

    private var app:App { return App.instance }

    let param:String

    var initialized:Bool = false

    var seasonGroupRef:Int?

    var seasonRefs:[Int: NSNull]?

    var typeTeamRefs:[Int: NSNull]?

    var competitionRefs:[Int: NSNull]?

    var listingRefs:[Int: NSNull]?

    var paramTypeVOs:[String: DefaultBalancingVO]?


    init(_ param:String? = nil) {
        self.param = param != nil ? param! : BalancingConstant.GENERIC_TYPE
    }

    func reset() {
        initialized = false
        seasonGroupRef = nil
        seasonRefs = nil
        typeTeamRefs = nil
        competitionRefs = nil
        listingRefs = nil
        paramTypeVOs = nil
    }

    func birthClone(_ param:String) -> DefaultBalancingVO {
        let vo:DefaultBalancingVO = birth(param)
        clone(vo)
        return vo
    }

    func birth(_ ref:String) -> DefaultBalancingVO {
        return DefaultBalancingVO(param)
    }

    func clone(_ vo:DefaultBalancingVO) {
        vo.initialized = initialized
        vo.seasonGroupRef = seasonGroupRef
        vo.seasonRefs = app.arrayHelper.cloneIntNulDic(seasonRefs!)
        vo.typeTeamRefs = app.arrayHelper.cloneIntNulDic(typeTeamRefs!)
        vo.competitionRefs = app.arrayHelper.cloneIntNulDic(competitionRefs!)
        vo.listingRefs = app.arrayHelper.cloneIntNulDic(listingRefs!)
    }

}
